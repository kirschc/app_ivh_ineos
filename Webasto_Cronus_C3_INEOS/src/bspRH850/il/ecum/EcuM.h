
#ifndef ECUM_H
#define ECUM_H


#include "Std_Types.h"
#include "EcuM_Cfg.h"


#define ECUM_SW_MAJOR_VERSION          1u

#define ECUM_SW_MINOR_VERSION          0u

#define ECUM_SW_PATCH_VERSION          0u

#define ECUM_MODULE_ID                 ((uint8_t) 0x0Au)

#define ECUM_INSTANCE_ID               ((uint8_t)0x00u)

#define ECUM_ARRAY_OFFSET              1u


#define EcuM_GetVersionInfo(                                 \
               \
   versioninfo)                                              \
   versioninfo.moduleID = ECUM_MODULE_ID;                    \
   versioninfo.vendorID = ECUM_VENDOR_ID;                    \
   versioninfo.sw_major_version = ECUM_SW_MAJOR_VERSION;     \
   versioninfo.sw_minor_version = ECUM_SW_MINOR_VERSION;     \
   versioninfo.sw_patch_version = ECUM_SW_PATCH_VERSION;


typedef uint8_t EcuM_UserType;

typedef void (*FnctType) (void);

typedef uint8_t EcuM_UserNumberType;

typedef enum
{
   ECUM_STATE_WAKEUP_ONE = 0x21u,
   ECUM_STATE_WAKEUP_TWO = 0x24u,
   ECUM_STATE_RUN  = 0x30u,
   ECUM_STATE_POST_RUN = 0x33,
   ECUM_STATE_SHUTDOWN = 0x40u,
   ECUM_STATE_PREP_SHUTDOWN = 0x44,
   ECUM_STATE_SLEEP = 0x50u,
   ECUM_STATE_POWERFAIL = 0x60u,
   ECUM_STATE_REVIVE_POWERFAIL = 0x61u,
   ECUM_STATE_INVALID = 0xFFu,
}
EcuM_StateType;

typedef enum
{
   REQUEST_NOT_ACTIVE,
   REQUEST_ACTIVE

}
EcuM_RequestType;

typedef struct
{
   unsigned int Asw              :1;
   unsigned int reserved         :7;
}
EcuM_PostRunSourceType;

typedef struct
{
   unsigned int Can              :1;
   unsigned int Asw              :1;
   unsigned int Nm               :1;
   unsigned int reserved         :5;
}
EcuM_SourceRunType;

typedef struct
{
   EcuM_SourceRunType runSource;
   EcuM_PostRunSourceType postRunSource;
}
EcuM_SourceType;

typedef struct
{
    EcuM_SourceType *sources;
    EcuM_PostRunSourceType defaultPostRunSourceInit;
    EcuM_SourceRunType defaultRunSourceInit;
    FnctType startUpOne;
    FnctType startUpTwo;
    FnctType wakeUpOne;
    FnctType wakeUpTwo;
    FnctType run;
    FnctType powerfail;
    FnctType revivePowerfail;
    FnctType prepShutDown;
    FnctType shutDown;
}
EcuM_ConfigType;


void EcuM_Init(EcuM_ConfigType *config);

void EcuM_StartupTwo(void);

Std_ReturnType EcuM_MainFunction(void);


Std_ReturnType EcuM_ReleaseRUN(
   EcuM_UserType user);

Std_ReturnType EcuM_RequestRUN(
   EcuM_UserType user);

Std_ReturnType EcuM_RequestPOST_RUN(
   EcuM_UserType user);

Std_ReturnType EcuM_ReleasePOST_RUN(
   EcuM_UserType user);

Std_ReturnType EcuM_GetState(
   EcuM_StateType* state);


extern EcuM_ConfigType EcuM_Config;

extern EcuM_StateType EcuM_currentState;


#else
   #error Multiple include of "EcuM.h"
#endif 


