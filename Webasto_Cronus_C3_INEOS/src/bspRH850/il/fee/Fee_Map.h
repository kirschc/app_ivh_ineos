
#ifndef FEE_MAP_H
#define FEE_MAP_H


#include "Std_Types.h"




#define ECU_STATUS_BLOCK               1u

#define ECU_STATUS_ADDR                EEP_GET_ADDR_BLOCK1(ecu_status)

#define ECU_STATUS_LEN                 EEP_GET_LEN_BLOCK1(ecu_status)

#define UDS_NV_MEMORY_SIGNATURE_BLOCK  1u

#define UDS_NV_MEMORY_SIGNATURE_ADDR   EEP_GET_ADDR_BLOCK1(uds_nvMemorySignature)

#define UDS_NV_MEMORY_SIGNATURE_LEN    EEP_GET_LEN_BLOCK1(uds_nvMemorySignature)

#define UDS_NV_DATA_BLOCK              1u

#define UDS_NV_DATA_ADDR               EEP_GET_ADDR_BLOCK1(uds_nvData)

#define UDS_NV_DATA_LEN                EEP_GET_LEN_BLOCK1(uds_nvData)

#define UDS_LOGICAL_SW_BLOCK_NUM       1u

#define UDS_SW_PROG_ATTEMPT_BLOCK      1u

#define UDS_SW_PROG_ATTEMPT_ADDR       EEP_GET_ADDR_BLOCK1(uds_LogSWBlockCntOfProgAttempts)

#define UDS_SW_PROG_ATTEMPT_LEN        EEP_GET_LEN_BLOCK1(uds_LogSWBlockCntOfProgAttempts)

#define UDS_SW_SUC_PROG_ATT_BLOCK      1u

#define UDS_SW_SUC_PROG_ATT_ADDR       EEP_GET_ADDR_BLOCK1(uds_LogSWBlockCntOfSucProgAttempts)

#define UDS_SW_SUC_PROG_ATT_LEN        EEP_GET_LEN_BLOCK1(uds_LogSWBlockCntOfSucProgAttempts)

#define UDS_SW_LOCK_VALUE_BLOCK        1u

#define UDS_SW_LOCK_VALUE_ADDR         EEP_GET_ADDR_BLOCK1(uds_LogSWBlockLockValue)

#define UDS_SW_LOCK_VALUE_LEN          EEP_GET_LEN_BLOCK1(uds_LogSWBlockLockValue)

#define UDS_FINGER_PRINT_BLOCK         1u

#define UDS_FINGER_PRINT_ADDR          EEP_GET_ADDR_BLOCK1(uds_fingerPrint)

#define UDS_FINGER_PRINT_LEN           EEP_GET_LEN_BLOCK1(uds_fingerPrint)

#define UDS_SGL_FINGER_PRINT_LEN       UDS_FINGER_PRINT_LEN / UDS_LOGICAL_SW_BLOCK_NUM

#define FEE_START_ADDRESS_BLOCK1_0       FLS_BASE_ADDRESS

#define FEE_START_ADDRESS_BLOCK1_1       (FEE_START_ADDRESS_BLOCK1_0 + (64u * (FEE_USED_HW_BLOCKS_0 / 4u) ))

#define FEE_START_ADDRESS_BLOCK1_2       (FEE_START_ADDRESS_BLOCK1_1 + (64u * (FEE_USED_HW_BLOCKS_0 / 4u) ))

#define FEE_START_ADDRESS_BLOCK1_3       (FEE_START_ADDRESS_BLOCK1_2 + (64u * (FEE_USED_HW_BLOCKS_0 / 4u) ))


#define UDS_ECU_INFO_BLOCK             2u

#define UDS_ECU_INFO_ADDR              EEP_GET_ADDR_BLOCK2(uds_SoftwareVersion)

#define UDS_ECU_INFO_LEN               108u

#define UDS_SW_VERSION_BLOCK           2u

#define UDS_SW_VERSION_ADDR            EEP_GET_ADDR_BLOCK2(uds_SoftwareVersion)

#define UDS_SW_VERSION_LEN             EEP_GET_LEN_BLOCK2(uds_SoftwareVersion)

#define UDS_ECU_HW_NUMBER_BLOCK        2u

#define UDS_ECU_HW_NUMBER_ADDR         EEP_GET_ADDR_BLOCK2(uds_EcuHardwareNumber)

#define UDS_ECU_HW_NUMBER_LEN          EEP_GET_LEN_BLOCK2(uds_EcuHardwareNumber)

#define UDS_ECU_HW_VERSION_BLOCK       2u

#define UDS_ECU_HW_VERSION_ADDR        EEP_GET_ADDR_BLOCK2(uds_EcuHardwareVersion)

#define UDS_ECU_HW_VERSION_LEN         EEP_GET_LEN_BLOCK2(uds_EcuHardwareVersion)

#define UDS_NODE_NUMBER_BLOCK         2u

#define UDS_NODE_NUMBER_ADDR          EEP_GET_ADDR_BLOCK2(uds_nodeNumber)

#define UDS_NODE_NUMBER_LEN           EEP_GET_LEN_BLOCK2(uds_nodeNumber)

#define UDS_NODE_TYPE_BLOCK         2u

#define UDS_NODE_TYPE_ADDR          EEP_GET_ADDR_BLOCK2(uds_nodeType)

#define UDS_NODE_TYPE_LEN           EEP_GET_LEN_BLOCK2(uds_nodeType)

#define FEE_START_ADDRESS_BLOCK2_0    (FEE_START_ADDRESS_BLOCK1_0 + (64u * FEE_USED_HW_BLOCKS_0))

#define FEE_START_ADDRESS_BLOCK2_1    (FEE_START_ADDRESS_BLOCK2_0 + (64u * (FEE_USED_HW_BLOCKS_1 / 2u) ))


#define UDS_ACTUATORCTRL_CONFIG_BLOCK  3u

#define UDS_ACTUATORCTRL_CONFIG_ADDR   EEP_GET_ADDR_BLOCK3(uds_ActuatorCtrlCfg)

#define UDS_ACTUATORCTRL_CONFIG_LEN    EEP_GET_LEN_BLOCK3(uds_ActuatorCtrlCfg)

#define FEE_START_ADDRESS_BLOCK3_0     (FEE_START_ADDRESS_BLOCK2_0 + (64u * FEE_USED_HW_BLOCKS_1))

#define FEE_START_ADDRESS_BLOCK3_1     (FEE_START_ADDRESS_BLOCK3_0 + (64u * (FEE_USED_HW_BLOCKS_2 / 2u) ))


#define UDS_SENSOR_CONFIG_BLOCK         4u

#define UDS_SENSOR_CONFIG_ADDR          EEP_GET_ADDR_BLOCK4(uds_SensorCfg)

#define UDS_SENSOR_CONFIG_LEN           EEP_GET_LEN_BLOCK4(uds_SensorCfg)

#define FEE_START_ADDRESS_BLOCK4_0    (FEE_START_ADDRESS_BLOCK3_0 + (64u * FEE_USED_HW_BLOCKS_2))

#define FEE_START_ADDRESS_BLOCK4_1    (FEE_START_ADDRESS_BLOCK4_0 + (64u * (FEE_USED_HW_BLOCKS_3 / 2u) ))


#define UDS_IOABS_CONFIG_BLOCK         5u

#define UDS_IOABS_CONFIG_ADDR          EEP_GET_ADDR_BLOCK5(uds_IoAbsCfg)

#define UDS_IOABS_CONFIG_LEN           EEP_GET_LEN_BLOCK5(uds_IoAbsCfg)

#define FEE_START_ADDRESS_BLOCK5_0    (FEE_START_ADDRESS_BLOCK4_0 + (64u * FEE_USED_HW_BLOCKS_3))

#define FEE_START_ADDRESS_BLOCK5_1    (FEE_START_ADDRESS_BLOCK5_0 + (64u * (FEE_USED_HW_BLOCKS_4 / 2u) ))


#define DEM_PRIMARY_EVENT_NV_BLOCK     6u

#define UDS_PRIMARY_EVENT_NV_ADDR          EEP_GET_ADDR_BLOCK6(dem_PrimaryEventMemory)

#define UDS_PRIMARY_EVENT_NV_LEN           EEP_GET_LEN_BLOCK6(dem_PrimaryEventMemory)

#define FEE_START_ADDRESS_BLOCK6_0    (FEE_START_ADDRESS_BLOCK5_0 + (64u * FEE_USED_HW_BLOCKS_4))

#define FEE_START_ADDRESS_BLOCK6_1    (FEE_START_ADDRESS_BLOCK6_0 + (64u * (FEE_USED_HW_BLOCKS_5 / 2u) ))


#define DEM_PRIMARY_DATA_NV_BLOCK     7u

#define DEM_PRIMARY_DATA_NV_ADDR          EEP_GET_ADDR_BLOCK7(dem_PrimaryDataMemory)

#define DEM_PRIMARY_DATA_NV_LEN           EEP_GET_LEN_BLOCK7(dem_PrimaryDataMemory)

#define FEE_START_ADDRESS_BLOCK7_0    (FEE_START_ADDRESS_BLOCK6_0 + (64u * FEE_USED_HW_BLOCKS_5))

#define FEE_START_ADDRESS_BLOCK7_1    (FEE_START_ADDRESS_BLOCK7_0 + (64u * (FEE_USED_HW_BLOCKS_6 / 2u) ))


#define UDS_APPL_UDS_CODING_BLOCK   8u

#define UDS_APPL_UDS_CODING_ADDR    EEP_GET_ADDR_BLOCK8(appl_udsCoding)

#define UDS_APPL_UDS_CODING_LEN     EEP_GET_LEN_BLOCK8(appl_udsCoding)

#define FEE_START_ADDRESS_BLOCK8_0  (FEE_START_ADDRESS_BLOCK7_0 + (64u * FEE_USED_HW_BLOCKS_6))

#define FEE_START_ADDRESS_BLOCK8_1  (FEE_START_ADDRESS_BLOCK8_0 + (64u * (FEE_USED_HW_BLOCKS_7 / 2u) ))


#define UDS_APPL_VT_CODING_BLOCK    9u

#define UDS_APPL_VT_CODING_ADDR     EEP_GET_ADDR_BLOCK9(appl_vtCoding)

#define UDS_APPL_VT_CODING_LEN      EEP_GET_LEN_BLOCK9(appl_vtCoding)

#define FEE_START_ADDRESS_BLOCK9_0  (FEE_START_ADDRESS_BLOCK8_0 + (64u * FEE_USED_HW_BLOCKS_7))

#define FEE_START_ADDRESS_BLOCK9_1  (FEE_START_ADDRESS_BLOCK9_0 + (64u * (FEE_USED_HW_BLOCKS_8 / 2u) ))


#define UDS_APPL_RUNTIME_DATA_BLOCK  10u

#define UDS_APPL_RUNTIME_DATA_ADDR   EEP_GET_ADDR_BLOCK10(appl_runtimeData)

#define UDS_APPL_RUNTIME_DATA_LEN    EEP_GET_LEN_BLOCK10(appl_runtimeData)

#define FEE_START_ADDRESS_BLOCK10_0  (FEE_START_ADDRESS_BLOCK9_0 + (64u * FEE_USED_HW_BLOCKS_8))

#define FEE_START_ADDRESS_BLOCK10_1  (FEE_START_ADDRESS_BLOCK10_0 + (64u * (FEE_USED_HW_BLOCKS_9 / 2u) ))


#define UDS_AUTOSAR_SW_ID_BLOCK        4u

typedef struct
{
   uint8_t ecu_status[2u];
   uint8_t uds_nvMemorySignature[16u];
   uint8_t uds_nvData[4u];

   uint8_t uds_LogSWBlockCntOfProgAttempts[2u];
   uint8_t uds_LogSWBlockCntOfSucProgAttempts[2u];
   uint8_t uds_LogSWBlockLockValue[2u];
   uint8_t uds_fingerPrint[UDS_LOGICAL_SW_BLOCK_NUM * 10u];
}
Eep_MapType_Block1;

typedef struct
{
   uint8_t uds_SoftwareVersion[4u];
   uint8_t uds_EcuHardwareNumber[11u];
   uint8_t uds_EcuHardwareVersion[3u];
   uint8_t uds_nodeNumber[2u];
   uint8_t uds_nodeType[1u];
}
Eep_MapType_Block2;

typedef struct
{
   uint8_t uds_ActuatorCtrlCfg[FEE_USED_DATA_LENGTH_2];
}
Eep_MapType_Block3;

typedef struct
{
   uint8_t uds_SensorCfg[FEE_USED_DATA_LENGTH_3];
}
Eep_MapType_Block4;

typedef struct
{
   uint8_t uds_IoAbsCfg[FEE_USED_DATA_LENGTH_4];
}
Eep_MapType_Block5;

typedef struct
{
   uint8_t dem_PrimaryEventMemory[FEE_USED_DATA_LENGTH_5];
}
Eep_MapType_Block6;

typedef struct
{
   uint8_t dem_PrimaryDataMemory[FEE_USED_DATA_LENGTH_6];
}
Eep_MapType_Block7;

typedef struct
{
   uint8_t appl_udsCoding[FEE_USED_DATA_LENGTH_7];
}
Eep_MapType_Block8;

typedef struct
{
   uint8_t appl_vtCoding[FEE_USED_DATA_LENGTH_8];
}
Eep_MapType_Block9;

typedef struct
{
   uint8_t appl_runtimeData[FEE_USED_DATA_LENGTH_9];
}
Eep_MapType_Block10;




#define EEP_GET_ADDR(block, elem)             (uint16_t)(uint32_t)&(((Eep_MapType_##block##*)(0u))->elem[0u])

#define EEP_GET_LEN(block, elem)              (uint16_t)sizeof(((Eep_MapType_##block##*)(0u))->elem)

#define EEP_GET_ADDR_BLOCK1(elem)             (uint16_t)(uint32_t)&(((Eep_MapType_Block1*)(0u))->elem[0u])

#define EEP_GET_LEN_BLOCK1(elem)              (uint16_t)sizeof(((Eep_MapType_Block1*)(0u))->elem)

#define EEP_GET_ADDR_BLOCK2(elem)             (uint16_t)(uint32_t)&(((Eep_MapType_Block2*)(0u))->elem[0u])

#define EEP_GET_LEN_BLOCK2(elem)              (uint16_t)sizeof(((Eep_MapType_Block2*)(0u))->elem)

#define EEP_GET_ADDR_BLOCK3(elem)             (uint16_t)(uint32_t)&(((Eep_MapType_Block3*)(0u))->elem[0u])

#define EEP_GET_LEN_BLOCK3(elem)              (uint16_t)sizeof(((Eep_MapType_Block3*)(0u))->elem)

#define EEP_GET_ADDR_BLOCK4(elem)             (uint16_t)(uint32_t)&(((Eep_MapType_Block4*)(0u))->elem[0u])

#define EEP_GET_LEN_BLOCK4(elem)              (uint16_t)sizeof(((Eep_MapType_Block4*)(0u))->elem)

#define EEP_GET_ADDR_BLOCK5(elem)             (uint16_t)(uint32_t)&(((Eep_MapType_Block5*)(0u))->elem[0u])

#define EEP_GET_LEN_BLOCK5(elem)              (uint16_t)sizeof(((Eep_MapType_Block5*)(0u))->elem)

#define EEP_GET_ADDR_BLOCK6(elem)             (uint16_t)(uint32_t)&(((Eep_MapType_Block6*)(0u))->elem[0u])

#define EEP_GET_LEN_BLOCK6(elem)              (uint16_t)sizeof(((Eep_MapType_Block6*)(0u))->elem)

#define EEP_GET_ADDR_BLOCK7(elem)             (uint16_t)(uint32_t)&(((Eep_MapType_Block7*)(0u))->elem[0u])

#define EEP_GET_LEN_BLOCK7(elem)              (uint16_t)sizeof(((Eep_MapType_Block7*)(0u))->elem)

#define EEP_GET_ADDR_BLOCK8(elem)             (uint16_t)(uint32_t)&(((Eep_MapType_Block8*)(0u))->elem[0u])

#define EEP_GET_LEN_BLOCK8(elem)              (uint16_t)sizeof(((Eep_MapType_Block8*)(0u))->elem)

#define EEP_GET_ADDR_BLOCK9(elem)             (uint16_t)(uint32_t)&(((Eep_MapType_Block9*)(0u))->elem[0u])

#define EEP_GET_LEN_BLOCK9(elem)              (uint16_t)sizeof(((Eep_MapType_Block9*)(0u))->elem)

#define EEP_GET_ADDR_BLOCK10(elem)            (uint16_t)(uint32_t)&(((Eep_MapType_Block10*)(0u))->elem[0u])

#define EEP_GET_LEN_BLOCK10(elem)             (uint16_t)sizeof(((Eep_MapType_Block10*)(0u))->elem)



#endif 


