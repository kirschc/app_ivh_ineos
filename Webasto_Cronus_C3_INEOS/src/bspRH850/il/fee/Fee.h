

#ifndef FEE_H
#define FEE_H


#include "Std_Types.h"
#include "MemIf_Types.h"
#include "Fls.h"
#include "Fee_Cfg.h"


#define FEE_SW_MAJOR_VERSION           2u

#define FEE_SW_MINOR_VERSION           0u

#define FEE_SW_PATCH_VERSION           0u

#define FEE_MODULE_ID                  ((uint8_t)21u)
#define FEE_VENDOR_ID                  ((uint16_t)0u)

#define FEE_INIT_ID                    ((uint8_t)0u)
#define FEE_SET_MODE_ID                ((uint8_t)1u)
#define FEE_READ_ID                    ((uint8_t)2u)
#define FEE_WRITE_ID                   ((uint8_t)3u)
#define FEE_CANCEL_ID                  ((uint8_t)4u)
#define FEE_GET_STATUS_ID              ((uint8_t)5u)
#define FEE_GET_JOB_RESULT_ID          ((uint8_t)6u)
#define FEE_INVALIDATE_BLOCK_ID        ((uint8_t)7u)
#define FEE_GET_VERSION_ID             ((uint8_t)8u)
#define FEE_ERASE_IMMEDIATE_BLOCK_ID   ((uint8_t)9u)
#define FEE_MAIN_FUNCTION_ID           ((uint8_t)0x12u)

#define FEE_E_UNINIT                   ((uint8_t)0x01u)
#define FEE_E_INVALID_BLOCK_NO         ((uint8_t)0x02u)
#define FEE_E_INVALID_BLOCK_OFS        ((uint8_t)0x03u)
#define FEE_E_INVALID_DATA_PTR         ((uint8_t)0x04u)
#define FEE_E_INVALID_BLOCK_LEN        ((uint8_t)0x05u)
#define FEE_E_BUSY                     ((uint8_t)0x06u)
#define FEE_E_BUSY_INTERNAL            ((uint8_t)0x07u)
#define FEE_E_INVALID_CANCEL           ((uint8_t)0x08u)

#define FEE_BLOCK_DATA_HEADER_SIZE		20u



typedef enum
{
   FEE_JOB_INIT,
   FEE_JOB_NONE,
   FEE_JOB_READ_BLOCK,
   FEE_JOB_WRITE_BLOCK,
   FEE_JOB_INVALIDATE_BLOCK,
   FEE_JOB_ERASE_UNUSED_BLOCKS
}
Fee_JobType;

typedef enum
{
   FEE_JOB_STATE_NONE,
   FEE_JOB_STATE_GET_EMPTY_SPACE,
   FEE_JOB_STATE_WRITE,
   FEE_JOB_STATE_READ,
   FEE_JOB_STATE_CHECK_EMPTY_SPACE,
   FEE_JOB_STATE_ERASE,
}
Fee_JobStateType;

typedef void (*Fee_JobEndNotificationType)(void);

typedef void (*Fee_JobErrorNotificationType)(void);

typedef struct
{
   uint32_t validationFlag; 
   uint32_t checksum; 
   uint16_t blockNumber; 
   uint16_t length; 
   uint32_t signature; 
   uint32_t counter; 
}
Fee_BlockDataHeaderType; 

typedef struct
{
   Fee_BlockDataHeaderType header;
   uint8_t data[];
}
Fee_BlockDataType;

typedef struct
{
   const uint16_t blockNumber;
   const uint16_t blockSize;
   const uint16_t usedHWBlocks;
   const boolean immediateData;
   const uint32_t numberOfWriteCycles;
   const uint32_t signature;
   const uint16_t usedDataLength;
   Fls_AddressType flashStartAddress;
   Fls_AddressType flashEndAddress;
   Fls_AddressType currentFlashAddress;
   uint8_t currentUsedBlock;
   uint32_t *erasedBlocks;
   boolean blockActive;
   Fee_BlockDataType *blockData;
}
Fee_BlockConfigType;

typedef struct
{
   const uint16_t mainFunctionPeriod;
   const Fee_JobEndNotificationType jobEndNotification;
   const Fee_JobErrorNotificationType jobErrorNotification;
   const uint16_t virtualPageSize;
   Fee_BlockConfigType *blockConfigs;
}
Fee_ConfigType;

typedef struct
{
   Fee_JobType jobType;
   Fee_JobStateType jobState;
   uint16_t blockNumberIdx;
   uint16_t offset;
   uint16_t length;
   const uint8_t *source;
   uint8_t *destination;
   Fls_AddressType flashAddress;
}
Fee_JobRequestType;

typedef struct
{
   Fee_JobType jobType;
   Fee_JobStateType jobState;
   uint16_t blockNumberIdx;
   uint8_t offset;
}
Fee_ManagementJobType;

typedef struct
{
   Fee_BlockDataHeaderType header;
   uint8_t data[FEE_MAX_WRITE_DATA];
}
Fee_TmpBlockData;



#define Fee_CallJobEndNotificaton()                \
   if(Fee_config.jobEndNotification != NULL_PTR)   \
   {                                               \
      Fee_config.jobEndNotification();             \
   }

#define Fee_CallJobErrorNotificaton()                 \
   if(Fee_config.jobErrorNotification != NULL_PTR)    \
   {                                                  \
      Fee_config.jobErrorNotification();              \
   }

#define FEE_GENERATE_BLOCK_DATA_TYPE(name,power)                        \
   typedef struct                                                       \
   {                                                                    \
   Fee_BlockDataHeaderType header;                                      \
   uint8_t data[((1u) << (power)) - sizeof(Fee_BlockDataHeaderType)];     \
   }                                                                    \
   Fee_##name


void Fee_Init(void);

#ifdef FEE_SET_MODE_SUPPORTED
#if (FEE_SET_MODE_SUPPORTED == STD_ON)
void Fee_SetMode(
   MemIf_ModeType mode);
#endif
#endif

Std_ReturnType Fee_Read(
   uint16_t length,
   uint8_t *dataBufferPtr,
   uint16_t blockOffset,
   uint16_t blockNumber);

Std_ReturnType Fee_Write(
   const uint8_t *dataBufferPtr,
   uint16_t blockNumber);

void Fee_Cancel(void);

MemIf_StatusType Fee_GetStatus(void);

MemIf_JobResultType Fee_GetJobResult(void);

Std_ReturnType Fee_InvalidateBlock(
   uint16_t blockNumber);

Std_ReturnType Fee_EraseImmediateBlock(
   uint16_t blockNumber);

extern void Fee_JobEndNotification(void);

extern void Fee_JobErrorNotification(void);

void Fee_MainFunction(void);

Std_ReturnType Fee_ProcessData(void);

Std_ReturnType Fee_getBlockNoByAddress(uint32_t addr, uint16_t* block_no, uint16_t* used_data_len, uint32_t* block_start_addr);


extern Fee_ConfigType Fee_config;



#else
#warning Multiple include of "Fee.h"
#endif 


