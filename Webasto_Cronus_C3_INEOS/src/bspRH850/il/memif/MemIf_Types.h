
#ifndef MEMIF_TYPES_H
#define MEMIF_TYPES_H





typedef enum
{
   MEMIF_UNINIT = 0u,
   MEMIF_IDLE,
   MEMIF_BUSY,
   MEMIF_BUSY_INTERNAL
}
MemIf_StatusType;

typedef enum
{
   MEMIF_JOB_OK = 0u,
   MEMIF_JOB_FAILED,
   MEMIF_JOB_PENDING,
   MEMIF_JOB_CANCELLED,
   MEMIF_BLOCK_INCONSISTENT,
   MEMIF_BLOCK_INVALID
}
MemIf_JobResultType;

typedef enum
{
   MEMIF_MODE_SLOW = 0u,
   MEMIF_MODE_FAST
}
MemIf_ModeType;




#else
    #error Multiple include of "MemIf_Types.h"
#endif 


