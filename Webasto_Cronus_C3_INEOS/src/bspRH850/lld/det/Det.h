
#ifndef DET_H
#define DET_H


#include "Std_Types.h"


#define DET_SW_MAJOR_VERSION                       0u

#define DET_SW_MINOR_VERSION                       1u

#define DET_SW_PATCH_VERSION                       0u

#define DET_MODULE_ID                              ((uint8_t)15)

#define DET_VENDOR_ID                              ((uint16_t)0)

#define DET_NUMBER_OF_TRACED_ERRORS                50u


#define Det_GetVersionInfo(                                 \
              \
   versioninfo)                                             \
   versioninfo.moduleID = DET_MODULE_ID;                    \
   versioninfo.vendorID = DET_VENDOR_ID;                    \
   versioninfo.sw_major_version = DET_SW_MAJOR_VERSION;     \
   versioninfo.sw_minor_version = DET_SW_MINOR_VERSION;     \
   versioninfo.sw_patch_version = DET_SW_PATCH_VERSION;


typedef struct
{
   uint16_t  ErrorNumber;
   uint16_t  ModuleId;
   uint8_t   InstanceId;
   uint8_t   ApiId;
   uint8_t   ErrorId;
   uint8_t   Reserved;
}
Det_ErrorType;


void Det_Init(void);

void Det_ReportError(
   uint16_t ModuleId,
   uint8_t InstanceId,
   uint8_t ApiId,
   uint8_t ErrorId);



#else
    #error Multiple include of "Det.h"
#endif 


