
#ifndef SPI_H
#define SPI_H


#include "Std_Types.h"
#include "Spi_Cfg.h"


#define SPI_SW_MAJOR_VERSION                       1u

#define SPI_SW_MINOR_VERSION                       0u

#define SPI_SW_PATCH_VERSION                       0u

#define SPI_MODULE_ID                              ((uint16_t)83)

#define SPI_INSTANCE_ID_0                          ((uint8_t)0)

#define SPI_VENDOR_ID                              ((uint16_t)0)

#define SPI_0                                      0x00u

#define SPI_1                                      0x01u

#define SPI_2                                      0x02u

#define SPI_3                                      0x03u

#define SPI_4                                      0x04u

#define SPI_CLOCK_IDLE_LOW                         0x01u

#define SPI_CLOCK_IDLE_HIGH                        0x00u

#define SPI_CLOCK_FIRST_EDGE                       0x01u

#define SPI_CLOCK_SECOND_EDGE                      0x00u

#define SPI_IB                                     0x00u

#define SPI_EB                                     0x01u

#define SPI_FILTER_BYPASS_ENABLE                   0x80u

#define SPI_PCLK_4                                 0x4000u

#define SPI_STCR_CONFIG                            0xffffu

#define SPI_WAIT_TIME                              2u


#define Spi_GetVersionInfo(                                                  \
     \
   versioninfo)                                                             \
   versioninfo.moduleID = SPI_MODULE_ID;                                    \
   versioninfo.vendorID = SPI_VENDOR_ID;                                    \
   versioninfo.sw_major_version = SPI_SW_MAJOR_VERSION;                     \
   versioninfo.sw_minor_version = SPI_SW_MINOR_VERSION;                     \
   versioninfo.sw_patch_version = SPI_SW_PATCH_VERSION;


typedef void (*Spi_NotificationType)(void);

typedef enum
{
   SPI_UNINIT = 0u,
   SPI_IDLE,
   SPI_BUSY

}
Spi_StatusType;

typedef enum
{
   SPI_JOB_OK = 0u,
   SPI_JOB_PENDING,
   SPI_JOB_FAILED

}
Spi_JobResultType;

typedef enum
{
   SPI_SEQ_OK = 0u,
   SPI_SEQ_PENDING,
   SPI_SEQ_FAILED,
   SPI_SEQ_CANCELLED

}
Spi_SeqResultType;

typedef uint16_t Spi_DataType;

typedef uint16_t Spi_NumberOfDataType;

typedef uint8_t Spi_ChannelType;

typedef enum
{
   SPI_BITS_16 = 0x0u,
   SPI_BITS_8 = 0x1u

}
Spi_DataWidth;

typedef uint16_t Spi_JobType;

typedef uint8_t Spi_SequenceType;

typedef uint8_t Spi_HWUnitType;

typedef enum
{
   SPI_POLLING_MODE = 0u,
   SPI_INTERRUPT_MODE
}
Spi_AsyncModeType;

typedef struct
{
   Spi_ChannelType Chn_Type;
   Spi_DataWidth Data_Width;
   Spi_NumberOfDataType Buffer_Width;
   Spi_DataType Default_Data;
}
Spi_ChannelDefType;

typedef struct
{
   uint16_t Num_Of_Chns;
   Spi_HWUnitType Hw_Unit;
   uint16_t Spi_BaudRate;
   uint8_t Spi_DRM;
   uint8_t Spi_ClkIdle;
   uint8_t Spi_EdgeTiming;
   Spi_ChannelType * Do_Channel;
   Spi_NotificationType Spi_JobEndNotification;
   uint8_t WaitingTime;
   uint8_t numChipSelect;
}
Spi_JobDefType;

typedef struct
{
   uint16_t Num_Of_Jobs;
   Spi_JobType * Do_Job;
   Spi_NotificationType Spi_SeqEndNotification;
}
Spi_SequenceDefType;

typedef struct
{
   const Spi_ChannelDefType* ChnPtr;
   const Spi_JobDefType* JobPtr;
   const Spi_SequenceDefType* SeqPtr;
   uint8_t Spi_MaxChannels;
   uint8_t Spi_MaxJobs;
   uint8_t Spi_MaxSeq;
}
Spi_ConfigType;

typedef enum
{
   SPI_EB_UNINIT = 0,
   SPI_EB_INIT
}
Spi_EBStatusType;


void Spi_Init(
   const Spi_ConfigType* configPtr);

Std_ReturnType Spi_DeInit(void);

Std_ReturnType Spi_WriteIB(
   Spi_ChannelType channel,
   const Spi_DataType* dataBufferPointer);

Std_ReturnType Spi_ReadIB(
   Spi_ChannelType channel,
   Spi_DataType* dataBufferPointer);

Std_ReturnType Spi_SetupEB(
   Spi_ChannelType channel,
   const Spi_DataType* srcDataBufferPtr,
   Spi_DataType* desDataBufferPtr,
   Spi_NumberOfDataType length);

Spi_StatusType Spi_GetStatus(void);

Spi_JobResultType Spi_GetJobResult(
   Spi_JobType job);

Spi_SeqResultType Spi_GetSequenceResult(
   Spi_SequenceType sequence);

Std_ReturnType Spi_SyncTransmit(
   Spi_SequenceType sequence);

Spi_StatusType Spi_GetHWUnitStatus(
   Spi_HWUnitType hwUnit);

void Spi_Cancel(
   Spi_SequenceType sequence);


extern Spi_SeqResultType Spi_StatusSeq[SPI_MAX_SEQ];

extern Spi_JobResultType Spi_StatusJob[SPI_MAX_JOBS];

extern Spi_DataType* Spi_Chn_Trm_Ptr[SPI_MAX_CHANNELS];

extern uint16_t Spi_BufferWidth[SPI_MAX_CHANNELS];

extern Spi_DataType* Spi_Chn_Rec_Ptr[SPI_MAX_CHANNELS];

extern const Spi_ConfigType Spi_Config;


#else
#error Multiple include of "Spi.h"
#endif 


