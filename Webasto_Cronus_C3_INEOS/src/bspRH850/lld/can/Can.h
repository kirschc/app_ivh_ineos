
#ifndef CAN_H
#define CAN_H



#include "Std_Types.h"
#include "Can_Cfg.h"



#define CAN_SW_MAJOR_VERSION                       1u

#define CAN_SW_MINOR_VERSION                       3u

#define CAN_SW_PATCH_VERSION                       2u

#define CAN_MODULE_ID                              80u

#define CAN_INSTANCE_ID0                           0x00u

#define CAN_VENDOR_ID                              0u


#define CAN_DEFAULT_DATA_BYTE                      0x00u

#define CAN_TMTRM_BIT_STS                          (uint8_t)0x08u

#define CAN_TMTRF_BIT_STS                          (uint8_t)0x06u

#define MAX_TX_BUFFER                              15u

#define CAN_CHANNEL_REGISTER_OFFSET_8BIT           (0x10u)

#define CAN_CHANNEL_REGISTER_OFFSET_32BIT          (0x00000100u/4u)

#define CAN_BYTE_OFFS                              1u

#define CAN_LONG_OFFS                              4u

#define CAN_DLC_SHIFT                              28u

#define CAN_EXTENDED_ID                            31u

#define RXFIFO_EMPTY                               (uint32_t)0x00000001u

#define RXFIFO_RXIF                                (uint32_t)0x00000008u

#define RXFIFO_NEXTMESSAGE                         (uint32_t)0x000000FFu

#define CAN_EXTENDED_FORMAT                        (uint32_t)0x80000000u

#define CAN_ID_EXTENDED_SET                        (uint32_t)0x1FFFFFFFu

#define CAN_ID_STANDARD_SET                        (uint32_t)0x00000FFFu

#define CAN_EXTRACT_DLC                            (uint32_t)0xF0000000u

#define CAN_DB                                     8u

#define CAN_TQ_BIT_TIME                            20u

#define CAN_BAUDRATE_INIT			   			   0x003E0007u



#define CAN_REGISTER_LONG(Register)                *((volatile uint32_t*) (&(Register)))

#define CAN_REGISTER_LONG_OFFSET16BYTES(Register,Offset) *(((volatile uint32_t*) &(Register)) + ((Offset) * 0x00000004u))

#define CAN_REGISTER_LONG_OFFSET4BYTES(Register,Offset) *(((volatile uint32_t*) &(Register)) + (Offset))

#define CAN_REGISTER_BYTE_OFFSET1BYTE(Register,Offset) *(((volatile uint8_t*) &(Register)) + (Offset))

#define CAN_SET_GATEWAY(can0,can1,can2,can3,can4,can5)         \
                     (uint16_t)(((can5) << 15u) | ((can4) << 12u) | ((can3) << 9u) | ((can2) << 6u) | ((can1) << 3u) | (can0))

#define Can_GetVersionInfo(                                 \
              \
   versioninfo)                                             \
   versioninfo.moduleID = CAN_MODULE_ID;                    \
   versioninfo.vendorID = CAN_VENDOR_ID;                    \
   versioninfo.sw_major_version = CAN_SW_MAJOR_VERSION;     \
   versioninfo.sw_minor_version = CAN_SW_MINOR_VERSION;     \
   versioninfo.sw_patch_version = CAN_SW_PATCH_VERSION;



typedef struct
{
   uint8_t messageDataByte[CAN_DB];
   uint8_t messageDLC;
}
Can_PreCopyAccessType;

typedef void (*Can_PretransmitType)(Can_PreCopyAccessType *);

typedef void (*Can_TransmitNotificationType)(uint32_t);

typedef void (*Can_ReceiveNotificationType)(uint8_t, uint8_t, uint32_t, uint8_t, uint8_t);

typedef void (*Can_NotificationType)(void);

typedef enum
{
   CAN_STATUS_NORMAL = 0u,
   CAN_STATUS_SINGLE_SHOT,
   CAN_STATUS_SLEEP,
   CAN_STATUS_STOP,
   CAN_STATUS_RESET,
   CAN_STATUS_HALT,
   CAN_STATUS_COMM,
   CAN_STATUS_RECEIVE_ONLY,
   CAN_STATUS_SELF_TEST,
   CAN_STATUS_INIT,
   CAN_STATUS_UNINIT
}
Can_StatusType;

typedef enum
{
   CAN_BAUDRATE_1000,
   CAN_BAUDRATE_500,
   CAN_BAUDRATE_250,
   CAN_BAUDRATE_125,
   CAN_BAUDRATE_100,
   CAN_BAUDRATE_83_3,
   CAN_BAUDRATE_50,
   CAN_BAUDRATE_33_3,
   CAN_BAUDRATE_MAX
}
Can_Baudrate;

typedef struct
{
   uint8_t regSJW;
   uint8_t regTSEG2;
   uint8_t regTSEG1;
   uint8_t regBRP;
}
Can_BaudrateSettings;

typedef struct
{
   uint32_t messageID;
   uint8_t messageDataByte[CAN_DB];
   uint8_t messageDLC;
   uint8_t extendedID;
   boolean flag;
}
Can_MessageType;

typedef struct
{
   uint32_t messageID;
   const uint8_t extendedID;
   const uint8_t messageDLC;
   Can_TransmitNotificationType confirmation;
   Can_PretransmitType pretransmit;
}
Can_TxMessageBufferConfigType;

typedef struct
{
   uint32_t messageID;
   const uint8_t extendedID;
   uint16_t gatewayConfig;
   Can_ReceiveNotificationType indication;
}
Can_RxMessageBufferConfigType;

typedef struct
{
   const uint8_t mode;
   const Can_Baudrate baudrate;
   const uint8_t numOfTxMessages;
   const uint8_t numOfRxMessages;
   const uint32_t acceptanceMask1;
   const uint32_t acceptanceMask2;
   const uint32_t acceptanceMask3;
   const uint32_t acceptanceMask4;
   const uint8_t taskCycleTime;
   const uint8_t pollingMode;
   const uint8_t intPrioError;
   const uint8_t intPrioWakeup;
   const uint8_t intPrioReceive;
   const uint8_t intPrioTransmit;
   Can_ReceiveNotificationType receiveNotification;
   Can_NotificationType wakeUpNotification;
   Can_NotificationType busOffNotification;
   Can_NotificationType crcErrorNotification;
   Can_NotificationType ackErrorNotification;
   Can_NotificationType txErrorNotification;
   Can_NotificationType rxErrorNotification;
   Can_TxMessageBufferConfigType *txMessageBufferConfig;
   const uint8_t numOfTxBuffers;
   const Can_RxMessageBufferConfigType *rxMessageBufferConfig;
   const uint8_t numOfRxBuffers;
   const uint8_t numOfRxFifoBuffers;
   const uint8_t numOfTxFifoBuffers;
   const uint8_t numOfGateFifoBuffers;
}
Can_CfgType;

typedef struct
{
   const Can_CfgType *source;
   uint16_t numberOfChannels;
}
Can_ConfigType;



void Can_Init(const Can_ConfigType* configPtr);

void Can_DeInit(uint8_t channel);

Std_ReturnType Can_SetBaudrate(uint8_t channel, Can_Baudrate baudrate);


Std_ReturnType Can_GetBaudrate(uint8_t channel, Can_Baudrate* baudrate);

Can_StatusType Can_GetStatus(uint8_t channel);

Std_ReturnType Can_SetStatus(uint8_t channel, Can_StatusType status);

void Can_InterruptDisable(uint8_t channel);

void Can_InterruptEnable(uint8_t channel);

Std_ReturnType Can_AddMsgToFifo(uint8_t channel, uint8_t msgHandle);

Std_ReturnType Can_Transmit(uint8_t channel, uint8_t msgHandle);

void Can_Task(uint8_t channel);

Std_ReturnType Can_GetRxMsgData(uint8_t channel, uint8_t msgHandle, uint8_t *dataPtr);

Std_ReturnType Can_GetDynRxMsgData(uint8_t channel, uint8_t msgHandle, uint8_t *dataPtr, uint32_t id, uint8_t dlc);

Std_ReturnType Can_SetTxMsgData(uint8_t channel, uint8_t msgHandle, const uint8_t *dataPtr);

Std_ReturnType Can_SetDynTxMsgData(uint8_t channel, uint8_t msgHandle, const uint8_t *dataPtr, uint32_t id, uint8_t dlc);

uint8_t Can_GetFreeHwTxBuffer(uint8_t channel);

void Can_ReceiveIsr(uint8_t channel);

void Can_TransmitIsr(uint8_t channel);

void Can_ErrorIsr(uint8_t channel);

Std_ReturnType Can_DisableTransceiver(uint8_t channel);

Std_ReturnType Can_EnableTransceiver(uint8_t channel);

Std_ReturnType Can_SetSleepTransceiver(uint8_t channel);



extern const Can_CfgType Can_Config[CAN_NUMBER_OF_CHANNELS];

extern const Can_ConfigType Can_Cfg;


#else
#error Multiple include of "Can.h"
#endif 



