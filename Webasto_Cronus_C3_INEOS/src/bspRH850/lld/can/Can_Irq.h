
#ifndef CAN_IRQ_H
#define CAN_IRQ_H







void INTRCAN0TRX(void);

void INTRCAN0REC(void);

void INTRCAN0ERR(void);

void INTRCAN1TRX(void);

void INTRCAN1REC(void);

void INTRCAN1ERR(void);

void INTRCAN2TRX(void);

void INTRCAN2REC(void);

void INTRCAN2ERR(void);

void INTRCAN3TRX(void);

void INTRCAN3REC(void);

void INTRCAN3ERR(void);

void INTRCAN4TRX(void);

void INTRCAN4REC(void);

void INTRCAN4ERR(void);

void INTRCAN5TRX(void);

void INTRCAN5REC(void);

void INTRCAN5ERR(void);



#else
#error Multiple include of "Can_Irq.h"
#endif 



