#ifndef STBC_H
#define STBC_H


#include "hal_data_types.h"
#include "Stbc_Cfg.h"

#define STBC_MODULE_ID                       ((uint8_t)100)
#define STBC_INSTANCE_ID_0                   ((uint8_t)0)

#define STBC_SET_INTERRUPTS_FOR_WAKE_UP_ID   0x00u 
#define STBC_SET_STBY_MODE_ID                0x01u 

#define STBC_E_CONFIG                        0x10u  
#define STBC_E_INTERRUPT_NOT_FOUND           0x11u  
#define STBC_E_INTERRUPT_NOT_ACCESSIBLE      0x12u  
#define STBC_E_PROTECTED_WRITE_FAILED        0x13u  

#define IC_REG_MAX                           288u
#define IC_REG_BASE_ADDR                     0xFFFF9000u
#define RFXXX                                0x1000u
#define MKXXX                                0x0080u
#define TBXXX                                0x0040u
#define RESF_RESET_MASK                      0x000007FFu
#define STBC0DISTTRG                         0x2u
#define STBC0STPTRG                          0x1u
#define WRITE_PROTECTION_CODE                0x000000A5u
#define IOHOLD_RELEASE                       0x0u
#define WAIT_DEEP_STOP                       10000u

#define IC_ACCESS_MASK_100PIN                               \
{                                                           \
    0xFFFFFFFFu,                       \
    0xFFF7FFFFu,                       \
    0xFFFFFFFFu,                     \
    0xFFFFFFC0u,                   \
    0xF7FFFFC0u,               \
    0xFFFF000Fu,                   \
    0xF07E005Fu,                 \
    0x00000000u,                  \
    0x03800700u,                  \
}

#define IC_ACCESS_MASK_144PIN                               \
{                                                           \
    0xFFFFFFFFu,                       \
    0xFFF7FFFFu,                       \
    0xFFFFFFFFu,                     \
    0xFFFFFFC0u,                   \
    0xFFFFFFC0u,               \
    0xFFFF000Fu,                   \
    0xFFFEFFFFu,                 \
    0x00FFFFFFu,                  \
    0x00000000u,                  \
}

#define IC_ACCESS_MASK_176PIN                               \
{                                                           \
    0xFFFFFFFFu,                       \
    0xFFF7FFFFu,                       \
    0xFFFFFFFFu,                     \
    0xFFFFFFC0u,                   \
    0xFFFFFFC0u,               \
    0xFFFF000Fu,                   \
    0xFFFEFFFFu,                 \
    0xFFFFFFFFu,                  \
    0xFFFFFFFFu,                  \
}

#define PROTECTED_WRITE(preg,pstatus,reg,value)   do{\
                                                  (preg)=WRITE_PROTECTION_CODE;\
                                                  (reg)=(value);\
                                                  (reg)=~(value);\
                                                  (reg)=(value);\
                                                  }while((pstatus)==1u)

typedef enum
{
    STBY_MODE_HALT                            =0u,
    STBY_MODE_STOP                               ,
    STBY_MODE_DEEPSTOP                           ,
    STBY_MODE_CYCLIC_RUN                         ,
    STBY_MODE_CYCLIC_STOP                        ,

}enum_STBY_MODES;

typedef enum
{
    FACTOR_REG_WUF0                          = 0u,
    FACTOR_REG_WUF_ISO0                          ,
    FACTOR_REG_WUF20                             ,
    FACTOR_REG_MAX

}enum_STBC_FACTOR_REG;

typedef enum
{
    WAKE_UP                                  = 0,
    NON_WAKE_UP

}enum_STBC_WAKE_UP_MODE;

typedef struct
{
    uint8_t factor;
    enum_STBC_FACTOR_REG reg;
    uint8_t reg_idx;
    uint16_t ir_no;
    uint8_t mode;

}stbc_wake_up_cfg_t;


void Stbc_ClearAllWakeUpFactor(void);

void Stbc_SetWakeupFactor(void);

void Stbc_ClearAllInterruptFlags(void);

void Stbc_SetInterruptForWakeUp(void);

uint8_t Stbc_IsInterruptAccessible(uint16_t ir_no);

void Stbc_SetStbyMode(enum_STBY_MODES mode);

uint32_t Stbc_CalcIcRegAddr(uint16_t ir_no);

void Stbc_ReleaseIoHold(void);

uint32_t Stbc_GetWakeUpReason(enum_STBC_FACTOR_REG reg);


#endif


