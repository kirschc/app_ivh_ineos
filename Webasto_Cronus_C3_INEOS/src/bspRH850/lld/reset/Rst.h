#ifndef RST_H
#define RST_H


#include "hal_data_types.h"
#include "Stbc.h"

#define RST_MODULE_ID                       ((uint8_t)102)
#define RST_INSTANCE_ID_0                   ((uint8_t)0)

#define RST_CLEAR_RESET_SOURCE_ID			 0x00u 

#define RST_E_CONFIG                         0x10u  

#define RST_SWRESA_MASK						 0x00000001u
#define RST_CLEAR_ALL_MASK					 0x000007FFu


typedef enum
{
	RST_SWRES   = 0u,
	RST_WDTA0,
	RST_WDTA1,
	RST_CLMA0,
	RST_CLMA1,
	RST_CLMA2,
	RST_LVI,
	RST_CVM,
	RST_POC,
	RST_OCD,
	RST_STB,
	RST_INVALID

}enum_RST_SOURCE;

typedef enum
{
    ECU_SOFTWARE_RESET_FLAG     = (uint32_t)0x0001,
    ECU_WDTA0_RESET_FLAG        = (uint32_t)0x0002,
    ECU_WDTA1_RESET_FLAG        = (uint32_t)0x0004,
    ECU_CLMA0_RESET_FLAG        = (uint32_t)0x0008,
    ECU_CLMA1_RESET_FLAG        = (uint32_t)0x0010,
    ECU_CLMA2_RESET_FLAG        = (uint32_t)0x0020,
    ECU_LVI_RESET_FLAG          = (uint32_t)0x0040,
    ECU_CVM_RESET_FLAG          = (uint32_t)0x0080,
    ECU_EXTERNAL_RESET_FLAG     = (uint32_t)0x0100,
    ECU_POWER_UP_RESET_FLAG     = (uint32_t)0x0200,
    ECU_DEEP_STOP_RESET_FLAG    = (uint32_t)0x0400
} enum_ECU_RESET_FLAG;




void Rst_SetSoftwareReset(void);

void Rst_ClearResetSource(enum_RST_SOURCE  rst_src);

void Rst_ClearAllResetSource(void);

uint32_t Rst_ValidateResetSource(void);

#endif


