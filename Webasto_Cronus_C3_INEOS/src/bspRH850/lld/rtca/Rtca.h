#ifndef _RTCA_H_
#define _RTCA_H_

#include "hal_data_types.h"

#ifdef RTOS_ENABLE
    #include "FreeRTOS.h"
    #include "semphr.h"
#endif

#define RTCA_TIME_FORMAT_24H


#ifdef RTCA_FREQUENCY_SELECTION_MODE
#define RTCA_ATCKI (4000000u - 1u)
#else
#define RTCA_ATCKI (0x7fffu)
#endif


#define RTCA_DAY_OF_WEEK_SU     0u
#define RTCA_DAY_OF_WEEK_MO     1u
#define RTCA_DAY_OF_WEEK_TU     2u
#define RTCA_DAY_OF_WEEK_WE     3u
#define RTCA_DAY_OF_WEEK_TH     4u
#define RTCA_DAY_OF_WEEK_FR     5u
#define RTCA_DAY_OF_WEEK_SA     6u

#define RTCA_DAY_OF_WEEK_CALC   0xFFu   


#define RTCA_ALARM_INT_EN
#ifdef RTCA_ALARM_INT_EN
    #define RTCA_ALARM_INT_ENABLE_INTERRUPT_BIT     0x0080u     
    #define RTCA_ALARM_INT_TABLE_REF_BIT            0x0040u     
    #define RTCA_ALARM_INT_PRIO_BIT                 0x0007u     
#endif



typedef struct
{
  uint8_t second;
  uint8_t minute;
  uint8_t hour;
  uint8_t day;
  uint8_t wday;
  uint8_t month;
  uint8_t year;
} struct_rtc_time_t;


#if defined(RTOS_ENABLE) && defined(configUSE_PREEMPTION)
void rtca_init_set_mutex(const SemaphoreHandle_t ptr_mutex);
#endif

boolean Rtca_Init(void);

boolean Rtca_SetTime( const struct_rtc_time_t * const rtc_time );

boolean Rtca_GetTime( struct_rtc_time_t * const rtc_time);

uint16_t Rtca_GetDaysSince_01_01_2000( const struct_rtc_time_t * const rtc_time );

uint8_t Rtca_TimeCalcDayOfWeek( const struct_rtc_time_t * const rtc_time );

int8_t  Rtca_TimeCompare( const struct_rtc_time_t * const rtc_time1, const struct_rtc_time_t * const rtc_time2, const uint16_t delta_seconds );

boolean Rtca_TimeCheck( const struct_rtc_time_t * const rtc_time );

#ifdef RTCA_ALARM_INT_EN
boolean Rtca_SetAlarmDayOfWeek( const struct_rtc_time_t * const rtc_time, void (*callback)(const uint8_t cbk_code), const uint8_t callback_code );

void INTRTCA0AL( void );

#endif

#endif 


