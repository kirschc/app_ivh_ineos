
#ifndef FLS_H
#define FLS_H


#include "Std_Types.h"
#include "Fls_Cfg.h"


#define FLS_SW_MAJOR_VERSION                       1u

#define FLS_SW_MINOR_VERSION                       1u

#define FLS_SW_PATCH_VERSION                       0u

#define FLS_MODULE_ID                              ((uint8_t)92u)

#define FLS_VENDOR_ID                              ((uint16_t)0u)

#define FLS_INIT_ID                                ((uint8_t)0u)

#define FLS_ERASE_ID                               ((uint8_t)1u)

#define FLS_WRITE_ID                               ((uint8_t)2u)

#define FLS_CANCEL_ID                              ((uint8_t)3u)

#define FLS_GET_STATUS_ID                          ((uint8_t)4u)

#define FLS_GET_JOB_RESULT_ID                      ((uint8_t)5u)

#define FLS_MAIN_FUNCTION_ID                       ((uint8_t)6u)

#define FLS_READ_ID                                ((uint8_t)7u)

#define FLS_COMPARE_ID                             ((uint8_t)8u)

#define FLS_SET_MODE_ID                            ((uint8_t)9u)

#define FLS_GET_VERSION_ID                         ((uint8_t)0x10u)

#define FLS_E_PARAM_CONFIG                         ((uint8_t)0x01u)

#define FLS_E_PARAM_ADDRESS                        ((uint8_t)0x02u)

#define FLS_E_PARAM_LENGTH                         ((uint8_t)0x03u)

#define FLS_E_PARAM_DATA                           ((uint8_t)0x04u)

#define FLS_E_UNINIT                               ((uint8_t)0x05u)

#define FLS_E_BUSY                                 ((uint8_t)0x06u)

#define FLS_E_VERIFY_ERASE_FAILED                  ((uint8_t)0x07u)

#define FLS_E_VERIFY_WRITE_FAILED                  ((uint8_t)0x08u)

#define FLS_E_TIMEOUT                              ((uint8_t)0x09u)
#define FLS_E_PARAM_POINTER                        ((uint8_t)0x0Au)

#define FLS_E_ERASE_FAILED                         ((uint16_t)0xFFFFu)

#define FLS_E_WRITE_FAILED                         ((uint16_t)0xFFFEu)

#define FLS_E_READ_FAILED                          ((uint16_t)0xFFFDu)

#define FLS_E_COMPARE_FAILED                       ((uint16_t)0xFFFCu)

#define FLS_BASE_ADDRESS                           0xFF200000u

#define FLS_TOTAL_SIZE                             0x00008000u

#define FLS_USE_INTERRUPTS                         STD_OFF

#define FLS_BLOCK_SIZE                             0x00000040u

#define FLS_PAGE_SIZE                              0x00000004u

#define FLS_POOL_SIZE                              512u

#define FLS_WORD_SIZE                              4u

#define FLS_MAX_WORD_COUNT                         (FLS_MAX_WRITE_FAST_MODE/FLS_WORD_SIZE)

#define FLS_BLANK_VALUE                            0xFFFFFFFFu

#define FLS_AUTHENTICATION_ID_1                    0xFFFFFFFFu
#define FLS_AUTHENTICATION_ID_2                    0xFFFFFFFFu
#define FLS_AUTHENTICATION_ID_3                    0xFFFFFFFFu
#define FLS_AUTHENTICATION_ID_4                    0xFFFFFFFFu


#define Fls_GetVersionInfo(                                 \
              \
   versioninfo)                                             \
   versioninfo.moduleID = FLS_MODULE_ID;                    \
   versioninfo.vendorID = FLS_VENDOR_ID;                    \
   versioninfo.sw_major_version = FLS_SW_MAJOR_VERSION;     \
   versioninfo.sw_minor_version = FLS_SW_MINOR_VERSION;     \
   versioninfo.sw_patch_version = FLS_SW_PATCH_VERSION;


typedef enum
{
   FLS_JOB_NONE,
   FLS_JOB_ERASE,
   FLS_JOB_WRITE,
   FLS_JOB_READ,
   FLS_JOB_COMPARE,
   FLS_JOB_COMPARE_WITH_BLANK_VALUE
#ifdef FLS_VERIFY_ERASE_API
   #if (FLS_VERIFY_ERASE_API == STD_ON)
   ,FLS_JOB_VERIFY_ERASE
   #endif
#endif
#ifdef FLS_VERIFY_WRITE_API
   #if (FLS_VERIFY_WRITE_API == STD_ON)
   ,FLS_JOB_VERIFY_WRITE
   #endif
#endif
}
Fls_JobType;

typedef enum
{
   FLS_ID_AU32_0,
   FLS_ID_AU32_1,
   FLS_ID_AU32_2,
   FLS_ID_AU32_3
}
Fls_IdAu32Type;

typedef void (*Fls_JobEndNotificationType)(void);
typedef void (*Fls_JobErrorNotificationType)(void);

typedef uint32_t Fls_AddressType;

typedef uint32_t Fls_LengthType;

typedef struct
{
   Fls_JobType jobType;
   Fls_AddressType address;
   Fls_LengthType length;
   uint8_t* destination;
   const uint8_t* source;
   Fls_LengthType offset;
}
Fls_JobConfigType;

typedef struct
{
   const uint16_t callCycle;
   const MemIf_ModeType defaultMode;
   const Fls_JobEndNotificationType jobEndNotification;
   const Fls_JobErrorNotificationType jobErrorNotification;
   const uint32_t maxReadFastMode;
   const uint32_t maxReadNormalMode;
   const uint32_t maxWriteFastMode;
   const uint32_t maxWriteNormalMode;
}
Fls_ConfigType;


void Fls_Init(
   const Fls_ConfigType* configPtr);

Std_ReturnType Fls_Erase(
   Fls_AddressType targetAddress, 
   Fls_LengthType length);

Std_ReturnType Fls_Write(
   Fls_AddressType targetAddress, 
   const uint8_t* sourceAddressPtr, 
   Fls_LengthType length);

#ifdef FLS_CANCEL_API
#if (FLS_CANCEL_API == STD_ON)
void Fls_Cancel(void);
#endif
#endif

MemIf_StatusType Fls_GetStatus(void);

MemIf_JobResultType Fls_GetJobResult(void);

Std_ReturnType Fls_Read(
   Fls_AddressType sourceAddress, 
   uint8_t* targetAddressPtr, 
   Fls_LengthType length);

Std_ReturnType Fls_Compare(
   Fls_AddressType sourceAddress, 
   const uint8_t* targetAddressPtr, 
   Fls_LengthType length);

Std_ReturnType Fls_CompareWithBlankValue(
   Fls_AddressType sourceAddress,
   Fls_LengthType length);
#ifdef FLS_SET_MODE_API
#if (FLS_SET_MODE_API == STD_ON)
void Fls_SetMode(
   MemIf_ModeType mode);
#endif
#endif

void Fls_MainFunction(void);


extern const Fls_ConfigType Fls_config;


#else
    #error Multiple include of "Fls.h"
#endif 


