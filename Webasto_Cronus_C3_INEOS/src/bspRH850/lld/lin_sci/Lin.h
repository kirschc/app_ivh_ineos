#ifndef LIN_H_
#define LIN_H_


#include "hal_data_types.h"
#include "Lin_Cfg.h"


#define LIN_MODULE_ID                       ((uint8_t)254)
#define LIN_INSTANCE_ID_0                   ((uint8_t)0)

#define LIN_INIT_ID                         0x00u 
#define LIN_SET_UNIT_TYPE_ID                0x02u 
#define LIN_GET_UNIT_TYPE_ID                0x03u 
#define LIN_SET_MODE_ID                     0x04u 
#define LIN_GET_MODE_ID                     0x05u 
#define LIN_TX_HEADER_FRAME_CB_ID           0x06u 
#define LIN_RX_FRAME_CB_ID                  0x07u 
#define LIN_ERROR_CB_ID                     0x08u 
#define LIN_TX_RX_START                     0x09u 
#define LIN_SET_BAUDRATE_ID                 0x0Au 
#define LIN_ENABLE_INTERRUPT                0x0bu 

#define LIN_E_CONFIG                        0x10u  
#define LIN_E_UNIT                          0x11u  
#define LIN_E_MODE                          0x12u  
#define LIN_E_TYPE                          0x13u  
#define LIN_E_STATUS                        0x14u  

#define LIN_BIT_SAMPLING_COUNT              16u
#define LIN_BIT_POS_SAMPLING_COUNT           4u
#define LIN_CLOCK_FREQUENCY                 40000000u

#define LMD_LRDNFS_MASK                     0x20u
#define LMD_LIOS_MASK                       0x10u
#define LMD_LCKS_MASK                       0x0Cu

#define LMD_MASK                            0x03u
#define LMD_MASK_MASTER                     0x00u
#define LMD_MASK_UART                       0x01u
#define LMD_MASK_SLAVE_AUTO                 0x02u
#define LMD_MASK_SLAVE_FIX                  0x03u
#define LMD_MASK_LCKS_FA                    0x00u   
#define LMD_MASK_LCKS_FB                    0x04u   
#define LMD_MASK_LCKS_FC                    0x08u   
#define LMD_MASK_LCKS_FD                    0x0Cu   

#define LMD_LCKS_FA_VALUE                   1u
#define LMD_LCKS_FB_VALUE                   2u
#define LMD_LCKS_FC_VALUE                   8u
#define LMD_LCKS_FD_VALUE                   2u

#define LMST_RESET_MASK                     0x01u
#define LMST_MODE_MASK                      0x02u

#define LMST_RESET_VALUE                    0x00u
#define LMST_WAKEUP_VALUE                   0x01u
#define LMST_OPERATION_VALUE                0x03u

#define LCUC_RESET_MASK                     0x01u
#define LCUC_MODE_MASK                      0x02u

#define LCUC_RESET_VALUE                    0x00u
#define LCUC_RESET_CANCEL_VALUE             0x01u
#define LCUC_WAKEUP_VALUE                   0x01u
#define LCUC_OPERATION_VALUE                0x03u

#define LIE_FTCIE_MASK                      0x01u
#define LIE_FRCIE_MASK                      0x02u
#define LIE_ERRIE_MASK                      0x04u
#define LIE_SHIE_MASK                       0x08u

#define LEDE_BERE_MASK                      0x01u
#define LEDE_PBERE_MASK                     0x02u
#define LEDE_FTERE_MASK                     0x04u
#define LEDE_TERE_MASK                      0x04u
#define LEDE_FERE_MASK                      0x08u
#define LEDE_SFERE_MASK                     0x10u
#define LEDE_IPERE_MASK                     0x40u
#define LEDE_LTES_MASK                      0x80u

#define LST_HTRC_MASK                       0x80u
#define LST_D1RC_MASK                       0x40u
#define LST_ERR_MASK                        0x08u
#define LST_FRC_MASK                        0x02u
#define LST_FTC_MASK                        0x01u

#define LDFC_RFDL_MASK                      0x0Fu
#define LDFC_RFT_RCDS_MASK                  0x10u
#define LDFC_CSM_LCS_MASK                   0x20u
#define LDFC_FSM_MASK                       0x40u
#define LDFC_LSS_MASK                       0x80u

#define LTRC_FTS                            0x01u
#define LTRC_RTS                            0x02u
#define LTRC_LNRR                           0x04u

#define LWBR_LWBR0_MASK                     0x01u
#define LWBR_LPRS_MASK                      0x0E

#define LWBR_MASK_LPRS_1                    0x00
#define LWBR_MASK_LPRS_2                    0x02
#define LWBR_MASK_LPRS_4                    0x04
#define LWBR_MASK_LPRS_8                    0x06
#define LWBR_MASK_LPRS_16                   0x08
#define LWBR_MASK_LPRS_32                   0x0A
#define LWBR_MASK_LPRS_64                   0x0C
#define LWBR_MASK_LPRS_128                  0x0E

#define LWBR_LPRS_1_VALUE                   1u
#define LWBR_LPRS_2_VALUE                   2u
#define LWBR_LPRS_4_VALUE                   4u
#define LWBR_LPRS_8_VALUE                   8u
#define LWBR_LPRS_16_VALUE                  16u
#define LWBR_LPRS_32_VALUE                  32
#define LWBR_LPRS_64_VALUE                  64u
#define LWBR_LPRS_128_VALUE                 128u

#define ICR_LINURX_IE_MASK                  0x0040u
#define ICR_LINURX_TE_MASK                  0x0080u


typedef enum
{
    LIN_TYPE_NOT_SET               = 0u,
    LIN_TYPE_MASTER                    ,
    LIN_TYPE_SLAVE_FIX_BAUD            ,
    LIN_TYPE_SLAVE_AUTO_BAUD           ,
    LIN_TYPE_UART

}enum_LIN_UNIT_TYPE;

typedef enum
{
    LIN_DATA_BUFFER_1             = 0u,
    LIN_DATA_BUFFER_2                 ,
    LIN_DATA_BUFFER_3                 ,
    LIN_DATA_BUFFER_4                 ,
    LIN_DATA_BUFFER_5                 ,
    LIN_DATA_BUFFER_6                 ,
    LIN_DATA_BUFFER_7                 ,
    LIN_DATA_BUFFER_8                 ,
    LIN_DATA_BUFFER_MAX

}enum_LIN_DATA_BUFFER;

typedef enum
{
    LIN_ISR_UR0                      = 0u,
    LIN_ISR_UR1                          ,
    LIN_ISR_UR2                          ,
    LIN_ISR_MAX

}enum_LIN_ISR_SOURCE;

typedef enum
{
    LIN_ISR_PRIO_HIGHEST            = 0u,
    LIN_ISR_PRIO_LEVEL_1                ,
    LIN_ISR_PRIO_LEVEL_2                ,
    LIN_ISR_PRIO_LEVEL_3                ,
    LIN_ISR_PRIO_LEVEL_4                ,
    LIN_ISR_PRIO_LEVEL_5                ,
    LIN_ISR_PRIO_LEVEL_6                ,
    LIN_ISR_PRIO_LOWEST

}enum_LIN_ISR_LEVEL;

typedef enum
{
    LIN_NOISE_OFF                    = 0u,
    LIN_NOISE_ON

}enum_LIN_NOISE_FILTER;

typedef enum
{
    LIN_MODE_NOT_SET                 = 0u,
    LIN_MODE_WAKE_UP                     ,
    LIN_MODE_OPERATION                   ,
    LIN_MODE_RESET_CANCEL                ,
    LIN_MODE_RESET

}enum_LIN_MODE;

typedef enum
{
    LIN_TX_RX_START_MODE_NOT_SET                 = 0u,
    LIN_TX_RX_START_MODE_COMMUNICATION_START         ,
    LIN_TX_RX_START_MODE_RESPONSE_START              ,
    LIN_TX_RX_START_MODE_RESPONSE_NO

}enum_LIN_TX_RX_START_MODE;

typedef enum
{
    LIN_FRAME_SEPARATE_MODE_NOT_SET       = 0u,
    LIN_FRAME_SEPARATE_MODE_SET

}enum_LIN_FRAME_SEPARATE_MODE;

typedef enum
{
    LIN_ENABLE_HEADER_TRANSMISSION_INTERRUPT      = 0u,
    LIN_ENABLE_FRAME_TRANSMISSION_INTERRUPT           ,
    LIN_ENABLE_FRAME_RECEPTION_INTERRUPT              ,
    LIN_ENABLE_ERROR_INTERRUPT

}enum_LIN_ENABLE_INTERRUPT;


typedef struct
{
    enum_LIN_UNIT           unit;
    enum_LIN_ISR_LEVEL      isr_level[LIN_ISR_MAX];
    uint8_t                 lbfc_bdt;
    uint8_t                 lbfc_blt;
    uint8_t                 lbfc_lblt;
    uint8_t                 lsc_ibs;
    uint8_t                 lsc_ibhs;
    enum_LIN_NOISE_FILTER   lmd_lrdnfs;
    uint8_t                 wup_wutl;

} lin_cfg_t;


void Lin_TransmitHeaderFrameCallback(enum_LIN_UNIT unit);

void Lin_ReceiveHeaderFrame1stDataCallback(enum_LIN_UNIT unit);

void Lin_ErrorCallback(enum_LIN_UNIT unit);

void Lin_Init(enum_LIN_UNIT unit, enum_LIN_UNIT_TYPE unit_type);

void Lin_SetBaudrate(enum_LIN_UNIT unit, uint32_t baudrate);

void Lin_SetUnitType(enum_LIN_UNIT unit, enum_LIN_UNIT_TYPE type);

enum_LIN_UNIT_TYPE Lin_GetUnitType(enum_LIN_UNIT unit);

void Lin_SetMode(enum_LIN_UNIT unit, enum_LIN_MODE mode);

enum_LIN_MODE Lin_GetMode(enum_LIN_UNIT unit);

uint8_t Lin_IsCommunicationActive(enum_LIN_UNIT unit);

void Lin_SetDataField(enum_LIN_UNIT unit,
                      uint8_t       rfdl,
                      uint8_t       rft_rcds,
                      uint8_t       csm_lcs,
                      uint8_t       fsm,
                      uint8_t       lss);

void Lin_TxId(enum_LIN_UNIT unit, uint8_t id);

uint8_t Lin_RxId(enum_LIN_UNIT unit);


void Lin_TxData(enum_LIN_UNIT unit, uint8_t* ptr_data);

void Lin_RxData(enum_LIN_UNIT unit, uint8_t* ptr_data, uint8_t* len);

uint8_t Lin_RxCrc(enum_LIN_UNIT unit);

void Lin_TxRxStart(enum_LIN_UNIT unit, enum_LIN_TX_RX_START_MODE mode);

enum_LIN_FRAME_SEPARATE_MODE Lin_GetFrameSeparateMode(enum_LIN_UNIT unit);

void Lin_EnableInterrupt(enum_LIN_UNIT unit, enum_LIN_ENABLE_INTERRUPT interrupt);

#endif


