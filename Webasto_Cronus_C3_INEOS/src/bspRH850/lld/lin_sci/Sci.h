
#ifndef SCI_H
#define SCI_H


#include "Std_Types.h"
#include "Sci_Cfg.h"



#define SCI_SW_MAJOR_VERSION                       0u

#define SCI_SW_MINOR_VERSION                       4u

#define SCI_SW_PATCH_VERSION                       2u

#define SCI_MODULE_ID                              ((uint8_t)255)

#define SCI_INSTANCE_ID_0                          12u

#define SCI_VENDOR_ID                              ((uint16_t)0)

#define SCI_SET_RESET_MODE                         0x0u

#define SCI_CANCEL_RESET_MODE                      0x1u

#define SCI_NOT_IN_RESET_MODE                      0x1u

#define SCI_IN_RESET_MODE                          0x0u

#define SCI_BIT_POS_RX_ENABLE                      1u


#define Sci_GetVersionInfo(                                                \
    \
   versioninfo)                                                            \
   versioninfo.moduleID = SCI_MODULE_ID;                                   \
   versioninfo.vendorID = SCI_VENDOR_ID;                                   \
   versioninfo.sw_major_version = SCI_SW_MAJOR_VERSION;                    \
   versioninfo.sw_minor_version = SCI_SW_MINOR_VERSION;                    \
   versioninfo.sw_patch_version = SCI_SW_PATCH_VERSION;


typedef enum
{
   SCI_NOISE_FILTERING_ON,
   SCI_NOISE_FILTERING_OFF
}
Sci_NoiseFilteringType;

typedef enum
{
   SCI_CONFIG_LSB,
   SCI_CONFIG_MSB
}
Sci_ByteOrderType;

typedef enum
{
   SCI_DATA_BIT_LENGTH_08,
   SCI_DATA_BIT_LENGTH_07
}
Sci_DataLengthType;

typedef enum
{
   SCI_STOP_BIT_LENGTH_1,
   SCI_STOP_BIT_LENGTH_2
}
Sci_StopbitLengthType;

typedef enum
{
   SCI_NO_PARITY,
   SCI_EVEN_PARITY,
   SCI_0_PARITY,
   SCI_ODD_PARITY
}
Sci_ParityType;

typedef enum
{
   SCI_SPACE_0,
   SCI_SPACE_1,
   SCI_SPACE_2,
   SCI_SPACE_3,
}
Sci_InterByteSpaceType;

typedef enum
{
   SCI_BIT_ERROR_DETECTION_OFF,
   SCI_BIT_ERROR_DETECTION_ON
}
Sci_BitErrorDetection;

typedef enum
{
   SCI_FRAMING_ERROR_DETECTION_OFF,
   SCI_FRAMING_ERROR_DETECTION_ON
}
Sci_FramingErrorDetection;

typedef enum
{
   SCI_OVERRUN_ERROR_DETECTION_OFF,
   SCI_OVERRUN_ERROR_DETECTION_ON
}
Sci_OverrunErrorDetection;

typedef enum
{
   SCI_STOP_URTE0_OPERATION,
   SCI_ENABLE_URTE0_OPERATION
}
Sci_EnableType;

typedef enum
{
   SCI_STOP_URTE0_TRANSMISSION_OPERATION,
   SCI_ENABLE_URTE0_TRANSMISSION_OPERATION
}
Sci_EnableTransmissionType;

typedef enum
{
   SCI_STOP_URTE0_RECEPTION_OPERATION,
   SCI_ENABLE_URTE0_RECEPTION_OPERATION
}
Sci_EnableReceptionType;

typedef enum
{
   SCI_ENABLE_TX_INTERRUPTS,
   SCI_DISABLE_TX_INTERRUPTS
}
Sci_EnableTxInterruptType;

typedef enum
{
   SCI_ENABLE_RX_INTERRUPTS,
   SCI_DISABLE_RX_INTERRUPTS
}
Sci_EnableRxInterruptType;

typedef enum
{
   SCI_ENABLE_STATUS_INTERRUPTS,
   SCI_DISABLE_STATUS_INTERRUPTS
}
Sci_EnableStatusInterruptType;

typedef void (*Sci_ReceivedNotificationType)(uint8_t channel, uint8_t value);

typedef void (*Sci_TransmitNotificationType)(uint8_t channel);

typedef void (*Sci_ErrorNotificationType)(uint8_t channel);

typedef struct
{
   volatile uint8_t *ICUARTUR0L;
   volatile uint8_t *ICUARTUR1L;
   volatile uint8_t *ICUARTUR2L;
   volatile uint8_t *UARTLWBR;
   volatile uint16_t *UARTLBRP01;
   volatile uint8_t *UARTLMD;
   volatile uint8_t *UARTLBFC;
   volatile uint8_t *UARTLSC;
   volatile uint8_t *UARTLEDE;
   volatile uint8_t *UARTLCUC;
   const volatile uint8_t *UARTLMST;
   volatile uint8_t *UARTLUOER;
   volatile uint16_t *UARTLUTDR;
   const volatile uint16_t *UARTLURDR;
   volatile uint8_t *UARTLST;
   volatile uint8_t *UARTLEST;
   volatile uint8_t *UARTLUOR1;
}
Sci_RegisterSetType;

typedef struct
{
   uint32_t baudRate;
   Sci_NoiseFilteringType noiseFiltering;
   Sci_ByteOrderType byteOrderLSB;
   Sci_DataLengthType dataBitLength;
   Sci_StopbitLengthType stopBitLength;
   Sci_ParityType parity;
   Sci_InterByteSpaceType interByteSpace;
   Sci_FramingErrorDetection framingErrorDetection;
   Sci_OverrunErrorDetection overrunErrorDetection;
   Sci_BitErrorDetection bitErrorDetection;
   Sci_EnableType enable;
   Sci_EnableTransmissionType enableTransmission;
   Sci_EnableReceptionType enableReception;
   Sci_EnableTxInterruptType enableTxInterrupts;
   Sci_EnableRxInterruptType enableRxInterrupts;
   Sci_EnableStatusInterruptType enableStatusInterrupts;
   Sci_ReceivedNotificationType receiveNotification;
   Sci_TransmitNotificationType transmitNotification;
   Sci_ErrorNotificationType errorNotification;
   Sci_RegisterSetType const *registerSet;
}
Sci_ChannelType;

typedef struct
{
   uint8_t numberOfChannels;
   Sci_ChannelType *channels;
}
Sci_ConfigType;

extern       Sci_ChannelType        Sci_channels[];
extern const Sci_ChannelType        WBUS_DRV_SCI_CHL_DEFAULT_CFG;
extern const Sci_RegisterSetType    registerSet[(SCI_NUMBER_OF_HW_CHANNELS)];



void Sci_Init(
    const Sci_ConfigType *configPtr,
    uint8_t channel);

void Sci_PutChar(
   const uint8_t channel,
   const uint8_t txByte);

void Sci_PutString(
   const uint8_t channel,
   const uint8_t* txStringPtr);

uint8_t Sci_GetChar(
   const uint8_t channel);

uint8_t Sci_IsTransmissionReady(
   const uint8_t channel);

uint8_t Sci_NewCharReceived(
   const uint8_t channel);

void Sci_ReceiveClbk(
  const uint8_t channel);

void Sci_TransmitClbk(
   const uint8_t channel);

void Sci_ErrorClbk(
   const uint8_t channel);

void Sci_SetBaudrate(
   const Sci_ConfigType *configPtr,
   const uint8_t channel,
   uint32_t baudrate);

boolean SciCheckBaudrate(
   uint32_t baudrate);


extern const Sci_ConfigType     Sci_Config;



#else
    #error Multiple include of "Sci.h"
#endif 


