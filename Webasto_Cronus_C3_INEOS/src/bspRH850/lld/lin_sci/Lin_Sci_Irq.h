
#ifndef LIN_SCI_IRQ_H
#define LIN_SCI_IRQ_H


#include "Std_Types.h"





void INTRLIN25(void);

void INTRLIN30UR0(void);

void INTRLIN30UR1(void);

void INTRLIN30UR2(void);

void INTRLIN31UR0(void);

void INTRLIN31UR1(void);

void INTRLIN31UR2(void);

void INTRLIN32UR0(void);

void INTRLIN32UR1(void);

void INTRLIN32UR2(void);

void INTRLIN33UR0(void);

void INTRLIN33UR1(void);

void INTRLIN33UR2(void);

void INTRLIN34UR0(void);

void INTRLIN34UR1(void);

void INTRLIN34UR2(void);

void INTRLIN35UR0(void);

void INTRLIN35UR1(void);

void INTRLIN35UR2(void);

void Sci_Irq_ErrorHandling(const uint8_t channel);



#else
    #error Multiple include of "Sci_Irq.h"
#endif 


