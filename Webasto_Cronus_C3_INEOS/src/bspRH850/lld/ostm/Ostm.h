#ifndef OSTM_H
#define OSTM_H

#include "hal_data_types.h"
#include "Ostm_Cfg.h"






void ostm_init(void);

void ostm_start(void);

void ostm_stop(void);

void ostm_set_interval(uint32_t interval_in_hz);

#endif


