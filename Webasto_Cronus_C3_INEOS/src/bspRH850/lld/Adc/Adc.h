
#ifndef ADC_H
#define ADC_H


#include "Std_Types.h"
#include "Adc_Cfg.h"


#define ADC_SW_MAJOR_VERSION                    1u

#define ADC_SW_MINOR_VERSION                    0u

#define ADC_SW_PATCH_VERSION                    0u

#define ADC_MODULE_ID                           ((uint8_t)123u)

#define ADC_INSTANCE_ID0                        ((uint8_t)0u)

#define ADC_VENDOR_ID                           ((uint16_t)0u)

#define ADC_NUMBER_OF_HW_UNITS                  2u

#define ADC_BASE_ADDRESS_OF_ADCA0               0xFF81D000u

#define ADC_BASE_ADDRESS_OF_ADCA1               0xFF81E000u

#define ADC_SUPPORTED_HW_GROUPS                 3u

#define ADC_RESOLUTION_12BIT                    0x00u

#define ADC_RESOLUTION_10BIT                    0x01u

#define ADC_ADDR_BYTE_OFFS                      0x40u

#define ADC_GROUP_INT_PRIO                      0x03

#define ADC_REGISTER_LONG_04(Register,Channel)  *((volatile uint32_t*)   &(Register) + ((Channel) * 0x00000001u))

#define Adc_GetVersionInfo(                                 \
              \
   versioninfo)                                             \
   versioninfo.moduleID = ADC_MODULE_ID;                    \
   versioninfo.vendorID = ADC_VENDOR_ID;                    \
   versioninfo.sw_major_version = ADC_SW_MAJOR_VERSION;     \
   versioninfo.sw_minor_version = ADC_SW_MINOR_VERSION;     \
   versioninfo.sw_patch_version = ADC_SW_PATCH_VERSION;



typedef uint8_t Adc_ChannelType;

typedef uint8_t Adc_GroupType;

typedef uint16_t Adc_ValueGroupType;

typedef uint8_t Adc_ClockSourceType;

typedef uint8_t Adc_PrescaleType;

typedef uint8_t Adc_ConversionTimeType;

typedef uint8_t Adc_SamplingTimeType;

typedef int8_t Adc_VoltageSourceType;

typedef uint8_t Adc_ResolutionType;

typedef enum
{
   ADC_IDLE = 0u,
   ADC_BUSY,
   ADC_COMPLETED,
   ADC_STREAM_COMPLETED
}
Adc_StatusType;

typedef enum
{
   ADC_TRIGGER_SRC_SW = 0u,
   ADC_TRIGGER_SRC_HW
}
Adc_TriggerSourceType;

typedef enum
{
   ADC_TRIGGER_DEFAULT = 0x0000u,
   ADC_TRIGGER_000 = 0x0001u,
   ADC_TRIGGER_001 = 0x0002u,
   ADC_TRIGGER_002 = 0x0004u,
   ADC_TRIGGER_003 = 0x0008u,
   ADC_TRIGGER_004 = 0x0010u,
   ADC_TRIGGER_005 = 0x0020u,
   ADC_TRIGGER_006 = 0x0040u,
   ADC_TRIGGER_007 = 0x0080u,
   ADC_TRIGGER_008 = 0x0100u,
   ADC_TRIGGER_009 = 0x0200u,
   ADC_TRIGGER_010 = 0x0400u,
   ADC_TRIGGER_011 = 0x0800u,
   ADC_TRIGGER_012 = 0x1000u,
   ADC_TRIGGER_013 = 0x2000u,
   ADC_TRIGGER_014 = 0x4000u,
   ADC_TRIGGER_015 = 0x8000u,
}
Adc_HWTriggerSourceType;

typedef enum
{
   ADC_CONV_MODE_ONESHOT = 0u,
   ADC_CONV_MODE_CONTINUOUS
}
Adc_GroupConvModeType;

typedef uint8_t Adc_GroupPriorityType;

typedef enum
{
   ADC_HW_UNIT_0 = 0u,
   ADC_HW_UNIT_1
}
Adc_HwUnitType;

typedef enum
{
   ADC_HW_CG0_ID = 0u,
   ADC_HW_CG1_ID,
   ADC_HW_CG2_ID,
   ADC_HW_NONE_ID
}
AdcHwCgIdType;

typedef void (*Adc_NotificationType)(void);

typedef uint16_t Adc_StreamNumSampleType;

typedef struct
{
   Adc_HwUnitType HW_Unit;
   uint8_t startChannel;
   uint8_t endChannel;
   Adc_NotificationType notification;
   Adc_TriggerSourceType triggerSource;
   Adc_HWTriggerSourceType HWTriggerSource;
   Adc_GroupConvModeType groupConvMode;
   Adc_ResolutionType resolution;
   Adc_ValueGroupType* groupValue;

}
Adc_GroupDefType;

typedef struct
{
	AdcPinTypes channel;			
	Adc_GroupType group;			
	uint16_t group_idx;				
	uint16_t resolution;			
	uint16_t adc_max;				
	uint16_t filter_max;			

} Adc_ChannelConfigType;

typedef enum
{
   ADC_STREAM_BUFFER_LINEAR = 0u,
   ADC_STREAM_BUFFER_CIRCULAR
}
Adc_StreamBufferModeType;

typedef enum
{
   ADC_ACCESS_MODE_SINGLE = 0u,
   ADC_ACCESS_MODE_STREAMING
}
Adc_GroupAccessModeType;

typedef enum
{
   ADC_HW_TRIG_RISING_EDGE = 0u,
   ADC_HW_TRIG_FALLING_EDGE,
   ADC_HW_TRIG_BOTH_EDGES
}
Adc_HwTriggerSignalType;

typedef uint8_t Adc_HwTriggerTimerType;

typedef enum
{
   ADC_PRIORITY_NONE = 0u,
   ADC_PRIORITY_HW,
   ADC_PRIORITY_HW_SW
}
Adc_PriorityImplementationType;

typedef enum
{
   ADC_GROUP_REPL_ABORT_RESTART = 0u,
   ADC_GROUP_REPL_SUSPEND_RESUME
}
Adc_GroupReplacementType;

typedef struct
{
   uint32_t regConf_ADCAnADCR;
   uint32_t regConf_ADCAnSMPCR;
   uint32_t regConf_ADCAnULLMTBR0;
   uint32_t regConf_ADCAnULLMTBR1;
   uint32_t regConf_ADCAnULLMTBR2;
   uint32_t regConf_ADCAnSGCR1;
   uint32_t regConf_ADCAnSGCR2;
   uint32_t regConf_ADCAnSGCR3;
   uint32_t regConf_ADCAnSGVCSP1;
   uint32_t regConf_ADCAnSGVCSP2;
   uint32_t regConf_ADCAnSGVCSP3;
   uint32_t regConf_ADCAnSGVCEP1;
   uint32_t regConf_ADCAnSGVCEP2;
   uint32_t regConf_ADCAnSGVCEP3;
   uint32_t regConf_ADCAnSGSTRCR1;
   uint32_t regConf_ADCAnSGSTRCR2;
   uint32_t regConf_ADCAnSGSTRCR3;
   uint8_t configuredGroups;
   const Adc_GroupDefType *groupConfig;
}
Adc_ConfigType;


void Adc_Init(
   const Adc_ConfigType *configPtr);

Std_ReturnType Adc_SetupResultBuffer(
   Adc_GroupType group,
   Adc_ValueGroupType* dataBufferPtr);

void Adc_DeInit(void);

void Adc_StartGroupConversion(
   Adc_GroupType group);

void Adc_StopGroupConversion(
   Adc_GroupType group);

Std_ReturnType Adc_ReadGroup(
   Adc_GroupType group,
   Adc_ValueGroupType* dataBufferPtr);

void Adc_EnableHardwareTrigger(
   Adc_GroupType group);

void Adc_DisableHardwareTrigger(
   Adc_GroupType group);

void Adc_EnableGroupNotification(
   Adc_GroupType group);

void Adc_DisableGroupNotification(
   Adc_GroupType group);

Adc_StatusType Adc_GetGroupStatus(
   Adc_GroupType group);

Adc_StreamNumSampleType Adc_GetStreamLastPointer(
   Adc_GroupType group,
   Adc_ValueGroupType** ptrToSamplePtr);

void Adc_Notification_Group(
   Adc_DefinedGroupsType group);

void Adc_Wait100us(void);

Adc_ValueGroupType* Adc_getGroupValues
	(Adc_GroupType group);


extern const Adc_ConfigType Adc_Config[ADC_NUMBER_OF_HW_UNITS];


#else
#error Multiple include of "Adc.h"
#endif 


