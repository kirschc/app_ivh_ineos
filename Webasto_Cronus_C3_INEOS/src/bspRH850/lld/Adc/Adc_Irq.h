
#ifndef ADC_IRQ_H
#define ADC_IRQ_H


void INTADCA0I0(void);

void INTADCA0I1(void);

void INTADCA0I2(void);

void INTADCA0ERR(void);

void INTADCA1I0(void);

void INTADCA1I1(void);

void INTADCA1I2(void);

void INTADCA1ERR(void);



#else
#error Multiple include of "Adc_Irq.h"
#endif 


