#ifndef __R7F701057IODEFINE_HEADER__
#define __R7F701057IODEFINE_HEADER__

#ifndef _IODEF_AUTOSAR_TYPEDEF_
#define _IODEF_AUTOSAR_TYPEDEF_
#include <io_macros_v2.h>
#endif

#ifndef _GHS_PRAGMA_IO_TYPEDEF_
#define _GHS_PRAGMA_IO_TYPEDEF_
#define PRAGMA(x) _Pragma(#x)
#define __READ volatile const
#define __WRITE volatile
#define __READ_WRITE volatile
#define __IOREG(reg, addr, attrib, type) PRAGMA(ghs io reg addr) \
extern attrib type reg 
#define __IOREGARRAY(reg, array, addr, attrib, type) PRAGMA(ghs io reg addr) \
extern attrib type reg[array] 
#endif

typedef struct
{
    uint8_t bit00:1;
    uint8_t bit01:1;
    uint8_t bit02:1;
    uint8_t bit03:1;
    uint8_t bit04:1;
    uint8_t bit05:1;
    uint8_t bit06:1;
    uint8_t bit07:1;
} __bitf_T;

#define  L 0
#define  H 1
#define LL 0
#define LH 1
#define HL 2
#define HH 3

typedef struct 
{                                                          
    uint8_t  AAS0:1;                                         
    uint8_t  AAS1:1;                                         
    uint8_t  AAS2:1;                                         
    uint8_t  GCA:1;                                          
    uint8_t  :1;                                             
    uint8_t  DID:1;                                          
    uint8_t  :2;                                             
    uint8_t  :8;                                             
    uint8_t  :8;                                             
    uint8_t  :8;                                             
} __type0;
typedef struct 
{                                                          
    uint8_t  TMOF:1;                                         
    uint8_t  AL:1;                                           
    uint8_t  START:1;                                        
    uint8_t  STOP:1;                                         
    uint8_t  NACKF:1;                                        
    uint8_t  RDRF:1;                                         
    uint8_t  TEND:1;                                         
    const uint8_t  TDRE:1;                                   
    uint8_t  :8;                                             
    uint8_t  :8;                                             
    uint8_t  :8;                                             
} __type1;
typedef struct 
{                                                          
    uint8_t  MBS:1;                                          
    uint8_t  JOBE:1;                                         
    uint8_t  :3;                                             
    uint8_t  RXE:1;                                          
    uint8_t  TXE:1;                                          
    uint8_t  PWR:1;                                          
} __type2;
typedef struct 
{                                                          
    uint8_t  :7;                                             
    uint8_t  SVSDIS:1;                                       
} __type3;
typedef struct 
{                                                          
    uint8_t  :5;                                             
    uint8_t  RXE:1;                                          
    uint8_t  TXE:1;                                          
    uint8_t  PWR:1;                                          
} __type4;
typedef struct 
{                                                          
    uint8_t  SCE:1;                                          
    uint8_t  :7;                                             
} __type5;
typedef struct 
{                                                          
    uint8_t  :4;                                             
    uint8_t  SLSB:1;                                         
    uint8_t  AMPM:1;                                         
    const uint8_t  CEST:1;                                   
    uint8_t  CE:1;                                           
} __type6;
typedef struct 
{                                                          
    uint8_t  CT0:1;                                          
    uint8_t  CT1:1;                                          
    uint8_t  CT2:1;                                          
    uint8_t  EN1S:1;                                         
    uint8_t  ENALM:1;                                        
    uint8_t  EN1HZ:1;                                        
    uint8_t  :2;                                             
} __type7;
typedef struct 
{                                                          
    uint8_t  WAIT:1;                                         
    const uint8_t  WST:1;                                    
    uint8_t  RSUB:1;                                         
    const uint8_t  RSST:1;                                   
    const uint8_t  WSST:1;                                   
    const uint8_t  WUST:1;                                   
    uint8_t  :2;                                             
} __type8;
typedef struct 
{                                                          
    uint8_t  KRM0:1;                                         
    uint8_t  KRM1:1;                                         
    uint8_t  KRM2:1;                                         
    uint8_t  KRM3:1;                                         
    uint8_t  KRM4:1;                                         
    uint8_t  KRM5:1;                                         
    uint8_t  KRM6:1;                                         
    uint8_t  KRM7:1;                                         
} __type9;
typedef union
{                                                          
    uint32_t uint32_t;                                         
    uint16_t uint16_t[2];                                      
    uint8_t  uint8_t[4];                                       
} __type10;
typedef union
{                                                          
    uint32_t uint32_t;                                         
    uint16_t uint16_t[2];                                      
    uint8_t  uint8_t[4];                                       
    __type0;                                               
} __type11;
typedef union
{                                                          
    uint32_t uint32_t;                                         
    uint16_t uint16_t[2];                                      
    uint8_t  uint8_t[4];                                       
    __type1;                                               
} __type12;
typedef union
{                                                          
    uint16_t uint16_t;                                         
    uint8_t  uint8_t[2];                                       
    struct 
    {
        union
        {                                                  
            uint8_t  uint8_t;                                  
        } LBRP0;
        union
        {                                                  
            uint8_t  uint8_t;                                  
        } LBRP1;
    };
} __type13;
typedef union
{                                                          
    uint16_t uint16_t;                                         
    uint8_t  uint8_t[2];                                       
} __type14;
typedef union
{                                                          
    uint32_t uint32_t;                                         
    uint16_t uint16_t[2];                                      
} __type15;
typedef union
{                                                          
    uint8_t  uint8_t;                                          
    __type2;                                               
} __type16;
typedef union
{                                                          
    uint8_t  uint8_t;                                          
    __type3;                                               
} __type17;
typedef union
{                                                          
    uint8_t  uint8_t;                                          
    __type4;                                               
} __type18;
typedef union
{                                                          
    uint8_t  uint8_t;                                          
    __type5;                                               
} __type19;
typedef union
{                                                          
    uint8_t  uint8_t;                                          
    __type6;                                               
} __type20;
typedef union
{                                                          
    uint8_t  uint8_t;                                          
    __type7;                                               
} __type21;
typedef union
{                                                          
    uint8_t  uint8_t;                                          
    __type8;                                               
} __type22;
typedef union
{                                                          
    uint8_t  uint8_t;                                          
    __type9;                                               
} __type23;

typedef struct 
{                                                          
    uint32_t ID0;                                            
    uint32_t ID1;                                            
    uint32_t ID2;                                            
    uint32_t ID3;                                            
    const uint32_t IDST;                                     
} __type24;
typedef struct 
{                                                          
    const uint8_t  FPMON;                                    
    uint8_t  dummy0[15];                                     
    uint8_t  FASTAT;                                         
    uint8_t  dummy1[3];                                      
    uint8_t  FAEINT;                                         
    uint8_t  dummy2[11];                                     
    uint16_t FAREASELC;                                      
    uint8_t  dummy3[14];                                     
    uint32_t FSADDR;                                         
    uint32_t FEADDR;                                         
    uint8_t  dummy4[28];                                     
    uint16_t FCURAME;                                        
    uint8_t  dummy5[42];                                     
    const uint32_t FSTATR;                                   
    uint16_t FENTRYR;                                        
    uint8_t  dummy6[2];                                      
    uint16_t FPROTR;                                         
    uint8_t  dummy7[2];                                      
    uint16_t FSUINITR;                                       
    uint8_t  dummy8[2];                                      
    const uint8_t  FLKSTAT;                                  
    uint8_t  dummy9[3];                                      
    const uint32_t FRFSTEADR;                                
    const uint8_t  FRTSTAT;                                  
    uint8_t  dummy10[7];                                     
    const uint16_t FCMDR;                                    
    uint8_t  dummy11[30];                                    
    const uint16_t FPESTAT;                                  
    uint8_t  dummy12[14];                                    
    uint8_t  FBCCNT;                                         
    uint8_t  dummy13[3];                                     
    const uint8_t  FBCSTAT;                                  
    uint8_t  dummy14[3];                                     
    const uint32_t FPSADDR;                                  
    uint8_t  dummy15[4];                                     
    uint16_t FCPSR;                                          
    uint8_t  dummy16[2];                                     
    uint16_t FPCKAR;                                         
    uint8_t  dummy17[26];                                    
    const uint16_t FECCEMON;                                 
    uint8_t  dummy18[2];                                     
    uint16_t FECCTMD;                                        
    uint8_t  dummy19[2];                                     
    uint16_t FDMYECC;                                        
} __type25;
typedef struct 
{                                                          
    uint8_t  BWCBUFEN;                                       
} __type26;
typedef struct 
{                                                          
    uint8_t  FCUFAREA;                                       
} __type27;
typedef struct 
{                                                          
    __type10 CR1;                                          
    __type10 CR2;                                          
    __type10 MR1;                                          
    __type10 MR2;                                          
    __type10 MR3;                                          
    __type10 FER;                                          
    __type10 SER;                                          
    __type10 IER;                                          
    __type11 SR1;                                          
    __type12 SR2;                                          
    __type10 SAR0;                                         
    __type10 SAR1;                                         
    __type10 SAR2;                                         
    __type10 BRL;                                          
    __type10 BRH;                                          
    __type10 DRT;                                          
    const __type10 DRR;                                    
} __type28;
typedef struct 
{                                                          
    uint32_t PRDNAME1;                                       
    uint32_t PRDNAME2;                                       
    uint32_t PRDNAME3;                                       
} __type29;
typedef struct 
{                                                          
    uint8_t  dummy20[1];                                     
    uint8_t  GLWBR;                                          
    uint8_t  GLBRP0;                                         
    uint8_t  GLBRP1;                                         
    uint8_t  GLSTC;                                          
} __type30;
typedef struct 
{                                                          
    uint8_t  L0MD;                                           
    uint8_t  L0BFC;                                          
    uint8_t  L0SC;                                           
    uint8_t  L0WUP;                                          
    uint8_t  L0IE;                                           
    uint8_t  L0EDE;                                          
    uint8_t  L0CUC;                                          
    uint8_t  dummy21[1];                                     
    uint8_t  L0TRC;                                          
    const uint8_t  L0MST;                                    
    uint8_t  L0ST;                                           
    uint8_t  L0EST;                                          
    uint8_t  L0DFC;                                          
    uint8_t  L0IDB;                                          
    uint8_t  L0CBR;                                          
    uint8_t  dummy22[1];                                     
    uint8_t  L0DBR1;                                         
    uint8_t  L0DBR2;                                         
    uint8_t  L0DBR3;                                         
    uint8_t  L0DBR4;                                         
    uint8_t  L0DBR5;                                         
    uint8_t  L0DBR6;                                         
    uint8_t  L0DBR7;                                         
    uint8_t  L0DBR8;                                         
} __type31;
typedef struct 
{                                                          
    uint8_t  L1MD;                                           
    uint8_t  L1BFC;                                          
    uint8_t  L1SC;                                           
    uint8_t  L1WUP;                                          
    uint8_t  L1IE;                                           
    uint8_t  L1EDE;                                          
    uint8_t  L1CUC;                                          
    uint8_t  dummy23[1];                                     
    uint8_t  L1TRC;                                          
    const uint8_t  L1MST;                                    
    uint8_t  L1ST;                                           
    uint8_t  L1EST;                                          
    uint8_t  L1DFC;                                          
    uint8_t  L1IDB;                                          
    uint8_t  L1CBR;                                          
    uint8_t  dummy24[1];                                     
    uint8_t  L1DBR1;                                         
    uint8_t  L1DBR2;                                         
    uint8_t  L1DBR3;                                         
    uint8_t  L1DBR4;                                         
    uint8_t  L1DBR5;                                         
    uint8_t  L1DBR6;                                         
    uint8_t  L1DBR7;                                         
    uint8_t  L1DBR8;                                         
} __type32;
typedef struct 
{                                                          
    uint8_t  L2MD;                                           
    uint8_t  L2BFC;                                          
    uint8_t  L2SC;                                           
    uint8_t  L2WUP;                                          
    uint8_t  L2IE;                                           
    uint8_t  L2EDE;                                          
    uint8_t  L2CUC;                                          
    uint8_t  dummy25[1];                                     
    uint8_t  L2TRC;                                          
    const uint8_t  L2MST;                                    
    uint8_t  L2ST;                                           
    uint8_t  L2EST;                                          
    uint8_t  L2DFC;                                          
    uint8_t  L2IDB;                                          
    uint8_t  L2CBR;                                          
    uint8_t  dummy26[1];                                     
    uint8_t  L2DBR1;                                         
    uint8_t  L2DBR2;                                         
    uint8_t  L2DBR3;                                         
    uint8_t  L2DBR4;                                         
    uint8_t  L2DBR5;                                         
    uint8_t  L2DBR6;                                         
    uint8_t  L2DBR7;                                         
    uint8_t  L2DBR8;                                         
} __type33;
typedef struct 
{                                                          
    uint8_t  L3MD;                                           
    uint8_t  L3BFC;                                          
    uint8_t  L3SC;                                           
    uint8_t  L3WUP;                                          
    uint8_t  L3IE;                                           
    uint8_t  L3EDE;                                          
    uint8_t  L3CUC;                                          
    uint8_t  dummy27[1];                                     
    uint8_t  L3TRC;                                          
    const uint8_t  L3MST;                                    
    uint8_t  L3ST;                                           
    uint8_t  L3EST;                                          
    uint8_t  L3DFC;                                          
    uint8_t  L3IDB;                                          
    uint8_t  L3CBR;                                          
    uint8_t  dummy28[1];                                     
    uint8_t  L3DBR1;                                         
    uint8_t  L3DBR2;                                         
    uint8_t  L3DBR3;                                         
    uint8_t  L3DBR4;                                         
    uint8_t  L3DBR5;                                         
    uint8_t  L3DBR6;                                         
    uint8_t  L3DBR7;                                         
    uint8_t  L3DBR8;                                         
} __type34;
typedef struct 
{                                                          
    uint8_t  dummy29[1];                                     
    uint8_t  LWBR;                                           
    __type13 LBRP01;                                       
    uint8_t  LSTC;                                           
    uint8_t  dummy30[3];                                     
    uint8_t  LMD;                                            
    uint8_t  LBFC;                                           
    uint8_t  LSC;                                            
    uint8_t  LWUP;                                           
    uint8_t  LIE;                                            
    uint8_t  LEDE;                                           
    uint8_t  LCUC;                                           
    uint8_t  dummy31[1];                                     
    uint8_t  LTRC;                                           
    const uint8_t  LMST;                                     
    uint8_t  LST;                                            
    uint8_t  LEST;                                           
    uint8_t  LDFC;                                           
    uint8_t  LIDB;                                           
    uint8_t  LCBR;                                           
    uint8_t  LUDB0;                                          
    uint8_t  LDBR1;                                          
    uint8_t  LDBR2;                                          
    uint8_t  LDBR3;                                          
    uint8_t  LDBR4;                                          
    uint8_t  LDBR5;                                          
    uint8_t  LDBR6;                                          
    uint8_t  LDBR7;                                          
    uint8_t  LDBR8;                                          
    uint8_t  LUOER;                                          
    uint8_t  LUOR1;                                          
    uint8_t  dummy32[2];                                     
    __type14 LUTDR;                                        
    const __type14 LURDR;                                  
    __type14 LUWTDR;                                       
    uint8_t  dummy33[22];                                    
} __type35;
typedef struct 
{                                                          
    __type10 C0CFG;                                        
    __type10 C0CTR;                                        
    const __type10 C0STS;                                  
    __type10 C0ERFL;                                       
    __type10 C1CFG;                                        
    __type10 C1CTR;                                        
    const __type10 C1STS;                                  
    __type10 C1ERFL;                                       
    __type10 C2CFG;                                        
    __type10 C2CTR;                                        
    const __type10 C2STS;                                  
    __type10 C2ERFL;                                       
    __type10 C3CFG;                                        
    __type10 C3CTR;                                        
    const __type10 C3STS;                                  
    __type10 C3ERFL;                                       
    __type10 C4CFG;                                        
    __type10 C4CTR;                                        
    const __type10 C4STS;                                  
    __type10 C4ERFL;                                       
    __type10 C5CFG;                                        
    __type10 C5CTR;                                        
    const __type10 C5STS;                                  
    __type10 C5ERFL;                                       
    uint8_t  dummy34[36];                                    
    __type10 GCFG;                                         
    __type10 GCTR;                                         
    const __type10 GSTS;                                   
    __type10 GERFL;                                        
    const __type15 GTSC;                                   
    __type10 GAFLECTR;                                     
    __type10 GAFLCFG0;                                     
    __type10 GAFLCFG1;                                     
    __type10 RMNB;                                         
    __type10 RMND0;                                        
    __type10 RMND1;                                        
    __type10 RMND2;                                        
    uint8_t  dummy35[4];                                     
    __type10 RFCC0;                                        
    __type10 RFCC1;                                        
    __type10 RFCC2;                                        
    __type10 RFCC3;                                        
    __type10 RFCC4;                                        
    __type10 RFCC5;                                        
    __type10 RFCC6;                                        
    __type10 RFCC7;                                        
    __type10 RFSTS0;                                       
    __type10 RFSTS1;                                       
    __type10 RFSTS2;                                       
    __type10 RFSTS3;                                       
    __type10 RFSTS4;                                       
    __type10 RFSTS5;                                       
    __type10 RFSTS6;                                       
    __type10 RFSTS7;                                       
    __type10 RFPCTR0;                                      
    __type10 RFPCTR1;                                      
    __type10 RFPCTR2;                                      
    __type10 RFPCTR3;                                      
    __type10 RFPCTR4;                                      
    __type10 RFPCTR5;                                      
    __type10 RFPCTR6;                                      
    __type10 RFPCTR7;                                      
    __type10 CFCC0;                                        
    __type10 CFCC1;                                        
    __type10 CFCC2;                                        
    __type10 CFCC3;                                        
    __type10 CFCC4;                                        
    __type10 CFCC5;                                        
    __type10 CFCC6;                                        
    __type10 CFCC7;                                        
    __type10 CFCC8;                                        
    __type10 CFCC9;                                        
    __type10 CFCC10;                                       
    __type10 CFCC11;                                       
    __type10 CFCC12;                                       
    __type10 CFCC13;                                       
    __type10 CFCC14;                                       
    __type10 CFCC15;                                       
    __type10 CFCC16;                                       
    __type10 CFCC17;                                       
    uint8_t  dummy36[24];                                    
    __type10 CFSTS0;                                       
    __type10 CFSTS1;                                       
    __type10 CFSTS2;                                       
    __type10 CFSTS3;                                       
    __type10 CFSTS4;                                       
    __type10 CFSTS5;                                       
    __type10 CFSTS6;                                       
    __type10 CFSTS7;                                       
    __type10 CFSTS8;                                       
    __type10 CFSTS9;                                       
    __type10 CFSTS10;                                      
    __type10 CFSTS11;                                      
    __type10 CFSTS12;                                      
    __type10 CFSTS13;                                      
    __type10 CFSTS14;                                      
    __type10 CFSTS15;                                      
    __type10 CFSTS16;                                      
    __type10 CFSTS17;                                      
    uint8_t  dummy37[24];                                    
    __type10 CFPCTR0;                                      
    __type10 CFPCTR1;                                      
    __type10 CFPCTR2;                                      
    __type10 CFPCTR3;                                      
    __type10 CFPCTR4;                                      
    __type10 CFPCTR5;                                      
    __type10 CFPCTR6;                                      
    __type10 CFPCTR7;                                      
    __type10 CFPCTR8;                                      
    __type10 CFPCTR9;                                      
    __type10 CFPCTR10;                                     
    __type10 CFPCTR11;                                     
    __type10 CFPCTR12;                                     
    __type10 CFPCTR13;                                     
    __type10 CFPCTR14;                                     
    __type10 CFPCTR15;                                     
    __type10 CFPCTR16;                                     
    __type10 CFPCTR17;                                     
    uint8_t  dummy38[24];                                    
    const __type10 FESTS;                                  
    const __type10 FFSTS;                                  
    const __type10 FMSTS;                                  
    const __type10 RFISTS;                                 
    const __type10 CFRISTS;                                
    const __type10 CFTISTS;                                
    uint8_t  TMC0;                                           
    uint8_t  TMC1;                                           
    uint8_t  TMC2;                                           
    uint8_t  TMC3;                                           
    uint8_t  TMC4;                                           
    uint8_t  TMC5;                                           
    uint8_t  TMC6;                                           
    uint8_t  TMC7;                                           
    uint8_t  TMC8;                                           
    uint8_t  TMC9;                                           
    uint8_t  TMC10;                                          
    uint8_t  TMC11;                                          
    uint8_t  TMC12;                                          
    uint8_t  TMC13;                                          
    uint8_t  TMC14;                                          
    uint8_t  TMC15;                                          
    uint8_t  TMC16;                                          
    uint8_t  TMC17;                                          
    uint8_t  TMC18;                                          
    uint8_t  TMC19;                                          
    uint8_t  TMC20;                                          
    uint8_t  TMC21;                                          
    uint8_t  TMC22;                                          
    uint8_t  TMC23;                                          
    uint8_t  TMC24;                                          
    uint8_t  TMC25;                                          
    uint8_t  TMC26;                                          
    uint8_t  TMC27;                                          
    uint8_t  TMC28;                                          
    uint8_t  TMC29;                                          
    uint8_t  TMC30;                                          
    uint8_t  TMC31;                                          
    uint8_t  TMC32;                                          
    uint8_t  TMC33;                                          
    uint8_t  TMC34;                                          
    uint8_t  TMC35;                                          
    uint8_t  TMC36;                                          
    uint8_t  TMC37;                                          
    uint8_t  TMC38;                                          
    uint8_t  TMC39;                                          
    uint8_t  TMC40;                                          
    uint8_t  TMC41;                                          
    uint8_t  TMC42;                                          
    uint8_t  TMC43;                                          
    uint8_t  TMC44;                                          
    uint8_t  TMC45;                                          
    uint8_t  TMC46;                                          
    uint8_t  TMC47;                                          
    uint8_t  TMC48;                                          
    uint8_t  TMC49;                                          
    uint8_t  TMC50;                                          
    uint8_t  TMC51;                                          
    uint8_t  TMC52;                                          
    uint8_t  TMC53;                                          
    uint8_t  TMC54;                                          
    uint8_t  TMC55;                                          
    uint8_t  TMC56;                                          
    uint8_t  TMC57;                                          
    uint8_t  TMC58;                                          
    uint8_t  TMC59;                                          
    uint8_t  TMC60;                                          
    uint8_t  TMC61;                                          
    uint8_t  TMC62;                                          
    uint8_t  TMC63;                                          
    uint8_t  TMC64;                                          
    uint8_t  TMC65;                                          
    uint8_t  TMC66;                                          
    uint8_t  TMC67;                                          
    uint8_t  TMC68;                                          
    uint8_t  TMC69;                                          
    uint8_t  TMC70;                                          
    uint8_t  TMC71;                                          
    uint8_t  TMC72;                                          
    uint8_t  TMC73;                                          
    uint8_t  TMC74;                                          
    uint8_t  TMC75;                                          
    uint8_t  TMC76;                                          
    uint8_t  TMC77;                                          
    uint8_t  TMC78;                                          
    uint8_t  TMC79;                                          
    uint8_t  TMC80;                                          
    uint8_t  TMC81;                                          
    uint8_t  TMC82;                                          
    uint8_t  TMC83;                                          
    uint8_t  TMC84;                                          
    uint8_t  TMC85;                                          
    uint8_t  TMC86;                                          
    uint8_t  TMC87;                                          
    uint8_t  TMC88;                                          
    uint8_t  TMC89;                                          
    uint8_t  TMC90;                                          
    uint8_t  TMC91;                                          
    uint8_t  TMC92;                                          
    uint8_t  TMC93;                                          
    uint8_t  TMC94;                                          
    uint8_t  TMC95;                                          
    uint8_t  dummy39[32];                                    
    uint8_t  TMSTS0;                                         
    uint8_t  TMSTS1;                                         
    uint8_t  TMSTS2;                                         
    uint8_t  TMSTS3;                                         
    uint8_t  TMSTS4;                                         
    uint8_t  TMSTS5;                                         
    uint8_t  TMSTS6;                                         
    uint8_t  TMSTS7;                                         
    uint8_t  TMSTS8;                                         
    uint8_t  TMSTS9;                                         
    uint8_t  TMSTS10;                                        
    uint8_t  TMSTS11;                                        
    uint8_t  TMSTS12;                                        
    uint8_t  TMSTS13;                                        
    uint8_t  TMSTS14;                                        
    uint8_t  TMSTS15;                                        
    uint8_t  TMSTS16;                                        
    uint8_t  TMSTS17;                                        
    uint8_t  TMSTS18;                                        
    uint8_t  TMSTS19;                                        
    uint8_t  TMSTS20;                                        
    uint8_t  TMSTS21;                                        
    uint8_t  TMSTS22;                                        
    uint8_t  TMSTS23;                                        
    uint8_t  TMSTS24;                                        
    uint8_t  TMSTS25;                                        
    uint8_t  TMSTS26;                                        
    uint8_t  TMSTS27;                                        
    uint8_t  TMSTS28;                                        
    uint8_t  TMSTS29;                                        
    uint8_t  TMSTS30;                                        
    uint8_t  TMSTS31;                                        
    uint8_t  TMSTS32;                                        
    uint8_t  TMSTS33;                                        
    uint8_t  TMSTS34;                                        
    uint8_t  TMSTS35;                                        
    uint8_t  TMSTS36;                                        
    uint8_t  TMSTS37;                                        
    uint8_t  TMSTS38;                                        
    uint8_t  TMSTS39;                                        
    uint8_t  TMSTS40;                                        
    uint8_t  TMSTS41;                                        
    uint8_t  TMSTS42;                                        
    uint8_t  TMSTS43;                                        
    uint8_t  TMSTS44;                                        
    uint8_t  TMSTS45;                                        
    uint8_t  TMSTS46;                                        
    uint8_t  TMSTS47;                                        
    uint8_t  TMSTS48;                                        
    uint8_t  TMSTS49;                                        
    uint8_t  TMSTS50;                                        
    uint8_t  TMSTS51;                                        
    uint8_t  TMSTS52;                                        
    uint8_t  TMSTS53;                                        
    uint8_t  TMSTS54;                                        
    uint8_t  TMSTS55;                                        
    uint8_t  TMSTS56;                                        
    uint8_t  TMSTS57;                                        
    uint8_t  TMSTS58;                                        
    uint8_t  TMSTS59;                                        
    uint8_t  TMSTS60;                                        
    uint8_t  TMSTS61;                                        
    uint8_t  TMSTS62;                                        
    uint8_t  TMSTS63;                                        
    uint8_t  TMSTS64;                                        
    uint8_t  TMSTS65;                                        
    uint8_t  TMSTS66;                                        
    uint8_t  TMSTS67;                                        
    uint8_t  TMSTS68;                                        
    uint8_t  TMSTS69;                                        
    uint8_t  TMSTS70;                                        
    uint8_t  TMSTS71;                                        
    uint8_t  TMSTS72;                                        
    uint8_t  TMSTS73;                                        
    uint8_t  TMSTS74;                                        
    uint8_t  TMSTS75;                                        
    uint8_t  TMSTS76;                                        
    uint8_t  TMSTS77;                                        
    uint8_t  TMSTS78;                                        
    uint8_t  TMSTS79;                                        
    uint8_t  TMSTS80;                                        
    uint8_t  TMSTS81;                                        
    uint8_t  TMSTS82;                                        
    uint8_t  TMSTS83;                                        
    uint8_t  TMSTS84;                                        
    uint8_t  TMSTS85;                                        
    uint8_t  TMSTS86;                                        
    uint8_t  TMSTS87;                                        
    uint8_t  TMSTS88;                                        
    uint8_t  TMSTS89;                                        
    uint8_t  TMSTS90;                                        
    uint8_t  TMSTS91;                                        
    uint8_t  TMSTS92;                                        
    uint8_t  TMSTS93;                                        
    uint8_t  TMSTS94;                                        
    uint8_t  TMSTS95;                                        
    uint8_t  dummy40[32];                                    
    const __type10 TMTRSTS0;                               
    const __type10 TMTRSTS1;                               
    const __type10 TMTRSTS2;                               
    uint8_t  dummy41[4];                                     
    const __type10 TMTARSTS0;                              
    const __type10 TMTARSTS1;                              
    const __type10 TMTARSTS2;                              
    uint8_t  dummy42[4];                                     
    const __type10 TMTCSTS0;                               
    const __type10 TMTCSTS1;                               
    const __type10 TMTCSTS2;                               
    uint8_t  dummy43[4];                                     
    const __type10 TMTASTS0;                               
    const __type10 TMTASTS1;                               
    const __type10 TMTASTS2;                               
    uint8_t  dummy44[4];                                     
    __type10 TMIEC0;                                       
    __type10 TMIEC1;                                       
    __type10 TMIEC2;                                       
    uint8_t  dummy45[4];                                     
    __type10 TXQCC0;                                       
    __type10 TXQCC1;                                       
    __type10 TXQCC2;                                       
    __type10 TXQCC3;                                       
    __type10 TXQCC4;                                       
    __type10 TXQCC5;                                       
    uint8_t  dummy46[8];                                     
    __type10 TXQSTS0;                                      
    __type10 TXQSTS1;                                      
    __type10 TXQSTS2;                                      
    __type10 TXQSTS3;                                      
    __type10 TXQSTS4;                                      
    __type10 TXQSTS5;                                      
    uint8_t  dummy47[8];                                     
    __type10 TXQPCTR0;                                     
    __type10 TXQPCTR1;                                     
    __type10 TXQPCTR2;                                     
    __type10 TXQPCTR3;                                     
    __type10 TXQPCTR4;                                     
    __type10 TXQPCTR5;                                     
    uint8_t  dummy48[8];                                     
    __type10 THLCC0;                                       
    __type10 THLCC1;                                       
    __type10 THLCC2;                                       
    __type10 THLCC3;                                       
    __type10 THLCC4;                                       
    __type10 THLCC5;                                       
    uint8_t  dummy49[8];                                     
    __type10 THLSTS0;                                      
    __type10 THLSTS1;                                      
    __type10 THLSTS2;                                      
    __type10 THLSTS3;                                      
    __type10 THLSTS4;                                      
    __type10 THLSTS5;                                      
    uint8_t  dummy50[8];                                     
    __type10 THLPCTR0;                                     
    __type10 THLPCTR1;                                     
    __type10 THLPCTR2;                                     
    __type10 THLPCTR3;                                     
    __type10 THLPCTR4;                                     
    __type10 THLPCTR5;                                     
    uint8_t  dummy51[8];                                     
    const __type10 GTINTSTS0;                              
    const __type10 GTINTSTS1;                              
    __type10 GTSTCFG;                                      
    __type10 GTSTCTR;                                      
    uint8_t  dummy52[12];                                    
    __type15 GLOCKK;                                       
    uint8_t  dummy53[128];                                   
    __type10 GAFLID0;                                      
    __type10 GAFLM0;                                       
    __type10 GAFLP00;                                      
    __type10 GAFLP10;                                      
    __type10 GAFLID1;                                      
    __type10 GAFLM1;                                       
    __type10 GAFLP01;                                      
    __type10 GAFLP11;                                      
    __type10 GAFLID2;                                      
    __type10 GAFLM2;                                       
    __type10 GAFLP02;                                      
    __type10 GAFLP12;                                      
    __type10 GAFLID3;                                      
    __type10 GAFLM3;                                       
    __type10 GAFLP03;                                      
    __type10 GAFLP13;                                      
    __type10 GAFLID4;                                      
    __type10 GAFLM4;                                       
    __type10 GAFLP04;                                      
    __type10 GAFLP14;                                      
    __type10 GAFLID5;                                      
    __type10 GAFLM5;                                       
    __type10 GAFLP05;                                      
    __type10 GAFLP15;                                      
    __type10 GAFLID6;                                      
    __type10 GAFLM6;                                       
    __type10 GAFLP06;                                      
    __type10 GAFLP16;                                      
    __type10 GAFLID7;                                      
    __type10 GAFLM7;                                       
    __type10 GAFLP07;                                      
    __type10 GAFLP17;                                      
    __type10 GAFLID8;                                      
    __type10 GAFLM8;                                       
    __type10 GAFLP08;                                      
    __type10 GAFLP18;                                      
    __type10 GAFLID9;                                      
    __type10 GAFLM9;                                       
    __type10 GAFLP09;                                      
    __type10 GAFLP19;                                      
    __type10 GAFLID10;                                     
    __type10 GAFLM10;                                      
    __type10 GAFLP010;                                     
    __type10 GAFLP110;                                     
    __type10 GAFLID11;                                     
    __type10 GAFLM11;                                      
    __type10 GAFLP011;                                     
    __type10 GAFLP111;                                     
    __type10 GAFLID12;                                     
    __type10 GAFLM12;                                      
    __type10 GAFLP012;                                     
    __type10 GAFLP112;                                     
    __type10 GAFLID13;                                     
    __type10 GAFLM13;                                      
    __type10 GAFLP013;                                     
    __type10 GAFLP113;                                     
    __type10 GAFLID14;                                     
    __type10 GAFLM14;                                      
    __type10 GAFLP014;                                     
    __type10 GAFLP114;                                     
    __type10 GAFLID15;                                     
    __type10 GAFLM15;                                      
    __type10 GAFLP015;                                     
    __type10 GAFLP115;                                     
    const __type10 RMID0;                                  
    const __type10 RMPTR0;                                 
    const __type10 RMDF00;                                 
    const __type10 RMDF10;                                 
    const __type10 RMID1;                                  
    const __type10 RMPTR1;                                 
    const __type10 RMDF01;                                 
    const __type10 RMDF11;                                 
    const __type10 RMID2;                                  
    const __type10 RMPTR2;                                 
    const __type10 RMDF02;                                 
    const __type10 RMDF12;                                 
    const __type10 RMID3;                                  
    const __type10 RMPTR3;                                 
    const __type10 RMDF03;                                 
    const __type10 RMDF13;                                 
    const __type10 RMID4;                                  
    const __type10 RMPTR4;                                 
    const __type10 RMDF04;                                 
    const __type10 RMDF14;                                 
    const __type10 RMID5;                                  
    const __type10 RMPTR5;                                 
    const __type10 RMDF05;                                 
    const __type10 RMDF15;                                 
    const __type10 RMID6;                                  
    const __type10 RMPTR6;                                 
    const __type10 RMDF06;                                 
    const __type10 RMDF16;                                 
    const __type10 RMID7;                                  
    const __type10 RMPTR7;                                 
    const __type10 RMDF07;                                 
    const __type10 RMDF17;                                 
    const __type10 RMID8;                                  
    const __type10 RMPTR8;                                 
    const __type10 RMDF08;                                 
    const __type10 RMDF18;                                 
    const __type10 RMID9;                                  
    const __type10 RMPTR9;                                 
    const __type10 RMDF09;                                 
    const __type10 RMDF19;                                 
    const __type10 RMID10;                                 
    const __type10 RMPTR10;                                
    const __type10 RMDF010;                                
    const __type10 RMDF110;                                
    const __type10 RMID11;                                 
    const __type10 RMPTR11;                                
    const __type10 RMDF011;                                
    const __type10 RMDF111;                                
    const __type10 RMID12;                                 
    const __type10 RMPTR12;                                
    const __type10 RMDF012;                                
    const __type10 RMDF112;                                
    const __type10 RMID13;                                 
    const __type10 RMPTR13;                                
    const __type10 RMDF013;                                
    const __type10 RMDF113;                                
    const __type10 RMID14;                                 
    const __type10 RMPTR14;                                
    const __type10 RMDF014;                                
    const __type10 RMDF114;                                
    const __type10 RMID15;                                 
    const __type10 RMPTR15;                                
    const __type10 RMDF015;                                
    const __type10 RMDF115;                                
    const __type10 RMID16;                                 
    const __type10 RMPTR16;                                
    const __type10 RMDF016;                                
    const __type10 RMDF116;                                
    const __type10 RMID17;                                 
    const __type10 RMPTR17;                                
    const __type10 RMDF017;                                
    const __type10 RMDF117;                                
    const __type10 RMID18;                                 
    const __type10 RMPTR18;                                
    const __type10 RMDF018;                                
    const __type10 RMDF118;                                
    const __type10 RMID19;                                 
    const __type10 RMPTR19;                                
    const __type10 RMDF019;                                
    const __type10 RMDF119;                                
    const __type10 RMID20;                                 
    const __type10 RMPTR20;                                
    const __type10 RMDF020;                                
    const __type10 RMDF120;                                
    const __type10 RMID21;                                 
    const __type10 RMPTR21;                                
    const __type10 RMDF021;                                
    const __type10 RMDF121;                                
    const __type10 RMID22;                                 
    const __type10 RMPTR22;                                
    const __type10 RMDF022;                                
    const __type10 RMDF122;                                
    const __type10 RMID23;                                 
    const __type10 RMPTR23;                                
    const __type10 RMDF023;                                
    const __type10 RMDF123;                                
    const __type10 RMID24;                                 
    const __type10 RMPTR24;                                
    const __type10 RMDF024;                                
    const __type10 RMDF124;                                
    const __type10 RMID25;                                 
    const __type10 RMPTR25;                                
    const __type10 RMDF025;                                
    const __type10 RMDF125;                                
    const __type10 RMID26;                                 
    const __type10 RMPTR26;                                
    const __type10 RMDF026;                                
    const __type10 RMDF126;                                
    const __type10 RMID27;                                 
    const __type10 RMPTR27;                                
    const __type10 RMDF027;                                
    const __type10 RMDF127;                                
    const __type10 RMID28;                                 
    const __type10 RMPTR28;                                
    const __type10 RMDF028;                                
    const __type10 RMDF128;                                
    const __type10 RMID29;                                 
    const __type10 RMPTR29;                                
    const __type10 RMDF029;                                
    const __type10 RMDF129;                                
    const __type10 RMID30;                                 
    const __type10 RMPTR30;                                
    const __type10 RMDF030;                                
    const __type10 RMDF130;                                
    const __type10 RMID31;                                 
    const __type10 RMPTR31;                                
    const __type10 RMDF031;                                
    const __type10 RMDF131;                                
    const __type10 RMID32;                                 
    const __type10 RMPTR32;                                
    const __type10 RMDF032;                                
    const __type10 RMDF132;                                
    const __type10 RMID33;                                 
    const __type10 RMPTR33;                                
    const __type10 RMDF033;                                
    const __type10 RMDF133;                                
    const __type10 RMID34;                                 
    const __type10 RMPTR34;                                
    const __type10 RMDF034;                                
    const __type10 RMDF134;                                
    const __type10 RMID35;                                 
    const __type10 RMPTR35;                                
    const __type10 RMDF035;                                
    const __type10 RMDF135;                                
    const __type10 RMID36;                                 
    const __type10 RMPTR36;                                
    const __type10 RMDF036;                                
    const __type10 RMDF136;                                
    const __type10 RMID37;                                 
    const __type10 RMPTR37;                                
    const __type10 RMDF037;                                
    const __type10 RMDF137;                                
    const __type10 RMID38;                                 
    const __type10 RMPTR38;                                
    const __type10 RMDF038;                                
    const __type10 RMDF138;                                
    const __type10 RMID39;                                 
    const __type10 RMPTR39;                                
    const __type10 RMDF039;                                
    const __type10 RMDF139;                                
    const __type10 RMID40;                                 
    const __type10 RMPTR40;                                
    const __type10 RMDF040;                                
    const __type10 RMDF140;                                
    const __type10 RMID41;                                 
    const __type10 RMPTR41;                                
    const __type10 RMDF041;                                
    const __type10 RMDF141;                                
    const __type10 RMID42;                                 
    const __type10 RMPTR42;                                
    const __type10 RMDF042;                                
    const __type10 RMDF142;                                
    const __type10 RMID43;                                 
    const __type10 RMPTR43;                                
    const __type10 RMDF043;                                
    const __type10 RMDF143;                                
    const __type10 RMID44;                                 
    const __type10 RMPTR44;                                
    const __type10 RMDF044;                                
    const __type10 RMDF144;                                
    const __type10 RMID45;                                 
    const __type10 RMPTR45;                                
    const __type10 RMDF045;                                
    const __type10 RMDF145;                                
    const __type10 RMID46;                                 
    const __type10 RMPTR46;                                
    const __type10 RMDF046;                                
    const __type10 RMDF146;                                
    const __type10 RMID47;                                 
    const __type10 RMPTR47;                                
    const __type10 RMDF047;                                
    const __type10 RMDF147;                                
    const __type10 RMID48;                                 
    const __type10 RMPTR48;                                
    const __type10 RMDF048;                                
    const __type10 RMDF148;                                
    const __type10 RMID49;                                 
    const __type10 RMPTR49;                                
    const __type10 RMDF049;                                
    const __type10 RMDF149;                                
    const __type10 RMID50;                                 
    const __type10 RMPTR50;                                
    const __type10 RMDF050;                                
    const __type10 RMDF150;                                
    const __type10 RMID51;                                 
    const __type10 RMPTR51;                                
    const __type10 RMDF051;                                
    const __type10 RMDF151;                                
    const __type10 RMID52;                                 
    const __type10 RMPTR52;                                
    const __type10 RMDF052;                                
    const __type10 RMDF152;                                
    const __type10 RMID53;                                 
    const __type10 RMPTR53;                                
    const __type10 RMDF053;                                
    const __type10 RMDF153;                                
    const __type10 RMID54;                                 
    const __type10 RMPTR54;                                
    const __type10 RMDF054;                                
    const __type10 RMDF154;                                
    const __type10 RMID55;                                 
    const __type10 RMPTR55;                                
    const __type10 RMDF055;                                
    const __type10 RMDF155;                                
    const __type10 RMID56;                                 
    const __type10 RMPTR56;                                
    const __type10 RMDF056;                                
    const __type10 RMDF156;                                
    const __type10 RMID57;                                 
    const __type10 RMPTR57;                                
    const __type10 RMDF057;                                
    const __type10 RMDF157;                                
    const __type10 RMID58;                                 
    const __type10 RMPTR58;                                
    const __type10 RMDF058;                                
    const __type10 RMDF158;                                
    const __type10 RMID59;                                 
    const __type10 RMPTR59;                                
    const __type10 RMDF059;                                
    const __type10 RMDF159;                                
    const __type10 RMID60;                                 
    const __type10 RMPTR60;                                
    const __type10 RMDF060;                                
    const __type10 RMDF160;                                
    const __type10 RMID61;                                 
    const __type10 RMPTR61;                                
    const __type10 RMDF061;                                
    const __type10 RMDF161;                                
    const __type10 RMID62;                                 
    const __type10 RMPTR62;                                
    const __type10 RMDF062;                                
    const __type10 RMDF162;                                
    const __type10 RMID63;                                 
    const __type10 RMPTR63;                                
    const __type10 RMDF063;                                
    const __type10 RMDF163;                                
    const __type10 RMID64;                                 
    const __type10 RMPTR64;                                
    const __type10 RMDF064;                                
    const __type10 RMDF164;                                
    const __type10 RMID65;                                 
    const __type10 RMPTR65;                                
    const __type10 RMDF065;                                
    const __type10 RMDF165;                                
    const __type10 RMID66;                                 
    const __type10 RMPTR66;                                
    const __type10 RMDF066;                                
    const __type10 RMDF166;                                
    const __type10 RMID67;                                 
    const __type10 RMPTR67;                                
    const __type10 RMDF067;                                
    const __type10 RMDF167;                                
    const __type10 RMID68;                                 
    const __type10 RMPTR68;                                
    const __type10 RMDF068;                                
    const __type10 RMDF168;                                
    const __type10 RMID69;                                 
    const __type10 RMPTR69;                                
    const __type10 RMDF069;                                
    const __type10 RMDF169;                                
    const __type10 RMID70;                                 
    const __type10 RMPTR70;                                
    const __type10 RMDF070;                                
    const __type10 RMDF170;                                
    const __type10 RMID71;                                 
    const __type10 RMPTR71;                                
    const __type10 RMDF071;                                
    const __type10 RMDF171;                                
    const __type10 RMID72;                                 
    const __type10 RMPTR72;                                
    const __type10 RMDF072;                                
    const __type10 RMDF172;                                
    const __type10 RMID73;                                 
    const __type10 RMPTR73;                                
    const __type10 RMDF073;                                
    const __type10 RMDF173;                                
    const __type10 RMID74;                                 
    const __type10 RMPTR74;                                
    const __type10 RMDF074;                                
    const __type10 RMDF174;                                
    const __type10 RMID75;                                 
    const __type10 RMPTR75;                                
    const __type10 RMDF075;                                
    const __type10 RMDF175;                                
    const __type10 RMID76;                                 
    const __type10 RMPTR76;                                
    const __type10 RMDF076;                                
    const __type10 RMDF176;                                
    const __type10 RMID77;                                 
    const __type10 RMPTR77;                                
    const __type10 RMDF077;                                
    const __type10 RMDF177;                                
    const __type10 RMID78;                                 
    const __type10 RMPTR78;                                
    const __type10 RMDF078;                                
    const __type10 RMDF178;                                
    const __type10 RMID79;                                 
    const __type10 RMPTR79;                                
    const __type10 RMDF079;                                
    const __type10 RMDF179;                                
    const __type10 RMID80;                                 
    const __type10 RMPTR80;                                
    const __type10 RMDF080;                                
    const __type10 RMDF180;                                
    const __type10 RMID81;                                 
    const __type10 RMPTR81;                                
    const __type10 RMDF081;                                
    const __type10 RMDF181;                                
    const __type10 RMID82;                                 
    const __type10 RMPTR82;                                
    const __type10 RMDF082;                                
    const __type10 RMDF182;                                
    const __type10 RMID83;                                 
    const __type10 RMPTR83;                                
    const __type10 RMDF083;                                
    const __type10 RMDF183;                                
    const __type10 RMID84;                                 
    const __type10 RMPTR84;                                
    const __type10 RMDF084;                                
    const __type10 RMDF184;                                
    const __type10 RMID85;                                 
    const __type10 RMPTR85;                                
    const __type10 RMDF085;                                
    const __type10 RMDF185;                                
    const __type10 RMID86;                                 
    const __type10 RMPTR86;                                
    const __type10 RMDF086;                                
    const __type10 RMDF186;                                
    const __type10 RMID87;                                 
    const __type10 RMPTR87;                                
    const __type10 RMDF087;                                
    const __type10 RMDF187;                                
    const __type10 RMID88;                                 
    const __type10 RMPTR88;                                
    const __type10 RMDF088;                                
    const __type10 RMDF188;                                
    const __type10 RMID89;                                 
    const __type10 RMPTR89;                                
    const __type10 RMDF089;                                
    const __type10 RMDF189;                                
    const __type10 RMID90;                                 
    const __type10 RMPTR90;                                
    const __type10 RMDF090;                                
    const __type10 RMDF190;                                
    const __type10 RMID91;                                 
    const __type10 RMPTR91;                                
    const __type10 RMDF091;                                
    const __type10 RMDF191;                                
    const __type10 RMID92;                                 
    const __type10 RMPTR92;                                
    const __type10 RMDF092;                                
    const __type10 RMDF192;                                
    const __type10 RMID93;                                 
    const __type10 RMPTR93;                                
    const __type10 RMDF093;                                
    const __type10 RMDF193;                                
    const __type10 RMID94;                                 
    const __type10 RMPTR94;                                
    const __type10 RMDF094;                                
    const __type10 RMDF194;                                
    const __type10 RMID95;                                 
    const __type10 RMPTR95;                                
    const __type10 RMDF095;                                
    const __type10 RMDF195;                                
    uint8_t  dummy54[512];                                   
    const __type10 RFID0;                                  
    const __type10 RFPTR0;                                 
    const __type10 RFDF00;                                 
    const __type10 RFDF10;                                 
    const __type10 RFID1;                                  
    const __type10 RFPTR1;                                 
    const __type10 RFDF01;                                 
    const __type10 RFDF11;                                 
    const __type10 RFID2;                                  
    const __type10 RFPTR2;                                 
    const __type10 RFDF02;                                 
    const __type10 RFDF12;                                 
    const __type10 RFID3;                                  
    const __type10 RFPTR3;                                 
    const __type10 RFDF03;                                 
    const __type10 RFDF13;                                 
    const __type10 RFID4;                                  
    const __type10 RFPTR4;                                 
    const __type10 RFDF04;                                 
    const __type10 RFDF14;                                 
    const __type10 RFID5;                                  
    const __type10 RFPTR5;                                 
    const __type10 RFDF05;                                 
    const __type10 RFDF15;                                 
    const __type10 RFID6;                                  
    const __type10 RFPTR6;                                 
    const __type10 RFDF06;                                 
    const __type10 RFDF16;                                 
    const __type10 RFID7;                                  
    const __type10 RFPTR7;                                 
    const __type10 RFDF07;                                 
    const __type10 RFDF17;                                 
    __type10 CFID0;                                        
    __type10 CFPTR0;                                       
    __type10 CFDF00;                                       
    __type10 CFDF10;                                       
    __type10 CFID1;                                        
    __type10 CFPTR1;                                       
    __type10 CFDF01;                                       
    __type10 CFDF11;                                       
    __type10 CFID2;                                        
    __type10 CFPTR2;                                       
    __type10 CFDF02;                                       
    __type10 CFDF12;                                       
    __type10 CFID3;                                        
    __type10 CFPTR3;                                       
    __type10 CFDF03;                                       
    __type10 CFDF13;                                       
    __type10 CFID4;                                        
    __type10 CFPTR4;                                       
    __type10 CFDF04;                                       
    __type10 CFDF14;                                       
    __type10 CFID5;                                        
    __type10 CFPTR5;                                       
    __type10 CFDF05;                                       
    __type10 CFDF15;                                       
    __type10 CFID6;                                        
    __type10 CFPTR6;                                       
    __type10 CFDF06;                                       
    __type10 CFDF16;                                       
    __type10 CFID7;                                        
    __type10 CFPTR7;                                       
    __type10 CFDF07;                                       
    __type10 CFDF17;                                       
    __type10 CFID8;                                        
    __type10 CFPTR8;                                       
    __type10 CFDF08;                                       
    __type10 CFDF18;                                       
    __type10 CFID9;                                        
    __type10 CFPTR9;                                       
    __type10 CFDF09;                                       
    __type10 CFDF19;                                       
    __type10 CFID10;                                       
    __type10 CFPTR10;                                      
    __type10 CFDF010;                                      
    __type10 CFDF110;                                      
    __type10 CFID11;                                       
    __type10 CFPTR11;                                      
    __type10 CFDF011;                                      
    __type10 CFDF111;                                      
    __type10 CFID12;                                       
    __type10 CFPTR12;                                      
    __type10 CFDF012;                                      
    __type10 CFDF112;                                      
    __type10 CFID13;                                       
    __type10 CFPTR13;                                      
    __type10 CFDF013;                                      
    __type10 CFDF113;                                      
    __type10 CFID14;                                       
    __type10 CFPTR14;                                      
    __type10 CFDF014;                                      
    __type10 CFDF114;                                      
    __type10 CFID15;                                       
    __type10 CFPTR15;                                      
    __type10 CFDF015;                                      
    __type10 CFDF115;                                      
    __type10 CFID16;                                       
    __type10 CFPTR16;                                      
    __type10 CFDF016;                                      
    __type10 CFDF116;                                      
    __type10 CFID17;                                       
    __type10 CFPTR17;                                      
    __type10 CFDF017;                                      
    __type10 CFDF117;                                      
    uint8_t  dummy55[96];                                    
    __type10 TMID0;                                        
    __type10 TMPTR0;                                       
    __type10 TMDF00;                                       
    __type10 TMDF10;                                       
    __type10 TMID1;                                        
    __type10 TMPTR1;                                       
    __type10 TMDF01;                                       
    __type10 TMDF11;                                       
    __type10 TMID2;                                        
    __type10 TMPTR2;                                       
    __type10 TMDF02;                                       
    __type10 TMDF12;                                       
    __type10 TMID3;                                        
    __type10 TMPTR3;                                       
    __type10 TMDF03;                                       
    __type10 TMDF13;                                       
    __type10 TMID4;                                        
    __type10 TMPTR4;                                       
    __type10 TMDF04;                                       
    __type10 TMDF14;                                       
    __type10 TMID5;                                        
    __type10 TMPTR5;                                       
    __type10 TMDF05;                                       
    __type10 TMDF15;                                       
    __type10 TMID6;                                        
    __type10 TMPTR6;                                       
    __type10 TMDF06;                                       
    __type10 TMDF16;                                       
    __type10 TMID7;                                        
    __type10 TMPTR7;                                       
    __type10 TMDF07;                                       
    __type10 TMDF17;                                       
    __type10 TMID8;                                        
    __type10 TMPTR8;                                       
    __type10 TMDF08;                                       
    __type10 TMDF18;                                       
    __type10 TMID9;                                        
    __type10 TMPTR9;                                       
    __type10 TMDF09;                                       
    __type10 TMDF19;                                       
    __type10 TMID10;                                       
    __type10 TMPTR10;                                      
    __type10 TMDF010;                                      
    __type10 TMDF110;                                      
    __type10 TMID11;                                       
    __type10 TMPTR11;                                      
    __type10 TMDF011;                                      
    __type10 TMDF111;                                      
    __type10 TMID12;                                       
    __type10 TMPTR12;                                      
    __type10 TMDF012;                                      
    __type10 TMDF112;                                      
    __type10 TMID13;                                       
    __type10 TMPTR13;                                      
    __type10 TMDF013;                                      
    __type10 TMDF113;                                      
    __type10 TMID14;                                       
    __type10 TMPTR14;                                      
    __type10 TMDF014;                                      
    __type10 TMDF114;                                      
    __type10 TMID15;                                       
    __type10 TMPTR15;                                      
    __type10 TMDF015;                                      
    __type10 TMDF115;                                      
    __type10 TMID16;                                       
    __type10 TMPTR16;                                      
    __type10 TMDF016;                                      
    __type10 TMDF116;                                      
    __type10 TMID17;                                       
    __type10 TMPTR17;                                      
    __type10 TMDF017;                                      
    __type10 TMDF117;                                      
    __type10 TMID18;                                       
    __type10 TMPTR18;                                      
    __type10 TMDF018;                                      
    __type10 TMDF118;                                      
    __type10 TMID19;                                       
    __type10 TMPTR19;                                      
    __type10 TMDF019;                                      
    __type10 TMDF119;                                      
    __type10 TMID20;                                       
    __type10 TMPTR20;                                      
    __type10 TMDF020;                                      
    __type10 TMDF120;                                      
    __type10 TMID21;                                       
    __type10 TMPTR21;                                      
    __type10 TMDF021;                                      
    __type10 TMDF121;                                      
    __type10 TMID22;                                       
    __type10 TMPTR22;                                      
    __type10 TMDF022;                                      
    __type10 TMDF122;                                      
    __type10 TMID23;                                       
    __type10 TMPTR23;                                      
    __type10 TMDF023;                                      
    __type10 TMDF123;                                      
    __type10 TMID24;                                       
    __type10 TMPTR24;                                      
    __type10 TMDF024;                                      
    __type10 TMDF124;                                      
    __type10 TMID25;                                       
    __type10 TMPTR25;                                      
    __type10 TMDF025;                                      
    __type10 TMDF125;                                      
    __type10 TMID26;                                       
    __type10 TMPTR26;                                      
    __type10 TMDF026;                                      
    __type10 TMDF126;                                      
    __type10 TMID27;                                       
    __type10 TMPTR27;                                      
    __type10 TMDF027;                                      
    __type10 TMDF127;                                      
    __type10 TMID28;                                       
    __type10 TMPTR28;                                      
    __type10 TMDF028;                                      
    __type10 TMDF128;                                      
    __type10 TMID29;                                       
    __type10 TMPTR29;                                      
    __type10 TMDF029;                                      
    __type10 TMDF129;                                      
    __type10 TMID30;                                       
    __type10 TMPTR30;                                      
    __type10 TMDF030;                                      
    __type10 TMDF130;                                      
    __type10 TMID31;                                       
    __type10 TMPTR31;                                      
    __type10 TMDF031;                                      
    __type10 TMDF131;                                      
    __type10 TMID32;                                       
    __type10 TMPTR32;                                      
    __type10 TMDF032;                                      
    __type10 TMDF132;                                      
    __type10 TMID33;                                       
    __type10 TMPTR33;                                      
    __type10 TMDF033;                                      
    __type10 TMDF133;                                      
    __type10 TMID34;                                       
    __type10 TMPTR34;                                      
    __type10 TMDF034;                                      
    __type10 TMDF134;                                      
    __type10 TMID35;                                       
    __type10 TMPTR35;                                      
    __type10 TMDF035;                                      
    __type10 TMDF135;                                      
    __type10 TMID36;                                       
    __type10 TMPTR36;                                      
    __type10 TMDF036;                                      
    __type10 TMDF136;                                      
    __type10 TMID37;                                       
    __type10 TMPTR37;                                      
    __type10 TMDF037;                                      
    __type10 TMDF137;                                      
    __type10 TMID38;                                       
    __type10 TMPTR38;                                      
    __type10 TMDF038;                                      
    __type10 TMDF138;                                      
    __type10 TMID39;                                       
    __type10 TMPTR39;                                      
    __type10 TMDF039;                                      
    __type10 TMDF139;                                      
    __type10 TMID40;                                       
    __type10 TMPTR40;                                      
    __type10 TMDF040;                                      
    __type10 TMDF140;                                      
    __type10 TMID41;                                       
    __type10 TMPTR41;                                      
    __type10 TMDF041;                                      
    __type10 TMDF141;                                      
    __type10 TMID42;                                       
    __type10 TMPTR42;                                      
    __type10 TMDF042;                                      
    __type10 TMDF142;                                      
    __type10 TMID43;                                       
    __type10 TMPTR43;                                      
    __type10 TMDF043;                                      
    __type10 TMDF143;                                      
    __type10 TMID44;                                       
    __type10 TMPTR44;                                      
    __type10 TMDF044;                                      
    __type10 TMDF144;                                      
    __type10 TMID45;                                       
    __type10 TMPTR45;                                      
    __type10 TMDF045;                                      
    __type10 TMDF145;                                      
    __type10 TMID46;                                       
    __type10 TMPTR46;                                      
    __type10 TMDF046;                                      
    __type10 TMDF146;                                      
    __type10 TMID47;                                       
    __type10 TMPTR47;                                      
    __type10 TMDF047;                                      
    __type10 TMDF147;                                      
    __type10 TMID48;                                       
    __type10 TMPTR48;                                      
    __type10 TMDF048;                                      
    __type10 TMDF148;                                      
    __type10 TMID49;                                       
    __type10 TMPTR49;                                      
    __type10 TMDF049;                                      
    __type10 TMDF149;                                      
    __type10 TMID50;                                       
    __type10 TMPTR50;                                      
    __type10 TMDF050;                                      
    __type10 TMDF150;                                      
    __type10 TMID51;                                       
    __type10 TMPTR51;                                      
    __type10 TMDF051;                                      
    __type10 TMDF151;                                      
    __type10 TMID52;                                       
    __type10 TMPTR52;                                      
    __type10 TMDF052;                                      
    __type10 TMDF152;                                      
    __type10 TMID53;                                       
    __type10 TMPTR53;                                      
    __type10 TMDF053;                                      
    __type10 TMDF153;                                      
    __type10 TMID54;                                       
    __type10 TMPTR54;                                      
    __type10 TMDF054;                                      
    __type10 TMDF154;                                      
    __type10 TMID55;                                       
    __type10 TMPTR55;                                      
    __type10 TMDF055;                                      
    __type10 TMDF155;                                      
    __type10 TMID56;                                       
    __type10 TMPTR56;                                      
    __type10 TMDF056;                                      
    __type10 TMDF156;                                      
    __type10 TMID57;                                       
    __type10 TMPTR57;                                      
    __type10 TMDF057;                                      
    __type10 TMDF157;                                      
    __type10 TMID58;                                       
    __type10 TMPTR58;                                      
    __type10 TMDF058;                                      
    __type10 TMDF158;                                      
    __type10 TMID59;                                       
    __type10 TMPTR59;                                      
    __type10 TMDF059;                                      
    __type10 TMDF159;                                      
    __type10 TMID60;                                       
    __type10 TMPTR60;                                      
    __type10 TMDF060;                                      
    __type10 TMDF160;                                      
    __type10 TMID61;                                       
    __type10 TMPTR61;                                      
    __type10 TMDF061;                                      
    __type10 TMDF161;                                      
    __type10 TMID62;                                       
    __type10 TMPTR62;                                      
    __type10 TMDF062;                                      
    __type10 TMDF162;                                      
    __type10 TMID63;                                       
    __type10 TMPTR63;                                      
    __type10 TMDF063;                                      
    __type10 TMDF163;                                      
    __type10 TMID64;                                       
    __type10 TMPTR64;                                      
    __type10 TMDF064;                                      
    __type10 TMDF164;                                      
    __type10 TMID65;                                       
    __type10 TMPTR65;                                      
    __type10 TMDF065;                                      
    __type10 TMDF165;                                      
    __type10 TMID66;                                       
    __type10 TMPTR66;                                      
    __type10 TMDF066;                                      
    __type10 TMDF166;                                      
    __type10 TMID67;                                       
    __type10 TMPTR67;                                      
    __type10 TMDF067;                                      
    __type10 TMDF167;                                      
    __type10 TMID68;                                       
    __type10 TMPTR68;                                      
    __type10 TMDF068;                                      
    __type10 TMDF168;                                      
    __type10 TMID69;                                       
    __type10 TMPTR69;                                      
    __type10 TMDF069;                                      
    __type10 TMDF169;                                      
    __type10 TMID70;                                       
    __type10 TMPTR70;                                      
    __type10 TMDF070;                                      
    __type10 TMDF170;                                      
    __type10 TMID71;                                       
    __type10 TMPTR71;                                      
    __type10 TMDF071;                                      
    __type10 TMDF171;                                      
    __type10 TMID72;                                       
    __type10 TMPTR72;                                      
    __type10 TMDF072;                                      
    __type10 TMDF172;                                      
    __type10 TMID73;                                       
    __type10 TMPTR73;                                      
    __type10 TMDF073;                                      
    __type10 TMDF173;                                      
    __type10 TMID74;                                       
    __type10 TMPTR74;                                      
    __type10 TMDF074;                                      
    __type10 TMDF174;                                      
    __type10 TMID75;                                       
    __type10 TMPTR75;                                      
    __type10 TMDF075;                                      
    __type10 TMDF175;                                      
    __type10 TMID76;                                       
    __type10 TMPTR76;                                      
    __type10 TMDF076;                                      
    __type10 TMDF176;                                      
    __type10 TMID77;                                       
    __type10 TMPTR77;                                      
    __type10 TMDF077;                                      
    __type10 TMDF177;                                      
    __type10 TMID78;                                       
    __type10 TMPTR78;                                      
    __type10 TMDF078;                                      
    __type10 TMDF178;                                      
    __type10 TMID79;                                       
    __type10 TMPTR79;                                      
    __type10 TMDF079;                                      
    __type10 TMDF179;                                      
    __type10 TMID80;                                       
    __type10 TMPTR80;                                      
    __type10 TMDF080;                                      
    __type10 TMDF180;                                      
    __type10 TMID81;                                       
    __type10 TMPTR81;                                      
    __type10 TMDF081;                                      
    __type10 TMDF181;                                      
    __type10 TMID82;                                       
    __type10 TMPTR82;                                      
    __type10 TMDF082;                                      
    __type10 TMDF182;                                      
    __type10 TMID83;                                       
    __type10 TMPTR83;                                      
    __type10 TMDF083;                                      
    __type10 TMDF183;                                      
    __type10 TMID84;                                       
    __type10 TMPTR84;                                      
    __type10 TMDF084;                                      
    __type10 TMDF184;                                      
    __type10 TMID85;                                       
    __type10 TMPTR85;                                      
    __type10 TMDF085;                                      
    __type10 TMDF185;                                      
    __type10 TMID86;                                       
    __type10 TMPTR86;                                      
    __type10 TMDF086;                                      
    __type10 TMDF186;                                      
    __type10 TMID87;                                       
    __type10 TMPTR87;                                      
    __type10 TMDF087;                                      
    __type10 TMDF187;                                      
    __type10 TMID88;                                       
    __type10 TMPTR88;                                      
    __type10 TMDF088;                                      
    __type10 TMDF188;                                      
    __type10 TMID89;                                       
    __type10 TMPTR89;                                      
    __type10 TMDF089;                                      
    __type10 TMDF189;                                      
    __type10 TMID90;                                       
    __type10 TMPTR90;                                      
    __type10 TMDF090;                                      
    __type10 TMDF190;                                      
    __type10 TMID91;                                       
    __type10 TMPTR91;                                      
    __type10 TMDF091;                                      
    __type10 TMDF191;                                      
    __type10 TMID92;                                       
    __type10 TMPTR92;                                      
    __type10 TMDF092;                                      
    __type10 TMDF192;                                      
    __type10 TMID93;                                       
    __type10 TMPTR93;                                      
    __type10 TMDF093;                                      
    __type10 TMDF193;                                      
    __type10 TMID94;                                       
    __type10 TMPTR94;                                      
    __type10 TMDF094;                                      
    __type10 TMDF194;                                      
    __type10 TMID95;                                       
    __type10 TMPTR95;                                      
    __type10 TMDF095;                                      
    __type10 TMDF195;                                      
    uint8_t  dummy56[512];                                   
    const __type10 THLACC0;                                
    const __type10 THLACC1;                                
    const __type10 THLACC2;                                
    const __type10 THLACC3;                                
    const __type10 THLACC4;                                
    const __type10 THLACC5;                                
    uint8_t  dummy57[232];                                   
    __type10 RPGACC0;                                      
    __type10 RPGACC1;                                      
    __type10 RPGACC2;                                      
    __type10 RPGACC3;                                      
    __type10 RPGACC4;                                      
    __type10 RPGACC5;                                      
    __type10 RPGACC6;                                      
    __type10 RPGACC7;                                      
    __type10 RPGACC8;                                      
    __type10 RPGACC9;                                      
    __type10 RPGACC10;                                     
    __type10 RPGACC11;                                     
    __type10 RPGACC12;                                     
    __type10 RPGACC13;                                     
    __type10 RPGACC14;                                     
    __type10 RPGACC15;                                     
    __type10 RPGACC16;                                     
    __type10 RPGACC17;                                     
    __type10 RPGACC18;                                     
    __type10 RPGACC19;                                     
    __type10 RPGACC20;                                     
    __type10 RPGACC21;                                     
    __type10 RPGACC22;                                     
    __type10 RPGACC23;                                     
    __type10 RPGACC24;                                     
    __type10 RPGACC25;                                     
    __type10 RPGACC26;                                     
    __type10 RPGACC27;                                     
    __type10 RPGACC28;                                     
    __type10 RPGACC29;                                     
    __type10 RPGACC30;                                     
    __type10 RPGACC31;                                     
    __type10 RPGACC32;                                     
    __type10 RPGACC33;                                     
    __type10 RPGACC34;                                     
    __type10 RPGACC35;                                     
    __type10 RPGACC36;                                     
    __type10 RPGACC37;                                     
    __type10 RPGACC38;                                     
    __type10 RPGACC39;                                     
    __type10 RPGACC40;                                     
    __type10 RPGACC41;                                     
    __type10 RPGACC42;                                     
    __type10 RPGACC43;                                     
    __type10 RPGACC44;                                     
    __type10 RPGACC45;                                     
    __type10 RPGACC46;                                     
    __type10 RPGACC47;                                     
    __type10 RPGACC48;                                     
    __type10 RPGACC49;                                     
    __type10 RPGACC50;                                     
    __type10 RPGACC51;                                     
    __type10 RPGACC52;                                     
    __type10 RPGACC53;                                     
    __type10 RPGACC54;                                     
    __type10 RPGACC55;                                     
    __type10 RPGACC56;                                     
    __type10 RPGACC57;                                     
    __type10 RPGACC58;                                     
    __type10 RPGACC59;                                     
    __type10 RPGACC60;                                     
    __type10 RPGACC61;                                     
    __type10 RPGACC62;                                     
    __type10 RPGACC63;                                     
} __type36;
typedef struct 
{                                                          
    __type16 CTL0;                                         
    uint8_t  dummy58[3];                                     
    const uint32_t STR0;                                     
    uint16_t STCR0;                                          
    uint8_t  dummy59[6];                                     
    uint32_t CTL1;                                           
    uint16_t CTL2;                                           
    uint8_t  dummy60[2];                                     
    __type17 EMU;                                          
    uint8_t  dummy61[4071];                                  
    uint32_t MCTL1;                                          
    uint32_t MCTL2;                                          
    uint32_t TX0W;                                           
    uint16_t TX0H;                                           
    uint8_t  dummy62[2];                                     
    const uint32_t RX0W;                                     
    const uint16_t RX0H;                                     
    uint8_t  dummy63[2];                                     
    uint32_t MRWP0;                                          
    uint8_t  dummy64[36];                                    
    uint16_t MCTL0;                                          
    uint8_t  dummy65[2];                                     
    uint32_t CFG0;                                           
    uint32_t CFG1;                                           
    uint32_t CFG2;                                           
    uint32_t CFG3;                                           
    uint32_t CFG4;                                           
    uint32_t CFG5;                                           
    uint32_t CFG6;                                           
    uint32_t CFG7;                                           
    uint8_t  dummy66[4];                                     
    uint16_t BRS0;                                           
    uint8_t  dummy67[2];                                     
    uint16_t BRS1;                                           
    uint8_t  dummy68[2];                                     
    uint16_t BRS2;                                           
    uint8_t  dummy69[2];                                     
    uint16_t BRS3;                                           
} __type37;
typedef struct 
{                                                          
    __type16 CTL0;                                         
    uint8_t  dummy70[3];                                     
    const uint32_t STR0;                                     
    uint16_t STCR0;                                          
    uint8_t  dummy71[6];                                     
    uint32_t CTL1;                                           
    uint16_t CTL2;                                           
    uint8_t  dummy72[2];                                     
    __type17 EMU;                                          
    uint8_t  dummy73[4071];                                  
    uint32_t MCTL1;                                          
    uint32_t MCTL2;                                          
    uint32_t TX0W;                                           
    uint16_t TX0H;                                           
    uint8_t  dummy74[2];                                     
    const uint32_t RX0W;                                     
    const uint16_t RX0H;                                     
    uint8_t  dummy75[2];                                     
    uint32_t MRWP0;                                          
    uint8_t  dummy76[36];                                    
    uint16_t MCTL0;                                          
    uint8_t  dummy77[2];                                     
    uint32_t CFG0;                                           
    uint32_t CFG1;                                           
    uint32_t CFG2;                                           
    uint32_t CFG3;                                           
    uint32_t CFG4;                                           
    uint32_t CFG5;                                           
    uint8_t  dummy78[12];                                    
    uint16_t BRS0;                                           
    uint8_t  dummy79[2];                                     
    uint16_t BRS1;                                           
    uint8_t  dummy80[2];                                     
    uint16_t BRS2;                                           
    uint8_t  dummy81[2];                                     
    uint16_t BRS3;                                           
} __type38;
typedef struct 
{                                                          
    __type16 CTL0;                                         
    uint8_t  dummy82[3];                                     
    const uint32_t STR0;                                     
    uint16_t STCR0;                                          
    uint8_t  dummy83[6];                                     
    uint32_t CTL1;                                           
    uint16_t CTL2;                                           
    uint8_t  dummy84[2];                                     
    __type17 EMU;                                          
    uint8_t  dummy85[4071];                                  
    uint32_t MCTL1;                                          
    uint32_t MCTL2;                                          
    uint32_t TX0W;                                           
    uint16_t TX0H;                                           
    uint8_t  dummy86[2];                                     
    const uint32_t RX0W;                                     
    const uint16_t RX0H;                                     
    uint8_t  dummy87[2];                                     
    uint32_t MRWP0;                                          
    uint8_t  dummy88[36];                                    
    uint16_t MCTL0;                                          
    uint8_t  dummy89[2];                                     
    uint32_t CFG0;                                           
    uint32_t CFG1;                                           
    uint32_t CFG2;                                           
    uint32_t CFG3;                                           
    uint8_t  dummy90[20];                                    
    uint16_t BRS0;                                           
    uint8_t  dummy91[2];                                     
    uint16_t BRS1;                                           
    uint8_t  dummy92[2];                                     
    uint16_t BRS2;                                           
    uint8_t  dummy93[2];                                     
    uint16_t BRS3;                                           
} __type39;
typedef struct 
{                                                          
    __type18 CTL0;                                         
    uint8_t  dummy94[3];                                     
    const uint32_t STR0;                                     
    uint16_t STCR0;                                          
    uint8_t  dummy95[6];                                     
    uint32_t CTL1;                                           
    uint16_t CTL2;                                           
    uint8_t  dummy96[2];                                     
    __type17 EMU;                                          
    uint8_t  dummy97[4071];                                  
    __type19 BCTL0;                                        
    uint8_t  dummy98[3];                                     
    uint32_t TX0W;                                           
    uint16_t TX0H;                                           
    uint8_t  dummy99[2];                                     
    const uint16_t RX0;                                      
    uint8_t  dummy100[2];                                    
    uint32_t CFG0;                                           
    uint8_t  dummy101[4076];                                 
} __type40;
typedef struct 
{                                                          
    uint8_t  SST;                                            
    uint8_t  dummy102[11];                                   
    uint16_t SSER0;                                          
    uint8_t  dummy103[6];                                    
    uint16_t SSER2;                                          
    uint8_t  dummy104[102];                                  
    uint8_t  HIZCEN0;                                        
    uint8_t  dummy105[15];                                   
    uint16_t ADTEN400;                                       
    uint8_t  dummy106[2];                                    
    uint16_t ADTEN401;                                       
    uint8_t  dummy107[2];                                    
    uint16_t ADTEN402;                                       
    uint8_t  dummy108[38];                                   
    uint32_t REG200;                                         
    uint32_t REG201;                                         
    uint32_t REG202;                                         
    uint32_t REG203;                                         
    uint8_t  dummy109[24];                                   
    uint32_t REG30;                                          
    uint32_t REG31;                                          
} __type41;
typedef struct 
{                                                          
    uint16_t CDR0;                                           
    uint8_t  dummy110[2];                                    
    uint16_t CDR1;                                           
    uint8_t  dummy111[2];                                    
    uint16_t CDR2;                                           
    uint8_t  dummy112[2];                                    
    uint16_t CDR3;                                           
    uint8_t  dummy113[2];                                    
    uint16_t CDR4;                                           
    uint8_t  dummy114[2];                                    
    uint16_t CDR5;                                           
    uint8_t  dummy115[2];                                    
    uint16_t CDR6;                                           
    uint8_t  dummy116[2];                                    
    uint16_t CDR7;                                           
    uint8_t  dummy117[2];                                    
    uint16_t CDR8;                                           
    uint8_t  dummy118[2];                                    
    uint16_t CDR9;                                           
    uint8_t  dummy119[2];                                    
    uint16_t CDR10;                                          
    uint8_t  dummy120[2];                                    
    uint16_t CDR11;                                          
    uint8_t  dummy121[2];                                    
    uint16_t CDR12;                                          
    uint8_t  dummy122[2];                                    
    uint16_t CDR13;                                          
    uint8_t  dummy123[2];                                    
    uint16_t CDR14;                                          
    uint8_t  dummy124[2];                                    
    uint16_t CDR15;                                          
    uint8_t  dummy125[2];                                    
    uint16_t TOL;                                            
    uint8_t  dummy126[2];                                    
    uint16_t RDT;                                            
    uint8_t  dummy127[2];                                    
    const uint16_t RSF;                                      
    uint8_t  dummy128[2];                                    
    uint16_t TRO;                                            
    uint8_t  dummy129[2];                                    
    uint16_t TME;                                            
    uint8_t  dummy130[2];                                    
    uint16_t TDL;                                            
    uint8_t  dummy131[2];                                    
    uint16_t TO;                                             
    uint8_t  dummy132[2];                                    
    uint16_t TOE;                                            
    uint8_t  dummy133[34];                                   
    const uint16_t CNT0;                                     
    uint8_t  dummy134[2];                                    
    const uint16_t CNT1;                                     
    uint8_t  dummy135[2];                                    
    const uint16_t CNT2;                                     
    uint8_t  dummy136[2];                                    
    const uint16_t CNT3;                                     
    uint8_t  dummy137[2];                                    
    const uint16_t CNT4;                                     
    uint8_t  dummy138[2];                                    
    const uint16_t CNT5;                                     
    uint8_t  dummy139[2];                                    
    const uint16_t CNT6;                                     
    uint8_t  dummy140[2];                                    
    const uint16_t CNT7;                                     
    uint8_t  dummy141[2];                                    
    const uint16_t CNT8;                                     
    uint8_t  dummy142[2];                                    
    const uint16_t CNT9;                                     
    uint8_t  dummy143[2];                                    
    const uint16_t CNT10;                                    
    uint8_t  dummy144[2];                                    
    const uint16_t CNT11;                                    
    uint8_t  dummy145[2];                                    
    const uint16_t CNT12;                                    
    uint8_t  dummy146[2];                                    
    const uint16_t CNT13;                                    
    uint8_t  dummy147[2];                                    
    const uint16_t CNT14;                                    
    uint8_t  dummy148[2];                                    
    const uint16_t CNT15;                                    
    uint8_t  dummy149[2];                                    
    uint8_t  CMUR0;                                          
    uint8_t  dummy150[3];                                    
    uint8_t  CMUR1;                                          
    uint8_t  dummy151[3];                                    
    uint8_t  CMUR2;                                          
    uint8_t  dummy152[3];                                    
    uint8_t  CMUR3;                                          
    uint8_t  dummy153[3];                                    
    uint8_t  CMUR4;                                          
    uint8_t  dummy154[3];                                    
    uint8_t  CMUR5;                                          
    uint8_t  dummy155[3];                                    
    uint8_t  CMUR6;                                          
    uint8_t  dummy156[3];                                    
    uint8_t  CMUR7;                                          
    uint8_t  dummy157[3];                                    
    uint8_t  CMUR8;                                          
    uint8_t  dummy158[3];                                    
    uint8_t  CMUR9;                                          
    uint8_t  dummy159[3];                                    
    uint8_t  CMUR10;                                         
    uint8_t  dummy160[3];                                    
    uint8_t  CMUR11;                                         
    uint8_t  dummy161[3];                                    
    uint8_t  CMUR12;                                         
    uint8_t  dummy162[3];                                    
    uint8_t  CMUR13;                                         
    uint8_t  dummy163[3];                                    
    uint8_t  CMUR14;                                         
    uint8_t  dummy164[3];                                    
    uint8_t  CMUR15;                                         
    uint8_t  dummy165[67];                                   
    const uint8_t  CSR0;                                     
    uint8_t  dummy166[3];                                    
    const uint8_t  CSR1;                                     
    uint8_t  dummy167[3];                                    
    const uint8_t  CSR2;                                     
    uint8_t  dummy168[3];                                    
    const uint8_t  CSR3;                                     
    uint8_t  dummy169[3];                                    
    const uint8_t  CSR4;                                     
    uint8_t  dummy170[3];                                    
    const uint8_t  CSR5;                                     
    uint8_t  dummy171[3];                                    
    const uint8_t  CSR6;                                     
    uint8_t  dummy172[3];                                    
    const uint8_t  CSR7;                                     
    uint8_t  dummy173[3];                                    
    const uint8_t  CSR8;                                     
    uint8_t  dummy174[3];                                    
    const uint8_t  CSR9;                                     
    uint8_t  dummy175[3];                                    
    const uint8_t  CSR10;                                    
    uint8_t  dummy176[3];                                    
    const uint8_t  CSR11;                                    
    uint8_t  dummy177[3];                                    
    const uint8_t  CSR12;                                    
    uint8_t  dummy178[3];                                    
    const uint8_t  CSR13;                                    
    uint8_t  dummy179[3];                                    
    const uint8_t  CSR14;                                    
    uint8_t  dummy180[3];                                    
    const uint8_t  CSR15;                                    
    uint8_t  dummy181[3];                                    
    uint8_t  CSC0;                                           
    uint8_t  dummy182[3];                                    
    uint8_t  CSC1;                                           
    uint8_t  dummy183[3];                                    
    uint8_t  CSC2;                                           
    uint8_t  dummy184[3];                                    
    uint8_t  CSC3;                                           
    uint8_t  dummy185[3];                                    
    uint8_t  CSC4;                                           
    uint8_t  dummy186[3];                                    
    uint8_t  CSC5;                                           
    uint8_t  dummy187[3];                                    
    uint8_t  CSC6;                                           
    uint8_t  dummy188[3];                                    
    uint8_t  CSC7;                                           
    uint8_t  dummy189[3];                                    
    uint8_t  CSC8;                                           
    uint8_t  dummy190[3];                                    
    uint8_t  CSC9;                                           
    uint8_t  dummy191[3];                                    
    uint8_t  CSC10;                                          
    uint8_t  dummy192[3];                                    
    uint8_t  CSC11;                                          
    uint8_t  dummy193[3];                                    
    uint8_t  CSC12;                                          
    uint8_t  dummy194[3];                                    
    uint8_t  CSC13;                                          
    uint8_t  dummy195[3];                                    
    uint8_t  CSC14;                                          
    uint8_t  dummy196[3];                                    
    uint8_t  CSC15;                                          
    uint8_t  dummy197[3];                                    
    const uint16_t TE;                                       
    uint8_t  dummy198[2];                                    
    uint16_t TS;                                             
    uint8_t  dummy199[2];                                    
    uint16_t TT;                                             
    uint8_t  dummy200[54];                                   
    uint16_t CMOR0;                                          
    uint8_t  dummy201[2];                                    
    uint16_t CMOR1;                                          
    uint8_t  dummy202[2];                                    
    uint16_t CMOR2;                                          
    uint8_t  dummy203[2];                                    
    uint16_t CMOR3;                                          
    uint8_t  dummy204[2];                                    
    uint16_t CMOR4;                                          
    uint8_t  dummy205[2];                                    
    uint16_t CMOR5;                                          
    uint8_t  dummy206[2];                                    
    uint16_t CMOR6;                                          
    uint8_t  dummy207[2];                                    
    uint16_t CMOR7;                                          
    uint8_t  dummy208[2];                                    
    uint16_t CMOR8;                                          
    uint8_t  dummy209[2];                                    
    uint16_t CMOR9;                                          
    uint8_t  dummy210[2];                                    
    uint16_t CMOR10;                                         
    uint8_t  dummy211[2];                                    
    uint16_t CMOR11;                                         
    uint8_t  dummy212[2];                                    
    uint16_t CMOR12;                                         
    uint8_t  dummy213[2];                                    
    uint16_t CMOR13;                                         
    uint8_t  dummy214[2];                                    
    uint16_t CMOR14;                                         
    uint8_t  dummy215[2];                                    
    uint16_t CMOR15;                                         
    uint8_t  dummy216[2];                                    
    uint16_t TPS;                                            
    uint8_t  dummy217[2];                                    
    uint8_t  BRS;                                            
    uint8_t  dummy218[3];                                    
    uint16_t TOM;                                            
    uint8_t  dummy219[2];                                    
    uint16_t TOC;                                            
    uint8_t  dummy220[2];                                    
    uint16_t TDE;                                            
    uint8_t  dummy221[2];                                    
    uint16_t TDM;                                            
    uint8_t  dummy222[2];                                    
    uint16_t TRE;                                            
    uint8_t  dummy223[2];                                    
    uint16_t TRC;                                            
    uint8_t  dummy224[2];                                    
    uint16_t RDE;                                            
    uint8_t  dummy225[2];                                    
    uint16_t RDM;                                            
    uint8_t  dummy226[2];                                    
    uint16_t RDS;                                            
    uint8_t  dummy227[2];                                    
    uint16_t RDC;                                            
    uint8_t  dummy228[34];                                   
    uint8_t  EMU;                                            
} __type42;
typedef struct 
{                                                          
    uint16_t CDR0;                                           
    uint8_t  dummy229[2];                                    
    uint16_t CDR1;                                           
    uint8_t  dummy230[2];                                    
    uint16_t CDR2;                                           
    uint8_t  dummy231[2];                                    
    uint16_t CDR3;                                           
    uint8_t  dummy232[2];                                    
    uint16_t CDR4;                                           
    uint8_t  dummy233[2];                                    
    uint16_t CDR5;                                           
    uint8_t  dummy234[2];                                    
    uint16_t CDR6;                                           
    uint8_t  dummy235[2];                                    
    uint16_t CDR7;                                           
    uint8_t  dummy236[2];                                    
    uint16_t CDR8;                                           
    uint8_t  dummy237[2];                                    
    uint16_t CDR9;                                           
    uint8_t  dummy238[2];                                    
    uint16_t CDR10;                                          
    uint8_t  dummy239[2];                                    
    uint16_t CDR11;                                          
    uint8_t  dummy240[2];                                    
    uint16_t CDR12;                                          
    uint8_t  dummy241[2];                                    
    uint16_t CDR13;                                          
    uint8_t  dummy242[2];                                    
    uint16_t CDR14;                                          
    uint8_t  dummy243[2];                                    
    uint16_t CDR15;                                          
    uint8_t  dummy244[2];                                    
    uint16_t TOL;                                            
    uint8_t  dummy245[2];                                    
    uint16_t RDT;                                            
    uint8_t  dummy246[2];                                    
    const uint16_t RSF;                                      
    uint8_t  dummy247[10];                                   
    uint16_t TDL;                                            
    uint8_t  dummy248[2];                                    
    uint16_t TO;                                             
    uint8_t  dummy249[2];                                    
    uint16_t TOE;                                            
    uint8_t  dummy250[34];                                   
    const uint16_t CNT0;                                     
    uint8_t  dummy251[2];                                    
    const uint16_t CNT1;                                     
    uint8_t  dummy252[2];                                    
    const uint16_t CNT2;                                     
    uint8_t  dummy253[2];                                    
    const uint16_t CNT3;                                     
    uint8_t  dummy254[2];                                    
    const uint16_t CNT4;                                     
    uint8_t  dummy255[2];                                    
    const uint16_t CNT5;                                     
    uint8_t  dummy256[2];                                    
    const uint16_t CNT6;                                     
    uint8_t  dummy257[2];                                    
    const uint16_t CNT7;                                     
    uint8_t  dummy258[2];                                    
    const uint16_t CNT8;                                     
    uint8_t  dummy259[2];                                    
    const uint16_t CNT9;                                     
    uint8_t  dummy260[2];                                    
    const uint16_t CNT10;                                    
    uint8_t  dummy261[2];                                    
    const uint16_t CNT11;                                    
    uint8_t  dummy262[2];                                    
    const uint16_t CNT12;                                    
    uint8_t  dummy263[2];                                    
    const uint16_t CNT13;                                    
    uint8_t  dummy264[2];                                    
    const uint16_t CNT14;                                    
    uint8_t  dummy265[2];                                    
    const uint16_t CNT15;                                    
    uint8_t  dummy266[2];                                    
    uint8_t  CMUR0;                                          
    uint8_t  dummy267[3];                                    
    uint8_t  CMUR1;                                          
    uint8_t  dummy268[3];                                    
    uint8_t  CMUR2;                                          
    uint8_t  dummy269[3];                                    
    uint8_t  CMUR3;                                          
    uint8_t  dummy270[3];                                    
    uint8_t  CMUR4;                                          
    uint8_t  dummy271[3];                                    
    uint8_t  CMUR5;                                          
    uint8_t  dummy272[3];                                    
    uint8_t  CMUR6;                                          
    uint8_t  dummy273[3];                                    
    uint8_t  CMUR7;                                          
    uint8_t  dummy274[3];                                    
    uint8_t  CMUR8;                                          
    uint8_t  dummy275[3];                                    
    uint8_t  CMUR9;                                          
    uint8_t  dummy276[3];                                    
    uint8_t  CMUR10;                                         
    uint8_t  dummy277[3];                                    
    uint8_t  CMUR11;                                         
    uint8_t  dummy278[3];                                    
    uint8_t  CMUR12;                                         
    uint8_t  dummy279[3];                                    
    uint8_t  CMUR13;                                         
    uint8_t  dummy280[3];                                    
    uint8_t  CMUR14;                                         
    uint8_t  dummy281[3];                                    
    uint8_t  CMUR15;                                         
    uint8_t  dummy282[67];                                   
    const uint8_t  CSR0;                                     
    uint8_t  dummy283[3];                                    
    const uint8_t  CSR1;                                     
    uint8_t  dummy284[3];                                    
    const uint8_t  CSR2;                                     
    uint8_t  dummy285[3];                                    
    const uint8_t  CSR3;                                     
    uint8_t  dummy286[3];                                    
    const uint8_t  CSR4;                                     
    uint8_t  dummy287[3];                                    
    const uint8_t  CSR5;                                     
    uint8_t  dummy288[3];                                    
    const uint8_t  CSR6;                                     
    uint8_t  dummy289[3];                                    
    const uint8_t  CSR7;                                     
    uint8_t  dummy290[3];                                    
    const uint8_t  CSR8;                                     
    uint8_t  dummy291[3];                                    
    const uint8_t  CSR9;                                     
    uint8_t  dummy292[3];                                    
    const uint8_t  CSR10;                                    
    uint8_t  dummy293[3];                                    
    const uint8_t  CSR11;                                    
    uint8_t  dummy294[3];                                    
    const uint8_t  CSR12;                                    
    uint8_t  dummy295[3];                                    
    const uint8_t  CSR13;                                    
    uint8_t  dummy296[3];                                    
    const uint8_t  CSR14;                                    
    uint8_t  dummy297[3];                                    
    const uint8_t  CSR15;                                    
    uint8_t  dummy298[3];                                    
    uint8_t  CSC0;                                           
    uint8_t  dummy299[3];                                    
    uint8_t  CSC1;                                           
    uint8_t  dummy300[3];                                    
    uint8_t  CSC2;                                           
    uint8_t  dummy301[3];                                    
    uint8_t  CSC3;                                           
    uint8_t  dummy302[3];                                    
    uint8_t  CSC4;                                           
    uint8_t  dummy303[3];                                    
    uint8_t  CSC5;                                           
    uint8_t  dummy304[3];                                    
    uint8_t  CSC6;                                           
    uint8_t  dummy305[3];                                    
    uint8_t  CSC7;                                           
    uint8_t  dummy306[3];                                    
    uint8_t  CSC8;                                           
    uint8_t  dummy307[3];                                    
    uint8_t  CSC9;                                           
    uint8_t  dummy308[3];                                    
    uint8_t  CSC10;                                          
    uint8_t  dummy309[3];                                    
    uint8_t  CSC11;                                          
    uint8_t  dummy310[3];                                    
    uint8_t  CSC12;                                          
    uint8_t  dummy311[3];                                    
    uint8_t  CSC13;                                          
    uint8_t  dummy312[3];                                    
    uint8_t  CSC14;                                          
    uint8_t  dummy313[3];                                    
    uint8_t  CSC15;                                          
    uint8_t  dummy314[3];                                    
    const uint16_t TE;                                       
    uint8_t  dummy315[2];                                    
    uint16_t TS;                                             
    uint8_t  dummy316[2];                                    
    uint16_t TT;                                             
    uint8_t  dummy317[54];                                   
    uint16_t CMOR0;                                          
    uint8_t  dummy318[2];                                    
    uint16_t CMOR1;                                          
    uint8_t  dummy319[2];                                    
    uint16_t CMOR2;                                          
    uint8_t  dummy320[2];                                    
    uint16_t CMOR3;                                          
    uint8_t  dummy321[2];                                    
    uint16_t CMOR4;                                          
    uint8_t  dummy322[2];                                    
    uint16_t CMOR5;                                          
    uint8_t  dummy323[2];                                    
    uint16_t CMOR6;                                          
    uint8_t  dummy324[2];                                    
    uint16_t CMOR7;                                          
    uint8_t  dummy325[2];                                    
    uint16_t CMOR8;                                          
    uint8_t  dummy326[2];                                    
    uint16_t CMOR9;                                          
    uint8_t  dummy327[2];                                    
    uint16_t CMOR10;                                         
    uint8_t  dummy328[2];                                    
    uint16_t CMOR11;                                         
    uint8_t  dummy329[2];                                    
    uint16_t CMOR12;                                         
    uint8_t  dummy330[2];                                    
    uint16_t CMOR13;                                         
    uint8_t  dummy331[2];                                    
    uint16_t CMOR14;                                         
    uint8_t  dummy332[2];                                    
    uint16_t CMOR15;                                         
    uint8_t  dummy333[2];                                    
    uint16_t TPS;                                            
    uint8_t  dummy334[6];                                    
    uint16_t TOM;                                            
    uint8_t  dummy335[2];                                    
    uint16_t TOC;                                            
    uint8_t  dummy336[2];                                    
    uint16_t TDE;                                            
    uint8_t  dummy337[14];                                   
    uint16_t RDE;                                            
    uint8_t  dummy338[2];                                    
    uint16_t RDM;                                            
    uint8_t  dummy339[2];                                    
    uint16_t RDS;                                            
    uint8_t  dummy340[2];                                    
    uint16_t RDC;                                            
    uint8_t  dummy341[34];                                   
    uint8_t  EMU;                                            
    uint8_t  dummy342[3439];                                 
} __type43;
typedef struct 
{                                                          
    uint32_t CDR0;                                           
    uint32_t CDR1;                                           
    uint32_t CDR2;                                           
    uint32_t CDR3;                                           
    const uint32_t CNT0;                                     
    const uint32_t CNT1;                                     
    const uint32_t CNT2;                                     
    const uint32_t CNT3;                                     
    uint8_t  CMUR0;                                          
    uint8_t  dummy343[3];                                    
    uint8_t  CMUR1;                                          
    uint8_t  dummy344[3];                                    
    uint8_t  CMUR2;                                          
    uint8_t  dummy345[3];                                    
    uint8_t  CMUR3;                                          
    uint8_t  dummy346[3];                                    
    const uint8_t  CSR0;                                     
    uint8_t  dummy347[3];                                    
    const uint8_t  CSR1;                                     
    uint8_t  dummy348[3];                                    
    const uint8_t  CSR2;                                     
    uint8_t  dummy349[3];                                    
    const uint8_t  CSR3;                                     
    uint8_t  dummy350[3];                                    
    uint8_t  CSC0;                                           
    uint8_t  dummy351[3];                                    
    uint8_t  CSC1;                                           
    uint8_t  dummy352[3];                                    
    uint8_t  CSC2;                                           
    uint8_t  dummy353[3];                                    
    uint8_t  CSC3;                                           
    uint8_t  dummy354[3];                                    
    const uint8_t  TE;                                       
    uint8_t  dummy355[3];                                    
    uint8_t  TS;                                             
    uint8_t  dummy356[3];                                    
    uint8_t  TT;                                             
    uint8_t  dummy357[3];                                    
    uint8_t  TO;                                             
    uint8_t  dummy358[3];                                    
    uint8_t  TOE;                                            
    uint8_t  dummy359[3];                                    
    uint8_t  TOL;                                            
    uint8_t  dummy360[3];                                    
    uint8_t  RDT;                                            
    uint8_t  dummy361[3];                                    
    const uint8_t  RSF;                                      
    uint8_t  dummy362[19];                                   
    uint16_t CMOR0;                                          
    uint8_t  dummy363[2];                                    
    uint16_t CMOR1;                                          
    uint8_t  dummy364[2];                                    
    uint16_t CMOR2;                                          
    uint8_t  dummy365[2];                                    
    uint16_t CMOR3;                                          
    uint8_t  dummy366[2];                                    
    uint16_t TPS;                                            
    uint8_t  dummy367[2];                                    
    uint8_t  BRS;                                            
    uint8_t  dummy368[3];                                    
    uint8_t  TOM;                                            
    uint8_t  dummy369[3];                                    
    uint8_t  TOC;                                            
    uint8_t  dummy370[3];                                    
    uint8_t  RDE;                                            
    uint8_t  dummy371[3];                                    
    uint8_t  RDM;                                            
    uint8_t  dummy372[3];                                    
    uint8_t  EMU;                                            
    uint8_t  dummy373[3927];                                 
} __type44;
typedef struct 
{                                                          
    uint8_t  CTL;                                            
    uint8_t  dummy374[3];                                    
    const uint8_t  STR;                                      
    uint8_t  dummy375[3];                                    
    uint8_t  STC;                                            
    uint8_t  dummy376[3];                                    
    uint8_t  EMU;                                            
    uint8_t  dummy377[19];                                   
    const uint8_t  QUE0;                                     
    uint8_t  dummy378[3];                                    
    const uint8_t  QUE1;                                     
    uint8_t  dummy379[3];                                    
    const uint8_t  QUE2;                                     
    uint8_t  dummy380[3];                                    
    const uint8_t  QUE3;                                     
    uint8_t  dummy381[3];                                    
    const uint8_t  QUE4;                                     
    uint8_t  dummy382[3];                                    
    const uint8_t  QUE5;                                     
    uint8_t  dummy383[3];                                    
    const uint8_t  QUE6;                                     
    uint8_t  dummy384[3];                                    
    const uint8_t  QUE7;                                     
    uint8_t  dummy385[3];                                    
    uint32_t PVCR00_01;                                      
    uint32_t PVCR02_03;                                      
    uint32_t PVCR04_05;                                      
    uint32_t PVCR06_07;                                      
    uint32_t PVCR08_09;                                      
    uint32_t PVCR10_11;                                      
    uint32_t PVCR12_13;                                      
    uint32_t PVCR14_15;                                      
    uint32_t PVCR16_17;                                      
    uint32_t PVCR18_19;                                      
    uint32_t PVCR20_21;                                      
    uint32_t PVCR22_23;                                      
    uint32_t PVCR24_25;                                      
    uint32_t PVCR26_27;                                      
    uint32_t PVCR28_29;                                      
    uint32_t PVCR30_31;                                      
    uint32_t PVCR32_33;                                      
    uint32_t PVCR34_35;                                      
    uint32_t PVCR36_37;                                      
    uint32_t PVCR38_39;                                      
    uint32_t PVCR40_41;                                      
    uint32_t PVCR42_43;                                      
    uint32_t PVCR44_45;                                      
    uint32_t PVCR46_47;                                      
    uint32_t PVCR48_49;                                      
    uint32_t PVCR50_51;                                      
    uint32_t PVCR52_53;                                      
    uint32_t PVCR54_55;                                      
    uint32_t PVCR56_57;                                      
    uint32_t PVCR58_59;                                      
    uint32_t PVCR60_61;                                      
    uint32_t PVCR62_63;                                      
    uint32_t PVCR64_65;                                      
    uint32_t PVCR66_67;                                      
    uint32_t PVCR68_69;                                      
    uint32_t PVCR70_71;                                      
} __type45;
typedef struct 
{                                                          
    uint16_t CSDR;                                           
    uint8_t  dummy386[2];                                    
    uint16_t CRDR;                                           
    uint8_t  dummy387[2];                                    
    uint16_t CTDR;                                           
    uint8_t  dummy388[2];                                    
    uint8_t  RDT;                                            
    uint8_t  dummy389[3];                                    
    const uint8_t  RSF;                                      
    uint8_t  dummy390[3];                                    
    const uint16_t CNT;                                      
    uint8_t  dummy391[10];                                   
    uint8_t  CTL;                                            
    uint8_t  dummy392[3];                                    
    const uint16_t CSBR;                                     
    uint8_t  dummy393[2];                                    
    const uint16_t CRBR;                                     
    uint8_t  dummy394[2];                                    
    const uint16_t CTBR;                                     
    uint8_t  dummy395[18];                                   
} __type46;
typedef struct 
{                                                          
    uint16_t BRS0;                                           
    uint8_t  dummy396[2];                                    
    uint16_t BRS1;                                           
    uint8_t  dummy397[2];                                    
    uint16_t BRS2;                                           
    uint8_t  dummy398[2];                                    
    uint16_t BRS3;                                           
    uint8_t  dummy399[2];                                    
    const uint8_t  TE;                                       
    uint8_t  dummy400[3];                                    
    uint8_t  TS;                                             
    uint8_t  dummy401[3];                                    
    uint8_t  TT;                                             
    uint8_t  dummy402[3];                                    
    uint8_t  EMU;                                            
} __type47;
typedef struct 
{                                                          
    __type20 CTL0;                                         
    uint8_t  dummy403[3];                                    
    __type21 CTL1;                                         
    uint8_t  dummy404[3];                                    
    __type22 CTL2;                                         
    uint8_t  dummy405[3];                                    
    const uint32_t SUBC;                                     
    const uint32_t SRBU;                                     
    uint8_t  SEC;                                            
    uint8_t  dummy406[3];                                    
    uint8_t  MIN;                                            
    uint8_t  dummy407[3];                                    
    uint8_t  HOUR;                                           
    uint8_t  dummy408[3];                                    
    uint8_t  WEEK;                                           
    uint8_t  dummy409[3];                                    
    uint8_t  DAY;                                            
    uint8_t  dummy410[3];                                    
    uint8_t  MONTH;                                          
    uint8_t  dummy411[3];                                    
    uint8_t  YEAR;                                           
    uint8_t  dummy412[3];                                    
    uint32_t TIME;                                           
    uint32_t CAL;                                            
    uint8_t  SUBU;                                           
    uint8_t  dummy413[3];                                    
    uint32_t SCMP;                                           
    uint8_t  ALM;                                            
    uint8_t  dummy414[3];                                    
    uint8_t  ALH;                                            
    uint8_t  dummy415[3];                                    
    uint8_t  ALW;                                            
    uint8_t  dummy416[3];                                    
    const uint8_t  SECC;                                     
    uint8_t  dummy417[3];                                    
    const uint8_t  MINC;                                     
    uint8_t  dummy418[3];                                    
    const uint8_t  HOURC;                                    
    uint8_t  dummy419[3];                                    
    const uint8_t  WEEKC;                                    
    uint8_t  dummy420[3];                                    
    const uint8_t  DAYC;                                     
    uint8_t  dummy421[3];                                    
    const uint8_t  MONC;                                     
    uint8_t  dummy422[3];                                    
    const uint8_t  YEARC;                                    
    uint8_t  dummy423[3];                                    
    const uint32_t TIMEC;                                    
    const uint32_t CALC;                                     
    uint8_t  dummy424[4];                                    
    __type17 EMU;                                          
} __type48;
typedef struct 
{                                                          
    uint16_t CCR0;                                           
    uint8_t  dummy425[2];                                    
    uint16_t CCR1;                                           
    uint8_t  dummy426[2];                                    
    uint16_t CNT;                                            
    uint8_t  dummy427[2];                                    
    const uint8_t  FLG;                                      
    uint8_t  dummy428[3];                                    
    uint8_t  FGC;                                            
    uint8_t  dummy429[3];                                    
    const uint8_t  TE;                                       
    uint8_t  dummy430[3];                                    
    uint8_t  TS;                                             
    uint8_t  dummy431[3];                                    
    uint8_t  TT;                                             
    uint8_t  dummy432[3];                                    
    uint8_t  IOC0;                                           
    uint8_t  dummy433[31];                                   
    uint16_t CTL;                                            
    uint8_t  dummy434[2];                                    
    uint8_t  IOC1;                                           
    uint8_t  dummy435[3];                                    
    uint8_t  EMU;                                            
} __type49;
typedef struct 
{                                                          
    const uint16_t FLG;                                      
    uint8_t  dummy436[2];                                    
    uint8_t  ACWE;                                           
    uint8_t  dummy437[3];                                    
    uint8_t  ACTS;                                           
    uint8_t  dummy438[3];                                    
    uint8_t  ACTT;                                           
    uint8_t  dummy439[7];                                    
    uint8_t  OPHS;                                           
    uint8_t  dummy440[3];                                    
    uint8_t  OPHT;                                           
    uint8_t  dummy441[7];                                    
    uint16_t CTL0;                                           
    uint8_t  dummy442[2];                                    
    uint8_t  CTL1;                                           
    uint8_t  dummy443[3];                                    
    uint8_t  EMU;                                            
} __type50;
typedef struct 
{                                                          
    uint32_t CMP;                                            
    const uint32_t CNT;                                      
    uint8_t  dummy444[8];                                    
    const uint8_t  TE;                                       
    uint8_t  dummy445[3];                                    
    uint8_t  TS;                                             
    uint8_t  dummy446[3];                                    
    uint8_t  TT;                                             
    uint8_t  dummy447[7];                                    
    uint8_t  CTL;                                            
    uint8_t  dummy448[3];                                    
    uint8_t  EMU;                                            
} __type51;
typedef struct 
{                                                          
    uint8_t  WDTE;                                           
    uint8_t  dummy449[3];                                    
    uint8_t  EVAC;                                           
    uint8_t  dummy450[3];                                    
    const uint8_t  REF;                                      
    uint8_t  dummy451[3];                                    
    uint8_t  MD;                                             
    uint8_t  dummy452[4083];                                 
} __type52;
typedef struct 
{                                                          
    __type10 VCR00;                                        
    __type10 VCR01;                                        
    __type10 VCR02;                                        
    __type10 VCR03;                                        
    __type10 VCR04;                                        
    __type10 VCR05;                                        
    __type10 VCR06;                                        
    __type10 VCR07;                                        
    __type10 VCR08;                                        
    __type10 VCR09;                                        
    __type10 VCR10;                                        
    __type10 VCR11;                                        
    __type10 VCR12;                                        
    __type10 VCR13;                                        
    __type10 VCR14;                                        
    __type10 VCR15;                                        
    __type10 VCR16;                                        
    __type10 VCR17;                                        
    __type10 VCR18;                                        
    __type10 VCR19;                                        
    __type10 VCR20;                                        
    __type10 VCR21;                                        
    __type10 VCR22;                                        
    __type10 VCR23;                                        
    __type10 VCR24;                                        
    __type10 VCR25;                                        
    __type10 VCR26;                                        
    __type10 VCR27;                                        
    __type10 VCR28;                                        
    __type10 VCR29;                                        
    __type10 VCR30;                                        
    __type10 VCR31;                                        
    __type10 VCR32;                                        
    __type10 VCR33;                                        
    __type10 VCR34;                                        
    __type10 VCR35;                                        
    __type10 VCR36;                                        
    __type10 VCR37;                                        
    __type10 VCR38;                                        
    __type10 VCR39;                                        
    __type10 VCR40;                                        
    __type10 VCR41;                                        
    __type10 VCR42;                                        
    __type10 VCR43;                                        
    __type10 VCR44;                                        
    __type10 VCR45;                                        
    __type10 VCR46;                                        
    __type10 VCR47;                                        
    __type10 VCR48;                                        
    __type10 VCR49;                                        
    uint8_t  dummy453[44];                                   
    const __type10 PWDVCR;                                 
    uint8_t  dummy454[8];                                    
    const __type15 DR00;                                   
    const __type15 DR02;                                   
    const __type15 DR04;                                   
    const __type15 DR06;                                   
    const __type15 DR08;                                   
    const __type15 DR10;                                   
    const __type15 DR12;                                   
    const __type15 DR14;                                   
    const __type15 DR16;                                   
    const __type15 DR18;                                   
    const __type15 DR20;                                   
    const __type15 DR22;                                   
    const __type15 DR24;                                   
    const __type15 DR26;                                   
    const __type15 DR28;                                   
    const __type15 DR30;                                   
    const __type15 DR32;                                   
    const __type15 DR34;                                   
    const __type15 DR36;                                   
    const __type15 DR38;                                   
    const __type15 DR40;                                   
    const __type15 DR42;                                   
    const __type15 DR44;                                   
    const __type15 DR46;                                   
    const __type15 DR48;                                   
    uint8_t  dummy455[20];                                   
    const __type15 PWDTSNDR;                               
    uint8_t  dummy456[132];                                  
    const uint32_t DIR00;                                    
    const uint32_t DIR01;                                    
    const uint32_t DIR02;                                    
    const uint32_t DIR03;                                    
    const uint32_t DIR04;                                    
    const uint32_t DIR05;                                    
    const uint32_t DIR06;                                    
    const uint32_t DIR07;                                    
    const uint32_t DIR08;                                    
    const uint32_t DIR09;                                    
    const uint32_t DIR10;                                    
    const uint32_t DIR11;                                    
    const uint32_t DIR12;                                    
    const uint32_t DIR13;                                    
    const uint32_t DIR14;                                    
    const uint32_t DIR15;                                    
    const uint32_t DIR16;                                    
    const uint32_t DIR17;                                    
    const uint32_t DIR18;                                    
    const uint32_t DIR19;                                    
    const uint32_t DIR20;                                    
    const uint32_t DIR21;                                    
    const uint32_t DIR22;                                    
    const uint32_t DIR23;                                    
    const uint32_t DIR24;                                    
    const uint32_t DIR25;                                    
    const uint32_t DIR26;                                    
    const uint32_t DIR27;                                    
    const uint32_t DIR28;                                    
    const uint32_t DIR29;                                    
    const uint32_t DIR30;                                    
    const uint32_t DIR31;                                    
    const uint32_t DIR32;                                    
    const uint32_t DIR33;                                    
    const uint32_t DIR34;                                    
    const uint32_t DIR35;                                    
    const uint32_t DIR36;                                    
    const uint32_t DIR37;                                    
    const uint32_t DIR38;                                    
    const uint32_t DIR39;                                    
    const uint32_t DIR40;                                    
    const uint32_t DIR41;                                    
    const uint32_t DIR42;                                    
    const uint32_t DIR43;                                    
    const uint32_t DIR44;                                    
    const uint32_t DIR45;                                    
    const uint32_t DIR46;                                    
    const uint32_t DIR47;                                    
    const uint32_t DIR48;                                    
    const uint32_t DIR49;                                    
    uint8_t  dummy457[44];                                   
    const uint32_t PWDDIR;                                   
    uint8_t  dummy458[8];                                    
    __type10 ADHALTR;                                      
    __type10 ADCR;                                         
    const __type15 SGSTR;                                  
    const __type10 MPXCURR;                                
    uint8_t  dummy459[4];                                    
    __type10 THSMPSTCR;                                    
    __type10 THCR;                                         
    __type10 THAHLDSTCR;                                   
    __type10 THBHLDSTCR;                                   
    __type10 THACR;                                        
    __type10 THBCR;                                        
    __type10 THER;                                         
    __type10 THGSR;                                        
    __type10 SFTCR;                                        
    __type15 ULLMTBR0;                                     
    __type15 ULLMTBR1;                                     
    __type15 ULLMTBR2;                                     
    __type10 ECR;                                          
    const __type10 ULER;                                   
    const __type10 OWER;                                   
    __type10 DGCTL0;                                       
    __type15 DGCTL1;                                       
    __type15 PDCTL1;                                       
    __type10 PDCTL2;                                       
    uint8_t  dummy460[32];                                   
    __type10 SMPCR;                                        
    uint8_t  dummy461[4];                                    
    uint8_t  EMU;                                            
    uint8_t  dummy462[183];                                  
    __type10 SGSTCR1;                                      
    uint8_t  dummy463[4];                                    
    __type10 SGCR1;                                        
    __type10 SGVCSP1;                                      
    __type10 SGVCEP1;                                      
    __type10 SGMCYCR1;                                     
    __type10 SGSEFCR1;                                     
    __type15 SGTSEL1;                                      
    uint8_t  dummy464[32];                                   
    __type10 SGSTCR2;                                      
    uint8_t  dummy465[4];                                    
    __type10 SGCR2;                                        
    __type10 SGVCSP2;                                      
    __type10 SGVCEP2;                                      
    __type10 SGMCYCR2;                                     
    __type10 SGSEFCR2;                                     
    __type15 SGTSEL2;                                      
    uint8_t  dummy466[32];                                   
    __type10 SGSTCR3;                                      
    uint8_t  dummy467[4];                                    
    __type10 SGCR3;                                        
    __type10 SGVCSP3;                                      
    __type10 SGVCEP3;                                      
    __type10 SGMCYCR3;                                     
    __type10 SGSEFCR3;                                     
    __type15 SGTSEL3;                                      
    uint8_t  dummy468[40];                                   
    __type10 PWDSGCR;                                      
    uint8_t  dummy469[12];                                   
    __type10 PWDSGSEFCR;                                   
} __type53;
typedef struct 
{                                                          
    __type10 VCR00;                                        
    __type10 VCR01;                                        
    __type10 VCR02;                                        
    __type10 VCR03;                                        
    __type10 VCR04;                                        
    __type10 VCR05;                                        
    __type10 VCR06;                                        
    __type10 VCR07;                                        
    __type10 VCR08;                                        
    __type10 VCR09;                                        
    __type10 VCR10;                                        
    __type10 VCR11;                                        
    __type10 VCR12;                                        
    __type10 VCR13;                                        
    __type10 VCR14;                                        
    __type10 VCR15;                                        
    __type10 VCR16;                                        
    __type10 VCR17;                                        
    __type10 VCR18;                                        
    __type10 VCR19;                                        
    __type10 VCR20;                                        
    __type10 VCR21;                                        
    __type10 VCR22;                                        
    __type10 VCR23;                                        
    uint8_t  dummy470[148];                                  
    const __type10 PWDVCR;                                 
    uint8_t  dummy471[8];                                    
    const __type15 DR00;                                   
    const __type15 DR02;                                   
    const __type15 DR04;                                   
    const __type15 DR06;                                   
    const __type15 DR08;                                   
    const __type15 DR10;                                   
    const __type15 DR12;                                   
    const __type15 DR14;                                   
    const __type15 DR16;                                   
    const __type15 DR18;                                   
    const __type15 DR20;                                   
    const __type15 DR22;                                   
    uint8_t  dummy472[72];                                   
    const __type15 PWDTSNDR;                               
    uint8_t  dummy473[132];                                  
    const uint32_t DIR00;                                    
    const uint32_t DIR01;                                    
    const uint32_t DIR02;                                    
    const uint32_t DIR03;                                    
    const uint32_t DIR04;                                    
    const uint32_t DIR05;                                    
    const uint32_t DIR06;                                    
    const uint32_t DIR07;                                    
    const uint32_t DIR08;                                    
    const uint32_t DIR09;                                    
    const uint32_t DIR10;                                    
    const uint32_t DIR11;                                    
    const uint32_t DIR12;                                    
    const uint32_t DIR13;                                    
    const uint32_t DIR14;                                    
    const uint32_t DIR15;                                    
    const uint32_t DIR16;                                    
    const uint32_t DIR17;                                    
    const uint32_t DIR18;                                    
    const uint32_t DIR19;                                    
    const uint32_t DIR20;                                    
    const uint32_t DIR21;                                    
    const uint32_t DIR22;                                    
    const uint32_t DIR23;                                    
    uint8_t  dummy474[148];                                  
    const uint32_t PWDDIR;                                   
    uint8_t  dummy475[8];                                    
    __type10 ADHALTR;                                      
    __type10 ADCR;                                         
    const __type15 SGSTR;                                  
    uint8_t  dummy476[40];                                   
    __type10 SFTCR;                                        
    __type15 ULLMTBR0;                                     
    __type15 ULLMTBR1;                                     
    __type15 ULLMTBR2;                                     
    __type10 ECR;                                          
    const __type10 ULER;                                   
    const __type10 OWER;                                   
    __type10 DGCTL0;                                       
    __type15 DGCTL1;                                       
    __type15 PDCTL1;                                       
    __type10 PDCTL2;                                       
    uint8_t  dummy477[32];                                   
    __type10 SMPCR;                                        
    uint8_t  dummy478[4];                                    
    uint8_t  EMU;                                            
    uint8_t  dummy479[183];                                  
    __type10 SGSTCR1;                                      
    uint8_t  dummy480[4];                                    
    __type10 SGCR1;                                        
    __type10 SGVCSP1;                                      
    __type10 SGVCEP1;                                      
    __type10 SGMCYCR1;                                     
    __type10 SGSEFCR1;                                     
    __type15 SGTSEL1;                                      
    uint8_t  dummy481[32];                                   
    __type10 SGSTCR2;                                      
    uint8_t  dummy482[4];                                    
    __type10 SGCR2;                                        
    __type10 SGVCSP2;                                      
    __type10 SGVCEP2;                                      
    __type10 SGMCYCR2;                                     
    __type10 SGSEFCR2;                                     
    __type15 SGTSEL2;                                      
    uint8_t  dummy483[32];                                   
    __type10 SGSTCR3;                                      
    uint8_t  dummy484[4];                                    
    __type10 SGCR3;                                        
    __type10 SGVCSP3;                                      
    __type10 SGVCEP3;                                      
    __type10 SGMCYCR3;                                     
    __type10 SGSEFCR3;                                     
    __type15 SGTSEL3;                                      
    uint8_t  dummy485[40];                                   
    __type10 PWDSGCR;                                      
    uint8_t  dummy486[12];                                   
    __type10 PWDSGSEFCR;                                   
} __type54;
typedef struct 
{                                                          
    uint32_t CIN;                                            
    uint32_t COUT;                                           
    uint8_t  dummy487[24];                                   
    uint8_t  CTL;                                            
    uint8_t  dummy488[4063];                                 
} __type55;
typedef struct 
{                                                          
    __type23 KRM;                                          
} __type56;
typedef struct 
{                                                          
    uint8_t  CTL0;                                           
    uint8_t  dummy489[7];                                    
    uint16_t CMPL;                                           
    uint8_t  dummy490[2];                                    
    uint16_t CMPH;                                           
    uint8_t  dummy491[2];                                    
    uint8_t  PCMD;                                           
    uint8_t  dummy492[3];                                    
    const uint8_t  PS;                                       
    uint8_t  dummy493[3];                                    
    uint8_t  EMU0;                                           
} __type57;
typedef struct 
{                                                          
    uint32_t TEST;                                           
    const uint32_t TESTS;                                    
} __type58;


__IOREG(FLMDCNT, 0xFFA00000, __READ_WRITE, uint32_t);
__IOREG(FLMDPCMD, 0xFFA00004, __READ_WRITE, uint32_t);
__IOREG(FLMDPS, 0xFFA00008, __READ, uint32_t);
__IOREG(SELB_TAUJ0I, 0xFFBC0100, __READ_WRITE, uint8_t);
__IOREG(SELB_TAUD0I, 0xFFBC0200, __READ_WRITE, uint16_t);
__IOREG(SELB_INTC1, 0xFFBC0300, __READ_WRITE, uint16_t);
__IOREG(SELB_INTC2, 0xFFBC0304, __READ_WRITE, uint16_t);
__IOREG(SELB_READTEST, 0xFFBC0600, __READ_WRITE, uint8_t);
__IOREG(SLPWGA0, 0xFFBC1000, __READ_WRITE, uint32_t);
__IOREG(SLPWGA1, 0xFFBC1004, __READ_WRITE, uint32_t);
__IOREG(SLPWGA2, 0xFFBC1008, __READ_WRITE, uint32_t);
__IOREG(SCTLR, 0xFFBC2000, __READ_WRITE, uint32_t);
__IOREG(EVFR, 0xFFBC2004, __READ_WRITE, uint32_t);
__IOREG(DPSELR0, 0xFFBC2008, __READ_WRITE, uint32_t);
__IOREG(DPSELRM, 0xFFBC200C, __READ_WRITE, uint32_t);
__IOREG(DPSELRML, 0xFFBC200C, __READ_WRITE, uint16_t);
__IOREG(DPSELR1, 0xFFBC200C, __READ_WRITE, uint8_t);
__IOREG(DPSELR2, 0xFFBC200D, __READ_WRITE, uint8_t);
__IOREG(DPSELRMH, 0xFFBC200E, __READ_WRITE, uint16_t);
__IOREG(DPSELR3, 0xFFBC200E, __READ_WRITE, uint8_t);
__IOREG(DPSELR4, 0xFFBC200F, __READ_WRITE, uint8_t);
__IOREG(DPSELRH, 0xFFBC2010, __READ_WRITE, uint32_t);
__IOREG(DPSELRHL, 0xFFBC2010, __READ_WRITE, uint16_t);
__IOREG(DPSELR5, 0xFFBC2010, __READ_WRITE, uint8_t);
__IOREG(DPSELR6, 0xFFBC2011, __READ_WRITE, uint8_t);
__IOREG(DPSELRHH, 0xFFBC2012, __READ_WRITE, uint16_t);
__IOREG(DPSELR7, 0xFFBC2012, __READ_WRITE, uint8_t);
__IOREG(DPDSR0, 0xFFBC2014, __READ_WRITE, uint32_t);
__IOREG(DPDSRM, 0xFFBC2018, __READ_WRITE, uint32_t);
__IOREG(DPDSRML, 0xFFBC2018, __READ_WRITE, uint16_t);
__IOREG(DPDSR1, 0xFFBC2018, __READ_WRITE, uint8_t);
__IOREG(DPDSR2, 0xFFBC2019, __READ_WRITE, uint8_t);
__IOREG(DPDSRMH, 0xFFBC201A, __READ_WRITE, uint16_t);
__IOREG(DPDSR3, 0xFFBC201A, __READ_WRITE, uint8_t);
__IOREG(DPDSR4, 0xFFBC201B, __READ_WRITE, uint8_t);
__IOREG(DPDSRH, 0xFFBC201C, __READ_WRITE, uint32_t);
__IOREG(DPDSRHL, 0xFFBC201C, __READ_WRITE, uint16_t);
__IOREG(DPDSR5, 0xFFBC201C, __READ_WRITE, uint8_t);
__IOREG(DPDSR6, 0xFFBC201D, __READ_WRITE, uint8_t);
__IOREG(DPDSRHH, 0xFFBC201E, __READ_WRITE, uint16_t);
__IOREG(DPDSR7, 0xFFBC201E, __READ_WRITE, uint8_t);
__IOREG(DPDIMR0, 0xFFBC2020, __READ, uint32_t);
__IOREG(DPDIMR1, 0xFFBC2024, __READ, uint8_t);
__IOREG(DPDIMR2, 0xFFBC2028, __READ, uint8_t);
__IOREG(DPDIMR3, 0xFFBC202C, __READ, uint8_t);
__IOREG(DPDIMR4, 0xFFBC2030, __READ, uint8_t);
__IOREG(DPDIMR5, 0xFFBC2034, __READ, uint8_t);
__IOREG(DPDIMR6, 0xFFBC2038, __READ, uint8_t);
__IOREG(DPDIMR7, 0xFFBC203C, __READ, uint8_t);
__IOREG(CNTVAL, 0xFFBC2040, __READ_WRITE, uint16_t);
__IOREG(SOSTR, 0xFFBC2044, __READ, uint8_t);
__IOREG(P0, 0xFFC10000, __READ_WRITE, uint16_t);
__IOREG(P1, 0xFFC10004, __READ_WRITE, uint16_t);
__IOREG(P2, 0xFFC10008, __READ_WRITE, uint16_t);
__IOREG(P8, 0xFFC10020, __READ_WRITE, uint16_t);
__IOREG(P9, 0xFFC10024, __READ_WRITE, uint16_t);
__IOREG(P10, 0xFFC10028, __READ_WRITE, uint16_t);
__IOREG(P11, 0xFFC1002C, __READ_WRITE, uint16_t);
__IOREG(P12, 0xFFC10030, __READ_WRITE, uint16_t);
__IOREG(P18, 0xFFC10048, __READ_WRITE, uint16_t);
__IOREG(P20, 0xFFC10050, __READ_WRITE, uint16_t);
__IOREG(AP0, 0xFFC100C8, __READ_WRITE, uint16_t);
__IOREG(AP1, 0xFFC100CC, __READ_WRITE, uint16_t);
__IOREG(PSR0, 0xFFC10100, __READ_WRITE, uint32_t);
__IOREG(PSR1, 0xFFC10104, __READ_WRITE, uint32_t);
__IOREG(PSR2, 0xFFC10108, __READ_WRITE, uint32_t);
__IOREG(PSR8, 0xFFC10120, __READ_WRITE, uint32_t);
__IOREG(PSR9, 0xFFC10124, __READ_WRITE, uint32_t);
__IOREG(PSR10, 0xFFC10128, __READ_WRITE, uint32_t);
__IOREG(PSR11, 0xFFC1012C, __READ_WRITE, uint32_t);
__IOREG(PSR12, 0xFFC10130, __READ_WRITE, uint32_t);
__IOREG(PSR18, 0xFFC10148, __READ_WRITE, uint32_t);
__IOREG(PSR20, 0xFFC10150, __READ_WRITE, uint32_t);
__IOREG(APSR0, 0xFFC101C8, __READ_WRITE, uint32_t);
__IOREG(APSR1, 0xFFC101CC, __READ_WRITE, uint32_t);
__IOREG(PPR0, 0xFFC10200, __READ, uint16_t);
__IOREG(PPR1, 0xFFC10204, __READ, uint16_t);
__IOREG(PPR2, 0xFFC10208, __READ, uint16_t);
__IOREG(PPR8, 0xFFC10220, __READ, uint16_t);
__IOREG(PPR9, 0xFFC10224, __READ, uint16_t);
__IOREG(PPR10, 0xFFC10228, __READ, uint16_t);
__IOREG(PPR11, 0xFFC1022C, __READ, uint16_t);
__IOREG(PPR12, 0xFFC10230, __READ, uint16_t);
__IOREG(PPR18, 0xFFC10248, __READ, uint16_t);
__IOREG(PPR20, 0xFFC10250, __READ, uint16_t);
__IOREG(APPR0, 0xFFC102C8, __READ, uint16_t);
__IOREG(APPR1, 0xFFC102CC, __READ, uint16_t);
__IOREG(IPPR0, 0xFFC102F0, __READ, uint16_t);
__IOREG(PM0, 0xFFC10300, __READ_WRITE, uint16_t);
__IOREG(PM1, 0xFFC10304, __READ_WRITE, uint16_t);
__IOREG(PM2, 0xFFC10308, __READ_WRITE, uint16_t);
__IOREG(PM8, 0xFFC10320, __READ_WRITE, uint16_t);
__IOREG(PM9, 0xFFC10324, __READ_WRITE, uint16_t);
__IOREG(PM10, 0xFFC10328, __READ_WRITE, uint16_t);
__IOREG(PM11, 0xFFC1032C, __READ_WRITE, uint16_t);
__IOREG(PM12, 0xFFC10330, __READ_WRITE, uint16_t);
__IOREG(PM18, 0xFFC10348, __READ_WRITE, uint16_t);
__IOREG(PM20, 0xFFC10350, __READ_WRITE, uint16_t);
__IOREG(APM0, 0xFFC103C8, __READ_WRITE, uint16_t);
__IOREG(APM1, 0xFFC103CC, __READ_WRITE, uint16_t);
__IOREG(PMC0, 0xFFC10400, __READ_WRITE, uint16_t);
__IOREG(PMC1, 0xFFC10404, __READ_WRITE, uint16_t);
__IOREG(PMC2, 0xFFC10408, __READ_WRITE, uint16_t);
__IOREG(PMC8, 0xFFC10420, __READ_WRITE, uint16_t);
__IOREG(PMC9, 0xFFC10424, __READ_WRITE, uint16_t);
__IOREG(PMC10, 0xFFC10428, __READ_WRITE, uint16_t);
__IOREG(PMC11, 0xFFC1042C, __READ_WRITE, uint16_t);
__IOREG(PMC12, 0xFFC10430, __READ_WRITE, uint16_t);
__IOREG(PMC18, 0xFFC10448, __READ_WRITE, uint16_t);
__IOREG(PMC20, 0xFFC10450, __READ_WRITE, uint16_t);
__IOREG(PFC0, 0xFFC10500, __READ_WRITE, uint16_t);
__IOREG(PFC1, 0xFFC10504, __READ_WRITE, uint16_t);
__IOREG(PFC8, 0xFFC10520, __READ_WRITE, uint16_t);
__IOREG(PFC9, 0xFFC10524, __READ_WRITE, uint16_t);
__IOREG(PFC10, 0xFFC10528, __READ_WRITE, uint16_t);
__IOREG(PFC11, 0xFFC1052C, __READ_WRITE, uint16_t);
__IOREG(PFC12, 0xFFC10530, __READ_WRITE, uint16_t);
__IOREG(PFC18, 0xFFC10548, __READ_WRITE, uint16_t);
__IOREG(PFC20, 0xFFC10550, __READ_WRITE, uint16_t);
__IOREG(PFCE0, 0xFFC10600, __READ_WRITE, uint16_t);
__IOREG(PFCE8, 0xFFC10620, __READ_WRITE, uint16_t);
__IOREG(PFCE9, 0xFFC10624, __READ_WRITE, uint16_t);
__IOREG(PFCE10, 0xFFC10628, __READ_WRITE, uint16_t);
__IOREG(PFCE11, 0xFFC1062C, __READ_WRITE, uint16_t);
__IOREG(PFCE12, 0xFFC10630, __READ_WRITE, uint16_t);
__IOREG(PNOT0, 0xFFC10700, __READ_WRITE, uint16_t);
__IOREG(PNOT1, 0xFFC10704, __READ_WRITE, uint16_t);
__IOREG(PNOT2, 0xFFC10708, __READ_WRITE, uint16_t);
__IOREG(PNOT8, 0xFFC10720, __READ_WRITE, uint16_t);
__IOREG(PNOT9, 0xFFC10724, __READ_WRITE, uint16_t);
__IOREG(PNOT10, 0xFFC10728, __READ_WRITE, uint16_t);
__IOREG(PNOT11, 0xFFC1072C, __READ_WRITE, uint16_t);
__IOREG(PNOT12, 0xFFC10730, __READ_WRITE, uint16_t);
__IOREG(PNOT18, 0xFFC10748, __READ_WRITE, uint16_t);
__IOREG(PNOT20, 0xFFC10750, __READ_WRITE, uint16_t);
__IOREG(APNOT0, 0xFFC107C8, __READ_WRITE, uint16_t);
__IOREG(APNOT1, 0xFFC107CC, __READ_WRITE, uint16_t);
__IOREG(PMSR0, 0xFFC10800, __READ_WRITE, uint32_t);
__IOREG(PMSR1, 0xFFC10804, __READ_WRITE, uint32_t);
__IOREG(PMSR2, 0xFFC10808, __READ_WRITE, uint32_t);
__IOREG(PMSR8, 0xFFC10820, __READ_WRITE, uint32_t);
__IOREG(PMSR9, 0xFFC10824, __READ_WRITE, uint32_t);
__IOREG(PMSR10, 0xFFC10828, __READ_WRITE, uint32_t);
__IOREG(PMSR11, 0xFFC1082C, __READ_WRITE, uint32_t);
__IOREG(PMSR12, 0xFFC10830, __READ_WRITE, uint32_t);
__IOREG(PMSR18, 0xFFC10848, __READ_WRITE, uint32_t);
__IOREG(PMSR20, 0xFFC10850, __READ_WRITE, uint32_t);
__IOREG(APMSR0, 0xFFC108C8, __READ_WRITE, uint32_t);
__IOREG(APMSR1, 0xFFC108CC, __READ_WRITE, uint32_t);
__IOREG(PMCSR0, 0xFFC10900, __READ_WRITE, uint32_t);
__IOREG(PMCSR1, 0xFFC10904, __READ_WRITE, uint32_t);
__IOREG(PMCSR2, 0xFFC10908, __READ_WRITE, uint32_t);
__IOREG(PMCSR8, 0xFFC10920, __READ_WRITE, uint32_t);
__IOREG(PMCSR9, 0xFFC10924, __READ_WRITE, uint32_t);
__IOREG(PMCSR10, 0xFFC10928, __READ_WRITE, uint32_t);
__IOREG(PMCSR11, 0xFFC1092C, __READ_WRITE, uint32_t);
__IOREG(PMCSR12, 0xFFC10930, __READ_WRITE, uint32_t);
__IOREG(PMCSR18, 0xFFC10948, __READ_WRITE, uint32_t);
__IOREG(PMCSR20, 0xFFC10950, __READ_WRITE, uint32_t);
__IOREG(PFCAE0, 0xFFC10A00, __READ_WRITE, uint16_t);
__IOREG(PFCAE10, 0xFFC10A28, __READ_WRITE, uint16_t);
__IOREG(PFCAE11, 0xFFC10A2C, __READ_WRITE, uint16_t);
__IOREG(PIBC0, 0xFFC14000, __READ_WRITE, uint16_t);
__IOREG(PIBC1, 0xFFC14004, __READ_WRITE, uint16_t);
__IOREG(PIBC2, 0xFFC14008, __READ_WRITE, uint16_t);
__IOREG(PIBC8, 0xFFC14020, __READ_WRITE, uint16_t);
__IOREG(PIBC9, 0xFFC14024, __READ_WRITE, uint16_t);
__IOREG(PIBC10, 0xFFC14028, __READ_WRITE, uint16_t);
__IOREG(PIBC11, 0xFFC1402C, __READ_WRITE, uint16_t);
__IOREG(PIBC12, 0xFFC14030, __READ_WRITE, uint16_t);
__IOREG(PIBC18, 0xFFC14048, __READ_WRITE, uint16_t);
__IOREG(PIBC20, 0xFFC14050, __READ_WRITE, uint16_t);
__IOREG(APIBC0, 0xFFC140C8, __READ_WRITE, uint16_t);
__IOREG(APIBC1, 0xFFC140CC, __READ_WRITE, uint16_t);
__IOREG(IPIBC0, 0xFFC140F0, __READ_WRITE, uint16_t);
__IOREG(PBDC0, 0xFFC14100, __READ_WRITE, uint16_t);
__IOREG(PBDC1, 0xFFC14104, __READ_WRITE, uint16_t);
__IOREG(PBDC2, 0xFFC14108, __READ_WRITE, uint16_t);
__IOREG(PBDC8, 0xFFC14120, __READ_WRITE, uint16_t);
__IOREG(PBDC9, 0xFFC14124, __READ_WRITE, uint16_t);
__IOREG(PBDC10, 0xFFC14128, __READ_WRITE, uint16_t);
__IOREG(PBDC11, 0xFFC1412C, __READ_WRITE, uint16_t);
__IOREG(PBDC12, 0xFFC14130, __READ_WRITE, uint16_t);
__IOREG(PBDC18, 0xFFC14148, __READ_WRITE, uint16_t);
__IOREG(PBDC20, 0xFFC14150, __READ_WRITE, uint16_t);
__IOREG(APBDC0, 0xFFC141C8, __READ_WRITE, uint16_t);
__IOREG(APBDC1, 0xFFC141CC, __READ_WRITE, uint16_t);
__IOREG(PIPC0, 0xFFC14200, __READ_WRITE, uint16_t);
__IOREG(PIPC10, 0xFFC14228, __READ_WRITE, uint16_t);
__IOREG(PIPC11, 0xFFC1422C, __READ_WRITE, uint16_t);
__IOREG(PU0, 0xFFC14300, __READ_WRITE, uint16_t);
__IOREG(PU1, 0xFFC14304, __READ_WRITE, uint16_t);
__IOREG(PU2, 0xFFC14308, __READ_WRITE, uint16_t);
__IOREG(PU8, 0xFFC14320, __READ_WRITE, uint16_t);
__IOREG(PU9, 0xFFC14324, __READ_WRITE, uint16_t);
__IOREG(PU10, 0xFFC14328, __READ_WRITE, uint16_t);
__IOREG(PU11, 0xFFC1432C, __READ_WRITE, uint16_t);
__IOREG(PU12, 0xFFC14330, __READ_WRITE, uint16_t);
__IOREG(PU18, 0xFFC14348, __READ_WRITE, uint16_t);
__IOREG(PU20, 0xFFC14350, __READ_WRITE, uint16_t);
__IOREG(PD0, 0xFFC14400, __READ_WRITE, uint16_t);
__IOREG(PD8, 0xFFC14420, __READ_WRITE, uint16_t);
__IOREG(PD9, 0xFFC14424, __READ_WRITE, uint16_t);
__IOREG(PD10, 0xFFC14428, __READ_WRITE, uint16_t);
__IOREG(PD11, 0xFFC1442C, __READ_WRITE, uint16_t);
__IOREG(PODC0, 0xFFC14500, __READ_WRITE, uint32_t);
__IOREG(PODC1, 0xFFC14504, __READ_WRITE, uint32_t);
__IOREG(PODC2, 0xFFC14508, __READ_WRITE, uint32_t);
__IOREG(PODC8, 0xFFC14520, __READ_WRITE, uint32_t);
__IOREG(PODC9, 0xFFC14524, __READ_WRITE, uint32_t);
__IOREG(PODC10, 0xFFC14528, __READ_WRITE, uint32_t);
__IOREG(PODC11, 0xFFC1452C, __READ_WRITE, uint32_t);
__IOREG(PODC12, 0xFFC14530, __READ_WRITE, uint32_t);
__IOREG(PODC18, 0xFFC14548, __READ_WRITE, uint32_t);
__IOREG(PODC20, 0xFFC14550, __READ_WRITE, uint32_t);
__IOREG(PDSC0, 0xFFC14600, __READ_WRITE, uint32_t);
__IOREG(PDSC10, 0xFFC14628, __READ_WRITE, uint32_t);
__IOREG(PDSC11, 0xFFC1462C, __READ_WRITE, uint32_t);
__IOREG(PDSC12, 0xFFC14630, __READ_WRITE, uint32_t);
__IOREG(PIS0, 0xFFC14700, __READ_WRITE, uint16_t);
__IOREG(PIS1, 0xFFC14704, __READ_WRITE, uint16_t);
__IOREG(PIS2, 0xFFC14708, __READ_WRITE, uint16_t);
__IOREG(PIS10, 0xFFC14728, __READ_WRITE, uint16_t);
__IOREG(PIS11, 0xFFC1472C, __READ_WRITE, uint16_t);
__IOREG(PIS12, 0xFFC14730, __READ_WRITE, uint16_t);
__IOREG(PIS20, 0xFFC14750, __READ_WRITE, uint16_t);
__IOREG(PPROTS0, 0xFFC14B00, __READ, uint32_t);
__IOREG(PPROTS1, 0xFFC14B04, __READ, uint32_t);
__IOREG(PPROTS2, 0xFFC14B08, __READ, uint32_t);
__IOREG(PPROTS8, 0xFFC14B20, __READ, uint32_t);
__IOREG(PPROTS9, 0xFFC14B24, __READ, uint32_t);
__IOREG(PPROTS10, 0xFFC14B28, __READ, uint32_t);
__IOREG(PPROTS11, 0xFFC14B2C, __READ, uint32_t);
__IOREG(PPROTS12, 0xFFC14B30, __READ, uint32_t);
__IOREG(PPROTS18, 0xFFC14B48, __READ, uint32_t);
__IOREG(PPROTS20, 0xFFC14B50, __READ, uint32_t);
__IOREG(PPCMD0, 0xFFC14C00, __READ_WRITE, uint32_t);
__IOREG(PPCMD1, 0xFFC14C04, __READ_WRITE, uint32_t);
__IOREG(PPCMD2, 0xFFC14C08, __READ_WRITE, uint32_t);
__IOREG(PPCMD8, 0xFFC14C20, __READ_WRITE, uint32_t);
__IOREG(PPCMD9, 0xFFC14C24, __READ_WRITE, uint32_t);
__IOREG(PPCMD10, 0xFFC14C28, __READ_WRITE, uint32_t);
__IOREG(PPCMD11, 0xFFC14C2C, __READ_WRITE, uint32_t);
__IOREG(PPCMD12, 0xFFC14C30, __READ_WRITE, uint32_t);
__IOREG(PPCMD18, 0xFFC14C48, __READ_WRITE, uint32_t);
__IOREG(PPCMD20, 0xFFC14C50, __READ_WRITE, uint32_t);
__IOREG(JP0, 0xFFC20000, __READ_WRITE, uint8_t);
__IOREG(JPSR0, 0xFFC20010, __READ_WRITE, uint32_t);
__IOREG(JPPR0, 0xFFC20020, __READ, uint8_t);
__IOREG(JPM0, 0xFFC20030, __READ_WRITE, uint8_t);
__IOREG(JPMC0, 0xFFC20040, __READ_WRITE, uint8_t);
__IOREG(JPFC0, 0xFFC20050, __READ_WRITE, uint8_t);
__IOREG(JPNOT0, 0xFFC20070, __READ_WRITE, uint8_t);
__IOREG(JPMSR0, 0xFFC20080, __READ_WRITE, uint32_t);
__IOREG(JPMCSR0, 0xFFC20090, __READ_WRITE, uint32_t);
__IOREG(JPIBC0, 0xFFC20400, __READ_WRITE, uint8_t);
__IOREG(JPBDC0, 0xFFC20410, __READ_WRITE, uint8_t);
__IOREG(JPU0, 0xFFC20430, __READ_WRITE, uint8_t);
__IOREG(JPD0, 0xFFC20440, __READ_WRITE, uint8_t);
__IOREG(JPODC0, 0xFFC20450, __READ_WRITE, uint32_t);
__IOREG(JPISA0, 0xFFC204A0, __READ_WRITE, uint8_t);
__IOREG(JPPROTS0, 0xFFC204B0, __READ, uint32_t);
__IOREG(JPPCMD0, 0xFFC204C0, __READ_WRITE, uint32_t);
__IOREG(DNFATAUD0ICTL, 0xFFC30000, __READ_WRITE, uint8_t);
__IOREG(DNFATAUD0IEN, 0xFFC30004, __READ_WRITE, uint16_t);
__IOREG(DNFATAUD0IENH, 0xFFC30008, __READ_WRITE, uint8_t);
#define DNFATAUD0IENH0     (((volatile __bitf_T *)0xFFC30008)->bit00)
#define DNFATAUD0IENH1     (((volatile __bitf_T *)0xFFC30008)->bit01)
#define DNFATAUD0IENH2     (((volatile __bitf_T *)0xFFC30008)->bit02)
#define DNFATAUD0IENH3     (((volatile __bitf_T *)0xFFC30008)->bit03)
#define DNFATAUD0IENH4     (((volatile __bitf_T *)0xFFC30008)->bit04)
#define DNFATAUD0IENH5     (((volatile __bitf_T *)0xFFC30008)->bit05)
#define DNFATAUD0IENH6     (((volatile __bitf_T *)0xFFC30008)->bit06)
#define DNFATAUD0IENH7     (((volatile __bitf_T *)0xFFC30008)->bit07)
__IOREG(DNFATAUD0IENL, 0xFFC3000C, __READ_WRITE, uint8_t);
#define DNFATAUD0IENL0     (((volatile __bitf_T *)0xFFC3000C)->bit00)
#define DNFATAUD0IENL1     (((volatile __bitf_T *)0xFFC3000C)->bit01)
#define DNFATAUD0IENL2     (((volatile __bitf_T *)0xFFC3000C)->bit02)
#define DNFATAUD0IENL3     (((volatile __bitf_T *)0xFFC3000C)->bit03)
#define DNFATAUD0IENL4     (((volatile __bitf_T *)0xFFC3000C)->bit04)
#define DNFATAUD0IENL5     (((volatile __bitf_T *)0xFFC3000C)->bit05)
#define DNFATAUD0IENL6     (((volatile __bitf_T *)0xFFC3000C)->bit06)
#define DNFATAUD0IENL7     (((volatile __bitf_T *)0xFFC3000C)->bit07)
__IOREG(DNFATAUB0ICTL, 0xFFC30020, __READ_WRITE, uint8_t);
__IOREG(DNFATAUB0IEN, 0xFFC30024, __READ_WRITE, uint16_t);
__IOREG(DNFATAUB0IENH, 0xFFC30028, __READ_WRITE, uint8_t);
#define DNFATAUB0IENH0     (((volatile __bitf_T *)0xFFC30028)->bit00)
#define DNFATAUB0IENH1     (((volatile __bitf_T *)0xFFC30028)->bit01)
#define DNFATAUB0IENH2     (((volatile __bitf_T *)0xFFC30028)->bit02)
#define DNFATAUB0IENH3     (((volatile __bitf_T *)0xFFC30028)->bit03)
#define DNFATAUB0IENH4     (((volatile __bitf_T *)0xFFC30028)->bit04)
#define DNFATAUB0IENH5     (((volatile __bitf_T *)0xFFC30028)->bit05)
#define DNFATAUB0IENH6     (((volatile __bitf_T *)0xFFC30028)->bit06)
#define DNFATAUB0IENH7     (((volatile __bitf_T *)0xFFC30028)->bit07)
__IOREG(DNFATAUB0IENL, 0xFFC3002C, __READ_WRITE, uint8_t);
#define DNFATAUB0IENL0     (((volatile __bitf_T *)0xFFC3002C)->bit00)
#define DNFATAUB0IENL1     (((volatile __bitf_T *)0xFFC3002C)->bit01)
#define DNFATAUB0IENL2     (((volatile __bitf_T *)0xFFC3002C)->bit02)
#define DNFATAUB0IENL3     (((volatile __bitf_T *)0xFFC3002C)->bit03)
#define DNFATAUB0IENL4     (((volatile __bitf_T *)0xFFC3002C)->bit04)
#define DNFATAUB0IENL5     (((volatile __bitf_T *)0xFFC3002C)->bit05)
#define DNFATAUB0IENL6     (((volatile __bitf_T *)0xFFC3002C)->bit06)
#define DNFATAUB0IENL7     (((volatile __bitf_T *)0xFFC3002C)->bit07)
__IOREG(DNFATAUB1ICTL, 0xFFC30040, __READ_WRITE, uint8_t);
__IOREG(DNFATAUB1IEN, 0xFFC30044, __READ_WRITE, uint16_t);
__IOREG(DNFATAUB1IENH, 0xFFC30048, __READ_WRITE, uint8_t);
#define DNFATAUB1IENH0     (((volatile __bitf_T *)0xFFC30048)->bit00)
#define DNFATAUB1IENH1     (((volatile __bitf_T *)0xFFC30048)->bit01)
#define DNFATAUB1IENH2     (((volatile __bitf_T *)0xFFC30048)->bit02)
#define DNFATAUB1IENH3     (((volatile __bitf_T *)0xFFC30048)->bit03)
#define DNFATAUB1IENH4     (((volatile __bitf_T *)0xFFC30048)->bit04)
#define DNFATAUB1IENH5     (((volatile __bitf_T *)0xFFC30048)->bit05)
#define DNFATAUB1IENH6     (((volatile __bitf_T *)0xFFC30048)->bit06)
#define DNFATAUB1IENH7     (((volatile __bitf_T *)0xFFC30048)->bit07)
__IOREG(DNFATAUB1IENL, 0xFFC3004C, __READ_WRITE, uint8_t);
#define DNFATAUB1IENL0     (((volatile __bitf_T *)0xFFC3004C)->bit00)
#define DNFATAUB1IENL1     (((volatile __bitf_T *)0xFFC3004C)->bit01)
#define DNFATAUB1IENL2     (((volatile __bitf_T *)0xFFC3004C)->bit02)
#define DNFATAUB1IENL3     (((volatile __bitf_T *)0xFFC3004C)->bit03)
#define DNFATAUB1IENL4     (((volatile __bitf_T *)0xFFC3004C)->bit04)
#define DNFATAUB1IENL5     (((volatile __bitf_T *)0xFFC3004C)->bit05)
#define DNFATAUB1IENL6     (((volatile __bitf_T *)0xFFC3004C)->bit06)
#define DNFATAUB1IENL7     (((volatile __bitf_T *)0xFFC3004C)->bit07)
__IOREG(DNFAENCA0ICTL, 0xFFC30060, __READ_WRITE, uint8_t);
__IOREG(DNFAENCA0IEN, 0xFFC30064, __READ_WRITE, uint16_t);
__IOREG(DNFAENCA0IENL, 0xFFC3006C, __READ_WRITE, uint8_t);
#define DNFAENCA0IENL0     (((volatile __bitf_T *)0xFFC3006C)->bit00)
#define DNFAENCA0IENL1     (((volatile __bitf_T *)0xFFC3006C)->bit01)
#define DNFAENCA0IENL2     (((volatile __bitf_T *)0xFFC3006C)->bit02)
#define DNFAENCA0IENL3     (((volatile __bitf_T *)0xFFC3006C)->bit03)
#define DNFAENCA0IENL4     (((volatile __bitf_T *)0xFFC3006C)->bit04)
__IOREG(DNFAADCTL0CTL, 0xFFC300A0, __READ_WRITE, uint8_t);
__IOREG(DNFAADCTL0EN, 0xFFC300A4, __READ_WRITE, uint16_t);
__IOREG(DNFAADCTL0ENL, 0xFFC300AC, __READ_WRITE, uint8_t);
#define DNFAADCTL0ENL0     (((volatile __bitf_T *)0xFFC300AC)->bit00)
#define DNFAADCTL0ENL1     (((volatile __bitf_T *)0xFFC300AC)->bit01)
#define DNFAADCTL0ENL2     (((volatile __bitf_T *)0xFFC300AC)->bit02)
__IOREG(DNFAADCTL1CTL, 0xFFC300C0, __READ_WRITE, uint8_t);
__IOREG(DNFAADCTL1EN, 0xFFC300C4, __READ_WRITE, uint16_t);
__IOREG(DNFAADCTL1ENL, 0xFFC300CC, __READ_WRITE, uint8_t);
#define DNFAADCTL1ENL0     (((volatile __bitf_T *)0xFFC300CC)->bit00)
#define DNFAADCTL1ENL1     (((volatile __bitf_T *)0xFFC300CC)->bit01)
#define DNFAADCTL1ENL2     (((volatile __bitf_T *)0xFFC300CC)->bit02)
__IOREG(FCLA0CTL0_NMI, 0xFFC34000, __READ_WRITE, uint8_t);
__IOREG(FCLA0CTL0_INTPL, 0xFFC34020, __READ_WRITE, uint8_t);
__IOREG(FCLA0CTL1_INTPL, 0xFFC34024, __READ_WRITE, uint8_t);
__IOREG(FCLA0CTL2_INTPL, 0xFFC34028, __READ_WRITE, uint8_t);
__IOREG(FCLA0CTL3_INTPL, 0xFFC3402C, __READ_WRITE, uint8_t);
__IOREG(FCLA0CTL4_INTPL, 0xFFC34030, __READ_WRITE, uint8_t);
__IOREG(FCLA0CTL5_INTPL, 0xFFC34034, __READ_WRITE, uint8_t);
__IOREG(FCLA0CTL6_INTPL, 0xFFC34038, __READ_WRITE, uint8_t);
__IOREG(FCLA0CTL7_INTPL, 0xFFC3403C, __READ_WRITE, uint8_t);
__IOREG(FCLA0CTL0_INTPH, 0xFFC34040, __READ_WRITE, uint8_t);
__IOREG(FCLA0CTL1_INTPH, 0xFFC34044, __READ_WRITE, uint8_t);
__IOREG(FCLA0CTL2_INTPH, 0xFFC34048, __READ_WRITE, uint8_t);
__IOREG(FCLA0CTL3_INTPH, 0xFFC3404C, __READ_WRITE, uint8_t);
__IOREG(FCLA0CTL4_INTPH, 0xFFC34050, __READ_WRITE, uint8_t);
__IOREG(FCLA0CTL5_INTPH, 0xFFC34054, __READ_WRITE, uint8_t);
__IOREG(FCLA0CTL6_INTPH, 0xFFC34058, __READ_WRITE, uint8_t);
__IOREG(FCLA0CTL7_INTPH, 0xFFC3405C, __READ_WRITE, uint8_t);
__IOREG(FCLA0CTL0_ADC0, 0xFFC34060, __READ_WRITE, uint8_t);
__IOREG(FCLA0CTL1_ADC0, 0xFFC34064, __READ_WRITE, uint8_t);
__IOREG(FCLA0CTL2_ADC0, 0xFFC34068, __READ_WRITE, uint8_t);
__IOREG(FCLA0CTL0_ADC1, 0xFFC34080, __READ_WRITE, uint8_t);
__IOREG(FCLA0CTL1_ADC1, 0xFFC34084, __READ_WRITE, uint8_t);
__IOREG(FCLA0CTL2_ADC1, 0xFFC34088, __READ_WRITE, uint8_t);
__IOREG(EEPRDCYCL, 0xFFC5A010, __READ_WRITE, uint8_t);
__IOREG(CFECCCTL, 0xFFC62000, __READ_WRITE, uint16_t);
__IOREG(CFFSTERSTR, 0xFFC62004, __READ, uint32_t);
__IOREG(CFFSTSTC, 0xFFC62024, __READ_WRITE, uint8_t);
__IOREG(CFOVFSTR, 0xFFC62028, __READ, uint8_t);
__IOREG(CFOVFSTC, 0xFFC6202C, __READ_WRITE, uint8_t);
__IOREG(CFERRINT, 0xFFC62030, __READ_WRITE, uint8_t);
__IOREG(CFFSTEADR, 0xFFC62034, __READ, uint32_t);
__IOREG(CFTSTCTL, 0xFFC62054, __READ_WRITE, uint16_t);
__IOREG(LRECCCTL, 0xFFC63000, __READ_WRITE, uint16_t);
__IOREG(LRFSTERSTR, 0xFFC63004, __READ, uint32_t);
__IOREG(LRSTCLR, 0xFFC63024, __READ_WRITE, uint8_t);
__IOREG(LROVFSTR, 0xFFC63028, __READ, uint8_t);
__IOREG(LROVFSTC, 0xFFC6302C, __READ_WRITE, uint8_t);
__IOREG(LRFSTEADR0, 0xFFC63030, __READ, uint32_t);
__IOREG(LRERRINT, 0xFFC630B0, __READ_WRITE, uint8_t);
__IOREG(LRTSTCTL, 0xFFC630B4, __READ_WRITE, uint16_t);
__IOREG(LRTDATBF0, 0xFFC630B8, __READ, uint32_t);
__IOREG(DFECCCTL, 0xFFC66000, __READ_WRITE, uint16_t);
__IOREG(DFERSTR, 0xFFC66004, __READ, uint32_t);
__IOREG(DFERSTC, 0xFFC66008, __READ_WRITE, uint8_t);
__IOREG(DFERRINT, 0xFFC66014, __READ_WRITE, uint8_t);
__IOREG(DFTSTCTL, 0xFFC6601C, __READ_WRITE, uint16_t);
__IOREG(ECCCSIH0CTL, 0xFFC70000, __READ_WRITE, uint16_t);
__IOREG(ECCCSIH0TMC, 0xFFC70004, __READ_WRITE, uint16_t);
__IOREG(ECCCSIH0TRC, 0xFFC70008, __READ_WRITE, uint32_t);
__IOREG(ECCCSIH0ERDB, 0xFFC70008, __READ_WRITE, uint8_t);
__IOREG(ECCCSIH0ECRD, 0xFFC70009, __READ_WRITE, uint8_t);
__IOREG(ECCCSIH0HORD, 0xFFC7000A, __READ_WRITE, uint8_t);
__IOREG(ECCCSIH0SYND, 0xFFC7000B, __READ_WRITE, uint8_t);
__IOREG(ECCCSIH0TED, 0xFFC7000C, __READ_WRITE, uint32_t);
__IOREG(ECCCSIH1CTL, 0xFFC70010, __READ_WRITE, uint16_t);
__IOREG(ECCCSIH1TMC, 0xFFC70014, __READ_WRITE, uint16_t);
__IOREG(ECCCSIH1TRC, 0xFFC70018, __READ_WRITE, uint32_t);
__IOREG(ECCCSIH1ERDB, 0xFFC70018, __READ_WRITE, uint8_t);
__IOREG(ECCCSIH1ECRD, 0xFFC70019, __READ_WRITE, uint8_t);
__IOREG(ECCCSIH1HORD, 0xFFC7001A, __READ_WRITE, uint8_t);
__IOREG(ECCCSIH1SYND, 0xFFC7001B, __READ_WRITE, uint8_t);
__IOREG(ECCCSIH1TED, 0xFFC7001C, __READ_WRITE, uint32_t);
__IOREG(ECCCSIH2CTL, 0xFFC70020, __READ_WRITE, uint16_t);
__IOREG(ECCCSIH2TMC, 0xFFC70024, __READ_WRITE, uint16_t);
__IOREG(ECCCSIH2TRC, 0xFFC70028, __READ_WRITE, uint32_t);
__IOREG(ECCCSIH2ERDB, 0xFFC70028, __READ_WRITE, uint8_t);
__IOREG(ECCCSIH2ECRD, 0xFFC70029, __READ_WRITE, uint8_t);
__IOREG(ECCCSIH2HORD, 0xFFC7002A, __READ_WRITE, uint8_t);
__IOREG(ECCCSIH2SYND, 0xFFC7002B, __READ_WRITE, uint8_t);
__IOREG(ECCCSIH2TED, 0xFFC7002C, __READ_WRITE, uint32_t);
__IOREG(ECCCSIH3CTL, 0xFFC70030, __READ_WRITE, uint16_t);
__IOREG(ECCCSIH3TMC, 0xFFC70034, __READ_WRITE, uint16_t);
__IOREG(ECCCSIH3TRC, 0xFFC70038, __READ_WRITE, uint32_t);
__IOREG(ECCCSIH3ERDB, 0xFFC70038, __READ_WRITE, uint8_t);
__IOREG(ECCCSIH3ECRD, 0xFFC70039, __READ_WRITE, uint8_t);
__IOREG(ECCCSIH3HORD, 0xFFC7003A, __READ_WRITE, uint8_t);
__IOREG(ECCCSIH3SYND, 0xFFC7003B, __READ_WRITE, uint8_t);
__IOREG(ECCCSIH3TED, 0xFFC7003C, __READ_WRITE, uint32_t);
__IOREG(ECCRCAN0CTL, 0xFFC71000, __READ_WRITE, uint16_t);
__IOREG(ECCRCAN0TMC, 0xFFC71004, __READ_WRITE, uint16_t);
__IOREG(ECCRCAN0TRC, 0xFFC71008, __READ_WRITE, uint32_t);
__IOREG(ECCRCAN0ERDB, 0xFFC71008, __READ_WRITE, uint8_t);
__IOREG(ECCRCAN0ECRD, 0xFFC71009, __READ_WRITE, uint8_t);
__IOREG(ECCRCAN0HORD, 0xFFC7100A, __READ_WRITE, uint8_t);
__IOREG(ECCRCAN0SYND, 0xFFC7100B, __READ_WRITE, uint8_t);
__IOREG(ECCRCAN0TED, 0xFFC7100C, __READ_WRITE, uint32_t);
__IOREG(CVMF, 0xFFF50000, __READ_WRITE, uint32_t);
__IOREG(CVMDE, 0xFFF50004, __READ, uint32_t);
__IOREG(CVMDIAG, 0xFFF50014, __READ_WRITE, uint32_t);
__IOREG(PROTCMDCVM, 0xFFF50100, __READ_WRITE, uint32_t);
__IOREG(PROTSCVM, 0xFFF50104, __READ, uint32_t);
__IOREG(PROTCMD0, 0xFFF80000, __READ_WRITE, uint32_t);
__IOREG(PROTS0, 0xFFF80004, __READ, uint32_t);
__IOREG(STBC0PSC, 0xFFF80100, __READ_WRITE, uint32_t);
__IOREG(STBC0STPT, 0xFFF80110, __READ_WRITE, uint32_t);
__IOREG(WDTNMIF, 0xFFF80200, __READ, uint32_t);
__IOREG(WDTNMIFC, 0xFFF80208, __READ_WRITE, uint32_t);
__IOREG(FEINTF, 0xFFF80300, __READ, uint32_t);
__IOREG(FEINTFMSK, 0xFFF80304, __READ_WRITE, uint32_t);
__IOREG(FEINTFC, 0xFFF80308, __READ_WRITE, uint32_t);
__IOREG(WUF0, 0xFFF80400, __READ, uint32_t);
__IOREG(WUFMSK0, 0xFFF80404, __READ_WRITE, uint32_t);
__IOREG(WUFC0, 0xFFF80408, __READ_WRITE, uint32_t);
__IOREG(WUF20, 0xFFF80520, __READ, uint32_t);
__IOREG(WUFMSK20, 0xFFF80524, __READ_WRITE, uint32_t);
__IOREG(WUFC20, 0xFFF80528, __READ_WRITE, uint32_t);
__IOREG(RESF, 0xFFF80760, __READ, uint32_t);
__IOREG(RESFC, 0xFFF80768, __READ_WRITE, uint32_t);
__IOREG(RESFR, 0xFFF80860, __READ, uint32_t);
__IOREG(RESFCR, 0xFFF80868, __READ_WRITE, uint32_t);
__IOREG(VLVF, 0xFFF80980, __READ, uint32_t);
__IOREG(VLVFC, 0xFFF80988, __READ_WRITE, uint32_t);
__IOREG(LVICNT, 0xFFF80A00, __READ_WRITE, uint32_t);
__IOREG(SWRESA, 0xFFF80A04, __READ_WRITE, uint32_t);
__IOREG(IOHOLD, 0xFFF80B00, __READ_WRITE, uint32_t);
__IOREG(ROSCE, 0xFFF81000, __READ_WRITE, uint32_t);
__IOREG(ROSCS, 0xFFF81004, __READ, uint32_t);
__IOREG(ROSCSTPM, 0xFFF81018, __READ_WRITE, uint32_t);
__IOREG(MOSCE, 0xFFF81100, __READ_WRITE, uint32_t);
__IOREG(MOSCS, 0xFFF81104, __READ, uint32_t);
__IOREG(MOSCC, 0xFFF81108, __READ_WRITE, uint32_t);
__IOREG(MOSCST, 0xFFF8110C, __READ_WRITE, uint32_t);
__IOREG(MOSCSTPM, 0xFFF81118, __READ_WRITE, uint32_t);
__IOREG(SOSCE, 0xFFF81200, __READ_WRITE, uint32_t);
__IOREG(SOSCS, 0xFFF81204, __READ, uint32_t);
__IOREG(SOSCST, 0xFFF8120C, __READ_WRITE, uint32_t);
__IOREG(CKSC_AWDTAD_CTL, 0xFFF82000, __READ_WRITE, uint32_t);
__IOREG(CKSC_AWDTAD_ACT, 0xFFF82008, __READ, uint32_t);
__IOREG(CKSC_AWDTAD_STPM, 0xFFF82018, __READ_WRITE, uint32_t);
__IOREG(CKSC_ATAUJS_CTL, 0xFFF82100, __READ_WRITE, uint32_t);
__IOREG(CKSC_ATAUJS_ACT, 0xFFF82108, __READ, uint32_t);
__IOREG(CKSC_ATAUJD_CTL, 0xFFF82200, __READ_WRITE, uint32_t);
__IOREG(CKSC_ATAUJD_ACT, 0xFFF82208, __READ, uint32_t);
__IOREG(CKSC_ATAUJD_STPM, 0xFFF82218, __READ_WRITE, uint32_t);
__IOREG(CKSC_ARTCAS_CTL, 0xFFF82300, __READ_WRITE, uint32_t);
__IOREG(CKSC_ARTCAS_ACT, 0xFFF82308, __READ, uint32_t);
__IOREG(CKSC_ARTCAD_CTL, 0xFFF82400, __READ_WRITE, uint32_t);
__IOREG(CKSC_ARTCAD_ACT, 0xFFF82408, __READ, uint32_t);
__IOREG(CKSC_ARTCAD_STPM, 0xFFF82418, __READ_WRITE, uint32_t);
__IOREG(CKSC_AADCAS_CTL, 0xFFF82500, __READ_WRITE, uint32_t);
__IOREG(CKSC_AADCAS_ACT, 0xFFF82508, __READ, uint32_t);
__IOREG(CKSC_AADCAD_CTL, 0xFFF82600, __READ_WRITE, uint32_t);
__IOREG(CKSC_AADCAD_ACT, 0xFFF82608, __READ, uint32_t);
__IOREG(CKSC_AADCAD_STPM, 0xFFF82618, __READ_WRITE, uint32_t);
__IOREG(CKSC_AFOUTS_CTL, 0xFFF82700, __READ_WRITE, uint32_t);
__IOREG(CKSC_AFOUTS_ACT, 0xFFF82708, __READ, uint32_t);
__IOREG(CKSC_AFOUTS_STPM, 0xFFF82718, __READ_WRITE, uint32_t);
__IOREG(FOUTDIV, 0xFFF82800, __READ_WRITE, uint32_t);
__IOREG(FOUTSTAT, 0xFFF82804, __READ, uint32_t);
__IOREG(PROTCMD1, 0xFFF88000, __READ_WRITE, uint32_t);
__IOREG(PROTS1, 0xFFF88004, __READ, uint32_t);
__IOREG(WUF_ISO0, 0xFFF88110, __READ, uint32_t);
__IOREG(WUFMSK_ISO0, 0xFFF88114, __READ_WRITE, uint32_t);
__IOREG(WUFC_ISO0, 0xFFF88118, __READ_WRITE, uint32_t);
__IOREG(PLLE, 0xFFF89000, __READ_WRITE, uint32_t);
__IOREG(PLLS, 0xFFF89004, __READ, uint32_t);
__IOREG(PLLC, 0xFFF89008, __READ_WRITE, uint32_t);
__IOREG(CKSC_CPUCLKS_CTL, 0xFFF8A000, __READ_WRITE, uint32_t);
__IOREG(CKSC_CPUCLKS_ACT, 0xFFF8A008, __READ, uint32_t);
__IOREG(CKSC_CPUCLKD_CTL, 0xFFF8A100, __READ_WRITE, uint32_t);
__IOREG(CKSC_CPUCLKD_ACT, 0xFFF8A108, __READ, uint32_t);
__IOREG(CKSC_IPERI1S_CTL, 0xFFF8A200, __READ_WRITE, uint32_t);
__IOREG(CKSC_IPERI1S_ACT, 0xFFF8A208, __READ, uint32_t);
__IOREG(CKSC_IPERI2S_CTL, 0xFFF8A300, __READ_WRITE, uint32_t);
__IOREG(CKSC_IPERI2S_ACT, 0xFFF8A308, __READ, uint32_t);
__IOREG(CKSC_ILINS_CTL, 0xFFF8A400, __READ_WRITE, uint32_t);
__IOREG(CKSC_ILINS_ACT, 0xFFF8A408, __READ, uint32_t);
__IOREG(CKSC_IADCAS_CTL, 0xFFF8A500, __READ_WRITE, uint32_t);
__IOREG(CKSC_IADCAS_ACT, 0xFFF8A508, __READ, uint32_t);
__IOREG(CKSC_IADCAD_CTL, 0xFFF8A600, __READ_WRITE, uint32_t);
__IOREG(CKSC_IADCAD_ACT, 0xFFF8A608, __READ, uint32_t);
__IOREG(CKSC_ILIND_CTL, 0xFFF8A800, __READ_WRITE, uint32_t);
__IOREG(CKSC_ILIND_ACT, 0xFFF8A808, __READ, uint32_t);
__IOREG(CKSC_ILIND_STPM, 0xFFF8A818, __READ_WRITE, uint32_t);
__IOREG(CKSC_ICANS_CTL, 0xFFF8A900, __READ_WRITE, uint32_t);
__IOREG(CKSC_ICANS_ACT, 0xFFF8A908, __READ, uint32_t);
__IOREG(CKSC_ICANS_STPM, 0xFFF8A918, __READ_WRITE, uint32_t);
__IOREG(CKSC_ICANOSCD_CTL, 0xFFF8AA00, __READ_WRITE, uint32_t);
__IOREG(CKSC_ICANOSCD_ACT, 0xFFF8AA08, __READ, uint32_t);
__IOREG(CKSC_ICANOSCD_STPM, 0xFFF8AA18, __READ_WRITE, uint32_t);
__IOREG(CKSC_ICSIS_CTL, 0xFFF8AB00, __READ_WRITE, uint32_t);
__IOREG(CKSC_ICSIS_ACT, 0xFFF8AB08, __READ, uint32_t);
__IOREG(PROTCMDCLMA, 0xFFF8C200, __READ_WRITE, uint32_t);
__IOREG(PROTSCLMA, 0xFFF8C204, __READ, uint32_t);
__IOREG(BSC, 0xFFFF8200, __READ_WRITE, uint16_t);
__IOREG(DEC, 0xFFFF8202, __READ_WRITE, uint16_t);
__IOREG(DWC, 0xFFFF8208, __READ_WRITE, uint16_t);
__IOREG(DHC, 0xFFFF820C, __READ_WRITE, uint16_t);
__IOREG(AWC, 0xFFFF8210, __READ_WRITE, uint16_t);
__IOREG(ICC, 0xFFFF8214, __READ_WRITE, uint16_t);
__IOREG(DTRC0, 0xFFFF8300, __READ_WRITE, uint8_t);
#define DTRC0ADS           (((volatile __bitf_T *)0xFFFF8300)->bit00)
#define DTRC0ERR           (((volatile __bitf_T *)0xFFFF8300)->bit07)
__IOREG(DMCM0, 0xFFFF8304, __READ_WRITE, uint16_t);
__IOREG(DSA0, 0xFFFF8314, __READ_WRITE, uint32_t);
__IOREG(DSA0L, 0xFFFF8314, __READ_WRITE, uint16_t);
__IOREG(DSA0H, 0xFFFF8316, __READ_WRITE, uint16_t);
__IOREG(DDA0, 0xFFFF8324, __READ_WRITE, uint32_t);
__IOREG(DDA0L, 0xFFFF8324, __READ_WRITE, uint16_t);
__IOREG(DDA0H, 0xFFFF8326, __READ_WRITE, uint16_t);
__IOREG(DTC0, 0xFFFF8332, __READ_WRITE, uint16_t);
__IOREG(DTCT0, 0xFFFF8338, __READ_WRITE, uint16_t);
__IOREG(DTS0, 0xFFFF833A, __READ_WRITE, uint8_t);
#define DTS0DTE            (((volatile __bitf_T *)0xFFFF833A)->bit00)
#define DTS0SR             (((volatile __bitf_T *)0xFFFF833A)->bit01)
#define DTS0DR             (((volatile __bitf_T *)0xFFFF833A)->bit02)
#define DTS0ER             (((volatile __bitf_T *)0xFFFF833A)->bit03)
#define DTS0DT             (((volatile __bitf_T *)0xFFFF833A)->bit06)
#define DTS0TC             (((volatile __bitf_T *)0xFFFF833A)->bit07)
__IOREG(DSA1, 0xFFFF8344, __READ_WRITE, uint32_t);
__IOREG(DSA1L, 0xFFFF8344, __READ_WRITE, uint16_t);
__IOREG(DSA1H, 0xFFFF8346, __READ_WRITE, uint16_t);
__IOREG(DDA1, 0xFFFF8354, __READ_WRITE, uint32_t);
__IOREG(DDA1L, 0xFFFF8354, __READ_WRITE, uint16_t);
__IOREG(DDA1H, 0xFFFF8356, __READ_WRITE, uint16_t);
__IOREG(DTC1, 0xFFFF8362, __READ_WRITE, uint16_t);
__IOREG(DTCT1, 0xFFFF8368, __READ_WRITE, uint16_t);
__IOREG(DTS1, 0xFFFF836A, __READ_WRITE, uint8_t);
#define DTS1DTE            (((volatile __bitf_T *)0xFFFF836A)->bit00)
#define DTS1SR             (((volatile __bitf_T *)0xFFFF836A)->bit01)
#define DTS1DR             (((volatile __bitf_T *)0xFFFF836A)->bit02)
#define DTS1ER             (((volatile __bitf_T *)0xFFFF836A)->bit03)
#define DTS1DT             (((volatile __bitf_T *)0xFFFF836A)->bit06)
#define DTS1TC             (((volatile __bitf_T *)0xFFFF836A)->bit07)
__IOREG(DSA2, 0xFFFF8374, __READ_WRITE, uint32_t);
__IOREG(DSA2L, 0xFFFF8374, __READ_WRITE, uint16_t);
__IOREG(DSA2H, 0xFFFF8376, __READ_WRITE, uint16_t);
__IOREG(DDA2, 0xFFFF8384, __READ_WRITE, uint32_t);
__IOREG(DDA2L, 0xFFFF8384, __READ_WRITE, uint16_t);
__IOREG(DDA2H, 0xFFFF8386, __READ_WRITE, uint16_t);
__IOREG(DTC2, 0xFFFF8392, __READ_WRITE, uint16_t);
__IOREG(DTCT2, 0xFFFF8398, __READ_WRITE, uint16_t);
__IOREG(DTS2, 0xFFFF839A, __READ_WRITE, uint8_t);
#define DTS2DTE            (((volatile __bitf_T *)0xFFFF839A)->bit00)
#define DTS2SR             (((volatile __bitf_T *)0xFFFF839A)->bit01)
#define DTS2DR             (((volatile __bitf_T *)0xFFFF839A)->bit02)
#define DTS2ER             (((volatile __bitf_T *)0xFFFF839A)->bit03)
#define DTS2DT             (((volatile __bitf_T *)0xFFFF839A)->bit06)
#define DTS2TC             (((volatile __bitf_T *)0xFFFF839A)->bit07)
__IOREG(DSA3, 0xFFFF83A4, __READ_WRITE, uint32_t);
__IOREG(DSA3L, 0xFFFF83A4, __READ_WRITE, uint16_t);
__IOREG(DSA3H, 0xFFFF83A6, __READ_WRITE, uint16_t);
__IOREG(DDA3, 0xFFFF83B4, __READ_WRITE, uint32_t);
__IOREG(DDA3L, 0xFFFF83B4, __READ_WRITE, uint16_t);
__IOREG(DDA3H, 0xFFFF83B6, __READ_WRITE, uint16_t);
__IOREG(DTC3, 0xFFFF83C2, __READ_WRITE, uint16_t);
__IOREG(DTCT3, 0xFFFF83C8, __READ_WRITE, uint16_t);
__IOREG(DTS3, 0xFFFF83CA, __READ_WRITE, uint8_t);
#define DTS3DTE            (((volatile __bitf_T *)0xFFFF83CA)->bit00)
#define DTS3SR             (((volatile __bitf_T *)0xFFFF83CA)->bit01)
#define DTS3DR             (((volatile __bitf_T *)0xFFFF83CA)->bit02)
#define DTS3ER             (((volatile __bitf_T *)0xFFFF83CA)->bit03)
#define DTS3DT             (((volatile __bitf_T *)0xFFFF83CA)->bit06)
#define DTS3TC             (((volatile __bitf_T *)0xFFFF83CA)->bit07)
__IOREG(DSA4, 0xFFFF83D4, __READ_WRITE, uint32_t);
__IOREG(DSA4L, 0xFFFF83D4, __READ_WRITE, uint16_t);
__IOREG(DSA4H, 0xFFFF83D6, __READ_WRITE, uint16_t);
__IOREG(DDA4, 0xFFFF83E4, __READ_WRITE, uint32_t);
__IOREG(DDA4L, 0xFFFF83E4, __READ_WRITE, uint16_t);
__IOREG(DDA4H, 0xFFFF83E6, __READ_WRITE, uint16_t);
__IOREG(DTC4, 0xFFFF83F2, __READ_WRITE, uint16_t);
__IOREG(DTCT4, 0xFFFF83F8, __READ_WRITE, uint16_t);
__IOREG(DTS4, 0xFFFF83FA, __READ_WRITE, uint8_t);
#define DTS4DTE            (((volatile __bitf_T *)0xFFFF83FA)->bit00)
#define DTS4SR             (((volatile __bitf_T *)0xFFFF83FA)->bit01)
#define DTS4DR             (((volatile __bitf_T *)0xFFFF83FA)->bit02)
#define DTS4ER             (((volatile __bitf_T *)0xFFFF83FA)->bit03)
#define DTS4DT             (((volatile __bitf_T *)0xFFFF83FA)->bit06)
#define DTS4TC             (((volatile __bitf_T *)0xFFFF83FA)->bit07)
__IOREG(DSA5, 0xFFFF8404, __READ_WRITE, uint32_t);
__IOREG(DSA5L, 0xFFFF8404, __READ_WRITE, uint16_t);
__IOREG(DSA5H, 0xFFFF8406, __READ_WRITE, uint16_t);
__IOREG(DDA5, 0xFFFF8414, __READ_WRITE, uint32_t);
__IOREG(DDA5L, 0xFFFF8414, __READ_WRITE, uint16_t);
__IOREG(DDA5H, 0xFFFF8416, __READ_WRITE, uint16_t);
__IOREG(DTC5, 0xFFFF8422, __READ_WRITE, uint16_t);
__IOREG(DTCT5, 0xFFFF8428, __READ_WRITE, uint16_t);
__IOREG(DTS5, 0xFFFF842A, __READ_WRITE, uint8_t);
#define DTS5DTE            (((volatile __bitf_T *)0xFFFF842A)->bit00)
#define DTS5SR             (((volatile __bitf_T *)0xFFFF842A)->bit01)
#define DTS5DR             (((volatile __bitf_T *)0xFFFF842A)->bit02)
#define DTS5ER             (((volatile __bitf_T *)0xFFFF842A)->bit03)
#define DTS5DT             (((volatile __bitf_T *)0xFFFF842A)->bit06)
#define DTS5TC             (((volatile __bitf_T *)0xFFFF842A)->bit07)
__IOREG(DSA6, 0xFFFF8434, __READ_WRITE, uint32_t);
__IOREG(DSA6L, 0xFFFF8434, __READ_WRITE, uint16_t);
__IOREG(DSA6H, 0xFFFF8436, __READ_WRITE, uint16_t);
__IOREG(DDA6, 0xFFFF8444, __READ_WRITE, uint32_t);
__IOREG(DDA6L, 0xFFFF8444, __READ_WRITE, uint16_t);
__IOREG(DDA6H, 0xFFFF8446, __READ_WRITE, uint16_t);
__IOREG(DTC6, 0xFFFF8452, __READ_WRITE, uint16_t);
__IOREG(DTCT6, 0xFFFF8458, __READ_WRITE, uint16_t);
__IOREG(DTS6, 0xFFFF845A, __READ_WRITE, uint8_t);
#define DTS6DTE            (((volatile __bitf_T *)0xFFFF845A)->bit00)
#define DTS6SR             (((volatile __bitf_T *)0xFFFF845A)->bit01)
#define DTS6DR             (((volatile __bitf_T *)0xFFFF845A)->bit02)
#define DTS6ER             (((volatile __bitf_T *)0xFFFF845A)->bit03)
#define DTS6DT             (((volatile __bitf_T *)0xFFFF845A)->bit06)
#define DTS6TC             (((volatile __bitf_T *)0xFFFF845A)->bit07)
__IOREG(DSA7, 0xFFFF8464, __READ_WRITE, uint32_t);
__IOREG(DSA7L, 0xFFFF8464, __READ_WRITE, uint16_t);
__IOREG(DSA7H, 0xFFFF8466, __READ_WRITE, uint16_t);
__IOREG(DDA7, 0xFFFF8474, __READ_WRITE, uint32_t);
__IOREG(DDA7L, 0xFFFF8474, __READ_WRITE, uint16_t);
__IOREG(DDA7H, 0xFFFF8476, __READ_WRITE, uint16_t);
__IOREG(DTC7, 0xFFFF8482, __READ_WRITE, uint16_t);
__IOREG(DTCT7, 0xFFFF8488, __READ_WRITE, uint16_t);
__IOREG(DTS7, 0xFFFF848A, __READ_WRITE, uint8_t);
#define DTS7DTE            (((volatile __bitf_T *)0xFFFF848A)->bit00)
#define DTS7SR             (((volatile __bitf_T *)0xFFFF848A)->bit01)
#define DTS7DR             (((volatile __bitf_T *)0xFFFF848A)->bit02)
#define DTS7ER             (((volatile __bitf_T *)0xFFFF848A)->bit03)
#define DTS7DT             (((volatile __bitf_T *)0xFFFF848A)->bit06)
#define DTS7TC             (((volatile __bitf_T *)0xFFFF848A)->bit07)
__IOREG(DSA8, 0xFFFF8514, __READ_WRITE, uint32_t);
__IOREG(DSA8L, 0xFFFF8514, __READ_WRITE, uint16_t);
__IOREG(DSA8H, 0xFFFF8516, __READ_WRITE, uint16_t);
__IOREG(DDA8, 0xFFFF8524, __READ_WRITE, uint32_t);
__IOREG(DDA8L, 0xFFFF8524, __READ_WRITE, uint16_t);
__IOREG(DDA8H, 0xFFFF8526, __READ_WRITE, uint16_t);
__IOREG(DTC8, 0xFFFF8532, __READ_WRITE, uint16_t);
__IOREG(DTCT8, 0xFFFF8538, __READ_WRITE, uint16_t);
__IOREG(DTS8, 0xFFFF853A, __READ_WRITE, uint8_t);
#define DTS8DTE            (((volatile __bitf_T *)0xFFFF853A)->bit00)
#define DTS8SR             (((volatile __bitf_T *)0xFFFF853A)->bit01)
#define DTS8DR             (((volatile __bitf_T *)0xFFFF853A)->bit02)
#define DTS8ER             (((volatile __bitf_T *)0xFFFF853A)->bit03)
#define DTS8DT             (((volatile __bitf_T *)0xFFFF853A)->bit06)
#define DTS8TC             (((volatile __bitf_T *)0xFFFF853A)->bit07)
__IOREG(DSA9, 0xFFFF8544, __READ_WRITE, uint32_t);
__IOREG(DSA9L, 0xFFFF8544, __READ_WRITE, uint16_t);
__IOREG(DSA9H, 0xFFFF8546, __READ_WRITE, uint16_t);
__IOREG(DDA9, 0xFFFF8554, __READ_WRITE, uint32_t);
__IOREG(DDA9L, 0xFFFF8554, __READ_WRITE, uint16_t);
__IOREG(DDA9H, 0xFFFF8556, __READ_WRITE, uint16_t);
__IOREG(DTC9, 0xFFFF8562, __READ_WRITE, uint16_t);
__IOREG(DTCT9, 0xFFFF8568, __READ_WRITE, uint16_t);
__IOREG(DTS9, 0xFFFF856A, __READ_WRITE, uint8_t);
#define DTS9DTE            (((volatile __bitf_T *)0xFFFF856A)->bit00)
#define DTS9SR             (((volatile __bitf_T *)0xFFFF856A)->bit01)
#define DTS9DR             (((volatile __bitf_T *)0xFFFF856A)->bit02)
#define DTS9ER             (((volatile __bitf_T *)0xFFFF856A)->bit03)
#define DTS9DT             (((volatile __bitf_T *)0xFFFF856A)->bit06)
#define DTS9TC             (((volatile __bitf_T *)0xFFFF856A)->bit07)
__IOREG(DSA10, 0xFFFF8574, __READ_WRITE, uint32_t);
__IOREG(DSA10L, 0xFFFF8574, __READ_WRITE, uint16_t);
__IOREG(DSA10H, 0xFFFF8576, __READ_WRITE, uint16_t);
__IOREG(DDA10, 0xFFFF8584, __READ_WRITE, uint32_t);
__IOREG(DDA10L, 0xFFFF8584, __READ_WRITE, uint16_t);
__IOREG(DDA10H, 0xFFFF8586, __READ_WRITE, uint16_t);
__IOREG(DTC10, 0xFFFF8592, __READ_WRITE, uint16_t);
__IOREG(DTCT10, 0xFFFF8598, __READ_WRITE, uint16_t);
__IOREG(DTS10, 0xFFFF859A, __READ_WRITE, uint8_t);
#define DTS10DTE           (((volatile __bitf_T *)0xFFFF859A)->bit00)
#define DTS10SR            (((volatile __bitf_T *)0xFFFF859A)->bit01)
#define DTS10DR            (((volatile __bitf_T *)0xFFFF859A)->bit02)
#define DTS10ER            (((volatile __bitf_T *)0xFFFF859A)->bit03)
#define DTS10DT            (((volatile __bitf_T *)0xFFFF859A)->bit06)
#define DTS10TC            (((volatile __bitf_T *)0xFFFF859A)->bit07)
__IOREG(DSA11, 0xFFFF85A4, __READ_WRITE, uint32_t);
__IOREG(DSA11L, 0xFFFF85A4, __READ_WRITE, uint16_t);
__IOREG(DSA11H, 0xFFFF85A6, __READ_WRITE, uint16_t);
__IOREG(DDA11, 0xFFFF85B4, __READ_WRITE, uint32_t);
__IOREG(DDA11L, 0xFFFF85B4, __READ_WRITE, uint16_t);
__IOREG(DDA11H, 0xFFFF85B6, __READ_WRITE, uint16_t);
__IOREG(DTC11, 0xFFFF85C2, __READ_WRITE, uint16_t);
__IOREG(DTCT11, 0xFFFF85C8, __READ_WRITE, uint16_t);
__IOREG(DTS11, 0xFFFF85CA, __READ_WRITE, uint8_t);
#define DTS11DTE           (((volatile __bitf_T *)0xFFFF85CA)->bit00)
#define DTS11SR            (((volatile __bitf_T *)0xFFFF85CA)->bit01)
#define DTS11DR            (((volatile __bitf_T *)0xFFFF85CA)->bit02)
#define DTS11ER            (((volatile __bitf_T *)0xFFFF85CA)->bit03)
#define DTS11DT            (((volatile __bitf_T *)0xFFFF85CA)->bit06)
#define DTS11TC            (((volatile __bitf_T *)0xFFFF85CA)->bit07)
__IOREG(DSA12, 0xFFFF85D4, __READ_WRITE, uint32_t);
__IOREG(DSA12L, 0xFFFF85D4, __READ_WRITE, uint16_t);
__IOREG(DSA12H, 0xFFFF85D6, __READ_WRITE, uint16_t);
__IOREG(DDA12, 0xFFFF85E4, __READ_WRITE, uint32_t);
__IOREG(DDA12L, 0xFFFF85E4, __READ_WRITE, uint16_t);
__IOREG(DDA12H, 0xFFFF85E6, __READ_WRITE, uint16_t);
__IOREG(DTC12, 0xFFFF85F2, __READ_WRITE, uint16_t);
__IOREG(DTCT12, 0xFFFF85F8, __READ_WRITE, uint16_t);
__IOREG(DTS12, 0xFFFF85FA, __READ_WRITE, uint8_t);
#define DTS12DTE           (((volatile __bitf_T *)0xFFFF85FA)->bit00)
#define DTS12SR            (((volatile __bitf_T *)0xFFFF85FA)->bit01)
#define DTS12DR            (((volatile __bitf_T *)0xFFFF85FA)->bit02)
#define DTS12ER            (((volatile __bitf_T *)0xFFFF85FA)->bit03)
#define DTS12DT            (((volatile __bitf_T *)0xFFFF85FA)->bit06)
#define DTS12TC            (((volatile __bitf_T *)0xFFFF85FA)->bit07)
__IOREG(DSA13, 0xFFFF8604, __READ_WRITE, uint32_t);
__IOREG(DSA13L, 0xFFFF8604, __READ_WRITE, uint16_t);
__IOREG(DSA13H, 0xFFFF8606, __READ_WRITE, uint16_t);
__IOREG(DDA13, 0xFFFF8614, __READ_WRITE, uint32_t);
__IOREG(DDA13L, 0xFFFF8614, __READ_WRITE, uint16_t);
__IOREG(DDA13H, 0xFFFF8616, __READ_WRITE, uint16_t);
__IOREG(DTC13, 0xFFFF8622, __READ_WRITE, uint16_t);
__IOREG(DTCT13, 0xFFFF8628, __READ_WRITE, uint16_t);
__IOREG(DTS13, 0xFFFF862A, __READ_WRITE, uint8_t);
#define DTS13DTE           (((volatile __bitf_T *)0xFFFF862A)->bit00)
#define DTS13SR            (((volatile __bitf_T *)0xFFFF862A)->bit01)
#define DTS13DR            (((volatile __bitf_T *)0xFFFF862A)->bit02)
#define DTS13ER            (((volatile __bitf_T *)0xFFFF862A)->bit03)
#define DTS13DT            (((volatile __bitf_T *)0xFFFF862A)->bit06)
#define DTS13TC            (((volatile __bitf_T *)0xFFFF862A)->bit07)
__IOREG(DSA14, 0xFFFF8634, __READ_WRITE, uint32_t);
__IOREG(DSA14L, 0xFFFF8634, __READ_WRITE, uint16_t);
__IOREG(DSA14H, 0xFFFF8636, __READ_WRITE, uint16_t);
__IOREG(DDA14, 0xFFFF8644, __READ_WRITE, uint32_t);
__IOREG(DDA14L, 0xFFFF8644, __READ_WRITE, uint16_t);
__IOREG(DDA14H, 0xFFFF8646, __READ_WRITE, uint16_t);
__IOREG(DTC14, 0xFFFF8652, __READ_WRITE, uint16_t);
__IOREG(DTCT14, 0xFFFF8658, __READ_WRITE, uint16_t);
__IOREG(DTS14, 0xFFFF865A, __READ_WRITE, uint8_t);
#define DTS14DTE           (((volatile __bitf_T *)0xFFFF865A)->bit00)
#define DTS14SR            (((volatile __bitf_T *)0xFFFF865A)->bit01)
#define DTS14DR            (((volatile __bitf_T *)0xFFFF865A)->bit02)
#define DTS14ER            (((volatile __bitf_T *)0xFFFF865A)->bit03)
#define DTS14DT            (((volatile __bitf_T *)0xFFFF865A)->bit06)
#define DTS14TC            (((volatile __bitf_T *)0xFFFF865A)->bit07)
__IOREG(DSA15, 0xFFFF8664, __READ_WRITE, uint32_t);
__IOREG(DSA15L, 0xFFFF8664, __READ_WRITE, uint16_t);
__IOREG(DSA15H, 0xFFFF8666, __READ_WRITE, uint16_t);
__IOREG(DDA15, 0xFFFF8674, __READ_WRITE, uint32_t);
__IOREG(DDA15L, 0xFFFF8674, __READ_WRITE, uint16_t);
__IOREG(DDA15H, 0xFFFF8676, __READ_WRITE, uint16_t);
__IOREG(DTC15, 0xFFFF8682, __READ_WRITE, uint16_t);
__IOREG(DTCT15, 0xFFFF8688, __READ_WRITE, uint16_t);
__IOREG(DTS15, 0xFFFF868A, __READ_WRITE, uint8_t);
#define DTS15DTE           (((volatile __bitf_T *)0xFFFF868A)->bit00)
#define DTS15SR            (((volatile __bitf_T *)0xFFFF868A)->bit01)
#define DTS15DR            (((volatile __bitf_T *)0xFFFF868A)->bit02)
#define DTS15ER            (((volatile __bitf_T *)0xFFFF868A)->bit03)
#define DTS15DT            (((volatile __bitf_T *)0xFFFF868A)->bit06)
#define DTS15TC            (((volatile __bitf_T *)0xFFFF868A)->bit07)
__IOREG(DTFR0, 0xFFFF8B00, __READ_WRITE, uint16_t);
__IOREG(DTFR1, 0xFFFF8B02, __READ_WRITE, uint16_t);
__IOREG(DTFR2, 0xFFFF8B04, __READ_WRITE, uint16_t);
__IOREG(DTFR3, 0xFFFF8B06, __READ_WRITE, uint16_t);
__IOREG(DTFR4, 0xFFFF8B08, __READ_WRITE, uint16_t);
__IOREG(DTFR5, 0xFFFF8B0A, __READ_WRITE, uint16_t);
__IOREG(DTFR6, 0xFFFF8B0C, __READ_WRITE, uint16_t);
__IOREG(DTFR7, 0xFFFF8B0E, __READ_WRITE, uint16_t);
__IOREG(DTFR8, 0xFFFF8B10, __READ_WRITE, uint16_t);
__IOREG(DTFR9, 0xFFFF8B12, __READ_WRITE, uint16_t);
__IOREG(DTFR10, 0xFFFF8B14, __READ_WRITE, uint16_t);
__IOREG(DTFR11, 0xFFFF8B16, __READ_WRITE, uint16_t);
__IOREG(DTFR12, 0xFFFF8B18, __READ_WRITE, uint16_t);
__IOREG(DTFR13, 0xFFFF8B1A, __READ_WRITE, uint16_t);
__IOREG(DTFR14, 0xFFFF8B1C, __READ_WRITE, uint16_t);
__IOREG(DTFR15, 0xFFFF8B1E, __READ_WRITE, uint16_t);
__IOREG(DRQCLR, 0xFFFF8B40, __READ_WRITE, uint16_t);
__IOREG(DRQSTR, 0xFFFF8B44, __READ, uint16_t);
__IOREG(SEG_CONT, 0xFFFF8C00, __READ_WRITE, uint16_t);
__IOREG(SEG_CONTL, 0xFFFF8C00, __READ_WRITE, uint8_t);
#define SEG_CONTROME       (((volatile __bitf_T *)0xFFFF8C00)->bit01)
#define SEG_CONTEXTE       (((volatile __bitf_T *)0xFFFF8C00)->bit02)
#define SEG_CONTRAME       (((volatile __bitf_T *)0xFFFF8C00)->bit04)
#define SEG_CONTRMWE       (((volatile __bitf_T *)0xFFFF8C00)->bit06)
#define SEG_CONTDMAE       (((volatile __bitf_T *)0xFFFF8C00)->bit07)
__IOREG(SEG_FLAG, 0xFFFF8C02, __READ_WRITE, uint16_t);
__IOREG(SEG_FLAGL, 0xFFFF8C02, __READ_WRITE, uint8_t);
#define SEG_FLAGROMF       (((volatile __bitf_T *)0xFFFF8C02)->bit01)
#define SEG_FLAGEXTF       (((volatile __bitf_T *)0xFFFF8C02)->bit02)
#define SEG_FLAGRAMF       (((volatile __bitf_T *)0xFFFF8C02)->bit04)
#define SEG_FLAGRMWF       (((volatile __bitf_T *)0xFFFF8C02)->bit06)
#define SEG_FLAGDMAF       (((volatile __bitf_T *)0xFFFF8C02)->bit07)
__IOREG(ICCSIH2IC_1, 0xFFFF9000, __READ_WRITE, uint16_t);
__IOREG(ICTAUD0I0, 0xFFFF9000, __READ_WRITE, uint16_t);
__IOREG(ICCSIH2ICL_1, 0xFFFF9000, __READ_WRITE, uint8_t);
__IOREG(ICTAUD0I0L, 0xFFFF9000, __READ_WRITE, uint8_t);
__IOREG(ICCSIH2ICH_1, 0xFFFF9001, __READ_WRITE, uint8_t);
__IOREG(ICTAUD0I0H, 0xFFFF9001, __READ_WRITE, uint8_t);
#define P0CSIH2IC_1        (((volatile __bitf_T *)0xFFFF9000)->bit00)
#define P0TAUD0I0          (((volatile __bitf_T *)0xFFFF9000)->bit00)
#define P1CSIH2IC_1        (((volatile __bitf_T *)0xFFFF9000)->bit01)
#define P1TAUD0I0          (((volatile __bitf_T *)0xFFFF9000)->bit01)
#define P2CSIH2IC_1        (((volatile __bitf_T *)0xFFFF9000)->bit02)
#define P2TAUD0I0          (((volatile __bitf_T *)0xFFFF9000)->bit02)
#define TBCSIH2IC_1        (((volatile __bitf_T *)0xFFFF9000)->bit06)
#define TBTAUD0I0          (((volatile __bitf_T *)0xFFFF9000)->bit06)
#define MKCSIH2IC_1        (((volatile __bitf_T *)0xFFFF9000)->bit07)
#define MKTAUD0I0          (((volatile __bitf_T *)0xFFFF9000)->bit07)
#define RFCSIH2IC_1        (((volatile __bitf_T *)0xFFFF9001)->bit04)
#define RFTAUD0I0          (((volatile __bitf_T *)0xFFFF9001)->bit04)
#define CTCSIH2IC_1        (((volatile __bitf_T *)0xFFFF9001)->bit07)
#define CTTAUD0I0          (((volatile __bitf_T *)0xFFFF9001)->bit07)
__IOREG(ICCSIH3IC_1, 0xFFFF9002, __READ_WRITE, uint16_t);
__IOREG(ICTAUD0I2, 0xFFFF9002, __READ_WRITE, uint16_t);
__IOREG(ICCSIH3ICL_1, 0xFFFF9002, __READ_WRITE, uint8_t);
__IOREG(ICTAUD0I2L, 0xFFFF9002, __READ_WRITE, uint8_t);
__IOREG(ICCSIH3ICH_1, 0xFFFF9003, __READ_WRITE, uint8_t);
__IOREG(ICTAUD0I2H, 0xFFFF9003, __READ_WRITE, uint8_t);
#define P0CSIH3IC_1        (((volatile __bitf_T *)0xFFFF9002)->bit00)
#define P0TAUD0I2          (((volatile __bitf_T *)0xFFFF9002)->bit00)
#define P1CSIH3IC_1        (((volatile __bitf_T *)0xFFFF9002)->bit01)
#define P1TAUD0I2          (((volatile __bitf_T *)0xFFFF9002)->bit01)
#define P2CSIH3IC_1        (((volatile __bitf_T *)0xFFFF9002)->bit02)
#define P2TAUD0I2          (((volatile __bitf_T *)0xFFFF9002)->bit02)
#define TBCSIH3IC_1        (((volatile __bitf_T *)0xFFFF9002)->bit06)
#define TBTAUD0I2          (((volatile __bitf_T *)0xFFFF9002)->bit06)
#define MKCSIH3IC_1        (((volatile __bitf_T *)0xFFFF9002)->bit07)
#define MKTAUD0I2          (((volatile __bitf_T *)0xFFFF9002)->bit07)
#define RFCSIH3IC_1        (((volatile __bitf_T *)0xFFFF9003)->bit04)
#define RFTAUD0I2          (((volatile __bitf_T *)0xFFFF9003)->bit04)
#define CTCSIH3IC_1        (((volatile __bitf_T *)0xFFFF9003)->bit07)
#define CTTAUD0I2          (((volatile __bitf_T *)0xFFFF9003)->bit07)
__IOREG(ICTAUD0I4, 0xFFFF9004, __READ_WRITE, uint16_t);
__IOREG(ICTAUD0I4L, 0xFFFF9004, __READ_WRITE, uint8_t);
__IOREG(ICTAUD0I4H, 0xFFFF9005, __READ_WRITE, uint8_t);
#define P0TAUD0I4          (((volatile __bitf_T *)0xFFFF9004)->bit00)
#define P1TAUD0I4          (((volatile __bitf_T *)0xFFFF9004)->bit01)
#define P2TAUD0I4          (((volatile __bitf_T *)0xFFFF9004)->bit02)
#define TBTAUD0I4          (((volatile __bitf_T *)0xFFFF9004)->bit06)
#define MKTAUD0I4          (((volatile __bitf_T *)0xFFFF9004)->bit07)
#define RFTAUD0I4          (((volatile __bitf_T *)0xFFFF9005)->bit04)
#define CTTAUD0I4          (((volatile __bitf_T *)0xFFFF9005)->bit07)
__IOREG(ICTAUD0I6, 0xFFFF9006, __READ_WRITE, uint16_t);
__IOREG(ICTAUD0I6L, 0xFFFF9006, __READ_WRITE, uint8_t);
__IOREG(ICTAUD0I6H, 0xFFFF9007, __READ_WRITE, uint8_t);
#define P0TAUD0I6          (((volatile __bitf_T *)0xFFFF9006)->bit00)
#define P1TAUD0I6          (((volatile __bitf_T *)0xFFFF9006)->bit01)
#define P2TAUD0I6          (((volatile __bitf_T *)0xFFFF9006)->bit02)
#define TBTAUD0I6          (((volatile __bitf_T *)0xFFFF9006)->bit06)
#define MKTAUD0I6          (((volatile __bitf_T *)0xFFFF9006)->bit07)
#define RFTAUD0I6          (((volatile __bitf_T *)0xFFFF9007)->bit04)
#define CTTAUD0I6          (((volatile __bitf_T *)0xFFFF9007)->bit07)
__IOREG(ICTAUD0I8, 0xFFFF9008, __READ_WRITE, uint16_t);
__IOREG(ICTAUD0I8L, 0xFFFF9008, __READ_WRITE, uint8_t);
__IOREG(ICTAUD0I8H, 0xFFFF9009, __READ_WRITE, uint8_t);
#define P0TAUD0I8          (((volatile __bitf_T *)0xFFFF9008)->bit00)
#define P1TAUD0I8          (((volatile __bitf_T *)0xFFFF9008)->bit01)
#define P2TAUD0I8          (((volatile __bitf_T *)0xFFFF9008)->bit02)
#define TBTAUD0I8          (((volatile __bitf_T *)0xFFFF9008)->bit06)
#define MKTAUD0I8          (((volatile __bitf_T *)0xFFFF9008)->bit07)
#define RFTAUD0I8          (((volatile __bitf_T *)0xFFFF9009)->bit04)
#define CTTAUD0I8          (((volatile __bitf_T *)0xFFFF9009)->bit07)
__IOREG(ICCSIH3IR_1, 0xFFFF900A, __READ_WRITE, uint16_t);
__IOREG(ICTAUD0I10, 0xFFFF900A, __READ_WRITE, uint16_t);
__IOREG(ICCSIH3IRL_1, 0xFFFF900A, __READ_WRITE, uint8_t);
__IOREG(ICTAUD0I10L, 0xFFFF900A, __READ_WRITE, uint8_t);
__IOREG(ICCSIH3IRH_1, 0xFFFF900B, __READ_WRITE, uint8_t);
__IOREG(ICTAUD0I10H, 0xFFFF900B, __READ_WRITE, uint8_t);
#define P0CSIH3IR_1        (((volatile __bitf_T *)0xFFFF900A)->bit00)
#define P0TAUD0I10         (((volatile __bitf_T *)0xFFFF900A)->bit00)
#define P1CSIH3IR_1        (((volatile __bitf_T *)0xFFFF900A)->bit01)
#define P1TAUD0I10         (((volatile __bitf_T *)0xFFFF900A)->bit01)
#define P2CSIH3IR_1        (((volatile __bitf_T *)0xFFFF900A)->bit02)
#define P2TAUD0I10         (((volatile __bitf_T *)0xFFFF900A)->bit02)
#define TBCSIH3IR_1        (((volatile __bitf_T *)0xFFFF900A)->bit06)
#define TBTAUD0I10         (((volatile __bitf_T *)0xFFFF900A)->bit06)
#define MKCSIH3IR_1        (((volatile __bitf_T *)0xFFFF900A)->bit07)
#define MKTAUD0I10         (((volatile __bitf_T *)0xFFFF900A)->bit07)
#define RFCSIH3IR_1        (((volatile __bitf_T *)0xFFFF900B)->bit04)
#define RFTAUD0I10         (((volatile __bitf_T *)0xFFFF900B)->bit04)
#define CTCSIH3IR_1        (((volatile __bitf_T *)0xFFFF900B)->bit07)
#define CTTAUD0I10         (((volatile __bitf_T *)0xFFFF900B)->bit07)
__IOREG(ICCSIH3IRE_1, 0xFFFF900C, __READ_WRITE, uint16_t);
__IOREG(ICTAUD0I12, 0xFFFF900C, __READ_WRITE, uint16_t);
__IOREG(ICCSIH3IREL_1, 0xFFFF900C, __READ_WRITE, uint8_t);
__IOREG(ICTAUD0I12L, 0xFFFF900C, __READ_WRITE, uint8_t);
__IOREG(ICCSIH3IREH_1, 0xFFFF900D, __READ_WRITE, uint8_t);
__IOREG(ICTAUD0I12H, 0xFFFF900D, __READ_WRITE, uint8_t);
#define P0CSIH3IRE_1       (((volatile __bitf_T *)0xFFFF900C)->bit00)
#define P0TAUD0I12         (((volatile __bitf_T *)0xFFFF900C)->bit00)
#define P1CSIH3IRE_1       (((volatile __bitf_T *)0xFFFF900C)->bit01)
#define P1TAUD0I12         (((volatile __bitf_T *)0xFFFF900C)->bit01)
#define P2CSIH3IRE_1       (((volatile __bitf_T *)0xFFFF900C)->bit02)
#define P2TAUD0I12         (((volatile __bitf_T *)0xFFFF900C)->bit02)
#define TBCSIH3IRE_1       (((volatile __bitf_T *)0xFFFF900C)->bit06)
#define TBTAUD0I12         (((volatile __bitf_T *)0xFFFF900C)->bit06)
#define MKCSIH3IRE_1       (((volatile __bitf_T *)0xFFFF900C)->bit07)
#define MKTAUD0I12         (((volatile __bitf_T *)0xFFFF900C)->bit07)
#define RFCSIH3IRE_1       (((volatile __bitf_T *)0xFFFF900D)->bit04)
#define RFTAUD0I12         (((volatile __bitf_T *)0xFFFF900D)->bit04)
#define CTCSIH3IRE_1       (((volatile __bitf_T *)0xFFFF900D)->bit07)
#define CTTAUD0I12         (((volatile __bitf_T *)0xFFFF900D)->bit07)
__IOREG(ICCSIH3IJC_1, 0xFFFF900E, __READ_WRITE, uint16_t);
__IOREG(ICTAUD0I14, 0xFFFF900E, __READ_WRITE, uint16_t);
__IOREG(ICCSIH3IJCL_1, 0xFFFF900E, __READ_WRITE, uint8_t);
__IOREG(ICTAUD0I14L, 0xFFFF900E, __READ_WRITE, uint8_t);
__IOREG(ICCSIH3IJCH_1, 0xFFFF900F, __READ_WRITE, uint8_t);
__IOREG(ICTAUD0I14H, 0xFFFF900F, __READ_WRITE, uint8_t);
#define P0CSIH3IJC_1       (((volatile __bitf_T *)0xFFFF900E)->bit00)
#define P0TAUD0I14         (((volatile __bitf_T *)0xFFFF900E)->bit00)
#define P1CSIH3IJC_1       (((volatile __bitf_T *)0xFFFF900E)->bit01)
#define P1TAUD0I14         (((volatile __bitf_T *)0xFFFF900E)->bit01)
#define P2CSIH3IJC_1       (((volatile __bitf_T *)0xFFFF900E)->bit02)
#define P2TAUD0I14         (((volatile __bitf_T *)0xFFFF900E)->bit02)
#define TBCSIH3IJC_1       (((volatile __bitf_T *)0xFFFF900E)->bit06)
#define TBTAUD0I14         (((volatile __bitf_T *)0xFFFF900E)->bit06)
#define MKCSIH3IJC_1       (((volatile __bitf_T *)0xFFFF900E)->bit07)
#define MKTAUD0I14         (((volatile __bitf_T *)0xFFFF900E)->bit07)
#define RFCSIH3IJC_1       (((volatile __bitf_T *)0xFFFF900F)->bit04)
#define RFTAUD0I14         (((volatile __bitf_T *)0xFFFF900F)->bit04)
#define CTCSIH3IJC_1       (((volatile __bitf_T *)0xFFFF900F)->bit07)
#define CTTAUD0I14         (((volatile __bitf_T *)0xFFFF900F)->bit07)
__IOREG(ICCSIH1IC_1, 0xFFFF9010, __READ_WRITE, uint16_t);
__IOREG(ICTAPA0IPEK0, 0xFFFF9010, __READ_WRITE, uint16_t);
__IOREG(ICCSIH1ICL_1, 0xFFFF9010, __READ_WRITE, uint8_t);
__IOREG(ICTAPA0IPEK0L, 0xFFFF9010, __READ_WRITE, uint8_t);
__IOREG(ICCSIH1ICH_1, 0xFFFF9011, __READ_WRITE, uint8_t);
__IOREG(ICTAPA0IPEK0H, 0xFFFF9011, __READ_WRITE, uint8_t);
#define P0CSIH1IC_1        (((volatile __bitf_T *)0xFFFF9010)->bit00)
#define P0TAPA0IPEK0       (((volatile __bitf_T *)0xFFFF9010)->bit00)
#define P1CSIH1IC_1        (((volatile __bitf_T *)0xFFFF9010)->bit01)
#define P1TAPA0IPEK0       (((volatile __bitf_T *)0xFFFF9010)->bit01)
#define P2CSIH1IC_1        (((volatile __bitf_T *)0xFFFF9010)->bit02)
#define P2TAPA0IPEK0       (((volatile __bitf_T *)0xFFFF9010)->bit02)
#define TBCSIH1IC_1        (((volatile __bitf_T *)0xFFFF9010)->bit06)
#define TBTAPA0IPEK0       (((volatile __bitf_T *)0xFFFF9010)->bit06)
#define MKCSIH1IC_1        (((volatile __bitf_T *)0xFFFF9010)->bit07)
#define MKTAPA0IPEK0       (((volatile __bitf_T *)0xFFFF9010)->bit07)
#define RFCSIH1IC_1        (((volatile __bitf_T *)0xFFFF9011)->bit04)
#define RFTAPA0IPEK0       (((volatile __bitf_T *)0xFFFF9011)->bit04)
#define CTCSIH1IC_1        (((volatile __bitf_T *)0xFFFF9011)->bit07)
#define CTTAPA0IPEK0       (((volatile __bitf_T *)0xFFFF9011)->bit07)
__IOREG(ICCSIH1IR_1, 0xFFFF9012, __READ_WRITE, uint16_t);
__IOREG(ICTAPA0IVLY0, 0xFFFF9012, __READ_WRITE, uint16_t);
__IOREG(ICCSIH1IRL_1, 0xFFFF9012, __READ_WRITE, uint8_t);
__IOREG(ICTAPA0IVLY0L, 0xFFFF9012, __READ_WRITE, uint8_t);
__IOREG(ICCSIH1IRH_1, 0xFFFF9013, __READ_WRITE, uint8_t);
__IOREG(ICTAPA0IVLY0H, 0xFFFF9013, __READ_WRITE, uint8_t);
#define P0CSIH1IR_1        (((volatile __bitf_T *)0xFFFF9012)->bit00)
#define P0TAPA0IVLY0       (((volatile __bitf_T *)0xFFFF9012)->bit00)
#define P1CSIH1IR_1        (((volatile __bitf_T *)0xFFFF9012)->bit01)
#define P1TAPA0IVLY0       (((volatile __bitf_T *)0xFFFF9012)->bit01)
#define P2CSIH1IR_1        (((volatile __bitf_T *)0xFFFF9012)->bit02)
#define P2TAPA0IVLY0       (((volatile __bitf_T *)0xFFFF9012)->bit02)
#define TBCSIH1IR_1        (((volatile __bitf_T *)0xFFFF9012)->bit06)
#define TBTAPA0IVLY0       (((volatile __bitf_T *)0xFFFF9012)->bit06)
#define MKCSIH1IR_1        (((volatile __bitf_T *)0xFFFF9012)->bit07)
#define MKTAPA0IVLY0       (((volatile __bitf_T *)0xFFFF9012)->bit07)
#define RFCSIH1IR_1        (((volatile __bitf_T *)0xFFFF9013)->bit04)
#define RFTAPA0IVLY0       (((volatile __bitf_T *)0xFFFF9013)->bit04)
#define CTCSIH1IR_1        (((volatile __bitf_T *)0xFFFF9013)->bit07)
#define CTTAPA0IVLY0       (((volatile __bitf_T *)0xFFFF9013)->bit07)
__IOREG(ICADCA0I0, 0xFFFF9014, __READ_WRITE, uint16_t);
__IOREG(ICADCA0I0L, 0xFFFF9014, __READ_WRITE, uint8_t);
__IOREG(ICADCA0I0H, 0xFFFF9015, __READ_WRITE, uint8_t);
#define P0ADCA0I0          (((volatile __bitf_T *)0xFFFF9014)->bit00)
#define P1ADCA0I0          (((volatile __bitf_T *)0xFFFF9014)->bit01)
#define P2ADCA0I0          (((volatile __bitf_T *)0xFFFF9014)->bit02)
#define TBADCA0I0          (((volatile __bitf_T *)0xFFFF9014)->bit06)
#define MKADCA0I0          (((volatile __bitf_T *)0xFFFF9014)->bit07)
#define RFADCA0I0          (((volatile __bitf_T *)0xFFFF9015)->bit04)
#define CTADCA0I0          (((volatile __bitf_T *)0xFFFF9015)->bit07)
__IOREG(ICADCA0I1, 0xFFFF9016, __READ_WRITE, uint16_t);
__IOREG(ICADCA0I1L, 0xFFFF9016, __READ_WRITE, uint8_t);
__IOREG(ICADCA0I1H, 0xFFFF9017, __READ_WRITE, uint8_t);
#define P0ADCA0I1          (((volatile __bitf_T *)0xFFFF9016)->bit00)
#define P1ADCA0I1          (((volatile __bitf_T *)0xFFFF9016)->bit01)
#define P2ADCA0I1          (((volatile __bitf_T *)0xFFFF9016)->bit02)
#define TBADCA0I1          (((volatile __bitf_T *)0xFFFF9016)->bit06)
#define MKADCA0I1          (((volatile __bitf_T *)0xFFFF9016)->bit07)
#define RFADCA0I1          (((volatile __bitf_T *)0xFFFF9017)->bit04)
#define CTADCA0I1          (((volatile __bitf_T *)0xFFFF9017)->bit07)
__IOREG(ICADCA0I2, 0xFFFF9018, __READ_WRITE, uint16_t);
__IOREG(ICADCA0I2L, 0xFFFF9018, __READ_WRITE, uint8_t);
__IOREG(ICADCA0I2H, 0xFFFF9019, __READ_WRITE, uint8_t);
#define P0ADCA0I2          (((volatile __bitf_T *)0xFFFF9018)->bit00)
#define P1ADCA0I2          (((volatile __bitf_T *)0xFFFF9018)->bit01)
#define P2ADCA0I2          (((volatile __bitf_T *)0xFFFF9018)->bit02)
#define TBADCA0I2          (((volatile __bitf_T *)0xFFFF9018)->bit06)
#define MKADCA0I2          (((volatile __bitf_T *)0xFFFF9018)->bit07)
#define RFADCA0I2          (((volatile __bitf_T *)0xFFFF9019)->bit04)
#define CTADCA0I2          (((volatile __bitf_T *)0xFFFF9019)->bit07)
__IOREG(ICDCUTDI, 0xFFFF901A, __READ_WRITE, uint16_t);
__IOREG(ICDCUTDIL, 0xFFFF901A, __READ_WRITE, uint8_t);
__IOREG(ICDCUTDIH, 0xFFFF901B, __READ_WRITE, uint8_t);
#define P0DCUTDI           (((volatile __bitf_T *)0xFFFF901A)->bit00)
#define P1DCUTDI           (((volatile __bitf_T *)0xFFFF901A)->bit01)
#define P2DCUTDI           (((volatile __bitf_T *)0xFFFF901A)->bit02)
#define TBDCUTDI           (((volatile __bitf_T *)0xFFFF901A)->bit06)
#define MKDCUTDI           (((volatile __bitf_T *)0xFFFF901A)->bit07)
#define RFDCUTDI           (((volatile __bitf_T *)0xFFFF901B)->bit04)
#define CTDCUTDI           (((volatile __bitf_T *)0xFFFF901B)->bit07)
__IOREG(ICRCANGERR, 0xFFFF901C, __READ_WRITE, uint16_t);
__IOREG(ICRCANGERRL, 0xFFFF901C, __READ_WRITE, uint8_t);
__IOREG(ICRCANGERRH, 0xFFFF901D, __READ_WRITE, uint8_t);
#define P0RCANGERR         (((volatile __bitf_T *)0xFFFF901C)->bit00)
#define P1RCANGERR         (((volatile __bitf_T *)0xFFFF901C)->bit01)
#define P2RCANGERR         (((volatile __bitf_T *)0xFFFF901C)->bit02)
#define TBRCANGERR         (((volatile __bitf_T *)0xFFFF901C)->bit06)
#define MKRCANGERR         (((volatile __bitf_T *)0xFFFF901C)->bit07)
#define RFRCANGERR         (((volatile __bitf_T *)0xFFFF901D)->bit04)
#define CTRCANGERR         (((volatile __bitf_T *)0xFFFF901D)->bit07)
__IOREG(ICRCANGRECC, 0xFFFF901E, __READ_WRITE, uint16_t);
__IOREG(ICRCANGRECCL, 0xFFFF901E, __READ_WRITE, uint8_t);
__IOREG(ICRCANGRECCH, 0xFFFF901F, __READ_WRITE, uint8_t);
#define P0RCANGRECC        (((volatile __bitf_T *)0xFFFF901E)->bit00)
#define P1RCANGRECC        (((volatile __bitf_T *)0xFFFF901E)->bit01)
#define P2RCANGRECC        (((volatile __bitf_T *)0xFFFF901E)->bit02)
#define TBRCANGRECC        (((volatile __bitf_T *)0xFFFF901E)->bit06)
#define MKRCANGRECC        (((volatile __bitf_T *)0xFFFF901E)->bit07)
#define RFRCANGRECC        (((volatile __bitf_T *)0xFFFF901F)->bit04)
#define CTRCANGRECC        (((volatile __bitf_T *)0xFFFF901F)->bit07)
__IOREG(ICRCAN0ERR, 0xFFFF9020, __READ_WRITE, uint16_t);
__IOREG(ICRCAN0ERRL, 0xFFFF9020, __READ_WRITE, uint8_t);
__IOREG(ICRCAN0ERRH, 0xFFFF9021, __READ_WRITE, uint8_t);
#define P0RCAN0ERR         (((volatile __bitf_T *)0xFFFF9020)->bit00)
#define P1RCAN0ERR         (((volatile __bitf_T *)0xFFFF9020)->bit01)
#define P2RCAN0ERR         (((volatile __bitf_T *)0xFFFF9020)->bit02)
#define TBRCAN0ERR         (((volatile __bitf_T *)0xFFFF9020)->bit06)
#define MKRCAN0ERR         (((volatile __bitf_T *)0xFFFF9020)->bit07)
#define RFRCAN0ERR         (((volatile __bitf_T *)0xFFFF9021)->bit04)
#define CTRCAN0ERR         (((volatile __bitf_T *)0xFFFF9021)->bit07)
__IOREG(ICRCAN0REC, 0xFFFF9022, __READ_WRITE, uint16_t);
__IOREG(ICRCAN0RECL, 0xFFFF9022, __READ_WRITE, uint8_t);
__IOREG(ICRCAN0RECH, 0xFFFF9023, __READ_WRITE, uint8_t);
#define P0RCAN0REC         (((volatile __bitf_T *)0xFFFF9022)->bit00)
#define P1RCAN0REC         (((volatile __bitf_T *)0xFFFF9022)->bit01)
#define P2RCAN0REC         (((volatile __bitf_T *)0xFFFF9022)->bit02)
#define TBRCAN0REC         (((volatile __bitf_T *)0xFFFF9022)->bit06)
#define MKRCAN0REC         (((volatile __bitf_T *)0xFFFF9022)->bit07)
#define RFRCAN0REC         (((volatile __bitf_T *)0xFFFF9023)->bit04)
#define CTRCAN0REC         (((volatile __bitf_T *)0xFFFF9023)->bit07)
__IOREG(ICRCAN0TRX, 0xFFFF9024, __READ_WRITE, uint16_t);
__IOREG(ICRCAN0TRXL, 0xFFFF9024, __READ_WRITE, uint8_t);
__IOREG(ICRCAN0TRXH, 0xFFFF9025, __READ_WRITE, uint8_t);
#define P0RCAN0TRX         (((volatile __bitf_T *)0xFFFF9024)->bit00)
#define P1RCAN0TRX         (((volatile __bitf_T *)0xFFFF9024)->bit01)
#define P2RCAN0TRX         (((volatile __bitf_T *)0xFFFF9024)->bit02)
#define TBRCAN0TRX         (((volatile __bitf_T *)0xFFFF9024)->bit06)
#define MKRCAN0TRX         (((volatile __bitf_T *)0xFFFF9024)->bit07)
#define RFRCAN0TRX         (((volatile __bitf_T *)0xFFFF9025)->bit04)
#define CTRCAN0TRX         (((volatile __bitf_T *)0xFFFF9025)->bit07)
__IOREG(ICCSIG0IC, 0xFFFF9026, __READ_WRITE, uint16_t);
__IOREG(ICCSIH1IRE_1, 0xFFFF9026, __READ_WRITE, uint16_t);
__IOREG(ICCSIG0ICL, 0xFFFF9026, __READ_WRITE, uint8_t);
__IOREG(ICCSIH1IREL_1, 0xFFFF9026, __READ_WRITE, uint8_t);
__IOREG(ICCSIG0ICH, 0xFFFF9027, __READ_WRITE, uint8_t);
__IOREG(ICCSIH1IREH_1, 0xFFFF9027, __READ_WRITE, uint8_t);
#define P0CSIG0IC          (((volatile __bitf_T *)0xFFFF9026)->bit00)
#define P0CSIH1IRE_1       (((volatile __bitf_T *)0xFFFF9026)->bit00)
#define P1CSIG0IC          (((volatile __bitf_T *)0xFFFF9026)->bit01)
#define P1CSIH1IRE_1       (((volatile __bitf_T *)0xFFFF9026)->bit01)
#define P2CSIG0IC          (((volatile __bitf_T *)0xFFFF9026)->bit02)
#define P2CSIH1IRE_1       (((volatile __bitf_T *)0xFFFF9026)->bit02)
#define TBCSIG0IC          (((volatile __bitf_T *)0xFFFF9026)->bit06)
#define TBCSIH1IRE_1       (((volatile __bitf_T *)0xFFFF9026)->bit06)
#define MKCSIG0IC          (((volatile __bitf_T *)0xFFFF9026)->bit07)
#define MKCSIH1IRE_1       (((volatile __bitf_T *)0xFFFF9026)->bit07)
#define RFCSIG0IC          (((volatile __bitf_T *)0xFFFF9027)->bit04)
#define RFCSIH1IRE_1       (((volatile __bitf_T *)0xFFFF9027)->bit04)
#define CTCSIG0IC          (((volatile __bitf_T *)0xFFFF9027)->bit07)
#define CTCSIH1IRE_1       (((volatile __bitf_T *)0xFFFF9027)->bit07)
__IOREG(ICCSIG0IR, 0xFFFF9028, __READ_WRITE, uint16_t);
__IOREG(ICCSIH1IJC_1, 0xFFFF9028, __READ_WRITE, uint16_t);
__IOREG(ICCSIG0IRL, 0xFFFF9028, __READ_WRITE, uint8_t);
__IOREG(ICCSIH1IJCL_1, 0xFFFF9028, __READ_WRITE, uint8_t);
__IOREG(ICCSIG0IRH, 0xFFFF9029, __READ_WRITE, uint8_t);
__IOREG(ICCSIH1IJCH_1, 0xFFFF9029, __READ_WRITE, uint8_t);
#define P0CSIG0IR          (((volatile __bitf_T *)0xFFFF9028)->bit00)
#define P0CSIH1IJC_1       (((volatile __bitf_T *)0xFFFF9028)->bit00)
#define P1CSIG0IR          (((volatile __bitf_T *)0xFFFF9028)->bit01)
#define P1CSIH1IJC_1       (((volatile __bitf_T *)0xFFFF9028)->bit01)
#define P2CSIG0IR          (((volatile __bitf_T *)0xFFFF9028)->bit02)
#define P2CSIH1IJC_1       (((volatile __bitf_T *)0xFFFF9028)->bit02)
#define TBCSIG0IR          (((volatile __bitf_T *)0xFFFF9028)->bit06)
#define TBCSIH1IJC_1       (((volatile __bitf_T *)0xFFFF9028)->bit06)
#define MKCSIG0IR          (((volatile __bitf_T *)0xFFFF9028)->bit07)
#define MKCSIH1IJC_1       (((volatile __bitf_T *)0xFFFF9028)->bit07)
#define RFCSIG0IR          (((volatile __bitf_T *)0xFFFF9029)->bit04)
#define RFCSIH1IJC_1       (((volatile __bitf_T *)0xFFFF9029)->bit04)
#define CTCSIG0IR          (((volatile __bitf_T *)0xFFFF9029)->bit07)
#define CTCSIH1IJC_1       (((volatile __bitf_T *)0xFFFF9029)->bit07)
__IOREG(ICCSIH0IC, 0xFFFF902A, __READ_WRITE, uint16_t);
__IOREG(ICCSIH0ICL, 0xFFFF902A, __READ_WRITE, uint8_t);
__IOREG(ICCSIH0ICH, 0xFFFF902B, __READ_WRITE, uint8_t);
#define P0CSIH0IC          (((volatile __bitf_T *)0xFFFF902A)->bit00)
#define P1CSIH0IC          (((volatile __bitf_T *)0xFFFF902A)->bit01)
#define P2CSIH0IC          (((volatile __bitf_T *)0xFFFF902A)->bit02)
#define TBCSIH0IC          (((volatile __bitf_T *)0xFFFF902A)->bit06)
#define MKCSIH0IC          (((volatile __bitf_T *)0xFFFF902A)->bit07)
#define RFCSIH0IC          (((volatile __bitf_T *)0xFFFF902B)->bit04)
#define CTCSIH0IC          (((volatile __bitf_T *)0xFFFF902B)->bit07)
__IOREG(ICCSIH0IR, 0xFFFF902C, __READ_WRITE, uint16_t);
__IOREG(ICCSIH0IRL, 0xFFFF902C, __READ_WRITE, uint8_t);
__IOREG(ICCSIH0IRH, 0xFFFF902D, __READ_WRITE, uint8_t);
#define P0CSIH0IR          (((volatile __bitf_T *)0xFFFF902C)->bit00)
#define P1CSIH0IR          (((volatile __bitf_T *)0xFFFF902C)->bit01)
#define P2CSIH0IR          (((volatile __bitf_T *)0xFFFF902C)->bit02)
#define TBCSIH0IR          (((volatile __bitf_T *)0xFFFF902C)->bit06)
#define MKCSIH0IR          (((volatile __bitf_T *)0xFFFF902C)->bit07)
#define RFCSIH0IR          (((volatile __bitf_T *)0xFFFF902D)->bit04)
#define CTCSIH0IR          (((volatile __bitf_T *)0xFFFF902D)->bit07)
__IOREG(ICCSIH0IRE, 0xFFFF902E, __READ_WRITE, uint16_t);
__IOREG(ICCSIH0IREL, 0xFFFF902E, __READ_WRITE, uint8_t);
__IOREG(ICCSIH0IREH, 0xFFFF902F, __READ_WRITE, uint8_t);
#define P0CSIH0IRE         (((volatile __bitf_T *)0xFFFF902E)->bit00)
#define P1CSIH0IRE         (((volatile __bitf_T *)0xFFFF902E)->bit01)
#define P2CSIH0IRE         (((volatile __bitf_T *)0xFFFF902E)->bit02)
#define TBCSIH0IRE         (((volatile __bitf_T *)0xFFFF902E)->bit06)
#define MKCSIH0IRE         (((volatile __bitf_T *)0xFFFF902E)->bit07)
#define RFCSIH0IRE         (((volatile __bitf_T *)0xFFFF902F)->bit04)
#define CTCSIH0IRE         (((volatile __bitf_T *)0xFFFF902F)->bit07)
__IOREG(ICCSIH0IJC, 0xFFFF9030, __READ_WRITE, uint16_t);
__IOREG(ICCSIH0IJCL, 0xFFFF9030, __READ_WRITE, uint8_t);
__IOREG(ICCSIH0IJCH, 0xFFFF9031, __READ_WRITE, uint8_t);
#define P0CSIH0IJC         (((volatile __bitf_T *)0xFFFF9030)->bit00)
#define P1CSIH0IJC         (((volatile __bitf_T *)0xFFFF9030)->bit01)
#define P2CSIH0IJC         (((volatile __bitf_T *)0xFFFF9030)->bit02)
#define TBCSIH0IJC         (((volatile __bitf_T *)0xFFFF9030)->bit06)
#define MKCSIH0IJC         (((volatile __bitf_T *)0xFFFF9030)->bit07)
#define RFCSIH0IJC         (((volatile __bitf_T *)0xFFFF9031)->bit04)
#define CTCSIH0IJC         (((volatile __bitf_T *)0xFFFF9031)->bit07)
__IOREG(ICRLIN30, 0xFFFF9032, __READ_WRITE, uint16_t);
__IOREG(ICRLIN30L, 0xFFFF9032, __READ_WRITE, uint8_t);
__IOREG(ICRLIN30H, 0xFFFF9033, __READ_WRITE, uint8_t);
#define P0RLIN30           (((volatile __bitf_T *)0xFFFF9032)->bit00)
#define P1RLIN30           (((volatile __bitf_T *)0xFFFF9032)->bit01)
#define P2RLIN30           (((volatile __bitf_T *)0xFFFF9032)->bit02)
#define TBRLIN30           (((volatile __bitf_T *)0xFFFF9032)->bit06)
#define MKRLIN30           (((volatile __bitf_T *)0xFFFF9032)->bit07)
#define RFRLIN30           (((volatile __bitf_T *)0xFFFF9033)->bit04)
#define CTRLIN30           (((volatile __bitf_T *)0xFFFF9033)->bit07)
__IOREG(ICRLIN30UR0, 0xFFFF9034, __READ_WRITE, uint16_t);
__IOREG(ICRLIN30UR0L, 0xFFFF9034, __READ_WRITE, uint8_t);
__IOREG(ICRLIN30UR0H, 0xFFFF9035, __READ_WRITE, uint8_t);
#define P0RLIN30UR0        (((volatile __bitf_T *)0xFFFF9034)->bit00)
#define P1RLIN30UR0        (((volatile __bitf_T *)0xFFFF9034)->bit01)
#define P2RLIN30UR0        (((volatile __bitf_T *)0xFFFF9034)->bit02)
#define TBRLIN30UR0        (((volatile __bitf_T *)0xFFFF9034)->bit06)
#define MKRLIN30UR0        (((volatile __bitf_T *)0xFFFF9034)->bit07)
#define RFRLIN30UR0        (((volatile __bitf_T *)0xFFFF9035)->bit04)
#define CTRLIN30UR0        (((volatile __bitf_T *)0xFFFF9035)->bit07)
__IOREG(ICRLIN30UR1, 0xFFFF9036, __READ_WRITE, uint16_t);
__IOREG(ICRLIN30UR1L, 0xFFFF9036, __READ_WRITE, uint8_t);
__IOREG(ICRLIN30UR1H, 0xFFFF9037, __READ_WRITE, uint8_t);
#define P0RLIN30UR1        (((volatile __bitf_T *)0xFFFF9036)->bit00)
#define P1RLIN30UR1        (((volatile __bitf_T *)0xFFFF9036)->bit01)
#define P2RLIN30UR1        (((volatile __bitf_T *)0xFFFF9036)->bit02)
#define TBRLIN30UR1        (((volatile __bitf_T *)0xFFFF9036)->bit06)
#define MKRLIN30UR1        (((volatile __bitf_T *)0xFFFF9036)->bit07)
#define RFRLIN30UR1        (((volatile __bitf_T *)0xFFFF9037)->bit04)
#define CTRLIN30UR1        (((volatile __bitf_T *)0xFFFF9037)->bit07)
__IOREG(ICRLIN30UR2, 0xFFFF9038, __READ_WRITE, uint16_t);
__IOREG(ICRLIN30UR2L, 0xFFFF9038, __READ_WRITE, uint8_t);
__IOREG(ICRLIN30UR2H, 0xFFFF9039, __READ_WRITE, uint8_t);
#define P0RLIN30UR2        (((volatile __bitf_T *)0xFFFF9038)->bit00)
#define P1RLIN30UR2        (((volatile __bitf_T *)0xFFFF9038)->bit01)
#define P2RLIN30UR2        (((volatile __bitf_T *)0xFFFF9038)->bit02)
#define TBRLIN30UR2        (((volatile __bitf_T *)0xFFFF9038)->bit06)
#define MKRLIN30UR2        (((volatile __bitf_T *)0xFFFF9038)->bit07)
#define RFRLIN30UR2        (((volatile __bitf_T *)0xFFFF9039)->bit04)
#define CTRLIN30UR2        (((volatile __bitf_T *)0xFFFF9039)->bit07)
__IOREG(ICCSIH2IR_1, 0xFFFF903A, __READ_WRITE, uint16_t);
__IOREG(ICP0, 0xFFFF903A, __READ_WRITE, uint16_t);
__IOREG(ICCSIH2IRL_1, 0xFFFF903A, __READ_WRITE, uint8_t);
__IOREG(ICP0L, 0xFFFF903A, __READ_WRITE, uint8_t);
__IOREG(ICCSIH2IRH_1, 0xFFFF903B, __READ_WRITE, uint8_t);
__IOREG(ICP0H, 0xFFFF903B, __READ_WRITE, uint8_t);
#define P0CSIH2IR_1        (((volatile __bitf_T *)0xFFFF903A)->bit00)
#define P0P0               (((volatile __bitf_T *)0xFFFF903A)->bit00)
#define P1CSIH2IR_1        (((volatile __bitf_T *)0xFFFF903A)->bit01)
#define P1P0               (((volatile __bitf_T *)0xFFFF903A)->bit01)
#define P2CSIH2IR_1        (((volatile __bitf_T *)0xFFFF903A)->bit02)
#define P2P0               (((volatile __bitf_T *)0xFFFF903A)->bit02)
#define TBCSIH2IR_1        (((volatile __bitf_T *)0xFFFF903A)->bit06)
#define TBP0               (((volatile __bitf_T *)0xFFFF903A)->bit06)
#define MKCSIH2IR_1        (((volatile __bitf_T *)0xFFFF903A)->bit07)
#define MKP0               (((volatile __bitf_T *)0xFFFF903A)->bit07)
#define RFCSIH2IR_1        (((volatile __bitf_T *)0xFFFF903B)->bit04)
#define RFP0               (((volatile __bitf_T *)0xFFFF903B)->bit04)
#define CTCSIH2IR_1        (((volatile __bitf_T *)0xFFFF903B)->bit07)
#define CTP0               (((volatile __bitf_T *)0xFFFF903B)->bit07)
__IOREG(ICCSIH2IRE_1, 0xFFFF903C, __READ_WRITE, uint16_t);
__IOREG(ICP1, 0xFFFF903C, __READ_WRITE, uint16_t);
__IOREG(ICCSIH2IREL_1, 0xFFFF903C, __READ_WRITE, uint8_t);
__IOREG(ICP1L, 0xFFFF903C, __READ_WRITE, uint8_t);
__IOREG(ICCSIH2IREH_1, 0xFFFF903D, __READ_WRITE, uint8_t);
__IOREG(ICP1H, 0xFFFF903D, __READ_WRITE, uint8_t);
#define P0CSIH2IRE_1       (((volatile __bitf_T *)0xFFFF903C)->bit00)
#define P0P1               (((volatile __bitf_T *)0xFFFF903C)->bit00)
#define P1CSIH2IRE_1       (((volatile __bitf_T *)0xFFFF903C)->bit01)
#define P1P1               (((volatile __bitf_T *)0xFFFF903C)->bit01)
#define P2CSIH2IRE_1       (((volatile __bitf_T *)0xFFFF903C)->bit02)
#define P2P1               (((volatile __bitf_T *)0xFFFF903C)->bit02)
#define TBCSIH2IRE_1       (((volatile __bitf_T *)0xFFFF903C)->bit06)
#define TBP1               (((volatile __bitf_T *)0xFFFF903C)->bit06)
#define MKCSIH2IRE_1       (((volatile __bitf_T *)0xFFFF903C)->bit07)
#define MKP1               (((volatile __bitf_T *)0xFFFF903C)->bit07)
#define RFCSIH2IRE_1       (((volatile __bitf_T *)0xFFFF903D)->bit04)
#define RFP1               (((volatile __bitf_T *)0xFFFF903D)->bit04)
#define CTCSIH2IRE_1       (((volatile __bitf_T *)0xFFFF903D)->bit07)
#define CTP1               (((volatile __bitf_T *)0xFFFF903D)->bit07)
__IOREG(ICCSIH2IJC_1, 0xFFFF903E, __READ_WRITE, uint16_t);
__IOREG(ICP2, 0xFFFF903E, __READ_WRITE, uint16_t);
__IOREG(ICCSIH2IJCL_1, 0xFFFF903E, __READ_WRITE, uint8_t);
__IOREG(ICP2L, 0xFFFF903E, __READ_WRITE, uint8_t);
__IOREG(ICCSIH2IJCH_1, 0xFFFF903F, __READ_WRITE, uint8_t);
__IOREG(ICP2H, 0xFFFF903F, __READ_WRITE, uint8_t);
#define P0CSIH2IJC_1       (((volatile __bitf_T *)0xFFFF903E)->bit00)
#define P0P2               (((volatile __bitf_T *)0xFFFF903E)->bit00)
#define P1CSIH2IJC_1       (((volatile __bitf_T *)0xFFFF903E)->bit01)
#define P1P2               (((volatile __bitf_T *)0xFFFF903E)->bit01)
#define P2CSIH2IJC_1       (((volatile __bitf_T *)0xFFFF903E)->bit02)
#define P2P2               (((volatile __bitf_T *)0xFFFF903E)->bit02)
#define TBCSIH2IJC_1       (((volatile __bitf_T *)0xFFFF903E)->bit06)
#define TBP2               (((volatile __bitf_T *)0xFFFF903E)->bit06)
#define MKCSIH2IJC_1       (((volatile __bitf_T *)0xFFFF903E)->bit07)
#define MKP2               (((volatile __bitf_T *)0xFFFF903E)->bit07)
#define RFCSIH2IJC_1       (((volatile __bitf_T *)0xFFFF903F)->bit04)
#define RFP2               (((volatile __bitf_T *)0xFFFF903F)->bit04)
#define CTCSIH2IJC_1       (((volatile __bitf_T *)0xFFFF903F)->bit07)
#define CTP2               (((volatile __bitf_T *)0xFFFF903F)->bit07)
__IOREG(FNC, 0xFFFF9078, __READ, uint16_t);
__IOREG(FNCH, 0xFFFF9079, __READ, uint8_t);
#define FNRF               (((const volatile __bitf_T *)0xFFFF9079)->bit04)
__IOREG(FIC, 0xFFFF907A, __READ, uint16_t);
__IOREG(FICH, 0xFFFF907B, __READ, uint8_t);
#define FIRF               (((const volatile __bitf_T *)0xFFFF907B)->bit04)
__IOREG(IMR0, 0xFFFF90F0, __READ_WRITE, uint32_t);
__IOREG(IMR0L, 0xFFFF90F0, __READ_WRITE, uint16_t);
__IOREG(IMR0LL, 0xFFFF90F0, __READ_WRITE, uint8_t);
__IOREG(IMR0LH, 0xFFFF90F1, __READ_WRITE, uint8_t);
__IOREG(IMR0H, 0xFFFF90F2, __READ_WRITE, uint16_t);
__IOREG(IMR0HL, 0xFFFF90F2, __READ_WRITE, uint8_t);
__IOREG(IMR0HH, 0xFFFF90F3, __READ_WRITE, uint8_t);
#define IMR0EIMK0          (((volatile __bitf_T *)0xFFFF90F0)->bit00)
#define IMR0EIMK1          (((volatile __bitf_T *)0xFFFF90F0)->bit01)
#define IMR0EIMK2          (((volatile __bitf_T *)0xFFFF90F0)->bit02)
#define IMR0EIMK3          (((volatile __bitf_T *)0xFFFF90F0)->bit03)
#define IMR0EIMK4          (((volatile __bitf_T *)0xFFFF90F0)->bit04)
#define IMR0EIMK5          (((volatile __bitf_T *)0xFFFF90F0)->bit05)
#define IMR0EIMK6          (((volatile __bitf_T *)0xFFFF90F0)->bit06)
#define IMR0EIMK7          (((volatile __bitf_T *)0xFFFF90F0)->bit07)
#define IMR0EIMK8          (((volatile __bitf_T *)0xFFFF90F1)->bit00)
#define IMR0EIMK9          (((volatile __bitf_T *)0xFFFF90F1)->bit01)
#define IMR0EIMK10         (((volatile __bitf_T *)0xFFFF90F1)->bit02)
#define IMR0EIMK11         (((volatile __bitf_T *)0xFFFF90F1)->bit03)
#define IMR0EIMK12         (((volatile __bitf_T *)0xFFFF90F1)->bit04)
#define IMR0EIMK13         (((volatile __bitf_T *)0xFFFF90F1)->bit05)
#define IMR0EIMK14         (((volatile __bitf_T *)0xFFFF90F1)->bit06)
#define IMR0EIMK15         (((volatile __bitf_T *)0xFFFF90F1)->bit07)
#define IMR0EIMK16         (((volatile __bitf_T *)0xFFFF90F2)->bit00)
#define IMR0EIMK17         (((volatile __bitf_T *)0xFFFF90F2)->bit01)
#define IMR0EIMK18         (((volatile __bitf_T *)0xFFFF90F2)->bit02)
#define IMR0EIMK19         (((volatile __bitf_T *)0xFFFF90F2)->bit03)
#define IMR0EIMK20         (((volatile __bitf_T *)0xFFFF90F2)->bit04)
#define IMR0EIMK21         (((volatile __bitf_T *)0xFFFF90F2)->bit05)
#define IMR0EIMK22         (((volatile __bitf_T *)0xFFFF90F2)->bit06)
#define IMR0EIMK23         (((volatile __bitf_T *)0xFFFF90F2)->bit07)
#define IMR0EIMK24         (((volatile __bitf_T *)0xFFFF90F3)->bit00)
#define IMR0EIMK25         (((volatile __bitf_T *)0xFFFF90F3)->bit01)
#define IMR0EIMK26         (((volatile __bitf_T *)0xFFFF90F3)->bit02)
#define IMR0EIMK27         (((volatile __bitf_T *)0xFFFF90F3)->bit03)
#define IMR0EIMK28         (((volatile __bitf_T *)0xFFFF90F3)->bit04)
#define IMR0EIMK29         (((volatile __bitf_T *)0xFFFF90F3)->bit05)
#define IMR0EIMK30         (((volatile __bitf_T *)0xFFFF90F3)->bit06)
#define IMR0EIMK31         (((volatile __bitf_T *)0xFFFF90F3)->bit07)
__IOREG(ICWDTA0, 0xFFFFA040, __READ_WRITE, uint16_t);
__IOREG(ICWDTA0L, 0xFFFFA040, __READ_WRITE, uint8_t);
__IOREG(ICWDTA0H, 0xFFFFA041, __READ_WRITE, uint8_t);
#define P0WDTA0            (((volatile __bitf_T *)0xFFFFA040)->bit00)
#define P1WDTA0            (((volatile __bitf_T *)0xFFFFA040)->bit01)
#define P2WDTA0            (((volatile __bitf_T *)0xFFFFA040)->bit02)
#define TBWDTA0            (((volatile __bitf_T *)0xFFFFA040)->bit06)
#define MKWDTA0            (((volatile __bitf_T *)0xFFFFA040)->bit07)
#define RFWDTA0            (((volatile __bitf_T *)0xFFFFA041)->bit04)
#define CTWDTA0            (((volatile __bitf_T *)0xFFFFA041)->bit07)
__IOREG(ICWDTA1, 0xFFFFA042, __READ_WRITE, uint16_t);
__IOREG(ICWDTA1L, 0xFFFFA042, __READ_WRITE, uint8_t);
__IOREG(ICWDTA1H, 0xFFFFA043, __READ_WRITE, uint8_t);
#define P0WDTA1            (((volatile __bitf_T *)0xFFFFA042)->bit00)
#define P1WDTA1            (((volatile __bitf_T *)0xFFFFA042)->bit01)
#define P2WDTA1            (((volatile __bitf_T *)0xFFFFA042)->bit02)
#define TBWDTA1            (((volatile __bitf_T *)0xFFFFA042)->bit06)
#define MKWDTA1            (((volatile __bitf_T *)0xFFFFA042)->bit07)
#define RFWDTA1            (((volatile __bitf_T *)0xFFFFA043)->bit04)
#define CTWDTA1            (((volatile __bitf_T *)0xFFFFA043)->bit07)
__IOREG(ICP3, 0xFFFFA044, __READ_WRITE, uint16_t);
__IOREG(ICP3L, 0xFFFFA044, __READ_WRITE, uint8_t);
__IOREG(ICP3H, 0xFFFFA045, __READ_WRITE, uint8_t);
#define P0P3               (((volatile __bitf_T *)0xFFFFA044)->bit00)
#define P1P3               (((volatile __bitf_T *)0xFFFFA044)->bit01)
#define P2P3               (((volatile __bitf_T *)0xFFFFA044)->bit02)
#define TBP3               (((volatile __bitf_T *)0xFFFFA044)->bit06)
#define MKP3               (((volatile __bitf_T *)0xFFFFA044)->bit07)
#define RFP3               (((volatile __bitf_T *)0xFFFFA045)->bit04)
#define CTP3               (((volatile __bitf_T *)0xFFFFA045)->bit07)
__IOREG(ICP4, 0xFFFFA046, __READ_WRITE, uint16_t);
__IOREG(ICP4L, 0xFFFFA046, __READ_WRITE, uint8_t);
__IOREG(ICP4H, 0xFFFFA047, __READ_WRITE, uint8_t);
#define P0P4               (((volatile __bitf_T *)0xFFFFA046)->bit00)
#define P1P4               (((volatile __bitf_T *)0xFFFFA046)->bit01)
#define P2P4               (((volatile __bitf_T *)0xFFFFA046)->bit02)
#define TBP4               (((volatile __bitf_T *)0xFFFFA046)->bit06)
#define MKP4               (((volatile __bitf_T *)0xFFFFA046)->bit07)
#define RFP4               (((volatile __bitf_T *)0xFFFFA047)->bit04)
#define CTP4               (((volatile __bitf_T *)0xFFFFA047)->bit07)
__IOREG(ICP5, 0xFFFFA048, __READ_WRITE, uint16_t);
__IOREG(ICP5L, 0xFFFFA048, __READ_WRITE, uint8_t);
__IOREG(ICP5H, 0xFFFFA049, __READ_WRITE, uint8_t);
#define P0P5               (((volatile __bitf_T *)0xFFFFA048)->bit00)
#define P1P5               (((volatile __bitf_T *)0xFFFFA048)->bit01)
#define P2P5               (((volatile __bitf_T *)0xFFFFA048)->bit02)
#define TBP5               (((volatile __bitf_T *)0xFFFFA048)->bit06)
#define MKP5               (((volatile __bitf_T *)0xFFFFA048)->bit07)
#define RFP5               (((volatile __bitf_T *)0xFFFFA049)->bit04)
#define CTP5               (((volatile __bitf_T *)0xFFFFA049)->bit07)
__IOREG(ICP10, 0xFFFFA04A, __READ_WRITE, uint16_t);
__IOREG(ICP10L, 0xFFFFA04A, __READ_WRITE, uint8_t);
__IOREG(ICP10H, 0xFFFFA04B, __READ_WRITE, uint8_t);
#define P0P10              (((volatile __bitf_T *)0xFFFFA04A)->bit00)
#define P1P10              (((volatile __bitf_T *)0xFFFFA04A)->bit01)
#define P2P10              (((volatile __bitf_T *)0xFFFFA04A)->bit02)
#define TBP10              (((volatile __bitf_T *)0xFFFFA04A)->bit06)
#define MKP10              (((volatile __bitf_T *)0xFFFFA04A)->bit07)
#define RFP10              (((volatile __bitf_T *)0xFFFFA04B)->bit04)
#define CTP10              (((volatile __bitf_T *)0xFFFFA04B)->bit07)
__IOREG(ICP11, 0xFFFFA04C, __READ_WRITE, uint16_t);
__IOREG(ICP11L, 0xFFFFA04C, __READ_WRITE, uint8_t);
__IOREG(ICP11H, 0xFFFFA04D, __READ_WRITE, uint8_t);
#define P0P11              (((volatile __bitf_T *)0xFFFFA04C)->bit00)
#define P1P11              (((volatile __bitf_T *)0xFFFFA04C)->bit01)
#define P2P11              (((volatile __bitf_T *)0xFFFFA04C)->bit02)
#define TBP11              (((volatile __bitf_T *)0xFFFFA04C)->bit06)
#define MKP11              (((volatile __bitf_T *)0xFFFFA04C)->bit07)
#define RFP11              (((volatile __bitf_T *)0xFFFFA04D)->bit04)
#define CTP11              (((volatile __bitf_T *)0xFFFFA04D)->bit07)
__IOREG(ICTAUD0I1, 0xFFFFA04E, __READ_WRITE, uint16_t);
__IOREG(ICTAUD0I1L, 0xFFFFA04E, __READ_WRITE, uint8_t);
__IOREG(ICTAUD0I1H, 0xFFFFA04F, __READ_WRITE, uint8_t);
#define P0TAUD0I1          (((volatile __bitf_T *)0xFFFFA04E)->bit00)
#define P1TAUD0I1          (((volatile __bitf_T *)0xFFFFA04E)->bit01)
#define P2TAUD0I1          (((volatile __bitf_T *)0xFFFFA04E)->bit02)
#define TBTAUD0I1          (((volatile __bitf_T *)0xFFFFA04E)->bit06)
#define MKTAUD0I1          (((volatile __bitf_T *)0xFFFFA04E)->bit07)
#define RFTAUD0I1          (((volatile __bitf_T *)0xFFFFA04F)->bit04)
#define CTTAUD0I1          (((volatile __bitf_T *)0xFFFFA04F)->bit07)
__IOREG(ICTAUD0I3, 0xFFFFA050, __READ_WRITE, uint16_t);
__IOREG(ICTAUD0I3L, 0xFFFFA050, __READ_WRITE, uint8_t);
__IOREG(ICTAUD0I3H, 0xFFFFA051, __READ_WRITE, uint8_t);
#define P0TAUD0I3          (((volatile __bitf_T *)0xFFFFA050)->bit00)
#define P1TAUD0I3          (((volatile __bitf_T *)0xFFFFA050)->bit01)
#define P2TAUD0I3          (((volatile __bitf_T *)0xFFFFA050)->bit02)
#define TBTAUD0I3          (((volatile __bitf_T *)0xFFFFA050)->bit06)
#define MKTAUD0I3          (((volatile __bitf_T *)0xFFFFA050)->bit07)
#define RFTAUD0I3          (((volatile __bitf_T *)0xFFFFA051)->bit04)
#define CTTAUD0I3          (((volatile __bitf_T *)0xFFFFA051)->bit07)
__IOREG(ICTAUD0I5, 0xFFFFA052, __READ_WRITE, uint16_t);
__IOREG(ICTAUD0I5L, 0xFFFFA052, __READ_WRITE, uint8_t);
__IOREG(ICTAUD0I5H, 0xFFFFA053, __READ_WRITE, uint8_t);
#define P0TAUD0I5          (((volatile __bitf_T *)0xFFFFA052)->bit00)
#define P1TAUD0I5          (((volatile __bitf_T *)0xFFFFA052)->bit01)
#define P2TAUD0I5          (((volatile __bitf_T *)0xFFFFA052)->bit02)
#define TBTAUD0I5          (((volatile __bitf_T *)0xFFFFA052)->bit06)
#define MKTAUD0I5          (((volatile __bitf_T *)0xFFFFA052)->bit07)
#define RFTAUD0I5          (((volatile __bitf_T *)0xFFFFA053)->bit04)
#define CTTAUD0I5          (((volatile __bitf_T *)0xFFFFA053)->bit07)
__IOREG(ICTAUD0I7, 0xFFFFA054, __READ_WRITE, uint16_t);
__IOREG(ICTAUD0I7L, 0xFFFFA054, __READ_WRITE, uint8_t);
__IOREG(ICTAUD0I7H, 0xFFFFA055, __READ_WRITE, uint8_t);
#define P0TAUD0I7          (((volatile __bitf_T *)0xFFFFA054)->bit00)
#define P1TAUD0I7          (((volatile __bitf_T *)0xFFFFA054)->bit01)
#define P2TAUD0I7          (((volatile __bitf_T *)0xFFFFA054)->bit02)
#define TBTAUD0I7          (((volatile __bitf_T *)0xFFFFA054)->bit06)
#define MKTAUD0I7          (((volatile __bitf_T *)0xFFFFA054)->bit07)
#define RFTAUD0I7          (((volatile __bitf_T *)0xFFFFA055)->bit04)
#define CTTAUD0I7          (((volatile __bitf_T *)0xFFFFA055)->bit07)
__IOREG(ICTAUD0I9, 0xFFFFA056, __READ_WRITE, uint16_t);
__IOREG(ICTAUD0I9L, 0xFFFFA056, __READ_WRITE, uint8_t);
__IOREG(ICTAUD0I9H, 0xFFFFA057, __READ_WRITE, uint8_t);
#define P0TAUD0I9          (((volatile __bitf_T *)0xFFFFA056)->bit00)
#define P1TAUD0I9          (((volatile __bitf_T *)0xFFFFA056)->bit01)
#define P2TAUD0I9          (((volatile __bitf_T *)0xFFFFA056)->bit02)
#define TBTAUD0I9          (((volatile __bitf_T *)0xFFFFA056)->bit06)
#define MKTAUD0I9          (((volatile __bitf_T *)0xFFFFA056)->bit07)
#define RFTAUD0I9          (((volatile __bitf_T *)0xFFFFA057)->bit04)
#define CTTAUD0I9          (((volatile __bitf_T *)0xFFFFA057)->bit07)
__IOREG(ICTAUD0I11, 0xFFFFA058, __READ_WRITE, uint16_t);
__IOREG(ICTAUD0I11L, 0xFFFFA058, __READ_WRITE, uint8_t);
__IOREG(ICTAUD0I11H, 0xFFFFA059, __READ_WRITE, uint8_t);
#define P0TAUD0I11         (((volatile __bitf_T *)0xFFFFA058)->bit00)
#define P1TAUD0I11         (((volatile __bitf_T *)0xFFFFA058)->bit01)
#define P2TAUD0I11         (((volatile __bitf_T *)0xFFFFA058)->bit02)
#define TBTAUD0I11         (((volatile __bitf_T *)0xFFFFA058)->bit06)
#define MKTAUD0I11         (((volatile __bitf_T *)0xFFFFA058)->bit07)
#define RFTAUD0I11         (((volatile __bitf_T *)0xFFFFA059)->bit04)
#define CTTAUD0I11         (((volatile __bitf_T *)0xFFFFA059)->bit07)
__IOREG(ICTAUD0I13, 0xFFFFA05A, __READ_WRITE, uint16_t);
__IOREG(ICTAUD0I13L, 0xFFFFA05A, __READ_WRITE, uint8_t);
__IOREG(ICTAUD0I13H, 0xFFFFA05B, __READ_WRITE, uint8_t);
#define P0TAUD0I13         (((volatile __bitf_T *)0xFFFFA05A)->bit00)
#define P1TAUD0I13         (((volatile __bitf_T *)0xFFFFA05A)->bit01)
#define P2TAUD0I13         (((volatile __bitf_T *)0xFFFFA05A)->bit02)
#define TBTAUD0I13         (((volatile __bitf_T *)0xFFFFA05A)->bit06)
#define MKTAUD0I13         (((volatile __bitf_T *)0xFFFFA05A)->bit07)
#define RFTAUD0I13         (((volatile __bitf_T *)0xFFFFA05B)->bit04)
#define CTTAUD0I13         (((volatile __bitf_T *)0xFFFFA05B)->bit07)
__IOREG(ICTAUD0I15, 0xFFFFA05C, __READ_WRITE, uint16_t);
__IOREG(ICTAUD0I15L, 0xFFFFA05C, __READ_WRITE, uint8_t);
__IOREG(ICTAUD0I15H, 0xFFFFA05D, __READ_WRITE, uint8_t);
#define P0TAUD0I15         (((volatile __bitf_T *)0xFFFFA05C)->bit00)
#define P1TAUD0I15         (((volatile __bitf_T *)0xFFFFA05C)->bit01)
#define P2TAUD0I15         (((volatile __bitf_T *)0xFFFFA05C)->bit02)
#define TBTAUD0I15         (((volatile __bitf_T *)0xFFFFA05C)->bit06)
#define MKTAUD0I15         (((volatile __bitf_T *)0xFFFFA05C)->bit07)
#define RFTAUD0I15         (((volatile __bitf_T *)0xFFFFA05D)->bit04)
#define CTTAUD0I15         (((volatile __bitf_T *)0xFFFFA05D)->bit07)
__IOREG(ICADCA0ERR, 0xFFFFA05E, __READ_WRITE, uint16_t);
__IOREG(ICADCA0ERRL, 0xFFFFA05E, __READ_WRITE, uint8_t);
__IOREG(ICADCA0ERRH, 0xFFFFA05F, __READ_WRITE, uint8_t);
#define P0ADCA0ERR         (((volatile __bitf_T *)0xFFFFA05E)->bit00)
#define P1ADCA0ERR         (((volatile __bitf_T *)0xFFFFA05E)->bit01)
#define P2ADCA0ERR         (((volatile __bitf_T *)0xFFFFA05E)->bit02)
#define TBADCA0ERR         (((volatile __bitf_T *)0xFFFFA05E)->bit06)
#define MKADCA0ERR         (((volatile __bitf_T *)0xFFFFA05E)->bit07)
#define RFADCA0ERR         (((volatile __bitf_T *)0xFFFFA05F)->bit04)
#define CTADCA0ERR         (((volatile __bitf_T *)0xFFFFA05F)->bit07)
__IOREG(ICCSIG0IRE, 0xFFFFA062, __READ_WRITE, uint16_t);
__IOREG(ICCSIG0IREL, 0xFFFFA062, __READ_WRITE, uint8_t);
__IOREG(ICCSIG0IREH, 0xFFFFA063, __READ_WRITE, uint8_t);
#define P0CSIG0IRE         (((volatile __bitf_T *)0xFFFFA062)->bit00)
#define P1CSIG0IRE         (((volatile __bitf_T *)0xFFFFA062)->bit01)
#define P2CSIG0IRE         (((volatile __bitf_T *)0xFFFFA062)->bit02)
#define TBCSIG0IRE         (((volatile __bitf_T *)0xFFFFA062)->bit06)
#define MKCSIG0IRE         (((volatile __bitf_T *)0xFFFFA062)->bit07)
#define RFCSIG0IRE         (((volatile __bitf_T *)0xFFFFA063)->bit04)
#define CTCSIG0IRE         (((volatile __bitf_T *)0xFFFFA063)->bit07)
__IOREG(ICRLIN20, 0xFFFFA064, __READ_WRITE, uint16_t);
__IOREG(ICRLIN20L, 0xFFFFA064, __READ_WRITE, uint8_t);
__IOREG(ICRLIN20H, 0xFFFFA065, __READ_WRITE, uint8_t);
#define P0RLIN20           (((volatile __bitf_T *)0xFFFFA064)->bit00)
#define P1RLIN20           (((volatile __bitf_T *)0xFFFFA064)->bit01)
#define P2RLIN20           (((volatile __bitf_T *)0xFFFFA064)->bit02)
#define TBRLIN20           (((volatile __bitf_T *)0xFFFFA064)->bit06)
#define MKRLIN20           (((volatile __bitf_T *)0xFFFFA064)->bit07)
#define RFRLIN20           (((volatile __bitf_T *)0xFFFFA065)->bit04)
#define CTRLIN20           (((volatile __bitf_T *)0xFFFFA065)->bit07)
__IOREG(ICRLIN21, 0xFFFFA066, __READ_WRITE, uint16_t);
__IOREG(ICRLIN21L, 0xFFFFA066, __READ_WRITE, uint8_t);
__IOREG(ICRLIN21H, 0xFFFFA067, __READ_WRITE, uint8_t);
#define P0RLIN21           (((volatile __bitf_T *)0xFFFFA066)->bit00)
#define P1RLIN21           (((volatile __bitf_T *)0xFFFFA066)->bit01)
#define P2RLIN21           (((volatile __bitf_T *)0xFFFFA066)->bit02)
#define TBRLIN21           (((volatile __bitf_T *)0xFFFFA066)->bit06)
#define MKRLIN21           (((volatile __bitf_T *)0xFFFFA066)->bit07)
#define RFRLIN21           (((volatile __bitf_T *)0xFFFFA067)->bit04)
#define CTRLIN21           (((volatile __bitf_T *)0xFFFFA067)->bit07)
__IOREG(ICDMA0, 0xFFFFA068, __READ_WRITE, uint16_t);
__IOREG(ICDMA0L, 0xFFFFA068, __READ_WRITE, uint8_t);
__IOREG(ICDMA0H, 0xFFFFA069, __READ_WRITE, uint8_t);
#define P0DMA0             (((volatile __bitf_T *)0xFFFFA068)->bit00)
#define P1DMA0             (((volatile __bitf_T *)0xFFFFA068)->bit01)
#define P2DMA0             (((volatile __bitf_T *)0xFFFFA068)->bit02)
#define TBDMA0             (((volatile __bitf_T *)0xFFFFA068)->bit06)
#define MKDMA0             (((volatile __bitf_T *)0xFFFFA068)->bit07)
#define RFDMA0             (((volatile __bitf_T *)0xFFFFA069)->bit04)
#define CTDMA0             (((volatile __bitf_T *)0xFFFFA069)->bit07)
__IOREG(ICDMA1, 0xFFFFA06A, __READ_WRITE, uint16_t);
__IOREG(ICDMA1L, 0xFFFFA06A, __READ_WRITE, uint8_t);
__IOREG(ICDMA1H, 0xFFFFA06B, __READ_WRITE, uint8_t);
#define P0DMA1             (((volatile __bitf_T *)0xFFFFA06A)->bit00)
#define P1DMA1             (((volatile __bitf_T *)0xFFFFA06A)->bit01)
#define P2DMA1             (((volatile __bitf_T *)0xFFFFA06A)->bit02)
#define TBDMA1             (((volatile __bitf_T *)0xFFFFA06A)->bit06)
#define MKDMA1             (((volatile __bitf_T *)0xFFFFA06A)->bit07)
#define RFDMA1             (((volatile __bitf_T *)0xFFFFA06B)->bit04)
#define CTDMA1             (((volatile __bitf_T *)0xFFFFA06B)->bit07)
__IOREG(ICDMA2, 0xFFFFA06C, __READ_WRITE, uint16_t);
__IOREG(ICDMA2L, 0xFFFFA06C, __READ_WRITE, uint8_t);
__IOREG(ICDMA2H, 0xFFFFA06D, __READ_WRITE, uint8_t);
#define P0DMA2             (((volatile __bitf_T *)0xFFFFA06C)->bit00)
#define P1DMA2             (((volatile __bitf_T *)0xFFFFA06C)->bit01)
#define P2DMA2             (((volatile __bitf_T *)0xFFFFA06C)->bit02)
#define TBDMA2             (((volatile __bitf_T *)0xFFFFA06C)->bit06)
#define MKDMA2             (((volatile __bitf_T *)0xFFFFA06C)->bit07)
#define RFDMA2             (((volatile __bitf_T *)0xFFFFA06D)->bit04)
#define CTDMA2             (((volatile __bitf_T *)0xFFFFA06D)->bit07)
__IOREG(ICDMA3, 0xFFFFA06E, __READ_WRITE, uint16_t);
__IOREG(ICDMA3L, 0xFFFFA06E, __READ_WRITE, uint8_t);
__IOREG(ICDMA3H, 0xFFFFA06F, __READ_WRITE, uint8_t);
#define P0DMA3             (((volatile __bitf_T *)0xFFFFA06E)->bit00)
#define P1DMA3             (((volatile __bitf_T *)0xFFFFA06E)->bit01)
#define P2DMA3             (((volatile __bitf_T *)0xFFFFA06E)->bit02)
#define TBDMA3             (((volatile __bitf_T *)0xFFFFA06E)->bit06)
#define MKDMA3             (((volatile __bitf_T *)0xFFFFA06E)->bit07)
#define RFDMA3             (((volatile __bitf_T *)0xFFFFA06F)->bit04)
#define CTDMA3             (((volatile __bitf_T *)0xFFFFA06F)->bit07)
__IOREG(ICDMA4, 0xFFFFA070, __READ_WRITE, uint16_t);
__IOREG(ICDMA4L, 0xFFFFA070, __READ_WRITE, uint8_t);
__IOREG(ICDMA4H, 0xFFFFA071, __READ_WRITE, uint8_t);
#define P0DMA4             (((volatile __bitf_T *)0xFFFFA070)->bit00)
#define P1DMA4             (((volatile __bitf_T *)0xFFFFA070)->bit01)
#define P2DMA4             (((volatile __bitf_T *)0xFFFFA070)->bit02)
#define TBDMA4             (((volatile __bitf_T *)0xFFFFA070)->bit06)
#define MKDMA4             (((volatile __bitf_T *)0xFFFFA070)->bit07)
#define RFDMA4             (((volatile __bitf_T *)0xFFFFA071)->bit04)
#define CTDMA4             (((volatile __bitf_T *)0xFFFFA071)->bit07)
__IOREG(ICDMA5, 0xFFFFA072, __READ_WRITE, uint16_t);
__IOREG(ICDMA5L, 0xFFFFA072, __READ_WRITE, uint8_t);
__IOREG(ICDMA5H, 0xFFFFA073, __READ_WRITE, uint8_t);
#define P0DMA5             (((volatile __bitf_T *)0xFFFFA072)->bit00)
#define P1DMA5             (((volatile __bitf_T *)0xFFFFA072)->bit01)
#define P2DMA5             (((volatile __bitf_T *)0xFFFFA072)->bit02)
#define TBDMA5             (((volatile __bitf_T *)0xFFFFA072)->bit06)
#define MKDMA5             (((volatile __bitf_T *)0xFFFFA072)->bit07)
#define RFDMA5             (((volatile __bitf_T *)0xFFFFA073)->bit04)
#define CTDMA5             (((volatile __bitf_T *)0xFFFFA073)->bit07)
__IOREG(ICDMA6, 0xFFFFA074, __READ_WRITE, uint16_t);
__IOREG(ICDMA6L, 0xFFFFA074, __READ_WRITE, uint8_t);
__IOREG(ICDMA6H, 0xFFFFA075, __READ_WRITE, uint8_t);
#define P0DMA6             (((volatile __bitf_T *)0xFFFFA074)->bit00)
#define P1DMA6             (((volatile __bitf_T *)0xFFFFA074)->bit01)
#define P2DMA6             (((volatile __bitf_T *)0xFFFFA074)->bit02)
#define TBDMA6             (((volatile __bitf_T *)0xFFFFA074)->bit06)
#define MKDMA6             (((volatile __bitf_T *)0xFFFFA074)->bit07)
#define RFDMA6             (((volatile __bitf_T *)0xFFFFA075)->bit04)
#define CTDMA6             (((volatile __bitf_T *)0xFFFFA075)->bit07)
__IOREG(ICDMA7, 0xFFFFA076, __READ_WRITE, uint16_t);
__IOREG(ICDMA7L, 0xFFFFA076, __READ_WRITE, uint8_t);
__IOREG(ICDMA7H, 0xFFFFA077, __READ_WRITE, uint8_t);
#define P0DMA7             (((volatile __bitf_T *)0xFFFFA076)->bit00)
#define P1DMA7             (((volatile __bitf_T *)0xFFFFA076)->bit01)
#define P2DMA7             (((volatile __bitf_T *)0xFFFFA076)->bit02)
#define TBDMA7             (((volatile __bitf_T *)0xFFFFA076)->bit06)
#define MKDMA7             (((volatile __bitf_T *)0xFFFFA076)->bit07)
#define RFDMA7             (((volatile __bitf_T *)0xFFFFA077)->bit04)
#define CTDMA7             (((volatile __bitf_T *)0xFFFFA077)->bit07)
__IOREG(ICDMA8, 0xFFFFA078, __READ_WRITE, uint16_t);
__IOREG(ICDMA8L, 0xFFFFA078, __READ_WRITE, uint8_t);
__IOREG(ICDMA8H, 0xFFFFA079, __READ_WRITE, uint8_t);
#define P0DMA8             (((volatile __bitf_T *)0xFFFFA078)->bit00)
#define P1DMA8             (((volatile __bitf_T *)0xFFFFA078)->bit01)
#define P2DMA8             (((volatile __bitf_T *)0xFFFFA078)->bit02)
#define TBDMA8             (((volatile __bitf_T *)0xFFFFA078)->bit06)
#define MKDMA8             (((volatile __bitf_T *)0xFFFFA078)->bit07)
#define RFDMA8             (((volatile __bitf_T *)0xFFFFA079)->bit04)
#define CTDMA8             (((volatile __bitf_T *)0xFFFFA079)->bit07)
__IOREG(ICDMA9, 0xFFFFA07A, __READ_WRITE, uint16_t);
__IOREG(ICDMA9L, 0xFFFFA07A, __READ_WRITE, uint8_t);
__IOREG(ICDMA9H, 0xFFFFA07B, __READ_WRITE, uint8_t);
#define P0DMA9             (((volatile __bitf_T *)0xFFFFA07A)->bit00)
#define P1DMA9             (((volatile __bitf_T *)0xFFFFA07A)->bit01)
#define P2DMA9             (((volatile __bitf_T *)0xFFFFA07A)->bit02)
#define TBDMA9             (((volatile __bitf_T *)0xFFFFA07A)->bit06)
#define MKDMA9             (((volatile __bitf_T *)0xFFFFA07A)->bit07)
#define RFDMA9             (((volatile __bitf_T *)0xFFFFA07B)->bit04)
#define CTDMA9             (((volatile __bitf_T *)0xFFFFA07B)->bit07)
__IOREG(ICDMA10, 0xFFFFA07C, __READ_WRITE, uint16_t);
__IOREG(ICDMA10L, 0xFFFFA07C, __READ_WRITE, uint8_t);
__IOREG(ICDMA10H, 0xFFFFA07D, __READ_WRITE, uint8_t);
#define P0DMA10            (((volatile __bitf_T *)0xFFFFA07C)->bit00)
#define P1DMA10            (((volatile __bitf_T *)0xFFFFA07C)->bit01)
#define P2DMA10            (((volatile __bitf_T *)0xFFFFA07C)->bit02)
#define TBDMA10            (((volatile __bitf_T *)0xFFFFA07C)->bit06)
#define MKDMA10            (((volatile __bitf_T *)0xFFFFA07C)->bit07)
#define RFDMA10            (((volatile __bitf_T *)0xFFFFA07D)->bit04)
#define CTDMA10            (((volatile __bitf_T *)0xFFFFA07D)->bit07)
__IOREG(ICDMA11, 0xFFFFA07E, __READ_WRITE, uint16_t);
__IOREG(ICDMA11L, 0xFFFFA07E, __READ_WRITE, uint8_t);
__IOREG(ICDMA11H, 0xFFFFA07F, __READ_WRITE, uint8_t);
#define P0DMA11            (((volatile __bitf_T *)0xFFFFA07E)->bit00)
#define P1DMA11            (((volatile __bitf_T *)0xFFFFA07E)->bit01)
#define P2DMA11            (((volatile __bitf_T *)0xFFFFA07E)->bit02)
#define TBDMA11            (((volatile __bitf_T *)0xFFFFA07E)->bit06)
#define MKDMA11            (((volatile __bitf_T *)0xFFFFA07E)->bit07)
#define RFDMA11            (((volatile __bitf_T *)0xFFFFA07F)->bit04)
#define CTDMA11            (((volatile __bitf_T *)0xFFFFA07F)->bit07)
__IOREG(ICDMA12, 0xFFFFA080, __READ_WRITE, uint16_t);
__IOREG(ICDMA12L, 0xFFFFA080, __READ_WRITE, uint8_t);
__IOREG(ICDMA12H, 0xFFFFA081, __READ_WRITE, uint8_t);
#define P0DMA12            (((volatile __bitf_T *)0xFFFFA080)->bit00)
#define P1DMA12            (((volatile __bitf_T *)0xFFFFA080)->bit01)
#define P2DMA12            (((volatile __bitf_T *)0xFFFFA080)->bit02)
#define TBDMA12            (((volatile __bitf_T *)0xFFFFA080)->bit06)
#define MKDMA12            (((volatile __bitf_T *)0xFFFFA080)->bit07)
#define RFDMA12            (((volatile __bitf_T *)0xFFFFA081)->bit04)
#define CTDMA12            (((volatile __bitf_T *)0xFFFFA081)->bit07)
__IOREG(ICDMA13, 0xFFFFA082, __READ_WRITE, uint16_t);
__IOREG(ICDMA13L, 0xFFFFA082, __READ_WRITE, uint8_t);
__IOREG(ICDMA13H, 0xFFFFA083, __READ_WRITE, uint8_t);
#define P0DMA13            (((volatile __bitf_T *)0xFFFFA082)->bit00)
#define P1DMA13            (((volatile __bitf_T *)0xFFFFA082)->bit01)
#define P2DMA13            (((volatile __bitf_T *)0xFFFFA082)->bit02)
#define TBDMA13            (((volatile __bitf_T *)0xFFFFA082)->bit06)
#define MKDMA13            (((volatile __bitf_T *)0xFFFFA082)->bit07)
#define RFDMA13            (((volatile __bitf_T *)0xFFFFA083)->bit04)
#define CTDMA13            (((volatile __bitf_T *)0xFFFFA083)->bit07)
__IOREG(ICDMA14, 0xFFFFA084, __READ_WRITE, uint16_t);
__IOREG(ICDMA14L, 0xFFFFA084, __READ_WRITE, uint8_t);
__IOREG(ICDMA14H, 0xFFFFA085, __READ_WRITE, uint8_t);
#define P0DMA14            (((volatile __bitf_T *)0xFFFFA084)->bit00)
#define P1DMA14            (((volatile __bitf_T *)0xFFFFA084)->bit01)
#define P2DMA14            (((volatile __bitf_T *)0xFFFFA084)->bit02)
#define TBDMA14            (((volatile __bitf_T *)0xFFFFA084)->bit06)
#define MKDMA14            (((volatile __bitf_T *)0xFFFFA084)->bit07)
#define RFDMA14            (((volatile __bitf_T *)0xFFFFA085)->bit04)
#define CTDMA14            (((volatile __bitf_T *)0xFFFFA085)->bit07)
__IOREG(ICDMA15, 0xFFFFA086, __READ_WRITE, uint16_t);
__IOREG(ICDMA15L, 0xFFFFA086, __READ_WRITE, uint8_t);
__IOREG(ICDMA15H, 0xFFFFA087, __READ_WRITE, uint8_t);
#define P0DMA15            (((volatile __bitf_T *)0xFFFFA086)->bit00)
#define P1DMA15            (((volatile __bitf_T *)0xFFFFA086)->bit01)
#define P2DMA15            (((volatile __bitf_T *)0xFFFFA086)->bit02)
#define TBDMA15            (((volatile __bitf_T *)0xFFFFA086)->bit06)
#define MKDMA15            (((volatile __bitf_T *)0xFFFFA086)->bit07)
#define RFDMA15            (((volatile __bitf_T *)0xFFFFA087)->bit04)
#define CTDMA15            (((volatile __bitf_T *)0xFFFFA087)->bit07)
__IOREG(ICRIIC0TI, 0xFFFFA088, __READ_WRITE, uint16_t);
__IOREG(ICRIIC0TIL, 0xFFFFA088, __READ_WRITE, uint8_t);
__IOREG(ICRIIC0TIH, 0xFFFFA089, __READ_WRITE, uint8_t);
#define P0RIIC0TI          (((volatile __bitf_T *)0xFFFFA088)->bit00)
#define P1RIIC0TI          (((volatile __bitf_T *)0xFFFFA088)->bit01)
#define P2RIIC0TI          (((volatile __bitf_T *)0xFFFFA088)->bit02)
#define TBRIIC0TI          (((volatile __bitf_T *)0xFFFFA088)->bit06)
#define MKRIIC0TI          (((volatile __bitf_T *)0xFFFFA088)->bit07)
#define RFRIIC0TI          (((volatile __bitf_T *)0xFFFFA089)->bit04)
#define CTRIIC0TI          (((volatile __bitf_T *)0xFFFFA089)->bit07)
__IOREG(ICRIIC0TEI, 0xFFFFA08A, __READ_WRITE, uint16_t);
__IOREG(ICRIIC0TEIL, 0xFFFFA08A, __READ_WRITE, uint8_t);
__IOREG(ICRIIC0TEIH, 0xFFFFA08B, __READ_WRITE, uint8_t);
#define P0RIIC0TEI         (((volatile __bitf_T *)0xFFFFA08A)->bit00)
#define P1RIIC0TEI         (((volatile __bitf_T *)0xFFFFA08A)->bit01)
#define P2RIIC0TEI         (((volatile __bitf_T *)0xFFFFA08A)->bit02)
#define TBRIIC0TEI         (((volatile __bitf_T *)0xFFFFA08A)->bit06)
#define MKRIIC0TEI         (((volatile __bitf_T *)0xFFFFA08A)->bit07)
#define RFRIIC0TEI         (((volatile __bitf_T *)0xFFFFA08B)->bit04)
#define CTRIIC0TEI         (((volatile __bitf_T *)0xFFFFA08B)->bit07)
__IOREG(ICRIIC0RI, 0xFFFFA08C, __READ_WRITE, uint16_t);
__IOREG(ICRIIC0RIL, 0xFFFFA08C, __READ_WRITE, uint8_t);
__IOREG(ICRIIC0RIH, 0xFFFFA08D, __READ_WRITE, uint8_t);
#define P0RIIC0RI          (((volatile __bitf_T *)0xFFFFA08C)->bit00)
#define P1RIIC0RI          (((volatile __bitf_T *)0xFFFFA08C)->bit01)
#define P2RIIC0RI          (((volatile __bitf_T *)0xFFFFA08C)->bit02)
#define TBRIIC0RI          (((volatile __bitf_T *)0xFFFFA08C)->bit06)
#define MKRIIC0RI          (((volatile __bitf_T *)0xFFFFA08C)->bit07)
#define RFRIIC0RI          (((volatile __bitf_T *)0xFFFFA08D)->bit04)
#define CTRIIC0RI          (((volatile __bitf_T *)0xFFFFA08D)->bit07)
__IOREG(ICRIIC0EE, 0xFFFFA08E, __READ_WRITE, uint16_t);
__IOREG(ICRIIC0EEL, 0xFFFFA08E, __READ_WRITE, uint8_t);
__IOREG(ICRIIC0EEH, 0xFFFFA08F, __READ_WRITE, uint8_t);
#define P0RIIC0EE          (((volatile __bitf_T *)0xFFFFA08E)->bit00)
#define P1RIIC0EE          (((volatile __bitf_T *)0xFFFFA08E)->bit01)
#define P2RIIC0EE          (((volatile __bitf_T *)0xFFFFA08E)->bit02)
#define TBRIIC0EE          (((volatile __bitf_T *)0xFFFFA08E)->bit06)
#define MKRIIC0EE          (((volatile __bitf_T *)0xFFFFA08E)->bit07)
#define RFRIIC0EE          (((volatile __bitf_T *)0xFFFFA08F)->bit04)
#define CTRIIC0EE          (((volatile __bitf_T *)0xFFFFA08F)->bit07)
__IOREG(ICTAUJ0I0, 0xFFFFA090, __READ_WRITE, uint16_t);
__IOREG(ICTAUJ0I0L, 0xFFFFA090, __READ_WRITE, uint8_t);
__IOREG(ICTAUJ0I0H, 0xFFFFA091, __READ_WRITE, uint8_t);
#define P0TAUJ0I0          (((volatile __bitf_T *)0xFFFFA090)->bit00)
#define P1TAUJ0I0          (((volatile __bitf_T *)0xFFFFA090)->bit01)
#define P2TAUJ0I0          (((volatile __bitf_T *)0xFFFFA090)->bit02)
#define TBTAUJ0I0          (((volatile __bitf_T *)0xFFFFA090)->bit06)
#define MKTAUJ0I0          (((volatile __bitf_T *)0xFFFFA090)->bit07)
#define RFTAUJ0I0          (((volatile __bitf_T *)0xFFFFA091)->bit04)
#define CTTAUJ0I0          (((volatile __bitf_T *)0xFFFFA091)->bit07)
__IOREG(ICTAUJ0I1, 0xFFFFA092, __READ_WRITE, uint16_t);
__IOREG(ICTAUJ0I1L, 0xFFFFA092, __READ_WRITE, uint8_t);
__IOREG(ICTAUJ0I1H, 0xFFFFA093, __READ_WRITE, uint8_t);
#define P0TAUJ0I1          (((volatile __bitf_T *)0xFFFFA092)->bit00)
#define P1TAUJ0I1          (((volatile __bitf_T *)0xFFFFA092)->bit01)
#define P2TAUJ0I1          (((volatile __bitf_T *)0xFFFFA092)->bit02)
#define TBTAUJ0I1          (((volatile __bitf_T *)0xFFFFA092)->bit06)
#define MKTAUJ0I1          (((volatile __bitf_T *)0xFFFFA092)->bit07)
#define RFTAUJ0I1          (((volatile __bitf_T *)0xFFFFA093)->bit04)
#define CTTAUJ0I1          (((volatile __bitf_T *)0xFFFFA093)->bit07)
__IOREG(ICTAUJ0I2, 0xFFFFA094, __READ_WRITE, uint16_t);
__IOREG(ICTAUJ0I2L, 0xFFFFA094, __READ_WRITE, uint8_t);
__IOREG(ICTAUJ0I2H, 0xFFFFA095, __READ_WRITE, uint8_t);
#define P0TAUJ0I2          (((volatile __bitf_T *)0xFFFFA094)->bit00)
#define P1TAUJ0I2          (((volatile __bitf_T *)0xFFFFA094)->bit01)
#define P2TAUJ0I2          (((volatile __bitf_T *)0xFFFFA094)->bit02)
#define TBTAUJ0I2          (((volatile __bitf_T *)0xFFFFA094)->bit06)
#define MKTAUJ0I2          (((volatile __bitf_T *)0xFFFFA094)->bit07)
#define RFTAUJ0I2          (((volatile __bitf_T *)0xFFFFA095)->bit04)
#define CTTAUJ0I2          (((volatile __bitf_T *)0xFFFFA095)->bit07)
__IOREG(ICTAUJ0I3, 0xFFFFA096, __READ_WRITE, uint16_t);
__IOREG(ICTAUJ0I3L, 0xFFFFA096, __READ_WRITE, uint8_t);
__IOREG(ICTAUJ0I3H, 0xFFFFA097, __READ_WRITE, uint8_t);
#define P0TAUJ0I3          (((volatile __bitf_T *)0xFFFFA096)->bit00)
#define P1TAUJ0I3          (((volatile __bitf_T *)0xFFFFA096)->bit01)
#define P2TAUJ0I3          (((volatile __bitf_T *)0xFFFFA096)->bit02)
#define TBTAUJ0I3          (((volatile __bitf_T *)0xFFFFA096)->bit06)
#define MKTAUJ0I3          (((volatile __bitf_T *)0xFFFFA096)->bit07)
#define RFTAUJ0I3          (((volatile __bitf_T *)0xFFFFA097)->bit04)
#define CTTAUJ0I3          (((volatile __bitf_T *)0xFFFFA097)->bit07)
__IOREG(ICOSTM0, 0xFFFFA098, __READ_WRITE, uint16_t);
__IOREG(ICOSTM0L, 0xFFFFA098, __READ_WRITE, uint8_t);
__IOREG(ICOSTM0H, 0xFFFFA099, __READ_WRITE, uint8_t);
#define P0OSTM0            (((volatile __bitf_T *)0xFFFFA098)->bit00)
#define P1OSTM0            (((volatile __bitf_T *)0xFFFFA098)->bit01)
#define P2OSTM0            (((volatile __bitf_T *)0xFFFFA098)->bit02)
#define TBOSTM0            (((volatile __bitf_T *)0xFFFFA098)->bit06)
#define MKOSTM0            (((volatile __bitf_T *)0xFFFFA098)->bit07)
#define RFOSTM0            (((volatile __bitf_T *)0xFFFFA099)->bit04)
#define CTOSTM0            (((volatile __bitf_T *)0xFFFFA099)->bit07)
__IOREG(ICENCA0IOV, 0xFFFFA09A, __READ_WRITE, uint16_t);
__IOREG(ICPWGA4, 0xFFFFA09A, __READ_WRITE, uint16_t);
__IOREG(ICENCA0IOVL, 0xFFFFA09A, __READ_WRITE, uint8_t);
__IOREG(ICPWGA4L, 0xFFFFA09A, __READ_WRITE, uint8_t);
__IOREG(ICENCA0IOVH, 0xFFFFA09B, __READ_WRITE, uint8_t);
__IOREG(ICPWGA4H, 0xFFFFA09B, __READ_WRITE, uint8_t);
#define P0ENCA0IOV         (((volatile __bitf_T *)0xFFFFA09A)->bit00)
#define P0PWGA4            (((volatile __bitf_T *)0xFFFFA09A)->bit00)
#define P1ENCA0IOV         (((volatile __bitf_T *)0xFFFFA09A)->bit01)
#define P1PWGA4            (((volatile __bitf_T *)0xFFFFA09A)->bit01)
#define P2ENCA0IOV         (((volatile __bitf_T *)0xFFFFA09A)->bit02)
#define P2PWGA4            (((volatile __bitf_T *)0xFFFFA09A)->bit02)
#define TBENCA0IOV         (((volatile __bitf_T *)0xFFFFA09A)->bit06)
#define TBPWGA4            (((volatile __bitf_T *)0xFFFFA09A)->bit06)
#define MKENCA0IOV         (((volatile __bitf_T *)0xFFFFA09A)->bit07)
#define MKPWGA4            (((volatile __bitf_T *)0xFFFFA09A)->bit07)
#define RFENCA0IOV         (((volatile __bitf_T *)0xFFFFA09B)->bit04)
#define RFPWGA4            (((volatile __bitf_T *)0xFFFFA09B)->bit04)
#define CTENCA0IOV         (((volatile __bitf_T *)0xFFFFA09B)->bit07)
#define CTPWGA4            (((volatile __bitf_T *)0xFFFFA09B)->bit07)
__IOREG(ICENCA0IUD, 0xFFFFA09C, __READ_WRITE, uint16_t);
__IOREG(ICPWGA5, 0xFFFFA09C, __READ_WRITE, uint16_t);
__IOREG(ICENCA0IUDL, 0xFFFFA09C, __READ_WRITE, uint8_t);
__IOREG(ICPWGA5L, 0xFFFFA09C, __READ_WRITE, uint8_t);
__IOREG(ICENCA0IUDH, 0xFFFFA09D, __READ_WRITE, uint8_t);
__IOREG(ICPWGA5H, 0xFFFFA09D, __READ_WRITE, uint8_t);
#define P0ENCA0IUD         (((volatile __bitf_T *)0xFFFFA09C)->bit00)
#define P0PWGA5            (((volatile __bitf_T *)0xFFFFA09C)->bit00)
#define P1ENCA0IUD         (((volatile __bitf_T *)0xFFFFA09C)->bit01)
#define P1PWGA5            (((volatile __bitf_T *)0xFFFFA09C)->bit01)
#define P2ENCA0IUD         (((volatile __bitf_T *)0xFFFFA09C)->bit02)
#define P2PWGA5            (((volatile __bitf_T *)0xFFFFA09C)->bit02)
#define TBENCA0IUD         (((volatile __bitf_T *)0xFFFFA09C)->bit06)
#define TBPWGA5            (((volatile __bitf_T *)0xFFFFA09C)->bit06)
#define MKENCA0IUD         (((volatile __bitf_T *)0xFFFFA09C)->bit07)
#define MKPWGA5            (((volatile __bitf_T *)0xFFFFA09C)->bit07)
#define RFENCA0IUD         (((volatile __bitf_T *)0xFFFFA09D)->bit04)
#define RFPWGA5            (((volatile __bitf_T *)0xFFFFA09D)->bit04)
#define CTENCA0IUD         (((volatile __bitf_T *)0xFFFFA09D)->bit07)
#define CTPWGA5            (((volatile __bitf_T *)0xFFFFA09D)->bit07)
__IOREG(ICENCA0I0, 0xFFFFA09E, __READ_WRITE, uint16_t);
__IOREG(ICPWGA6, 0xFFFFA09E, __READ_WRITE, uint16_t);
__IOREG(ICENCA0I0L, 0xFFFFA09E, __READ_WRITE, uint8_t);
__IOREG(ICPWGA6L, 0xFFFFA09E, __READ_WRITE, uint8_t);
__IOREG(ICENCA0I0H, 0xFFFFA09F, __READ_WRITE, uint8_t);
__IOREG(ICPWGA6H, 0xFFFFA09F, __READ_WRITE, uint8_t);
#define P0ENCA0I0          (((volatile __bitf_T *)0xFFFFA09E)->bit00)
#define P0PWGA6            (((volatile __bitf_T *)0xFFFFA09E)->bit00)
#define P1ENCA0I0          (((volatile __bitf_T *)0xFFFFA09E)->bit01)
#define P1PWGA6            (((volatile __bitf_T *)0xFFFFA09E)->bit01)
#define P2ENCA0I0          (((volatile __bitf_T *)0xFFFFA09E)->bit02)
#define P2PWGA6            (((volatile __bitf_T *)0xFFFFA09E)->bit02)
#define TBENCA0I0          (((volatile __bitf_T *)0xFFFFA09E)->bit06)
#define TBPWGA6            (((volatile __bitf_T *)0xFFFFA09E)->bit06)
#define MKENCA0I0          (((volatile __bitf_T *)0xFFFFA09E)->bit07)
#define MKPWGA6            (((volatile __bitf_T *)0xFFFFA09E)->bit07)
#define RFENCA0I0          (((volatile __bitf_T *)0xFFFFA09F)->bit04)
#define RFPWGA6            (((volatile __bitf_T *)0xFFFFA09F)->bit04)
#define CTENCA0I0          (((volatile __bitf_T *)0xFFFFA09F)->bit07)
#define CTPWGA6            (((volatile __bitf_T *)0xFFFFA09F)->bit07)
__IOREG(ICENCA0I1, 0xFFFFA0A0, __READ_WRITE, uint16_t);
__IOREG(ICPWGA7, 0xFFFFA0A0, __READ_WRITE, uint16_t);
__IOREG(ICENCA0I1L, 0xFFFFA0A0, __READ_WRITE, uint8_t);
__IOREG(ICPWGA7L, 0xFFFFA0A0, __READ_WRITE, uint8_t);
__IOREG(ICENCA0I1H, 0xFFFFA0A1, __READ_WRITE, uint8_t);
__IOREG(ICPWGA7H, 0xFFFFA0A1, __READ_WRITE, uint8_t);
#define P0ENCA0I1          (((volatile __bitf_T *)0xFFFFA0A0)->bit00)
#define P0PWGA7            (((volatile __bitf_T *)0xFFFFA0A0)->bit00)
#define P1ENCA0I1          (((volatile __bitf_T *)0xFFFFA0A0)->bit01)
#define P1PWGA7            (((volatile __bitf_T *)0xFFFFA0A0)->bit01)
#define P2ENCA0I1          (((volatile __bitf_T *)0xFFFFA0A0)->bit02)
#define P2PWGA7            (((volatile __bitf_T *)0xFFFFA0A0)->bit02)
#define TBENCA0I1          (((volatile __bitf_T *)0xFFFFA0A0)->bit06)
#define TBPWGA7            (((volatile __bitf_T *)0xFFFFA0A0)->bit06)
#define MKENCA0I1          (((volatile __bitf_T *)0xFFFFA0A0)->bit07)
#define MKPWGA7            (((volatile __bitf_T *)0xFFFFA0A0)->bit07)
#define RFENCA0I1          (((volatile __bitf_T *)0xFFFFA0A1)->bit04)
#define RFPWGA7            (((volatile __bitf_T *)0xFFFFA0A1)->bit04)
#define CTENCA0I1          (((volatile __bitf_T *)0xFFFFA0A1)->bit07)
#define CTPWGA7            (((volatile __bitf_T *)0xFFFFA0A1)->bit07)
__IOREG(ICENCA0IEC, 0xFFFFA0A2, __READ_WRITE, uint16_t);
__IOREG(ICENCA0IECL, 0xFFFFA0A2, __READ_WRITE, uint8_t);
__IOREG(ICENCA0IECH, 0xFFFFA0A3, __READ_WRITE, uint8_t);
#define P0ENCA0IEC         (((volatile __bitf_T *)0xFFFFA0A2)->bit00)
#define P1ENCA0IEC         (((volatile __bitf_T *)0xFFFFA0A2)->bit01)
#define P2ENCA0IEC         (((volatile __bitf_T *)0xFFFFA0A2)->bit02)
#define TBENCA0IEC         (((volatile __bitf_T *)0xFFFFA0A2)->bit06)
#define MKENCA0IEC         (((volatile __bitf_T *)0xFFFFA0A2)->bit07)
#define RFENCA0IEC         (((volatile __bitf_T *)0xFFFFA0A3)->bit04)
#define CTENCA0IEC         (((volatile __bitf_T *)0xFFFFA0A3)->bit07)
__IOREG(ICKR0, 0xFFFFA0A4, __READ_WRITE, uint16_t);
__IOREG(ICKR0L, 0xFFFFA0A4, __READ_WRITE, uint8_t);
__IOREG(ICKR0H, 0xFFFFA0A5, __READ_WRITE, uint8_t);
#define P0KR0              (((volatile __bitf_T *)0xFFFFA0A4)->bit00)
#define P1KR0              (((volatile __bitf_T *)0xFFFFA0A4)->bit01)
#define P2KR0              (((volatile __bitf_T *)0xFFFFA0A4)->bit02)
#define TBKR0              (((volatile __bitf_T *)0xFFFFA0A4)->bit06)
#define MKKR0              (((volatile __bitf_T *)0xFFFFA0A4)->bit07)
#define RFKR0              (((volatile __bitf_T *)0xFFFFA0A5)->bit04)
#define CTKR0              (((volatile __bitf_T *)0xFFFFA0A5)->bit07)
__IOREG(ICQFULL, 0xFFFFA0A6, __READ_WRITE, uint16_t);
__IOREG(ICQFULLL, 0xFFFFA0A6, __READ_WRITE, uint8_t);
__IOREG(ICQFULLH, 0xFFFFA0A7, __READ_WRITE, uint8_t);
#define P0QFULL            (((volatile __bitf_T *)0xFFFFA0A6)->bit00)
#define P1QFULL            (((volatile __bitf_T *)0xFFFFA0A6)->bit01)
#define P2QFULL            (((volatile __bitf_T *)0xFFFFA0A6)->bit02)
#define TBQFULL            (((volatile __bitf_T *)0xFFFFA0A6)->bit06)
#define MKQFULL            (((volatile __bitf_T *)0xFFFFA0A6)->bit07)
#define RFQFULL            (((volatile __bitf_T *)0xFFFFA0A7)->bit04)
#define CTQFULL            (((volatile __bitf_T *)0xFFFFA0A7)->bit07)
__IOREG(ICPWGA0, 0xFFFFA0A8, __READ_WRITE, uint16_t);
__IOREG(ICPWGA0L, 0xFFFFA0A8, __READ_WRITE, uint8_t);
__IOREG(ICPWGA0H, 0xFFFFA0A9, __READ_WRITE, uint8_t);
#define P0PWGA0            (((volatile __bitf_T *)0xFFFFA0A8)->bit00)
#define P1PWGA0            (((volatile __bitf_T *)0xFFFFA0A8)->bit01)
#define P2PWGA0            (((volatile __bitf_T *)0xFFFFA0A8)->bit02)
#define TBPWGA0            (((volatile __bitf_T *)0xFFFFA0A8)->bit06)
#define MKPWGA0            (((volatile __bitf_T *)0xFFFFA0A8)->bit07)
#define RFPWGA0            (((volatile __bitf_T *)0xFFFFA0A9)->bit04)
#define CTPWGA0            (((volatile __bitf_T *)0xFFFFA0A9)->bit07)
__IOREG(ICPWGA1, 0xFFFFA0AA, __READ_WRITE, uint16_t);
__IOREG(ICPWGA1L, 0xFFFFA0AA, __READ_WRITE, uint8_t);
__IOREG(ICPWGA1H, 0xFFFFA0AB, __READ_WRITE, uint8_t);
#define P0PWGA1            (((volatile __bitf_T *)0xFFFFA0AA)->bit00)
#define P1PWGA1            (((volatile __bitf_T *)0xFFFFA0AA)->bit01)
#define P2PWGA1            (((volatile __bitf_T *)0xFFFFA0AA)->bit02)
#define TBPWGA1            (((volatile __bitf_T *)0xFFFFA0AA)->bit06)
#define MKPWGA1            (((volatile __bitf_T *)0xFFFFA0AA)->bit07)
#define RFPWGA1            (((volatile __bitf_T *)0xFFFFA0AB)->bit04)
#define CTPWGA1            (((volatile __bitf_T *)0xFFFFA0AB)->bit07)
__IOREG(ICPWGA2, 0xFFFFA0AC, __READ_WRITE, uint16_t);
__IOREG(ICPWGA2L, 0xFFFFA0AC, __READ_WRITE, uint8_t);
__IOREG(ICPWGA2H, 0xFFFFA0AD, __READ_WRITE, uint8_t);
#define P0PWGA2            (((volatile __bitf_T *)0xFFFFA0AC)->bit00)
#define P1PWGA2            (((volatile __bitf_T *)0xFFFFA0AC)->bit01)
#define P2PWGA2            (((volatile __bitf_T *)0xFFFFA0AC)->bit02)
#define TBPWGA2            (((volatile __bitf_T *)0xFFFFA0AC)->bit06)
#define MKPWGA2            (((volatile __bitf_T *)0xFFFFA0AC)->bit07)
#define RFPWGA2            (((volatile __bitf_T *)0xFFFFA0AD)->bit04)
#define CTPWGA2            (((volatile __bitf_T *)0xFFFFA0AD)->bit07)
__IOREG(ICPWGA3, 0xFFFFA0AE, __READ_WRITE, uint16_t);
__IOREG(ICPWGA3L, 0xFFFFA0AE, __READ_WRITE, uint8_t);
__IOREG(ICPWGA3H, 0xFFFFA0AF, __READ_WRITE, uint8_t);
#define P0PWGA3            (((volatile __bitf_T *)0xFFFFA0AE)->bit00)
#define P1PWGA3            (((volatile __bitf_T *)0xFFFFA0AE)->bit01)
#define P2PWGA3            (((volatile __bitf_T *)0xFFFFA0AE)->bit02)
#define TBPWGA3            (((volatile __bitf_T *)0xFFFFA0AE)->bit06)
#define MKPWGA3            (((volatile __bitf_T *)0xFFFFA0AE)->bit07)
#define RFPWGA3            (((volatile __bitf_T *)0xFFFFA0AF)->bit04)
#define CTPWGA3            (((volatile __bitf_T *)0xFFFFA0AF)->bit07)
__IOREG(ICPWGA8, 0xFFFFA0B0, __READ_WRITE, uint16_t);
__IOREG(ICPWGA8L, 0xFFFFA0B0, __READ_WRITE, uint8_t);
__IOREG(ICPWGA8H, 0xFFFFA0B1, __READ_WRITE, uint8_t);
#define P0PWGA8            (((volatile __bitf_T *)0xFFFFA0B0)->bit00)
#define P1PWGA8            (((volatile __bitf_T *)0xFFFFA0B0)->bit01)
#define P2PWGA8            (((volatile __bitf_T *)0xFFFFA0B0)->bit02)
#define TBPWGA8            (((volatile __bitf_T *)0xFFFFA0B0)->bit06)
#define MKPWGA8            (((volatile __bitf_T *)0xFFFFA0B0)->bit07)
#define RFPWGA8            (((volatile __bitf_T *)0xFFFFA0B1)->bit04)
#define CTPWGA8            (((volatile __bitf_T *)0xFFFFA0B1)->bit07)
__IOREG(ICPWGA9, 0xFFFFA0B2, __READ_WRITE, uint16_t);
__IOREG(ICPWGA9L, 0xFFFFA0B2, __READ_WRITE, uint8_t);
__IOREG(ICPWGA9H, 0xFFFFA0B3, __READ_WRITE, uint8_t);
#define P0PWGA9            (((volatile __bitf_T *)0xFFFFA0B2)->bit00)
#define P1PWGA9            (((volatile __bitf_T *)0xFFFFA0B2)->bit01)
#define P2PWGA9            (((volatile __bitf_T *)0xFFFFA0B2)->bit02)
#define TBPWGA9            (((volatile __bitf_T *)0xFFFFA0B2)->bit06)
#define MKPWGA9            (((volatile __bitf_T *)0xFFFFA0B2)->bit07)
#define RFPWGA9            (((volatile __bitf_T *)0xFFFFA0B3)->bit04)
#define CTPWGA9            (((volatile __bitf_T *)0xFFFFA0B3)->bit07)
__IOREG(ICPWGA10, 0xFFFFA0B4, __READ_WRITE, uint16_t);
__IOREG(ICPWGA10L, 0xFFFFA0B4, __READ_WRITE, uint8_t);
__IOREG(ICPWGA10H, 0xFFFFA0B5, __READ_WRITE, uint8_t);
#define P0PWGA10           (((volatile __bitf_T *)0xFFFFA0B4)->bit00)
#define P1PWGA10           (((volatile __bitf_T *)0xFFFFA0B4)->bit01)
#define P2PWGA10           (((volatile __bitf_T *)0xFFFFA0B4)->bit02)
#define TBPWGA10           (((volatile __bitf_T *)0xFFFFA0B4)->bit06)
#define MKPWGA10           (((volatile __bitf_T *)0xFFFFA0B4)->bit07)
#define RFPWGA10           (((volatile __bitf_T *)0xFFFFA0B5)->bit04)
#define CTPWGA10           (((volatile __bitf_T *)0xFFFFA0B5)->bit07)
__IOREG(ICPWGA11, 0xFFFFA0B6, __READ_WRITE, uint16_t);
__IOREG(ICPWGA11L, 0xFFFFA0B6, __READ_WRITE, uint8_t);
__IOREG(ICPWGA11H, 0xFFFFA0B7, __READ_WRITE, uint8_t);
#define P0PWGA11           (((volatile __bitf_T *)0xFFFFA0B6)->bit00)
#define P1PWGA11           (((volatile __bitf_T *)0xFFFFA0B6)->bit01)
#define P2PWGA11           (((volatile __bitf_T *)0xFFFFA0B6)->bit02)
#define TBPWGA11           (((volatile __bitf_T *)0xFFFFA0B6)->bit06)
#define MKPWGA11           (((volatile __bitf_T *)0xFFFFA0B6)->bit07)
#define RFPWGA11           (((volatile __bitf_T *)0xFFFFA0B7)->bit04)
#define CTPWGA11           (((volatile __bitf_T *)0xFFFFA0B7)->bit07)
__IOREG(ICPWGA12, 0xFFFFA0B8, __READ_WRITE, uint16_t);
__IOREG(ICPWGA12L, 0xFFFFA0B8, __READ_WRITE, uint8_t);
__IOREG(ICPWGA12H, 0xFFFFA0B9, __READ_WRITE, uint8_t);
#define P0PWGA12           (((volatile __bitf_T *)0xFFFFA0B8)->bit00)
#define P1PWGA12           (((volatile __bitf_T *)0xFFFFA0B8)->bit01)
#define P2PWGA12           (((volatile __bitf_T *)0xFFFFA0B8)->bit02)
#define TBPWGA12           (((volatile __bitf_T *)0xFFFFA0B8)->bit06)
#define MKPWGA12           (((volatile __bitf_T *)0xFFFFA0B8)->bit07)
#define RFPWGA12           (((volatile __bitf_T *)0xFFFFA0B9)->bit04)
#define CTPWGA12           (((volatile __bitf_T *)0xFFFFA0B9)->bit07)
__IOREG(ICPWGA13, 0xFFFFA0BA, __READ_WRITE, uint16_t);
__IOREG(ICPWGA13L, 0xFFFFA0BA, __READ_WRITE, uint8_t);
__IOREG(ICPWGA13H, 0xFFFFA0BB, __READ_WRITE, uint8_t);
#define P0PWGA13           (((volatile __bitf_T *)0xFFFFA0BA)->bit00)
#define P1PWGA13           (((volatile __bitf_T *)0xFFFFA0BA)->bit01)
#define P2PWGA13           (((volatile __bitf_T *)0xFFFFA0BA)->bit02)
#define TBPWGA13           (((volatile __bitf_T *)0xFFFFA0BA)->bit06)
#define MKPWGA13           (((volatile __bitf_T *)0xFFFFA0BA)->bit07)
#define RFPWGA13           (((volatile __bitf_T *)0xFFFFA0BB)->bit04)
#define CTPWGA13           (((volatile __bitf_T *)0xFFFFA0BB)->bit07)
__IOREG(ICPWGA14, 0xFFFFA0BC, __READ_WRITE, uint16_t);
__IOREG(ICPWGA14L, 0xFFFFA0BC, __READ_WRITE, uint8_t);
__IOREG(ICPWGA14H, 0xFFFFA0BD, __READ_WRITE, uint8_t);
#define P0PWGA14           (((volatile __bitf_T *)0xFFFFA0BC)->bit00)
#define P1PWGA14           (((volatile __bitf_T *)0xFFFFA0BC)->bit01)
#define P2PWGA14           (((volatile __bitf_T *)0xFFFFA0BC)->bit02)
#define TBPWGA14           (((volatile __bitf_T *)0xFFFFA0BC)->bit06)
#define MKPWGA14           (((volatile __bitf_T *)0xFFFFA0BC)->bit07)
#define RFPWGA14           (((volatile __bitf_T *)0xFFFFA0BD)->bit04)
#define CTPWGA14           (((volatile __bitf_T *)0xFFFFA0BD)->bit07)
__IOREG(ICPWGA15, 0xFFFFA0BE, __READ_WRITE, uint16_t);
__IOREG(ICPWGA15L, 0xFFFFA0BE, __READ_WRITE, uint8_t);
__IOREG(ICPWGA15H, 0xFFFFA0BF, __READ_WRITE, uint8_t);
#define P0PWGA15           (((volatile __bitf_T *)0xFFFFA0BE)->bit00)
#define P1PWGA15           (((volatile __bitf_T *)0xFFFFA0BE)->bit01)
#define P2PWGA15           (((volatile __bitf_T *)0xFFFFA0BE)->bit02)
#define TBPWGA15           (((volatile __bitf_T *)0xFFFFA0BE)->bit06)
#define MKPWGA15           (((volatile __bitf_T *)0xFFFFA0BE)->bit07)
#define RFPWGA15           (((volatile __bitf_T *)0xFFFFA0BF)->bit04)
#define CTPWGA15           (((volatile __bitf_T *)0xFFFFA0BF)->bit07)
__IOREG(ICFLERR, 0xFFFFA0CC, __READ_WRITE, uint16_t);
__IOREG(ICFLERRL, 0xFFFFA0CC, __READ_WRITE, uint8_t);
__IOREG(ICFLERRH, 0xFFFFA0CD, __READ_WRITE, uint8_t);
#define P0FLERR            (((volatile __bitf_T *)0xFFFFA0CC)->bit00)
#define P1FLERR            (((volatile __bitf_T *)0xFFFFA0CC)->bit01)
#define P2FLERR            (((volatile __bitf_T *)0xFFFFA0CC)->bit02)
#define TBFLERR            (((volatile __bitf_T *)0xFFFFA0CC)->bit06)
#define MKFLERR            (((volatile __bitf_T *)0xFFFFA0CC)->bit07)
#define RFFLERR            (((volatile __bitf_T *)0xFFFFA0CD)->bit04)
#define CTFLERR            (((volatile __bitf_T *)0xFFFFA0CD)->bit07)
__IOREG(ICFLENDNM, 0xFFFFA0CE, __READ_WRITE, uint16_t);
__IOREG(ICFLENDNML, 0xFFFFA0CE, __READ_WRITE, uint8_t);
__IOREG(ICFLENDNMH, 0xFFFFA0CF, __READ_WRITE, uint8_t);
#define P0FLENDNM          (((volatile __bitf_T *)0xFFFFA0CE)->bit00)
#define P1FLENDNM          (((volatile __bitf_T *)0xFFFFA0CE)->bit01)
#define P2FLENDNM          (((volatile __bitf_T *)0xFFFFA0CE)->bit02)
#define TBFLENDNM          (((volatile __bitf_T *)0xFFFFA0CE)->bit06)
#define MKFLENDNM          (((volatile __bitf_T *)0xFFFFA0CE)->bit07)
#define RFFLENDNM          (((volatile __bitf_T *)0xFFFFA0CF)->bit04)
#define CTFLENDNM          (((volatile __bitf_T *)0xFFFFA0CF)->bit07)
__IOREG(ICCWEND, 0xFFFFA0D0, __READ_WRITE, uint16_t);
__IOREG(ICCWENDL, 0xFFFFA0D0, __READ_WRITE, uint8_t);
__IOREG(ICCWENDH, 0xFFFFA0D1, __READ_WRITE, uint8_t);
#define P0CWEND            (((volatile __bitf_T *)0xFFFFA0D0)->bit00)
#define P1CWEND            (((volatile __bitf_T *)0xFFFFA0D0)->bit01)
#define P2CWEND            (((volatile __bitf_T *)0xFFFFA0D0)->bit02)
#define TBCWEND            (((volatile __bitf_T *)0xFFFFA0D0)->bit06)
#define MKCWEND            (((volatile __bitf_T *)0xFFFFA0D0)->bit07)
#define RFCWEND            (((volatile __bitf_T *)0xFFFFA0D1)->bit04)
#define CTCWEND            (((volatile __bitf_T *)0xFFFFA0D1)->bit07)
__IOREG(ICRCAN1ERR, 0xFFFFA0D2, __READ_WRITE, uint16_t);
__IOREG(ICRCAN1ERRL, 0xFFFFA0D2, __READ_WRITE, uint8_t);
__IOREG(ICRCAN1ERRH, 0xFFFFA0D3, __READ_WRITE, uint8_t);
#define P0RCAN1ERR         (((volatile __bitf_T *)0xFFFFA0D2)->bit00)
#define P1RCAN1ERR         (((volatile __bitf_T *)0xFFFFA0D2)->bit01)
#define P2RCAN1ERR         (((volatile __bitf_T *)0xFFFFA0D2)->bit02)
#define TBRCAN1ERR         (((volatile __bitf_T *)0xFFFFA0D2)->bit06)
#define MKRCAN1ERR         (((volatile __bitf_T *)0xFFFFA0D2)->bit07)
#define RFRCAN1ERR         (((volatile __bitf_T *)0xFFFFA0D3)->bit04)
#define CTRCAN1ERR         (((volatile __bitf_T *)0xFFFFA0D3)->bit07)
__IOREG(ICRCAN1REC, 0xFFFFA0D4, __READ_WRITE, uint16_t);
__IOREG(ICRCAN1RECL, 0xFFFFA0D4, __READ_WRITE, uint8_t);
__IOREG(ICRCAN1RECH, 0xFFFFA0D5, __READ_WRITE, uint8_t);
#define P0RCAN1REC         (((volatile __bitf_T *)0xFFFFA0D4)->bit00)
#define P1RCAN1REC         (((volatile __bitf_T *)0xFFFFA0D4)->bit01)
#define P2RCAN1REC         (((volatile __bitf_T *)0xFFFFA0D4)->bit02)
#define TBRCAN1REC         (((volatile __bitf_T *)0xFFFFA0D4)->bit06)
#define MKRCAN1REC         (((volatile __bitf_T *)0xFFFFA0D4)->bit07)
#define RFRCAN1REC         (((volatile __bitf_T *)0xFFFFA0D5)->bit04)
#define CTRCAN1REC         (((volatile __bitf_T *)0xFFFFA0D5)->bit07)
__IOREG(ICRCAN1TRX, 0xFFFFA0D6, __READ_WRITE, uint16_t);
__IOREG(ICRCAN1TRXL, 0xFFFFA0D6, __READ_WRITE, uint8_t);
__IOREG(ICRCAN1TRXH, 0xFFFFA0D7, __READ_WRITE, uint8_t);
#define P0RCAN1TRX         (((volatile __bitf_T *)0xFFFFA0D6)->bit00)
#define P1RCAN1TRX         (((volatile __bitf_T *)0xFFFFA0D6)->bit01)
#define P2RCAN1TRX         (((volatile __bitf_T *)0xFFFFA0D6)->bit02)
#define TBRCAN1TRX         (((volatile __bitf_T *)0xFFFFA0D6)->bit06)
#define MKRCAN1TRX         (((volatile __bitf_T *)0xFFFFA0D6)->bit07)
#define RFRCAN1TRX         (((volatile __bitf_T *)0xFFFFA0D7)->bit04)
#define CTRCAN1TRX         (((volatile __bitf_T *)0xFFFFA0D7)->bit07)
__IOREG(ICCSIH1IC, 0xFFFFA0D8, __READ_WRITE, uint16_t);
__IOREG(ICTAPA0IPEK0_2, 0xFFFFA0D8, __READ_WRITE, uint16_t);
__IOREG(ICCSIH1ICL, 0xFFFFA0D8, __READ_WRITE, uint8_t);
__IOREG(ICTAPA0IPEK0L_2, 0xFFFFA0D8, __READ_WRITE, uint8_t);
__IOREG(ICCSIH1ICH, 0xFFFFA0D9, __READ_WRITE, uint8_t);
__IOREG(ICTAPA0IPEK0H_2, 0xFFFFA0D9, __READ_WRITE, uint8_t);
#define P0CSIH1IC          (((volatile __bitf_T *)0xFFFFA0D8)->bit00)
#define P0TAPA0IPEK0_2     (((volatile __bitf_T *)0xFFFFA0D8)->bit00)
#define P1CSIH1IC          (((volatile __bitf_T *)0xFFFFA0D8)->bit01)
#define P1TAPA0IPEK0_2     (((volatile __bitf_T *)0xFFFFA0D8)->bit01)
#define P2CSIH1IC          (((volatile __bitf_T *)0xFFFFA0D8)->bit02)
#define P2TAPA0IPEK0_2     (((volatile __bitf_T *)0xFFFFA0D8)->bit02)
#define TBCSIH1IC          (((volatile __bitf_T *)0xFFFFA0D8)->bit06)
#define TBTAPA0IPEK0_2     (((volatile __bitf_T *)0xFFFFA0D8)->bit06)
#define MKCSIH1IC          (((volatile __bitf_T *)0xFFFFA0D8)->bit07)
#define MKTAPA0IPEK0_2     (((volatile __bitf_T *)0xFFFFA0D8)->bit07)
#define RFCSIH1IC          (((volatile __bitf_T *)0xFFFFA0D9)->bit04)
#define RFTAPA0IPEK0_2     (((volatile __bitf_T *)0xFFFFA0D9)->bit04)
#define CTCSIH1IC          (((volatile __bitf_T *)0xFFFFA0D9)->bit07)
#define CTTAPA0IPEK0_2     (((volatile __bitf_T *)0xFFFFA0D9)->bit07)
__IOREG(ICCSIH1IR, 0xFFFFA0DA, __READ_WRITE, uint16_t);
__IOREG(ICTAPA0IVLY0_2, 0xFFFFA0DA, __READ_WRITE, uint16_t);
__IOREG(ICCSIH1IRL, 0xFFFFA0DA, __READ_WRITE, uint8_t);
__IOREG(ICTAPA0IVLY0L_2, 0xFFFFA0DA, __READ_WRITE, uint8_t);
__IOREG(ICCSIH1IRH, 0xFFFFA0DB, __READ_WRITE, uint8_t);
__IOREG(ICTAPA0IVLY0H_2, 0xFFFFA0DB, __READ_WRITE, uint8_t);
#define P0CSIH1IR          (((volatile __bitf_T *)0xFFFFA0DA)->bit00)
#define P0TAPA0IVLY0_2     (((volatile __bitf_T *)0xFFFFA0DA)->bit00)
#define P1CSIH1IR          (((volatile __bitf_T *)0xFFFFA0DA)->bit01)
#define P1TAPA0IVLY0_2     (((volatile __bitf_T *)0xFFFFA0DA)->bit01)
#define P2CSIH1IR          (((volatile __bitf_T *)0xFFFFA0DA)->bit02)
#define P2TAPA0IVLY0_2     (((volatile __bitf_T *)0xFFFFA0DA)->bit02)
#define TBCSIH1IR          (((volatile __bitf_T *)0xFFFFA0DA)->bit06)
#define TBTAPA0IVLY0_2     (((volatile __bitf_T *)0xFFFFA0DA)->bit06)
#define MKCSIH1IR          (((volatile __bitf_T *)0xFFFFA0DA)->bit07)
#define MKTAPA0IVLY0_2     (((volatile __bitf_T *)0xFFFFA0DA)->bit07)
#define RFCSIH1IR          (((volatile __bitf_T *)0xFFFFA0DB)->bit04)
#define RFTAPA0IVLY0_2     (((volatile __bitf_T *)0xFFFFA0DB)->bit04)
#define CTCSIH1IR          (((volatile __bitf_T *)0xFFFFA0DB)->bit07)
#define CTTAPA0IVLY0_2     (((volatile __bitf_T *)0xFFFFA0DB)->bit07)
__IOREG(ICCSIG0IC_2, 0xFFFFA0DC, __READ_WRITE, uint16_t);
__IOREG(ICCSIH1IRE, 0xFFFFA0DC, __READ_WRITE, uint16_t);
__IOREG(ICCSIG0ICL_2, 0xFFFFA0DC, __READ_WRITE, uint8_t);
__IOREG(ICCSIH1IREL, 0xFFFFA0DC, __READ_WRITE, uint8_t);
__IOREG(ICCSIG0ICH_2, 0xFFFFA0DD, __READ_WRITE, uint8_t);
__IOREG(ICCSIH1IREH, 0xFFFFA0DD, __READ_WRITE, uint8_t);
#define P0CSIG0IC_2        (((volatile __bitf_T *)0xFFFFA0DC)->bit00)
#define P0CSIH1IRE         (((volatile __bitf_T *)0xFFFFA0DC)->bit00)
#define P1CSIG0IC_2        (((volatile __bitf_T *)0xFFFFA0DC)->bit01)
#define P1CSIH1IRE         (((volatile __bitf_T *)0xFFFFA0DC)->bit01)
#define P2CSIG0IC_2        (((volatile __bitf_T *)0xFFFFA0DC)->bit02)
#define P2CSIH1IRE         (((volatile __bitf_T *)0xFFFFA0DC)->bit02)
#define TBCSIG0IC_2        (((volatile __bitf_T *)0xFFFFA0DC)->bit06)
#define TBCSIH1IRE         (((volatile __bitf_T *)0xFFFFA0DC)->bit06)
#define MKCSIG0IC_2        (((volatile __bitf_T *)0xFFFFA0DC)->bit07)
#define MKCSIH1IRE         (((volatile __bitf_T *)0xFFFFA0DC)->bit07)
#define RFCSIG0IC_2        (((volatile __bitf_T *)0xFFFFA0DD)->bit04)
#define RFCSIH1IRE         (((volatile __bitf_T *)0xFFFFA0DD)->bit04)
#define CTCSIG0IC_2        (((volatile __bitf_T *)0xFFFFA0DD)->bit07)
#define CTCSIH1IRE         (((volatile __bitf_T *)0xFFFFA0DD)->bit07)
__IOREG(ICCSIG0IR_2, 0xFFFFA0DE, __READ_WRITE, uint16_t);
__IOREG(ICCSIH1IJC, 0xFFFFA0DE, __READ_WRITE, uint16_t);
__IOREG(ICCSIG0IRL_2, 0xFFFFA0DE, __READ_WRITE, uint8_t);
__IOREG(ICCSIH1IJCL, 0xFFFFA0DE, __READ_WRITE, uint8_t);
__IOREG(ICCSIG0IRH_2, 0xFFFFA0DF, __READ_WRITE, uint8_t);
__IOREG(ICCSIH1IJCH, 0xFFFFA0DF, __READ_WRITE, uint8_t);
#define P0CSIG0IR_2        (((volatile __bitf_T *)0xFFFFA0DE)->bit00)
#define P0CSIH1IJC         (((volatile __bitf_T *)0xFFFFA0DE)->bit00)
#define P1CSIG0IR_2        (((volatile __bitf_T *)0xFFFFA0DE)->bit01)
#define P1CSIH1IJC         (((volatile __bitf_T *)0xFFFFA0DE)->bit01)
#define P2CSIG0IR_2        (((volatile __bitf_T *)0xFFFFA0DE)->bit02)
#define P2CSIH1IJC         (((volatile __bitf_T *)0xFFFFA0DE)->bit02)
#define TBCSIG0IR_2        (((volatile __bitf_T *)0xFFFFA0DE)->bit06)
#define TBCSIH1IJC         (((volatile __bitf_T *)0xFFFFA0DE)->bit06)
#define MKCSIG0IR_2        (((volatile __bitf_T *)0xFFFFA0DE)->bit07)
#define MKCSIH1IJC         (((volatile __bitf_T *)0xFFFFA0DE)->bit07)
#define RFCSIG0IR_2        (((volatile __bitf_T *)0xFFFFA0DF)->bit04)
#define RFCSIH1IJC         (((volatile __bitf_T *)0xFFFFA0DF)->bit04)
#define CTCSIG0IR_2        (((volatile __bitf_T *)0xFFFFA0DF)->bit07)
#define CTCSIH1IJC         (((volatile __bitf_T *)0xFFFFA0DF)->bit07)
__IOREG(ICRLIN31, 0xFFFFA0E0, __READ_WRITE, uint16_t);
__IOREG(ICRLIN31L, 0xFFFFA0E0, __READ_WRITE, uint8_t);
__IOREG(ICRLIN31H, 0xFFFFA0E1, __READ_WRITE, uint8_t);
#define P0RLIN31           (((volatile __bitf_T *)0xFFFFA0E0)->bit00)
#define P1RLIN31           (((volatile __bitf_T *)0xFFFFA0E0)->bit01)
#define P2RLIN31           (((volatile __bitf_T *)0xFFFFA0E0)->bit02)
#define TBRLIN31           (((volatile __bitf_T *)0xFFFFA0E0)->bit06)
#define MKRLIN31           (((volatile __bitf_T *)0xFFFFA0E0)->bit07)
#define RFRLIN31           (((volatile __bitf_T *)0xFFFFA0E1)->bit04)
#define CTRLIN31           (((volatile __bitf_T *)0xFFFFA0E1)->bit07)
__IOREG(ICRLIN31UR0, 0xFFFFA0E2, __READ_WRITE, uint16_t);
__IOREG(ICRLIN31UR0L, 0xFFFFA0E2, __READ_WRITE, uint8_t);
__IOREG(ICRLIN31UR0H, 0xFFFFA0E3, __READ_WRITE, uint8_t);
#define P0RLIN31UR0        (((volatile __bitf_T *)0xFFFFA0E2)->bit00)
#define P1RLIN31UR0        (((volatile __bitf_T *)0xFFFFA0E2)->bit01)
#define P2RLIN31UR0        (((volatile __bitf_T *)0xFFFFA0E2)->bit02)
#define TBRLIN31UR0        (((volatile __bitf_T *)0xFFFFA0E2)->bit06)
#define MKRLIN31UR0        (((volatile __bitf_T *)0xFFFFA0E2)->bit07)
#define RFRLIN31UR0        (((volatile __bitf_T *)0xFFFFA0E3)->bit04)
#define CTRLIN31UR0        (((volatile __bitf_T *)0xFFFFA0E3)->bit07)
__IOREG(ICRLIN31UR1, 0xFFFFA0E4, __READ_WRITE, uint16_t);
__IOREG(ICRLIN31UR1L, 0xFFFFA0E4, __READ_WRITE, uint8_t);
__IOREG(ICRLIN31UR1H, 0xFFFFA0E5, __READ_WRITE, uint8_t);
#define P0RLIN31UR1        (((volatile __bitf_T *)0xFFFFA0E4)->bit00)
#define P1RLIN31UR1        (((volatile __bitf_T *)0xFFFFA0E4)->bit01)
#define P2RLIN31UR1        (((volatile __bitf_T *)0xFFFFA0E4)->bit02)
#define TBRLIN31UR1        (((volatile __bitf_T *)0xFFFFA0E4)->bit06)
#define MKRLIN31UR1        (((volatile __bitf_T *)0xFFFFA0E4)->bit07)
#define RFRLIN31UR1        (((volatile __bitf_T *)0xFFFFA0E5)->bit04)
#define CTRLIN31UR1        (((volatile __bitf_T *)0xFFFFA0E5)->bit07)
__IOREG(ICRLIN31UR2, 0xFFFFA0E6, __READ_WRITE, uint16_t);
__IOREG(ICRLIN31UR2L, 0xFFFFA0E6, __READ_WRITE, uint8_t);
__IOREG(ICRLIN31UR2H, 0xFFFFA0E7, __READ_WRITE, uint8_t);
#define P0RLIN31UR2        (((volatile __bitf_T *)0xFFFFA0E6)->bit00)
#define P1RLIN31UR2        (((volatile __bitf_T *)0xFFFFA0E6)->bit01)
#define P2RLIN31UR2        (((volatile __bitf_T *)0xFFFFA0E6)->bit02)
#define TBRLIN31UR2        (((volatile __bitf_T *)0xFFFFA0E6)->bit06)
#define MKRLIN31UR2        (((volatile __bitf_T *)0xFFFFA0E6)->bit07)
#define RFRLIN31UR2        (((volatile __bitf_T *)0xFFFFA0E7)->bit04)
#define CTRLIN31UR2        (((volatile __bitf_T *)0xFFFFA0E7)->bit07)
__IOREG(ICPWGA20, 0xFFFFA0E8, __READ_WRITE, uint16_t);
__IOREG(ICPWGA20L, 0xFFFFA0E8, __READ_WRITE, uint8_t);
__IOREG(ICPWGA20H, 0xFFFFA0E9, __READ_WRITE, uint8_t);
#define P0PWGA20           (((volatile __bitf_T *)0xFFFFA0E8)->bit00)
#define P1PWGA20           (((volatile __bitf_T *)0xFFFFA0E8)->bit01)
#define P2PWGA20           (((volatile __bitf_T *)0xFFFFA0E8)->bit02)
#define TBPWGA20           (((volatile __bitf_T *)0xFFFFA0E8)->bit06)
#define MKPWGA20           (((volatile __bitf_T *)0xFFFFA0E8)->bit07)
#define RFPWGA20           (((volatile __bitf_T *)0xFFFFA0E9)->bit04)
#define CTPWGA20           (((volatile __bitf_T *)0xFFFFA0E9)->bit07)
__IOREG(ICPWGA21, 0xFFFFA0EA, __READ_WRITE, uint16_t);
__IOREG(ICPWGA21L, 0xFFFFA0EA, __READ_WRITE, uint8_t);
__IOREG(ICPWGA21H, 0xFFFFA0EB, __READ_WRITE, uint8_t);
#define P0PWGA21           (((volatile __bitf_T *)0xFFFFA0EA)->bit00)
#define P1PWGA21           (((volatile __bitf_T *)0xFFFFA0EA)->bit01)
#define P2PWGA21           (((volatile __bitf_T *)0xFFFFA0EA)->bit02)
#define TBPWGA21           (((volatile __bitf_T *)0xFFFFA0EA)->bit06)
#define MKPWGA21           (((volatile __bitf_T *)0xFFFFA0EA)->bit07)
#define RFPWGA21           (((volatile __bitf_T *)0xFFFFA0EB)->bit04)
#define CTPWGA21           (((volatile __bitf_T *)0xFFFFA0EB)->bit07)
__IOREG(ICPWGA22, 0xFFFFA0EC, __READ_WRITE, uint16_t);
__IOREG(ICPWGA22L, 0xFFFFA0EC, __READ_WRITE, uint8_t);
__IOREG(ICPWGA22H, 0xFFFFA0ED, __READ_WRITE, uint8_t);
#define P0PWGA22           (((volatile __bitf_T *)0xFFFFA0EC)->bit00)
#define P1PWGA22           (((volatile __bitf_T *)0xFFFFA0EC)->bit01)
#define P2PWGA22           (((volatile __bitf_T *)0xFFFFA0EC)->bit02)
#define TBPWGA22           (((volatile __bitf_T *)0xFFFFA0EC)->bit06)
#define MKPWGA22           (((volatile __bitf_T *)0xFFFFA0EC)->bit07)
#define RFPWGA22           (((volatile __bitf_T *)0xFFFFA0ED)->bit04)
#define CTPWGA22           (((volatile __bitf_T *)0xFFFFA0ED)->bit07)
__IOREG(ICPWGA23, 0xFFFFA0EE, __READ_WRITE, uint16_t);
__IOREG(ICPWGA23L, 0xFFFFA0EE, __READ_WRITE, uint8_t);
__IOREG(ICPWGA23H, 0xFFFFA0EF, __READ_WRITE, uint8_t);
#define P0PWGA23           (((volatile __bitf_T *)0xFFFFA0EE)->bit00)
#define P1PWGA23           (((volatile __bitf_T *)0xFFFFA0EE)->bit01)
#define P2PWGA23           (((volatile __bitf_T *)0xFFFFA0EE)->bit02)
#define TBPWGA23           (((volatile __bitf_T *)0xFFFFA0EE)->bit06)
#define MKPWGA23           (((volatile __bitf_T *)0xFFFFA0EE)->bit07)
#define RFPWGA23           (((volatile __bitf_T *)0xFFFFA0EF)->bit04)
#define CTPWGA23           (((volatile __bitf_T *)0xFFFFA0EF)->bit07)
__IOREG(ICP6, 0xFFFFA0F0, __READ_WRITE, uint16_t);
__IOREG(ICP6L, 0xFFFFA0F0, __READ_WRITE, uint8_t);
__IOREG(ICP6H, 0xFFFFA0F1, __READ_WRITE, uint8_t);
#define P0P6               (((volatile __bitf_T *)0xFFFFA0F0)->bit00)
#define P1P6               (((volatile __bitf_T *)0xFFFFA0F0)->bit01)
#define P2P6               (((volatile __bitf_T *)0xFFFFA0F0)->bit02)
#define TBP6               (((volatile __bitf_T *)0xFFFFA0F0)->bit06)
#define MKP6               (((volatile __bitf_T *)0xFFFFA0F0)->bit07)
#define RFP6               (((volatile __bitf_T *)0xFFFFA0F1)->bit04)
#define CTP6               (((volatile __bitf_T *)0xFFFFA0F1)->bit07)
__IOREG(ICP7, 0xFFFFA0F2, __READ_WRITE, uint16_t);
__IOREG(ICP7L, 0xFFFFA0F2, __READ_WRITE, uint8_t);
__IOREG(ICP7H, 0xFFFFA0F3, __READ_WRITE, uint8_t);
#define P0P7               (((volatile __bitf_T *)0xFFFFA0F2)->bit00)
#define P1P7               (((volatile __bitf_T *)0xFFFFA0F2)->bit01)
#define P2P7               (((volatile __bitf_T *)0xFFFFA0F2)->bit02)
#define TBP7               (((volatile __bitf_T *)0xFFFFA0F2)->bit06)
#define MKP7               (((volatile __bitf_T *)0xFFFFA0F2)->bit07)
#define RFP7               (((volatile __bitf_T *)0xFFFFA0F3)->bit04)
#define CTP7               (((volatile __bitf_T *)0xFFFFA0F3)->bit07)
__IOREG(ICP8, 0xFFFFA0F4, __READ_WRITE, uint16_t);
__IOREG(ICP8L, 0xFFFFA0F4, __READ_WRITE, uint8_t);
__IOREG(ICP8H, 0xFFFFA0F5, __READ_WRITE, uint8_t);
#define P0P8               (((volatile __bitf_T *)0xFFFFA0F4)->bit00)
#define P1P8               (((volatile __bitf_T *)0xFFFFA0F4)->bit01)
#define P2P8               (((volatile __bitf_T *)0xFFFFA0F4)->bit02)
#define TBP8               (((volatile __bitf_T *)0xFFFFA0F4)->bit06)
#define MKP8               (((volatile __bitf_T *)0xFFFFA0F4)->bit07)
#define RFP8               (((volatile __bitf_T *)0xFFFFA0F5)->bit04)
#define CTP8               (((volatile __bitf_T *)0xFFFFA0F5)->bit07)
__IOREG(ICP12, 0xFFFFA0F6, __READ_WRITE, uint16_t);
__IOREG(ICP12L, 0xFFFFA0F6, __READ_WRITE, uint8_t);
__IOREG(ICP12H, 0xFFFFA0F7, __READ_WRITE, uint8_t);
#define P0P12              (((volatile __bitf_T *)0xFFFFA0F6)->bit00)
#define P1P12              (((volatile __bitf_T *)0xFFFFA0F6)->bit01)
#define P2P12              (((volatile __bitf_T *)0xFFFFA0F6)->bit02)
#define TBP12              (((volatile __bitf_T *)0xFFFFA0F6)->bit06)
#define MKP12              (((volatile __bitf_T *)0xFFFFA0F6)->bit07)
#define RFP12              (((volatile __bitf_T *)0xFFFFA0F7)->bit04)
#define CTP12              (((volatile __bitf_T *)0xFFFFA0F7)->bit07)
__IOREG(ICCSIH2IC, 0xFFFFA0F8, __READ_WRITE, uint16_t);
__IOREG(ICTAUD0I0_2, 0xFFFFA0F8, __READ_WRITE, uint16_t);
__IOREG(ICCSIH2ICL, 0xFFFFA0F8, __READ_WRITE, uint8_t);
__IOREG(ICTAUD0I0L_2, 0xFFFFA0F8, __READ_WRITE, uint8_t);
__IOREG(ICCSIH2ICH, 0xFFFFA0F9, __READ_WRITE, uint8_t);
__IOREG(ICTAUD0I0H_2, 0xFFFFA0F9, __READ_WRITE, uint8_t);
#define P0CSIH2IC          (((volatile __bitf_T *)0xFFFFA0F8)->bit00)
#define P0TAUD0I0_2        (((volatile __bitf_T *)0xFFFFA0F8)->bit00)
#define P1CSIH2IC          (((volatile __bitf_T *)0xFFFFA0F8)->bit01)
#define P1TAUD0I0_2        (((volatile __bitf_T *)0xFFFFA0F8)->bit01)
#define P2CSIH2IC          (((volatile __bitf_T *)0xFFFFA0F8)->bit02)
#define P2TAUD0I0_2        (((volatile __bitf_T *)0xFFFFA0F8)->bit02)
#define TBCSIH2IC          (((volatile __bitf_T *)0xFFFFA0F8)->bit06)
#define TBTAUD0I0_2        (((volatile __bitf_T *)0xFFFFA0F8)->bit06)
#define MKCSIH2IC          (((volatile __bitf_T *)0xFFFFA0F8)->bit07)
#define MKTAUD0I0_2        (((volatile __bitf_T *)0xFFFFA0F8)->bit07)
#define RFCSIH2IC          (((volatile __bitf_T *)0xFFFFA0F9)->bit04)
#define RFTAUD0I0_2        (((volatile __bitf_T *)0xFFFFA0F9)->bit04)
#define CTCSIH2IC          (((volatile __bitf_T *)0xFFFFA0F9)->bit07)
#define CTTAUD0I0_2        (((volatile __bitf_T *)0xFFFFA0F9)->bit07)
__IOREG(ICCSIH2IR, 0xFFFFA0FA, __READ_WRITE, uint16_t);
__IOREG(ICP0_2, 0xFFFFA0FA, __READ_WRITE, uint16_t);
__IOREG(ICCSIH2IRL, 0xFFFFA0FA, __READ_WRITE, uint8_t);
__IOREG(ICP0L_2, 0xFFFFA0FA, __READ_WRITE, uint8_t);
__IOREG(ICCSIH2IRH, 0xFFFFA0FB, __READ_WRITE, uint8_t);
__IOREG(ICP0H_2, 0xFFFFA0FB, __READ_WRITE, uint8_t);
#define P0CSIH2IR          (((volatile __bitf_T *)0xFFFFA0FA)->bit00)
#define P0P0_2             (((volatile __bitf_T *)0xFFFFA0FA)->bit00)
#define P1CSIH2IR          (((volatile __bitf_T *)0xFFFFA0FA)->bit01)
#define P1P0_2             (((volatile __bitf_T *)0xFFFFA0FA)->bit01)
#define P2CSIH2IR          (((volatile __bitf_T *)0xFFFFA0FA)->bit02)
#define P2P0_2             (((volatile __bitf_T *)0xFFFFA0FA)->bit02)
#define TBCSIH2IR          (((volatile __bitf_T *)0xFFFFA0FA)->bit06)
#define TBP0_2             (((volatile __bitf_T *)0xFFFFA0FA)->bit06)
#define MKCSIH2IR          (((volatile __bitf_T *)0xFFFFA0FA)->bit07)
#define MKP0_2             (((volatile __bitf_T *)0xFFFFA0FA)->bit07)
#define RFCSIH2IR          (((volatile __bitf_T *)0xFFFFA0FB)->bit04)
#define RFP0_2             (((volatile __bitf_T *)0xFFFFA0FB)->bit04)
#define CTCSIH2IR          (((volatile __bitf_T *)0xFFFFA0FB)->bit07)
#define CTP0_2             (((volatile __bitf_T *)0xFFFFA0FB)->bit07)
__IOREG(ICCSIH2IRE, 0xFFFFA0FC, __READ_WRITE, uint16_t);
__IOREG(ICP1_2, 0xFFFFA0FC, __READ_WRITE, uint16_t);
__IOREG(ICCSIH2IREL, 0xFFFFA0FC, __READ_WRITE, uint8_t);
__IOREG(ICP1L_2, 0xFFFFA0FC, __READ_WRITE, uint8_t);
__IOREG(ICCSIH2IREH, 0xFFFFA0FD, __READ_WRITE, uint8_t);
__IOREG(ICP1H_2, 0xFFFFA0FD, __READ_WRITE, uint8_t);
#define P0CSIH2IRE         (((volatile __bitf_T *)0xFFFFA0FC)->bit00)
#define P0P1_2             (((volatile __bitf_T *)0xFFFFA0FC)->bit00)
#define P1CSIH2IRE         (((volatile __bitf_T *)0xFFFFA0FC)->bit01)
#define P1P1_2             (((volatile __bitf_T *)0xFFFFA0FC)->bit01)
#define P2CSIH2IRE         (((volatile __bitf_T *)0xFFFFA0FC)->bit02)
#define P2P1_2             (((volatile __bitf_T *)0xFFFFA0FC)->bit02)
#define TBCSIH2IRE         (((volatile __bitf_T *)0xFFFFA0FC)->bit06)
#define TBP1_2             (((volatile __bitf_T *)0xFFFFA0FC)->bit06)
#define MKCSIH2IRE         (((volatile __bitf_T *)0xFFFFA0FC)->bit07)
#define MKP1_2             (((volatile __bitf_T *)0xFFFFA0FC)->bit07)
#define RFCSIH2IRE         (((volatile __bitf_T *)0xFFFFA0FD)->bit04)
#define RFP1_2             (((volatile __bitf_T *)0xFFFFA0FD)->bit04)
#define CTCSIH2IRE         (((volatile __bitf_T *)0xFFFFA0FD)->bit07)
#define CTP1_2             (((volatile __bitf_T *)0xFFFFA0FD)->bit07)
__IOREG(ICCSIH2IJC, 0xFFFFA0FE, __READ_WRITE, uint16_t);
__IOREG(ICP2_2, 0xFFFFA0FE, __READ_WRITE, uint16_t);
__IOREG(ICCSIH2IJCL, 0xFFFFA0FE, __READ_WRITE, uint8_t);
__IOREG(ICP2L_2, 0xFFFFA0FE, __READ_WRITE, uint8_t);
__IOREG(ICCSIH2IJCH, 0xFFFFA0FF, __READ_WRITE, uint8_t);
__IOREG(ICP2H_2, 0xFFFFA0FF, __READ_WRITE, uint8_t);
#define P0CSIH2IJC         (((volatile __bitf_T *)0xFFFFA0FE)->bit00)
#define P0P2_2             (((volatile __bitf_T *)0xFFFFA0FE)->bit00)
#define P1CSIH2IJC         (((volatile __bitf_T *)0xFFFFA0FE)->bit01)
#define P1P2_2             (((volatile __bitf_T *)0xFFFFA0FE)->bit01)
#define P2CSIH2IJC         (((volatile __bitf_T *)0xFFFFA0FE)->bit02)
#define P2P2_2             (((volatile __bitf_T *)0xFFFFA0FE)->bit02)
#define TBCSIH2IJC         (((volatile __bitf_T *)0xFFFFA0FE)->bit06)
#define TBP2_2             (((volatile __bitf_T *)0xFFFFA0FE)->bit06)
#define MKCSIH2IJC         (((volatile __bitf_T *)0xFFFFA0FE)->bit07)
#define MKP2_2             (((volatile __bitf_T *)0xFFFFA0FE)->bit07)
#define RFCSIH2IJC         (((volatile __bitf_T *)0xFFFFA0FF)->bit04)
#define RFP2_2             (((volatile __bitf_T *)0xFFFFA0FF)->bit04)
#define CTCSIH2IJC         (((volatile __bitf_T *)0xFFFFA0FF)->bit07)
#define CTP2_2             (((volatile __bitf_T *)0xFFFFA0FF)->bit07)
__IOREG(ICTAUB0I0, 0xFFFFA10C, __READ_WRITE, uint16_t);
__IOREG(ICTAUB0I0L, 0xFFFFA10C, __READ_WRITE, uint8_t);
__IOREG(ICTAUB0I0H, 0xFFFFA10D, __READ_WRITE, uint8_t);
#define P0TAUB0I0          (((volatile __bitf_T *)0xFFFFA10C)->bit00)
#define P1TAUB0I0          (((volatile __bitf_T *)0xFFFFA10C)->bit01)
#define P2TAUB0I0          (((volatile __bitf_T *)0xFFFFA10C)->bit02)
#define TBTAUB0I0          (((volatile __bitf_T *)0xFFFFA10C)->bit06)
#define MKTAUB0I0          (((volatile __bitf_T *)0xFFFFA10C)->bit07)
#define RFTAUB0I0          (((volatile __bitf_T *)0xFFFFA10D)->bit04)
#define CTTAUB0I0          (((volatile __bitf_T *)0xFFFFA10D)->bit07)
__IOREG(ICTAUB0I1, 0xFFFFA10E, __READ_WRITE, uint16_t);
__IOREG(ICTAUB0I1L, 0xFFFFA10E, __READ_WRITE, uint8_t);
__IOREG(ICTAUB0I1H, 0xFFFFA10F, __READ_WRITE, uint8_t);
#define P0TAUB0I1          (((volatile __bitf_T *)0xFFFFA10E)->bit00)
#define P1TAUB0I1          (((volatile __bitf_T *)0xFFFFA10E)->bit01)
#define P2TAUB0I1          (((volatile __bitf_T *)0xFFFFA10E)->bit02)
#define TBTAUB0I1          (((volatile __bitf_T *)0xFFFFA10E)->bit06)
#define MKTAUB0I1          (((volatile __bitf_T *)0xFFFFA10E)->bit07)
#define RFTAUB0I1          (((volatile __bitf_T *)0xFFFFA10F)->bit04)
#define CTTAUB0I1          (((volatile __bitf_T *)0xFFFFA10F)->bit07)
__IOREG(ICTAUB0I2, 0xFFFFA110, __READ_WRITE, uint16_t);
__IOREG(ICTAUB0I2L, 0xFFFFA110, __READ_WRITE, uint8_t);
__IOREG(ICTAUB0I2H, 0xFFFFA111, __READ_WRITE, uint8_t);
#define P0TAUB0I2          (((volatile __bitf_T *)0xFFFFA110)->bit00)
#define P1TAUB0I2          (((volatile __bitf_T *)0xFFFFA110)->bit01)
#define P2TAUB0I2          (((volatile __bitf_T *)0xFFFFA110)->bit02)
#define TBTAUB0I2          (((volatile __bitf_T *)0xFFFFA110)->bit06)
#define MKTAUB0I2          (((volatile __bitf_T *)0xFFFFA110)->bit07)
#define RFTAUB0I2          (((volatile __bitf_T *)0xFFFFA111)->bit04)
#define CTTAUB0I2          (((volatile __bitf_T *)0xFFFFA111)->bit07)
__IOREG(ICPWGA16, 0xFFFFA112, __READ_WRITE, uint16_t);
__IOREG(ICTAUB0I3, 0xFFFFA112, __READ_WRITE, uint16_t);
__IOREG(ICPWGA16L, 0xFFFFA112, __READ_WRITE, uint8_t);
__IOREG(ICTAUB0I3L, 0xFFFFA112, __READ_WRITE, uint8_t);
__IOREG(ICPWGA16H, 0xFFFFA113, __READ_WRITE, uint8_t);
__IOREG(ICTAUB0I3H, 0xFFFFA113, __READ_WRITE, uint8_t);
#define P0PWGA16           (((volatile __bitf_T *)0xFFFFA112)->bit00)
#define P0TAUB0I3          (((volatile __bitf_T *)0xFFFFA112)->bit00)
#define P1PWGA16           (((volatile __bitf_T *)0xFFFFA112)->bit01)
#define P1TAUB0I3          (((volatile __bitf_T *)0xFFFFA112)->bit01)
#define P2PWGA16           (((volatile __bitf_T *)0xFFFFA112)->bit02)
#define P2TAUB0I3          (((volatile __bitf_T *)0xFFFFA112)->bit02)
#define TBPWGA16           (((volatile __bitf_T *)0xFFFFA112)->bit06)
#define TBTAUB0I3          (((volatile __bitf_T *)0xFFFFA112)->bit06)
#define MKPWGA16           (((volatile __bitf_T *)0xFFFFA112)->bit07)
#define MKTAUB0I3          (((volatile __bitf_T *)0xFFFFA112)->bit07)
#define RFPWGA16           (((volatile __bitf_T *)0xFFFFA113)->bit04)
#define RFTAUB0I3          (((volatile __bitf_T *)0xFFFFA113)->bit04)
#define CTPWGA16           (((volatile __bitf_T *)0xFFFFA113)->bit07)
#define CTTAUB0I3          (((volatile __bitf_T *)0xFFFFA113)->bit07)
__IOREG(ICTAUB0I4, 0xFFFFA114, __READ_WRITE, uint16_t);
__IOREG(ICTAUB0I4L, 0xFFFFA114, __READ_WRITE, uint8_t);
__IOREG(ICTAUB0I4H, 0xFFFFA115, __READ_WRITE, uint8_t);
#define P0TAUB0I4          (((volatile __bitf_T *)0xFFFFA114)->bit00)
#define P1TAUB0I4          (((volatile __bitf_T *)0xFFFFA114)->bit01)
#define P2TAUB0I4          (((volatile __bitf_T *)0xFFFFA114)->bit02)
#define TBTAUB0I4          (((volatile __bitf_T *)0xFFFFA114)->bit06)
#define MKTAUB0I4          (((volatile __bitf_T *)0xFFFFA114)->bit07)
#define RFTAUB0I4          (((volatile __bitf_T *)0xFFFFA115)->bit04)
#define CTTAUB0I4          (((volatile __bitf_T *)0xFFFFA115)->bit07)
__IOREG(ICPWGA17, 0xFFFFA116, __READ_WRITE, uint16_t);
__IOREG(ICTAUB0I5, 0xFFFFA116, __READ_WRITE, uint16_t);
__IOREG(ICPWGA17L, 0xFFFFA116, __READ_WRITE, uint8_t);
__IOREG(ICTAUB0I5L, 0xFFFFA116, __READ_WRITE, uint8_t);
__IOREG(ICPWGA17H, 0xFFFFA117, __READ_WRITE, uint8_t);
__IOREG(ICTAUB0I5H, 0xFFFFA117, __READ_WRITE, uint8_t);
#define P0PWGA17           (((volatile __bitf_T *)0xFFFFA116)->bit00)
#define P0TAUB0I5          (((volatile __bitf_T *)0xFFFFA116)->bit00)
#define P1PWGA17           (((volatile __bitf_T *)0xFFFFA116)->bit01)
#define P1TAUB0I5          (((volatile __bitf_T *)0xFFFFA116)->bit01)
#define P2PWGA17           (((volatile __bitf_T *)0xFFFFA116)->bit02)
#define P2TAUB0I5          (((volatile __bitf_T *)0xFFFFA116)->bit02)
#define TBPWGA17           (((volatile __bitf_T *)0xFFFFA116)->bit06)
#define TBTAUB0I5          (((volatile __bitf_T *)0xFFFFA116)->bit06)
#define MKPWGA17           (((volatile __bitf_T *)0xFFFFA116)->bit07)
#define MKTAUB0I5          (((volatile __bitf_T *)0xFFFFA116)->bit07)
#define RFPWGA17           (((volatile __bitf_T *)0xFFFFA117)->bit04)
#define RFTAUB0I5          (((volatile __bitf_T *)0xFFFFA117)->bit04)
#define CTPWGA17           (((volatile __bitf_T *)0xFFFFA117)->bit07)
#define CTTAUB0I5          (((volatile __bitf_T *)0xFFFFA117)->bit07)
__IOREG(ICTAUB0I6, 0xFFFFA118, __READ_WRITE, uint16_t);
__IOREG(ICTAUB0I6L, 0xFFFFA118, __READ_WRITE, uint8_t);
__IOREG(ICTAUB0I6H, 0xFFFFA119, __READ_WRITE, uint8_t);
#define P0TAUB0I6          (((volatile __bitf_T *)0xFFFFA118)->bit00)
#define P1TAUB0I6          (((volatile __bitf_T *)0xFFFFA118)->bit01)
#define P2TAUB0I6          (((volatile __bitf_T *)0xFFFFA118)->bit02)
#define TBTAUB0I6          (((volatile __bitf_T *)0xFFFFA118)->bit06)
#define MKTAUB0I6          (((volatile __bitf_T *)0xFFFFA118)->bit07)
#define RFTAUB0I6          (((volatile __bitf_T *)0xFFFFA119)->bit04)
#define CTTAUB0I6          (((volatile __bitf_T *)0xFFFFA119)->bit07)
__IOREG(ICPWGA18, 0xFFFFA11A, __READ_WRITE, uint16_t);
__IOREG(ICTAUB0I7, 0xFFFFA11A, __READ_WRITE, uint16_t);
__IOREG(ICPWGA18L, 0xFFFFA11A, __READ_WRITE, uint8_t);
__IOREG(ICTAUB0I7L, 0xFFFFA11A, __READ_WRITE, uint8_t);
__IOREG(ICPWGA18H, 0xFFFFA11B, __READ_WRITE, uint8_t);
__IOREG(ICTAUB0I7H, 0xFFFFA11B, __READ_WRITE, uint8_t);
#define P0PWGA18           (((volatile __bitf_T *)0xFFFFA11A)->bit00)
#define P0TAUB0I7          (((volatile __bitf_T *)0xFFFFA11A)->bit00)
#define P1PWGA18           (((volatile __bitf_T *)0xFFFFA11A)->bit01)
#define P1TAUB0I7          (((volatile __bitf_T *)0xFFFFA11A)->bit01)
#define P2PWGA18           (((volatile __bitf_T *)0xFFFFA11A)->bit02)
#define P2TAUB0I7          (((volatile __bitf_T *)0xFFFFA11A)->bit02)
#define TBPWGA18           (((volatile __bitf_T *)0xFFFFA11A)->bit06)
#define TBTAUB0I7          (((volatile __bitf_T *)0xFFFFA11A)->bit06)
#define MKPWGA18           (((volatile __bitf_T *)0xFFFFA11A)->bit07)
#define MKTAUB0I7          (((volatile __bitf_T *)0xFFFFA11A)->bit07)
#define RFPWGA18           (((volatile __bitf_T *)0xFFFFA11B)->bit04)
#define RFTAUB0I7          (((volatile __bitf_T *)0xFFFFA11B)->bit04)
#define CTPWGA18           (((volatile __bitf_T *)0xFFFFA11B)->bit07)
#define CTTAUB0I7          (((volatile __bitf_T *)0xFFFFA11B)->bit07)
__IOREG(ICTAUB0I8, 0xFFFFA11C, __READ_WRITE, uint16_t);
__IOREG(ICTAUB0I8L, 0xFFFFA11C, __READ_WRITE, uint8_t);
__IOREG(ICTAUB0I8H, 0xFFFFA11D, __READ_WRITE, uint8_t);
#define P0TAUB0I8          (((volatile __bitf_T *)0xFFFFA11C)->bit00)
#define P1TAUB0I8          (((volatile __bitf_T *)0xFFFFA11C)->bit01)
#define P2TAUB0I8          (((volatile __bitf_T *)0xFFFFA11C)->bit02)
#define TBTAUB0I8          (((volatile __bitf_T *)0xFFFFA11C)->bit06)
#define MKTAUB0I8          (((volatile __bitf_T *)0xFFFFA11C)->bit07)
#define RFTAUB0I8          (((volatile __bitf_T *)0xFFFFA11D)->bit04)
#define CTTAUB0I8          (((volatile __bitf_T *)0xFFFFA11D)->bit07)
__IOREG(ICPWGA19, 0xFFFFA11E, __READ_WRITE, uint16_t);
__IOREG(ICTAUB0I9, 0xFFFFA11E, __READ_WRITE, uint16_t);
__IOREG(ICPWGA19L, 0xFFFFA11E, __READ_WRITE, uint8_t);
__IOREG(ICTAUB0I9L, 0xFFFFA11E, __READ_WRITE, uint8_t);
__IOREG(ICPWGA19H, 0xFFFFA11F, __READ_WRITE, uint8_t);
__IOREG(ICTAUB0I9H, 0xFFFFA11F, __READ_WRITE, uint8_t);
#define P0PWGA19           (((volatile __bitf_T *)0xFFFFA11E)->bit00)
#define P0TAUB0I9          (((volatile __bitf_T *)0xFFFFA11E)->bit00)
#define P1PWGA19           (((volatile __bitf_T *)0xFFFFA11E)->bit01)
#define P1TAUB0I9          (((volatile __bitf_T *)0xFFFFA11E)->bit01)
#define P2PWGA19           (((volatile __bitf_T *)0xFFFFA11E)->bit02)
#define P2TAUB0I9          (((volatile __bitf_T *)0xFFFFA11E)->bit02)
#define TBPWGA19           (((volatile __bitf_T *)0xFFFFA11E)->bit06)
#define TBTAUB0I9          (((volatile __bitf_T *)0xFFFFA11E)->bit06)
#define MKPWGA19           (((volatile __bitf_T *)0xFFFFA11E)->bit07)
#define MKTAUB0I9          (((volatile __bitf_T *)0xFFFFA11E)->bit07)
#define RFPWGA19           (((volatile __bitf_T *)0xFFFFA11F)->bit04)
#define RFTAUB0I9          (((volatile __bitf_T *)0xFFFFA11F)->bit04)
#define CTPWGA19           (((volatile __bitf_T *)0xFFFFA11F)->bit07)
#define CTTAUB0I9          (((volatile __bitf_T *)0xFFFFA11F)->bit07)
__IOREG(ICTAUB0I10, 0xFFFFA120, __READ_WRITE, uint16_t);
__IOREG(ICTAUB0I10L, 0xFFFFA120, __READ_WRITE, uint8_t);
__IOREG(ICTAUB0I10H, 0xFFFFA121, __READ_WRITE, uint8_t);
#define P0TAUB0I10         (((volatile __bitf_T *)0xFFFFA120)->bit00)
#define P1TAUB0I10         (((volatile __bitf_T *)0xFFFFA120)->bit01)
#define P2TAUB0I10         (((volatile __bitf_T *)0xFFFFA120)->bit02)
#define TBTAUB0I10         (((volatile __bitf_T *)0xFFFFA120)->bit06)
#define MKTAUB0I10         (((volatile __bitf_T *)0xFFFFA120)->bit07)
#define RFTAUB0I10         (((volatile __bitf_T *)0xFFFFA121)->bit04)
#define CTTAUB0I10         (((volatile __bitf_T *)0xFFFFA121)->bit07)
__IOREG(ICPWGA26, 0xFFFFA122, __READ_WRITE, uint16_t);
__IOREG(ICTAUB0I11, 0xFFFFA122, __READ_WRITE, uint16_t);
__IOREG(ICPWGA26L, 0xFFFFA122, __READ_WRITE, uint8_t);
__IOREG(ICTAUB0I11L, 0xFFFFA122, __READ_WRITE, uint8_t);
__IOREG(ICPWGA26H, 0xFFFFA123, __READ_WRITE, uint8_t);
__IOREG(ICTAUB0I11H, 0xFFFFA123, __READ_WRITE, uint8_t);
#define P0PWGA26           (((volatile __bitf_T *)0xFFFFA122)->bit00)
#define P0TAUB0I11         (((volatile __bitf_T *)0xFFFFA122)->bit00)
#define P1PWGA26           (((volatile __bitf_T *)0xFFFFA122)->bit01)
#define P1TAUB0I11         (((volatile __bitf_T *)0xFFFFA122)->bit01)
#define P2PWGA26           (((volatile __bitf_T *)0xFFFFA122)->bit02)
#define P2TAUB0I11         (((volatile __bitf_T *)0xFFFFA122)->bit02)
#define TBPWGA26           (((volatile __bitf_T *)0xFFFFA122)->bit06)
#define TBTAUB0I11         (((volatile __bitf_T *)0xFFFFA122)->bit06)
#define MKPWGA26           (((volatile __bitf_T *)0xFFFFA122)->bit07)
#define MKTAUB0I11         (((volatile __bitf_T *)0xFFFFA122)->bit07)
#define RFPWGA26           (((volatile __bitf_T *)0xFFFFA123)->bit04)
#define RFTAUB0I11         (((volatile __bitf_T *)0xFFFFA123)->bit04)
#define CTPWGA26           (((volatile __bitf_T *)0xFFFFA123)->bit07)
#define CTTAUB0I11         (((volatile __bitf_T *)0xFFFFA123)->bit07)
__IOREG(ICTAUB0I12, 0xFFFFA124, __READ_WRITE, uint16_t);
__IOREG(ICTAUB0I12L, 0xFFFFA124, __READ_WRITE, uint8_t);
__IOREG(ICTAUB0I12H, 0xFFFFA125, __READ_WRITE, uint8_t);
#define P0TAUB0I12         (((volatile __bitf_T *)0xFFFFA124)->bit00)
#define P1TAUB0I12         (((volatile __bitf_T *)0xFFFFA124)->bit01)
#define P2TAUB0I12         (((volatile __bitf_T *)0xFFFFA124)->bit02)
#define TBTAUB0I12         (((volatile __bitf_T *)0xFFFFA124)->bit06)
#define MKTAUB0I12         (((volatile __bitf_T *)0xFFFFA124)->bit07)
#define RFTAUB0I12         (((volatile __bitf_T *)0xFFFFA125)->bit04)
#define CTTAUB0I12         (((volatile __bitf_T *)0xFFFFA125)->bit07)
__IOREG(ICPWGA30, 0xFFFFA126, __READ_WRITE, uint16_t);
__IOREG(ICTAUB0I13, 0xFFFFA126, __READ_WRITE, uint16_t);
__IOREG(ICPWGA30L, 0xFFFFA126, __READ_WRITE, uint8_t);
__IOREG(ICTAUB0I13L, 0xFFFFA126, __READ_WRITE, uint8_t);
__IOREG(ICPWGA30H, 0xFFFFA127, __READ_WRITE, uint8_t);
__IOREG(ICTAUB0I13H, 0xFFFFA127, __READ_WRITE, uint8_t);
#define P0PWGA30           (((volatile __bitf_T *)0xFFFFA126)->bit00)
#define P0TAUB0I13         (((volatile __bitf_T *)0xFFFFA126)->bit00)
#define P1PWGA30           (((volatile __bitf_T *)0xFFFFA126)->bit01)
#define P1TAUB0I13         (((volatile __bitf_T *)0xFFFFA126)->bit01)
#define P2PWGA30           (((volatile __bitf_T *)0xFFFFA126)->bit02)
#define P2TAUB0I13         (((volatile __bitf_T *)0xFFFFA126)->bit02)
#define TBPWGA30           (((volatile __bitf_T *)0xFFFFA126)->bit06)
#define TBTAUB0I13         (((volatile __bitf_T *)0xFFFFA126)->bit06)
#define MKPWGA30           (((volatile __bitf_T *)0xFFFFA126)->bit07)
#define MKTAUB0I13         (((volatile __bitf_T *)0xFFFFA126)->bit07)
#define RFPWGA30           (((volatile __bitf_T *)0xFFFFA127)->bit04)
#define RFTAUB0I13         (((volatile __bitf_T *)0xFFFFA127)->bit04)
#define CTPWGA30           (((volatile __bitf_T *)0xFFFFA127)->bit07)
#define CTTAUB0I13         (((volatile __bitf_T *)0xFFFFA127)->bit07)
__IOREG(ICTAUB0I14, 0xFFFFA128, __READ_WRITE, uint16_t);
__IOREG(ICTAUB0I14L, 0xFFFFA128, __READ_WRITE, uint8_t);
__IOREG(ICTAUB0I14H, 0xFFFFA129, __READ_WRITE, uint8_t);
#define P0TAUB0I14         (((volatile __bitf_T *)0xFFFFA128)->bit00)
#define P1TAUB0I14         (((volatile __bitf_T *)0xFFFFA128)->bit01)
#define P2TAUB0I14         (((volatile __bitf_T *)0xFFFFA128)->bit02)
#define TBTAUB0I14         (((volatile __bitf_T *)0xFFFFA128)->bit06)
#define MKTAUB0I14         (((volatile __bitf_T *)0xFFFFA128)->bit07)
#define RFTAUB0I14         (((volatile __bitf_T *)0xFFFFA129)->bit04)
#define CTTAUB0I14         (((volatile __bitf_T *)0xFFFFA129)->bit07)
__IOREG(ICPWGA31, 0xFFFFA12A, __READ_WRITE, uint16_t);
__IOREG(ICTAUB0I15, 0xFFFFA12A, __READ_WRITE, uint16_t);
__IOREG(ICPWGA31L, 0xFFFFA12A, __READ_WRITE, uint8_t);
__IOREG(ICTAUB0I15L, 0xFFFFA12A, __READ_WRITE, uint8_t);
__IOREG(ICPWGA31H, 0xFFFFA12B, __READ_WRITE, uint8_t);
__IOREG(ICTAUB0I15H, 0xFFFFA12B, __READ_WRITE, uint8_t);
#define P0PWGA31           (((volatile __bitf_T *)0xFFFFA12A)->bit00)
#define P0TAUB0I15         (((volatile __bitf_T *)0xFFFFA12A)->bit00)
#define P1PWGA31           (((volatile __bitf_T *)0xFFFFA12A)->bit01)
#define P1TAUB0I15         (((volatile __bitf_T *)0xFFFFA12A)->bit01)
#define P2PWGA31           (((volatile __bitf_T *)0xFFFFA12A)->bit02)
#define P2TAUB0I15         (((volatile __bitf_T *)0xFFFFA12A)->bit02)
#define TBPWGA31           (((volatile __bitf_T *)0xFFFFA12A)->bit06)
#define TBTAUB0I15         (((volatile __bitf_T *)0xFFFFA12A)->bit06)
#define MKPWGA31           (((volatile __bitf_T *)0xFFFFA12A)->bit07)
#define MKTAUB0I15         (((volatile __bitf_T *)0xFFFFA12A)->bit07)
#define RFPWGA31           (((volatile __bitf_T *)0xFFFFA12B)->bit04)
#define RFTAUB0I15         (((volatile __bitf_T *)0xFFFFA12B)->bit04)
#define CTPWGA31           (((volatile __bitf_T *)0xFFFFA12B)->bit07)
#define CTTAUB0I15         (((volatile __bitf_T *)0xFFFFA12B)->bit07)
__IOREG(ICCSIH3IC, 0xFFFFA12C, __READ_WRITE, uint16_t);
__IOREG(ICTAUD0I2_2, 0xFFFFA12C, __READ_WRITE, uint16_t);
__IOREG(ICCSIH3ICL, 0xFFFFA12C, __READ_WRITE, uint8_t);
__IOREG(ICTAUD0I2L_2, 0xFFFFA12C, __READ_WRITE, uint8_t);
__IOREG(ICCSIH3ICH, 0xFFFFA12D, __READ_WRITE, uint8_t);
__IOREG(ICTAUD0I2H_2, 0xFFFFA12D, __READ_WRITE, uint8_t);
#define P0CSIH3IC          (((volatile __bitf_T *)0xFFFFA12C)->bit00)
#define P0TAUD0I2_2        (((volatile __bitf_T *)0xFFFFA12C)->bit00)
#define P1CSIH3IC          (((volatile __bitf_T *)0xFFFFA12C)->bit01)
#define P1TAUD0I2_2        (((volatile __bitf_T *)0xFFFFA12C)->bit01)
#define P2CSIH3IC          (((volatile __bitf_T *)0xFFFFA12C)->bit02)
#define P2TAUD0I2_2        (((volatile __bitf_T *)0xFFFFA12C)->bit02)
#define TBCSIH3IC          (((volatile __bitf_T *)0xFFFFA12C)->bit06)
#define TBTAUD0I2_2        (((volatile __bitf_T *)0xFFFFA12C)->bit06)
#define MKCSIH3IC          (((volatile __bitf_T *)0xFFFFA12C)->bit07)
#define MKTAUD0I2_2        (((volatile __bitf_T *)0xFFFFA12C)->bit07)
#define RFCSIH3IC          (((volatile __bitf_T *)0xFFFFA12D)->bit04)
#define RFTAUD0I2_2        (((volatile __bitf_T *)0xFFFFA12D)->bit04)
#define CTCSIH3IC          (((volatile __bitf_T *)0xFFFFA12D)->bit07)
#define CTTAUD0I2_2        (((volatile __bitf_T *)0xFFFFA12D)->bit07)
__IOREG(ICCSIH3IR, 0xFFFFA12E, __READ_WRITE, uint16_t);
__IOREG(ICTAUD0I10_2, 0xFFFFA12E, __READ_WRITE, uint16_t);
__IOREG(ICCSIH3IRL, 0xFFFFA12E, __READ_WRITE, uint8_t);
__IOREG(ICTAUD0I10L_2, 0xFFFFA12E, __READ_WRITE, uint8_t);
__IOREG(ICCSIH3IRH, 0xFFFFA12F, __READ_WRITE, uint8_t);
__IOREG(ICTAUD0I10H_2, 0xFFFFA12F, __READ_WRITE, uint8_t);
#define P0CSIH3IR          (((volatile __bitf_T *)0xFFFFA12E)->bit00)
#define P0TAUD0I10_2       (((volatile __bitf_T *)0xFFFFA12E)->bit00)
#define P1CSIH3IR          (((volatile __bitf_T *)0xFFFFA12E)->bit01)
#define P1TAUD0I10_2       (((volatile __bitf_T *)0xFFFFA12E)->bit01)
#define P2CSIH3IR          (((volatile __bitf_T *)0xFFFFA12E)->bit02)
#define P2TAUD0I10_2       (((volatile __bitf_T *)0xFFFFA12E)->bit02)
#define TBCSIH3IR          (((volatile __bitf_T *)0xFFFFA12E)->bit06)
#define TBTAUD0I10_2       (((volatile __bitf_T *)0xFFFFA12E)->bit06)
#define MKCSIH3IR          (((volatile __bitf_T *)0xFFFFA12E)->bit07)
#define MKTAUD0I10_2       (((volatile __bitf_T *)0xFFFFA12E)->bit07)
#define RFCSIH3IR          (((volatile __bitf_T *)0xFFFFA12F)->bit04)
#define RFTAUD0I10_2       (((volatile __bitf_T *)0xFFFFA12F)->bit04)
#define CTCSIH3IR          (((volatile __bitf_T *)0xFFFFA12F)->bit07)
#define CTTAUD0I10_2       (((volatile __bitf_T *)0xFFFFA12F)->bit07)
__IOREG(ICCSIH3IRE, 0xFFFFA130, __READ_WRITE, uint16_t);
__IOREG(ICTAUD0I12_2, 0xFFFFA130, __READ_WRITE, uint16_t);
__IOREG(ICCSIH3IREL, 0xFFFFA130, __READ_WRITE, uint8_t);
__IOREG(ICTAUD0I12L_2, 0xFFFFA130, __READ_WRITE, uint8_t);
__IOREG(ICCSIH3IREH, 0xFFFFA131, __READ_WRITE, uint8_t);
__IOREG(ICTAUD0I12H_2, 0xFFFFA131, __READ_WRITE, uint8_t);
#define P0CSIH3IRE         (((volatile __bitf_T *)0xFFFFA130)->bit00)
#define P0TAUD0I12_2       (((volatile __bitf_T *)0xFFFFA130)->bit00)
#define P1CSIH3IRE         (((volatile __bitf_T *)0xFFFFA130)->bit01)
#define P1TAUD0I12_2       (((volatile __bitf_T *)0xFFFFA130)->bit01)
#define P2CSIH3IRE         (((volatile __bitf_T *)0xFFFFA130)->bit02)
#define P2TAUD0I12_2       (((volatile __bitf_T *)0xFFFFA130)->bit02)
#define TBCSIH3IRE         (((volatile __bitf_T *)0xFFFFA130)->bit06)
#define TBTAUD0I12_2       (((volatile __bitf_T *)0xFFFFA130)->bit06)
#define MKCSIH3IRE         (((volatile __bitf_T *)0xFFFFA130)->bit07)
#define MKTAUD0I12_2       (((volatile __bitf_T *)0xFFFFA130)->bit07)
#define RFCSIH3IRE         (((volatile __bitf_T *)0xFFFFA131)->bit04)
#define RFTAUD0I12_2       (((volatile __bitf_T *)0xFFFFA131)->bit04)
#define CTCSIH3IRE         (((volatile __bitf_T *)0xFFFFA131)->bit07)
#define CTTAUD0I12_2       (((volatile __bitf_T *)0xFFFFA131)->bit07)
__IOREG(ICCSIH3IJC, 0xFFFFA132, __READ_WRITE, uint16_t);
__IOREG(ICTAUD0I14_2, 0xFFFFA132, __READ_WRITE, uint16_t);
__IOREG(ICCSIH3IJCL, 0xFFFFA132, __READ_WRITE, uint8_t);
__IOREG(ICTAUD0I14L_2, 0xFFFFA132, __READ_WRITE, uint8_t);
__IOREG(ICCSIH3IJCH, 0xFFFFA133, __READ_WRITE, uint8_t);
__IOREG(ICTAUD0I14H_2, 0xFFFFA133, __READ_WRITE, uint8_t);
#define P0CSIH3IJC         (((volatile __bitf_T *)0xFFFFA132)->bit00)
#define P0TAUD0I14_2       (((volatile __bitf_T *)0xFFFFA132)->bit00)
#define P1CSIH3IJC         (((volatile __bitf_T *)0xFFFFA132)->bit01)
#define P1TAUD0I14_2       (((volatile __bitf_T *)0xFFFFA132)->bit01)
#define P2CSIH3IJC         (((volatile __bitf_T *)0xFFFFA132)->bit02)
#define P2TAUD0I14_2       (((volatile __bitf_T *)0xFFFFA132)->bit02)
#define TBCSIH3IJC         (((volatile __bitf_T *)0xFFFFA132)->bit06)
#define TBTAUD0I14_2       (((volatile __bitf_T *)0xFFFFA132)->bit06)
#define MKCSIH3IJC         (((volatile __bitf_T *)0xFFFFA132)->bit07)
#define MKTAUD0I14_2       (((volatile __bitf_T *)0xFFFFA132)->bit07)
#define RFCSIH3IJC         (((volatile __bitf_T *)0xFFFFA133)->bit04)
#define RFTAUD0I14_2       (((volatile __bitf_T *)0xFFFFA133)->bit04)
#define CTCSIH3IJC         (((volatile __bitf_T *)0xFFFFA133)->bit07)
#define CTTAUD0I14_2       (((volatile __bitf_T *)0xFFFFA133)->bit07)
__IOREG(ICRLIN22, 0xFFFFA134, __READ_WRITE, uint16_t);
__IOREG(ICRLIN22L, 0xFFFFA134, __READ_WRITE, uint8_t);
__IOREG(ICRLIN22H, 0xFFFFA135, __READ_WRITE, uint8_t);
#define P0RLIN22           (((volatile __bitf_T *)0xFFFFA134)->bit00)
#define P1RLIN22           (((volatile __bitf_T *)0xFFFFA134)->bit01)
#define P2RLIN22           (((volatile __bitf_T *)0xFFFFA134)->bit02)
#define TBRLIN22           (((volatile __bitf_T *)0xFFFFA134)->bit06)
#define MKRLIN22           (((volatile __bitf_T *)0xFFFFA134)->bit07)
#define RFRLIN22           (((volatile __bitf_T *)0xFFFFA135)->bit04)
#define CTRLIN22           (((volatile __bitf_T *)0xFFFFA135)->bit07)
__IOREG(ICRLIN23, 0xFFFFA136, __READ_WRITE, uint16_t);
__IOREG(ICRLIN23L, 0xFFFFA136, __READ_WRITE, uint8_t);
__IOREG(ICRLIN23H, 0xFFFFA137, __READ_WRITE, uint8_t);
#define P0RLIN23           (((volatile __bitf_T *)0xFFFFA136)->bit00)
#define P1RLIN23           (((volatile __bitf_T *)0xFFFFA136)->bit01)
#define P2RLIN23           (((volatile __bitf_T *)0xFFFFA136)->bit02)
#define TBRLIN23           (((volatile __bitf_T *)0xFFFFA136)->bit06)
#define MKRLIN23           (((volatile __bitf_T *)0xFFFFA136)->bit07)
#define RFRLIN23           (((volatile __bitf_T *)0xFFFFA137)->bit04)
#define CTRLIN23           (((volatile __bitf_T *)0xFFFFA137)->bit07)
__IOREG(ICRLIN32, 0xFFFFA138, __READ_WRITE, uint16_t);
__IOREG(ICRLIN32L, 0xFFFFA138, __READ_WRITE, uint8_t);
__IOREG(ICRLIN32H, 0xFFFFA139, __READ_WRITE, uint8_t);
#define P0RLIN32           (((volatile __bitf_T *)0xFFFFA138)->bit00)
#define P1RLIN32           (((volatile __bitf_T *)0xFFFFA138)->bit01)
#define P2RLIN32           (((volatile __bitf_T *)0xFFFFA138)->bit02)
#define TBRLIN32           (((volatile __bitf_T *)0xFFFFA138)->bit06)
#define MKRLIN32           (((volatile __bitf_T *)0xFFFFA138)->bit07)
#define RFRLIN32           (((volatile __bitf_T *)0xFFFFA139)->bit04)
#define CTRLIN32           (((volatile __bitf_T *)0xFFFFA139)->bit07)
__IOREG(ICRLIN32UR0, 0xFFFFA13A, __READ_WRITE, uint16_t);
__IOREG(ICRLIN32UR0L, 0xFFFFA13A, __READ_WRITE, uint8_t);
__IOREG(ICRLIN32UR0H, 0xFFFFA13B, __READ_WRITE, uint8_t);
#define P0RLIN32UR0        (((volatile __bitf_T *)0xFFFFA13A)->bit00)
#define P1RLIN32UR0        (((volatile __bitf_T *)0xFFFFA13A)->bit01)
#define P2RLIN32UR0        (((volatile __bitf_T *)0xFFFFA13A)->bit02)
#define TBRLIN32UR0        (((volatile __bitf_T *)0xFFFFA13A)->bit06)
#define MKRLIN32UR0        (((volatile __bitf_T *)0xFFFFA13A)->bit07)
#define RFRLIN32UR0        (((volatile __bitf_T *)0xFFFFA13B)->bit04)
#define CTRLIN32UR0        (((volatile __bitf_T *)0xFFFFA13B)->bit07)
__IOREG(ICRLIN32UR1, 0xFFFFA13C, __READ_WRITE, uint16_t);
__IOREG(ICRLIN32UR1L, 0xFFFFA13C, __READ_WRITE, uint8_t);
__IOREG(ICRLIN32UR1H, 0xFFFFA13D, __READ_WRITE, uint8_t);
#define P0RLIN32UR1        (((volatile __bitf_T *)0xFFFFA13C)->bit00)
#define P1RLIN32UR1        (((volatile __bitf_T *)0xFFFFA13C)->bit01)
#define P2RLIN32UR1        (((volatile __bitf_T *)0xFFFFA13C)->bit02)
#define TBRLIN32UR1        (((volatile __bitf_T *)0xFFFFA13C)->bit06)
#define MKRLIN32UR1        (((volatile __bitf_T *)0xFFFFA13C)->bit07)
#define RFRLIN32UR1        (((volatile __bitf_T *)0xFFFFA13D)->bit04)
#define CTRLIN32UR1        (((volatile __bitf_T *)0xFFFFA13D)->bit07)
__IOREG(ICRLIN32UR2, 0xFFFFA13E, __READ_WRITE, uint16_t);
__IOREG(ICRLIN32UR2L, 0xFFFFA13E, __READ_WRITE, uint8_t);
__IOREG(ICRLIN32UR2H, 0xFFFFA13F, __READ_WRITE, uint8_t);
#define P0RLIN32UR2        (((volatile __bitf_T *)0xFFFFA13E)->bit00)
#define P1RLIN32UR2        (((volatile __bitf_T *)0xFFFFA13E)->bit01)
#define P2RLIN32UR2        (((volatile __bitf_T *)0xFFFFA13E)->bit02)
#define TBRLIN32UR2        (((volatile __bitf_T *)0xFFFFA13E)->bit06)
#define MKRLIN32UR2        (((volatile __bitf_T *)0xFFFFA13E)->bit07)
#define RFRLIN32UR2        (((volatile __bitf_T *)0xFFFFA13F)->bit04)
#define CTRLIN32UR2        (((volatile __bitf_T *)0xFFFFA13F)->bit07)
__IOREG(ICTAUJ1I0, 0xFFFFA140, __READ_WRITE, uint16_t);
__IOREG(ICTAUJ1I0L, 0xFFFFA140, __READ_WRITE, uint8_t);
__IOREG(ICTAUJ1I0H, 0xFFFFA141, __READ_WRITE, uint8_t);
#define P0TAUJ1I0          (((volatile __bitf_T *)0xFFFFA140)->bit00)
#define P1TAUJ1I0          (((volatile __bitf_T *)0xFFFFA140)->bit01)
#define P2TAUJ1I0          (((volatile __bitf_T *)0xFFFFA140)->bit02)
#define TBTAUJ1I0          (((volatile __bitf_T *)0xFFFFA140)->bit06)
#define MKTAUJ1I0          (((volatile __bitf_T *)0xFFFFA140)->bit07)
#define RFTAUJ1I0          (((volatile __bitf_T *)0xFFFFA141)->bit04)
#define CTTAUJ1I0          (((volatile __bitf_T *)0xFFFFA141)->bit07)
__IOREG(ICTAUJ1I1, 0xFFFFA142, __READ_WRITE, uint16_t);
__IOREG(ICTAUJ1I1L, 0xFFFFA142, __READ_WRITE, uint8_t);
__IOREG(ICTAUJ1I1H, 0xFFFFA143, __READ_WRITE, uint8_t);
#define P0TAUJ1I1          (((volatile __bitf_T *)0xFFFFA142)->bit00)
#define P1TAUJ1I1          (((volatile __bitf_T *)0xFFFFA142)->bit01)
#define P2TAUJ1I1          (((volatile __bitf_T *)0xFFFFA142)->bit02)
#define TBTAUJ1I1          (((volatile __bitf_T *)0xFFFFA142)->bit06)
#define MKTAUJ1I1          (((volatile __bitf_T *)0xFFFFA142)->bit07)
#define RFTAUJ1I1          (((volatile __bitf_T *)0xFFFFA143)->bit04)
#define CTTAUJ1I1          (((volatile __bitf_T *)0xFFFFA143)->bit07)
__IOREG(ICTAUJ1I2, 0xFFFFA144, __READ_WRITE, uint16_t);
__IOREG(ICTAUJ1I2L, 0xFFFFA144, __READ_WRITE, uint8_t);
__IOREG(ICTAUJ1I2H, 0xFFFFA145, __READ_WRITE, uint8_t);
#define P0TAUJ1I2          (((volatile __bitf_T *)0xFFFFA144)->bit00)
#define P1TAUJ1I2          (((volatile __bitf_T *)0xFFFFA144)->bit01)
#define P2TAUJ1I2          (((volatile __bitf_T *)0xFFFFA144)->bit02)
#define TBTAUJ1I2          (((volatile __bitf_T *)0xFFFFA144)->bit06)
#define MKTAUJ1I2          (((volatile __bitf_T *)0xFFFFA144)->bit07)
#define RFTAUJ1I2          (((volatile __bitf_T *)0xFFFFA145)->bit04)
#define CTTAUJ1I2          (((volatile __bitf_T *)0xFFFFA145)->bit07)
__IOREG(ICTAUJ1I3, 0xFFFFA146, __READ_WRITE, uint16_t);
__IOREG(ICTAUJ1I3L, 0xFFFFA146, __READ_WRITE, uint8_t);
__IOREG(ICTAUJ1I3H, 0xFFFFA147, __READ_WRITE, uint8_t);
#define P0TAUJ1I3          (((volatile __bitf_T *)0xFFFFA146)->bit00)
#define P1TAUJ1I3          (((volatile __bitf_T *)0xFFFFA146)->bit01)
#define P2TAUJ1I3          (((volatile __bitf_T *)0xFFFFA146)->bit02)
#define TBTAUJ1I3          (((volatile __bitf_T *)0xFFFFA146)->bit06)
#define MKTAUJ1I3          (((volatile __bitf_T *)0xFFFFA146)->bit07)
#define RFTAUJ1I3          (((volatile __bitf_T *)0xFFFFA147)->bit04)
#define CTTAUJ1I3          (((volatile __bitf_T *)0xFFFFA147)->bit07)
__IOREG(ICPWGA24, 0xFFFFA160, __READ_WRITE, uint16_t);
__IOREG(ICPWGA24L, 0xFFFFA160, __READ_WRITE, uint8_t);
__IOREG(ICPWGA24H, 0xFFFFA161, __READ_WRITE, uint8_t);
#define P0PWGA24           (((volatile __bitf_T *)0xFFFFA160)->bit00)
#define P1PWGA24           (((volatile __bitf_T *)0xFFFFA160)->bit01)
#define P2PWGA24           (((volatile __bitf_T *)0xFFFFA160)->bit02)
#define TBPWGA24           (((volatile __bitf_T *)0xFFFFA160)->bit06)
#define MKPWGA24           (((volatile __bitf_T *)0xFFFFA160)->bit07)
#define RFPWGA24           (((volatile __bitf_T *)0xFFFFA161)->bit04)
#define CTPWGA24           (((volatile __bitf_T *)0xFFFFA161)->bit07)
__IOREG(ICPWGA25, 0xFFFFA162, __READ_WRITE, uint16_t);
__IOREG(ICPWGA25L, 0xFFFFA162, __READ_WRITE, uint8_t);
__IOREG(ICPWGA25H, 0xFFFFA163, __READ_WRITE, uint8_t);
#define P0PWGA25           (((volatile __bitf_T *)0xFFFFA162)->bit00)
#define P1PWGA25           (((volatile __bitf_T *)0xFFFFA162)->bit01)
#define P2PWGA25           (((volatile __bitf_T *)0xFFFFA162)->bit02)
#define TBPWGA25           (((volatile __bitf_T *)0xFFFFA162)->bit06)
#define MKPWGA25           (((volatile __bitf_T *)0xFFFFA162)->bit07)
#define RFPWGA25           (((volatile __bitf_T *)0xFFFFA163)->bit04)
#define CTPWGA25           (((volatile __bitf_T *)0xFFFFA163)->bit07)
__IOREG(ICPWGA27, 0xFFFFA164, __READ_WRITE, uint16_t);
__IOREG(ICPWGA27L, 0xFFFFA164, __READ_WRITE, uint8_t);
__IOREG(ICPWGA27H, 0xFFFFA165, __READ_WRITE, uint8_t);
#define P0PWGA27           (((volatile __bitf_T *)0xFFFFA164)->bit00)
#define P1PWGA27           (((volatile __bitf_T *)0xFFFFA164)->bit01)
#define P2PWGA27           (((volatile __bitf_T *)0xFFFFA164)->bit02)
#define TBPWGA27           (((volatile __bitf_T *)0xFFFFA164)->bit06)
#define MKPWGA27           (((volatile __bitf_T *)0xFFFFA164)->bit07)
#define RFPWGA27           (((volatile __bitf_T *)0xFFFFA165)->bit04)
#define CTPWGA27           (((volatile __bitf_T *)0xFFFFA165)->bit07)
__IOREG(ICPWGA28, 0xFFFFA166, __READ_WRITE, uint16_t);
__IOREG(ICPWGA28L, 0xFFFFA166, __READ_WRITE, uint8_t);
__IOREG(ICPWGA28H, 0xFFFFA167, __READ_WRITE, uint8_t);
#define P0PWGA28           (((volatile __bitf_T *)0xFFFFA166)->bit00)
#define P1PWGA28           (((volatile __bitf_T *)0xFFFFA166)->bit01)
#define P2PWGA28           (((volatile __bitf_T *)0xFFFFA166)->bit02)
#define TBPWGA28           (((volatile __bitf_T *)0xFFFFA166)->bit06)
#define MKPWGA28           (((volatile __bitf_T *)0xFFFFA166)->bit07)
#define RFPWGA28           (((volatile __bitf_T *)0xFFFFA167)->bit04)
#define CTPWGA28           (((volatile __bitf_T *)0xFFFFA167)->bit07)
__IOREG(ICPWGA29, 0xFFFFA168, __READ_WRITE, uint16_t);
__IOREG(ICPWGA29L, 0xFFFFA168, __READ_WRITE, uint8_t);
__IOREG(ICPWGA29H, 0xFFFFA169, __READ_WRITE, uint8_t);
#define P0PWGA29           (((volatile __bitf_T *)0xFFFFA168)->bit00)
#define P1PWGA29           (((volatile __bitf_T *)0xFFFFA168)->bit01)
#define P2PWGA29           (((volatile __bitf_T *)0xFFFFA168)->bit02)
#define TBPWGA29           (((volatile __bitf_T *)0xFFFFA168)->bit06)
#define MKPWGA29           (((volatile __bitf_T *)0xFFFFA168)->bit07)
#define RFPWGA29           (((volatile __bitf_T *)0xFFFFA169)->bit04)
#define CTPWGA29           (((volatile __bitf_T *)0xFFFFA169)->bit07)
__IOREG(ICPWGA32, 0xFFFFA16A, __READ_WRITE, uint16_t);
__IOREG(ICPWGA32L, 0xFFFFA16A, __READ_WRITE, uint8_t);
__IOREG(ICPWGA32H, 0xFFFFA16B, __READ_WRITE, uint8_t);
#define P0PWGA32           (((volatile __bitf_T *)0xFFFFA16A)->bit00)
#define P1PWGA32           (((volatile __bitf_T *)0xFFFFA16A)->bit01)
#define P2PWGA32           (((volatile __bitf_T *)0xFFFFA16A)->bit02)
#define TBPWGA32           (((volatile __bitf_T *)0xFFFFA16A)->bit06)
#define MKPWGA32           (((volatile __bitf_T *)0xFFFFA16A)->bit07)
#define RFPWGA32           (((volatile __bitf_T *)0xFFFFA16B)->bit04)
#define CTPWGA32           (((volatile __bitf_T *)0xFFFFA16B)->bit07)
__IOREG(ICPWGA33, 0xFFFFA16C, __READ_WRITE, uint16_t);
__IOREG(ICPWGA33L, 0xFFFFA16C, __READ_WRITE, uint8_t);
__IOREG(ICPWGA33H, 0xFFFFA16D, __READ_WRITE, uint8_t);
#define P0PWGA33           (((volatile __bitf_T *)0xFFFFA16C)->bit00)
#define P1PWGA33           (((volatile __bitf_T *)0xFFFFA16C)->bit01)
#define P2PWGA33           (((volatile __bitf_T *)0xFFFFA16C)->bit02)
#define TBPWGA33           (((volatile __bitf_T *)0xFFFFA16C)->bit06)
#define MKPWGA33           (((volatile __bitf_T *)0xFFFFA16C)->bit07)
#define RFPWGA33           (((volatile __bitf_T *)0xFFFFA16D)->bit04)
#define CTPWGA33           (((volatile __bitf_T *)0xFFFFA16D)->bit07)
__IOREG(ICPWGA34, 0xFFFFA16E, __READ_WRITE, uint16_t);
__IOREG(ICPWGA34L, 0xFFFFA16E, __READ_WRITE, uint8_t);
__IOREG(ICPWGA34H, 0xFFFFA16F, __READ_WRITE, uint8_t);
#define P0PWGA34           (((volatile __bitf_T *)0xFFFFA16E)->bit00)
#define P1PWGA34           (((volatile __bitf_T *)0xFFFFA16E)->bit01)
#define P2PWGA34           (((volatile __bitf_T *)0xFFFFA16E)->bit02)
#define TBPWGA34           (((volatile __bitf_T *)0xFFFFA16E)->bit06)
#define MKPWGA34           (((volatile __bitf_T *)0xFFFFA16E)->bit07)
#define RFPWGA34           (((volatile __bitf_T *)0xFFFFA16F)->bit04)
#define CTPWGA34           (((volatile __bitf_T *)0xFFFFA16F)->bit07)
__IOREG(ICPWGA35, 0xFFFFA170, __READ_WRITE, uint16_t);
__IOREG(ICPWGA35L, 0xFFFFA170, __READ_WRITE, uint8_t);
__IOREG(ICPWGA35H, 0xFFFFA171, __READ_WRITE, uint8_t);
#define P0PWGA35           (((volatile __bitf_T *)0xFFFFA170)->bit00)
#define P1PWGA35           (((volatile __bitf_T *)0xFFFFA170)->bit01)
#define P2PWGA35           (((volatile __bitf_T *)0xFFFFA170)->bit02)
#define TBPWGA35           (((volatile __bitf_T *)0xFFFFA170)->bit06)
#define MKPWGA35           (((volatile __bitf_T *)0xFFFFA170)->bit07)
#define RFPWGA35           (((volatile __bitf_T *)0xFFFFA171)->bit04)
#define CTPWGA35           (((volatile __bitf_T *)0xFFFFA171)->bit07)
__IOREG(ICPWGA36, 0xFFFFA172, __READ_WRITE, uint16_t);
__IOREG(ICPWGA36L, 0xFFFFA172, __READ_WRITE, uint8_t);
__IOREG(ICPWGA36H, 0xFFFFA173, __READ_WRITE, uint8_t);
#define P0PWGA36           (((volatile __bitf_T *)0xFFFFA172)->bit00)
#define P1PWGA36           (((volatile __bitf_T *)0xFFFFA172)->bit01)
#define P2PWGA36           (((volatile __bitf_T *)0xFFFFA172)->bit02)
#define TBPWGA36           (((volatile __bitf_T *)0xFFFFA172)->bit06)
#define MKPWGA36           (((volatile __bitf_T *)0xFFFFA172)->bit07)
#define RFPWGA36           (((volatile __bitf_T *)0xFFFFA173)->bit04)
#define CTPWGA36           (((volatile __bitf_T *)0xFFFFA173)->bit07)
__IOREG(ICPWGA37, 0xFFFFA174, __READ_WRITE, uint16_t);
__IOREG(ICPWGA37L, 0xFFFFA174, __READ_WRITE, uint8_t);
__IOREG(ICPWGA37H, 0xFFFFA175, __READ_WRITE, uint8_t);
#define P0PWGA37           (((volatile __bitf_T *)0xFFFFA174)->bit00)
#define P1PWGA37           (((volatile __bitf_T *)0xFFFFA174)->bit01)
#define P2PWGA37           (((volatile __bitf_T *)0xFFFFA174)->bit02)
#define TBPWGA37           (((volatile __bitf_T *)0xFFFFA174)->bit06)
#define MKPWGA37           (((volatile __bitf_T *)0xFFFFA174)->bit07)
#define RFPWGA37           (((volatile __bitf_T *)0xFFFFA175)->bit04)
#define CTPWGA37           (((volatile __bitf_T *)0xFFFFA175)->bit07)
__IOREG(ICPWGA38, 0xFFFFA176, __READ_WRITE, uint16_t);
__IOREG(ICPWGA38L, 0xFFFFA176, __READ_WRITE, uint8_t);
__IOREG(ICPWGA38H, 0xFFFFA177, __READ_WRITE, uint8_t);
#define P0PWGA38           (((volatile __bitf_T *)0xFFFFA176)->bit00)
#define P1PWGA38           (((volatile __bitf_T *)0xFFFFA176)->bit01)
#define P2PWGA38           (((volatile __bitf_T *)0xFFFFA176)->bit02)
#define TBPWGA38           (((volatile __bitf_T *)0xFFFFA176)->bit06)
#define MKPWGA38           (((volatile __bitf_T *)0xFFFFA176)->bit07)
#define RFPWGA38           (((volatile __bitf_T *)0xFFFFA177)->bit04)
#define CTPWGA38           (((volatile __bitf_T *)0xFFFFA177)->bit07)
__IOREG(ICPWGA39, 0xFFFFA178, __READ_WRITE, uint16_t);
__IOREG(ICPWGA39L, 0xFFFFA178, __READ_WRITE, uint8_t);
__IOREG(ICPWGA39H, 0xFFFFA179, __READ_WRITE, uint8_t);
#define P0PWGA39           (((volatile __bitf_T *)0xFFFFA178)->bit00)
#define P1PWGA39           (((volatile __bitf_T *)0xFFFFA178)->bit01)
#define P2PWGA39           (((volatile __bitf_T *)0xFFFFA178)->bit02)
#define TBPWGA39           (((volatile __bitf_T *)0xFFFFA178)->bit06)
#define MKPWGA39           (((volatile __bitf_T *)0xFFFFA178)->bit07)
#define RFPWGA39           (((volatile __bitf_T *)0xFFFFA179)->bit04)
#define CTPWGA39           (((volatile __bitf_T *)0xFFFFA179)->bit07)
__IOREG(ICPWGA40, 0xFFFFA17A, __READ_WRITE, uint16_t);
__IOREG(ICPWGA40L, 0xFFFFA17A, __READ_WRITE, uint8_t);
__IOREG(ICPWGA40H, 0xFFFFA17B, __READ_WRITE, uint8_t);
#define P0PWGA40           (((volatile __bitf_T *)0xFFFFA17A)->bit00)
#define P1PWGA40           (((volatile __bitf_T *)0xFFFFA17A)->bit01)
#define P2PWGA40           (((volatile __bitf_T *)0xFFFFA17A)->bit02)
#define TBPWGA40           (((volatile __bitf_T *)0xFFFFA17A)->bit06)
#define MKPWGA40           (((volatile __bitf_T *)0xFFFFA17A)->bit07)
#define RFPWGA40           (((volatile __bitf_T *)0xFFFFA17B)->bit04)
#define CTPWGA40           (((volatile __bitf_T *)0xFFFFA17B)->bit07)
__IOREG(ICPWGA41, 0xFFFFA17C, __READ_WRITE, uint16_t);
__IOREG(ICPWGA41L, 0xFFFFA17C, __READ_WRITE, uint8_t);
__IOREG(ICPWGA41H, 0xFFFFA17D, __READ_WRITE, uint8_t);
#define P0PWGA41           (((volatile __bitf_T *)0xFFFFA17C)->bit00)
#define P1PWGA41           (((volatile __bitf_T *)0xFFFFA17C)->bit01)
#define P2PWGA41           (((volatile __bitf_T *)0xFFFFA17C)->bit02)
#define TBPWGA41           (((volatile __bitf_T *)0xFFFFA17C)->bit06)
#define MKPWGA41           (((volatile __bitf_T *)0xFFFFA17C)->bit07)
#define RFPWGA41           (((volatile __bitf_T *)0xFFFFA17D)->bit04)
#define CTPWGA41           (((volatile __bitf_T *)0xFFFFA17D)->bit07)
__IOREG(ICPWGA42, 0xFFFFA17E, __READ_WRITE, uint16_t);
__IOREG(ICPWGA42L, 0xFFFFA17E, __READ_WRITE, uint8_t);
__IOREG(ICPWGA42H, 0xFFFFA17F, __READ_WRITE, uint8_t);
#define P0PWGA42           (((volatile __bitf_T *)0xFFFFA17E)->bit00)
#define P1PWGA42           (((volatile __bitf_T *)0xFFFFA17E)->bit01)
#define P2PWGA42           (((volatile __bitf_T *)0xFFFFA17E)->bit02)
#define TBPWGA42           (((volatile __bitf_T *)0xFFFFA17E)->bit06)
#define MKPWGA42           (((volatile __bitf_T *)0xFFFFA17E)->bit07)
#define RFPWGA42           (((volatile __bitf_T *)0xFFFFA17F)->bit04)
#define CTPWGA42           (((volatile __bitf_T *)0xFFFFA17F)->bit07)
__IOREG(ICPWGA43, 0xFFFFA180, __READ_WRITE, uint16_t);
__IOREG(ICPWGA43L, 0xFFFFA180, __READ_WRITE, uint8_t);
__IOREG(ICPWGA43H, 0xFFFFA181, __READ_WRITE, uint8_t);
#define P0PWGA43           (((volatile __bitf_T *)0xFFFFA180)->bit00)
#define P1PWGA43           (((volatile __bitf_T *)0xFFFFA180)->bit01)
#define P2PWGA43           (((volatile __bitf_T *)0xFFFFA180)->bit02)
#define TBPWGA43           (((volatile __bitf_T *)0xFFFFA180)->bit06)
#define MKPWGA43           (((volatile __bitf_T *)0xFFFFA180)->bit07)
#define RFPWGA43           (((volatile __bitf_T *)0xFFFFA181)->bit04)
#define CTPWGA43           (((volatile __bitf_T *)0xFFFFA181)->bit07)
__IOREG(ICPWGA44, 0xFFFFA182, __READ_WRITE, uint16_t);
__IOREG(ICPWGA44L, 0xFFFFA182, __READ_WRITE, uint8_t);
__IOREG(ICPWGA44H, 0xFFFFA183, __READ_WRITE, uint8_t);
#define P0PWGA44           (((volatile __bitf_T *)0xFFFFA182)->bit00)
#define P1PWGA44           (((volatile __bitf_T *)0xFFFFA182)->bit01)
#define P2PWGA44           (((volatile __bitf_T *)0xFFFFA182)->bit02)
#define TBPWGA44           (((volatile __bitf_T *)0xFFFFA182)->bit06)
#define MKPWGA44           (((volatile __bitf_T *)0xFFFFA182)->bit07)
#define RFPWGA44           (((volatile __bitf_T *)0xFFFFA183)->bit04)
#define CTPWGA44           (((volatile __bitf_T *)0xFFFFA183)->bit07)
__IOREG(ICPWGA45, 0xFFFFA184, __READ_WRITE, uint16_t);
__IOREG(ICPWGA45L, 0xFFFFA184, __READ_WRITE, uint8_t);
__IOREG(ICPWGA45H, 0xFFFFA185, __READ_WRITE, uint8_t);
#define P0PWGA45           (((volatile __bitf_T *)0xFFFFA184)->bit00)
#define P1PWGA45           (((volatile __bitf_T *)0xFFFFA184)->bit01)
#define P2PWGA45           (((volatile __bitf_T *)0xFFFFA184)->bit02)
#define TBPWGA45           (((volatile __bitf_T *)0xFFFFA184)->bit06)
#define MKPWGA45           (((volatile __bitf_T *)0xFFFFA184)->bit07)
#define RFPWGA45           (((volatile __bitf_T *)0xFFFFA185)->bit04)
#define CTPWGA45           (((volatile __bitf_T *)0xFFFFA185)->bit07)
__IOREG(ICPWGA46, 0xFFFFA186, __READ_WRITE, uint16_t);
__IOREG(ICPWGA46L, 0xFFFFA186, __READ_WRITE, uint8_t);
__IOREG(ICPWGA46H, 0xFFFFA187, __READ_WRITE, uint8_t);
#define P0PWGA46           (((volatile __bitf_T *)0xFFFFA186)->bit00)
#define P1PWGA46           (((volatile __bitf_T *)0xFFFFA186)->bit01)
#define P2PWGA46           (((volatile __bitf_T *)0xFFFFA186)->bit02)
#define TBPWGA46           (((volatile __bitf_T *)0xFFFFA186)->bit06)
#define MKPWGA46           (((volatile __bitf_T *)0xFFFFA186)->bit07)
#define RFPWGA46           (((volatile __bitf_T *)0xFFFFA187)->bit04)
#define CTPWGA46           (((volatile __bitf_T *)0xFFFFA187)->bit07)
__IOREG(ICPWGA47, 0xFFFFA188, __READ_WRITE, uint16_t);
__IOREG(ICPWGA47L, 0xFFFFA188, __READ_WRITE, uint8_t);
__IOREG(ICPWGA47H, 0xFFFFA189, __READ_WRITE, uint8_t);
#define P0PWGA47           (((volatile __bitf_T *)0xFFFFA188)->bit00)
#define P1PWGA47           (((volatile __bitf_T *)0xFFFFA188)->bit01)
#define P2PWGA47           (((volatile __bitf_T *)0xFFFFA188)->bit02)
#define TBPWGA47           (((volatile __bitf_T *)0xFFFFA188)->bit06)
#define MKPWGA47           (((volatile __bitf_T *)0xFFFFA188)->bit07)
#define RFPWGA47           (((volatile __bitf_T *)0xFFFFA189)->bit04)
#define CTPWGA47           (((volatile __bitf_T *)0xFFFFA189)->bit07)
__IOREG(ICP9, 0xFFFFA18A, __READ_WRITE, uint16_t);
__IOREG(ICP9L, 0xFFFFA18A, __READ_WRITE, uint8_t);
__IOREG(ICP9H, 0xFFFFA18B, __READ_WRITE, uint8_t);
#define P0P9               (((volatile __bitf_T *)0xFFFFA18A)->bit00)
#define P1P9               (((volatile __bitf_T *)0xFFFFA18A)->bit01)
#define P2P9               (((volatile __bitf_T *)0xFFFFA18A)->bit02)
#define TBP9               (((volatile __bitf_T *)0xFFFFA18A)->bit06)
#define MKP9               (((volatile __bitf_T *)0xFFFFA18A)->bit07)
#define RFP9               (((volatile __bitf_T *)0xFFFFA18B)->bit04)
#define CTP9               (((volatile __bitf_T *)0xFFFFA18B)->bit07)
__IOREG(ICP13, 0xFFFFA18C, __READ_WRITE, uint16_t);
__IOREG(ICP13L, 0xFFFFA18C, __READ_WRITE, uint8_t);
__IOREG(ICP13H, 0xFFFFA18D, __READ_WRITE, uint8_t);
#define P0P13              (((volatile __bitf_T *)0xFFFFA18C)->bit00)
#define P1P13              (((volatile __bitf_T *)0xFFFFA18C)->bit01)
#define P2P13              (((volatile __bitf_T *)0xFFFFA18C)->bit02)
#define TBP13              (((volatile __bitf_T *)0xFFFFA18C)->bit06)
#define MKP13              (((volatile __bitf_T *)0xFFFFA18C)->bit07)
#define RFP13              (((volatile __bitf_T *)0xFFFFA18D)->bit04)
#define CTP13              (((volatile __bitf_T *)0xFFFFA18D)->bit07)
__IOREG(ICP14, 0xFFFFA18E, __READ_WRITE, uint16_t);
__IOREG(ICP14L, 0xFFFFA18E, __READ_WRITE, uint8_t);
__IOREG(ICP14H, 0xFFFFA18F, __READ_WRITE, uint8_t);
#define P0P14              (((volatile __bitf_T *)0xFFFFA18E)->bit00)
#define P1P14              (((volatile __bitf_T *)0xFFFFA18E)->bit01)
#define P2P14              (((volatile __bitf_T *)0xFFFFA18E)->bit02)
#define TBP14              (((volatile __bitf_T *)0xFFFFA18E)->bit06)
#define MKP14              (((volatile __bitf_T *)0xFFFFA18E)->bit07)
#define RFP14              (((volatile __bitf_T *)0xFFFFA18F)->bit04)
#define CTP14              (((volatile __bitf_T *)0xFFFFA18F)->bit07)
__IOREG(ICP15, 0xFFFFA190, __READ_WRITE, uint16_t);
__IOREG(ICP15L, 0xFFFFA190, __READ_WRITE, uint8_t);
__IOREG(ICP15H, 0xFFFFA191, __READ_WRITE, uint8_t);
#define P0P15              (((volatile __bitf_T *)0xFFFFA190)->bit00)
#define P1P15              (((volatile __bitf_T *)0xFFFFA190)->bit01)
#define P2P15              (((volatile __bitf_T *)0xFFFFA190)->bit02)
#define TBP15              (((volatile __bitf_T *)0xFFFFA190)->bit06)
#define MKP15              (((volatile __bitf_T *)0xFFFFA190)->bit07)
#define RFP15              (((volatile __bitf_T *)0xFFFFA191)->bit04)
#define CTP15              (((volatile __bitf_T *)0xFFFFA191)->bit07)
__IOREG(ICRTCA01S, 0xFFFFA192, __READ_WRITE, uint16_t);
__IOREG(ICRTCA01SL, 0xFFFFA192, __READ_WRITE, uint8_t);
__IOREG(ICRTCA01SH, 0xFFFFA193, __READ_WRITE, uint8_t);
#define P0RTCA01S          (((volatile __bitf_T *)0xFFFFA192)->bit00)
#define P1RTCA01S          (((volatile __bitf_T *)0xFFFFA192)->bit01)
#define P2RTCA01S          (((volatile __bitf_T *)0xFFFFA192)->bit02)
#define TBRTCA01S          (((volatile __bitf_T *)0xFFFFA192)->bit06)
#define MKRTCA01S          (((volatile __bitf_T *)0xFFFFA192)->bit07)
#define RFRTCA01S          (((volatile __bitf_T *)0xFFFFA193)->bit04)
#define CTRTCA01S          (((volatile __bitf_T *)0xFFFFA193)->bit07)
__IOREG(ICRTCA0AL, 0xFFFFA194, __READ_WRITE, uint16_t);
__IOREG(ICRTCA0ALL, 0xFFFFA194, __READ_WRITE, uint8_t);
__IOREG(ICRTCA0ALH, 0xFFFFA195, __READ_WRITE, uint8_t);
#define P0RTCA0AL          (((volatile __bitf_T *)0xFFFFA194)->bit00)
#define P1RTCA0AL          (((volatile __bitf_T *)0xFFFFA194)->bit01)
#define P2RTCA0AL          (((volatile __bitf_T *)0xFFFFA194)->bit02)
#define TBRTCA0AL          (((volatile __bitf_T *)0xFFFFA194)->bit06)
#define MKRTCA0AL          (((volatile __bitf_T *)0xFFFFA194)->bit07)
#define RFRTCA0AL          (((volatile __bitf_T *)0xFFFFA195)->bit04)
#define CTRTCA0AL          (((volatile __bitf_T *)0xFFFFA195)->bit07)
__IOREG(ICRTCA0R, 0xFFFFA196, __READ_WRITE, uint16_t);
__IOREG(ICRTCA0RL, 0xFFFFA196, __READ_WRITE, uint8_t);
__IOREG(ICRTCA0RH, 0xFFFFA197, __READ_WRITE, uint8_t);
#define P0RTCA0R           (((volatile __bitf_T *)0xFFFFA196)->bit00)
#define P1RTCA0R           (((volatile __bitf_T *)0xFFFFA196)->bit01)
#define P2RTCA0R           (((volatile __bitf_T *)0xFFFFA196)->bit02)
#define TBRTCA0R           (((volatile __bitf_T *)0xFFFFA196)->bit06)
#define MKRTCA0R           (((volatile __bitf_T *)0xFFFFA196)->bit07)
#define RFRTCA0R           (((volatile __bitf_T *)0xFFFFA197)->bit04)
#define CTRTCA0R           (((volatile __bitf_T *)0xFFFFA197)->bit07)
__IOREG(ICADCA1ERR, 0xFFFFA198, __READ_WRITE, uint16_t);
__IOREG(ICADCA1ERRL, 0xFFFFA198, __READ_WRITE, uint8_t);
__IOREG(ICADCA1ERRH, 0xFFFFA199, __READ_WRITE, uint8_t);
#define P0ADCA1ERR         (((volatile __bitf_T *)0xFFFFA198)->bit00)
#define P1ADCA1ERR         (((volatile __bitf_T *)0xFFFFA198)->bit01)
#define P2ADCA1ERR         (((volatile __bitf_T *)0xFFFFA198)->bit02)
#define TBADCA1ERR         (((volatile __bitf_T *)0xFFFFA198)->bit06)
#define MKADCA1ERR         (((volatile __bitf_T *)0xFFFFA198)->bit07)
#define RFADCA1ERR         (((volatile __bitf_T *)0xFFFFA199)->bit04)
#define CTADCA1ERR         (((volatile __bitf_T *)0xFFFFA199)->bit07)
__IOREG(ICADCA1I0, 0xFFFFA19A, __READ_WRITE, uint16_t);
__IOREG(ICADCA1I0L, 0xFFFFA19A, __READ_WRITE, uint8_t);
__IOREG(ICADCA1I0H, 0xFFFFA19B, __READ_WRITE, uint8_t);
#define P0ADCA1I0          (((volatile __bitf_T *)0xFFFFA19A)->bit00)
#define P1ADCA1I0          (((volatile __bitf_T *)0xFFFFA19A)->bit01)
#define P2ADCA1I0          (((volatile __bitf_T *)0xFFFFA19A)->bit02)
#define TBADCA1I0          (((volatile __bitf_T *)0xFFFFA19A)->bit06)
#define MKADCA1I0          (((volatile __bitf_T *)0xFFFFA19A)->bit07)
#define RFADCA1I0          (((volatile __bitf_T *)0xFFFFA19B)->bit04)
#define CTADCA1I0          (((volatile __bitf_T *)0xFFFFA19B)->bit07)
__IOREG(ICADCA1I1, 0xFFFFA19C, __READ_WRITE, uint16_t);
__IOREG(ICADCA1I1L, 0xFFFFA19C, __READ_WRITE, uint8_t);
__IOREG(ICADCA1I1H, 0xFFFFA19D, __READ_WRITE, uint8_t);
#define P0ADCA1I1          (((volatile __bitf_T *)0xFFFFA19C)->bit00)
#define P1ADCA1I1          (((volatile __bitf_T *)0xFFFFA19C)->bit01)
#define P2ADCA1I1          (((volatile __bitf_T *)0xFFFFA19C)->bit02)
#define TBADCA1I1          (((volatile __bitf_T *)0xFFFFA19C)->bit06)
#define MKADCA1I1          (((volatile __bitf_T *)0xFFFFA19C)->bit07)
#define RFADCA1I1          (((volatile __bitf_T *)0xFFFFA19D)->bit04)
#define CTADCA1I1          (((volatile __bitf_T *)0xFFFFA19D)->bit07)
__IOREG(ICADCA1I2, 0xFFFFA19E, __READ_WRITE, uint16_t);
__IOREG(ICADCA1I2L, 0xFFFFA19E, __READ_WRITE, uint8_t);
__IOREG(ICADCA1I2H, 0xFFFFA19F, __READ_WRITE, uint8_t);
#define P0ADCA1I2          (((volatile __bitf_T *)0xFFFFA19E)->bit00)
#define P1ADCA1I2          (((volatile __bitf_T *)0xFFFFA19E)->bit01)
#define P2ADCA1I2          (((volatile __bitf_T *)0xFFFFA19E)->bit02)
#define TBADCA1I2          (((volatile __bitf_T *)0xFFFFA19E)->bit06)
#define MKADCA1I2          (((volatile __bitf_T *)0xFFFFA19E)->bit07)
#define RFADCA1I2          (((volatile __bitf_T *)0xFFFFA19F)->bit04)
#define CTADCA1I2          (((volatile __bitf_T *)0xFFFFA19F)->bit07)
__IOREG(ICRCAN2ERR, 0xFFFFA1A2, __READ_WRITE, uint16_t);
__IOREG(ICRCAN2ERRL, 0xFFFFA1A2, __READ_WRITE, uint8_t);
__IOREG(ICRCAN2ERRH, 0xFFFFA1A3, __READ_WRITE, uint8_t);
#define P0RCAN2ERR         (((volatile __bitf_T *)0xFFFFA1A2)->bit00)
#define P1RCAN2ERR         (((volatile __bitf_T *)0xFFFFA1A2)->bit01)
#define P2RCAN2ERR         (((volatile __bitf_T *)0xFFFFA1A2)->bit02)
#define TBRCAN2ERR         (((volatile __bitf_T *)0xFFFFA1A2)->bit06)
#define MKRCAN2ERR         (((volatile __bitf_T *)0xFFFFA1A2)->bit07)
#define RFRCAN2ERR         (((volatile __bitf_T *)0xFFFFA1A3)->bit04)
#define CTRCAN2ERR         (((volatile __bitf_T *)0xFFFFA1A3)->bit07)
__IOREG(ICRCAN2REC, 0xFFFFA1A4, __READ_WRITE, uint16_t);
__IOREG(ICRCAN2RECL, 0xFFFFA1A4, __READ_WRITE, uint8_t);
__IOREG(ICRCAN2RECH, 0xFFFFA1A5, __READ_WRITE, uint8_t);
#define P0RCAN2REC         (((volatile __bitf_T *)0xFFFFA1A4)->bit00)
#define P1RCAN2REC         (((volatile __bitf_T *)0xFFFFA1A4)->bit01)
#define P2RCAN2REC         (((volatile __bitf_T *)0xFFFFA1A4)->bit02)
#define TBRCAN2REC         (((volatile __bitf_T *)0xFFFFA1A4)->bit06)
#define MKRCAN2REC         (((volatile __bitf_T *)0xFFFFA1A4)->bit07)
#define RFRCAN2REC         (((volatile __bitf_T *)0xFFFFA1A5)->bit04)
#define CTRCAN2REC         (((volatile __bitf_T *)0xFFFFA1A5)->bit07)
__IOREG(ICRCAN2TRX, 0xFFFFA1A6, __READ_WRITE, uint16_t);
__IOREG(ICRCAN2TRXL, 0xFFFFA1A6, __READ_WRITE, uint8_t);
__IOREG(ICRCAN2TRXH, 0xFFFFA1A7, __READ_WRITE, uint8_t);
#define P0RCAN2TRX         (((volatile __bitf_T *)0xFFFFA1A6)->bit00)
#define P1RCAN2TRX         (((volatile __bitf_T *)0xFFFFA1A6)->bit01)
#define P2RCAN2TRX         (((volatile __bitf_T *)0xFFFFA1A6)->bit02)
#define TBRCAN2TRX         (((volatile __bitf_T *)0xFFFFA1A6)->bit06)
#define MKRCAN2TRX         (((volatile __bitf_T *)0xFFFFA1A6)->bit07)
#define RFRCAN2TRX         (((volatile __bitf_T *)0xFFFFA1A7)->bit04)
#define CTRCAN2TRX         (((volatile __bitf_T *)0xFFFFA1A7)->bit07)
__IOREG(ICRCAN3ERR, 0xFFFFA1A8, __READ_WRITE, uint16_t);
__IOREG(ICRCAN3ERRL, 0xFFFFA1A8, __READ_WRITE, uint8_t);
__IOREG(ICRCAN3ERRH, 0xFFFFA1A9, __READ_WRITE, uint8_t);
#define P0RCAN3ERR         (((volatile __bitf_T *)0xFFFFA1A8)->bit00)
#define P1RCAN3ERR         (((volatile __bitf_T *)0xFFFFA1A8)->bit01)
#define P2RCAN3ERR         (((volatile __bitf_T *)0xFFFFA1A8)->bit02)
#define TBRCAN3ERR         (((volatile __bitf_T *)0xFFFFA1A8)->bit06)
#define MKRCAN3ERR         (((volatile __bitf_T *)0xFFFFA1A8)->bit07)
#define RFRCAN3ERR         (((volatile __bitf_T *)0xFFFFA1A9)->bit04)
#define CTRCAN3ERR         (((volatile __bitf_T *)0xFFFFA1A9)->bit07)
__IOREG(ICRCAN3REC, 0xFFFFA1AA, __READ_WRITE, uint16_t);
__IOREG(ICRCAN3RECL, 0xFFFFA1AA, __READ_WRITE, uint8_t);
__IOREG(ICRCAN3RECH, 0xFFFFA1AB, __READ_WRITE, uint8_t);
#define P0RCAN3REC         (((volatile __bitf_T *)0xFFFFA1AA)->bit00)
#define P1RCAN3REC         (((volatile __bitf_T *)0xFFFFA1AA)->bit01)
#define P2RCAN3REC         (((volatile __bitf_T *)0xFFFFA1AA)->bit02)
#define TBRCAN3REC         (((volatile __bitf_T *)0xFFFFA1AA)->bit06)
#define MKRCAN3REC         (((volatile __bitf_T *)0xFFFFA1AA)->bit07)
#define RFRCAN3REC         (((volatile __bitf_T *)0xFFFFA1AB)->bit04)
#define CTRCAN3REC         (((volatile __bitf_T *)0xFFFFA1AB)->bit07)
__IOREG(ICRCAN3TRX, 0xFFFFA1AC, __READ_WRITE, uint16_t);
__IOREG(ICRCAN3TRXL, 0xFFFFA1AC, __READ_WRITE, uint8_t);
__IOREG(ICRCAN3TRXH, 0xFFFFA1AD, __READ_WRITE, uint8_t);
#define P0RCAN3TRX         (((volatile __bitf_T *)0xFFFFA1AC)->bit00)
#define P1RCAN3TRX         (((volatile __bitf_T *)0xFFFFA1AC)->bit01)
#define P2RCAN3TRX         (((volatile __bitf_T *)0xFFFFA1AC)->bit02)
#define TBRCAN3TRX         (((volatile __bitf_T *)0xFFFFA1AC)->bit06)
#define MKRCAN3TRX         (((volatile __bitf_T *)0xFFFFA1AC)->bit07)
#define RFRCAN3TRX         (((volatile __bitf_T *)0xFFFFA1AD)->bit04)
#define CTRCAN3TRX         (((volatile __bitf_T *)0xFFFFA1AD)->bit07)
__IOREG(ICCSIG1IC, 0xFFFFA1AE, __READ_WRITE, uint16_t);
__IOREG(ICCSIG1ICL, 0xFFFFA1AE, __READ_WRITE, uint8_t);
__IOREG(ICCSIG1ICH, 0xFFFFA1AF, __READ_WRITE, uint8_t);
#define P0CSIG1IC          (((volatile __bitf_T *)0xFFFFA1AE)->bit00)
#define P1CSIG1IC          (((volatile __bitf_T *)0xFFFFA1AE)->bit01)
#define P2CSIG1IC          (((volatile __bitf_T *)0xFFFFA1AE)->bit02)
#define TBCSIG1IC          (((volatile __bitf_T *)0xFFFFA1AE)->bit06)
#define MKCSIG1IC          (((volatile __bitf_T *)0xFFFFA1AE)->bit07)
#define RFCSIG1IC          (((volatile __bitf_T *)0xFFFFA1AF)->bit04)
#define CTCSIG1IC          (((volatile __bitf_T *)0xFFFFA1AF)->bit07)
__IOREG(ICCSIG1IR, 0xFFFFA1B0, __READ_WRITE, uint16_t);
__IOREG(ICCSIG1IRL, 0xFFFFA1B0, __READ_WRITE, uint8_t);
__IOREG(ICCSIG1IRH, 0xFFFFA1B1, __READ_WRITE, uint8_t);
#define P0CSIG1IR          (((volatile __bitf_T *)0xFFFFA1B0)->bit00)
#define P1CSIG1IR          (((volatile __bitf_T *)0xFFFFA1B0)->bit01)
#define P2CSIG1IR          (((volatile __bitf_T *)0xFFFFA1B0)->bit02)
#define TBCSIG1IR          (((volatile __bitf_T *)0xFFFFA1B0)->bit06)
#define MKCSIG1IR          (((volatile __bitf_T *)0xFFFFA1B0)->bit07)
#define RFCSIG1IR          (((volatile __bitf_T *)0xFFFFA1B1)->bit04)
#define CTCSIG1IR          (((volatile __bitf_T *)0xFFFFA1B1)->bit07)
__IOREG(ICCSIG1IRE, 0xFFFFA1B2, __READ_WRITE, uint16_t);
__IOREG(ICCSIG1IREL, 0xFFFFA1B2, __READ_WRITE, uint8_t);
__IOREG(ICCSIG1IREH, 0xFFFFA1B3, __READ_WRITE, uint8_t);
#define P0CSIG1IRE         (((volatile __bitf_T *)0xFFFFA1B2)->bit00)
#define P1CSIG1IRE         (((volatile __bitf_T *)0xFFFFA1B2)->bit01)
#define P2CSIG1IRE         (((volatile __bitf_T *)0xFFFFA1B2)->bit02)
#define TBCSIG1IRE         (((volatile __bitf_T *)0xFFFFA1B2)->bit06)
#define MKCSIG1IRE         (((volatile __bitf_T *)0xFFFFA1B2)->bit07)
#define RFCSIG1IRE         (((volatile __bitf_T *)0xFFFFA1B3)->bit04)
#define CTCSIG1IRE         (((volatile __bitf_T *)0xFFFFA1B3)->bit07)
__IOREG(ICRLIN24, 0xFFFFA1B4, __READ_WRITE, uint16_t);
__IOREG(ICRLIN24L, 0xFFFFA1B4, __READ_WRITE, uint8_t);
__IOREG(ICRLIN24H, 0xFFFFA1B5, __READ_WRITE, uint8_t);
#define P0RLIN24           (((volatile __bitf_T *)0xFFFFA1B4)->bit00)
#define P1RLIN24           (((volatile __bitf_T *)0xFFFFA1B4)->bit01)
#define P2RLIN24           (((volatile __bitf_T *)0xFFFFA1B4)->bit02)
#define TBRLIN24           (((volatile __bitf_T *)0xFFFFA1B4)->bit06)
#define MKRLIN24           (((volatile __bitf_T *)0xFFFFA1B4)->bit07)
#define RFRLIN24           (((volatile __bitf_T *)0xFFFFA1B5)->bit04)
#define CTRLIN24           (((volatile __bitf_T *)0xFFFFA1B5)->bit07)
__IOREG(ICRLIN25, 0xFFFFA1B6, __READ_WRITE, uint16_t);
__IOREG(ICRLIN25L, 0xFFFFA1B6, __READ_WRITE, uint8_t);
__IOREG(ICRLIN25H, 0xFFFFA1B7, __READ_WRITE, uint8_t);
#define P0RLIN25           (((volatile __bitf_T *)0xFFFFA1B6)->bit00)
#define P1RLIN25           (((volatile __bitf_T *)0xFFFFA1B6)->bit01)
#define P2RLIN25           (((volatile __bitf_T *)0xFFFFA1B6)->bit02)
#define TBRLIN25           (((volatile __bitf_T *)0xFFFFA1B6)->bit06)
#define MKRLIN25           (((volatile __bitf_T *)0xFFFFA1B6)->bit07)
#define RFRLIN25           (((volatile __bitf_T *)0xFFFFA1B7)->bit04)
#define CTRLIN25           (((volatile __bitf_T *)0xFFFFA1B7)->bit07)
__IOREG(ICRLIN33, 0xFFFFA1B8, __READ_WRITE, uint16_t);
__IOREG(ICRLIN33L, 0xFFFFA1B8, __READ_WRITE, uint8_t);
__IOREG(ICRLIN33H, 0xFFFFA1B9, __READ_WRITE, uint8_t);
#define P0RLIN33           (((volatile __bitf_T *)0xFFFFA1B8)->bit00)
#define P1RLIN33           (((volatile __bitf_T *)0xFFFFA1B8)->bit01)
#define P2RLIN33           (((volatile __bitf_T *)0xFFFFA1B8)->bit02)
#define TBRLIN33           (((volatile __bitf_T *)0xFFFFA1B8)->bit06)
#define MKRLIN33           (((volatile __bitf_T *)0xFFFFA1B8)->bit07)
#define RFRLIN33           (((volatile __bitf_T *)0xFFFFA1B9)->bit04)
#define CTRLIN33           (((volatile __bitf_T *)0xFFFFA1B9)->bit07)
__IOREG(ICRLIN33UR0, 0xFFFFA1BA, __READ_WRITE, uint16_t);
__IOREG(ICRLIN33UR0L, 0xFFFFA1BA, __READ_WRITE, uint8_t);
__IOREG(ICRLIN33UR0H, 0xFFFFA1BB, __READ_WRITE, uint8_t);
#define P0RLIN33UR0        (((volatile __bitf_T *)0xFFFFA1BA)->bit00)
#define P1RLIN33UR0        (((volatile __bitf_T *)0xFFFFA1BA)->bit01)
#define P2RLIN33UR0        (((volatile __bitf_T *)0xFFFFA1BA)->bit02)
#define TBRLIN33UR0        (((volatile __bitf_T *)0xFFFFA1BA)->bit06)
#define MKRLIN33UR0        (((volatile __bitf_T *)0xFFFFA1BA)->bit07)
#define RFRLIN33UR0        (((volatile __bitf_T *)0xFFFFA1BB)->bit04)
#define CTRLIN33UR0        (((volatile __bitf_T *)0xFFFFA1BB)->bit07)
__IOREG(ICRLIN33UR1, 0xFFFFA1BC, __READ_WRITE, uint16_t);
__IOREG(ICRLIN33UR1L, 0xFFFFA1BC, __READ_WRITE, uint8_t);
__IOREG(ICRLIN33UR1H, 0xFFFFA1BD, __READ_WRITE, uint8_t);
#define P0RLIN33UR1        (((volatile __bitf_T *)0xFFFFA1BC)->bit00)
#define P1RLIN33UR1        (((volatile __bitf_T *)0xFFFFA1BC)->bit01)
#define P2RLIN33UR1        (((volatile __bitf_T *)0xFFFFA1BC)->bit02)
#define TBRLIN33UR1        (((volatile __bitf_T *)0xFFFFA1BC)->bit06)
#define MKRLIN33UR1        (((volatile __bitf_T *)0xFFFFA1BC)->bit07)
#define RFRLIN33UR1        (((volatile __bitf_T *)0xFFFFA1BD)->bit04)
#define CTRLIN33UR1        (((volatile __bitf_T *)0xFFFFA1BD)->bit07)
__IOREG(ICRLIN33UR2, 0xFFFFA1BE, __READ_WRITE, uint16_t);
__IOREG(ICRLIN33UR2L, 0xFFFFA1BE, __READ_WRITE, uint8_t);
__IOREG(ICRLIN33UR2H, 0xFFFFA1BF, __READ_WRITE, uint8_t);
#define P0RLIN33UR2        (((volatile __bitf_T *)0xFFFFA1BE)->bit00)
#define P1RLIN33UR2        (((volatile __bitf_T *)0xFFFFA1BE)->bit01)
#define P2RLIN33UR2        (((volatile __bitf_T *)0xFFFFA1BE)->bit02)
#define TBRLIN33UR2        (((volatile __bitf_T *)0xFFFFA1BE)->bit06)
#define MKRLIN33UR2        (((volatile __bitf_T *)0xFFFFA1BE)->bit07)
#define RFRLIN33UR2        (((volatile __bitf_T *)0xFFFFA1BF)->bit04)
#define CTRLIN33UR2        (((volatile __bitf_T *)0xFFFFA1BF)->bit07)
__IOREG(ICRLIN34, 0xFFFFA1C0, __READ_WRITE, uint16_t);
__IOREG(ICRLIN34L, 0xFFFFA1C0, __READ_WRITE, uint8_t);
__IOREG(ICRLIN34H, 0xFFFFA1C1, __READ_WRITE, uint8_t);
#define P0RLIN34           (((volatile __bitf_T *)0xFFFFA1C0)->bit00)
#define P1RLIN34           (((volatile __bitf_T *)0xFFFFA1C0)->bit01)
#define P2RLIN34           (((volatile __bitf_T *)0xFFFFA1C0)->bit02)
#define TBRLIN34           (((volatile __bitf_T *)0xFFFFA1C0)->bit06)
#define MKRLIN34           (((volatile __bitf_T *)0xFFFFA1C0)->bit07)
#define RFRLIN34           (((volatile __bitf_T *)0xFFFFA1C1)->bit04)
#define CTRLIN34           (((volatile __bitf_T *)0xFFFFA1C1)->bit07)
__IOREG(ICRLIN34UR0, 0xFFFFA1C2, __READ_WRITE, uint16_t);
__IOREG(ICRLIN34UR0L, 0xFFFFA1C2, __READ_WRITE, uint8_t);
__IOREG(ICRLIN34UR0H, 0xFFFFA1C3, __READ_WRITE, uint8_t);
#define P0RLIN34UR0        (((volatile __bitf_T *)0xFFFFA1C2)->bit00)
#define P1RLIN34UR0        (((volatile __bitf_T *)0xFFFFA1C2)->bit01)
#define P2RLIN34UR0        (((volatile __bitf_T *)0xFFFFA1C2)->bit02)
#define TBRLIN34UR0        (((volatile __bitf_T *)0xFFFFA1C2)->bit06)
#define MKRLIN34UR0        (((volatile __bitf_T *)0xFFFFA1C2)->bit07)
#define RFRLIN34UR0        (((volatile __bitf_T *)0xFFFFA1C3)->bit04)
#define CTRLIN34UR0        (((volatile __bitf_T *)0xFFFFA1C3)->bit07)
__IOREG(ICRLIN34UR1, 0xFFFFA1C4, __READ_WRITE, uint16_t);
__IOREG(ICRLIN34UR1L, 0xFFFFA1C4, __READ_WRITE, uint8_t);
__IOREG(ICRLIN34UR1H, 0xFFFFA1C5, __READ_WRITE, uint8_t);
#define P0RLIN34UR1        (((volatile __bitf_T *)0xFFFFA1C4)->bit00)
#define P1RLIN34UR1        (((volatile __bitf_T *)0xFFFFA1C4)->bit01)
#define P2RLIN34UR1        (((volatile __bitf_T *)0xFFFFA1C4)->bit02)
#define TBRLIN34UR1        (((volatile __bitf_T *)0xFFFFA1C4)->bit06)
#define MKRLIN34UR1        (((volatile __bitf_T *)0xFFFFA1C4)->bit07)
#define RFRLIN34UR1        (((volatile __bitf_T *)0xFFFFA1C5)->bit04)
#define CTRLIN34UR1        (((volatile __bitf_T *)0xFFFFA1C5)->bit07)
__IOREG(ICRLIN34UR2, 0xFFFFA1C6, __READ_WRITE, uint16_t);
__IOREG(ICRLIN34UR2L, 0xFFFFA1C6, __READ_WRITE, uint8_t);
__IOREG(ICRLIN34UR2H, 0xFFFFA1C7, __READ_WRITE, uint8_t);
#define P0RLIN34UR2        (((volatile __bitf_T *)0xFFFFA1C6)->bit00)
#define P1RLIN34UR2        (((volatile __bitf_T *)0xFFFFA1C6)->bit01)
#define P2RLIN34UR2        (((volatile __bitf_T *)0xFFFFA1C6)->bit02)
#define TBRLIN34UR2        (((volatile __bitf_T *)0xFFFFA1C6)->bit06)
#define MKRLIN34UR2        (((volatile __bitf_T *)0xFFFFA1C6)->bit07)
#define RFRLIN34UR2        (((volatile __bitf_T *)0xFFFFA1C7)->bit04)
#define CTRLIN34UR2        (((volatile __bitf_T *)0xFFFFA1C7)->bit07)
__IOREG(ICRLIN35, 0xFFFFA1C8, __READ_WRITE, uint16_t);
__IOREG(ICRLIN35L, 0xFFFFA1C8, __READ_WRITE, uint8_t);
__IOREG(ICRLIN35H, 0xFFFFA1C9, __READ_WRITE, uint8_t);
#define P0RLIN35           (((volatile __bitf_T *)0xFFFFA1C8)->bit00)
#define P1RLIN35           (((volatile __bitf_T *)0xFFFFA1C8)->bit01)
#define P2RLIN35           (((volatile __bitf_T *)0xFFFFA1C8)->bit02)
#define TBRLIN35           (((volatile __bitf_T *)0xFFFFA1C8)->bit06)
#define MKRLIN35           (((volatile __bitf_T *)0xFFFFA1C8)->bit07)
#define RFRLIN35           (((volatile __bitf_T *)0xFFFFA1C9)->bit04)
#define CTRLIN35           (((volatile __bitf_T *)0xFFFFA1C9)->bit07)
__IOREG(ICRLIN35UR0, 0xFFFFA1CA, __READ_WRITE, uint16_t);
__IOREG(ICRLIN35UR0L, 0xFFFFA1CA, __READ_WRITE, uint8_t);
__IOREG(ICRLIN35UR0H, 0xFFFFA1CB, __READ_WRITE, uint8_t);
#define P0RLIN35UR0        (((volatile __bitf_T *)0xFFFFA1CA)->bit00)
#define P1RLIN35UR0        (((volatile __bitf_T *)0xFFFFA1CA)->bit01)
#define P2RLIN35UR0        (((volatile __bitf_T *)0xFFFFA1CA)->bit02)
#define TBRLIN35UR0        (((volatile __bitf_T *)0xFFFFA1CA)->bit06)
#define MKRLIN35UR0        (((volatile __bitf_T *)0xFFFFA1CA)->bit07)
#define RFRLIN35UR0        (((volatile __bitf_T *)0xFFFFA1CB)->bit04)
#define CTRLIN35UR0        (((volatile __bitf_T *)0xFFFFA1CB)->bit07)
__IOREG(ICRLIN35UR1, 0xFFFFA1CC, __READ_WRITE, uint16_t);
__IOREG(ICRLIN35UR1L, 0xFFFFA1CC, __READ_WRITE, uint8_t);
__IOREG(ICRLIN35UR1H, 0xFFFFA1CD, __READ_WRITE, uint8_t);
#define P0RLIN35UR1        (((volatile __bitf_T *)0xFFFFA1CC)->bit00)
#define P1RLIN35UR1        (((volatile __bitf_T *)0xFFFFA1CC)->bit01)
#define P2RLIN35UR1        (((volatile __bitf_T *)0xFFFFA1CC)->bit02)
#define TBRLIN35UR1        (((volatile __bitf_T *)0xFFFFA1CC)->bit06)
#define MKRLIN35UR1        (((volatile __bitf_T *)0xFFFFA1CC)->bit07)
#define RFRLIN35UR1        (((volatile __bitf_T *)0xFFFFA1CD)->bit04)
#define CTRLIN35UR1        (((volatile __bitf_T *)0xFFFFA1CD)->bit07)
__IOREG(ICRLIN35UR2, 0xFFFFA1CE, __READ_WRITE, uint16_t);
__IOREG(ICRLIN35UR2L, 0xFFFFA1CE, __READ_WRITE, uint8_t);
__IOREG(ICRLIN35UR2H, 0xFFFFA1CF, __READ_WRITE, uint8_t);
#define P0RLIN35UR2        (((volatile __bitf_T *)0xFFFFA1CE)->bit00)
#define P1RLIN35UR2        (((volatile __bitf_T *)0xFFFFA1CE)->bit01)
#define P2RLIN35UR2        (((volatile __bitf_T *)0xFFFFA1CE)->bit02)
#define TBRLIN35UR2        (((volatile __bitf_T *)0xFFFFA1CE)->bit06)
#define MKRLIN35UR2        (((volatile __bitf_T *)0xFFFFA1CE)->bit07)
#define RFRLIN35UR2        (((volatile __bitf_T *)0xFFFFA1CF)->bit04)
#define CTRLIN35UR2        (((volatile __bitf_T *)0xFFFFA1CF)->bit07)
__IOREG(ICPWGA48, 0xFFFFA1D0, __READ_WRITE, uint16_t);
__IOREG(ICPWGA48L, 0xFFFFA1D0, __READ_WRITE, uint8_t);
__IOREG(ICPWGA48H, 0xFFFFA1D1, __READ_WRITE, uint8_t);
#define P0PWGA48           (((volatile __bitf_T *)0xFFFFA1D0)->bit00)
#define P1PWGA48           (((volatile __bitf_T *)0xFFFFA1D0)->bit01)
#define P2PWGA48           (((volatile __bitf_T *)0xFFFFA1D0)->bit02)
#define TBPWGA48           (((volatile __bitf_T *)0xFFFFA1D0)->bit06)
#define MKPWGA48           (((volatile __bitf_T *)0xFFFFA1D0)->bit07)
#define RFPWGA48           (((volatile __bitf_T *)0xFFFFA1D1)->bit04)
#define CTPWGA48           (((volatile __bitf_T *)0xFFFFA1D1)->bit07)
__IOREG(ICPWGA49, 0xFFFFA1D2, __READ_WRITE, uint16_t);
__IOREG(ICPWGA49L, 0xFFFFA1D2, __READ_WRITE, uint8_t);
__IOREG(ICPWGA49H, 0xFFFFA1D3, __READ_WRITE, uint8_t);
#define P0PWGA49           (((volatile __bitf_T *)0xFFFFA1D2)->bit00)
#define P1PWGA49           (((volatile __bitf_T *)0xFFFFA1D2)->bit01)
#define P2PWGA49           (((volatile __bitf_T *)0xFFFFA1D2)->bit02)
#define TBPWGA49           (((volatile __bitf_T *)0xFFFFA1D2)->bit06)
#define MKPWGA49           (((volatile __bitf_T *)0xFFFFA1D2)->bit07)
#define RFPWGA49           (((volatile __bitf_T *)0xFFFFA1D3)->bit04)
#define CTPWGA49           (((volatile __bitf_T *)0xFFFFA1D3)->bit07)
__IOREG(ICPWGA50, 0xFFFFA1D4, __READ_WRITE, uint16_t);
__IOREG(ICPWGA50L, 0xFFFFA1D4, __READ_WRITE, uint8_t);
__IOREG(ICPWGA50H, 0xFFFFA1D5, __READ_WRITE, uint8_t);
#define P0PWGA50           (((volatile __bitf_T *)0xFFFFA1D4)->bit00)
#define P1PWGA50           (((volatile __bitf_T *)0xFFFFA1D4)->bit01)
#define P2PWGA50           (((volatile __bitf_T *)0xFFFFA1D4)->bit02)
#define TBPWGA50           (((volatile __bitf_T *)0xFFFFA1D4)->bit06)
#define MKPWGA50           (((volatile __bitf_T *)0xFFFFA1D4)->bit07)
#define RFPWGA50           (((volatile __bitf_T *)0xFFFFA1D5)->bit04)
#define CTPWGA50           (((volatile __bitf_T *)0xFFFFA1D5)->bit07)
__IOREG(ICPWGA51, 0xFFFFA1D6, __READ_WRITE, uint16_t);
__IOREG(ICPWGA51L, 0xFFFFA1D6, __READ_WRITE, uint8_t);
__IOREG(ICPWGA51H, 0xFFFFA1D7, __READ_WRITE, uint8_t);
#define P0PWGA51           (((volatile __bitf_T *)0xFFFFA1D6)->bit00)
#define P1PWGA51           (((volatile __bitf_T *)0xFFFFA1D6)->bit01)
#define P2PWGA51           (((volatile __bitf_T *)0xFFFFA1D6)->bit02)
#define TBPWGA51           (((volatile __bitf_T *)0xFFFFA1D6)->bit06)
#define MKPWGA51           (((volatile __bitf_T *)0xFFFFA1D6)->bit07)
#define RFPWGA51           (((volatile __bitf_T *)0xFFFFA1D7)->bit04)
#define CTPWGA51           (((volatile __bitf_T *)0xFFFFA1D7)->bit07)
__IOREG(ICPWGA52, 0xFFFFA1D8, __READ_WRITE, uint16_t);
__IOREG(ICPWGA52L, 0xFFFFA1D8, __READ_WRITE, uint8_t);
__IOREG(ICPWGA52H, 0xFFFFA1D9, __READ_WRITE, uint8_t);
#define P0PWGA52           (((volatile __bitf_T *)0xFFFFA1D8)->bit00)
#define P1PWGA52           (((volatile __bitf_T *)0xFFFFA1D8)->bit01)
#define P2PWGA52           (((volatile __bitf_T *)0xFFFFA1D8)->bit02)
#define TBPWGA52           (((volatile __bitf_T *)0xFFFFA1D8)->bit06)
#define MKPWGA52           (((volatile __bitf_T *)0xFFFFA1D8)->bit07)
#define RFPWGA52           (((volatile __bitf_T *)0xFFFFA1D9)->bit04)
#define CTPWGA52           (((volatile __bitf_T *)0xFFFFA1D9)->bit07)
__IOREG(ICPWGA53, 0xFFFFA1DA, __READ_WRITE, uint16_t);
__IOREG(ICPWGA53L, 0xFFFFA1DA, __READ_WRITE, uint8_t);
__IOREG(ICPWGA53H, 0xFFFFA1DB, __READ_WRITE, uint8_t);
#define P0PWGA53           (((volatile __bitf_T *)0xFFFFA1DA)->bit00)
#define P1PWGA53           (((volatile __bitf_T *)0xFFFFA1DA)->bit01)
#define P2PWGA53           (((volatile __bitf_T *)0xFFFFA1DA)->bit02)
#define TBPWGA53           (((volatile __bitf_T *)0xFFFFA1DA)->bit06)
#define MKPWGA53           (((volatile __bitf_T *)0xFFFFA1DA)->bit07)
#define RFPWGA53           (((volatile __bitf_T *)0xFFFFA1DB)->bit04)
#define CTPWGA53           (((volatile __bitf_T *)0xFFFFA1DB)->bit07)
__IOREG(ICPWGA54, 0xFFFFA1DC, __READ_WRITE, uint16_t);
__IOREG(ICPWGA54L, 0xFFFFA1DC, __READ_WRITE, uint8_t);
__IOREG(ICPWGA54H, 0xFFFFA1DD, __READ_WRITE, uint8_t);
#define P0PWGA54           (((volatile __bitf_T *)0xFFFFA1DC)->bit00)
#define P1PWGA54           (((volatile __bitf_T *)0xFFFFA1DC)->bit01)
#define P2PWGA54           (((volatile __bitf_T *)0xFFFFA1DC)->bit02)
#define TBPWGA54           (((volatile __bitf_T *)0xFFFFA1DC)->bit06)
#define MKPWGA54           (((volatile __bitf_T *)0xFFFFA1DC)->bit07)
#define RFPWGA54           (((volatile __bitf_T *)0xFFFFA1DD)->bit04)
#define CTPWGA54           (((volatile __bitf_T *)0xFFFFA1DD)->bit07)
__IOREG(ICPWGA55, 0xFFFFA1DE, __READ_WRITE, uint16_t);
__IOREG(ICPWGA55L, 0xFFFFA1DE, __READ_WRITE, uint8_t);
__IOREG(ICPWGA55H, 0xFFFFA1DF, __READ_WRITE, uint8_t);
#define P0PWGA55           (((volatile __bitf_T *)0xFFFFA1DE)->bit00)
#define P1PWGA55           (((volatile __bitf_T *)0xFFFFA1DE)->bit01)
#define P2PWGA55           (((volatile __bitf_T *)0xFFFFA1DE)->bit02)
#define TBPWGA55           (((volatile __bitf_T *)0xFFFFA1DE)->bit06)
#define MKPWGA55           (((volatile __bitf_T *)0xFFFFA1DE)->bit07)
#define RFPWGA55           (((volatile __bitf_T *)0xFFFFA1DF)->bit04)
#define CTPWGA55           (((volatile __bitf_T *)0xFFFFA1DF)->bit07)
__IOREG(ICPWGA56, 0xFFFFA1E0, __READ_WRITE, uint16_t);
__IOREG(ICPWGA56L, 0xFFFFA1E0, __READ_WRITE, uint8_t);
__IOREG(ICPWGA56H, 0xFFFFA1E1, __READ_WRITE, uint8_t);
#define P0PWGA56           (((volatile __bitf_T *)0xFFFFA1E0)->bit00)
#define P1PWGA56           (((volatile __bitf_T *)0xFFFFA1E0)->bit01)
#define P2PWGA56           (((volatile __bitf_T *)0xFFFFA1E0)->bit02)
#define TBPWGA56           (((volatile __bitf_T *)0xFFFFA1E0)->bit06)
#define MKPWGA56           (((volatile __bitf_T *)0xFFFFA1E0)->bit07)
#define RFPWGA56           (((volatile __bitf_T *)0xFFFFA1E1)->bit04)
#define CTPWGA56           (((volatile __bitf_T *)0xFFFFA1E1)->bit07)
__IOREG(ICPWGA57, 0xFFFFA1E2, __READ_WRITE, uint16_t);
__IOREG(ICPWGA57L, 0xFFFFA1E2, __READ_WRITE, uint8_t);
__IOREG(ICPWGA57H, 0xFFFFA1E3, __READ_WRITE, uint8_t);
#define P0PWGA57           (((volatile __bitf_T *)0xFFFFA1E2)->bit00)
#define P1PWGA57           (((volatile __bitf_T *)0xFFFFA1E2)->bit01)
#define P2PWGA57           (((volatile __bitf_T *)0xFFFFA1E2)->bit02)
#define TBPWGA57           (((volatile __bitf_T *)0xFFFFA1E2)->bit06)
#define MKPWGA57           (((volatile __bitf_T *)0xFFFFA1E2)->bit07)
#define RFPWGA57           (((volatile __bitf_T *)0xFFFFA1E3)->bit04)
#define CTPWGA57           (((volatile __bitf_T *)0xFFFFA1E3)->bit07)
__IOREG(ICPWGA58, 0xFFFFA1E4, __READ_WRITE, uint16_t);
__IOREG(ICPWGA58L, 0xFFFFA1E4, __READ_WRITE, uint8_t);
__IOREG(ICPWGA58H, 0xFFFFA1E5, __READ_WRITE, uint8_t);
#define P0PWGA58           (((volatile __bitf_T *)0xFFFFA1E4)->bit00)
#define P1PWGA58           (((volatile __bitf_T *)0xFFFFA1E4)->bit01)
#define P2PWGA58           (((volatile __bitf_T *)0xFFFFA1E4)->bit02)
#define TBPWGA58           (((volatile __bitf_T *)0xFFFFA1E4)->bit06)
#define MKPWGA58           (((volatile __bitf_T *)0xFFFFA1E4)->bit07)
#define RFPWGA58           (((volatile __bitf_T *)0xFFFFA1E5)->bit04)
#define CTPWGA58           (((volatile __bitf_T *)0xFFFFA1E5)->bit07)
__IOREG(ICPWGA59, 0xFFFFA1E6, __READ_WRITE, uint16_t);
__IOREG(ICPWGA59L, 0xFFFFA1E6, __READ_WRITE, uint8_t);
__IOREG(ICPWGA59H, 0xFFFFA1E7, __READ_WRITE, uint8_t);
#define P0PWGA59           (((volatile __bitf_T *)0xFFFFA1E6)->bit00)
#define P1PWGA59           (((volatile __bitf_T *)0xFFFFA1E6)->bit01)
#define P2PWGA59           (((volatile __bitf_T *)0xFFFFA1E6)->bit02)
#define TBPWGA59           (((volatile __bitf_T *)0xFFFFA1E6)->bit06)
#define MKPWGA59           (((volatile __bitf_T *)0xFFFFA1E6)->bit07)
#define RFPWGA59           (((volatile __bitf_T *)0xFFFFA1E7)->bit04)
#define CTPWGA59           (((volatile __bitf_T *)0xFFFFA1E7)->bit07)
__IOREG(ICPWGA60, 0xFFFFA1E8, __READ_WRITE, uint16_t);
__IOREG(ICPWGA60L, 0xFFFFA1E8, __READ_WRITE, uint8_t);
__IOREG(ICPWGA60H, 0xFFFFA1E9, __READ_WRITE, uint8_t);
#define P0PWGA60           (((volatile __bitf_T *)0xFFFFA1E8)->bit00)
#define P1PWGA60           (((volatile __bitf_T *)0xFFFFA1E8)->bit01)
#define P2PWGA60           (((volatile __bitf_T *)0xFFFFA1E8)->bit02)
#define TBPWGA60           (((volatile __bitf_T *)0xFFFFA1E8)->bit06)
#define MKPWGA60           (((volatile __bitf_T *)0xFFFFA1E8)->bit07)
#define RFPWGA60           (((volatile __bitf_T *)0xFFFFA1E9)->bit04)
#define CTPWGA60           (((volatile __bitf_T *)0xFFFFA1E9)->bit07)
__IOREG(ICPWGA61, 0xFFFFA1EA, __READ_WRITE, uint16_t);
__IOREG(ICPWGA61L, 0xFFFFA1EA, __READ_WRITE, uint8_t);
__IOREG(ICPWGA61H, 0xFFFFA1EB, __READ_WRITE, uint8_t);
#define P0PWGA61           (((volatile __bitf_T *)0xFFFFA1EA)->bit00)
#define P1PWGA61           (((volatile __bitf_T *)0xFFFFA1EA)->bit01)
#define P2PWGA61           (((volatile __bitf_T *)0xFFFFA1EA)->bit02)
#define TBPWGA61           (((volatile __bitf_T *)0xFFFFA1EA)->bit06)
#define MKPWGA61           (((volatile __bitf_T *)0xFFFFA1EA)->bit07)
#define RFPWGA61           (((volatile __bitf_T *)0xFFFFA1EB)->bit04)
#define CTPWGA61           (((volatile __bitf_T *)0xFFFFA1EB)->bit07)
__IOREG(ICPWGA62, 0xFFFFA1EC, __READ_WRITE, uint16_t);
__IOREG(ICPWGA62L, 0xFFFFA1EC, __READ_WRITE, uint8_t);
__IOREG(ICPWGA62H, 0xFFFFA1ED, __READ_WRITE, uint8_t);
#define P0PWGA62           (((volatile __bitf_T *)0xFFFFA1EC)->bit00)
#define P1PWGA62           (((volatile __bitf_T *)0xFFFFA1EC)->bit01)
#define P2PWGA62           (((volatile __bitf_T *)0xFFFFA1EC)->bit02)
#define TBPWGA62           (((volatile __bitf_T *)0xFFFFA1EC)->bit06)
#define MKPWGA62           (((volatile __bitf_T *)0xFFFFA1EC)->bit07)
#define RFPWGA62           (((volatile __bitf_T *)0xFFFFA1ED)->bit04)
#define CTPWGA62           (((volatile __bitf_T *)0xFFFFA1ED)->bit07)
__IOREG(ICPWGA63, 0xFFFFA1EE, __READ_WRITE, uint16_t);
__IOREG(ICPWGA63L, 0xFFFFA1EE, __READ_WRITE, uint8_t);
__IOREG(ICPWGA63H, 0xFFFFA1EF, __READ_WRITE, uint8_t);
#define P0PWGA63           (((volatile __bitf_T *)0xFFFFA1EE)->bit00)
#define P1PWGA63           (((volatile __bitf_T *)0xFFFFA1EE)->bit01)
#define P2PWGA63           (((volatile __bitf_T *)0xFFFFA1EE)->bit02)
#define TBPWGA63           (((volatile __bitf_T *)0xFFFFA1EE)->bit06)
#define MKPWGA63           (((volatile __bitf_T *)0xFFFFA1EE)->bit07)
#define RFPWGA63           (((volatile __bitf_T *)0xFFFFA1EF)->bit04)
#define CTPWGA63           (((volatile __bitf_T *)0xFFFFA1EF)->bit07)
__IOREG(ICTAUB1I0, 0xFFFFA1F0, __READ_WRITE, uint16_t);
__IOREG(ICTAUB1I0L, 0xFFFFA1F0, __READ_WRITE, uint8_t);
__IOREG(ICTAUB1I0H, 0xFFFFA1F1, __READ_WRITE, uint8_t);
#define P0TAUB1I0          (((volatile __bitf_T *)0xFFFFA1F0)->bit00)
#define P1TAUB1I0          (((volatile __bitf_T *)0xFFFFA1F0)->bit01)
#define P2TAUB1I0          (((volatile __bitf_T *)0xFFFFA1F0)->bit02)
#define TBTAUB1I0          (((volatile __bitf_T *)0xFFFFA1F0)->bit06)
#define MKTAUB1I0          (((volatile __bitf_T *)0xFFFFA1F0)->bit07)
#define RFTAUB1I0          (((volatile __bitf_T *)0xFFFFA1F1)->bit04)
#define CTTAUB1I0          (((volatile __bitf_T *)0xFFFFA1F1)->bit07)
__IOREG(ICTAUB1I1, 0xFFFFA1F2, __READ_WRITE, uint16_t);
__IOREG(ICTAUB1I1L, 0xFFFFA1F2, __READ_WRITE, uint8_t);
__IOREG(ICTAUB1I1H, 0xFFFFA1F3, __READ_WRITE, uint8_t);
#define P0TAUB1I1          (((volatile __bitf_T *)0xFFFFA1F2)->bit00)
#define P1TAUB1I1          (((volatile __bitf_T *)0xFFFFA1F2)->bit01)
#define P2TAUB1I1          (((volatile __bitf_T *)0xFFFFA1F2)->bit02)
#define TBTAUB1I1          (((volatile __bitf_T *)0xFFFFA1F2)->bit06)
#define MKTAUB1I1          (((volatile __bitf_T *)0xFFFFA1F2)->bit07)
#define RFTAUB1I1          (((volatile __bitf_T *)0xFFFFA1F3)->bit04)
#define CTTAUB1I1          (((volatile __bitf_T *)0xFFFFA1F3)->bit07)
__IOREG(ICTAUB1I2, 0xFFFFA1F4, __READ_WRITE, uint16_t);
__IOREG(ICTAUB1I2L, 0xFFFFA1F4, __READ_WRITE, uint8_t);
__IOREG(ICTAUB1I2H, 0xFFFFA1F5, __READ_WRITE, uint8_t);
#define P0TAUB1I2          (((volatile __bitf_T *)0xFFFFA1F4)->bit00)
#define P1TAUB1I2          (((volatile __bitf_T *)0xFFFFA1F4)->bit01)
#define P2TAUB1I2          (((volatile __bitf_T *)0xFFFFA1F4)->bit02)
#define TBTAUB1I2          (((volatile __bitf_T *)0xFFFFA1F4)->bit06)
#define MKTAUB1I2          (((volatile __bitf_T *)0xFFFFA1F4)->bit07)
#define RFTAUB1I2          (((volatile __bitf_T *)0xFFFFA1F5)->bit04)
#define CTTAUB1I2          (((volatile __bitf_T *)0xFFFFA1F5)->bit07)
__IOREG(ICTAUB1I3, 0xFFFFA1F6, __READ_WRITE, uint16_t);
__IOREG(ICTAUB1I3L, 0xFFFFA1F6, __READ_WRITE, uint8_t);
__IOREG(ICTAUB1I3H, 0xFFFFA1F7, __READ_WRITE, uint8_t);
#define P0TAUB1I3          (((volatile __bitf_T *)0xFFFFA1F6)->bit00)
#define P1TAUB1I3          (((volatile __bitf_T *)0xFFFFA1F6)->bit01)
#define P2TAUB1I3          (((volatile __bitf_T *)0xFFFFA1F6)->bit02)
#define TBTAUB1I3          (((volatile __bitf_T *)0xFFFFA1F6)->bit06)
#define MKTAUB1I3          (((volatile __bitf_T *)0xFFFFA1F6)->bit07)
#define RFTAUB1I3          (((volatile __bitf_T *)0xFFFFA1F7)->bit04)
#define CTTAUB1I3          (((volatile __bitf_T *)0xFFFFA1F7)->bit07)
__IOREG(ICTAUB1I4, 0xFFFFA1F8, __READ_WRITE, uint16_t);
__IOREG(ICTAUB1I4L, 0xFFFFA1F8, __READ_WRITE, uint8_t);
__IOREG(ICTAUB1I4H, 0xFFFFA1F9, __READ_WRITE, uint8_t);
#define P0TAUB1I4          (((volatile __bitf_T *)0xFFFFA1F8)->bit00)
#define P1TAUB1I4          (((volatile __bitf_T *)0xFFFFA1F8)->bit01)
#define P2TAUB1I4          (((volatile __bitf_T *)0xFFFFA1F8)->bit02)
#define TBTAUB1I4          (((volatile __bitf_T *)0xFFFFA1F8)->bit06)
#define MKTAUB1I4          (((volatile __bitf_T *)0xFFFFA1F8)->bit07)
#define RFTAUB1I4          (((volatile __bitf_T *)0xFFFFA1F9)->bit04)
#define CTTAUB1I4          (((volatile __bitf_T *)0xFFFFA1F9)->bit07)
__IOREG(ICTAUB1I5, 0xFFFFA1FA, __READ_WRITE, uint16_t);
__IOREG(ICTAUB1I5L, 0xFFFFA1FA, __READ_WRITE, uint8_t);
__IOREG(ICTAUB1I5H, 0xFFFFA1FB, __READ_WRITE, uint8_t);
#define P0TAUB1I5          (((volatile __bitf_T *)0xFFFFA1FA)->bit00)
#define P1TAUB1I5          (((volatile __bitf_T *)0xFFFFA1FA)->bit01)
#define P2TAUB1I5          (((volatile __bitf_T *)0xFFFFA1FA)->bit02)
#define TBTAUB1I5          (((volatile __bitf_T *)0xFFFFA1FA)->bit06)
#define MKTAUB1I5          (((volatile __bitf_T *)0xFFFFA1FA)->bit07)
#define RFTAUB1I5          (((volatile __bitf_T *)0xFFFFA1FB)->bit04)
#define CTTAUB1I5          (((volatile __bitf_T *)0xFFFFA1FB)->bit07)
__IOREG(ICTAUB1I6, 0xFFFFA1FC, __READ_WRITE, uint16_t);
__IOREG(ICTAUB1I6L, 0xFFFFA1FC, __READ_WRITE, uint8_t);
__IOREG(ICTAUB1I6H, 0xFFFFA1FD, __READ_WRITE, uint8_t);
#define P0TAUB1I6          (((volatile __bitf_T *)0xFFFFA1FC)->bit00)
#define P1TAUB1I6          (((volatile __bitf_T *)0xFFFFA1FC)->bit01)
#define P2TAUB1I6          (((volatile __bitf_T *)0xFFFFA1FC)->bit02)
#define TBTAUB1I6          (((volatile __bitf_T *)0xFFFFA1FC)->bit06)
#define MKTAUB1I6          (((volatile __bitf_T *)0xFFFFA1FC)->bit07)
#define RFTAUB1I6          (((volatile __bitf_T *)0xFFFFA1FD)->bit04)
#define CTTAUB1I6          (((volatile __bitf_T *)0xFFFFA1FD)->bit07)
__IOREG(ICTAUB1I7, 0xFFFFA1FE, __READ_WRITE, uint16_t);
__IOREG(ICTAUB1I7L, 0xFFFFA1FE, __READ_WRITE, uint8_t);
__IOREG(ICTAUB1I7H, 0xFFFFA1FF, __READ_WRITE, uint8_t);
#define P0TAUB1I7          (((volatile __bitf_T *)0xFFFFA1FE)->bit00)
#define P1TAUB1I7          (((volatile __bitf_T *)0xFFFFA1FE)->bit01)
#define P2TAUB1I7          (((volatile __bitf_T *)0xFFFFA1FE)->bit02)
#define TBTAUB1I7          (((volatile __bitf_T *)0xFFFFA1FE)->bit06)
#define MKTAUB1I7          (((volatile __bitf_T *)0xFFFFA1FE)->bit07)
#define RFTAUB1I7          (((volatile __bitf_T *)0xFFFFA1FF)->bit04)
#define CTTAUB1I7          (((volatile __bitf_T *)0xFFFFA1FF)->bit07)
__IOREG(ICTAUB1I8, 0xFFFFA200, __READ_WRITE, uint16_t);
__IOREG(ICTAUB1I8L, 0xFFFFA200, __READ_WRITE, uint8_t);
__IOREG(ICTAUB1I8H, 0xFFFFA201, __READ_WRITE, uint8_t);
#define P0TAUB1I8          (((volatile __bitf_T *)0xFFFFA200)->bit00)
#define P1TAUB1I8          (((volatile __bitf_T *)0xFFFFA200)->bit01)
#define P2TAUB1I8          (((volatile __bitf_T *)0xFFFFA200)->bit02)
#define TBTAUB1I8          (((volatile __bitf_T *)0xFFFFA200)->bit06)
#define MKTAUB1I8          (((volatile __bitf_T *)0xFFFFA200)->bit07)
#define RFTAUB1I8          (((volatile __bitf_T *)0xFFFFA201)->bit04)
#define CTTAUB1I8          (((volatile __bitf_T *)0xFFFFA201)->bit07)
__IOREG(ICTAUB1I9, 0xFFFFA202, __READ_WRITE, uint16_t);
__IOREG(ICTAUB1I9L, 0xFFFFA202, __READ_WRITE, uint8_t);
__IOREG(ICTAUB1I9H, 0xFFFFA203, __READ_WRITE, uint8_t);
#define P0TAUB1I9          (((volatile __bitf_T *)0xFFFFA202)->bit00)
#define P1TAUB1I9          (((volatile __bitf_T *)0xFFFFA202)->bit01)
#define P2TAUB1I9          (((volatile __bitf_T *)0xFFFFA202)->bit02)
#define TBTAUB1I9          (((volatile __bitf_T *)0xFFFFA202)->bit06)
#define MKTAUB1I9          (((volatile __bitf_T *)0xFFFFA202)->bit07)
#define RFTAUB1I9          (((volatile __bitf_T *)0xFFFFA203)->bit04)
#define CTTAUB1I9          (((volatile __bitf_T *)0xFFFFA203)->bit07)
__IOREG(ICTAUB1I10, 0xFFFFA204, __READ_WRITE, uint16_t);
__IOREG(ICTAUB1I10L, 0xFFFFA204, __READ_WRITE, uint8_t);
__IOREG(ICTAUB1I10H, 0xFFFFA205, __READ_WRITE, uint8_t);
#define P0TAUB1I10         (((volatile __bitf_T *)0xFFFFA204)->bit00)
#define P1TAUB1I10         (((volatile __bitf_T *)0xFFFFA204)->bit01)
#define P2TAUB1I10         (((volatile __bitf_T *)0xFFFFA204)->bit02)
#define TBTAUB1I10         (((volatile __bitf_T *)0xFFFFA204)->bit06)
#define MKTAUB1I10         (((volatile __bitf_T *)0xFFFFA204)->bit07)
#define RFTAUB1I10         (((volatile __bitf_T *)0xFFFFA205)->bit04)
#define CTTAUB1I10         (((volatile __bitf_T *)0xFFFFA205)->bit07)
__IOREG(ICTAUB1I11, 0xFFFFA206, __READ_WRITE, uint16_t);
__IOREG(ICTAUB1I11L, 0xFFFFA206, __READ_WRITE, uint8_t);
__IOREG(ICTAUB1I11H, 0xFFFFA207, __READ_WRITE, uint8_t);
#define P0TAUB1I11         (((volatile __bitf_T *)0xFFFFA206)->bit00)
#define P1TAUB1I11         (((volatile __bitf_T *)0xFFFFA206)->bit01)
#define P2TAUB1I11         (((volatile __bitf_T *)0xFFFFA206)->bit02)
#define TBTAUB1I11         (((volatile __bitf_T *)0xFFFFA206)->bit06)
#define MKTAUB1I11         (((volatile __bitf_T *)0xFFFFA206)->bit07)
#define RFTAUB1I11         (((volatile __bitf_T *)0xFFFFA207)->bit04)
#define CTTAUB1I11         (((volatile __bitf_T *)0xFFFFA207)->bit07)
__IOREG(ICTAUB1I12, 0xFFFFA208, __READ_WRITE, uint16_t);
__IOREG(ICTAUB1I12L, 0xFFFFA208, __READ_WRITE, uint8_t);
__IOREG(ICTAUB1I12H, 0xFFFFA209, __READ_WRITE, uint8_t);
#define P0TAUB1I12         (((volatile __bitf_T *)0xFFFFA208)->bit00)
#define P1TAUB1I12         (((volatile __bitf_T *)0xFFFFA208)->bit01)
#define P2TAUB1I12         (((volatile __bitf_T *)0xFFFFA208)->bit02)
#define TBTAUB1I12         (((volatile __bitf_T *)0xFFFFA208)->bit06)
#define MKTAUB1I12         (((volatile __bitf_T *)0xFFFFA208)->bit07)
#define RFTAUB1I12         (((volatile __bitf_T *)0xFFFFA209)->bit04)
#define CTTAUB1I12         (((volatile __bitf_T *)0xFFFFA209)->bit07)
__IOREG(ICTAUB1I13, 0xFFFFA20A, __READ_WRITE, uint16_t);
__IOREG(ICTAUB1I13L, 0xFFFFA20A, __READ_WRITE, uint8_t);
__IOREG(ICTAUB1I13H, 0xFFFFA20B, __READ_WRITE, uint8_t);
#define P0TAUB1I13         (((volatile __bitf_T *)0xFFFFA20A)->bit00)
#define P1TAUB1I13         (((volatile __bitf_T *)0xFFFFA20A)->bit01)
#define P2TAUB1I13         (((volatile __bitf_T *)0xFFFFA20A)->bit02)
#define TBTAUB1I13         (((volatile __bitf_T *)0xFFFFA20A)->bit06)
#define MKTAUB1I13         (((volatile __bitf_T *)0xFFFFA20A)->bit07)
#define RFTAUB1I13         (((volatile __bitf_T *)0xFFFFA20B)->bit04)
#define CTTAUB1I13         (((volatile __bitf_T *)0xFFFFA20B)->bit07)
__IOREG(ICTAUB1I14, 0xFFFFA20C, __READ_WRITE, uint16_t);
__IOREG(ICTAUB1I14L, 0xFFFFA20C, __READ_WRITE, uint8_t);
__IOREG(ICTAUB1I14H, 0xFFFFA20D, __READ_WRITE, uint8_t);
#define P0TAUB1I14         (((volatile __bitf_T *)0xFFFFA20C)->bit00)
#define P1TAUB1I14         (((volatile __bitf_T *)0xFFFFA20C)->bit01)
#define P2TAUB1I14         (((volatile __bitf_T *)0xFFFFA20C)->bit02)
#define TBTAUB1I14         (((volatile __bitf_T *)0xFFFFA20C)->bit06)
#define MKTAUB1I14         (((volatile __bitf_T *)0xFFFFA20C)->bit07)
#define RFTAUB1I14         (((volatile __bitf_T *)0xFFFFA20D)->bit04)
#define CTTAUB1I14         (((volatile __bitf_T *)0xFFFFA20D)->bit07)
__IOREG(ICTAUB1I15, 0xFFFFA20E, __READ_WRITE, uint16_t);
__IOREG(ICTAUB1I15L, 0xFFFFA20E, __READ_WRITE, uint8_t);
__IOREG(ICTAUB1I15H, 0xFFFFA20F, __READ_WRITE, uint8_t);
#define P0TAUB1I15         (((volatile __bitf_T *)0xFFFFA20E)->bit00)
#define P1TAUB1I15         (((volatile __bitf_T *)0xFFFFA20E)->bit01)
#define P2TAUB1I15         (((volatile __bitf_T *)0xFFFFA20E)->bit02)
#define TBTAUB1I15         (((volatile __bitf_T *)0xFFFFA20E)->bit06)
#define MKTAUB1I15         (((volatile __bitf_T *)0xFFFFA20E)->bit07)
#define RFTAUB1I15         (((volatile __bitf_T *)0xFFFFA20F)->bit04)
#define CTTAUB1I15         (((volatile __bitf_T *)0xFFFFA20F)->bit07)
__IOREG(ICRCAN4ERR, 0xFFFFA210, __READ_WRITE, uint16_t);
__IOREG(ICRCAN4ERRL, 0xFFFFA210, __READ_WRITE, uint8_t);
__IOREG(ICRCAN4ERRH, 0xFFFFA211, __READ_WRITE, uint8_t);
#define P0RCAN4ERR         (((volatile __bitf_T *)0xFFFFA210)->bit00)
#define P1RCAN4ERR         (((volatile __bitf_T *)0xFFFFA210)->bit01)
#define P2RCAN4ERR         (((volatile __bitf_T *)0xFFFFA210)->bit02)
#define TBRCAN4ERR         (((volatile __bitf_T *)0xFFFFA210)->bit06)
#define MKRCAN4ERR         (((volatile __bitf_T *)0xFFFFA210)->bit07)
#define RFRCAN4ERR         (((volatile __bitf_T *)0xFFFFA211)->bit04)
#define CTRCAN4ERR         (((volatile __bitf_T *)0xFFFFA211)->bit07)
__IOREG(ICRCAN4REC, 0xFFFFA212, __READ_WRITE, uint16_t);
__IOREG(ICRCAN4RECL, 0xFFFFA212, __READ_WRITE, uint8_t);
__IOREG(ICRCAN4RECH, 0xFFFFA213, __READ_WRITE, uint8_t);
#define P0RCAN4REC         (((volatile __bitf_T *)0xFFFFA212)->bit00)
#define P1RCAN4REC         (((volatile __bitf_T *)0xFFFFA212)->bit01)
#define P2RCAN4REC         (((volatile __bitf_T *)0xFFFFA212)->bit02)
#define TBRCAN4REC         (((volatile __bitf_T *)0xFFFFA212)->bit06)
#define MKRCAN4REC         (((volatile __bitf_T *)0xFFFFA212)->bit07)
#define RFRCAN4REC         (((volatile __bitf_T *)0xFFFFA213)->bit04)
#define CTRCAN4REC         (((volatile __bitf_T *)0xFFFFA213)->bit07)
__IOREG(ICRCAN4TRX, 0xFFFFA214, __READ_WRITE, uint16_t);
__IOREG(ICRCAN4TRXL, 0xFFFFA214, __READ_WRITE, uint8_t);
__IOREG(ICRCAN4TRXH, 0xFFFFA215, __READ_WRITE, uint8_t);
#define P0RCAN4TRX         (((volatile __bitf_T *)0xFFFFA214)->bit00)
#define P1RCAN4TRX         (((volatile __bitf_T *)0xFFFFA214)->bit01)
#define P2RCAN4TRX         (((volatile __bitf_T *)0xFFFFA214)->bit02)
#define TBRCAN4TRX         (((volatile __bitf_T *)0xFFFFA214)->bit06)
#define MKRCAN4TRX         (((volatile __bitf_T *)0xFFFFA214)->bit07)
#define RFRCAN4TRX         (((volatile __bitf_T *)0xFFFFA215)->bit04)
#define CTRCAN4TRX         (((volatile __bitf_T *)0xFFFFA215)->bit07)
__IOREG(ICRLIN26, 0xFFFFA216, __READ_WRITE, uint16_t);
__IOREG(ICRLIN26L, 0xFFFFA216, __READ_WRITE, uint8_t);
__IOREG(ICRLIN26H, 0xFFFFA217, __READ_WRITE, uint8_t);
#define P0RLIN26           (((volatile __bitf_T *)0xFFFFA216)->bit00)
#define P1RLIN26           (((volatile __bitf_T *)0xFFFFA216)->bit01)
#define P2RLIN26           (((volatile __bitf_T *)0xFFFFA216)->bit02)
#define TBRLIN26           (((volatile __bitf_T *)0xFFFFA216)->bit06)
#define MKRLIN26           (((volatile __bitf_T *)0xFFFFA216)->bit07)
#define RFRLIN26           (((volatile __bitf_T *)0xFFFFA217)->bit04)
#define CTRLIN26           (((volatile __bitf_T *)0xFFFFA217)->bit07)
__IOREG(ICRLIN27, 0xFFFFA218, __READ_WRITE, uint16_t);
__IOREG(ICRLIN27L, 0xFFFFA218, __READ_WRITE, uint8_t);
__IOREG(ICRLIN27H, 0xFFFFA219, __READ_WRITE, uint8_t);
#define P0RLIN27           (((volatile __bitf_T *)0xFFFFA218)->bit00)
#define P1RLIN27           (((volatile __bitf_T *)0xFFFFA218)->bit01)
#define P2RLIN27           (((volatile __bitf_T *)0xFFFFA218)->bit02)
#define TBRLIN27           (((volatile __bitf_T *)0xFFFFA218)->bit06)
#define MKRLIN27           (((volatile __bitf_T *)0xFFFFA218)->bit07)
#define RFRLIN27           (((volatile __bitf_T *)0xFFFFA219)->bit04)
#define CTRLIN27           (((volatile __bitf_T *)0xFFFFA219)->bit07)
__IOREG(ICPWGA64, 0xFFFFA21A, __READ_WRITE, uint16_t);
__IOREG(ICPWGA64L, 0xFFFFA21A, __READ_WRITE, uint8_t);
__IOREG(ICPWGA64H, 0xFFFFA21B, __READ_WRITE, uint8_t);
#define P0PWGA64           (((volatile __bitf_T *)0xFFFFA21A)->bit00)
#define P1PWGA64           (((volatile __bitf_T *)0xFFFFA21A)->bit01)
#define P2PWGA64           (((volatile __bitf_T *)0xFFFFA21A)->bit02)
#define TBPWGA64           (((volatile __bitf_T *)0xFFFFA21A)->bit06)
#define MKPWGA64           (((volatile __bitf_T *)0xFFFFA21A)->bit07)
#define RFPWGA64           (((volatile __bitf_T *)0xFFFFA21B)->bit04)
#define CTPWGA64           (((volatile __bitf_T *)0xFFFFA21B)->bit07)
__IOREG(ICPWGA65, 0xFFFFA21C, __READ_WRITE, uint16_t);
__IOREG(ICPWGA65L, 0xFFFFA21C, __READ_WRITE, uint8_t);
__IOREG(ICPWGA65H, 0xFFFFA21D, __READ_WRITE, uint8_t);
#define P0PWGA65           (((volatile __bitf_T *)0xFFFFA21C)->bit00)
#define P1PWGA65           (((volatile __bitf_T *)0xFFFFA21C)->bit01)
#define P2PWGA65           (((volatile __bitf_T *)0xFFFFA21C)->bit02)
#define TBPWGA65           (((volatile __bitf_T *)0xFFFFA21C)->bit06)
#define MKPWGA65           (((volatile __bitf_T *)0xFFFFA21C)->bit07)
#define RFPWGA65           (((volatile __bitf_T *)0xFFFFA21D)->bit04)
#define CTPWGA65           (((volatile __bitf_T *)0xFFFFA21D)->bit07)
__IOREG(ICPWGA66, 0xFFFFA21E, __READ_WRITE, uint16_t);
__IOREG(ICPWGA66L, 0xFFFFA21E, __READ_WRITE, uint8_t);
__IOREG(ICPWGA66H, 0xFFFFA21F, __READ_WRITE, uint8_t);
#define P0PWGA66           (((volatile __bitf_T *)0xFFFFA21E)->bit00)
#define P1PWGA66           (((volatile __bitf_T *)0xFFFFA21E)->bit01)
#define P2PWGA66           (((volatile __bitf_T *)0xFFFFA21E)->bit02)
#define TBPWGA66           (((volatile __bitf_T *)0xFFFFA21E)->bit06)
#define MKPWGA66           (((volatile __bitf_T *)0xFFFFA21E)->bit07)
#define RFPWGA66           (((volatile __bitf_T *)0xFFFFA21F)->bit04)
#define CTPWGA66           (((volatile __bitf_T *)0xFFFFA21F)->bit07)
__IOREG(ICPWGA67, 0xFFFFA220, __READ_WRITE, uint16_t);
__IOREG(ICPWGA67L, 0xFFFFA220, __READ_WRITE, uint8_t);
__IOREG(ICPWGA67H, 0xFFFFA221, __READ_WRITE, uint8_t);
#define P0PWGA67           (((volatile __bitf_T *)0xFFFFA220)->bit00)
#define P1PWGA67           (((volatile __bitf_T *)0xFFFFA220)->bit01)
#define P2PWGA67           (((volatile __bitf_T *)0xFFFFA220)->bit02)
#define TBPWGA67           (((volatile __bitf_T *)0xFFFFA220)->bit06)
#define MKPWGA67           (((volatile __bitf_T *)0xFFFFA220)->bit07)
#define RFPWGA67           (((volatile __bitf_T *)0xFFFFA221)->bit04)
#define CTPWGA67           (((volatile __bitf_T *)0xFFFFA221)->bit07)
__IOREG(ICPWGA68, 0xFFFFA222, __READ_WRITE, uint16_t);
__IOREG(ICPWGA68L, 0xFFFFA222, __READ_WRITE, uint8_t);
__IOREG(ICPWGA68H, 0xFFFFA223, __READ_WRITE, uint8_t);
#define P0PWGA68           (((volatile __bitf_T *)0xFFFFA222)->bit00)
#define P1PWGA68           (((volatile __bitf_T *)0xFFFFA222)->bit01)
#define P2PWGA68           (((volatile __bitf_T *)0xFFFFA222)->bit02)
#define TBPWGA68           (((volatile __bitf_T *)0xFFFFA222)->bit06)
#define MKPWGA68           (((volatile __bitf_T *)0xFFFFA222)->bit07)
#define RFPWGA68           (((volatile __bitf_T *)0xFFFFA223)->bit04)
#define CTPWGA68           (((volatile __bitf_T *)0xFFFFA223)->bit07)
__IOREG(ICPWGA69, 0xFFFFA224, __READ_WRITE, uint16_t);
__IOREG(ICPWGA69L, 0xFFFFA224, __READ_WRITE, uint8_t);
__IOREG(ICPWGA69H, 0xFFFFA225, __READ_WRITE, uint8_t);
#define P0PWGA69           (((volatile __bitf_T *)0xFFFFA224)->bit00)
#define P1PWGA69           (((volatile __bitf_T *)0xFFFFA224)->bit01)
#define P2PWGA69           (((volatile __bitf_T *)0xFFFFA224)->bit02)
#define TBPWGA69           (((volatile __bitf_T *)0xFFFFA224)->bit06)
#define MKPWGA69           (((volatile __bitf_T *)0xFFFFA224)->bit07)
#define RFPWGA69           (((volatile __bitf_T *)0xFFFFA225)->bit04)
#define CTPWGA69           (((volatile __bitf_T *)0xFFFFA225)->bit07)
__IOREG(ICPWGA70, 0xFFFFA226, __READ_WRITE, uint16_t);
__IOREG(ICPWGA70L, 0xFFFFA226, __READ_WRITE, uint8_t);
__IOREG(ICPWGA70H, 0xFFFFA227, __READ_WRITE, uint8_t);
#define P0PWGA70           (((volatile __bitf_T *)0xFFFFA226)->bit00)
#define P1PWGA70           (((volatile __bitf_T *)0xFFFFA226)->bit01)
#define P2PWGA70           (((volatile __bitf_T *)0xFFFFA226)->bit02)
#define TBPWGA70           (((volatile __bitf_T *)0xFFFFA226)->bit06)
#define MKPWGA70           (((volatile __bitf_T *)0xFFFFA226)->bit07)
#define RFPWGA70           (((volatile __bitf_T *)0xFFFFA227)->bit04)
#define CTPWGA70           (((volatile __bitf_T *)0xFFFFA227)->bit07)
__IOREG(ICPWGA71, 0xFFFFA228, __READ_WRITE, uint16_t);
__IOREG(ICPWGA71L, 0xFFFFA228, __READ_WRITE, uint8_t);
__IOREG(ICPWGA71H, 0xFFFFA229, __READ_WRITE, uint8_t);
#define P0PWGA71           (((volatile __bitf_T *)0xFFFFA228)->bit00)
#define P1PWGA71           (((volatile __bitf_T *)0xFFFFA228)->bit01)
#define P2PWGA71           (((volatile __bitf_T *)0xFFFFA228)->bit02)
#define TBPWGA71           (((volatile __bitf_T *)0xFFFFA228)->bit06)
#define MKPWGA71           (((volatile __bitf_T *)0xFFFFA228)->bit07)
#define RFPWGA71           (((volatile __bitf_T *)0xFFFFA229)->bit04)
#define CTPWGA71           (((volatile __bitf_T *)0xFFFFA229)->bit07)
__IOREG(ICRLIN28, 0xFFFFA22A, __READ_WRITE, uint16_t);
__IOREG(ICRLIN28L, 0xFFFFA22A, __READ_WRITE, uint8_t);
__IOREG(ICRLIN28H, 0xFFFFA22B, __READ_WRITE, uint8_t);
#define P0RLIN28           (((volatile __bitf_T *)0xFFFFA22A)->bit00)
#define P1RLIN28           (((volatile __bitf_T *)0xFFFFA22A)->bit01)
#define P2RLIN28           (((volatile __bitf_T *)0xFFFFA22A)->bit02)
#define TBRLIN28           (((volatile __bitf_T *)0xFFFFA22A)->bit06)
#define MKRLIN28           (((volatile __bitf_T *)0xFFFFA22A)->bit07)
#define RFRLIN28           (((volatile __bitf_T *)0xFFFFA22B)->bit04)
#define CTRLIN28           (((volatile __bitf_T *)0xFFFFA22B)->bit07)
__IOREG(ICRLIN29, 0xFFFFA22C, __READ_WRITE, uint16_t);
__IOREG(ICRLIN29L, 0xFFFFA22C, __READ_WRITE, uint8_t);
__IOREG(ICRLIN29H, 0xFFFFA22D, __READ_WRITE, uint8_t);
#define P0RLIN29           (((volatile __bitf_T *)0xFFFFA22C)->bit00)
#define P1RLIN29           (((volatile __bitf_T *)0xFFFFA22C)->bit01)
#define P2RLIN29           (((volatile __bitf_T *)0xFFFFA22C)->bit02)
#define TBRLIN29           (((volatile __bitf_T *)0xFFFFA22C)->bit06)
#define MKRLIN29           (((volatile __bitf_T *)0xFFFFA22C)->bit07)
#define RFRLIN29           (((volatile __bitf_T *)0xFFFFA22D)->bit04)
#define CTRLIN29           (((volatile __bitf_T *)0xFFFFA22D)->bit07)
__IOREG(ICRCAN5ERR, 0xFFFFA22E, __READ_WRITE, uint16_t);
__IOREG(ICRCAN5ERRL, 0xFFFFA22E, __READ_WRITE, uint8_t);
__IOREG(ICRCAN5ERRH, 0xFFFFA22F, __READ_WRITE, uint8_t);
#define P0RCAN5ERR         (((volatile __bitf_T *)0xFFFFA22E)->bit00)
#define P1RCAN5ERR         (((volatile __bitf_T *)0xFFFFA22E)->bit01)
#define P2RCAN5ERR         (((volatile __bitf_T *)0xFFFFA22E)->bit02)
#define TBRCAN5ERR         (((volatile __bitf_T *)0xFFFFA22E)->bit06)
#define MKRCAN5ERR         (((volatile __bitf_T *)0xFFFFA22E)->bit07)
#define RFRCAN5ERR         (((volatile __bitf_T *)0xFFFFA22F)->bit04)
#define CTRCAN5ERR         (((volatile __bitf_T *)0xFFFFA22F)->bit07)
__IOREG(ICRCAN5REC, 0xFFFFA230, __READ_WRITE, uint16_t);
__IOREG(ICRCAN5RECL, 0xFFFFA230, __READ_WRITE, uint8_t);
__IOREG(ICRCAN5RECH, 0xFFFFA231, __READ_WRITE, uint8_t);
#define P0RCAN5REC         (((volatile __bitf_T *)0xFFFFA230)->bit00)
#define P1RCAN5REC         (((volatile __bitf_T *)0xFFFFA230)->bit01)
#define P2RCAN5REC         (((volatile __bitf_T *)0xFFFFA230)->bit02)
#define TBRCAN5REC         (((volatile __bitf_T *)0xFFFFA230)->bit06)
#define MKRCAN5REC         (((volatile __bitf_T *)0xFFFFA230)->bit07)
#define RFRCAN5REC         (((volatile __bitf_T *)0xFFFFA231)->bit04)
#define CTRCAN5REC         (((volatile __bitf_T *)0xFFFFA231)->bit07)
__IOREG(ICRCAN5TRX, 0xFFFFA232, __READ_WRITE, uint16_t);
__IOREG(ICRCAN5TRXL, 0xFFFFA232, __READ_WRITE, uint8_t);
__IOREG(ICRCAN5TRXH, 0xFFFFA233, __READ_WRITE, uint8_t);
#define P0RCAN5TRX         (((volatile __bitf_T *)0xFFFFA232)->bit00)
#define P1RCAN5TRX         (((volatile __bitf_T *)0xFFFFA232)->bit01)
#define P2RCAN5TRX         (((volatile __bitf_T *)0xFFFFA232)->bit02)
#define TBRCAN5TRX         (((volatile __bitf_T *)0xFFFFA232)->bit06)
#define MKRCAN5TRX         (((volatile __bitf_T *)0xFFFFA232)->bit07)
#define RFRCAN5TRX         (((volatile __bitf_T *)0xFFFFA233)->bit04)
#define CTRCAN5TRX         (((volatile __bitf_T *)0xFFFFA233)->bit07)
__IOREG(IMR1, 0xFFFFA404, __READ_WRITE, uint32_t);
__IOREG(IMR1L, 0xFFFFA404, __READ_WRITE, uint16_t);
__IOREG(IMR1LL, 0xFFFFA404, __READ_WRITE, uint8_t);
__IOREG(IMR1LH, 0xFFFFA405, __READ_WRITE, uint8_t);
__IOREG(IMR1H, 0xFFFFA406, __READ_WRITE, uint16_t);
__IOREG(IMR1HL, 0xFFFFA406, __READ_WRITE, uint8_t);
__IOREG(IMR1HH, 0xFFFFA407, __READ_WRITE, uint8_t);
#define IMR1EIMK32         (((volatile __bitf_T *)0xFFFFA404)->bit00)
#define IMR1EIMK33         (((volatile __bitf_T *)0xFFFFA404)->bit01)
#define IMR1EIMK34         (((volatile __bitf_T *)0xFFFFA404)->bit02)
#define IMR1EIMK35         (((volatile __bitf_T *)0xFFFFA404)->bit03)
#define IMR1EIMK36         (((volatile __bitf_T *)0xFFFFA404)->bit04)
#define IMR1EIMK37         (((volatile __bitf_T *)0xFFFFA404)->bit05)
#define IMR1EIMK38         (((volatile __bitf_T *)0xFFFFA404)->bit06)
#define IMR1EIMK39         (((volatile __bitf_T *)0xFFFFA404)->bit07)
#define IMR1EIMK40         (((volatile __bitf_T *)0xFFFFA405)->bit00)
#define IMR1EIMK41         (((volatile __bitf_T *)0xFFFFA405)->bit01)
#define IMR1EIMK42         (((volatile __bitf_T *)0xFFFFA405)->bit02)
#define IMR1EIMK43         (((volatile __bitf_T *)0xFFFFA405)->bit03)
#define IMR1EIMK44         (((volatile __bitf_T *)0xFFFFA405)->bit04)
#define IMR1EIMK45         (((volatile __bitf_T *)0xFFFFA405)->bit05)
#define IMR1EIMK46         (((volatile __bitf_T *)0xFFFFA405)->bit06)
#define IMR1EIMK47         (((volatile __bitf_T *)0xFFFFA405)->bit07)
#define IMR1EIMK49         (((volatile __bitf_T *)0xFFFFA406)->bit01)
#define IMR1EIMK50         (((volatile __bitf_T *)0xFFFFA406)->bit02)
#define IMR1EIMK51         (((volatile __bitf_T *)0xFFFFA406)->bit03)
#define IMR1EIMK52         (((volatile __bitf_T *)0xFFFFA406)->bit04)
#define IMR1EIMK53         (((volatile __bitf_T *)0xFFFFA406)->bit05)
#define IMR1EIMK54         (((volatile __bitf_T *)0xFFFFA406)->bit06)
#define IMR1EIMK55         (((volatile __bitf_T *)0xFFFFA406)->bit07)
#define IMR1EIMK56         (((volatile __bitf_T *)0xFFFFA407)->bit00)
#define IMR1EIMK57         (((volatile __bitf_T *)0xFFFFA407)->bit01)
#define IMR1EIMK58         (((volatile __bitf_T *)0xFFFFA407)->bit02)
#define IMR1EIMK59         (((volatile __bitf_T *)0xFFFFA407)->bit03)
#define IMR1EIMK60         (((volatile __bitf_T *)0xFFFFA407)->bit04)
#define IMR1EIMK61         (((volatile __bitf_T *)0xFFFFA407)->bit05)
#define IMR1EIMK62         (((volatile __bitf_T *)0xFFFFA407)->bit06)
#define IMR1EIMK63         (((volatile __bitf_T *)0xFFFFA407)->bit07)
__IOREG(IMR2, 0xFFFFA408, __READ_WRITE, uint32_t);
__IOREG(IMR2L, 0xFFFFA408, __READ_WRITE, uint16_t);
__IOREG(IMR2LL, 0xFFFFA408, __READ_WRITE, uint8_t);
__IOREG(IMR2LH, 0xFFFFA409, __READ_WRITE, uint8_t);
__IOREG(IMR2H, 0xFFFFA40A, __READ_WRITE, uint16_t);
__IOREG(IMR2HL, 0xFFFFA40A, __READ_WRITE, uint8_t);
__IOREG(IMR2HH, 0xFFFFA40B, __READ_WRITE, uint8_t);
#define IMR2EIMK64         (((volatile __bitf_T *)0xFFFFA408)->bit00)
#define IMR2EIMK65         (((volatile __bitf_T *)0xFFFFA408)->bit01)
#define IMR2EIMK66         (((volatile __bitf_T *)0xFFFFA408)->bit02)
#define IMR2EIMK67         (((volatile __bitf_T *)0xFFFFA408)->bit03)
#define IMR2EIMK68         (((volatile __bitf_T *)0xFFFFA408)->bit04)
#define IMR2EIMK69         (((volatile __bitf_T *)0xFFFFA408)->bit05)
#define IMR2EIMK70         (((volatile __bitf_T *)0xFFFFA408)->bit06)
#define IMR2EIMK71         (((volatile __bitf_T *)0xFFFFA408)->bit07)
#define IMR2EIMK72         (((volatile __bitf_T *)0xFFFFA409)->bit00)
#define IMR2EIMK73         (((volatile __bitf_T *)0xFFFFA409)->bit01)
#define IMR2EIMK74         (((volatile __bitf_T *)0xFFFFA409)->bit02)
#define IMR2EIMK75         (((volatile __bitf_T *)0xFFFFA409)->bit03)
#define IMR2EIMK76         (((volatile __bitf_T *)0xFFFFA409)->bit04)
#define IMR2EIMK77         (((volatile __bitf_T *)0xFFFFA409)->bit05)
#define IMR2EIMK78         (((volatile __bitf_T *)0xFFFFA409)->bit06)
#define IMR2EIMK79         (((volatile __bitf_T *)0xFFFFA409)->bit07)
#define IMR2EIMK80         (((volatile __bitf_T *)0xFFFFA40A)->bit00)
#define IMR2EIMK81         (((volatile __bitf_T *)0xFFFFA40A)->bit01)
#define IMR2EIMK82         (((volatile __bitf_T *)0xFFFFA40A)->bit02)
#define IMR2EIMK83         (((volatile __bitf_T *)0xFFFFA40A)->bit03)
#define IMR2EIMK84         (((volatile __bitf_T *)0xFFFFA40A)->bit04)
#define IMR2EIMK85         (((volatile __bitf_T *)0xFFFFA40A)->bit05)
#define IMR2EIMK86         (((volatile __bitf_T *)0xFFFFA40A)->bit06)
#define IMR2EIMK87         (((volatile __bitf_T *)0xFFFFA40A)->bit07)
#define IMR2EIMK88         (((volatile __bitf_T *)0xFFFFA40B)->bit00)
#define IMR2EIMK89         (((volatile __bitf_T *)0xFFFFA40B)->bit01)
#define IMR2EIMK90         (((volatile __bitf_T *)0xFFFFA40B)->bit02)
#define IMR2EIMK91         (((volatile __bitf_T *)0xFFFFA40B)->bit03)
#define IMR2EIMK92         (((volatile __bitf_T *)0xFFFFA40B)->bit04)
#define IMR2EIMK93         (((volatile __bitf_T *)0xFFFFA40B)->bit05)
#define IMR2EIMK94         (((volatile __bitf_T *)0xFFFFA40B)->bit06)
#define IMR2EIMK95         (((volatile __bitf_T *)0xFFFFA40B)->bit07)
__IOREG(IMR3, 0xFFFFA40C, __READ_WRITE, uint32_t);
__IOREG(IMR3L, 0xFFFFA40C, __READ_WRITE, uint16_t);
__IOREG(IMR3LL, 0xFFFFA40C, __READ_WRITE, uint8_t);
__IOREG(IMR3LH, 0xFFFFA40D, __READ_WRITE, uint8_t);
__IOREG(IMR3H, 0xFFFFA40E, __READ_WRITE, uint16_t);
__IOREG(IMR3HL, 0xFFFFA40E, __READ_WRITE, uint8_t);
__IOREG(IMR3HH, 0xFFFFA40F, __READ_WRITE, uint8_t);
#define IMR3EIMK102        (((volatile __bitf_T *)0xFFFFA40C)->bit06)
#define IMR3EIMK103        (((volatile __bitf_T *)0xFFFFA40C)->bit07)
#define IMR3EIMK104        (((volatile __bitf_T *)0xFFFFA40D)->bit00)
#define IMR3EIMK105        (((volatile __bitf_T *)0xFFFFA40D)->bit01)
#define IMR3EIMK106        (((volatile __bitf_T *)0xFFFFA40D)->bit02)
#define IMR3EIMK107        (((volatile __bitf_T *)0xFFFFA40D)->bit03)
#define IMR3EIMK108        (((volatile __bitf_T *)0xFFFFA40D)->bit04)
#define IMR3EIMK109        (((volatile __bitf_T *)0xFFFFA40D)->bit05)
#define IMR3EIMK110        (((volatile __bitf_T *)0xFFFFA40D)->bit06)
#define IMR3EIMK111        (((volatile __bitf_T *)0xFFFFA40D)->bit07)
#define IMR3EIMK112        (((volatile __bitf_T *)0xFFFFA40E)->bit00)
#define IMR3EIMK113        (((volatile __bitf_T *)0xFFFFA40E)->bit01)
#define IMR3EIMK114        (((volatile __bitf_T *)0xFFFFA40E)->bit02)
#define IMR3EIMK115        (((volatile __bitf_T *)0xFFFFA40E)->bit03)
#define IMR3EIMK116        (((volatile __bitf_T *)0xFFFFA40E)->bit04)
#define IMR3EIMK117        (((volatile __bitf_T *)0xFFFFA40E)->bit05)
#define IMR3EIMK118        (((volatile __bitf_T *)0xFFFFA40E)->bit06)
#define IMR3EIMK119        (((volatile __bitf_T *)0xFFFFA40E)->bit07)
#define IMR3EIMK120        (((volatile __bitf_T *)0xFFFFA40F)->bit00)
#define IMR3EIMK121        (((volatile __bitf_T *)0xFFFFA40F)->bit01)
#define IMR3EIMK122        (((volatile __bitf_T *)0xFFFFA40F)->bit02)
#define IMR3EIMK123        (((volatile __bitf_T *)0xFFFFA40F)->bit03)
#define IMR3EIMK124        (((volatile __bitf_T *)0xFFFFA40F)->bit04)
#define IMR3EIMK125        (((volatile __bitf_T *)0xFFFFA40F)->bit05)
#define IMR3EIMK126        (((volatile __bitf_T *)0xFFFFA40F)->bit06)
#define IMR3EIMK127        (((volatile __bitf_T *)0xFFFFA40F)->bit07)
__IOREG(IMR4, 0xFFFFA410, __READ_WRITE, uint32_t);
__IOREG(IMR4L, 0xFFFFA410, __READ_WRITE, uint16_t);
__IOREG(IMR4LL, 0xFFFFA410, __READ_WRITE, uint8_t);
__IOREG(IMR4LH, 0xFFFFA411, __READ_WRITE, uint8_t);
__IOREG(IMR4H, 0xFFFFA412, __READ_WRITE, uint16_t);
__IOREG(IMR4HL, 0xFFFFA412, __READ_WRITE, uint8_t);
__IOREG(IMR4HH, 0xFFFFA413, __READ_WRITE, uint8_t);
#define IMR4EIMK134        (((volatile __bitf_T *)0xFFFFA410)->bit06)
#define IMR4EIMK135        (((volatile __bitf_T *)0xFFFFA410)->bit07)
#define IMR4EIMK136        (((volatile __bitf_T *)0xFFFFA411)->bit00)
#define IMR4EIMK137        (((volatile __bitf_T *)0xFFFFA411)->bit01)
#define IMR4EIMK138        (((volatile __bitf_T *)0xFFFFA411)->bit02)
#define IMR4EIMK139        (((volatile __bitf_T *)0xFFFFA411)->bit03)
#define IMR4EIMK140        (((volatile __bitf_T *)0xFFFFA411)->bit04)
#define IMR4EIMK141        (((volatile __bitf_T *)0xFFFFA411)->bit05)
#define IMR4EIMK142        (((volatile __bitf_T *)0xFFFFA411)->bit06)
#define IMR4EIMK143        (((volatile __bitf_T *)0xFFFFA411)->bit07)
#define IMR4EIMK144        (((volatile __bitf_T *)0xFFFFA412)->bit00)
#define IMR4EIMK145        (((volatile __bitf_T *)0xFFFFA412)->bit01)
#define IMR4EIMK146        (((volatile __bitf_T *)0xFFFFA412)->bit02)
#define IMR4EIMK147        (((volatile __bitf_T *)0xFFFFA412)->bit03)
#define IMR4EIMK148        (((volatile __bitf_T *)0xFFFFA412)->bit04)
#define IMR4EIMK149        (((volatile __bitf_T *)0xFFFFA412)->bit05)
#define IMR4EIMK150        (((volatile __bitf_T *)0xFFFFA412)->bit06)
#define IMR4EIMK151        (((volatile __bitf_T *)0xFFFFA412)->bit07)
#define IMR4EIMK152        (((volatile __bitf_T *)0xFFFFA413)->bit00)
#define IMR4EIMK153        (((volatile __bitf_T *)0xFFFFA413)->bit01)
#define IMR4EIMK154        (((volatile __bitf_T *)0xFFFFA413)->bit02)
#define IMR4EIMK155        (((volatile __bitf_T *)0xFFFFA413)->bit03)
#define IMR4EIMK156        (((volatile __bitf_T *)0xFFFFA413)->bit04)
#define IMR4EIMK157        (((volatile __bitf_T *)0xFFFFA413)->bit05)
#define IMR4EIMK158        (((volatile __bitf_T *)0xFFFFA413)->bit06)
#define IMR4EIMK159        (((volatile __bitf_T *)0xFFFFA413)->bit07)
__IOREG(IMR5, 0xFFFFA414, __READ_WRITE, uint32_t);
__IOREG(IMR5L, 0xFFFFA414, __READ_WRITE, uint16_t);
__IOREG(IMR5LL, 0xFFFFA414, __READ_WRITE, uint8_t);
__IOREG(IMR5H, 0xFFFFA416, __READ_WRITE, uint16_t);
__IOREG(IMR5HL, 0xFFFFA416, __READ_WRITE, uint8_t);
__IOREG(IMR5HH, 0xFFFFA417, __READ_WRITE, uint8_t);
#define IMR5EIMK160        (((volatile __bitf_T *)0xFFFFA414)->bit00)
#define IMR5EIMK161        (((volatile __bitf_T *)0xFFFFA414)->bit01)
#define IMR5EIMK162        (((volatile __bitf_T *)0xFFFFA414)->bit02)
#define IMR5EIMK163        (((volatile __bitf_T *)0xFFFFA414)->bit03)
#define IMR5EIMK176        (((volatile __bitf_T *)0xFFFFA416)->bit00)
#define IMR5EIMK177        (((volatile __bitf_T *)0xFFFFA416)->bit01)
#define IMR5EIMK178        (((volatile __bitf_T *)0xFFFFA416)->bit02)
#define IMR5EIMK179        (((volatile __bitf_T *)0xFFFFA416)->bit03)
#define IMR5EIMK180        (((volatile __bitf_T *)0xFFFFA416)->bit04)
#define IMR5EIMK181        (((volatile __bitf_T *)0xFFFFA416)->bit05)
#define IMR5EIMK182        (((volatile __bitf_T *)0xFFFFA416)->bit06)
#define IMR5EIMK183        (((volatile __bitf_T *)0xFFFFA416)->bit07)
#define IMR5EIMK184        (((volatile __bitf_T *)0xFFFFA417)->bit00)
#define IMR5EIMK185        (((volatile __bitf_T *)0xFFFFA417)->bit01)
#define IMR5EIMK186        (((volatile __bitf_T *)0xFFFFA417)->bit02)
#define IMR5EIMK187        (((volatile __bitf_T *)0xFFFFA417)->bit03)
#define IMR5EIMK188        (((volatile __bitf_T *)0xFFFFA417)->bit04)
#define IMR5EIMK189        (((volatile __bitf_T *)0xFFFFA417)->bit05)
#define IMR5EIMK190        (((volatile __bitf_T *)0xFFFFA417)->bit06)
#define IMR5EIMK191        (((volatile __bitf_T *)0xFFFFA417)->bit07)
__IOREG(IMR6, 0xFFFFA418, __READ_WRITE, uint32_t);
__IOREG(IMR6L, 0xFFFFA418, __READ_WRITE, uint16_t);
__IOREG(IMR6LL, 0xFFFFA418, __READ_WRITE, uint8_t);
__IOREG(IMR6LH, 0xFFFFA419, __READ_WRITE, uint8_t);
__IOREG(IMR6H, 0xFFFFA41A, __READ_WRITE, uint16_t);
__IOREG(IMR6HL, 0xFFFFA41A, __READ_WRITE, uint8_t);
__IOREG(IMR6HH, 0xFFFFA41B, __READ_WRITE, uint8_t);
#define IMR6EIMK192        (((volatile __bitf_T *)0xFFFFA418)->bit00)
#define IMR6EIMK193        (((volatile __bitf_T *)0xFFFFA418)->bit01)
#define IMR6EIMK194        (((volatile __bitf_T *)0xFFFFA418)->bit02)
#define IMR6EIMK195        (((volatile __bitf_T *)0xFFFFA418)->bit03)
#define IMR6EIMK196        (((volatile __bitf_T *)0xFFFFA418)->bit04)
#define IMR6EIMK197        (((volatile __bitf_T *)0xFFFFA418)->bit05)
#define IMR6EIMK198        (((volatile __bitf_T *)0xFFFFA418)->bit06)
#define IMR6EIMK199        (((volatile __bitf_T *)0xFFFFA418)->bit07)
#define IMR6EIMK200        (((volatile __bitf_T *)0xFFFFA419)->bit00)
#define IMR6EIMK201        (((volatile __bitf_T *)0xFFFFA419)->bit01)
#define IMR6EIMK202        (((volatile __bitf_T *)0xFFFFA419)->bit02)
#define IMR6EIMK203        (((volatile __bitf_T *)0xFFFFA419)->bit03)
#define IMR6EIMK204        (((volatile __bitf_T *)0xFFFFA419)->bit04)
#define IMR6EIMK205        (((volatile __bitf_T *)0xFFFFA419)->bit05)
#define IMR6EIMK206        (((volatile __bitf_T *)0xFFFFA419)->bit06)
#define IMR6EIMK207        (((volatile __bitf_T *)0xFFFFA419)->bit07)
#define IMR6EIMK209        (((volatile __bitf_T *)0xFFFFA41A)->bit01)
#define IMR6EIMK210        (((volatile __bitf_T *)0xFFFFA41A)->bit02)
#define IMR6EIMK211        (((volatile __bitf_T *)0xFFFFA41A)->bit03)
#define IMR6EIMK212        (((volatile __bitf_T *)0xFFFFA41A)->bit04)
#define IMR6EIMK213        (((volatile __bitf_T *)0xFFFFA41A)->bit05)
#define IMR6EIMK214        (((volatile __bitf_T *)0xFFFFA41A)->bit06)
#define IMR6EIMK215        (((volatile __bitf_T *)0xFFFFA41A)->bit07)
#define IMR6EIMK216        (((volatile __bitf_T *)0xFFFFA41B)->bit00)
#define IMR6EIMK217        (((volatile __bitf_T *)0xFFFFA41B)->bit01)
#define IMR6EIMK218        (((volatile __bitf_T *)0xFFFFA41B)->bit02)
#define IMR6EIMK219        (((volatile __bitf_T *)0xFFFFA41B)->bit03)
#define IMR6EIMK220        (((volatile __bitf_T *)0xFFFFA41B)->bit04)
#define IMR6EIMK221        (((volatile __bitf_T *)0xFFFFA41B)->bit05)
#define IMR6EIMK222        (((volatile __bitf_T *)0xFFFFA41B)->bit06)
#define IMR6EIMK223        (((volatile __bitf_T *)0xFFFFA41B)->bit07)
__IOREG(IMR7, 0xFFFFA41C, __READ_WRITE, uint32_t);
__IOREG(IMR7L, 0xFFFFA41C, __READ_WRITE, uint16_t);
__IOREG(IMR7LL, 0xFFFFA41C, __READ_WRITE, uint8_t);
__IOREG(IMR7LH, 0xFFFFA41D, __READ_WRITE, uint8_t);
__IOREG(IMR7H, 0xFFFFA41E, __READ_WRITE, uint16_t);
__IOREG(IMR7HL, 0xFFFFA41E, __READ_WRITE, uint8_t);
__IOREG(IMR7HH, 0xFFFFA41F, __READ_WRITE, uint8_t);
#define IMR7EIMK224        (((volatile __bitf_T *)0xFFFFA41C)->bit00)
#define IMR7EIMK225        (((volatile __bitf_T *)0xFFFFA41C)->bit01)
#define IMR7EIMK226        (((volatile __bitf_T *)0xFFFFA41C)->bit02)
#define IMR7EIMK227        (((volatile __bitf_T *)0xFFFFA41C)->bit03)
#define IMR7EIMK228        (((volatile __bitf_T *)0xFFFFA41C)->bit04)
#define IMR7EIMK229        (((volatile __bitf_T *)0xFFFFA41C)->bit05)
#define IMR7EIMK230        (((volatile __bitf_T *)0xFFFFA41C)->bit06)
#define IMR7EIMK231        (((volatile __bitf_T *)0xFFFFA41C)->bit07)
#define IMR7EIMK232        (((volatile __bitf_T *)0xFFFFA41D)->bit00)
#define IMR7EIMK233        (((volatile __bitf_T *)0xFFFFA41D)->bit01)
#define IMR7EIMK234        (((volatile __bitf_T *)0xFFFFA41D)->bit02)
#define IMR7EIMK235        (((volatile __bitf_T *)0xFFFFA41D)->bit03)
#define IMR7EIMK236        (((volatile __bitf_T *)0xFFFFA41D)->bit04)
#define IMR7EIMK237        (((volatile __bitf_T *)0xFFFFA41D)->bit05)
#define IMR7EIMK238        (((volatile __bitf_T *)0xFFFFA41D)->bit06)
#define IMR7EIMK239        (((volatile __bitf_T *)0xFFFFA41D)->bit07)
#define IMR7EIMK240        (((volatile __bitf_T *)0xFFFFA41E)->bit00)
#define IMR7EIMK241        (((volatile __bitf_T *)0xFFFFA41E)->bit01)
#define IMR7EIMK242        (((volatile __bitf_T *)0xFFFFA41E)->bit02)
#define IMR7EIMK243        (((volatile __bitf_T *)0xFFFFA41E)->bit03)
#define IMR7EIMK244        (((volatile __bitf_T *)0xFFFFA41E)->bit04)
#define IMR7EIMK245        (((volatile __bitf_T *)0xFFFFA41E)->bit05)
#define IMR7EIMK246        (((volatile __bitf_T *)0xFFFFA41E)->bit06)
#define IMR7EIMK247        (((volatile __bitf_T *)0xFFFFA41E)->bit07)
#define IMR7EIMK248        (((volatile __bitf_T *)0xFFFFA41F)->bit00)
#define IMR7EIMK249        (((volatile __bitf_T *)0xFFFFA41F)->bit01)
#define IMR7EIMK250        (((volatile __bitf_T *)0xFFFFA41F)->bit02)
#define IMR7EIMK251        (((volatile __bitf_T *)0xFFFFA41F)->bit03)
#define IMR7EIMK252        (((volatile __bitf_T *)0xFFFFA41F)->bit04)
#define IMR7EIMK253        (((volatile __bitf_T *)0xFFFFA41F)->bit05)
#define IMR7EIMK254        (((volatile __bitf_T *)0xFFFFA41F)->bit06)
#define IMR7EIMK255        (((volatile __bitf_T *)0xFFFFA41F)->bit07)
__IOREG(IMR8, 0xFFFFA420, __READ_WRITE, uint32_t);
__IOREG(IMR8L, 0xFFFFA420, __READ_WRITE, uint16_t);
__IOREG(IMR8LL, 0xFFFFA420, __READ_WRITE, uint8_t);
__IOREG(IMR8LH, 0xFFFFA421, __READ_WRITE, uint8_t);
__IOREG(IMR8H, 0xFFFFA422, __READ_WRITE, uint16_t);
__IOREG(IMR8HL, 0xFFFFA422, __READ_WRITE, uint8_t);
__IOREG(IMR8HH, 0xFFFFA423, __READ_WRITE, uint8_t);
#define IMR8EIMK256        (((volatile __bitf_T *)0xFFFFA420)->bit00)
#define IMR8EIMK257        (((volatile __bitf_T *)0xFFFFA420)->bit01)
#define IMR8EIMK258        (((volatile __bitf_T *)0xFFFFA420)->bit02)
#define IMR8EIMK259        (((volatile __bitf_T *)0xFFFFA420)->bit03)
#define IMR8EIMK260        (((volatile __bitf_T *)0xFFFFA420)->bit04)
#define IMR8EIMK261        (((volatile __bitf_T *)0xFFFFA420)->bit05)
#define IMR8EIMK262        (((volatile __bitf_T *)0xFFFFA420)->bit06)
#define IMR8EIMK263        (((volatile __bitf_T *)0xFFFFA420)->bit07)
#define IMR8EIMK264        (((volatile __bitf_T *)0xFFFFA421)->bit00)
#define IMR8EIMK265        (((volatile __bitf_T *)0xFFFFA421)->bit01)
#define IMR8EIMK266        (((volatile __bitf_T *)0xFFFFA421)->bit02)
#define IMR8EIMK267        (((volatile __bitf_T *)0xFFFFA421)->bit03)
#define IMR8EIMK268        (((volatile __bitf_T *)0xFFFFA421)->bit04)
#define IMR8EIMK269        (((volatile __bitf_T *)0xFFFFA421)->bit05)
#define IMR8EIMK270        (((volatile __bitf_T *)0xFFFFA421)->bit06)
#define IMR8EIMK271        (((volatile __bitf_T *)0xFFFFA421)->bit07)
#define IMR8EIMK272        (((volatile __bitf_T *)0xFFFFA422)->bit00)
#define IMR8EIMK273        (((volatile __bitf_T *)0xFFFFA422)->bit01)
#define IMR8EIMK274        (((volatile __bitf_T *)0xFFFFA422)->bit02)
#define IMR8EIMK275        (((volatile __bitf_T *)0xFFFFA422)->bit03)
#define IMR8EIMK276        (((volatile __bitf_T *)0xFFFFA422)->bit04)
#define IMR8EIMK277        (((volatile __bitf_T *)0xFFFFA422)->bit05)
#define IMR8EIMK278        (((volatile __bitf_T *)0xFFFFA422)->bit06)
#define IMR8EIMK279        (((volatile __bitf_T *)0xFFFFA422)->bit07)
#define IMR8EIMK280        (((volatile __bitf_T *)0xFFFFA423)->bit00)
#define IMR8EIMK281        (((volatile __bitf_T *)0xFFFFA423)->bit01)

__IOREG(SELF, 0xFFA08000, __READ_WRITE, __type24);                    
__IOREG(FACI, 0xFFA10000, __READ_WRITE, __type25);                    
__IOREG(PFSS, 0xFFBC0700, __READ_WRITE, __type26);                    
__IOREG(CCIB, 0xFFC59008, __READ_WRITE, __type27);                    
__IOREG(RIIC0, 0xFFCA0000, __READ_WRITE, __type28);                   
__IOREG(SCDS, 0xFFCD00D0, __READ_WRITE, __type29);                    
__IOREG(RLN240, 0xFFCE0000, __READ_WRITE, __type30);                  
__IOREG(RLN2400, 0xFFCE0008, __READ_WRITE, __type31);                 
__IOREG(RLN2401, 0xFFCE0028, __READ_WRITE, __type32);                 
__IOREG(RLN2402, 0xFFCE0048, __READ_WRITE, __type33);                 
__IOREG(RLN2403, 0xFFCE0068, __READ_WRITE, __type34);                 
__IOREG(RLN241, 0xFFCE0080, __READ_WRITE, __type30);                  
__IOREG(RLN2414, 0xFFCE0088, __READ_WRITE, __type31);                 
__IOREG(RLN2415, 0xFFCE00A8, __READ_WRITE, __type32);                 
__IOREG(RLN2416, 0xFFCE00C8, __READ_WRITE, __type33);                 
__IOREG(RLN2417, 0xFFCE00E8, __READ_WRITE, __type34);                 
__IOREG(RLN210, 0xFFCE0100, __READ_WRITE, __type30);                  
__IOREG(RLN2108, 0xFFCE0108, __READ_WRITE, __type31);                 
__IOREG(RLN211, 0xFFCE0120, __READ_WRITE, __type30);                  
__IOREG(RLN2119, 0xFFCE0128, __READ_WRITE, __type31);                 
__IOREG(RLN30, 0xFFCF0000, __READ_WRITE, __type35);                   
__IOREG(RLN31, 0xFFCF0040, __READ_WRITE, __type35);                   
__IOREG(RLN32, 0xFFCF0080, __READ_WRITE, __type35);                   
__IOREG(RLN33, 0xFFCF00C0, __READ_WRITE, __type35);                   
__IOREG(RLN34, 0xFFCF0100, __READ_WRITE, __type35);                   
__IOREG(RLN35, 0xFFCF0140, __READ_WRITE, __type35);                   
__IOREG(RSCAN0, 0xFFD00000, __READ_WRITE, __type36);                  
__IOREG(CSIH0, 0xFFD80000, __READ_WRITE, __type37);                   
__IOREG(CSIH1, 0xFFD82000, __READ_WRITE, __type38);                   
__IOREG(CSIH2, 0xFFD84000, __READ_WRITE, __type38);                   
__IOREG(CSIH3, 0xFFD86000, __READ_WRITE, __type39);                   
__IOREG(CSIG0, 0xFFDB0000, __READ_WRITE, __type40);                   
__IOREG(CSIG1, 0xFFDB2000, __READ_WRITE, __type40);                   
__IOREG(PIC0, 0xFFDD0004, __READ_WRITE, __type41);                    
__IOREG(TAUD0, 0xFFE20000, __READ_WRITE, __type42);                   
__IOREG(TAUB0, 0xFFE30000, __READ_WRITE, __type43);                   
__IOREG(TAUB1, 0xFFE31000, __READ_WRITE, __type43);                   
__IOREG(TAUJ0, 0xFFE50000, __READ_WRITE, __type44);                   
__IOREG(TAUJ1, 0xFFE51000, __READ_WRITE, __type44);                   
__IOREG(PWSA0, 0xFFE70000, __READ_WRITE, __type45);                   
__IOREG(PWGA0, 0xFFE71000, __READ_WRITE, __type46);                   
__IOREG(PWGA1, 0xFFE71040, __READ_WRITE, __type46);                   
__IOREG(PWGA2, 0xFFE71080, __READ_WRITE, __type46);                   
__IOREG(PWGA3, 0xFFE710C0, __READ_WRITE, __type46);                   
__IOREG(PWGA4, 0xFFE71100, __READ_WRITE, __type46);                   
__IOREG(PWGA5, 0xFFE71140, __READ_WRITE, __type46);                   
__IOREG(PWGA6, 0xFFE71180, __READ_WRITE, __type46);                   
__IOREG(PWGA7, 0xFFE711C0, __READ_WRITE, __type46);                   
__IOREG(PWGA8, 0xFFE71200, __READ_WRITE, __type46);                   
__IOREG(PWGA9, 0xFFE71240, __READ_WRITE, __type46);                   
__IOREG(PWGA10, 0xFFE71280, __READ_WRITE, __type46);                  
__IOREG(PWGA11, 0xFFE712C0, __READ_WRITE, __type46);                  
__IOREG(PWGA12, 0xFFE71300, __READ_WRITE, __type46);                  
__IOREG(PWGA13, 0xFFE71340, __READ_WRITE, __type46);                  
__IOREG(PWGA14, 0xFFE71380, __READ_WRITE, __type46);                  
__IOREG(PWGA15, 0xFFE713C0, __READ_WRITE, __type46);                  
__IOREG(PWGA16, 0xFFE71400, __READ_WRITE, __type46);                  
__IOREG(PWGA17, 0xFFE71440, __READ_WRITE, __type46);                  
__IOREG(PWGA18, 0xFFE71480, __READ_WRITE, __type46);                  
__IOREG(PWGA19, 0xFFE714C0, __READ_WRITE, __type46);                  
__IOREG(PWGA20, 0xFFE71500, __READ_WRITE, __type46);                  
__IOREG(PWGA21, 0xFFE71540, __READ_WRITE, __type46);                  
__IOREG(PWGA22, 0xFFE71580, __READ_WRITE, __type46);                  
__IOREG(PWGA23, 0xFFE715C0, __READ_WRITE, __type46);                  
__IOREG(PWGA24, 0xFFE71600, __READ_WRITE, __type46);                  
__IOREG(PWGA25, 0xFFE71640, __READ_WRITE, __type46);                  
__IOREG(PWGA26, 0xFFE71680, __READ_WRITE, __type46);                  
__IOREG(PWGA27, 0xFFE716C0, __READ_WRITE, __type46);                  
__IOREG(PWGA28, 0xFFE71700, __READ_WRITE, __type46);                  
__IOREG(PWGA29, 0xFFE71740, __READ_WRITE, __type46);                  
__IOREG(PWGA30, 0xFFE71780, __READ_WRITE, __type46);                  
__IOREG(PWGA31, 0xFFE717C0, __READ_WRITE, __type46);                  
__IOREG(PWGA32, 0xFFE71800, __READ_WRITE, __type46);                  
__IOREG(PWGA33, 0xFFE71840, __READ_WRITE, __type46);                  
__IOREG(PWGA34, 0xFFE71880, __READ_WRITE, __type46);                  
__IOREG(PWGA35, 0xFFE718C0, __READ_WRITE, __type46);                  
__IOREG(PWGA36, 0xFFE71900, __READ_WRITE, __type46);                  
__IOREG(PWGA37, 0xFFE71940, __READ_WRITE, __type46);                  
__IOREG(PWGA38, 0xFFE71980, __READ_WRITE, __type46);                  
__IOREG(PWGA39, 0xFFE719C0, __READ_WRITE, __type46);                  
__IOREG(PWGA40, 0xFFE71A00, __READ_WRITE, __type46);                  
__IOREG(PWGA41, 0xFFE71A40, __READ_WRITE, __type46);                  
__IOREG(PWGA42, 0xFFE71A80, __READ_WRITE, __type46);                  
__IOREG(PWGA43, 0xFFE71AC0, __READ_WRITE, __type46);                  
__IOREG(PWGA44, 0xFFE71B00, __READ_WRITE, __type46);                  
__IOREG(PWGA45, 0xFFE71B40, __READ_WRITE, __type46);                  
__IOREG(PWGA46, 0xFFE71B80, __READ_WRITE, __type46);                  
__IOREG(PWGA47, 0xFFE71BC0, __READ_WRITE, __type46);                  
__IOREG(PWGA48, 0xFFE71C00, __READ_WRITE, __type46);                  
__IOREG(PWGA49, 0xFFE71C40, __READ_WRITE, __type46);                  
__IOREG(PWGA50, 0xFFE71C80, __READ_WRITE, __type46);                  
__IOREG(PWGA51, 0xFFE71CC0, __READ_WRITE, __type46);                  
__IOREG(PWGA52, 0xFFE71D00, __READ_WRITE, __type46);                  
__IOREG(PWGA53, 0xFFE71D40, __READ_WRITE, __type46);                  
__IOREG(PWGA54, 0xFFE71D80, __READ_WRITE, __type46);                  
__IOREG(PWGA55, 0xFFE71DC0, __READ_WRITE, __type46);                  
__IOREG(PWGA56, 0xFFE71E00, __READ_WRITE, __type46);                  
__IOREG(PWGA57, 0xFFE71E40, __READ_WRITE, __type46);                  
__IOREG(PWGA58, 0xFFE71E80, __READ_WRITE, __type46);                  
__IOREG(PWGA59, 0xFFE71EC0, __READ_WRITE, __type46);                  
__IOREG(PWGA60, 0xFFE71F00, __READ_WRITE, __type46);                  
__IOREG(PWGA61, 0xFFE71F40, __READ_WRITE, __type46);                  
__IOREG(PWGA62, 0xFFE71F80, __READ_WRITE, __type46);                  
__IOREG(PWGA63, 0xFFE71FC0, __READ_WRITE, __type46);                  
__IOREG(PWGA64, 0xFFE72000, __READ_WRITE, __type46);                  
__IOREG(PWGA65, 0xFFE72040, __READ_WRITE, __type46);                  
__IOREG(PWGA66, 0xFFE72080, __READ_WRITE, __type46);                  
__IOREG(PWGA67, 0xFFE720C0, __READ_WRITE, __type46);                  
__IOREG(PWGA68, 0xFFE72100, __READ_WRITE, __type46);                  
__IOREG(PWGA69, 0xFFE72140, __READ_WRITE, __type46);                  
__IOREG(PWGA70, 0xFFE72180, __READ_WRITE, __type46);                  
__IOREG(PWGA71, 0xFFE721C0, __READ_WRITE, __type46);                  
__IOREG(PWBA0, 0xFFE72800, __READ_WRITE, __type47);                   
__IOREG(RTCA0, 0xFFE78000, __READ_WRITE, __type48);                   
__IOREG(ENCA0, 0xFFE80000, __READ_WRITE, __type49);                   
__IOREG(TAPA0, 0xFFE90000, __READ_WRITE, __type50);                   
__IOREG(OSTM0, 0xFFEC0000, __READ_WRITE, __type51);                   
__IOREG(WDTA0, 0xFFED0000, __READ_WRITE, __type52);                   
__IOREG(WDTA1, 0xFFED1000, __READ_WRITE, __type52);                   
__IOREG(ADCA0, 0xFFF20000, __READ_WRITE, __type53);                   
__IOREG(ADCA1, 0xFFF21000, __READ_WRITE, __type54);                   
__IOREG(DCRA0, 0xFFF70000, __READ_WRITE, __type55);                   
__IOREG(DCRA1, 0xFFF71000, __READ_WRITE, __type55);                   
__IOREG(DCRA2, 0xFFF72000, __READ_WRITE, __type55);                   
__IOREG(DCRA3, 0xFFF73000, __READ_WRITE, __type55);                   
__IOREG(KR0, 0xFFF78000, __READ_WRITE, __type56);                     
__IOREG(CLMA0, 0xFFF8C000, __READ_WRITE, __type57);                   
__IOREG(CLMA, 0xFFF8C100, __READ_WRITE, __type58);                    
__IOREG(CLMA1, 0xFFF8D000, __READ_WRITE, __type57);                   
__IOREG(CLMA2, 0xFFF8E000, __READ_WRITE, __type57);                   

__IOREGARRAY(CSIG, 2, 0xFFDB0000, __READ_WRITE, __type40);            
__IOREGARRAY(TAUB, 2, 0xFFE30000, __READ_WRITE, __type43);            
__IOREGARRAY(TAUJ, 2, 0xFFE50000, __READ_WRITE, __type44);            
__IOREGARRAY(PWGA, 72, 0xFFE71000, __READ_WRITE, __type46);           
__IOREGARRAY(RLN3, 6, 0xFFCF0000, __READ_WRITE, __type35);            
__IOREGARRAY(WDTA, 2, 0xFFED0000, __READ_WRITE, __type52);            
__IOREGARRAY(DCRA, 4, 0xFFF70000, __READ_WRITE, __type55);            

#define SELFID0 SELF.ID0
#define SELFID1 SELF.ID1
#define SELFID2 SELF.ID2
#define SELFID3 SELF.ID3
#define SELFIDST SELF.IDST
#define FACIFPMON FACI.FPMON
#define FACIFASTAT FACI.FASTAT
#define FACIFAEINT FACI.FAEINT
#define FACIFAREASELC FACI.FAREASELC
#define FACIFSADDR FACI.FSADDR
#define FACIFEADDR FACI.FEADDR
#define FACIFCURAME FACI.FCURAME
#define FACIFSTATR FACI.FSTATR
#define FACIFENTRYR FACI.FENTRYR
#define FACIFPROTR FACI.FPROTR
#define FACIFSUINITR FACI.FSUINITR
#define FACIFLKSTAT FACI.FLKSTAT
#define FACIFRFSTEADR FACI.FRFSTEADR
#define FACIFRTSTAT FACI.FRTSTAT
#define FACIFCMDR FACI.FCMDR
#define FACIFPESTAT FACI.FPESTAT
#define FACIFBCCNT FACI.FBCCNT
#define FACIFBCSTAT FACI.FBCSTAT
#define FACIFPSADDR FACI.FPSADDR
#define FACIFCPSR FACI.FCPSR
#define FACIFPCKAR FACI.FPCKAR
#define FACIFECCEMON FACI.FECCEMON
#define FACIFECCTMD FACI.FECCTMD
#define FACIFDMYECC FACI.FDMYECC
#define PFSSBWCBUFEN PFSS.BWCBUFEN
#define CCIBFCUFAREA CCIB.FCUFAREA
#define RIIC0CR1 RIIC0.CR1.uint32_t
#define RIIC0CR1L RIIC0.CR1.uint16_t[L]
#define RIIC0CR1LL RIIC0.CR1.uint8_t[LL]
#define RIIC0CR1LH RIIC0.CR1.uint8_t[LH]
#define RIIC0CR1H RIIC0.CR1.uint16_t[H]
#define RIIC0CR1HL RIIC0.CR1.uint8_t[HL]
#define RIIC0CR1HH RIIC0.CR1.uint8_t[HH]
#define RIIC0CR2 RIIC0.CR2.uint32_t
#define RIIC0CR2L RIIC0.CR2.uint16_t[L]
#define RIIC0CR2LL RIIC0.CR2.uint8_t[LL]
#define RIIC0CR2LH RIIC0.CR2.uint8_t[LH]
#define RIIC0CR2H RIIC0.CR2.uint16_t[H]
#define RIIC0CR2HL RIIC0.CR2.uint8_t[HL]
#define RIIC0CR2HH RIIC0.CR2.uint8_t[HH]
#define RIIC0MR1 RIIC0.MR1.uint32_t
#define RIIC0MR1L RIIC0.MR1.uint16_t[L]
#define RIIC0MR1LL RIIC0.MR1.uint8_t[LL]
#define RIIC0MR1LH RIIC0.MR1.uint8_t[LH]
#define RIIC0MR1H RIIC0.MR1.uint16_t[H]
#define RIIC0MR1HL RIIC0.MR1.uint8_t[HL]
#define RIIC0MR1HH RIIC0.MR1.uint8_t[HH]
#define RIIC0MR2 RIIC0.MR2.uint32_t
#define RIIC0MR2L RIIC0.MR2.uint16_t[L]
#define RIIC0MR2LL RIIC0.MR2.uint8_t[LL]
#define RIIC0MR2LH RIIC0.MR2.uint8_t[LH]
#define RIIC0MR2H RIIC0.MR2.uint16_t[H]
#define RIIC0MR2HL RIIC0.MR2.uint8_t[HL]
#define RIIC0MR2HH RIIC0.MR2.uint8_t[HH]
#define RIIC0MR3 RIIC0.MR3.uint32_t
#define RIIC0MR3L RIIC0.MR3.uint16_t[L]
#define RIIC0MR3LL RIIC0.MR3.uint8_t[LL]
#define RIIC0MR3LH RIIC0.MR3.uint8_t[LH]
#define RIIC0MR3H RIIC0.MR3.uint16_t[H]
#define RIIC0MR3HL RIIC0.MR3.uint8_t[HL]
#define RIIC0MR3HH RIIC0.MR3.uint8_t[HH]
#define RIIC0FER RIIC0.FER.uint32_t
#define RIIC0FERL RIIC0.FER.uint16_t[L]
#define RIIC0FERLL RIIC0.FER.uint8_t[LL]
#define RIIC0FERLH RIIC0.FER.uint8_t[LH]
#define RIIC0FERH RIIC0.FER.uint16_t[H]
#define RIIC0FERHL RIIC0.FER.uint8_t[HL]
#define RIIC0FERHH RIIC0.FER.uint8_t[HH]
#define RIIC0SER RIIC0.SER.uint32_t
#define RIIC0SERL RIIC0.SER.uint16_t[L]
#define RIIC0SERLL RIIC0.SER.uint8_t[LL]
#define RIIC0SERLH RIIC0.SER.uint8_t[LH]
#define RIIC0SERH RIIC0.SER.uint16_t[H]
#define RIIC0SERHL RIIC0.SER.uint8_t[HL]
#define RIIC0SERHH RIIC0.SER.uint8_t[HH]
#define RIIC0IER RIIC0.IER.uint32_t
#define RIIC0IERL RIIC0.IER.uint16_t[L]
#define RIIC0IERLL RIIC0.IER.uint8_t[LL]
#define RIIC0IERLH RIIC0.IER.uint8_t[LH]
#define RIIC0IERH RIIC0.IER.uint16_t[H]
#define RIIC0IERHL RIIC0.IER.uint8_t[HL]
#define RIIC0IERHH RIIC0.IER.uint8_t[HH]
#define RIIC0SR1 RIIC0.SR1.uint32_t
#define RIIC0SR1L RIIC0.SR1.uint16_t[L]
#define RIIC0SR1LL RIIC0.SR1.uint8_t[LL]
#define RIIC0SR1LH RIIC0.SR1.uint8_t[LH]
#define RIIC0SR1H RIIC0.SR1.uint16_t[H]
#define RIIC0SR1HL RIIC0.SR1.uint8_t[HL]
#define RIIC0SR1HH RIIC0.SR1.uint8_t[HH]
#define RIIC0AAS0 RIIC0.SR1.AAS0
#define RIIC0AAS1 RIIC0.SR1.AAS1
#define RIIC0AAS2 RIIC0.SR1.AAS2
#define RIIC0GCA RIIC0.SR1.GCA
#define RIIC0DID RIIC0.SR1.DID
#define RIIC0SR2 RIIC0.SR2.uint32_t
#define RIIC0SR2L RIIC0.SR2.uint16_t[L]
#define RIIC0SR2LL RIIC0.SR2.uint8_t[LL]
#define RIIC0SR2LH RIIC0.SR2.uint8_t[LH]
#define RIIC0SR2H RIIC0.SR2.uint16_t[H]
#define RIIC0SR2HL RIIC0.SR2.uint8_t[HL]
#define RIIC0SR2HH RIIC0.SR2.uint8_t[HH]
#define RIIC0TMOF RIIC0.SR2.TMOF
#define RIIC0AL RIIC0.SR2.AL
#define RIIC0START RIIC0.SR2.START
#define RIIC0STOP RIIC0.SR2.STOP
#define RIIC0NACKF RIIC0.SR2.NACKF
#define RIIC0RDRF RIIC0.SR2.RDRF
#define RIIC0TEND RIIC0.SR2.TEND
#define RIIC0TDRE RIIC0.SR2.TDRE
#define RIIC0SAR0 RIIC0.SAR0.uint32_t
#define RIIC0SAR0L RIIC0.SAR0.uint16_t[L]
#define RIIC0SAR0LL RIIC0.SAR0.uint8_t[LL]
#define RIIC0SAR0LH RIIC0.SAR0.uint8_t[LH]
#define RIIC0SAR0H RIIC0.SAR0.uint16_t[H]
#define RIIC0SAR0HL RIIC0.SAR0.uint8_t[HL]
#define RIIC0SAR0HH RIIC0.SAR0.uint8_t[HH]
#define RIIC0SAR1 RIIC0.SAR1.uint32_t
#define RIIC0SAR1L RIIC0.SAR1.uint16_t[L]
#define RIIC0SAR1LL RIIC0.SAR1.uint8_t[LL]
#define RIIC0SAR1LH RIIC0.SAR1.uint8_t[LH]
#define RIIC0SAR1H RIIC0.SAR1.uint16_t[H]
#define RIIC0SAR1HL RIIC0.SAR1.uint8_t[HL]
#define RIIC0SAR1HH RIIC0.SAR1.uint8_t[HH]
#define RIIC0SAR2 RIIC0.SAR2.uint32_t
#define RIIC0SAR2L RIIC0.SAR2.uint16_t[L]
#define RIIC0SAR2LL RIIC0.SAR2.uint8_t[LL]
#define RIIC0SAR2LH RIIC0.SAR2.uint8_t[LH]
#define RIIC0SAR2H RIIC0.SAR2.uint16_t[H]
#define RIIC0SAR2HL RIIC0.SAR2.uint8_t[HL]
#define RIIC0SAR2HH RIIC0.SAR2.uint8_t[HH]
#define RIIC0BRL RIIC0.BRL.uint32_t
#define RIIC0BRLL RIIC0.BRL.uint16_t[L]
#define RIIC0BRLLL RIIC0.BRL.uint8_t[LL]
#define RIIC0BRLLH RIIC0.BRL.uint8_t[LH]
#define RIIC0BRLH RIIC0.BRL.uint16_t[H]
#define RIIC0BRLHL RIIC0.BRL.uint8_t[HL]
#define RIIC0BRLHH RIIC0.BRL.uint8_t[HH]
#define RIIC0BRH RIIC0.BRH.uint32_t
#define RIIC0BRHL RIIC0.BRH.uint16_t[L]
#define RIIC0BRHLL RIIC0.BRH.uint8_t[LL]
#define RIIC0BRHLH RIIC0.BRH.uint8_t[LH]
#define RIIC0BRHH RIIC0.BRH.uint16_t[H]
#define RIIC0BRHHL RIIC0.BRH.uint8_t[HL]
#define RIIC0BRHHH RIIC0.BRH.uint8_t[HH]
#define RIIC0DRT RIIC0.DRT.uint32_t
#define RIIC0DRTL RIIC0.DRT.uint16_t[L]
#define RIIC0DRTLL RIIC0.DRT.uint8_t[LL]
#define RIIC0DRTLH RIIC0.DRT.uint8_t[LH]
#define RIIC0DRTH RIIC0.DRT.uint16_t[H]
#define RIIC0DRTHL RIIC0.DRT.uint8_t[HL]
#define RIIC0DRTHH RIIC0.DRT.uint8_t[HH]
#define RIIC0DRR RIIC0.DRR.uint32_t
#define RIIC0DRRL RIIC0.DRR.uint16_t[L]
#define RIIC0DRRLL RIIC0.DRR.uint8_t[LL]
#define RIIC0DRRLH RIIC0.DRR.uint8_t[LH]
#define RIIC0DRRH RIIC0.DRR.uint16_t[H]
#define RIIC0DRRHL RIIC0.DRR.uint8_t[HL]
#define RIIC0DRRHH RIIC0.DRR.uint8_t[HH]
#define SCDSPRDNAME1 SCDS.PRDNAME1
#define SCDSPRDNAME2 SCDS.PRDNAME2
#define SCDSPRDNAME3 SCDS.PRDNAME3
#define RLN240GLWBR RLN240.GLWBR
#define RLN240GLBRP0 RLN240.GLBRP0
#define RLN240GLBRP1 RLN240.GLBRP1
#define RLN240GLSTC RLN240.GLSTC
#define RLN2400L0MD RLN2400.L0MD
#define RLN2400L0BFC RLN2400.L0BFC
#define RLN2400L0SC RLN2400.L0SC
#define RLN2400L0WUP RLN2400.L0WUP
#define RLN2400L0IE RLN2400.L0IE
#define RLN2400L0EDE RLN2400.L0EDE
#define RLN2400L0CUC RLN2400.L0CUC
#define RLN2400L0TRC RLN2400.L0TRC
#define RLN2400L0MST RLN2400.L0MST
#define RLN2400L0ST RLN2400.L0ST
#define RLN2400L0EST RLN2400.L0EST
#define RLN2400L0DFC RLN2400.L0DFC
#define RLN2400L0IDB RLN2400.L0IDB
#define RLN2400L0CBR RLN2400.L0CBR
#define RLN2400L0DBR1 RLN2400.L0DBR1
#define RLN2400L0DBR2 RLN2400.L0DBR2
#define RLN2400L0DBR3 RLN2400.L0DBR3
#define RLN2400L0DBR4 RLN2400.L0DBR4
#define RLN2400L0DBR5 RLN2400.L0DBR5
#define RLN2400L0DBR6 RLN2400.L0DBR6
#define RLN2400L0DBR7 RLN2400.L0DBR7
#define RLN2400L0DBR8 RLN2400.L0DBR8
#define RLN2401L1MD RLN2401.L1MD
#define RLN2401L1BFC RLN2401.L1BFC
#define RLN2401L1SC RLN2401.L1SC
#define RLN2401L1WUP RLN2401.L1WUP
#define RLN2401L1IE RLN2401.L1IE
#define RLN2401L1EDE RLN2401.L1EDE
#define RLN2401L1CUC RLN2401.L1CUC
#define RLN2401L1TRC RLN2401.L1TRC
#define RLN2401L1MST RLN2401.L1MST
#define RLN2401L1ST RLN2401.L1ST
#define RLN2401L1EST RLN2401.L1EST
#define RLN2401L1DFC RLN2401.L1DFC
#define RLN2401L1IDB RLN2401.L1IDB
#define RLN2401L1CBR RLN2401.L1CBR
#define RLN2401L1DBR1 RLN2401.L1DBR1
#define RLN2401L1DBR2 RLN2401.L1DBR2
#define RLN2401L1DBR3 RLN2401.L1DBR3
#define RLN2401L1DBR4 RLN2401.L1DBR4
#define RLN2401L1DBR5 RLN2401.L1DBR5
#define RLN2401L1DBR6 RLN2401.L1DBR6
#define RLN2401L1DBR7 RLN2401.L1DBR7
#define RLN2401L1DBR8 RLN2401.L1DBR8
#define RLN2402L2MD RLN2402.L2MD
#define RLN2402L2BFC RLN2402.L2BFC
#define RLN2402L2SC RLN2402.L2SC
#define RLN2402L2WUP RLN2402.L2WUP
#define RLN2402L2IE RLN2402.L2IE
#define RLN2402L2EDE RLN2402.L2EDE
#define RLN2402L2CUC RLN2402.L2CUC
#define RLN2402L2TRC RLN2402.L2TRC
#define RLN2402L2MST RLN2402.L2MST
#define RLN2402L2ST RLN2402.L2ST
#define RLN2402L2EST RLN2402.L2EST
#define RLN2402L2DFC RLN2402.L2DFC
#define RLN2402L2IDB RLN2402.L2IDB
#define RLN2402L2CBR RLN2402.L2CBR
#define RLN2402L2DBR1 RLN2402.L2DBR1
#define RLN2402L2DBR2 RLN2402.L2DBR2
#define RLN2402L2DBR3 RLN2402.L2DBR3
#define RLN2402L2DBR4 RLN2402.L2DBR4
#define RLN2402L2DBR5 RLN2402.L2DBR5
#define RLN2402L2DBR6 RLN2402.L2DBR6
#define RLN2402L2DBR7 RLN2402.L2DBR7
#define RLN2402L2DBR8 RLN2402.L2DBR8
#define RLN2403L3MD RLN2403.L3MD
#define RLN2403L3BFC RLN2403.L3BFC
#define RLN2403L3SC RLN2403.L3SC
#define RLN2403L3WUP RLN2403.L3WUP
#define RLN2403L3IE RLN2403.L3IE
#define RLN2403L3EDE RLN2403.L3EDE
#define RLN2403L3CUC RLN2403.L3CUC
#define RLN2403L3TRC RLN2403.L3TRC
#define RLN2403L3MST RLN2403.L3MST
#define RLN2403L3ST RLN2403.L3ST
#define RLN2403L3EST RLN2403.L3EST
#define RLN2403L3DFC RLN2403.L3DFC
#define RLN2403L3IDB RLN2403.L3IDB
#define RLN2403L3CBR RLN2403.L3CBR
#define RLN2403L3DBR1 RLN2403.L3DBR1
#define RLN2403L3DBR2 RLN2403.L3DBR2
#define RLN2403L3DBR3 RLN2403.L3DBR3
#define RLN2403L3DBR4 RLN2403.L3DBR4
#define RLN2403L3DBR5 RLN2403.L3DBR5
#define RLN2403L3DBR6 RLN2403.L3DBR6
#define RLN2403L3DBR7 RLN2403.L3DBR7
#define RLN2403L3DBR8 RLN2403.L3DBR8
#define RLN241GLWBR RLN241.GLWBR
#define RLN241GLBRP0 RLN241.GLBRP0
#define RLN241GLBRP1 RLN241.GLBRP1
#define RLN241GLSTC RLN241.GLSTC
#define RLN2414L0MD RLN2414.L0MD
#define RLN2414L0BFC RLN2414.L0BFC
#define RLN2414L0SC RLN2414.L0SC
#define RLN2414L0WUP RLN2414.L0WUP
#define RLN2414L0IE RLN2414.L0IE
#define RLN2414L0EDE RLN2414.L0EDE
#define RLN2414L0CUC RLN2414.L0CUC
#define RLN2414L0TRC RLN2414.L0TRC
#define RLN2414L0MST RLN2414.L0MST
#define RLN2414L0ST RLN2414.L0ST
#define RLN2414L0EST RLN2414.L0EST
#define RLN2414L0DFC RLN2414.L0DFC
#define RLN2414L0IDB RLN2414.L0IDB
#define RLN2414L0CBR RLN2414.L0CBR
#define RLN2414L0DBR1 RLN2414.L0DBR1
#define RLN2414L0DBR2 RLN2414.L0DBR2
#define RLN2414L0DBR3 RLN2414.L0DBR3
#define RLN2414L0DBR4 RLN2414.L0DBR4
#define RLN2414L0DBR5 RLN2414.L0DBR5
#define RLN2414L0DBR6 RLN2414.L0DBR6
#define RLN2414L0DBR7 RLN2414.L0DBR7
#define RLN2414L0DBR8 RLN2414.L0DBR8
#define RLN2415L1MD RLN2415.L1MD
#define RLN2415L1BFC RLN2415.L1BFC
#define RLN2415L1SC RLN2415.L1SC
#define RLN2415L1WUP RLN2415.L1WUP
#define RLN2415L1IE RLN2415.L1IE
#define RLN2415L1EDE RLN2415.L1EDE
#define RLN2415L1CUC RLN2415.L1CUC
#define RLN2415L1TRC RLN2415.L1TRC
#define RLN2415L1MST RLN2415.L1MST
#define RLN2415L1ST RLN2415.L1ST
#define RLN2415L1EST RLN2415.L1EST
#define RLN2415L1DFC RLN2415.L1DFC
#define RLN2415L1IDB RLN2415.L1IDB
#define RLN2415L1CBR RLN2415.L1CBR
#define RLN2415L1DBR1 RLN2415.L1DBR1
#define RLN2415L1DBR2 RLN2415.L1DBR2
#define RLN2415L1DBR3 RLN2415.L1DBR3
#define RLN2415L1DBR4 RLN2415.L1DBR4
#define RLN2415L1DBR5 RLN2415.L1DBR5
#define RLN2415L1DBR6 RLN2415.L1DBR6
#define RLN2415L1DBR7 RLN2415.L1DBR7
#define RLN2415L1DBR8 RLN2415.L1DBR8
#define RLN2416L2MD RLN2416.L2MD
#define RLN2416L2BFC RLN2416.L2BFC
#define RLN2416L2SC RLN2416.L2SC
#define RLN2416L2WUP RLN2416.L2WUP
#define RLN2416L2IE RLN2416.L2IE
#define RLN2416L2EDE RLN2416.L2EDE
#define RLN2416L2CUC RLN2416.L2CUC
#define RLN2416L2TRC RLN2416.L2TRC
#define RLN2416L2MST RLN2416.L2MST
#define RLN2416L2ST RLN2416.L2ST
#define RLN2416L2EST RLN2416.L2EST
#define RLN2416L2DFC RLN2416.L2DFC
#define RLN2416L2IDB RLN2416.L2IDB
#define RLN2416L2CBR RLN2416.L2CBR
#define RLN2416L2DBR1 RLN2416.L2DBR1
#define RLN2416L2DBR2 RLN2416.L2DBR2
#define RLN2416L2DBR3 RLN2416.L2DBR3
#define RLN2416L2DBR4 RLN2416.L2DBR4
#define RLN2416L2DBR5 RLN2416.L2DBR5
#define RLN2416L2DBR6 RLN2416.L2DBR6
#define RLN2416L2DBR7 RLN2416.L2DBR7
#define RLN2416L2DBR8 RLN2416.L2DBR8
#define RLN2417L3MD RLN2417.L3MD
#define RLN2417L3BFC RLN2417.L3BFC
#define RLN2417L3SC RLN2417.L3SC
#define RLN2417L3WUP RLN2417.L3WUP
#define RLN2417L3IE RLN2417.L3IE
#define RLN2417L3EDE RLN2417.L3EDE
#define RLN2417L3CUC RLN2417.L3CUC
#define RLN2417L3TRC RLN2417.L3TRC
#define RLN2417L3MST RLN2417.L3MST
#define RLN2417L3ST RLN2417.L3ST
#define RLN2417L3EST RLN2417.L3EST
#define RLN2417L3DFC RLN2417.L3DFC
#define RLN2417L3IDB RLN2417.L3IDB
#define RLN2417L3CBR RLN2417.L3CBR
#define RLN2417L3DBR1 RLN2417.L3DBR1
#define RLN2417L3DBR2 RLN2417.L3DBR2
#define RLN2417L3DBR3 RLN2417.L3DBR3
#define RLN2417L3DBR4 RLN2417.L3DBR4
#define RLN2417L3DBR5 RLN2417.L3DBR5
#define RLN2417L3DBR6 RLN2417.L3DBR6
#define RLN2417L3DBR7 RLN2417.L3DBR7
#define RLN2417L3DBR8 RLN2417.L3DBR8
#define RLN210GLWBR RLN210.GLWBR
#define RLN210GLBRP0 RLN210.GLBRP0
#define RLN210GLBRP1 RLN210.GLBRP1
#define RLN210GLSTC RLN210.GLSTC
#define RLN2108L0MD RLN2108.L0MD
#define RLN2108L0BFC RLN2108.L0BFC
#define RLN2108L0SC RLN2108.L0SC
#define RLN2108L0WUP RLN2108.L0WUP
#define RLN2108L0IE RLN2108.L0IE
#define RLN2108L0EDE RLN2108.L0EDE
#define RLN2108L0CUC RLN2108.L0CUC
#define RLN2108L0TRC RLN2108.L0TRC
#define RLN2108L0MST RLN2108.L0MST
#define RLN2108L0ST RLN2108.L0ST
#define RLN2108L0EST RLN2108.L0EST
#define RLN2108L0DFC RLN2108.L0DFC
#define RLN2108L0IDB RLN2108.L0IDB
#define RLN2108L0CBR RLN2108.L0CBR
#define RLN2108L0DBR1 RLN2108.L0DBR1
#define RLN2108L0DBR2 RLN2108.L0DBR2
#define RLN2108L0DBR3 RLN2108.L0DBR3
#define RLN2108L0DBR4 RLN2108.L0DBR4
#define RLN2108L0DBR5 RLN2108.L0DBR5
#define RLN2108L0DBR6 RLN2108.L0DBR6
#define RLN2108L0DBR7 RLN2108.L0DBR7
#define RLN2108L0DBR8 RLN2108.L0DBR8
#define RLN211GLWBR RLN211.GLWBR
#define RLN211GLBRP0 RLN211.GLBRP0
#define RLN211GLBRP1 RLN211.GLBRP1
#define RLN211GLSTC RLN211.GLSTC
#define RLN2119L0MD RLN2119.L0MD
#define RLN2119L0BFC RLN2119.L0BFC
#define RLN2119L0SC RLN2119.L0SC
#define RLN2119L0WUP RLN2119.L0WUP
#define RLN2119L0IE RLN2119.L0IE
#define RLN2119L0EDE RLN2119.L0EDE
#define RLN2119L0CUC RLN2119.L0CUC
#define RLN2119L0TRC RLN2119.L0TRC
#define RLN2119L0MST RLN2119.L0MST
#define RLN2119L0ST RLN2119.L0ST
#define RLN2119L0EST RLN2119.L0EST
#define RLN2119L0DFC RLN2119.L0DFC
#define RLN2119L0IDB RLN2119.L0IDB
#define RLN2119L0CBR RLN2119.L0CBR
#define RLN2119L0DBR1 RLN2119.L0DBR1
#define RLN2119L0DBR2 RLN2119.L0DBR2
#define RLN2119L0DBR3 RLN2119.L0DBR3
#define RLN2119L0DBR4 RLN2119.L0DBR4
#define RLN2119L0DBR5 RLN2119.L0DBR5
#define RLN2119L0DBR6 RLN2119.L0DBR6
#define RLN2119L0DBR7 RLN2119.L0DBR7
#define RLN2119L0DBR8 RLN2119.L0DBR8
#define RLN30LWBR RLN30.LWBR
#define RLN30LBRP01 RLN30.LBRP01.uint16_t
#define RLN30LBRP0 RLN30.LBRP01.uint8_t[L]
#define RLN30LBRP1 RLN30.LBRP01.uint8_t[H]
#define RLN30LSTC RLN30.LSTC
#define RLN30LMD RLN30.LMD
#define RLN30LBFC RLN30.LBFC
#define RLN30LSC RLN30.LSC
#define RLN30LWUP RLN30.LWUP
#define RLN30LIE RLN30.LIE
#define RLN30LEDE RLN30.LEDE
#define RLN30LCUC RLN30.LCUC
#define RLN30LTRC RLN30.LTRC
#define RLN30LMST RLN30.LMST
#define RLN30LST RLN30.LST
#define RLN30LEST RLN30.LEST
#define RLN30LDFC RLN30.LDFC
#define RLN30LIDB RLN30.LIDB
#define RLN30LCBR RLN30.LCBR
#define RLN30LUDB0 RLN30.LUDB0
#define RLN30LDBR1 RLN30.LDBR1
#define RLN30LDBR2 RLN30.LDBR2
#define RLN30LDBR3 RLN30.LDBR3
#define RLN30LDBR4 RLN30.LDBR4
#define RLN30LDBR5 RLN30.LDBR5
#define RLN30LDBR6 RLN30.LDBR6
#define RLN30LDBR7 RLN30.LDBR7
#define RLN30LDBR8 RLN30.LDBR8
#define RLN30LUOER RLN30.LUOER
#define RLN30LUOR1 RLN30.LUOR1
#define RLN30LUTDR RLN30.LUTDR.uint16_t
#define RLN30LUTDRL RLN30.LUTDR.uint8_t[L]
#define RLN30LUTDRH RLN30.LUTDR.uint8_t[H]
#define RLN30LURDR RLN30.LURDR.uint16_t
#define RLN30LURDRL RLN30.LURDR.uint8_t[L]
#define RLN30LURDRH RLN30.LURDR.uint8_t[H]
#define RLN30LUWTDR RLN30.LUWTDR.uint16_t
#define RLN30LUWTDRL RLN30.LUWTDR.uint8_t[L]
#define RLN30LUWTDRH RLN30.LUWTDR.uint8_t[H]
#define RLN31LWBR RLN31.LWBR
#define RLN31LBRP01 RLN31.LBRP01.uint16_t
#define RLN31LBRP0 RLN31.LBRP01.uint8_t[L]
#define RLN31LBRP1 RLN31.LBRP01.uint8_t[H]
#define RLN31LSTC RLN31.LSTC
#define RLN31LMD RLN31.LMD
#define RLN31LBFC RLN31.LBFC
#define RLN31LSC RLN31.LSC
#define RLN31LWUP RLN31.LWUP
#define RLN31LIE RLN31.LIE
#define RLN31LEDE RLN31.LEDE
#define RLN31LCUC RLN31.LCUC
#define RLN31LTRC RLN31.LTRC
#define RLN31LMST RLN31.LMST
#define RLN31LST RLN31.LST
#define RLN31LEST RLN31.LEST
#define RLN31LDFC RLN31.LDFC
#define RLN31LIDB RLN31.LIDB
#define RLN31LCBR RLN31.LCBR
#define RLN31LUDB0 RLN31.LUDB0
#define RLN31LDBR1 RLN31.LDBR1
#define RLN31LDBR2 RLN31.LDBR2
#define RLN31LDBR3 RLN31.LDBR3
#define RLN31LDBR4 RLN31.LDBR4
#define RLN31LDBR5 RLN31.LDBR5
#define RLN31LDBR6 RLN31.LDBR6
#define RLN31LDBR7 RLN31.LDBR7
#define RLN31LDBR8 RLN31.LDBR8
#define RLN31LUOER RLN31.LUOER
#define RLN31LUOR1 RLN31.LUOR1
#define RLN31LUTDR RLN31.LUTDR.uint16_t
#define RLN31LUTDRL RLN31.LUTDR.uint8_t[L]
#define RLN31LUTDRH RLN31.LUTDR.uint8_t[H]
#define RLN31LURDR RLN31.LURDR.uint16_t
#define RLN31LURDRL RLN31.LURDR.uint8_t[L]
#define RLN31LURDRH RLN31.LURDR.uint8_t[H]
#define RLN31LUWTDR RLN31.LUWTDR.uint16_t
#define RLN31LUWTDRL RLN31.LUWTDR.uint8_t[L]
#define RLN31LUWTDRH RLN31.LUWTDR.uint8_t[H]
#define RLN32LWBR RLN32.LWBR
#define RLN32LBRP01 RLN32.LBRP01.uint16_t
#define RLN32LBRP0 RLN32.LBRP01.uint8_t[L]
#define RLN32LBRP1 RLN32.LBRP01.uint8_t[H]
#define RLN32LSTC RLN32.LSTC
#define RLN32LMD RLN32.LMD
#define RLN32LBFC RLN32.LBFC
#define RLN32LSC RLN32.LSC
#define RLN32LWUP RLN32.LWUP
#define RLN32LIE RLN32.LIE
#define RLN32LEDE RLN32.LEDE
#define RLN32LCUC RLN32.LCUC
#define RLN32LTRC RLN32.LTRC
#define RLN32LMST RLN32.LMST
#define RLN32LST RLN32.LST
#define RLN32LEST RLN32.LEST
#define RLN32LDFC RLN32.LDFC
#define RLN32LIDB RLN32.LIDB
#define RLN32LCBR RLN32.LCBR
#define RLN32LUDB0 RLN32.LUDB0
#define RLN32LDBR1 RLN32.LDBR1
#define RLN32LDBR2 RLN32.LDBR2
#define RLN32LDBR3 RLN32.LDBR3
#define RLN32LDBR4 RLN32.LDBR4
#define RLN32LDBR5 RLN32.LDBR5
#define RLN32LDBR6 RLN32.LDBR6
#define RLN32LDBR7 RLN32.LDBR7
#define RLN32LDBR8 RLN32.LDBR8
#define RLN32LUOER RLN32.LUOER
#define RLN32LUOR1 RLN32.LUOR1
#define RLN32LUTDR RLN32.LUTDR.uint16_t
#define RLN32LUTDRL RLN32.LUTDR.uint8_t[L]
#define RLN32LUTDRH RLN32.LUTDR.uint8_t[H]
#define RLN32LURDR RLN32.LURDR.uint16_t
#define RLN32LURDRL RLN32.LURDR.uint8_t[L]
#define RLN32LURDRH RLN32.LURDR.uint8_t[H]
#define RLN32LUWTDR RLN32.LUWTDR.uint16_t
#define RLN32LUWTDRL RLN32.LUWTDR.uint8_t[L]
#define RLN32LUWTDRH RLN32.LUWTDR.uint8_t[H]
#define RLN33LWBR RLN33.LWBR
#define RLN33LBRP01 RLN33.LBRP01.uint16_t
#define RLN33LBRP0 RLN33.LBRP01.uint8_t[L]
#define RLN33LBRP1 RLN33.LBRP01.uint8_t[H]
#define RLN33LSTC RLN33.LSTC
#define RLN33LMD RLN33.LMD
#define RLN33LBFC RLN33.LBFC
#define RLN33LSC RLN33.LSC
#define RLN33LWUP RLN33.LWUP
#define RLN33LIE RLN33.LIE
#define RLN33LEDE RLN33.LEDE
#define RLN33LCUC RLN33.LCUC
#define RLN33LTRC RLN33.LTRC
#define RLN33LMST RLN33.LMST
#define RLN33LST RLN33.LST
#define RLN33LEST RLN33.LEST
#define RLN33LDFC RLN33.LDFC
#define RLN33LIDB RLN33.LIDB
#define RLN33LCBR RLN33.LCBR
#define RLN33LUDB0 RLN33.LUDB0
#define RLN33LDBR1 RLN33.LDBR1
#define RLN33LDBR2 RLN33.LDBR2
#define RLN33LDBR3 RLN33.LDBR3
#define RLN33LDBR4 RLN33.LDBR4
#define RLN33LDBR5 RLN33.LDBR5
#define RLN33LDBR6 RLN33.LDBR6
#define RLN33LDBR7 RLN33.LDBR7
#define RLN33LDBR8 RLN33.LDBR8
#define RLN33LUOER RLN33.LUOER
#define RLN33LUOR1 RLN33.LUOR1
#define RLN33LUTDR RLN33.LUTDR.uint16_t
#define RLN33LUTDRL RLN33.LUTDR.uint8_t[L]
#define RLN33LUTDRH RLN33.LUTDR.uint8_t[H]
#define RLN33LURDR RLN33.LURDR.uint16_t
#define RLN33LURDRL RLN33.LURDR.uint8_t[L]
#define RLN33LURDRH RLN33.LURDR.uint8_t[H]
#define RLN33LUWTDR RLN33.LUWTDR.uint16_t
#define RLN33LUWTDRL RLN33.LUWTDR.uint8_t[L]
#define RLN33LUWTDRH RLN33.LUWTDR.uint8_t[H]
#define RLN34LWBR RLN34.LWBR
#define RLN34LBRP01 RLN34.LBRP01.uint16_t
#define RLN34LBRP0 RLN34.LBRP01.uint8_t[L]
#define RLN34LBRP1 RLN34.LBRP01.uint8_t[H]
#define RLN34LSTC RLN34.LSTC
#define RLN34LMD RLN34.LMD
#define RLN34LBFC RLN34.LBFC
#define RLN34LSC RLN34.LSC
#define RLN34LWUP RLN34.LWUP
#define RLN34LIE RLN34.LIE
#define RLN34LEDE RLN34.LEDE
#define RLN34LCUC RLN34.LCUC
#define RLN34LTRC RLN34.LTRC
#define RLN34LMST RLN34.LMST
#define RLN34LST RLN34.LST
#define RLN34LEST RLN34.LEST
#define RLN34LDFC RLN34.LDFC
#define RLN34LIDB RLN34.LIDB
#define RLN34LCBR RLN34.LCBR
#define RLN34LUDB0 RLN34.LUDB0
#define RLN34LDBR1 RLN34.LDBR1
#define RLN34LDBR2 RLN34.LDBR2
#define RLN34LDBR3 RLN34.LDBR3
#define RLN34LDBR4 RLN34.LDBR4
#define RLN34LDBR5 RLN34.LDBR5
#define RLN34LDBR6 RLN34.LDBR6
#define RLN34LDBR7 RLN34.LDBR7
#define RLN34LDBR8 RLN34.LDBR8
#define RLN34LUOER RLN34.LUOER
#define RLN34LUOR1 RLN34.LUOR1
#define RLN34LUTDR RLN34.LUTDR.uint16_t
#define RLN34LUTDRL RLN34.LUTDR.uint8_t[L]
#define RLN34LUTDRH RLN34.LUTDR.uint8_t[H]
#define RLN34LURDR RLN34.LURDR.uint16_t
#define RLN34LURDRL RLN34.LURDR.uint8_t[L]
#define RLN34LURDRH RLN34.LURDR.uint8_t[H]
#define RLN34LUWTDR RLN34.LUWTDR.uint16_t
#define RLN34LUWTDRL RLN34.LUWTDR.uint8_t[L]
#define RLN34LUWTDRH RLN34.LUWTDR.uint8_t[H]
#define RLN35LWBR RLN35.LWBR
#define RLN35LBRP01 RLN35.LBRP01.uint16_t
#define RLN35LBRP0 RLN35.LBRP01.uint8_t[L]
#define RLN35LBRP1 RLN35.LBRP01.uint8_t[H]
#define RLN35LSTC RLN35.LSTC
#define RLN35LMD RLN35.LMD
#define RLN35LBFC RLN35.LBFC
#define RLN35LSC RLN35.LSC
#define RLN35LWUP RLN35.LWUP
#define RLN35LIE RLN35.LIE
#define RLN35LEDE RLN35.LEDE
#define RLN35LCUC RLN35.LCUC
#define RLN35LTRC RLN35.LTRC
#define RLN35LMST RLN35.LMST
#define RLN35LST RLN35.LST
#define RLN35LEST RLN35.LEST
#define RLN35LDFC RLN35.LDFC
#define RLN35LIDB RLN35.LIDB
#define RLN35LCBR RLN35.LCBR
#define RLN35LUDB0 RLN35.LUDB0
#define RLN35LDBR1 RLN35.LDBR1
#define RLN35LDBR2 RLN35.LDBR2
#define RLN35LDBR3 RLN35.LDBR3
#define RLN35LDBR4 RLN35.LDBR4
#define RLN35LDBR5 RLN35.LDBR5
#define RLN35LDBR6 RLN35.LDBR6
#define RLN35LDBR7 RLN35.LDBR7
#define RLN35LDBR8 RLN35.LDBR8
#define RLN35LUOER RLN35.LUOER
#define RLN35LUOR1 RLN35.LUOR1
#define RLN35LUTDR RLN35.LUTDR.uint16_t
#define RLN35LUTDRL RLN35.LUTDR.uint8_t[L]
#define RLN35LUTDRH RLN35.LUTDR.uint8_t[H]
#define RLN35LURDR RLN35.LURDR.uint16_t
#define RLN35LURDRL RLN35.LURDR.uint8_t[L]
#define RLN35LURDRH RLN35.LURDR.uint8_t[H]
#define RLN35LUWTDR RLN35.LUWTDR.uint16_t
#define RLN35LUWTDRL RLN35.LUWTDR.uint8_t[L]
#define RLN35LUWTDRH RLN35.LUWTDR.uint8_t[H]
#define RSCAN0C0CFG RSCAN0.C0CFG.uint32_t
#define RSCAN0C0CFGL RSCAN0.C0CFG.uint16_t[L]
#define RSCAN0C0CFGLL RSCAN0.C0CFG.uint8_t[LL]
#define RSCAN0C0CFGLH RSCAN0.C0CFG.uint8_t[LH]
#define RSCAN0C0CFGH RSCAN0.C0CFG.uint16_t[H]
#define RSCAN0C0CFGHL RSCAN0.C0CFG.uint8_t[HL]
#define RSCAN0C0CFGHH RSCAN0.C0CFG.uint8_t[HH]
#define RSCAN0C0CTR RSCAN0.C0CTR.uint32_t
#define RSCAN0C0CTRL RSCAN0.C0CTR.uint16_t[L]
#define RSCAN0C0CTRLL RSCAN0.C0CTR.uint8_t[LL]
#define RSCAN0C0CTRLH RSCAN0.C0CTR.uint8_t[LH]
#define RSCAN0C0CTRH RSCAN0.C0CTR.uint16_t[H]
#define RSCAN0C0CTRHL RSCAN0.C0CTR.uint8_t[HL]
#define RSCAN0C0CTRHH RSCAN0.C0CTR.uint8_t[HH]
#define RSCAN0C0STS RSCAN0.C0STS.uint32_t
#define RSCAN0C0STSL RSCAN0.C0STS.uint16_t[L]
#define RSCAN0C0STSLL RSCAN0.C0STS.uint8_t[LL]
#define RSCAN0C0STSH RSCAN0.C0STS.uint16_t[H]
#define RSCAN0C0STSHL RSCAN0.C0STS.uint8_t[HL]
#define RSCAN0C0STSHH RSCAN0.C0STS.uint8_t[HH]
#define RSCAN0C0ERFL RSCAN0.C0ERFL.uint32_t
#define RSCAN0C0ERFLL RSCAN0.C0ERFL.uint16_t[L]
#define RSCAN0C0ERFLLL RSCAN0.C0ERFL.uint8_t[LL]
#define RSCAN0C0ERFLLH RSCAN0.C0ERFL.uint8_t[LH]
#define RSCAN0C0ERFLH RSCAN0.C0ERFL.uint16_t[H]
#define RSCAN0C0ERFLHL RSCAN0.C0ERFL.uint8_t[HL]
#define RSCAN0C0ERFLHH RSCAN0.C0ERFL.uint8_t[HH]
#define RSCAN0C1CFG RSCAN0.C1CFG.uint32_t
#define RSCAN0C1CFGL RSCAN0.C1CFG.uint16_t[L]
#define RSCAN0C1CFGLL RSCAN0.C1CFG.uint8_t[LL]
#define RSCAN0C1CFGLH RSCAN0.C1CFG.uint8_t[LH]
#define RSCAN0C1CFGH RSCAN0.C1CFG.uint16_t[H]
#define RSCAN0C1CFGHL RSCAN0.C1CFG.uint8_t[HL]
#define RSCAN0C1CFGHH RSCAN0.C1CFG.uint8_t[HH]
#define RSCAN0C1CTR RSCAN0.C1CTR.uint32_t
#define RSCAN0C1CTRL RSCAN0.C1CTR.uint16_t[L]
#define RSCAN0C1CTRLL RSCAN0.C1CTR.uint8_t[LL]
#define RSCAN0C1CTRLH RSCAN0.C1CTR.uint8_t[LH]
#define RSCAN0C1CTRH RSCAN0.C1CTR.uint16_t[H]
#define RSCAN0C1CTRHL RSCAN0.C1CTR.uint8_t[HL]
#define RSCAN0C1CTRHH RSCAN0.C1CTR.uint8_t[HH]
#define RSCAN0C1STS RSCAN0.C1STS.uint32_t
#define RSCAN0C1STSL RSCAN0.C1STS.uint16_t[L]
#define RSCAN0C1STSLL RSCAN0.C1STS.uint8_t[LL]
#define RSCAN0C1STSH RSCAN0.C1STS.uint16_t[H]
#define RSCAN0C1STSHL RSCAN0.C1STS.uint8_t[HL]
#define RSCAN0C1STSHH RSCAN0.C1STS.uint8_t[HH]
#define RSCAN0C1ERFL RSCAN0.C1ERFL.uint32_t
#define RSCAN0C1ERFLL RSCAN0.C1ERFL.uint16_t[L]
#define RSCAN0C1ERFLLL RSCAN0.C1ERFL.uint8_t[LL]
#define RSCAN0C1ERFLLH RSCAN0.C1ERFL.uint8_t[LH]
#define RSCAN0C1ERFLH RSCAN0.C1ERFL.uint16_t[H]
#define RSCAN0C1ERFLHL RSCAN0.C1ERFL.uint8_t[HL]
#define RSCAN0C1ERFLHH RSCAN0.C1ERFL.uint8_t[HH]
#define RSCAN0C2CFG RSCAN0.C2CFG.uint32_t
#define RSCAN0C2CFGL RSCAN0.C2CFG.uint16_t[L]
#define RSCAN0C2CFGLL RSCAN0.C2CFG.uint8_t[LL]
#define RSCAN0C2CFGLH RSCAN0.C2CFG.uint8_t[LH]
#define RSCAN0C2CFGH RSCAN0.C2CFG.uint16_t[H]
#define RSCAN0C2CFGHL RSCAN0.C2CFG.uint8_t[HL]
#define RSCAN0C2CFGHH RSCAN0.C2CFG.uint8_t[HH]
#define RSCAN0C2CTR RSCAN0.C2CTR.uint32_t
#define RSCAN0C2CTRL RSCAN0.C2CTR.uint16_t[L]
#define RSCAN0C2CTRLL RSCAN0.C2CTR.uint8_t[LL]
#define RSCAN0C2CTRLH RSCAN0.C2CTR.uint8_t[LH]
#define RSCAN0C2CTRH RSCAN0.C2CTR.uint16_t[H]
#define RSCAN0C2CTRHL RSCAN0.C2CTR.uint8_t[HL]
#define RSCAN0C2CTRHH RSCAN0.C2CTR.uint8_t[HH]
#define RSCAN0C2STS RSCAN0.C2STS.uint32_t
#define RSCAN0C2STSL RSCAN0.C2STS.uint16_t[L]
#define RSCAN0C2STSLL RSCAN0.C2STS.uint8_t[LL]
#define RSCAN0C2STSH RSCAN0.C2STS.uint16_t[H]
#define RSCAN0C2STSHL RSCAN0.C2STS.uint8_t[HL]
#define RSCAN0C2STSHH RSCAN0.C2STS.uint8_t[HH]
#define RSCAN0C2ERFL RSCAN0.C2ERFL.uint32_t
#define RSCAN0C2ERFLL RSCAN0.C2ERFL.uint16_t[L]
#define RSCAN0C2ERFLLL RSCAN0.C2ERFL.uint8_t[LL]
#define RSCAN0C2ERFLLH RSCAN0.C2ERFL.uint8_t[LH]
#define RSCAN0C2ERFLH RSCAN0.C2ERFL.uint16_t[H]
#define RSCAN0C2ERFLHL RSCAN0.C2ERFL.uint8_t[HL]
#define RSCAN0C2ERFLHH RSCAN0.C2ERFL.uint8_t[HH]
#define RSCAN0C3CFG RSCAN0.C3CFG.uint32_t
#define RSCAN0C3CFGL RSCAN0.C3CFG.uint16_t[L]
#define RSCAN0C3CFGLL RSCAN0.C3CFG.uint8_t[LL]
#define RSCAN0C3CFGLH RSCAN0.C3CFG.uint8_t[LH]
#define RSCAN0C3CFGH RSCAN0.C3CFG.uint16_t[H]
#define RSCAN0C3CFGHL RSCAN0.C3CFG.uint8_t[HL]
#define RSCAN0C3CFGHH RSCAN0.C3CFG.uint8_t[HH]
#define RSCAN0C3CTR RSCAN0.C3CTR.uint32_t
#define RSCAN0C3CTRL RSCAN0.C3CTR.uint16_t[L]
#define RSCAN0C3CTRLL RSCAN0.C3CTR.uint8_t[LL]
#define RSCAN0C3CTRLH RSCAN0.C3CTR.uint8_t[LH]
#define RSCAN0C3CTRH RSCAN0.C3CTR.uint16_t[H]
#define RSCAN0C3CTRHL RSCAN0.C3CTR.uint8_t[HL]
#define RSCAN0C3CTRHH RSCAN0.C3CTR.uint8_t[HH]
#define RSCAN0C3STS RSCAN0.C3STS.uint32_t
#define RSCAN0C3STSL RSCAN0.C3STS.uint16_t[L]
#define RSCAN0C3STSLL RSCAN0.C3STS.uint8_t[LL]
#define RSCAN0C3STSH RSCAN0.C3STS.uint16_t[H]
#define RSCAN0C3STSHL RSCAN0.C3STS.uint8_t[HL]
#define RSCAN0C3STSHH RSCAN0.C3STS.uint8_t[HH]
#define RSCAN0C3ERFL RSCAN0.C3ERFL.uint32_t
#define RSCAN0C3ERFLL RSCAN0.C3ERFL.uint16_t[L]
#define RSCAN0C3ERFLLL RSCAN0.C3ERFL.uint8_t[LL]
#define RSCAN0C3ERFLLH RSCAN0.C3ERFL.uint8_t[LH]
#define RSCAN0C3ERFLH RSCAN0.C3ERFL.uint16_t[H]
#define RSCAN0C3ERFLHL RSCAN0.C3ERFL.uint8_t[HL]
#define RSCAN0C3ERFLHH RSCAN0.C3ERFL.uint8_t[HH]
#define RSCAN0C4CFG RSCAN0.C4CFG.uint32_t
#define RSCAN0C4CFGL RSCAN0.C4CFG.uint16_t[L]
#define RSCAN0C4CFGLL RSCAN0.C4CFG.uint8_t[LL]
#define RSCAN0C4CFGLH RSCAN0.C4CFG.uint8_t[LH]
#define RSCAN0C4CFGH RSCAN0.C4CFG.uint16_t[H]
#define RSCAN0C4CFGHL RSCAN0.C4CFG.uint8_t[HL]
#define RSCAN0C4CFGHH RSCAN0.C4CFG.uint8_t[HH]
#define RSCAN0C4CTR RSCAN0.C4CTR.uint32_t
#define RSCAN0C4CTRL RSCAN0.C4CTR.uint16_t[L]
#define RSCAN0C4CTRLL RSCAN0.C4CTR.uint8_t[LL]
#define RSCAN0C4CTRLH RSCAN0.C4CTR.uint8_t[LH]
#define RSCAN0C4CTRH RSCAN0.C4CTR.uint16_t[H]
#define RSCAN0C4CTRHL RSCAN0.C4CTR.uint8_t[HL]
#define RSCAN0C4CTRHH RSCAN0.C4CTR.uint8_t[HH]
#define RSCAN0C4STS RSCAN0.C4STS.uint32_t
#define RSCAN0C4STSL RSCAN0.C4STS.uint16_t[L]
#define RSCAN0C4STSLL RSCAN0.C4STS.uint8_t[LL]
#define RSCAN0C4STSH RSCAN0.C4STS.uint16_t[H]
#define RSCAN0C4STSHL RSCAN0.C4STS.uint8_t[HL]
#define RSCAN0C4STSHH RSCAN0.C4STS.uint8_t[HH]
#define RSCAN0C4ERFL RSCAN0.C4ERFL.uint32_t
#define RSCAN0C4ERFLL RSCAN0.C4ERFL.uint16_t[L]
#define RSCAN0C4ERFLLL RSCAN0.C4ERFL.uint8_t[LL]
#define RSCAN0C4ERFLLH RSCAN0.C4ERFL.uint8_t[LH]
#define RSCAN0C4ERFLH RSCAN0.C4ERFL.uint16_t[H]
#define RSCAN0C4ERFLHL RSCAN0.C4ERFL.uint8_t[HL]
#define RSCAN0C4ERFLHH RSCAN0.C4ERFL.uint8_t[HH]
#define RSCAN0C5CFG RSCAN0.C5CFG.uint32_t
#define RSCAN0C5CFGL RSCAN0.C5CFG.uint16_t[L]
#define RSCAN0C5CFGLL RSCAN0.C5CFG.uint8_t[LL]
#define RSCAN0C5CFGLH RSCAN0.C5CFG.uint8_t[LH]
#define RSCAN0C5CFGH RSCAN0.C5CFG.uint16_t[H]
#define RSCAN0C5CFGHL RSCAN0.C5CFG.uint8_t[HL]
#define RSCAN0C5CFGHH RSCAN0.C5CFG.uint8_t[HH]
#define RSCAN0C5CTR RSCAN0.C5CTR.uint32_t
#define RSCAN0C5CTRL RSCAN0.C5CTR.uint16_t[L]
#define RSCAN0C5CTRLL RSCAN0.C5CTR.uint8_t[LL]
#define RSCAN0C5CTRLH RSCAN0.C5CTR.uint8_t[LH]
#define RSCAN0C5CTRH RSCAN0.C5CTR.uint16_t[H]
#define RSCAN0C5CTRHL RSCAN0.C5CTR.uint8_t[HL]
#define RSCAN0C5CTRHH RSCAN0.C5CTR.uint8_t[HH]
#define RSCAN0C5STS RSCAN0.C5STS.uint32_t
#define RSCAN0C5STSL RSCAN0.C5STS.uint16_t[L]
#define RSCAN0C5STSLL RSCAN0.C5STS.uint8_t[LL]
#define RSCAN0C5STSH RSCAN0.C5STS.uint16_t[H]
#define RSCAN0C5STSHL RSCAN0.C5STS.uint8_t[HL]
#define RSCAN0C5STSHH RSCAN0.C5STS.uint8_t[HH]
#define RSCAN0C5ERFL RSCAN0.C5ERFL.uint32_t
#define RSCAN0C5ERFLL RSCAN0.C5ERFL.uint16_t[L]
#define RSCAN0C5ERFLLL RSCAN0.C5ERFL.uint8_t[LL]
#define RSCAN0C5ERFLLH RSCAN0.C5ERFL.uint8_t[LH]
#define RSCAN0C5ERFLH RSCAN0.C5ERFL.uint16_t[H]
#define RSCAN0C5ERFLHL RSCAN0.C5ERFL.uint8_t[HL]
#define RSCAN0C5ERFLHH RSCAN0.C5ERFL.uint8_t[HH]
#define RSCAN0GCFG RSCAN0.GCFG.uint32_t
#define RSCAN0GCFGL RSCAN0.GCFG.uint16_t[L]
#define RSCAN0GCFGLL RSCAN0.GCFG.uint8_t[LL]
#define RSCAN0GCFGLH RSCAN0.GCFG.uint8_t[LH]
#define RSCAN0GCFGH RSCAN0.GCFG.uint16_t[H]
#define RSCAN0GCFGHL RSCAN0.GCFG.uint8_t[HL]
#define RSCAN0GCFGHH RSCAN0.GCFG.uint8_t[HH]
#define RSCAN0GCTR RSCAN0.GCTR.uint32_t
#define RSCAN0GCTRL RSCAN0.GCTR.uint16_t[L]
#define RSCAN0GCTRLL RSCAN0.GCTR.uint8_t[LL]
#define RSCAN0GCTRLH RSCAN0.GCTR.uint8_t[LH]
#define RSCAN0GCTRH RSCAN0.GCTR.uint16_t[H]
#define RSCAN0GCTRHL RSCAN0.GCTR.uint8_t[HL]
#define RSCAN0GSTS RSCAN0.GSTS.uint32_t
#define RSCAN0GSTSL RSCAN0.GSTS.uint16_t[L]
#define RSCAN0GSTSLL RSCAN0.GSTS.uint8_t[LL]
#define RSCAN0GERFL RSCAN0.GERFL.uint32_t
#define RSCAN0GERFLL RSCAN0.GERFL.uint16_t[L]
#define RSCAN0GERFLLL RSCAN0.GERFL.uint8_t[LL]
#define RSCAN0GTSC RSCAN0.GTSC.uint32_t
#define RSCAN0GTSCL RSCAN0.GTSC.uint16_t[L]
#define RSCAN0GAFLECTR RSCAN0.GAFLECTR.uint32_t
#define RSCAN0GAFLECTRL RSCAN0.GAFLECTR.uint16_t[L]
#define RSCAN0GAFLECTRLL RSCAN0.GAFLECTR.uint8_t[LL]
#define RSCAN0GAFLECTRLH RSCAN0.GAFLECTR.uint8_t[LH]
#define RSCAN0GAFLCFG0 RSCAN0.GAFLCFG0.uint32_t
#define RSCAN0GAFLCFG0L RSCAN0.GAFLCFG0.uint16_t[L]
#define RSCAN0GAFLCFG0LL RSCAN0.GAFLCFG0.uint8_t[LL]
#define RSCAN0GAFLCFG0LH RSCAN0.GAFLCFG0.uint8_t[LH]
#define RSCAN0GAFLCFG0H RSCAN0.GAFLCFG0.uint16_t[H]
#define RSCAN0GAFLCFG0HL RSCAN0.GAFLCFG0.uint8_t[HL]
#define RSCAN0GAFLCFG0HH RSCAN0.GAFLCFG0.uint8_t[HH]
#define RSCAN0GAFLCFG1 RSCAN0.GAFLCFG1.uint32_t
#define RSCAN0GAFLCFG1H RSCAN0.GAFLCFG1.uint16_t[H]
#define RSCAN0GAFLCFG1HL RSCAN0.GAFLCFG1.uint8_t[HL]
#define RSCAN0GAFLCFG1HH RSCAN0.GAFLCFG1.uint8_t[HH]
#define RSCAN0RMNB RSCAN0.RMNB.uint32_t
#define RSCAN0RMNBL RSCAN0.RMNB.uint16_t[L]
#define RSCAN0RMNBLL RSCAN0.RMNB.uint8_t[LL]
#define RSCAN0RMND0 RSCAN0.RMND0.uint32_t
#define RSCAN0RMND0L RSCAN0.RMND0.uint16_t[L]
#define RSCAN0RMND0LL RSCAN0.RMND0.uint8_t[LL]
#define RSCAN0RMND0LH RSCAN0.RMND0.uint8_t[LH]
#define RSCAN0RMND0H RSCAN0.RMND0.uint16_t[H]
#define RSCAN0RMND0HL RSCAN0.RMND0.uint8_t[HL]
#define RSCAN0RMND0HH RSCAN0.RMND0.uint8_t[HH]
#define RSCAN0RMND1 RSCAN0.RMND1.uint32_t
#define RSCAN0RMND1L RSCAN0.RMND1.uint16_t[L]
#define RSCAN0RMND1LL RSCAN0.RMND1.uint8_t[LL]
#define RSCAN0RMND1LH RSCAN0.RMND1.uint8_t[LH]
#define RSCAN0RMND1H RSCAN0.RMND1.uint16_t[H]
#define RSCAN0RMND1HL RSCAN0.RMND1.uint8_t[HL]
#define RSCAN0RMND1HH RSCAN0.RMND1.uint8_t[HH]
#define RSCAN0RMND2 RSCAN0.RMND2.uint32_t
#define RSCAN0RMND2L RSCAN0.RMND2.uint16_t[L]
#define RSCAN0RMND2LL RSCAN0.RMND2.uint8_t[LL]
#define RSCAN0RMND2LH RSCAN0.RMND2.uint8_t[LH]
#define RSCAN0RMND2H RSCAN0.RMND2.uint16_t[H]
#define RSCAN0RMND2HL RSCAN0.RMND2.uint8_t[HL]
#define RSCAN0RMND2HH RSCAN0.RMND2.uint8_t[HH]
#define RSCAN0RFCC0 RSCAN0.RFCC0.uint32_t
#define RSCAN0RFCC0L RSCAN0.RFCC0.uint16_t[L]
#define RSCAN0RFCC0LL RSCAN0.RFCC0.uint8_t[LL]
#define RSCAN0RFCC0LH RSCAN0.RFCC0.uint8_t[LH]
#define RSCAN0RFCC1 RSCAN0.RFCC1.uint32_t
#define RSCAN0RFCC1L RSCAN0.RFCC1.uint16_t[L]
#define RSCAN0RFCC1LL RSCAN0.RFCC1.uint8_t[LL]
#define RSCAN0RFCC1LH RSCAN0.RFCC1.uint8_t[LH]
#define RSCAN0RFCC2 RSCAN0.RFCC2.uint32_t
#define RSCAN0RFCC2L RSCAN0.RFCC2.uint16_t[L]
#define RSCAN0RFCC2LL RSCAN0.RFCC2.uint8_t[LL]
#define RSCAN0RFCC2LH RSCAN0.RFCC2.uint8_t[LH]
#define RSCAN0RFCC3 RSCAN0.RFCC3.uint32_t
#define RSCAN0RFCC3L RSCAN0.RFCC3.uint16_t[L]
#define RSCAN0RFCC3LL RSCAN0.RFCC3.uint8_t[LL]
#define RSCAN0RFCC3LH RSCAN0.RFCC3.uint8_t[LH]
#define RSCAN0RFCC4 RSCAN0.RFCC4.uint32_t
#define RSCAN0RFCC4L RSCAN0.RFCC4.uint16_t[L]
#define RSCAN0RFCC4LL RSCAN0.RFCC4.uint8_t[LL]
#define RSCAN0RFCC4LH RSCAN0.RFCC4.uint8_t[LH]
#define RSCAN0RFCC5 RSCAN0.RFCC5.uint32_t
#define RSCAN0RFCC5L RSCAN0.RFCC5.uint16_t[L]
#define RSCAN0RFCC5LL RSCAN0.RFCC5.uint8_t[LL]
#define RSCAN0RFCC5LH RSCAN0.RFCC5.uint8_t[LH]
#define RSCAN0RFCC6 RSCAN0.RFCC6.uint32_t
#define RSCAN0RFCC6L RSCAN0.RFCC6.uint16_t[L]
#define RSCAN0RFCC6LL RSCAN0.RFCC6.uint8_t[LL]
#define RSCAN0RFCC6LH RSCAN0.RFCC6.uint8_t[LH]
#define RSCAN0RFCC7 RSCAN0.RFCC7.uint32_t
#define RSCAN0RFCC7L RSCAN0.RFCC7.uint16_t[L]
#define RSCAN0RFCC7LL RSCAN0.RFCC7.uint8_t[LL]
#define RSCAN0RFCC7LH RSCAN0.RFCC7.uint8_t[LH]
#define RSCAN0RFSTS0 RSCAN0.RFSTS0.uint32_t
#define RSCAN0RFSTS0L RSCAN0.RFSTS0.uint16_t[L]
#define RSCAN0RFSTS0LL RSCAN0.RFSTS0.uint8_t[LL]
#define RSCAN0RFSTS0LH RSCAN0.RFSTS0.uint8_t[LH]
#define RSCAN0RFSTS1 RSCAN0.RFSTS1.uint32_t
#define RSCAN0RFSTS1L RSCAN0.RFSTS1.uint16_t[L]
#define RSCAN0RFSTS1LL RSCAN0.RFSTS1.uint8_t[LL]
#define RSCAN0RFSTS1LH RSCAN0.RFSTS1.uint8_t[LH]
#define RSCAN0RFSTS2 RSCAN0.RFSTS2.uint32_t
#define RSCAN0RFSTS2L RSCAN0.RFSTS2.uint16_t[L]
#define RSCAN0RFSTS2LL RSCAN0.RFSTS2.uint8_t[LL]
#define RSCAN0RFSTS2LH RSCAN0.RFSTS2.uint8_t[LH]
#define RSCAN0RFSTS3 RSCAN0.RFSTS3.uint32_t
#define RSCAN0RFSTS3L RSCAN0.RFSTS3.uint16_t[L]
#define RSCAN0RFSTS3LL RSCAN0.RFSTS3.uint8_t[LL]
#define RSCAN0RFSTS3LH RSCAN0.RFSTS3.uint8_t[LH]
#define RSCAN0RFSTS4 RSCAN0.RFSTS4.uint32_t
#define RSCAN0RFSTS4L RSCAN0.RFSTS4.uint16_t[L]
#define RSCAN0RFSTS4LL RSCAN0.RFSTS4.uint8_t[LL]
#define RSCAN0RFSTS4LH RSCAN0.RFSTS4.uint8_t[LH]
#define RSCAN0RFSTS5 RSCAN0.RFSTS5.uint32_t
#define RSCAN0RFSTS5L RSCAN0.RFSTS5.uint16_t[L]
#define RSCAN0RFSTS5LL RSCAN0.RFSTS5.uint8_t[LL]
#define RSCAN0RFSTS5LH RSCAN0.RFSTS5.uint8_t[LH]
#define RSCAN0RFSTS6 RSCAN0.RFSTS6.uint32_t
#define RSCAN0RFSTS6L RSCAN0.RFSTS6.uint16_t[L]
#define RSCAN0RFSTS6LL RSCAN0.RFSTS6.uint8_t[LL]
#define RSCAN0RFSTS6LH RSCAN0.RFSTS6.uint8_t[LH]
#define RSCAN0RFSTS7 RSCAN0.RFSTS7.uint32_t
#define RSCAN0RFSTS7L RSCAN0.RFSTS7.uint16_t[L]
#define RSCAN0RFSTS7LL RSCAN0.RFSTS7.uint8_t[LL]
#define RSCAN0RFSTS7LH RSCAN0.RFSTS7.uint8_t[LH]
#define RSCAN0RFPCTR0 RSCAN0.RFPCTR0.uint32_t
#define RSCAN0RFPCTR0L RSCAN0.RFPCTR0.uint16_t[L]
#define RSCAN0RFPCTR0LL RSCAN0.RFPCTR0.uint8_t[LL]
#define RSCAN0RFPCTR1 RSCAN0.RFPCTR1.uint32_t
#define RSCAN0RFPCTR1L RSCAN0.RFPCTR1.uint16_t[L]
#define RSCAN0RFPCTR1LL RSCAN0.RFPCTR1.uint8_t[LL]
#define RSCAN0RFPCTR2 RSCAN0.RFPCTR2.uint32_t
#define RSCAN0RFPCTR2L RSCAN0.RFPCTR2.uint16_t[L]
#define RSCAN0RFPCTR2LL RSCAN0.RFPCTR2.uint8_t[LL]
#define RSCAN0RFPCTR3 RSCAN0.RFPCTR3.uint32_t
#define RSCAN0RFPCTR3L RSCAN0.RFPCTR3.uint16_t[L]
#define RSCAN0RFPCTR3LL RSCAN0.RFPCTR3.uint8_t[LL]
#define RSCAN0RFPCTR4 RSCAN0.RFPCTR4.uint32_t
#define RSCAN0RFPCTR4L RSCAN0.RFPCTR4.uint16_t[L]
#define RSCAN0RFPCTR4LL RSCAN0.RFPCTR4.uint8_t[LL]
#define RSCAN0RFPCTR5 RSCAN0.RFPCTR5.uint32_t
#define RSCAN0RFPCTR5L RSCAN0.RFPCTR5.uint16_t[L]
#define RSCAN0RFPCTR5LL RSCAN0.RFPCTR5.uint8_t[LL]
#define RSCAN0RFPCTR6 RSCAN0.RFPCTR6.uint32_t
#define RSCAN0RFPCTR6L RSCAN0.RFPCTR6.uint16_t[L]
#define RSCAN0RFPCTR6LL RSCAN0.RFPCTR6.uint8_t[LL]
#define RSCAN0RFPCTR7 RSCAN0.RFPCTR7.uint32_t
#define RSCAN0RFPCTR7L RSCAN0.RFPCTR7.uint16_t[L]
#define RSCAN0RFPCTR7LL RSCAN0.RFPCTR7.uint8_t[LL]
#define RSCAN0CFCC0 RSCAN0.CFCC0.uint32_t
#define RSCAN0CFCC0L RSCAN0.CFCC0.uint16_t[L]
#define RSCAN0CFCC0LL RSCAN0.CFCC0.uint8_t[LL]
#define RSCAN0CFCC0LH RSCAN0.CFCC0.uint8_t[LH]
#define RSCAN0CFCC0H RSCAN0.CFCC0.uint16_t[H]
#define RSCAN0CFCC0HL RSCAN0.CFCC0.uint8_t[HL]
#define RSCAN0CFCC0HH RSCAN0.CFCC0.uint8_t[HH]
#define RSCAN0CFCC1 RSCAN0.CFCC1.uint32_t
#define RSCAN0CFCC1L RSCAN0.CFCC1.uint16_t[L]
#define RSCAN0CFCC1LL RSCAN0.CFCC1.uint8_t[LL]
#define RSCAN0CFCC1LH RSCAN0.CFCC1.uint8_t[LH]
#define RSCAN0CFCC1H RSCAN0.CFCC1.uint16_t[H]
#define RSCAN0CFCC1HL RSCAN0.CFCC1.uint8_t[HL]
#define RSCAN0CFCC1HH RSCAN0.CFCC1.uint8_t[HH]
#define RSCAN0CFCC2 RSCAN0.CFCC2.uint32_t
#define RSCAN0CFCC2L RSCAN0.CFCC2.uint16_t[L]
#define RSCAN0CFCC2LL RSCAN0.CFCC2.uint8_t[LL]
#define RSCAN0CFCC2LH RSCAN0.CFCC2.uint8_t[LH]
#define RSCAN0CFCC2H RSCAN0.CFCC2.uint16_t[H]
#define RSCAN0CFCC2HL RSCAN0.CFCC2.uint8_t[HL]
#define RSCAN0CFCC2HH RSCAN0.CFCC2.uint8_t[HH]
#define RSCAN0CFCC3 RSCAN0.CFCC3.uint32_t
#define RSCAN0CFCC3L RSCAN0.CFCC3.uint16_t[L]
#define RSCAN0CFCC3LL RSCAN0.CFCC3.uint8_t[LL]
#define RSCAN0CFCC3LH RSCAN0.CFCC3.uint8_t[LH]
#define RSCAN0CFCC3H RSCAN0.CFCC3.uint16_t[H]
#define RSCAN0CFCC3HL RSCAN0.CFCC3.uint8_t[HL]
#define RSCAN0CFCC3HH RSCAN0.CFCC3.uint8_t[HH]
#define RSCAN0CFCC4 RSCAN0.CFCC4.uint32_t
#define RSCAN0CFCC4L RSCAN0.CFCC4.uint16_t[L]
#define RSCAN0CFCC4LL RSCAN0.CFCC4.uint8_t[LL]
#define RSCAN0CFCC4LH RSCAN0.CFCC4.uint8_t[LH]
#define RSCAN0CFCC4H RSCAN0.CFCC4.uint16_t[H]
#define RSCAN0CFCC4HL RSCAN0.CFCC4.uint8_t[HL]
#define RSCAN0CFCC4HH RSCAN0.CFCC4.uint8_t[HH]
#define RSCAN0CFCC5 RSCAN0.CFCC5.uint32_t
#define RSCAN0CFCC5L RSCAN0.CFCC5.uint16_t[L]
#define RSCAN0CFCC5LL RSCAN0.CFCC5.uint8_t[LL]
#define RSCAN0CFCC5LH RSCAN0.CFCC5.uint8_t[LH]
#define RSCAN0CFCC5H RSCAN0.CFCC5.uint16_t[H]
#define RSCAN0CFCC5HL RSCAN0.CFCC5.uint8_t[HL]
#define RSCAN0CFCC5HH RSCAN0.CFCC5.uint8_t[HH]
#define RSCAN0CFCC6 RSCAN0.CFCC6.uint32_t
#define RSCAN0CFCC6L RSCAN0.CFCC6.uint16_t[L]
#define RSCAN0CFCC6LL RSCAN0.CFCC6.uint8_t[LL]
#define RSCAN0CFCC6LH RSCAN0.CFCC6.uint8_t[LH]
#define RSCAN0CFCC6H RSCAN0.CFCC6.uint16_t[H]
#define RSCAN0CFCC6HL RSCAN0.CFCC6.uint8_t[HL]
#define RSCAN0CFCC6HH RSCAN0.CFCC6.uint8_t[HH]
#define RSCAN0CFCC7 RSCAN0.CFCC7.uint32_t
#define RSCAN0CFCC7L RSCAN0.CFCC7.uint16_t[L]
#define RSCAN0CFCC7LL RSCAN0.CFCC7.uint8_t[LL]
#define RSCAN0CFCC7LH RSCAN0.CFCC7.uint8_t[LH]
#define RSCAN0CFCC7H RSCAN0.CFCC7.uint16_t[H]
#define RSCAN0CFCC7HL RSCAN0.CFCC7.uint8_t[HL]
#define RSCAN0CFCC7HH RSCAN0.CFCC7.uint8_t[HH]
#define RSCAN0CFCC8 RSCAN0.CFCC8.uint32_t
#define RSCAN0CFCC8L RSCAN0.CFCC8.uint16_t[L]
#define RSCAN0CFCC8LL RSCAN0.CFCC8.uint8_t[LL]
#define RSCAN0CFCC8LH RSCAN0.CFCC8.uint8_t[LH]
#define RSCAN0CFCC8H RSCAN0.CFCC8.uint16_t[H]
#define RSCAN0CFCC8HL RSCAN0.CFCC8.uint8_t[HL]
#define RSCAN0CFCC8HH RSCAN0.CFCC8.uint8_t[HH]
#define RSCAN0CFCC9 RSCAN0.CFCC9.uint32_t
#define RSCAN0CFCC9L RSCAN0.CFCC9.uint16_t[L]
#define RSCAN0CFCC9LL RSCAN0.CFCC9.uint8_t[LL]
#define RSCAN0CFCC9LH RSCAN0.CFCC9.uint8_t[LH]
#define RSCAN0CFCC9H RSCAN0.CFCC9.uint16_t[H]
#define RSCAN0CFCC9HL RSCAN0.CFCC9.uint8_t[HL]
#define RSCAN0CFCC9HH RSCAN0.CFCC9.uint8_t[HH]
#define RSCAN0CFCC10 RSCAN0.CFCC10.uint32_t
#define RSCAN0CFCC10L RSCAN0.CFCC10.uint16_t[L]
#define RSCAN0CFCC10LL RSCAN0.CFCC10.uint8_t[LL]
#define RSCAN0CFCC10LH RSCAN0.CFCC10.uint8_t[LH]
#define RSCAN0CFCC10H RSCAN0.CFCC10.uint16_t[H]
#define RSCAN0CFCC10HL RSCAN0.CFCC10.uint8_t[HL]
#define RSCAN0CFCC10HH RSCAN0.CFCC10.uint8_t[HH]
#define RSCAN0CFCC11 RSCAN0.CFCC11.uint32_t
#define RSCAN0CFCC11L RSCAN0.CFCC11.uint16_t[L]
#define RSCAN0CFCC11LL RSCAN0.CFCC11.uint8_t[LL]
#define RSCAN0CFCC11LH RSCAN0.CFCC11.uint8_t[LH]
#define RSCAN0CFCC11H RSCAN0.CFCC11.uint16_t[H]
#define RSCAN0CFCC11HL RSCAN0.CFCC11.uint8_t[HL]
#define RSCAN0CFCC11HH RSCAN0.CFCC11.uint8_t[HH]
#define RSCAN0CFCC12 RSCAN0.CFCC12.uint32_t
#define RSCAN0CFCC12L RSCAN0.CFCC12.uint16_t[L]
#define RSCAN0CFCC12LL RSCAN0.CFCC12.uint8_t[LL]
#define RSCAN0CFCC12LH RSCAN0.CFCC12.uint8_t[LH]
#define RSCAN0CFCC12H RSCAN0.CFCC12.uint16_t[H]
#define RSCAN0CFCC12HL RSCAN0.CFCC12.uint8_t[HL]
#define RSCAN0CFCC12HH RSCAN0.CFCC12.uint8_t[HH]
#define RSCAN0CFCC13 RSCAN0.CFCC13.uint32_t
#define RSCAN0CFCC13L RSCAN0.CFCC13.uint16_t[L]
#define RSCAN0CFCC13LL RSCAN0.CFCC13.uint8_t[LL]
#define RSCAN0CFCC13LH RSCAN0.CFCC13.uint8_t[LH]
#define RSCAN0CFCC13H RSCAN0.CFCC13.uint16_t[H]
#define RSCAN0CFCC13HL RSCAN0.CFCC13.uint8_t[HL]
#define RSCAN0CFCC13HH RSCAN0.CFCC13.uint8_t[HH]
#define RSCAN0CFCC14 RSCAN0.CFCC14.uint32_t
#define RSCAN0CFCC14L RSCAN0.CFCC14.uint16_t[L]
#define RSCAN0CFCC14LL RSCAN0.CFCC14.uint8_t[LL]
#define RSCAN0CFCC14LH RSCAN0.CFCC14.uint8_t[LH]
#define RSCAN0CFCC14H RSCAN0.CFCC14.uint16_t[H]
#define RSCAN0CFCC14HL RSCAN0.CFCC14.uint8_t[HL]
#define RSCAN0CFCC14HH RSCAN0.CFCC14.uint8_t[HH]
#define RSCAN0CFCC15 RSCAN0.CFCC15.uint32_t
#define RSCAN0CFCC15L RSCAN0.CFCC15.uint16_t[L]
#define RSCAN0CFCC15LL RSCAN0.CFCC15.uint8_t[LL]
#define RSCAN0CFCC15LH RSCAN0.CFCC15.uint8_t[LH]
#define RSCAN0CFCC15H RSCAN0.CFCC15.uint16_t[H]
#define RSCAN0CFCC15HL RSCAN0.CFCC15.uint8_t[HL]
#define RSCAN0CFCC15HH RSCAN0.CFCC15.uint8_t[HH]
#define RSCAN0CFCC16 RSCAN0.CFCC16.uint32_t
#define RSCAN0CFCC16L RSCAN0.CFCC16.uint16_t[L]
#define RSCAN0CFCC16LL RSCAN0.CFCC16.uint8_t[LL]
#define RSCAN0CFCC16LH RSCAN0.CFCC16.uint8_t[LH]
#define RSCAN0CFCC16H RSCAN0.CFCC16.uint16_t[H]
#define RSCAN0CFCC16HL RSCAN0.CFCC16.uint8_t[HL]
#define RSCAN0CFCC16HH RSCAN0.CFCC16.uint8_t[HH]
#define RSCAN0CFCC17 RSCAN0.CFCC17.uint32_t
#define RSCAN0CFCC17L RSCAN0.CFCC17.uint16_t[L]
#define RSCAN0CFCC17LL RSCAN0.CFCC17.uint8_t[LL]
#define RSCAN0CFCC17LH RSCAN0.CFCC17.uint8_t[LH]
#define RSCAN0CFCC17H RSCAN0.CFCC17.uint16_t[H]
#define RSCAN0CFCC17HL RSCAN0.CFCC17.uint8_t[HL]
#define RSCAN0CFCC17HH RSCAN0.CFCC17.uint8_t[HH]
#define RSCAN0CFSTS0 RSCAN0.CFSTS0.uint32_t
#define RSCAN0CFSTS0L RSCAN0.CFSTS0.uint16_t[L]
#define RSCAN0CFSTS0LL RSCAN0.CFSTS0.uint8_t[LL]
#define RSCAN0CFSTS0LH RSCAN0.CFSTS0.uint8_t[LH]
#define RSCAN0CFSTS1 RSCAN0.CFSTS1.uint32_t
#define RSCAN0CFSTS1L RSCAN0.CFSTS1.uint16_t[L]
#define RSCAN0CFSTS1LL RSCAN0.CFSTS1.uint8_t[LL]
#define RSCAN0CFSTS1LH RSCAN0.CFSTS1.uint8_t[LH]
#define RSCAN0CFSTS2 RSCAN0.CFSTS2.uint32_t
#define RSCAN0CFSTS2L RSCAN0.CFSTS2.uint16_t[L]
#define RSCAN0CFSTS2LL RSCAN0.CFSTS2.uint8_t[LL]
#define RSCAN0CFSTS2LH RSCAN0.CFSTS2.uint8_t[LH]
#define RSCAN0CFSTS3 RSCAN0.CFSTS3.uint32_t
#define RSCAN0CFSTS3L RSCAN0.CFSTS3.uint16_t[L]
#define RSCAN0CFSTS3LL RSCAN0.CFSTS3.uint8_t[LL]
#define RSCAN0CFSTS3LH RSCAN0.CFSTS3.uint8_t[LH]
#define RSCAN0CFSTS4 RSCAN0.CFSTS4.uint32_t
#define RSCAN0CFSTS4L RSCAN0.CFSTS4.uint16_t[L]
#define RSCAN0CFSTS4LL RSCAN0.CFSTS4.uint8_t[LL]
#define RSCAN0CFSTS4LH RSCAN0.CFSTS4.uint8_t[LH]
#define RSCAN0CFSTS5 RSCAN0.CFSTS5.uint32_t
#define RSCAN0CFSTS5L RSCAN0.CFSTS5.uint16_t[L]
#define RSCAN0CFSTS5LL RSCAN0.CFSTS5.uint8_t[LL]
#define RSCAN0CFSTS5LH RSCAN0.CFSTS5.uint8_t[LH]
#define RSCAN0CFSTS6 RSCAN0.CFSTS6.uint32_t
#define RSCAN0CFSTS6L RSCAN0.CFSTS6.uint16_t[L]
#define RSCAN0CFSTS6LL RSCAN0.CFSTS6.uint8_t[LL]
#define RSCAN0CFSTS6LH RSCAN0.CFSTS6.uint8_t[LH]
#define RSCAN0CFSTS7 RSCAN0.CFSTS7.uint32_t
#define RSCAN0CFSTS7L RSCAN0.CFSTS7.uint16_t[L]
#define RSCAN0CFSTS7LL RSCAN0.CFSTS7.uint8_t[LL]
#define RSCAN0CFSTS7LH RSCAN0.CFSTS7.uint8_t[LH]
#define RSCAN0CFSTS8 RSCAN0.CFSTS8.uint32_t
#define RSCAN0CFSTS8L RSCAN0.CFSTS8.uint16_t[L]
#define RSCAN0CFSTS8LL RSCAN0.CFSTS8.uint8_t[LL]
#define RSCAN0CFSTS8LH RSCAN0.CFSTS8.uint8_t[LH]
#define RSCAN0CFSTS9 RSCAN0.CFSTS9.uint32_t
#define RSCAN0CFSTS9L RSCAN0.CFSTS9.uint16_t[L]
#define RSCAN0CFSTS9LL RSCAN0.CFSTS9.uint8_t[LL]
#define RSCAN0CFSTS9LH RSCAN0.CFSTS9.uint8_t[LH]
#define RSCAN0CFSTS10 RSCAN0.CFSTS10.uint32_t
#define RSCAN0CFSTS10L RSCAN0.CFSTS10.uint16_t[L]
#define RSCAN0CFSTS10LL RSCAN0.CFSTS10.uint8_t[LL]
#define RSCAN0CFSTS10LH RSCAN0.CFSTS10.uint8_t[LH]
#define RSCAN0CFSTS11 RSCAN0.CFSTS11.uint32_t
#define RSCAN0CFSTS11L RSCAN0.CFSTS11.uint16_t[L]
#define RSCAN0CFSTS11LL RSCAN0.CFSTS11.uint8_t[LL]
#define RSCAN0CFSTS11LH RSCAN0.CFSTS11.uint8_t[LH]
#define RSCAN0CFSTS12 RSCAN0.CFSTS12.uint32_t
#define RSCAN0CFSTS12L RSCAN0.CFSTS12.uint16_t[L]
#define RSCAN0CFSTS12LL RSCAN0.CFSTS12.uint8_t[LL]
#define RSCAN0CFSTS12LH RSCAN0.CFSTS12.uint8_t[LH]
#define RSCAN0CFSTS13 RSCAN0.CFSTS13.uint32_t
#define RSCAN0CFSTS13L RSCAN0.CFSTS13.uint16_t[L]
#define RSCAN0CFSTS13LL RSCAN0.CFSTS13.uint8_t[LL]
#define RSCAN0CFSTS13LH RSCAN0.CFSTS13.uint8_t[LH]
#define RSCAN0CFSTS14 RSCAN0.CFSTS14.uint32_t
#define RSCAN0CFSTS14L RSCAN0.CFSTS14.uint16_t[L]
#define RSCAN0CFSTS14LL RSCAN0.CFSTS14.uint8_t[LL]
#define RSCAN0CFSTS14LH RSCAN0.CFSTS14.uint8_t[LH]
#define RSCAN0CFSTS15 RSCAN0.CFSTS15.uint32_t
#define RSCAN0CFSTS15L RSCAN0.CFSTS15.uint16_t[L]
#define RSCAN0CFSTS15LL RSCAN0.CFSTS15.uint8_t[LL]
#define RSCAN0CFSTS15LH RSCAN0.CFSTS15.uint8_t[LH]
#define RSCAN0CFSTS16 RSCAN0.CFSTS16.uint32_t
#define RSCAN0CFSTS16L RSCAN0.CFSTS16.uint16_t[L]
#define RSCAN0CFSTS16LL RSCAN0.CFSTS16.uint8_t[LL]
#define RSCAN0CFSTS16LH RSCAN0.CFSTS16.uint8_t[LH]
#define RSCAN0CFSTS17 RSCAN0.CFSTS17.uint32_t
#define RSCAN0CFSTS17L RSCAN0.CFSTS17.uint16_t[L]
#define RSCAN0CFSTS17LL RSCAN0.CFSTS17.uint8_t[LL]
#define RSCAN0CFSTS17LH RSCAN0.CFSTS17.uint8_t[LH]
#define RSCAN0CFPCTR0 RSCAN0.CFPCTR0.uint32_t
#define RSCAN0CFPCTR0L RSCAN0.CFPCTR0.uint16_t[L]
#define RSCAN0CFPCTR0LL RSCAN0.CFPCTR0.uint8_t[LL]
#define RSCAN0CFPCTR1 RSCAN0.CFPCTR1.uint32_t
#define RSCAN0CFPCTR1L RSCAN0.CFPCTR1.uint16_t[L]
#define RSCAN0CFPCTR1LL RSCAN0.CFPCTR1.uint8_t[LL]
#define RSCAN0CFPCTR2 RSCAN0.CFPCTR2.uint32_t
#define RSCAN0CFPCTR2L RSCAN0.CFPCTR2.uint16_t[L]
#define RSCAN0CFPCTR2LL RSCAN0.CFPCTR2.uint8_t[LL]
#define RSCAN0CFPCTR3 RSCAN0.CFPCTR3.uint32_t
#define RSCAN0CFPCTR3L RSCAN0.CFPCTR3.uint16_t[L]
#define RSCAN0CFPCTR3LL RSCAN0.CFPCTR3.uint8_t[LL]
#define RSCAN0CFPCTR4 RSCAN0.CFPCTR4.uint32_t
#define RSCAN0CFPCTR4L RSCAN0.CFPCTR4.uint16_t[L]
#define RSCAN0CFPCTR4LL RSCAN0.CFPCTR4.uint8_t[LL]
#define RSCAN0CFPCTR5 RSCAN0.CFPCTR5.uint32_t
#define RSCAN0CFPCTR5L RSCAN0.CFPCTR5.uint16_t[L]
#define RSCAN0CFPCTR5LL RSCAN0.CFPCTR5.uint8_t[LL]
#define RSCAN0CFPCTR6 RSCAN0.CFPCTR6.uint32_t
#define RSCAN0CFPCTR6L RSCAN0.CFPCTR6.uint16_t[L]
#define RSCAN0CFPCTR6LL RSCAN0.CFPCTR6.uint8_t[LL]
#define RSCAN0CFPCTR7 RSCAN0.CFPCTR7.uint32_t
#define RSCAN0CFPCTR7L RSCAN0.CFPCTR7.uint16_t[L]
#define RSCAN0CFPCTR7LL RSCAN0.CFPCTR7.uint8_t[LL]
#define RSCAN0CFPCTR8 RSCAN0.CFPCTR8.uint32_t
#define RSCAN0CFPCTR8L RSCAN0.CFPCTR8.uint16_t[L]
#define RSCAN0CFPCTR8LL RSCAN0.CFPCTR8.uint8_t[LL]
#define RSCAN0CFPCTR9 RSCAN0.CFPCTR9.uint32_t
#define RSCAN0CFPCTR9L RSCAN0.CFPCTR9.uint16_t[L]
#define RSCAN0CFPCTR9LL RSCAN0.CFPCTR9.uint8_t[LL]
#define RSCAN0CFPCTR10 RSCAN0.CFPCTR10.uint32_t
#define RSCAN0CFPCTR10L RSCAN0.CFPCTR10.uint16_t[L]
#define RSCAN0CFPCTR10LL RSCAN0.CFPCTR10.uint8_t[LL]
#define RSCAN0CFPCTR11 RSCAN0.CFPCTR11.uint32_t
#define RSCAN0CFPCTR11L RSCAN0.CFPCTR11.uint16_t[L]
#define RSCAN0CFPCTR11LL RSCAN0.CFPCTR11.uint8_t[LL]
#define RSCAN0CFPCTR12 RSCAN0.CFPCTR12.uint32_t
#define RSCAN0CFPCTR12L RSCAN0.CFPCTR12.uint16_t[L]
#define RSCAN0CFPCTR12LL RSCAN0.CFPCTR12.uint8_t[LL]
#define RSCAN0CFPCTR13 RSCAN0.CFPCTR13.uint32_t
#define RSCAN0CFPCTR13L RSCAN0.CFPCTR13.uint16_t[L]
#define RSCAN0CFPCTR13LL RSCAN0.CFPCTR13.uint8_t[LL]
#define RSCAN0CFPCTR14 RSCAN0.CFPCTR14.uint32_t
#define RSCAN0CFPCTR14L RSCAN0.CFPCTR14.uint16_t[L]
#define RSCAN0CFPCTR14LL RSCAN0.CFPCTR14.uint8_t[LL]
#define RSCAN0CFPCTR15 RSCAN0.CFPCTR15.uint32_t
#define RSCAN0CFPCTR15L RSCAN0.CFPCTR15.uint16_t[L]
#define RSCAN0CFPCTR15LL RSCAN0.CFPCTR15.uint8_t[LL]
#define RSCAN0CFPCTR16 RSCAN0.CFPCTR16.uint32_t
#define RSCAN0CFPCTR16L RSCAN0.CFPCTR16.uint16_t[L]
#define RSCAN0CFPCTR16LL RSCAN0.CFPCTR16.uint8_t[LL]
#define RSCAN0CFPCTR17 RSCAN0.CFPCTR17.uint32_t
#define RSCAN0CFPCTR17L RSCAN0.CFPCTR17.uint16_t[L]
#define RSCAN0CFPCTR17LL RSCAN0.CFPCTR17.uint8_t[LL]
#define RSCAN0FESTS RSCAN0.FESTS.uint32_t
#define RSCAN0FESTSL RSCAN0.FESTS.uint16_t[L]
#define RSCAN0FESTSLL RSCAN0.FESTS.uint8_t[LL]
#define RSCAN0FESTSLH RSCAN0.FESTS.uint8_t[LH]
#define RSCAN0FESTSH RSCAN0.FESTS.uint16_t[H]
#define RSCAN0FESTSHL RSCAN0.FESTS.uint8_t[HL]
#define RSCAN0FESTSHH RSCAN0.FESTS.uint8_t[HH]
#define RSCAN0FFSTS RSCAN0.FFSTS.uint32_t
#define RSCAN0FFSTSL RSCAN0.FFSTS.uint16_t[L]
#define RSCAN0FFSTSLL RSCAN0.FFSTS.uint8_t[LL]
#define RSCAN0FFSTSLH RSCAN0.FFSTS.uint8_t[LH]
#define RSCAN0FFSTSH RSCAN0.FFSTS.uint16_t[H]
#define RSCAN0FFSTSHL RSCAN0.FFSTS.uint8_t[HL]
#define RSCAN0FFSTSHH RSCAN0.FFSTS.uint8_t[HH]
#define RSCAN0FMSTS RSCAN0.FMSTS.uint32_t
#define RSCAN0FMSTSL RSCAN0.FMSTS.uint16_t[L]
#define RSCAN0FMSTSLL RSCAN0.FMSTS.uint8_t[LL]
#define RSCAN0FMSTSLH RSCAN0.FMSTS.uint8_t[LH]
#define RSCAN0FMSTSH RSCAN0.FMSTS.uint16_t[H]
#define RSCAN0FMSTSHL RSCAN0.FMSTS.uint8_t[HL]
#define RSCAN0FMSTSHH RSCAN0.FMSTS.uint8_t[HH]
#define RSCAN0RFISTS RSCAN0.RFISTS.uint32_t
#define RSCAN0RFISTSL RSCAN0.RFISTS.uint16_t[L]
#define RSCAN0RFISTSLL RSCAN0.RFISTS.uint8_t[LL]
#define RSCAN0CFRISTS RSCAN0.CFRISTS.uint32_t
#define RSCAN0CFRISTSL RSCAN0.CFRISTS.uint16_t[L]
#define RSCAN0CFRISTSLL RSCAN0.CFRISTS.uint8_t[LL]
#define RSCAN0CFRISTSLH RSCAN0.CFRISTS.uint8_t[LH]
#define RSCAN0CFRISTSH RSCAN0.CFRISTS.uint16_t[H]
#define RSCAN0CFRISTSHL RSCAN0.CFRISTS.uint8_t[HL]
#define RSCAN0CFTISTS RSCAN0.CFTISTS.uint32_t
#define RSCAN0CFTISTSL RSCAN0.CFTISTS.uint16_t[L]
#define RSCAN0CFTISTSLL RSCAN0.CFTISTS.uint8_t[LL]
#define RSCAN0CFTISTSLH RSCAN0.CFTISTS.uint8_t[LH]
#define RSCAN0CFTISTSH RSCAN0.CFTISTS.uint16_t[H]
#define RSCAN0CFTISTSHL RSCAN0.CFTISTS.uint8_t[HL]
#define RSCAN0TMC0 RSCAN0.TMC0
#define RSCAN0TMC1 RSCAN0.TMC1
#define RSCAN0TMC2 RSCAN0.TMC2
#define RSCAN0TMC3 RSCAN0.TMC3
#define RSCAN0TMC4 RSCAN0.TMC4
#define RSCAN0TMC5 RSCAN0.TMC5
#define RSCAN0TMC6 RSCAN0.TMC6
#define RSCAN0TMC7 RSCAN0.TMC7
#define RSCAN0TMC8 RSCAN0.TMC8
#define RSCAN0TMC9 RSCAN0.TMC9
#define RSCAN0TMC10 RSCAN0.TMC10
#define RSCAN0TMC11 RSCAN0.TMC11
#define RSCAN0TMC12 RSCAN0.TMC12
#define RSCAN0TMC13 RSCAN0.TMC13
#define RSCAN0TMC14 RSCAN0.TMC14
#define RSCAN0TMC15 RSCAN0.TMC15
#define RSCAN0TMC16 RSCAN0.TMC16
#define RSCAN0TMC17 RSCAN0.TMC17
#define RSCAN0TMC18 RSCAN0.TMC18
#define RSCAN0TMC19 RSCAN0.TMC19
#define RSCAN0TMC20 RSCAN0.TMC20
#define RSCAN0TMC21 RSCAN0.TMC21
#define RSCAN0TMC22 RSCAN0.TMC22
#define RSCAN0TMC23 RSCAN0.TMC23
#define RSCAN0TMC24 RSCAN0.TMC24
#define RSCAN0TMC25 RSCAN0.TMC25
#define RSCAN0TMC26 RSCAN0.TMC26
#define RSCAN0TMC27 RSCAN0.TMC27
#define RSCAN0TMC28 RSCAN0.TMC28
#define RSCAN0TMC29 RSCAN0.TMC29
#define RSCAN0TMC30 RSCAN0.TMC30
#define RSCAN0TMC31 RSCAN0.TMC31
#define RSCAN0TMC32 RSCAN0.TMC32
#define RSCAN0TMC33 RSCAN0.TMC33
#define RSCAN0TMC34 RSCAN0.TMC34
#define RSCAN0TMC35 RSCAN0.TMC35
#define RSCAN0TMC36 RSCAN0.TMC36
#define RSCAN0TMC37 RSCAN0.TMC37
#define RSCAN0TMC38 RSCAN0.TMC38
#define RSCAN0TMC39 RSCAN0.TMC39
#define RSCAN0TMC40 RSCAN0.TMC40
#define RSCAN0TMC41 RSCAN0.TMC41
#define RSCAN0TMC42 RSCAN0.TMC42
#define RSCAN0TMC43 RSCAN0.TMC43
#define RSCAN0TMC44 RSCAN0.TMC44
#define RSCAN0TMC45 RSCAN0.TMC45
#define RSCAN0TMC46 RSCAN0.TMC46
#define RSCAN0TMC47 RSCAN0.TMC47
#define RSCAN0TMC48 RSCAN0.TMC48
#define RSCAN0TMC49 RSCAN0.TMC49
#define RSCAN0TMC50 RSCAN0.TMC50
#define RSCAN0TMC51 RSCAN0.TMC51
#define RSCAN0TMC52 RSCAN0.TMC52
#define RSCAN0TMC53 RSCAN0.TMC53
#define RSCAN0TMC54 RSCAN0.TMC54
#define RSCAN0TMC55 RSCAN0.TMC55
#define RSCAN0TMC56 RSCAN0.TMC56
#define RSCAN0TMC57 RSCAN0.TMC57
#define RSCAN0TMC58 RSCAN0.TMC58
#define RSCAN0TMC59 RSCAN0.TMC59
#define RSCAN0TMC60 RSCAN0.TMC60
#define RSCAN0TMC61 RSCAN0.TMC61
#define RSCAN0TMC62 RSCAN0.TMC62
#define RSCAN0TMC63 RSCAN0.TMC63
#define RSCAN0TMC64 RSCAN0.TMC64
#define RSCAN0TMC65 RSCAN0.TMC65
#define RSCAN0TMC66 RSCAN0.TMC66
#define RSCAN0TMC67 RSCAN0.TMC67
#define RSCAN0TMC68 RSCAN0.TMC68
#define RSCAN0TMC69 RSCAN0.TMC69
#define RSCAN0TMC70 RSCAN0.TMC70
#define RSCAN0TMC71 RSCAN0.TMC71
#define RSCAN0TMC72 RSCAN0.TMC72
#define RSCAN0TMC73 RSCAN0.TMC73
#define RSCAN0TMC74 RSCAN0.TMC74
#define RSCAN0TMC75 RSCAN0.TMC75
#define RSCAN0TMC76 RSCAN0.TMC76
#define RSCAN0TMC77 RSCAN0.TMC77
#define RSCAN0TMC78 RSCAN0.TMC78
#define RSCAN0TMC79 RSCAN0.TMC79
#define RSCAN0TMC80 RSCAN0.TMC80
#define RSCAN0TMC81 RSCAN0.TMC81
#define RSCAN0TMC82 RSCAN0.TMC82
#define RSCAN0TMC83 RSCAN0.TMC83
#define RSCAN0TMC84 RSCAN0.TMC84
#define RSCAN0TMC85 RSCAN0.TMC85
#define RSCAN0TMC86 RSCAN0.TMC86
#define RSCAN0TMC87 RSCAN0.TMC87
#define RSCAN0TMC88 RSCAN0.TMC88
#define RSCAN0TMC89 RSCAN0.TMC89
#define RSCAN0TMC90 RSCAN0.TMC90
#define RSCAN0TMC91 RSCAN0.TMC91
#define RSCAN0TMC92 RSCAN0.TMC92
#define RSCAN0TMC93 RSCAN0.TMC93
#define RSCAN0TMC94 RSCAN0.TMC94
#define RSCAN0TMC95 RSCAN0.TMC95
#define RSCAN0TMSTS0 RSCAN0.TMSTS0
#define RSCAN0TMSTS1 RSCAN0.TMSTS1
#define RSCAN0TMSTS2 RSCAN0.TMSTS2
#define RSCAN0TMSTS3 RSCAN0.TMSTS3
#define RSCAN0TMSTS4 RSCAN0.TMSTS4
#define RSCAN0TMSTS5 RSCAN0.TMSTS5
#define RSCAN0TMSTS6 RSCAN0.TMSTS6
#define RSCAN0TMSTS7 RSCAN0.TMSTS7
#define RSCAN0TMSTS8 RSCAN0.TMSTS8
#define RSCAN0TMSTS9 RSCAN0.TMSTS9
#define RSCAN0TMSTS10 RSCAN0.TMSTS10
#define RSCAN0TMSTS11 RSCAN0.TMSTS11
#define RSCAN0TMSTS12 RSCAN0.TMSTS12
#define RSCAN0TMSTS13 RSCAN0.TMSTS13
#define RSCAN0TMSTS14 RSCAN0.TMSTS14
#define RSCAN0TMSTS15 RSCAN0.TMSTS15
#define RSCAN0TMSTS16 RSCAN0.TMSTS16
#define RSCAN0TMSTS17 RSCAN0.TMSTS17
#define RSCAN0TMSTS18 RSCAN0.TMSTS18
#define RSCAN0TMSTS19 RSCAN0.TMSTS19
#define RSCAN0TMSTS20 RSCAN0.TMSTS20
#define RSCAN0TMSTS21 RSCAN0.TMSTS21
#define RSCAN0TMSTS22 RSCAN0.TMSTS22
#define RSCAN0TMSTS23 RSCAN0.TMSTS23
#define RSCAN0TMSTS24 RSCAN0.TMSTS24
#define RSCAN0TMSTS25 RSCAN0.TMSTS25
#define RSCAN0TMSTS26 RSCAN0.TMSTS26
#define RSCAN0TMSTS27 RSCAN0.TMSTS27
#define RSCAN0TMSTS28 RSCAN0.TMSTS28
#define RSCAN0TMSTS29 RSCAN0.TMSTS29
#define RSCAN0TMSTS30 RSCAN0.TMSTS30
#define RSCAN0TMSTS31 RSCAN0.TMSTS31
#define RSCAN0TMSTS32 RSCAN0.TMSTS32
#define RSCAN0TMSTS33 RSCAN0.TMSTS33
#define RSCAN0TMSTS34 RSCAN0.TMSTS34
#define RSCAN0TMSTS35 RSCAN0.TMSTS35
#define RSCAN0TMSTS36 RSCAN0.TMSTS36
#define RSCAN0TMSTS37 RSCAN0.TMSTS37
#define RSCAN0TMSTS38 RSCAN0.TMSTS38
#define RSCAN0TMSTS39 RSCAN0.TMSTS39
#define RSCAN0TMSTS40 RSCAN0.TMSTS40
#define RSCAN0TMSTS41 RSCAN0.TMSTS41
#define RSCAN0TMSTS42 RSCAN0.TMSTS42
#define RSCAN0TMSTS43 RSCAN0.TMSTS43
#define RSCAN0TMSTS44 RSCAN0.TMSTS44
#define RSCAN0TMSTS45 RSCAN0.TMSTS45
#define RSCAN0TMSTS46 RSCAN0.TMSTS46
#define RSCAN0TMSTS47 RSCAN0.TMSTS47
#define RSCAN0TMSTS48 RSCAN0.TMSTS48
#define RSCAN0TMSTS49 RSCAN0.TMSTS49
#define RSCAN0TMSTS50 RSCAN0.TMSTS50
#define RSCAN0TMSTS51 RSCAN0.TMSTS51
#define RSCAN0TMSTS52 RSCAN0.TMSTS52
#define RSCAN0TMSTS53 RSCAN0.TMSTS53
#define RSCAN0TMSTS54 RSCAN0.TMSTS54
#define RSCAN0TMSTS55 RSCAN0.TMSTS55
#define RSCAN0TMSTS56 RSCAN0.TMSTS56
#define RSCAN0TMSTS57 RSCAN0.TMSTS57
#define RSCAN0TMSTS58 RSCAN0.TMSTS58
#define RSCAN0TMSTS59 RSCAN0.TMSTS59
#define RSCAN0TMSTS60 RSCAN0.TMSTS60
#define RSCAN0TMSTS61 RSCAN0.TMSTS61
#define RSCAN0TMSTS62 RSCAN0.TMSTS62
#define RSCAN0TMSTS63 RSCAN0.TMSTS63
#define RSCAN0TMSTS64 RSCAN0.TMSTS64
#define RSCAN0TMSTS65 RSCAN0.TMSTS65
#define RSCAN0TMSTS66 RSCAN0.TMSTS66
#define RSCAN0TMSTS67 RSCAN0.TMSTS67
#define RSCAN0TMSTS68 RSCAN0.TMSTS68
#define RSCAN0TMSTS69 RSCAN0.TMSTS69
#define RSCAN0TMSTS70 RSCAN0.TMSTS70
#define RSCAN0TMSTS71 RSCAN0.TMSTS71
#define RSCAN0TMSTS72 RSCAN0.TMSTS72
#define RSCAN0TMSTS73 RSCAN0.TMSTS73
#define RSCAN0TMSTS74 RSCAN0.TMSTS74
#define RSCAN0TMSTS75 RSCAN0.TMSTS75
#define RSCAN0TMSTS76 RSCAN0.TMSTS76
#define RSCAN0TMSTS77 RSCAN0.TMSTS77
#define RSCAN0TMSTS78 RSCAN0.TMSTS78
#define RSCAN0TMSTS79 RSCAN0.TMSTS79
#define RSCAN0TMSTS80 RSCAN0.TMSTS80
#define RSCAN0TMSTS81 RSCAN0.TMSTS81
#define RSCAN0TMSTS82 RSCAN0.TMSTS82
#define RSCAN0TMSTS83 RSCAN0.TMSTS83
#define RSCAN0TMSTS84 RSCAN0.TMSTS84
#define RSCAN0TMSTS85 RSCAN0.TMSTS85
#define RSCAN0TMSTS86 RSCAN0.TMSTS86
#define RSCAN0TMSTS87 RSCAN0.TMSTS87
#define RSCAN0TMSTS88 RSCAN0.TMSTS88
#define RSCAN0TMSTS89 RSCAN0.TMSTS89
#define RSCAN0TMSTS90 RSCAN0.TMSTS90
#define RSCAN0TMSTS91 RSCAN0.TMSTS91
#define RSCAN0TMSTS92 RSCAN0.TMSTS92
#define RSCAN0TMSTS93 RSCAN0.TMSTS93
#define RSCAN0TMSTS94 RSCAN0.TMSTS94
#define RSCAN0TMSTS95 RSCAN0.TMSTS95
#define RSCAN0TMTRSTS0 RSCAN0.TMTRSTS0.uint32_t
#define RSCAN0TMTRSTS0L RSCAN0.TMTRSTS0.uint16_t[L]
#define RSCAN0TMTRSTS0LL RSCAN0.TMTRSTS0.uint8_t[LL]
#define RSCAN0TMTRSTS0LH RSCAN0.TMTRSTS0.uint8_t[LH]
#define RSCAN0TMTRSTS0H RSCAN0.TMTRSTS0.uint16_t[H]
#define RSCAN0TMTRSTS0HL RSCAN0.TMTRSTS0.uint8_t[HL]
#define RSCAN0TMTRSTS0HH RSCAN0.TMTRSTS0.uint8_t[HH]
#define RSCAN0TMTRSTS1 RSCAN0.TMTRSTS1.uint32_t
#define RSCAN0TMTRSTS1L RSCAN0.TMTRSTS1.uint16_t[L]
#define RSCAN0TMTRSTS1LL RSCAN0.TMTRSTS1.uint8_t[LL]
#define RSCAN0TMTRSTS1LH RSCAN0.TMTRSTS1.uint8_t[LH]
#define RSCAN0TMTRSTS1H RSCAN0.TMTRSTS1.uint16_t[H]
#define RSCAN0TMTRSTS1HL RSCAN0.TMTRSTS1.uint8_t[HL]
#define RSCAN0TMTRSTS1HH RSCAN0.TMTRSTS1.uint8_t[HH]
#define RSCAN0TMTRSTS2 RSCAN0.TMTRSTS2.uint32_t
#define RSCAN0TMTRSTS2L RSCAN0.TMTRSTS2.uint16_t[L]
#define RSCAN0TMTRSTS2LL RSCAN0.TMTRSTS2.uint8_t[LL]
#define RSCAN0TMTRSTS2LH RSCAN0.TMTRSTS2.uint8_t[LH]
#define RSCAN0TMTRSTS2H RSCAN0.TMTRSTS2.uint16_t[H]
#define RSCAN0TMTRSTS2HL RSCAN0.TMTRSTS2.uint8_t[HL]
#define RSCAN0TMTRSTS2HH RSCAN0.TMTRSTS2.uint8_t[HH]
#define RSCAN0TMTARSTS0 RSCAN0.TMTARSTS0.uint32_t
#define RSCAN0TMTARSTS0L RSCAN0.TMTARSTS0.uint16_t[L]
#define RSCAN0TMTARSTS0LL RSCAN0.TMTARSTS0.uint8_t[LL]
#define RSCAN0TMTARSTS0LH RSCAN0.TMTARSTS0.uint8_t[LH]
#define RSCAN0TMTARSTS0H RSCAN0.TMTARSTS0.uint16_t[H]
#define RSCAN0TMTARSTS0HL RSCAN0.TMTARSTS0.uint8_t[HL]
#define RSCAN0TMTARSTS0HH RSCAN0.TMTARSTS0.uint8_t[HH]
#define RSCAN0TMTARSTS1 RSCAN0.TMTARSTS1.uint32_t
#define RSCAN0TMTARSTS1L RSCAN0.TMTARSTS1.uint16_t[L]
#define RSCAN0TMTARSTS1LL RSCAN0.TMTARSTS1.uint8_t[LL]
#define RSCAN0TMTARSTS1LH RSCAN0.TMTARSTS1.uint8_t[LH]
#define RSCAN0TMTARSTS1H RSCAN0.TMTARSTS1.uint16_t[H]
#define RSCAN0TMTARSTS1HL RSCAN0.TMTARSTS1.uint8_t[HL]
#define RSCAN0TMTARSTS1HH RSCAN0.TMTARSTS1.uint8_t[HH]
#define RSCAN0TMTARSTS2 RSCAN0.TMTARSTS2.uint32_t
#define RSCAN0TMTARSTS2L RSCAN0.TMTARSTS2.uint16_t[L]
#define RSCAN0TMTARSTS2LL RSCAN0.TMTARSTS2.uint8_t[LL]
#define RSCAN0TMTARSTS2LH RSCAN0.TMTARSTS2.uint8_t[LH]
#define RSCAN0TMTARSTS2H RSCAN0.TMTARSTS2.uint16_t[H]
#define RSCAN0TMTARSTS2HL RSCAN0.TMTARSTS2.uint8_t[HL]
#define RSCAN0TMTARSTS2HH RSCAN0.TMTARSTS2.uint8_t[HH]
#define RSCAN0TMTCSTS0 RSCAN0.TMTCSTS0.uint32_t
#define RSCAN0TMTCSTS0L RSCAN0.TMTCSTS0.uint16_t[L]
#define RSCAN0TMTCSTS0LL RSCAN0.TMTCSTS0.uint8_t[LL]
#define RSCAN0TMTCSTS0LH RSCAN0.TMTCSTS0.uint8_t[LH]
#define RSCAN0TMTCSTS0H RSCAN0.TMTCSTS0.uint16_t[H]
#define RSCAN0TMTCSTS0HL RSCAN0.TMTCSTS0.uint8_t[HL]
#define RSCAN0TMTCSTS0HH RSCAN0.TMTCSTS0.uint8_t[HH]
#define RSCAN0TMTCSTS1 RSCAN0.TMTCSTS1.uint32_t
#define RSCAN0TMTCSTS1L RSCAN0.TMTCSTS1.uint16_t[L]
#define RSCAN0TMTCSTS1LL RSCAN0.TMTCSTS1.uint8_t[LL]
#define RSCAN0TMTCSTS1LH RSCAN0.TMTCSTS1.uint8_t[LH]
#define RSCAN0TMTCSTS1H RSCAN0.TMTCSTS1.uint16_t[H]
#define RSCAN0TMTCSTS1HL RSCAN0.TMTCSTS1.uint8_t[HL]
#define RSCAN0TMTCSTS1HH RSCAN0.TMTCSTS1.uint8_t[HH]
#define RSCAN0TMTCSTS2 RSCAN0.TMTCSTS2.uint32_t
#define RSCAN0TMTCSTS2L RSCAN0.TMTCSTS2.uint16_t[L]
#define RSCAN0TMTCSTS2LL RSCAN0.TMTCSTS2.uint8_t[LL]
#define RSCAN0TMTCSTS2LH RSCAN0.TMTCSTS2.uint8_t[LH]
#define RSCAN0TMTCSTS2H RSCAN0.TMTCSTS2.uint16_t[H]
#define RSCAN0TMTCSTS2HL RSCAN0.TMTCSTS2.uint8_t[HL]
#define RSCAN0TMTCSTS2HH RSCAN0.TMTCSTS2.uint8_t[HH]
#define RSCAN0TMTASTS0 RSCAN0.TMTASTS0.uint32_t
#define RSCAN0TMTASTS0L RSCAN0.TMTASTS0.uint16_t[L]
#define RSCAN0TMTASTS0LL RSCAN0.TMTASTS0.uint8_t[LL]
#define RSCAN0TMTASTS0LH RSCAN0.TMTASTS0.uint8_t[LH]
#define RSCAN0TMTASTS0H RSCAN0.TMTASTS0.uint16_t[H]
#define RSCAN0TMTASTS0HL RSCAN0.TMTASTS0.uint8_t[HL]
#define RSCAN0TMTASTS0HH RSCAN0.TMTASTS0.uint8_t[HH]
#define RSCAN0TMTASTS1 RSCAN0.TMTASTS1.uint32_t
#define RSCAN0TMTASTS1L RSCAN0.TMTASTS1.uint16_t[L]
#define RSCAN0TMTASTS1LL RSCAN0.TMTASTS1.uint8_t[LL]
#define RSCAN0TMTASTS1LH RSCAN0.TMTASTS1.uint8_t[LH]
#define RSCAN0TMTASTS1H RSCAN0.TMTASTS1.uint16_t[H]
#define RSCAN0TMTASTS1HL RSCAN0.TMTASTS1.uint8_t[HL]
#define RSCAN0TMTASTS1HH RSCAN0.TMTASTS1.uint8_t[HH]
#define RSCAN0TMTASTS2 RSCAN0.TMTASTS2.uint32_t
#define RSCAN0TMTASTS2L RSCAN0.TMTASTS2.uint16_t[L]
#define RSCAN0TMTASTS2LL RSCAN0.TMTASTS2.uint8_t[LL]
#define RSCAN0TMTASTS2LH RSCAN0.TMTASTS2.uint8_t[LH]
#define RSCAN0TMTASTS2H RSCAN0.TMTASTS2.uint16_t[H]
#define RSCAN0TMTASTS2HL RSCAN0.TMTASTS2.uint8_t[HL]
#define RSCAN0TMTASTS2HH RSCAN0.TMTASTS2.uint8_t[HH]
#define RSCAN0TMIEC0 RSCAN0.TMIEC0.uint32_t
#define RSCAN0TMIEC0L RSCAN0.TMIEC0.uint16_t[L]
#define RSCAN0TMIEC0LL RSCAN0.TMIEC0.uint8_t[LL]
#define RSCAN0TMIEC0LH RSCAN0.TMIEC0.uint8_t[LH]
#define RSCAN0TMIEC0H RSCAN0.TMIEC0.uint16_t[H]
#define RSCAN0TMIEC0HL RSCAN0.TMIEC0.uint8_t[HL]
#define RSCAN0TMIEC0HH RSCAN0.TMIEC0.uint8_t[HH]
#define RSCAN0TMIEC1 RSCAN0.TMIEC1.uint32_t
#define RSCAN0TMIEC1L RSCAN0.TMIEC1.uint16_t[L]
#define RSCAN0TMIEC1LL RSCAN0.TMIEC1.uint8_t[LL]
#define RSCAN0TMIEC1LH RSCAN0.TMIEC1.uint8_t[LH]
#define RSCAN0TMIEC1H RSCAN0.TMIEC1.uint16_t[H]
#define RSCAN0TMIEC1HL RSCAN0.TMIEC1.uint8_t[HL]
#define RSCAN0TMIEC1HH RSCAN0.TMIEC1.uint8_t[HH]
#define RSCAN0TMIEC2 RSCAN0.TMIEC2.uint32_t
#define RSCAN0TMIEC2L RSCAN0.TMIEC2.uint16_t[L]
#define RSCAN0TMIEC2LL RSCAN0.TMIEC2.uint8_t[LL]
#define RSCAN0TMIEC2LH RSCAN0.TMIEC2.uint8_t[LH]
#define RSCAN0TMIEC2H RSCAN0.TMIEC2.uint16_t[H]
#define RSCAN0TMIEC2HL RSCAN0.TMIEC2.uint8_t[HL]
#define RSCAN0TMIEC2HH RSCAN0.TMIEC2.uint8_t[HH]
#define RSCAN0TXQCC0 RSCAN0.TXQCC0.uint32_t
#define RSCAN0TXQCC0L RSCAN0.TXQCC0.uint16_t[L]
#define RSCAN0TXQCC0LL RSCAN0.TXQCC0.uint8_t[LL]
#define RSCAN0TXQCC0LH RSCAN0.TXQCC0.uint8_t[LH]
#define RSCAN0TXQCC1 RSCAN0.TXQCC1.uint32_t
#define RSCAN0TXQCC1L RSCAN0.TXQCC1.uint16_t[L]
#define RSCAN0TXQCC1LL RSCAN0.TXQCC1.uint8_t[LL]
#define RSCAN0TXQCC1LH RSCAN0.TXQCC1.uint8_t[LH]
#define RSCAN0TXQCC2 RSCAN0.TXQCC2.uint32_t
#define RSCAN0TXQCC2L RSCAN0.TXQCC2.uint16_t[L]
#define RSCAN0TXQCC2LL RSCAN0.TXQCC2.uint8_t[LL]
#define RSCAN0TXQCC2LH RSCAN0.TXQCC2.uint8_t[LH]
#define RSCAN0TXQCC3 RSCAN0.TXQCC3.uint32_t
#define RSCAN0TXQCC3L RSCAN0.TXQCC3.uint16_t[L]
#define RSCAN0TXQCC3LL RSCAN0.TXQCC3.uint8_t[LL]
#define RSCAN0TXQCC3LH RSCAN0.TXQCC3.uint8_t[LH]
#define RSCAN0TXQCC4 RSCAN0.TXQCC4.uint32_t
#define RSCAN0TXQCC4L RSCAN0.TXQCC4.uint16_t[L]
#define RSCAN0TXQCC4LL RSCAN0.TXQCC4.uint8_t[LL]
#define RSCAN0TXQCC4LH RSCAN0.TXQCC4.uint8_t[LH]
#define RSCAN0TXQCC5 RSCAN0.TXQCC5.uint32_t
#define RSCAN0TXQCC5L RSCAN0.TXQCC5.uint16_t[L]
#define RSCAN0TXQCC5LL RSCAN0.TXQCC5.uint8_t[LL]
#define RSCAN0TXQCC5LH RSCAN0.TXQCC5.uint8_t[LH]
#define RSCAN0TXQSTS0 RSCAN0.TXQSTS0.uint32_t
#define RSCAN0TXQSTS0L RSCAN0.TXQSTS0.uint16_t[L]
#define RSCAN0TXQSTS0LL RSCAN0.TXQSTS0.uint8_t[LL]
#define RSCAN0TXQSTS1 RSCAN0.TXQSTS1.uint32_t
#define RSCAN0TXQSTS1L RSCAN0.TXQSTS1.uint16_t[L]
#define RSCAN0TXQSTS1LL RSCAN0.TXQSTS1.uint8_t[LL]
#define RSCAN0TXQSTS2 RSCAN0.TXQSTS2.uint32_t
#define RSCAN0TXQSTS2L RSCAN0.TXQSTS2.uint16_t[L]
#define RSCAN0TXQSTS2LL RSCAN0.TXQSTS2.uint8_t[LL]
#define RSCAN0TXQSTS3 RSCAN0.TXQSTS3.uint32_t
#define RSCAN0TXQSTS3L RSCAN0.TXQSTS3.uint16_t[L]
#define RSCAN0TXQSTS3LL RSCAN0.TXQSTS3.uint8_t[LL]
#define RSCAN0TXQSTS4 RSCAN0.TXQSTS4.uint32_t
#define RSCAN0TXQSTS4L RSCAN0.TXQSTS4.uint16_t[L]
#define RSCAN0TXQSTS4LL RSCAN0.TXQSTS4.uint8_t[LL]
#define RSCAN0TXQSTS5 RSCAN0.TXQSTS5.uint32_t
#define RSCAN0TXQSTS5L RSCAN0.TXQSTS5.uint16_t[L]
#define RSCAN0TXQSTS5LL RSCAN0.TXQSTS5.uint8_t[LL]
#define RSCAN0TXQPCTR0 RSCAN0.TXQPCTR0.uint32_t
#define RSCAN0TXQPCTR0L RSCAN0.TXQPCTR0.uint16_t[L]
#define RSCAN0TXQPCTR0LL RSCAN0.TXQPCTR0.uint8_t[LL]
#define RSCAN0TXQPCTR1 RSCAN0.TXQPCTR1.uint32_t
#define RSCAN0TXQPCTR1L RSCAN0.TXQPCTR1.uint16_t[L]
#define RSCAN0TXQPCTR1LL RSCAN0.TXQPCTR1.uint8_t[LL]
#define RSCAN0TXQPCTR2 RSCAN0.TXQPCTR2.uint32_t
#define RSCAN0TXQPCTR2L RSCAN0.TXQPCTR2.uint16_t[L]
#define RSCAN0TXQPCTR2LL RSCAN0.TXQPCTR2.uint8_t[LL]
#define RSCAN0TXQPCTR3 RSCAN0.TXQPCTR3.uint32_t
#define RSCAN0TXQPCTR3L RSCAN0.TXQPCTR3.uint16_t[L]
#define RSCAN0TXQPCTR3LL RSCAN0.TXQPCTR3.uint8_t[LL]
#define RSCAN0TXQPCTR4 RSCAN0.TXQPCTR4.uint32_t
#define RSCAN0TXQPCTR4L RSCAN0.TXQPCTR4.uint16_t[L]
#define RSCAN0TXQPCTR4LL RSCAN0.TXQPCTR4.uint8_t[LL]
#define RSCAN0TXQPCTR5 RSCAN0.TXQPCTR5.uint32_t
#define RSCAN0TXQPCTR5L RSCAN0.TXQPCTR5.uint16_t[L]
#define RSCAN0TXQPCTR5LL RSCAN0.TXQPCTR5.uint8_t[LL]
#define RSCAN0THLCC0 RSCAN0.THLCC0.uint32_t
#define RSCAN0THLCC0L RSCAN0.THLCC0.uint16_t[L]
#define RSCAN0THLCC0LL RSCAN0.THLCC0.uint8_t[LL]
#define RSCAN0THLCC0LH RSCAN0.THLCC0.uint8_t[LH]
#define RSCAN0THLCC1 RSCAN0.THLCC1.uint32_t
#define RSCAN0THLCC1L RSCAN0.THLCC1.uint16_t[L]
#define RSCAN0THLCC1LL RSCAN0.THLCC1.uint8_t[LL]
#define RSCAN0THLCC1LH RSCAN0.THLCC1.uint8_t[LH]
#define RSCAN0THLCC2 RSCAN0.THLCC2.uint32_t
#define RSCAN0THLCC2L RSCAN0.THLCC2.uint16_t[L]
#define RSCAN0THLCC2LL RSCAN0.THLCC2.uint8_t[LL]
#define RSCAN0THLCC2LH RSCAN0.THLCC2.uint8_t[LH]
#define RSCAN0THLCC3 RSCAN0.THLCC3.uint32_t
#define RSCAN0THLCC3L RSCAN0.THLCC3.uint16_t[L]
#define RSCAN0THLCC3LL RSCAN0.THLCC3.uint8_t[LL]
#define RSCAN0THLCC3LH RSCAN0.THLCC3.uint8_t[LH]
#define RSCAN0THLCC4 RSCAN0.THLCC4.uint32_t
#define RSCAN0THLCC4L RSCAN0.THLCC4.uint16_t[L]
#define RSCAN0THLCC4LL RSCAN0.THLCC4.uint8_t[LL]
#define RSCAN0THLCC4LH RSCAN0.THLCC4.uint8_t[LH]
#define RSCAN0THLCC5 RSCAN0.THLCC5.uint32_t
#define RSCAN0THLCC5L RSCAN0.THLCC5.uint16_t[L]
#define RSCAN0THLCC5LL RSCAN0.THLCC5.uint8_t[LL]
#define RSCAN0THLCC5LH RSCAN0.THLCC5.uint8_t[LH]
#define RSCAN0THLSTS0 RSCAN0.THLSTS0.uint32_t
#define RSCAN0THLSTS0L RSCAN0.THLSTS0.uint16_t[L]
#define RSCAN0THLSTS0LL RSCAN0.THLSTS0.uint8_t[LL]
#define RSCAN0THLSTS0LH RSCAN0.THLSTS0.uint8_t[LH]
#define RSCAN0THLSTS1 RSCAN0.THLSTS1.uint32_t
#define RSCAN0THLSTS1L RSCAN0.THLSTS1.uint16_t[L]
#define RSCAN0THLSTS1LL RSCAN0.THLSTS1.uint8_t[LL]
#define RSCAN0THLSTS1LH RSCAN0.THLSTS1.uint8_t[LH]
#define RSCAN0THLSTS2 RSCAN0.THLSTS2.uint32_t
#define RSCAN0THLSTS2L RSCAN0.THLSTS2.uint16_t[L]
#define RSCAN0THLSTS2LL RSCAN0.THLSTS2.uint8_t[LL]
#define RSCAN0THLSTS2LH RSCAN0.THLSTS2.uint8_t[LH]
#define RSCAN0THLSTS3 RSCAN0.THLSTS3.uint32_t
#define RSCAN0THLSTS3L RSCAN0.THLSTS3.uint16_t[L]
#define RSCAN0THLSTS3LL RSCAN0.THLSTS3.uint8_t[LL]
#define RSCAN0THLSTS3LH RSCAN0.THLSTS3.uint8_t[LH]
#define RSCAN0THLSTS4 RSCAN0.THLSTS4.uint32_t
#define RSCAN0THLSTS4L RSCAN0.THLSTS4.uint16_t[L]
#define RSCAN0THLSTS4LL RSCAN0.THLSTS4.uint8_t[LL]
#define RSCAN0THLSTS4LH RSCAN0.THLSTS4.uint8_t[LH]
#define RSCAN0THLSTS5 RSCAN0.THLSTS5.uint32_t
#define RSCAN0THLSTS5L RSCAN0.THLSTS5.uint16_t[L]
#define RSCAN0THLSTS5LL RSCAN0.THLSTS5.uint8_t[LL]
#define RSCAN0THLSTS5LH RSCAN0.THLSTS5.uint8_t[LH]
#define RSCAN0THLPCTR0 RSCAN0.THLPCTR0.uint32_t
#define RSCAN0THLPCTR0L RSCAN0.THLPCTR0.uint16_t[L]
#define RSCAN0THLPCTR0LL RSCAN0.THLPCTR0.uint8_t[LL]
#define RSCAN0THLPCTR1 RSCAN0.THLPCTR1.uint32_t
#define RSCAN0THLPCTR1L RSCAN0.THLPCTR1.uint16_t[L]
#define RSCAN0THLPCTR1LL RSCAN0.THLPCTR1.uint8_t[LL]
#define RSCAN0THLPCTR2 RSCAN0.THLPCTR2.uint32_t
#define RSCAN0THLPCTR2L RSCAN0.THLPCTR2.uint16_t[L]
#define RSCAN0THLPCTR2LL RSCAN0.THLPCTR2.uint8_t[LL]
#define RSCAN0THLPCTR3 RSCAN0.THLPCTR3.uint32_t
#define RSCAN0THLPCTR3L RSCAN0.THLPCTR3.uint16_t[L]
#define RSCAN0THLPCTR3LL RSCAN0.THLPCTR3.uint8_t[LL]
#define RSCAN0THLPCTR4 RSCAN0.THLPCTR4.uint32_t
#define RSCAN0THLPCTR4L RSCAN0.THLPCTR4.uint16_t[L]
#define RSCAN0THLPCTR4LL RSCAN0.THLPCTR4.uint8_t[LL]
#define RSCAN0THLPCTR5 RSCAN0.THLPCTR5.uint32_t
#define RSCAN0THLPCTR5L RSCAN0.THLPCTR5.uint16_t[L]
#define RSCAN0THLPCTR5LL RSCAN0.THLPCTR5.uint8_t[LL]
#define RSCAN0GTINTSTS0 RSCAN0.GTINTSTS0.uint32_t
#define RSCAN0GTINTSTS0L RSCAN0.GTINTSTS0.uint16_t[L]
#define RSCAN0GTINTSTS0LL RSCAN0.GTINTSTS0.uint8_t[LL]
#define RSCAN0GTINTSTS0LH RSCAN0.GTINTSTS0.uint8_t[LH]
#define RSCAN0GTINTSTS0H RSCAN0.GTINTSTS0.uint16_t[H]
#define RSCAN0GTINTSTS0HL RSCAN0.GTINTSTS0.uint8_t[HL]
#define RSCAN0GTINTSTS0HH RSCAN0.GTINTSTS0.uint8_t[HH]
#define RSCAN0GTINTSTS1 RSCAN0.GTINTSTS1.uint32_t
#define RSCAN0GTINTSTS1L RSCAN0.GTINTSTS1.uint16_t[L]
#define RSCAN0GTINTSTS1LL RSCAN0.GTINTSTS1.uint8_t[LL]
#define RSCAN0GTINTSTS1LH RSCAN0.GTINTSTS1.uint8_t[LH]
#define RSCAN0GTSTCFG RSCAN0.GTSTCFG.uint32_t
#define RSCAN0GTSTCFGL RSCAN0.GTSTCFG.uint16_t[L]
#define RSCAN0GTSTCFGLL RSCAN0.GTSTCFG.uint8_t[LL]
#define RSCAN0GTSTCFGH RSCAN0.GTSTCFG.uint16_t[H]
#define RSCAN0GTSTCFGHL RSCAN0.GTSTCFG.uint8_t[HL]
#define RSCAN0GTSTCTR RSCAN0.GTSTCTR.uint32_t
#define RSCAN0GTSTCTRL RSCAN0.GTSTCTR.uint16_t[L]
#define RSCAN0GTSTCTRLL RSCAN0.GTSTCTR.uint8_t[LL]
#define RSCAN0GLOCKK RSCAN0.GLOCKK.uint32_t
#define RSCAN0GLOCKKL RSCAN0.GLOCKK.uint16_t[L]
#define RSCAN0GAFLID0 RSCAN0.GAFLID0.uint32_t
#define RSCAN0GAFLID0L RSCAN0.GAFLID0.uint16_t[L]
#define RSCAN0GAFLID0LL RSCAN0.GAFLID0.uint8_t[LL]
#define RSCAN0GAFLID0LH RSCAN0.GAFLID0.uint8_t[LH]
#define RSCAN0GAFLID0H RSCAN0.GAFLID0.uint16_t[H]
#define RSCAN0GAFLID0HL RSCAN0.GAFLID0.uint8_t[HL]
#define RSCAN0GAFLID0HH RSCAN0.GAFLID0.uint8_t[HH]
#define RSCAN0GAFLM0 RSCAN0.GAFLM0.uint32_t
#define RSCAN0GAFLM0L RSCAN0.GAFLM0.uint16_t[L]
#define RSCAN0GAFLM0LL RSCAN0.GAFLM0.uint8_t[LL]
#define RSCAN0GAFLM0LH RSCAN0.GAFLM0.uint8_t[LH]
#define RSCAN0GAFLM0H RSCAN0.GAFLM0.uint16_t[H]
#define RSCAN0GAFLM0HL RSCAN0.GAFLM0.uint8_t[HL]
#define RSCAN0GAFLM0HH RSCAN0.GAFLM0.uint8_t[HH]
#define RSCAN0GAFLP00 RSCAN0.GAFLP00.uint32_t
#define RSCAN0GAFLP00L RSCAN0.GAFLP00.uint16_t[L]
#define RSCAN0GAFLP00LH RSCAN0.GAFLP00.uint8_t[LH]
#define RSCAN0GAFLP00H RSCAN0.GAFLP00.uint16_t[H]
#define RSCAN0GAFLP00HL RSCAN0.GAFLP00.uint8_t[HL]
#define RSCAN0GAFLP00HH RSCAN0.GAFLP00.uint8_t[HH]
#define RSCAN0GAFLP10 RSCAN0.GAFLP10.uint32_t
#define RSCAN0GAFLP10L RSCAN0.GAFLP10.uint16_t[L]
#define RSCAN0GAFLP10LL RSCAN0.GAFLP10.uint8_t[LL]
#define RSCAN0GAFLP10LH RSCAN0.GAFLP10.uint8_t[LH]
#define RSCAN0GAFLP10H RSCAN0.GAFLP10.uint16_t[H]
#define RSCAN0GAFLP10HL RSCAN0.GAFLP10.uint8_t[HL]
#define RSCAN0GAFLP10HH RSCAN0.GAFLP10.uint8_t[HH]
#define RSCAN0GAFLID1 RSCAN0.GAFLID1.uint32_t
#define RSCAN0GAFLID1L RSCAN0.GAFLID1.uint16_t[L]
#define RSCAN0GAFLID1LL RSCAN0.GAFLID1.uint8_t[LL]
#define RSCAN0GAFLID1LH RSCAN0.GAFLID1.uint8_t[LH]
#define RSCAN0GAFLID1H RSCAN0.GAFLID1.uint16_t[H]
#define RSCAN0GAFLID1HL RSCAN0.GAFLID1.uint8_t[HL]
#define RSCAN0GAFLID1HH RSCAN0.GAFLID1.uint8_t[HH]
#define RSCAN0GAFLM1 RSCAN0.GAFLM1.uint32_t
#define RSCAN0GAFLM1L RSCAN0.GAFLM1.uint16_t[L]
#define RSCAN0GAFLM1LL RSCAN0.GAFLM1.uint8_t[LL]
#define RSCAN0GAFLM1LH RSCAN0.GAFLM1.uint8_t[LH]
#define RSCAN0GAFLM1H RSCAN0.GAFLM1.uint16_t[H]
#define RSCAN0GAFLM1HL RSCAN0.GAFLM1.uint8_t[HL]
#define RSCAN0GAFLM1HH RSCAN0.GAFLM1.uint8_t[HH]
#define RSCAN0GAFLP01 RSCAN0.GAFLP01.uint32_t
#define RSCAN0GAFLP01L RSCAN0.GAFLP01.uint16_t[L]
#define RSCAN0GAFLP01LH RSCAN0.GAFLP01.uint8_t[LH]
#define RSCAN0GAFLP01H RSCAN0.GAFLP01.uint16_t[H]
#define RSCAN0GAFLP01HL RSCAN0.GAFLP01.uint8_t[HL]
#define RSCAN0GAFLP01HH RSCAN0.GAFLP01.uint8_t[HH]
#define RSCAN0GAFLP11 RSCAN0.GAFLP11.uint32_t
#define RSCAN0GAFLP11L RSCAN0.GAFLP11.uint16_t[L]
#define RSCAN0GAFLP11LL RSCAN0.GAFLP11.uint8_t[LL]
#define RSCAN0GAFLP11LH RSCAN0.GAFLP11.uint8_t[LH]
#define RSCAN0GAFLP11H RSCAN0.GAFLP11.uint16_t[H]
#define RSCAN0GAFLP11HL RSCAN0.GAFLP11.uint8_t[HL]
#define RSCAN0GAFLP11HH RSCAN0.GAFLP11.uint8_t[HH]
#define RSCAN0GAFLID2 RSCAN0.GAFLID2.uint32_t
#define RSCAN0GAFLID2L RSCAN0.GAFLID2.uint16_t[L]
#define RSCAN0GAFLID2LL RSCAN0.GAFLID2.uint8_t[LL]
#define RSCAN0GAFLID2LH RSCAN0.GAFLID2.uint8_t[LH]
#define RSCAN0GAFLID2H RSCAN0.GAFLID2.uint16_t[H]
#define RSCAN0GAFLID2HL RSCAN0.GAFLID2.uint8_t[HL]
#define RSCAN0GAFLID2HH RSCAN0.GAFLID2.uint8_t[HH]
#define RSCAN0GAFLM2 RSCAN0.GAFLM2.uint32_t
#define RSCAN0GAFLM2L RSCAN0.GAFLM2.uint16_t[L]
#define RSCAN0GAFLM2LL RSCAN0.GAFLM2.uint8_t[LL]
#define RSCAN0GAFLM2LH RSCAN0.GAFLM2.uint8_t[LH]
#define RSCAN0GAFLM2H RSCAN0.GAFLM2.uint16_t[H]
#define RSCAN0GAFLM2HL RSCAN0.GAFLM2.uint8_t[HL]
#define RSCAN0GAFLM2HH RSCAN0.GAFLM2.uint8_t[HH]
#define RSCAN0GAFLP02 RSCAN0.GAFLP02.uint32_t
#define RSCAN0GAFLP02L RSCAN0.GAFLP02.uint16_t[L]
#define RSCAN0GAFLP02LH RSCAN0.GAFLP02.uint8_t[LH]
#define RSCAN0GAFLP02H RSCAN0.GAFLP02.uint16_t[H]
#define RSCAN0GAFLP02HL RSCAN0.GAFLP02.uint8_t[HL]
#define RSCAN0GAFLP02HH RSCAN0.GAFLP02.uint8_t[HH]
#define RSCAN0GAFLP12 RSCAN0.GAFLP12.uint32_t
#define RSCAN0GAFLP12L RSCAN0.GAFLP12.uint16_t[L]
#define RSCAN0GAFLP12LL RSCAN0.GAFLP12.uint8_t[LL]
#define RSCAN0GAFLP12LH RSCAN0.GAFLP12.uint8_t[LH]
#define RSCAN0GAFLP12H RSCAN0.GAFLP12.uint16_t[H]
#define RSCAN0GAFLP12HL RSCAN0.GAFLP12.uint8_t[HL]
#define RSCAN0GAFLP12HH RSCAN0.GAFLP12.uint8_t[HH]
#define RSCAN0GAFLID3 RSCAN0.GAFLID3.uint32_t
#define RSCAN0GAFLID3L RSCAN0.GAFLID3.uint16_t[L]
#define RSCAN0GAFLID3LL RSCAN0.GAFLID3.uint8_t[LL]
#define RSCAN0GAFLID3LH RSCAN0.GAFLID3.uint8_t[LH]
#define RSCAN0GAFLID3H RSCAN0.GAFLID3.uint16_t[H]
#define RSCAN0GAFLID3HL RSCAN0.GAFLID3.uint8_t[HL]
#define RSCAN0GAFLID3HH RSCAN0.GAFLID3.uint8_t[HH]
#define RSCAN0GAFLM3 RSCAN0.GAFLM3.uint32_t
#define RSCAN0GAFLM3L RSCAN0.GAFLM3.uint16_t[L]
#define RSCAN0GAFLM3LL RSCAN0.GAFLM3.uint8_t[LL]
#define RSCAN0GAFLM3LH RSCAN0.GAFLM3.uint8_t[LH]
#define RSCAN0GAFLM3H RSCAN0.GAFLM3.uint16_t[H]
#define RSCAN0GAFLM3HL RSCAN0.GAFLM3.uint8_t[HL]
#define RSCAN0GAFLM3HH RSCAN0.GAFLM3.uint8_t[HH]
#define RSCAN0GAFLP03 RSCAN0.GAFLP03.uint32_t
#define RSCAN0GAFLP03L RSCAN0.GAFLP03.uint16_t[L]
#define RSCAN0GAFLP03LH RSCAN0.GAFLP03.uint8_t[LH]
#define RSCAN0GAFLP03H RSCAN0.GAFLP03.uint16_t[H]
#define RSCAN0GAFLP03HL RSCAN0.GAFLP03.uint8_t[HL]
#define RSCAN0GAFLP03HH RSCAN0.GAFLP03.uint8_t[HH]
#define RSCAN0GAFLP13 RSCAN0.GAFLP13.uint32_t
#define RSCAN0GAFLP13L RSCAN0.GAFLP13.uint16_t[L]
#define RSCAN0GAFLP13LL RSCAN0.GAFLP13.uint8_t[LL]
#define RSCAN0GAFLP13LH RSCAN0.GAFLP13.uint8_t[LH]
#define RSCAN0GAFLP13H RSCAN0.GAFLP13.uint16_t[H]
#define RSCAN0GAFLP13HL RSCAN0.GAFLP13.uint8_t[HL]
#define RSCAN0GAFLP13HH RSCAN0.GAFLP13.uint8_t[HH]
#define RSCAN0GAFLID4 RSCAN0.GAFLID4.uint32_t
#define RSCAN0GAFLID4L RSCAN0.GAFLID4.uint16_t[L]
#define RSCAN0GAFLID4LL RSCAN0.GAFLID4.uint8_t[LL]
#define RSCAN0GAFLID4LH RSCAN0.GAFLID4.uint8_t[LH]
#define RSCAN0GAFLID4H RSCAN0.GAFLID4.uint16_t[H]
#define RSCAN0GAFLID4HL RSCAN0.GAFLID4.uint8_t[HL]
#define RSCAN0GAFLID4HH RSCAN0.GAFLID4.uint8_t[HH]
#define RSCAN0GAFLM4 RSCAN0.GAFLM4.uint32_t
#define RSCAN0GAFLM4L RSCAN0.GAFLM4.uint16_t[L]
#define RSCAN0GAFLM4LL RSCAN0.GAFLM4.uint8_t[LL]
#define RSCAN0GAFLM4LH RSCAN0.GAFLM4.uint8_t[LH]
#define RSCAN0GAFLM4H RSCAN0.GAFLM4.uint16_t[H]
#define RSCAN0GAFLM4HL RSCAN0.GAFLM4.uint8_t[HL]
#define RSCAN0GAFLM4HH RSCAN0.GAFLM4.uint8_t[HH]
#define RSCAN0GAFLP04 RSCAN0.GAFLP04.uint32_t
#define RSCAN0GAFLP04L RSCAN0.GAFLP04.uint16_t[L]
#define RSCAN0GAFLP04LH RSCAN0.GAFLP04.uint8_t[LH]
#define RSCAN0GAFLP04H RSCAN0.GAFLP04.uint16_t[H]
#define RSCAN0GAFLP04HL RSCAN0.GAFLP04.uint8_t[HL]
#define RSCAN0GAFLP04HH RSCAN0.GAFLP04.uint8_t[HH]
#define RSCAN0GAFLP14 RSCAN0.GAFLP14.uint32_t
#define RSCAN0GAFLP14L RSCAN0.GAFLP14.uint16_t[L]
#define RSCAN0GAFLP14LL RSCAN0.GAFLP14.uint8_t[LL]
#define RSCAN0GAFLP14LH RSCAN0.GAFLP14.uint8_t[LH]
#define RSCAN0GAFLP14H RSCAN0.GAFLP14.uint16_t[H]
#define RSCAN0GAFLP14HL RSCAN0.GAFLP14.uint8_t[HL]
#define RSCAN0GAFLP14HH RSCAN0.GAFLP14.uint8_t[HH]
#define RSCAN0GAFLID5 RSCAN0.GAFLID5.uint32_t
#define RSCAN0GAFLID5L RSCAN0.GAFLID5.uint16_t[L]
#define RSCAN0GAFLID5LL RSCAN0.GAFLID5.uint8_t[LL]
#define RSCAN0GAFLID5LH RSCAN0.GAFLID5.uint8_t[LH]
#define RSCAN0GAFLID5H RSCAN0.GAFLID5.uint16_t[H]
#define RSCAN0GAFLID5HL RSCAN0.GAFLID5.uint8_t[HL]
#define RSCAN0GAFLID5HH RSCAN0.GAFLID5.uint8_t[HH]
#define RSCAN0GAFLM5 RSCAN0.GAFLM5.uint32_t
#define RSCAN0GAFLM5L RSCAN0.GAFLM5.uint16_t[L]
#define RSCAN0GAFLM5LL RSCAN0.GAFLM5.uint8_t[LL]
#define RSCAN0GAFLM5LH RSCAN0.GAFLM5.uint8_t[LH]
#define RSCAN0GAFLM5H RSCAN0.GAFLM5.uint16_t[H]
#define RSCAN0GAFLM5HL RSCAN0.GAFLM5.uint8_t[HL]
#define RSCAN0GAFLM5HH RSCAN0.GAFLM5.uint8_t[HH]
#define RSCAN0GAFLP05 RSCAN0.GAFLP05.uint32_t
#define RSCAN0GAFLP05L RSCAN0.GAFLP05.uint16_t[L]
#define RSCAN0GAFLP05LH RSCAN0.GAFLP05.uint8_t[LH]
#define RSCAN0GAFLP05H RSCAN0.GAFLP05.uint16_t[H]
#define RSCAN0GAFLP05HL RSCAN0.GAFLP05.uint8_t[HL]
#define RSCAN0GAFLP05HH RSCAN0.GAFLP05.uint8_t[HH]
#define RSCAN0GAFLP15 RSCAN0.GAFLP15.uint32_t
#define RSCAN0GAFLP15L RSCAN0.GAFLP15.uint16_t[L]
#define RSCAN0GAFLP15LL RSCAN0.GAFLP15.uint8_t[LL]
#define RSCAN0GAFLP15LH RSCAN0.GAFLP15.uint8_t[LH]
#define RSCAN0GAFLP15H RSCAN0.GAFLP15.uint16_t[H]
#define RSCAN0GAFLP15HL RSCAN0.GAFLP15.uint8_t[HL]
#define RSCAN0GAFLP15HH RSCAN0.GAFLP15.uint8_t[HH]
#define RSCAN0GAFLID6 RSCAN0.GAFLID6.uint32_t
#define RSCAN0GAFLID6L RSCAN0.GAFLID6.uint16_t[L]
#define RSCAN0GAFLID6LL RSCAN0.GAFLID6.uint8_t[LL]
#define RSCAN0GAFLID6LH RSCAN0.GAFLID6.uint8_t[LH]
#define RSCAN0GAFLID6H RSCAN0.GAFLID6.uint16_t[H]
#define RSCAN0GAFLID6HL RSCAN0.GAFLID6.uint8_t[HL]
#define RSCAN0GAFLID6HH RSCAN0.GAFLID6.uint8_t[HH]
#define RSCAN0GAFLM6 RSCAN0.GAFLM6.uint32_t
#define RSCAN0GAFLM6L RSCAN0.GAFLM6.uint16_t[L]
#define RSCAN0GAFLM6LL RSCAN0.GAFLM6.uint8_t[LL]
#define RSCAN0GAFLM6LH RSCAN0.GAFLM6.uint8_t[LH]
#define RSCAN0GAFLM6H RSCAN0.GAFLM6.uint16_t[H]
#define RSCAN0GAFLM6HL RSCAN0.GAFLM6.uint8_t[HL]
#define RSCAN0GAFLM6HH RSCAN0.GAFLM6.uint8_t[HH]
#define RSCAN0GAFLP06 RSCAN0.GAFLP06.uint32_t
#define RSCAN0GAFLP06L RSCAN0.GAFLP06.uint16_t[L]
#define RSCAN0GAFLP06LH RSCAN0.GAFLP06.uint8_t[LH]
#define RSCAN0GAFLP06H RSCAN0.GAFLP06.uint16_t[H]
#define RSCAN0GAFLP06HL RSCAN0.GAFLP06.uint8_t[HL]
#define RSCAN0GAFLP06HH RSCAN0.GAFLP06.uint8_t[HH]
#define RSCAN0GAFLP16 RSCAN0.GAFLP16.uint32_t
#define RSCAN0GAFLP16L RSCAN0.GAFLP16.uint16_t[L]
#define RSCAN0GAFLP16LL RSCAN0.GAFLP16.uint8_t[LL]
#define RSCAN0GAFLP16LH RSCAN0.GAFLP16.uint8_t[LH]
#define RSCAN0GAFLP16H RSCAN0.GAFLP16.uint16_t[H]
#define RSCAN0GAFLP16HL RSCAN0.GAFLP16.uint8_t[HL]
#define RSCAN0GAFLP16HH RSCAN0.GAFLP16.uint8_t[HH]
#define RSCAN0GAFLID7 RSCAN0.GAFLID7.uint32_t
#define RSCAN0GAFLID7L RSCAN0.GAFLID7.uint16_t[L]
#define RSCAN0GAFLID7LL RSCAN0.GAFLID7.uint8_t[LL]
#define RSCAN0GAFLID7LH RSCAN0.GAFLID7.uint8_t[LH]
#define RSCAN0GAFLID7H RSCAN0.GAFLID7.uint16_t[H]
#define RSCAN0GAFLID7HL RSCAN0.GAFLID7.uint8_t[HL]
#define RSCAN0GAFLID7HH RSCAN0.GAFLID7.uint8_t[HH]
#define RSCAN0GAFLM7 RSCAN0.GAFLM7.uint32_t
#define RSCAN0GAFLM7L RSCAN0.GAFLM7.uint16_t[L]
#define RSCAN0GAFLM7LL RSCAN0.GAFLM7.uint8_t[LL]
#define RSCAN0GAFLM7LH RSCAN0.GAFLM7.uint8_t[LH]
#define RSCAN0GAFLM7H RSCAN0.GAFLM7.uint16_t[H]
#define RSCAN0GAFLM7HL RSCAN0.GAFLM7.uint8_t[HL]
#define RSCAN0GAFLM7HH RSCAN0.GAFLM7.uint8_t[HH]
#define RSCAN0GAFLP07 RSCAN0.GAFLP07.uint32_t
#define RSCAN0GAFLP07L RSCAN0.GAFLP07.uint16_t[L]
#define RSCAN0GAFLP07LH RSCAN0.GAFLP07.uint8_t[LH]
#define RSCAN0GAFLP07H RSCAN0.GAFLP07.uint16_t[H]
#define RSCAN0GAFLP07HL RSCAN0.GAFLP07.uint8_t[HL]
#define RSCAN0GAFLP07HH RSCAN0.GAFLP07.uint8_t[HH]
#define RSCAN0GAFLP17 RSCAN0.GAFLP17.uint32_t
#define RSCAN0GAFLP17L RSCAN0.GAFLP17.uint16_t[L]
#define RSCAN0GAFLP17LL RSCAN0.GAFLP17.uint8_t[LL]
#define RSCAN0GAFLP17LH RSCAN0.GAFLP17.uint8_t[LH]
#define RSCAN0GAFLP17H RSCAN0.GAFLP17.uint16_t[H]
#define RSCAN0GAFLP17HL RSCAN0.GAFLP17.uint8_t[HL]
#define RSCAN0GAFLP17HH RSCAN0.GAFLP17.uint8_t[HH]
#define RSCAN0GAFLID8 RSCAN0.GAFLID8.uint32_t
#define RSCAN0GAFLID8L RSCAN0.GAFLID8.uint16_t[L]
#define RSCAN0GAFLID8LL RSCAN0.GAFLID8.uint8_t[LL]
#define RSCAN0GAFLID8LH RSCAN0.GAFLID8.uint8_t[LH]
#define RSCAN0GAFLID8H RSCAN0.GAFLID8.uint16_t[H]
#define RSCAN0GAFLID8HL RSCAN0.GAFLID8.uint8_t[HL]
#define RSCAN0GAFLID8HH RSCAN0.GAFLID8.uint8_t[HH]
#define RSCAN0GAFLM8 RSCAN0.GAFLM8.uint32_t
#define RSCAN0GAFLM8L RSCAN0.GAFLM8.uint16_t[L]
#define RSCAN0GAFLM8LL RSCAN0.GAFLM8.uint8_t[LL]
#define RSCAN0GAFLM8LH RSCAN0.GAFLM8.uint8_t[LH]
#define RSCAN0GAFLM8H RSCAN0.GAFLM8.uint16_t[H]
#define RSCAN0GAFLM8HL RSCAN0.GAFLM8.uint8_t[HL]
#define RSCAN0GAFLM8HH RSCAN0.GAFLM8.uint8_t[HH]
#define RSCAN0GAFLP08 RSCAN0.GAFLP08.uint32_t
#define RSCAN0GAFLP08L RSCAN0.GAFLP08.uint16_t[L]
#define RSCAN0GAFLP08LH RSCAN0.GAFLP08.uint8_t[LH]
#define RSCAN0GAFLP08H RSCAN0.GAFLP08.uint16_t[H]
#define RSCAN0GAFLP08HL RSCAN0.GAFLP08.uint8_t[HL]
#define RSCAN0GAFLP08HH RSCAN0.GAFLP08.uint8_t[HH]
#define RSCAN0GAFLP18 RSCAN0.GAFLP18.uint32_t
#define RSCAN0GAFLP18L RSCAN0.GAFLP18.uint16_t[L]
#define RSCAN0GAFLP18LL RSCAN0.GAFLP18.uint8_t[LL]
#define RSCAN0GAFLP18LH RSCAN0.GAFLP18.uint8_t[LH]
#define RSCAN0GAFLP18H RSCAN0.GAFLP18.uint16_t[H]
#define RSCAN0GAFLP18HL RSCAN0.GAFLP18.uint8_t[HL]
#define RSCAN0GAFLP18HH RSCAN0.GAFLP18.uint8_t[HH]
#define RSCAN0GAFLID9 RSCAN0.GAFLID9.uint32_t
#define RSCAN0GAFLID9L RSCAN0.GAFLID9.uint16_t[L]
#define RSCAN0GAFLID9LL RSCAN0.GAFLID9.uint8_t[LL]
#define RSCAN0GAFLID9LH RSCAN0.GAFLID9.uint8_t[LH]
#define RSCAN0GAFLID9H RSCAN0.GAFLID9.uint16_t[H]
#define RSCAN0GAFLID9HL RSCAN0.GAFLID9.uint8_t[HL]
#define RSCAN0GAFLID9HH RSCAN0.GAFLID9.uint8_t[HH]
#define RSCAN0GAFLM9 RSCAN0.GAFLM9.uint32_t
#define RSCAN0GAFLM9L RSCAN0.GAFLM9.uint16_t[L]
#define RSCAN0GAFLM9LL RSCAN0.GAFLM9.uint8_t[LL]
#define RSCAN0GAFLM9LH RSCAN0.GAFLM9.uint8_t[LH]
#define RSCAN0GAFLM9H RSCAN0.GAFLM9.uint16_t[H]
#define RSCAN0GAFLM9HL RSCAN0.GAFLM9.uint8_t[HL]
#define RSCAN0GAFLM9HH RSCAN0.GAFLM9.uint8_t[HH]
#define RSCAN0GAFLP09 RSCAN0.GAFLP09.uint32_t
#define RSCAN0GAFLP09L RSCAN0.GAFLP09.uint16_t[L]
#define RSCAN0GAFLP09LH RSCAN0.GAFLP09.uint8_t[LH]
#define RSCAN0GAFLP09H RSCAN0.GAFLP09.uint16_t[H]
#define RSCAN0GAFLP09HL RSCAN0.GAFLP09.uint8_t[HL]
#define RSCAN0GAFLP09HH RSCAN0.GAFLP09.uint8_t[HH]
#define RSCAN0GAFLP19 RSCAN0.GAFLP19.uint32_t
#define RSCAN0GAFLP19L RSCAN0.GAFLP19.uint16_t[L]
#define RSCAN0GAFLP19LL RSCAN0.GAFLP19.uint8_t[LL]
#define RSCAN0GAFLP19LH RSCAN0.GAFLP19.uint8_t[LH]
#define RSCAN0GAFLP19H RSCAN0.GAFLP19.uint16_t[H]
#define RSCAN0GAFLP19HL RSCAN0.GAFLP19.uint8_t[HL]
#define RSCAN0GAFLP19HH RSCAN0.GAFLP19.uint8_t[HH]
#define RSCAN0GAFLID10 RSCAN0.GAFLID10.uint32_t
#define RSCAN0GAFLID10L RSCAN0.GAFLID10.uint16_t[L]
#define RSCAN0GAFLID10LL RSCAN0.GAFLID10.uint8_t[LL]
#define RSCAN0GAFLID10LH RSCAN0.GAFLID10.uint8_t[LH]
#define RSCAN0GAFLID10H RSCAN0.GAFLID10.uint16_t[H]
#define RSCAN0GAFLID10HL RSCAN0.GAFLID10.uint8_t[HL]
#define RSCAN0GAFLID10HH RSCAN0.GAFLID10.uint8_t[HH]
#define RSCAN0GAFLM10 RSCAN0.GAFLM10.uint32_t
#define RSCAN0GAFLM10L RSCAN0.GAFLM10.uint16_t[L]
#define RSCAN0GAFLM10LL RSCAN0.GAFLM10.uint8_t[LL]
#define RSCAN0GAFLM10LH RSCAN0.GAFLM10.uint8_t[LH]
#define RSCAN0GAFLM10H RSCAN0.GAFLM10.uint16_t[H]
#define RSCAN0GAFLM10HL RSCAN0.GAFLM10.uint8_t[HL]
#define RSCAN0GAFLM10HH RSCAN0.GAFLM10.uint8_t[HH]
#define RSCAN0GAFLP010 RSCAN0.GAFLP010.uint32_t
#define RSCAN0GAFLP010L RSCAN0.GAFLP010.uint16_t[L]
#define RSCAN0GAFLP010LH RSCAN0.GAFLP010.uint8_t[LH]
#define RSCAN0GAFLP010H RSCAN0.GAFLP010.uint16_t[H]
#define RSCAN0GAFLP010HL RSCAN0.GAFLP010.uint8_t[HL]
#define RSCAN0GAFLP010HH RSCAN0.GAFLP010.uint8_t[HH]
#define RSCAN0GAFLP110 RSCAN0.GAFLP110.uint32_t
#define RSCAN0GAFLP110L RSCAN0.GAFLP110.uint16_t[L]
#define RSCAN0GAFLP110LL RSCAN0.GAFLP110.uint8_t[LL]
#define RSCAN0GAFLP110LH RSCAN0.GAFLP110.uint8_t[LH]
#define RSCAN0GAFLP110H RSCAN0.GAFLP110.uint16_t[H]
#define RSCAN0GAFLP110HL RSCAN0.GAFLP110.uint8_t[HL]
#define RSCAN0GAFLP110HH RSCAN0.GAFLP110.uint8_t[HH]
#define RSCAN0GAFLID11 RSCAN0.GAFLID11.uint32_t
#define RSCAN0GAFLID11L RSCAN0.GAFLID11.uint16_t[L]
#define RSCAN0GAFLID11LL RSCAN0.GAFLID11.uint8_t[LL]
#define RSCAN0GAFLID11LH RSCAN0.GAFLID11.uint8_t[LH]
#define RSCAN0GAFLID11H RSCAN0.GAFLID11.uint16_t[H]
#define RSCAN0GAFLID11HL RSCAN0.GAFLID11.uint8_t[HL]
#define RSCAN0GAFLID11HH RSCAN0.GAFLID11.uint8_t[HH]
#define RSCAN0GAFLM11 RSCAN0.GAFLM11.uint32_t
#define RSCAN0GAFLM11L RSCAN0.GAFLM11.uint16_t[L]
#define RSCAN0GAFLM11LL RSCAN0.GAFLM11.uint8_t[LL]
#define RSCAN0GAFLM11LH RSCAN0.GAFLM11.uint8_t[LH]
#define RSCAN0GAFLM11H RSCAN0.GAFLM11.uint16_t[H]
#define RSCAN0GAFLM11HL RSCAN0.GAFLM11.uint8_t[HL]
#define RSCAN0GAFLM11HH RSCAN0.GAFLM11.uint8_t[HH]
#define RSCAN0GAFLP011 RSCAN0.GAFLP011.uint32_t
#define RSCAN0GAFLP011L RSCAN0.GAFLP011.uint16_t[L]
#define RSCAN0GAFLP011LH RSCAN0.GAFLP011.uint8_t[LH]
#define RSCAN0GAFLP011H RSCAN0.GAFLP011.uint16_t[H]
#define RSCAN0GAFLP011HL RSCAN0.GAFLP011.uint8_t[HL]
#define RSCAN0GAFLP011HH RSCAN0.GAFLP011.uint8_t[HH]
#define RSCAN0GAFLP111 RSCAN0.GAFLP111.uint32_t
#define RSCAN0GAFLP111L RSCAN0.GAFLP111.uint16_t[L]
#define RSCAN0GAFLP111LL RSCAN0.GAFLP111.uint8_t[LL]
#define RSCAN0GAFLP111LH RSCAN0.GAFLP111.uint8_t[LH]
#define RSCAN0GAFLP111H RSCAN0.GAFLP111.uint16_t[H]
#define RSCAN0GAFLP111HL RSCAN0.GAFLP111.uint8_t[HL]
#define RSCAN0GAFLP111HH RSCAN0.GAFLP111.uint8_t[HH]
#define RSCAN0GAFLID12 RSCAN0.GAFLID12.uint32_t
#define RSCAN0GAFLID12L RSCAN0.GAFLID12.uint16_t[L]
#define RSCAN0GAFLID12LL RSCAN0.GAFLID12.uint8_t[LL]
#define RSCAN0GAFLID12LH RSCAN0.GAFLID12.uint8_t[LH]
#define RSCAN0GAFLID12H RSCAN0.GAFLID12.uint16_t[H]
#define RSCAN0GAFLID12HL RSCAN0.GAFLID12.uint8_t[HL]
#define RSCAN0GAFLID12HH RSCAN0.GAFLID12.uint8_t[HH]
#define RSCAN0GAFLM12 RSCAN0.GAFLM12.uint32_t
#define RSCAN0GAFLM12L RSCAN0.GAFLM12.uint16_t[L]
#define RSCAN0GAFLM12LL RSCAN0.GAFLM12.uint8_t[LL]
#define RSCAN0GAFLM12LH RSCAN0.GAFLM12.uint8_t[LH]
#define RSCAN0GAFLM12H RSCAN0.GAFLM12.uint16_t[H]
#define RSCAN0GAFLM12HL RSCAN0.GAFLM12.uint8_t[HL]
#define RSCAN0GAFLM12HH RSCAN0.GAFLM12.uint8_t[HH]
#define RSCAN0GAFLP012 RSCAN0.GAFLP012.uint32_t
#define RSCAN0GAFLP012L RSCAN0.GAFLP012.uint16_t[L]
#define RSCAN0GAFLP012LH RSCAN0.GAFLP012.uint8_t[LH]
#define RSCAN0GAFLP012H RSCAN0.GAFLP012.uint16_t[H]
#define RSCAN0GAFLP012HL RSCAN0.GAFLP012.uint8_t[HL]
#define RSCAN0GAFLP012HH RSCAN0.GAFLP012.uint8_t[HH]
#define RSCAN0GAFLP112 RSCAN0.GAFLP112.uint32_t
#define RSCAN0GAFLP112L RSCAN0.GAFLP112.uint16_t[L]
#define RSCAN0GAFLP112LL RSCAN0.GAFLP112.uint8_t[LL]
#define RSCAN0GAFLP112LH RSCAN0.GAFLP112.uint8_t[LH]
#define RSCAN0GAFLP112H RSCAN0.GAFLP112.uint16_t[H]
#define RSCAN0GAFLP112HL RSCAN0.GAFLP112.uint8_t[HL]
#define RSCAN0GAFLP112HH RSCAN0.GAFLP112.uint8_t[HH]
#define RSCAN0GAFLID13 RSCAN0.GAFLID13.uint32_t
#define RSCAN0GAFLID13L RSCAN0.GAFLID13.uint16_t[L]
#define RSCAN0GAFLID13LL RSCAN0.GAFLID13.uint8_t[LL]
#define RSCAN0GAFLID13LH RSCAN0.GAFLID13.uint8_t[LH]
#define RSCAN0GAFLID13H RSCAN0.GAFLID13.uint16_t[H]
#define RSCAN0GAFLID13HL RSCAN0.GAFLID13.uint8_t[HL]
#define RSCAN0GAFLID13HH RSCAN0.GAFLID13.uint8_t[HH]
#define RSCAN0GAFLM13 RSCAN0.GAFLM13.uint32_t
#define RSCAN0GAFLM13L RSCAN0.GAFLM13.uint16_t[L]
#define RSCAN0GAFLM13LL RSCAN0.GAFLM13.uint8_t[LL]
#define RSCAN0GAFLM13LH RSCAN0.GAFLM13.uint8_t[LH]
#define RSCAN0GAFLM13H RSCAN0.GAFLM13.uint16_t[H]
#define RSCAN0GAFLM13HL RSCAN0.GAFLM13.uint8_t[HL]
#define RSCAN0GAFLM13HH RSCAN0.GAFLM13.uint8_t[HH]
#define RSCAN0GAFLP013 RSCAN0.GAFLP013.uint32_t
#define RSCAN0GAFLP013L RSCAN0.GAFLP013.uint16_t[L]
#define RSCAN0GAFLP013LH RSCAN0.GAFLP013.uint8_t[LH]
#define RSCAN0GAFLP013H RSCAN0.GAFLP013.uint16_t[H]
#define RSCAN0GAFLP013HL RSCAN0.GAFLP013.uint8_t[HL]
#define RSCAN0GAFLP013HH RSCAN0.GAFLP013.uint8_t[HH]
#define RSCAN0GAFLP113 RSCAN0.GAFLP113.uint32_t
#define RSCAN0GAFLP113L RSCAN0.GAFLP113.uint16_t[L]
#define RSCAN0GAFLP113LL RSCAN0.GAFLP113.uint8_t[LL]
#define RSCAN0GAFLP113LH RSCAN0.GAFLP113.uint8_t[LH]
#define RSCAN0GAFLP113H RSCAN0.GAFLP113.uint16_t[H]
#define RSCAN0GAFLP113HL RSCAN0.GAFLP113.uint8_t[HL]
#define RSCAN0GAFLP113HH RSCAN0.GAFLP113.uint8_t[HH]
#define RSCAN0GAFLID14 RSCAN0.GAFLID14.uint32_t
#define RSCAN0GAFLID14L RSCAN0.GAFLID14.uint16_t[L]
#define RSCAN0GAFLID14LL RSCAN0.GAFLID14.uint8_t[LL]
#define RSCAN0GAFLID14LH RSCAN0.GAFLID14.uint8_t[LH]
#define RSCAN0GAFLID14H RSCAN0.GAFLID14.uint16_t[H]
#define RSCAN0GAFLID14HL RSCAN0.GAFLID14.uint8_t[HL]
#define RSCAN0GAFLID14HH RSCAN0.GAFLID14.uint8_t[HH]
#define RSCAN0GAFLM14 RSCAN0.GAFLM14.uint32_t
#define RSCAN0GAFLM14L RSCAN0.GAFLM14.uint16_t[L]
#define RSCAN0GAFLM14LL RSCAN0.GAFLM14.uint8_t[LL]
#define RSCAN0GAFLM14LH RSCAN0.GAFLM14.uint8_t[LH]
#define RSCAN0GAFLM14H RSCAN0.GAFLM14.uint16_t[H]
#define RSCAN0GAFLM14HL RSCAN0.GAFLM14.uint8_t[HL]
#define RSCAN0GAFLM14HH RSCAN0.GAFLM14.uint8_t[HH]
#define RSCAN0GAFLP014 RSCAN0.GAFLP014.uint32_t
#define RSCAN0GAFLP014L RSCAN0.GAFLP014.uint16_t[L]
#define RSCAN0GAFLP014LH RSCAN0.GAFLP014.uint8_t[LH]
#define RSCAN0GAFLP014H RSCAN0.GAFLP014.uint16_t[H]
#define RSCAN0GAFLP014HL RSCAN0.GAFLP014.uint8_t[HL]
#define RSCAN0GAFLP014HH RSCAN0.GAFLP014.uint8_t[HH]
#define RSCAN0GAFLP114 RSCAN0.GAFLP114.uint32_t
#define RSCAN0GAFLP114L RSCAN0.GAFLP114.uint16_t[L]
#define RSCAN0GAFLP114LL RSCAN0.GAFLP114.uint8_t[LL]
#define RSCAN0GAFLP114LH RSCAN0.GAFLP114.uint8_t[LH]
#define RSCAN0GAFLP114H RSCAN0.GAFLP114.uint16_t[H]
#define RSCAN0GAFLP114HL RSCAN0.GAFLP114.uint8_t[HL]
#define RSCAN0GAFLP114HH RSCAN0.GAFLP114.uint8_t[HH]
#define RSCAN0GAFLID15 RSCAN0.GAFLID15.uint32_t
#define RSCAN0GAFLID15L RSCAN0.GAFLID15.uint16_t[L]
#define RSCAN0GAFLID15LL RSCAN0.GAFLID15.uint8_t[LL]
#define RSCAN0GAFLID15LH RSCAN0.GAFLID15.uint8_t[LH]
#define RSCAN0GAFLID15H RSCAN0.GAFLID15.uint16_t[H]
#define RSCAN0GAFLID15HL RSCAN0.GAFLID15.uint8_t[HL]
#define RSCAN0GAFLID15HH RSCAN0.GAFLID15.uint8_t[HH]
#define RSCAN0GAFLM15 RSCAN0.GAFLM15.uint32_t
#define RSCAN0GAFLM15L RSCAN0.GAFLM15.uint16_t[L]
#define RSCAN0GAFLM15LL RSCAN0.GAFLM15.uint8_t[LL]
#define RSCAN0GAFLM15LH RSCAN0.GAFLM15.uint8_t[LH]
#define RSCAN0GAFLM15H RSCAN0.GAFLM15.uint16_t[H]
#define RSCAN0GAFLM15HL RSCAN0.GAFLM15.uint8_t[HL]
#define RSCAN0GAFLM15HH RSCAN0.GAFLM15.uint8_t[HH]
#define RSCAN0GAFLP015 RSCAN0.GAFLP015.uint32_t
#define RSCAN0GAFLP015L RSCAN0.GAFLP015.uint16_t[L]
#define RSCAN0GAFLP015LH RSCAN0.GAFLP015.uint8_t[LH]
#define RSCAN0GAFLP015H RSCAN0.GAFLP015.uint16_t[H]
#define RSCAN0GAFLP015HL RSCAN0.GAFLP015.uint8_t[HL]
#define RSCAN0GAFLP015HH RSCAN0.GAFLP015.uint8_t[HH]
#define RSCAN0GAFLP115 RSCAN0.GAFLP115.uint32_t
#define RSCAN0GAFLP115L RSCAN0.GAFLP115.uint16_t[L]
#define RSCAN0GAFLP115LL RSCAN0.GAFLP115.uint8_t[LL]
#define RSCAN0GAFLP115LH RSCAN0.GAFLP115.uint8_t[LH]
#define RSCAN0GAFLP115H RSCAN0.GAFLP115.uint16_t[H]
#define RSCAN0GAFLP115HL RSCAN0.GAFLP115.uint8_t[HL]
#define RSCAN0GAFLP115HH RSCAN0.GAFLP115.uint8_t[HH]
#define RSCAN0RMID0 RSCAN0.RMID0.uint32_t
#define RSCAN0RMID0L RSCAN0.RMID0.uint16_t[L]
#define RSCAN0RMID0LL RSCAN0.RMID0.uint8_t[LL]
#define RSCAN0RMID0LH RSCAN0.RMID0.uint8_t[LH]
#define RSCAN0RMID0H RSCAN0.RMID0.uint16_t[H]
#define RSCAN0RMID0HL RSCAN0.RMID0.uint8_t[HL]
#define RSCAN0RMID0HH RSCAN0.RMID0.uint8_t[HH]
#define RSCAN0RMPTR0 RSCAN0.RMPTR0.uint32_t
#define RSCAN0RMPTR0L RSCAN0.RMPTR0.uint16_t[L]
#define RSCAN0RMPTR0LL RSCAN0.RMPTR0.uint8_t[LL]
#define RSCAN0RMPTR0LH RSCAN0.RMPTR0.uint8_t[LH]
#define RSCAN0RMPTR0H RSCAN0.RMPTR0.uint16_t[H]
#define RSCAN0RMPTR0HL RSCAN0.RMPTR0.uint8_t[HL]
#define RSCAN0RMPTR0HH RSCAN0.RMPTR0.uint8_t[HH]
#define RSCAN0RMDF00 RSCAN0.RMDF00.uint32_t
#define RSCAN0RMDF00L RSCAN0.RMDF00.uint16_t[L]
#define RSCAN0RMDF00LL RSCAN0.RMDF00.uint8_t[LL]
#define RSCAN0RMDF00LH RSCAN0.RMDF00.uint8_t[LH]
#define RSCAN0RMDF00H RSCAN0.RMDF00.uint16_t[H]
#define RSCAN0RMDF00HL RSCAN0.RMDF00.uint8_t[HL]
#define RSCAN0RMDF00HH RSCAN0.RMDF00.uint8_t[HH]
#define RSCAN0RMDF10 RSCAN0.RMDF10.uint32_t
#define RSCAN0RMDF10L RSCAN0.RMDF10.uint16_t[L]
#define RSCAN0RMDF10LL RSCAN0.RMDF10.uint8_t[LL]
#define RSCAN0RMDF10LH RSCAN0.RMDF10.uint8_t[LH]
#define RSCAN0RMDF10H RSCAN0.RMDF10.uint16_t[H]
#define RSCAN0RMDF10HL RSCAN0.RMDF10.uint8_t[HL]
#define RSCAN0RMDF10HH RSCAN0.RMDF10.uint8_t[HH]
#define RSCAN0RMID1 RSCAN0.RMID1.uint32_t
#define RSCAN0RMID1L RSCAN0.RMID1.uint16_t[L]
#define RSCAN0RMID1LL RSCAN0.RMID1.uint8_t[LL]
#define RSCAN0RMID1LH RSCAN0.RMID1.uint8_t[LH]
#define RSCAN0RMID1H RSCAN0.RMID1.uint16_t[H]
#define RSCAN0RMID1HL RSCAN0.RMID1.uint8_t[HL]
#define RSCAN0RMID1HH RSCAN0.RMID1.uint8_t[HH]
#define RSCAN0RMPTR1 RSCAN0.RMPTR1.uint32_t
#define RSCAN0RMPTR1L RSCAN0.RMPTR1.uint16_t[L]
#define RSCAN0RMPTR1LL RSCAN0.RMPTR1.uint8_t[LL]
#define RSCAN0RMPTR1LH RSCAN0.RMPTR1.uint8_t[LH]
#define RSCAN0RMPTR1H RSCAN0.RMPTR1.uint16_t[H]
#define RSCAN0RMPTR1HL RSCAN0.RMPTR1.uint8_t[HL]
#define RSCAN0RMPTR1HH RSCAN0.RMPTR1.uint8_t[HH]
#define RSCAN0RMDF01 RSCAN0.RMDF01.uint32_t
#define RSCAN0RMDF01L RSCAN0.RMDF01.uint16_t[L]
#define RSCAN0RMDF01LL RSCAN0.RMDF01.uint8_t[LL]
#define RSCAN0RMDF01LH RSCAN0.RMDF01.uint8_t[LH]
#define RSCAN0RMDF01H RSCAN0.RMDF01.uint16_t[H]
#define RSCAN0RMDF01HL RSCAN0.RMDF01.uint8_t[HL]
#define RSCAN0RMDF01HH RSCAN0.RMDF01.uint8_t[HH]
#define RSCAN0RMDF11 RSCAN0.RMDF11.uint32_t
#define RSCAN0RMDF11L RSCAN0.RMDF11.uint16_t[L]
#define RSCAN0RMDF11LL RSCAN0.RMDF11.uint8_t[LL]
#define RSCAN0RMDF11LH RSCAN0.RMDF11.uint8_t[LH]
#define RSCAN0RMDF11H RSCAN0.RMDF11.uint16_t[H]
#define RSCAN0RMDF11HL RSCAN0.RMDF11.uint8_t[HL]
#define RSCAN0RMDF11HH RSCAN0.RMDF11.uint8_t[HH]
#define RSCAN0RMID2 RSCAN0.RMID2.uint32_t
#define RSCAN0RMID2L RSCAN0.RMID2.uint16_t[L]
#define RSCAN0RMID2LL RSCAN0.RMID2.uint8_t[LL]
#define RSCAN0RMID2LH RSCAN0.RMID2.uint8_t[LH]
#define RSCAN0RMID2H RSCAN0.RMID2.uint16_t[H]
#define RSCAN0RMID2HL RSCAN0.RMID2.uint8_t[HL]
#define RSCAN0RMID2HH RSCAN0.RMID2.uint8_t[HH]
#define RSCAN0RMPTR2 RSCAN0.RMPTR2.uint32_t
#define RSCAN0RMPTR2L RSCAN0.RMPTR2.uint16_t[L]
#define RSCAN0RMPTR2LL RSCAN0.RMPTR2.uint8_t[LL]
#define RSCAN0RMPTR2LH RSCAN0.RMPTR2.uint8_t[LH]
#define RSCAN0RMPTR2H RSCAN0.RMPTR2.uint16_t[H]
#define RSCAN0RMPTR2HL RSCAN0.RMPTR2.uint8_t[HL]
#define RSCAN0RMPTR2HH RSCAN0.RMPTR2.uint8_t[HH]
#define RSCAN0RMDF02 RSCAN0.RMDF02.uint32_t
#define RSCAN0RMDF02L RSCAN0.RMDF02.uint16_t[L]
#define RSCAN0RMDF02LL RSCAN0.RMDF02.uint8_t[LL]
#define RSCAN0RMDF02LH RSCAN0.RMDF02.uint8_t[LH]
#define RSCAN0RMDF02H RSCAN0.RMDF02.uint16_t[H]
#define RSCAN0RMDF02HL RSCAN0.RMDF02.uint8_t[HL]
#define RSCAN0RMDF02HH RSCAN0.RMDF02.uint8_t[HH]
#define RSCAN0RMDF12 RSCAN0.RMDF12.uint32_t
#define RSCAN0RMDF12L RSCAN0.RMDF12.uint16_t[L]
#define RSCAN0RMDF12LL RSCAN0.RMDF12.uint8_t[LL]
#define RSCAN0RMDF12LH RSCAN0.RMDF12.uint8_t[LH]
#define RSCAN0RMDF12H RSCAN0.RMDF12.uint16_t[H]
#define RSCAN0RMDF12HL RSCAN0.RMDF12.uint8_t[HL]
#define RSCAN0RMDF12HH RSCAN0.RMDF12.uint8_t[HH]
#define RSCAN0RMID3 RSCAN0.RMID3.uint32_t
#define RSCAN0RMID3L RSCAN0.RMID3.uint16_t[L]
#define RSCAN0RMID3LL RSCAN0.RMID3.uint8_t[LL]
#define RSCAN0RMID3LH RSCAN0.RMID3.uint8_t[LH]
#define RSCAN0RMID3H RSCAN0.RMID3.uint16_t[H]
#define RSCAN0RMID3HL RSCAN0.RMID3.uint8_t[HL]
#define RSCAN0RMID3HH RSCAN0.RMID3.uint8_t[HH]
#define RSCAN0RMPTR3 RSCAN0.RMPTR3.uint32_t
#define RSCAN0RMPTR3L RSCAN0.RMPTR3.uint16_t[L]
#define RSCAN0RMPTR3LL RSCAN0.RMPTR3.uint8_t[LL]
#define RSCAN0RMPTR3LH RSCAN0.RMPTR3.uint8_t[LH]
#define RSCAN0RMPTR3H RSCAN0.RMPTR3.uint16_t[H]
#define RSCAN0RMPTR3HL RSCAN0.RMPTR3.uint8_t[HL]
#define RSCAN0RMPTR3HH RSCAN0.RMPTR3.uint8_t[HH]
#define RSCAN0RMDF03 RSCAN0.RMDF03.uint32_t
#define RSCAN0RMDF03L RSCAN0.RMDF03.uint16_t[L]
#define RSCAN0RMDF03LL RSCAN0.RMDF03.uint8_t[LL]
#define RSCAN0RMDF03LH RSCAN0.RMDF03.uint8_t[LH]
#define RSCAN0RMDF03H RSCAN0.RMDF03.uint16_t[H]
#define RSCAN0RMDF03HL RSCAN0.RMDF03.uint8_t[HL]
#define RSCAN0RMDF03HH RSCAN0.RMDF03.uint8_t[HH]
#define RSCAN0RMDF13 RSCAN0.RMDF13.uint32_t
#define RSCAN0RMDF13L RSCAN0.RMDF13.uint16_t[L]
#define RSCAN0RMDF13LL RSCAN0.RMDF13.uint8_t[LL]
#define RSCAN0RMDF13LH RSCAN0.RMDF13.uint8_t[LH]
#define RSCAN0RMDF13H RSCAN0.RMDF13.uint16_t[H]
#define RSCAN0RMDF13HL RSCAN0.RMDF13.uint8_t[HL]
#define RSCAN0RMDF13HH RSCAN0.RMDF13.uint8_t[HH]
#define RSCAN0RMID4 RSCAN0.RMID4.uint32_t
#define RSCAN0RMID4L RSCAN0.RMID4.uint16_t[L]
#define RSCAN0RMID4LL RSCAN0.RMID4.uint8_t[LL]
#define RSCAN0RMID4LH RSCAN0.RMID4.uint8_t[LH]
#define RSCAN0RMID4H RSCAN0.RMID4.uint16_t[H]
#define RSCAN0RMID4HL RSCAN0.RMID4.uint8_t[HL]
#define RSCAN0RMID4HH RSCAN0.RMID4.uint8_t[HH]
#define RSCAN0RMPTR4 RSCAN0.RMPTR4.uint32_t
#define RSCAN0RMPTR4L RSCAN0.RMPTR4.uint16_t[L]
#define RSCAN0RMPTR4LL RSCAN0.RMPTR4.uint8_t[LL]
#define RSCAN0RMPTR4LH RSCAN0.RMPTR4.uint8_t[LH]
#define RSCAN0RMPTR4H RSCAN0.RMPTR4.uint16_t[H]
#define RSCAN0RMPTR4HL RSCAN0.RMPTR4.uint8_t[HL]
#define RSCAN0RMPTR4HH RSCAN0.RMPTR4.uint8_t[HH]
#define RSCAN0RMDF04 RSCAN0.RMDF04.uint32_t
#define RSCAN0RMDF04L RSCAN0.RMDF04.uint16_t[L]
#define RSCAN0RMDF04LL RSCAN0.RMDF04.uint8_t[LL]
#define RSCAN0RMDF04LH RSCAN0.RMDF04.uint8_t[LH]
#define RSCAN0RMDF04H RSCAN0.RMDF04.uint16_t[H]
#define RSCAN0RMDF04HL RSCAN0.RMDF04.uint8_t[HL]
#define RSCAN0RMDF04HH RSCAN0.RMDF04.uint8_t[HH]
#define RSCAN0RMDF14 RSCAN0.RMDF14.uint32_t
#define RSCAN0RMDF14L RSCAN0.RMDF14.uint16_t[L]
#define RSCAN0RMDF14LL RSCAN0.RMDF14.uint8_t[LL]
#define RSCAN0RMDF14LH RSCAN0.RMDF14.uint8_t[LH]
#define RSCAN0RMDF14H RSCAN0.RMDF14.uint16_t[H]
#define RSCAN0RMDF14HL RSCAN0.RMDF14.uint8_t[HL]
#define RSCAN0RMDF14HH RSCAN0.RMDF14.uint8_t[HH]
#define RSCAN0RMID5 RSCAN0.RMID5.uint32_t
#define RSCAN0RMID5L RSCAN0.RMID5.uint16_t[L]
#define RSCAN0RMID5LL RSCAN0.RMID5.uint8_t[LL]
#define RSCAN0RMID5LH RSCAN0.RMID5.uint8_t[LH]
#define RSCAN0RMID5H RSCAN0.RMID5.uint16_t[H]
#define RSCAN0RMID5HL RSCAN0.RMID5.uint8_t[HL]
#define RSCAN0RMID5HH RSCAN0.RMID5.uint8_t[HH]
#define RSCAN0RMPTR5 RSCAN0.RMPTR5.uint32_t
#define RSCAN0RMPTR5L RSCAN0.RMPTR5.uint16_t[L]
#define RSCAN0RMPTR5LL RSCAN0.RMPTR5.uint8_t[LL]
#define RSCAN0RMPTR5LH RSCAN0.RMPTR5.uint8_t[LH]
#define RSCAN0RMPTR5H RSCAN0.RMPTR5.uint16_t[H]
#define RSCAN0RMPTR5HL RSCAN0.RMPTR5.uint8_t[HL]
#define RSCAN0RMPTR5HH RSCAN0.RMPTR5.uint8_t[HH]
#define RSCAN0RMDF05 RSCAN0.RMDF05.uint32_t
#define RSCAN0RMDF05L RSCAN0.RMDF05.uint16_t[L]
#define RSCAN0RMDF05LL RSCAN0.RMDF05.uint8_t[LL]
#define RSCAN0RMDF05LH RSCAN0.RMDF05.uint8_t[LH]
#define RSCAN0RMDF05H RSCAN0.RMDF05.uint16_t[H]
#define RSCAN0RMDF05HL RSCAN0.RMDF05.uint8_t[HL]
#define RSCAN0RMDF05HH RSCAN0.RMDF05.uint8_t[HH]
#define RSCAN0RMDF15 RSCAN0.RMDF15.uint32_t
#define RSCAN0RMDF15L RSCAN0.RMDF15.uint16_t[L]
#define RSCAN0RMDF15LL RSCAN0.RMDF15.uint8_t[LL]
#define RSCAN0RMDF15LH RSCAN0.RMDF15.uint8_t[LH]
#define RSCAN0RMDF15H RSCAN0.RMDF15.uint16_t[H]
#define RSCAN0RMDF15HL RSCAN0.RMDF15.uint8_t[HL]
#define RSCAN0RMDF15HH RSCAN0.RMDF15.uint8_t[HH]
#define RSCAN0RMID6 RSCAN0.RMID6.uint32_t
#define RSCAN0RMID6L RSCAN0.RMID6.uint16_t[L]
#define RSCAN0RMID6LL RSCAN0.RMID6.uint8_t[LL]
#define RSCAN0RMID6LH RSCAN0.RMID6.uint8_t[LH]
#define RSCAN0RMID6H RSCAN0.RMID6.uint16_t[H]
#define RSCAN0RMID6HL RSCAN0.RMID6.uint8_t[HL]
#define RSCAN0RMID6HH RSCAN0.RMID6.uint8_t[HH]
#define RSCAN0RMPTR6 RSCAN0.RMPTR6.uint32_t
#define RSCAN0RMPTR6L RSCAN0.RMPTR6.uint16_t[L]
#define RSCAN0RMPTR6LL RSCAN0.RMPTR6.uint8_t[LL]
#define RSCAN0RMPTR6LH RSCAN0.RMPTR6.uint8_t[LH]
#define RSCAN0RMPTR6H RSCAN0.RMPTR6.uint16_t[H]
#define RSCAN0RMPTR6HL RSCAN0.RMPTR6.uint8_t[HL]
#define RSCAN0RMPTR6HH RSCAN0.RMPTR6.uint8_t[HH]
#define RSCAN0RMDF06 RSCAN0.RMDF06.uint32_t
#define RSCAN0RMDF06L RSCAN0.RMDF06.uint16_t[L]
#define RSCAN0RMDF06LL RSCAN0.RMDF06.uint8_t[LL]
#define RSCAN0RMDF06LH RSCAN0.RMDF06.uint8_t[LH]
#define RSCAN0RMDF06H RSCAN0.RMDF06.uint16_t[H]
#define RSCAN0RMDF06HL RSCAN0.RMDF06.uint8_t[HL]
#define RSCAN0RMDF06HH RSCAN0.RMDF06.uint8_t[HH]
#define RSCAN0RMDF16 RSCAN0.RMDF16.uint32_t
#define RSCAN0RMDF16L RSCAN0.RMDF16.uint16_t[L]
#define RSCAN0RMDF16LL RSCAN0.RMDF16.uint8_t[LL]
#define RSCAN0RMDF16LH RSCAN0.RMDF16.uint8_t[LH]
#define RSCAN0RMDF16H RSCAN0.RMDF16.uint16_t[H]
#define RSCAN0RMDF16HL RSCAN0.RMDF16.uint8_t[HL]
#define RSCAN0RMDF16HH RSCAN0.RMDF16.uint8_t[HH]
#define RSCAN0RMID7 RSCAN0.RMID7.uint32_t
#define RSCAN0RMID7L RSCAN0.RMID7.uint16_t[L]
#define RSCAN0RMID7LL RSCAN0.RMID7.uint8_t[LL]
#define RSCAN0RMID7LH RSCAN0.RMID7.uint8_t[LH]
#define RSCAN0RMID7H RSCAN0.RMID7.uint16_t[H]
#define RSCAN0RMID7HL RSCAN0.RMID7.uint8_t[HL]
#define RSCAN0RMID7HH RSCAN0.RMID7.uint8_t[HH]
#define RSCAN0RMPTR7 RSCAN0.RMPTR7.uint32_t
#define RSCAN0RMPTR7L RSCAN0.RMPTR7.uint16_t[L]
#define RSCAN0RMPTR7LL RSCAN0.RMPTR7.uint8_t[LL]
#define RSCAN0RMPTR7LH RSCAN0.RMPTR7.uint8_t[LH]
#define RSCAN0RMPTR7H RSCAN0.RMPTR7.uint16_t[H]
#define RSCAN0RMPTR7HL RSCAN0.RMPTR7.uint8_t[HL]
#define RSCAN0RMPTR7HH RSCAN0.RMPTR7.uint8_t[HH]
#define RSCAN0RMDF07 RSCAN0.RMDF07.uint32_t
#define RSCAN0RMDF07L RSCAN0.RMDF07.uint16_t[L]
#define RSCAN0RMDF07LL RSCAN0.RMDF07.uint8_t[LL]
#define RSCAN0RMDF07LH RSCAN0.RMDF07.uint8_t[LH]
#define RSCAN0RMDF07H RSCAN0.RMDF07.uint16_t[H]
#define RSCAN0RMDF07HL RSCAN0.RMDF07.uint8_t[HL]
#define RSCAN0RMDF07HH RSCAN0.RMDF07.uint8_t[HH]
#define RSCAN0RMDF17 RSCAN0.RMDF17.uint32_t
#define RSCAN0RMDF17L RSCAN0.RMDF17.uint16_t[L]
#define RSCAN0RMDF17LL RSCAN0.RMDF17.uint8_t[LL]
#define RSCAN0RMDF17LH RSCAN0.RMDF17.uint8_t[LH]
#define RSCAN0RMDF17H RSCAN0.RMDF17.uint16_t[H]
#define RSCAN0RMDF17HL RSCAN0.RMDF17.uint8_t[HL]
#define RSCAN0RMDF17HH RSCAN0.RMDF17.uint8_t[HH]
#define RSCAN0RMID8 RSCAN0.RMID8.uint32_t
#define RSCAN0RMID8L RSCAN0.RMID8.uint16_t[L]
#define RSCAN0RMID8LL RSCAN0.RMID8.uint8_t[LL]
#define RSCAN0RMID8LH RSCAN0.RMID8.uint8_t[LH]
#define RSCAN0RMID8H RSCAN0.RMID8.uint16_t[H]
#define RSCAN0RMID8HL RSCAN0.RMID8.uint8_t[HL]
#define RSCAN0RMID8HH RSCAN0.RMID8.uint8_t[HH]
#define RSCAN0RMPTR8 RSCAN0.RMPTR8.uint32_t
#define RSCAN0RMPTR8L RSCAN0.RMPTR8.uint16_t[L]
#define RSCAN0RMPTR8LL RSCAN0.RMPTR8.uint8_t[LL]
#define RSCAN0RMPTR8LH RSCAN0.RMPTR8.uint8_t[LH]
#define RSCAN0RMPTR8H RSCAN0.RMPTR8.uint16_t[H]
#define RSCAN0RMPTR8HL RSCAN0.RMPTR8.uint8_t[HL]
#define RSCAN0RMPTR8HH RSCAN0.RMPTR8.uint8_t[HH]
#define RSCAN0RMDF08 RSCAN0.RMDF08.uint32_t
#define RSCAN0RMDF08L RSCAN0.RMDF08.uint16_t[L]
#define RSCAN0RMDF08LL RSCAN0.RMDF08.uint8_t[LL]
#define RSCAN0RMDF08LH RSCAN0.RMDF08.uint8_t[LH]
#define RSCAN0RMDF08H RSCAN0.RMDF08.uint16_t[H]
#define RSCAN0RMDF08HL RSCAN0.RMDF08.uint8_t[HL]
#define RSCAN0RMDF08HH RSCAN0.RMDF08.uint8_t[HH]
#define RSCAN0RMDF18 RSCAN0.RMDF18.uint32_t
#define RSCAN0RMDF18L RSCAN0.RMDF18.uint16_t[L]
#define RSCAN0RMDF18LL RSCAN0.RMDF18.uint8_t[LL]
#define RSCAN0RMDF18LH RSCAN0.RMDF18.uint8_t[LH]
#define RSCAN0RMDF18H RSCAN0.RMDF18.uint16_t[H]
#define RSCAN0RMDF18HL RSCAN0.RMDF18.uint8_t[HL]
#define RSCAN0RMDF18HH RSCAN0.RMDF18.uint8_t[HH]
#define RSCAN0RMID9 RSCAN0.RMID9.uint32_t
#define RSCAN0RMID9L RSCAN0.RMID9.uint16_t[L]
#define RSCAN0RMID9LL RSCAN0.RMID9.uint8_t[LL]
#define RSCAN0RMID9LH RSCAN0.RMID9.uint8_t[LH]
#define RSCAN0RMID9H RSCAN0.RMID9.uint16_t[H]
#define RSCAN0RMID9HL RSCAN0.RMID9.uint8_t[HL]
#define RSCAN0RMID9HH RSCAN0.RMID9.uint8_t[HH]
#define RSCAN0RMPTR9 RSCAN0.RMPTR9.uint32_t
#define RSCAN0RMPTR9L RSCAN0.RMPTR9.uint16_t[L]
#define RSCAN0RMPTR9LL RSCAN0.RMPTR9.uint8_t[LL]
#define RSCAN0RMPTR9LH RSCAN0.RMPTR9.uint8_t[LH]
#define RSCAN0RMPTR9H RSCAN0.RMPTR9.uint16_t[H]
#define RSCAN0RMPTR9HL RSCAN0.RMPTR9.uint8_t[HL]
#define RSCAN0RMPTR9HH RSCAN0.RMPTR9.uint8_t[HH]
#define RSCAN0RMDF09 RSCAN0.RMDF09.uint32_t
#define RSCAN0RMDF09L RSCAN0.RMDF09.uint16_t[L]
#define RSCAN0RMDF09LL RSCAN0.RMDF09.uint8_t[LL]
#define RSCAN0RMDF09LH RSCAN0.RMDF09.uint8_t[LH]
#define RSCAN0RMDF09H RSCAN0.RMDF09.uint16_t[H]
#define RSCAN0RMDF09HL RSCAN0.RMDF09.uint8_t[HL]
#define RSCAN0RMDF09HH RSCAN0.RMDF09.uint8_t[HH]
#define RSCAN0RMDF19 RSCAN0.RMDF19.uint32_t
#define RSCAN0RMDF19L RSCAN0.RMDF19.uint16_t[L]
#define RSCAN0RMDF19LL RSCAN0.RMDF19.uint8_t[LL]
#define RSCAN0RMDF19LH RSCAN0.RMDF19.uint8_t[LH]
#define RSCAN0RMDF19H RSCAN0.RMDF19.uint16_t[H]
#define RSCAN0RMDF19HL RSCAN0.RMDF19.uint8_t[HL]
#define RSCAN0RMDF19HH RSCAN0.RMDF19.uint8_t[HH]
#define RSCAN0RMID10 RSCAN0.RMID10.uint32_t
#define RSCAN0RMID10L RSCAN0.RMID10.uint16_t[L]
#define RSCAN0RMID10LL RSCAN0.RMID10.uint8_t[LL]
#define RSCAN0RMID10LH RSCAN0.RMID10.uint8_t[LH]
#define RSCAN0RMID10H RSCAN0.RMID10.uint16_t[H]
#define RSCAN0RMID10HL RSCAN0.RMID10.uint8_t[HL]
#define RSCAN0RMID10HH RSCAN0.RMID10.uint8_t[HH]
#define RSCAN0RMPTR10 RSCAN0.RMPTR10.uint32_t
#define RSCAN0RMPTR10L RSCAN0.RMPTR10.uint16_t[L]
#define RSCAN0RMPTR10LL RSCAN0.RMPTR10.uint8_t[LL]
#define RSCAN0RMPTR10LH RSCAN0.RMPTR10.uint8_t[LH]
#define RSCAN0RMPTR10H RSCAN0.RMPTR10.uint16_t[H]
#define RSCAN0RMPTR10HL RSCAN0.RMPTR10.uint8_t[HL]
#define RSCAN0RMPTR10HH RSCAN0.RMPTR10.uint8_t[HH]
#define RSCAN0RMDF010 RSCAN0.RMDF010.uint32_t
#define RSCAN0RMDF010L RSCAN0.RMDF010.uint16_t[L]
#define RSCAN0RMDF010LL RSCAN0.RMDF010.uint8_t[LL]
#define RSCAN0RMDF010LH RSCAN0.RMDF010.uint8_t[LH]
#define RSCAN0RMDF010H RSCAN0.RMDF010.uint16_t[H]
#define RSCAN0RMDF010HL RSCAN0.RMDF010.uint8_t[HL]
#define RSCAN0RMDF010HH RSCAN0.RMDF010.uint8_t[HH]
#define RSCAN0RMDF110 RSCAN0.RMDF110.uint32_t
#define RSCAN0RMDF110L RSCAN0.RMDF110.uint16_t[L]
#define RSCAN0RMDF110LL RSCAN0.RMDF110.uint8_t[LL]
#define RSCAN0RMDF110LH RSCAN0.RMDF110.uint8_t[LH]
#define RSCAN0RMDF110H RSCAN0.RMDF110.uint16_t[H]
#define RSCAN0RMDF110HL RSCAN0.RMDF110.uint8_t[HL]
#define RSCAN0RMDF110HH RSCAN0.RMDF110.uint8_t[HH]
#define RSCAN0RMID11 RSCAN0.RMID11.uint32_t
#define RSCAN0RMID11L RSCAN0.RMID11.uint16_t[L]
#define RSCAN0RMID11LL RSCAN0.RMID11.uint8_t[LL]
#define RSCAN0RMID11LH RSCAN0.RMID11.uint8_t[LH]
#define RSCAN0RMID11H RSCAN0.RMID11.uint16_t[H]
#define RSCAN0RMID11HL RSCAN0.RMID11.uint8_t[HL]
#define RSCAN0RMID11HH RSCAN0.RMID11.uint8_t[HH]
#define RSCAN0RMPTR11 RSCAN0.RMPTR11.uint32_t
#define RSCAN0RMPTR11L RSCAN0.RMPTR11.uint16_t[L]
#define RSCAN0RMPTR11LL RSCAN0.RMPTR11.uint8_t[LL]
#define RSCAN0RMPTR11LH RSCAN0.RMPTR11.uint8_t[LH]
#define RSCAN0RMPTR11H RSCAN0.RMPTR11.uint16_t[H]
#define RSCAN0RMPTR11HL RSCAN0.RMPTR11.uint8_t[HL]
#define RSCAN0RMPTR11HH RSCAN0.RMPTR11.uint8_t[HH]
#define RSCAN0RMDF011 RSCAN0.RMDF011.uint32_t
#define RSCAN0RMDF011L RSCAN0.RMDF011.uint16_t[L]
#define RSCAN0RMDF011LL RSCAN0.RMDF011.uint8_t[LL]
#define RSCAN0RMDF011LH RSCAN0.RMDF011.uint8_t[LH]
#define RSCAN0RMDF011H RSCAN0.RMDF011.uint16_t[H]
#define RSCAN0RMDF011HL RSCAN0.RMDF011.uint8_t[HL]
#define RSCAN0RMDF011HH RSCAN0.RMDF011.uint8_t[HH]
#define RSCAN0RMDF111 RSCAN0.RMDF111.uint32_t
#define RSCAN0RMDF111L RSCAN0.RMDF111.uint16_t[L]
#define RSCAN0RMDF111LL RSCAN0.RMDF111.uint8_t[LL]
#define RSCAN0RMDF111LH RSCAN0.RMDF111.uint8_t[LH]
#define RSCAN0RMDF111H RSCAN0.RMDF111.uint16_t[H]
#define RSCAN0RMDF111HL RSCAN0.RMDF111.uint8_t[HL]
#define RSCAN0RMDF111HH RSCAN0.RMDF111.uint8_t[HH]
#define RSCAN0RMID12 RSCAN0.RMID12.uint32_t
#define RSCAN0RMID12L RSCAN0.RMID12.uint16_t[L]
#define RSCAN0RMID12LL RSCAN0.RMID12.uint8_t[LL]
#define RSCAN0RMID12LH RSCAN0.RMID12.uint8_t[LH]
#define RSCAN0RMID12H RSCAN0.RMID12.uint16_t[H]
#define RSCAN0RMID12HL RSCAN0.RMID12.uint8_t[HL]
#define RSCAN0RMID12HH RSCAN0.RMID12.uint8_t[HH]
#define RSCAN0RMPTR12 RSCAN0.RMPTR12.uint32_t
#define RSCAN0RMPTR12L RSCAN0.RMPTR12.uint16_t[L]
#define RSCAN0RMPTR12LL RSCAN0.RMPTR12.uint8_t[LL]
#define RSCAN0RMPTR12LH RSCAN0.RMPTR12.uint8_t[LH]
#define RSCAN0RMPTR12H RSCAN0.RMPTR12.uint16_t[H]
#define RSCAN0RMPTR12HL RSCAN0.RMPTR12.uint8_t[HL]
#define RSCAN0RMPTR12HH RSCAN0.RMPTR12.uint8_t[HH]
#define RSCAN0RMDF012 RSCAN0.RMDF012.uint32_t
#define RSCAN0RMDF012L RSCAN0.RMDF012.uint16_t[L]
#define RSCAN0RMDF012LL RSCAN0.RMDF012.uint8_t[LL]
#define RSCAN0RMDF012LH RSCAN0.RMDF012.uint8_t[LH]
#define RSCAN0RMDF012H RSCAN0.RMDF012.uint16_t[H]
#define RSCAN0RMDF012HL RSCAN0.RMDF012.uint8_t[HL]
#define RSCAN0RMDF012HH RSCAN0.RMDF012.uint8_t[HH]
#define RSCAN0RMDF112 RSCAN0.RMDF112.uint32_t
#define RSCAN0RMDF112L RSCAN0.RMDF112.uint16_t[L]
#define RSCAN0RMDF112LL RSCAN0.RMDF112.uint8_t[LL]
#define RSCAN0RMDF112LH RSCAN0.RMDF112.uint8_t[LH]
#define RSCAN0RMDF112H RSCAN0.RMDF112.uint16_t[H]
#define RSCAN0RMDF112HL RSCAN0.RMDF112.uint8_t[HL]
#define RSCAN0RMDF112HH RSCAN0.RMDF112.uint8_t[HH]
#define RSCAN0RMID13 RSCAN0.RMID13.uint32_t
#define RSCAN0RMID13L RSCAN0.RMID13.uint16_t[L]
#define RSCAN0RMID13LL RSCAN0.RMID13.uint8_t[LL]
#define RSCAN0RMID13LH RSCAN0.RMID13.uint8_t[LH]
#define RSCAN0RMID13H RSCAN0.RMID13.uint16_t[H]
#define RSCAN0RMID13HL RSCAN0.RMID13.uint8_t[HL]
#define RSCAN0RMID13HH RSCAN0.RMID13.uint8_t[HH]
#define RSCAN0RMPTR13 RSCAN0.RMPTR13.uint32_t
#define RSCAN0RMPTR13L RSCAN0.RMPTR13.uint16_t[L]
#define RSCAN0RMPTR13LL RSCAN0.RMPTR13.uint8_t[LL]
#define RSCAN0RMPTR13LH RSCAN0.RMPTR13.uint8_t[LH]
#define RSCAN0RMPTR13H RSCAN0.RMPTR13.uint16_t[H]
#define RSCAN0RMPTR13HL RSCAN0.RMPTR13.uint8_t[HL]
#define RSCAN0RMPTR13HH RSCAN0.RMPTR13.uint8_t[HH]
#define RSCAN0RMDF013 RSCAN0.RMDF013.uint32_t
#define RSCAN0RMDF013L RSCAN0.RMDF013.uint16_t[L]
#define RSCAN0RMDF013LL RSCAN0.RMDF013.uint8_t[LL]
#define RSCAN0RMDF013LH RSCAN0.RMDF013.uint8_t[LH]
#define RSCAN0RMDF013H RSCAN0.RMDF013.uint16_t[H]
#define RSCAN0RMDF013HL RSCAN0.RMDF013.uint8_t[HL]
#define RSCAN0RMDF013HH RSCAN0.RMDF013.uint8_t[HH]
#define RSCAN0RMDF113 RSCAN0.RMDF113.uint32_t
#define RSCAN0RMDF113L RSCAN0.RMDF113.uint16_t[L]
#define RSCAN0RMDF113LL RSCAN0.RMDF113.uint8_t[LL]
#define RSCAN0RMDF113LH RSCAN0.RMDF113.uint8_t[LH]
#define RSCAN0RMDF113H RSCAN0.RMDF113.uint16_t[H]
#define RSCAN0RMDF113HL RSCAN0.RMDF113.uint8_t[HL]
#define RSCAN0RMDF113HH RSCAN0.RMDF113.uint8_t[HH]
#define RSCAN0RMID14 RSCAN0.RMID14.uint32_t
#define RSCAN0RMID14L RSCAN0.RMID14.uint16_t[L]
#define RSCAN0RMID14LL RSCAN0.RMID14.uint8_t[LL]
#define RSCAN0RMID14LH RSCAN0.RMID14.uint8_t[LH]
#define RSCAN0RMID14H RSCAN0.RMID14.uint16_t[H]
#define RSCAN0RMID14HL RSCAN0.RMID14.uint8_t[HL]
#define RSCAN0RMID14HH RSCAN0.RMID14.uint8_t[HH]
#define RSCAN0RMPTR14 RSCAN0.RMPTR14.uint32_t
#define RSCAN0RMPTR14L RSCAN0.RMPTR14.uint16_t[L]
#define RSCAN0RMPTR14LL RSCAN0.RMPTR14.uint8_t[LL]
#define RSCAN0RMPTR14LH RSCAN0.RMPTR14.uint8_t[LH]
#define RSCAN0RMPTR14H RSCAN0.RMPTR14.uint16_t[H]
#define RSCAN0RMPTR14HL RSCAN0.RMPTR14.uint8_t[HL]
#define RSCAN0RMPTR14HH RSCAN0.RMPTR14.uint8_t[HH]
#define RSCAN0RMDF014 RSCAN0.RMDF014.uint32_t
#define RSCAN0RMDF014L RSCAN0.RMDF014.uint16_t[L]
#define RSCAN0RMDF014LL RSCAN0.RMDF014.uint8_t[LL]
#define RSCAN0RMDF014LH RSCAN0.RMDF014.uint8_t[LH]
#define RSCAN0RMDF014H RSCAN0.RMDF014.uint16_t[H]
#define RSCAN0RMDF014HL RSCAN0.RMDF014.uint8_t[HL]
#define RSCAN0RMDF014HH RSCAN0.RMDF014.uint8_t[HH]
#define RSCAN0RMDF114 RSCAN0.RMDF114.uint32_t
#define RSCAN0RMDF114L RSCAN0.RMDF114.uint16_t[L]
#define RSCAN0RMDF114LL RSCAN0.RMDF114.uint8_t[LL]
#define RSCAN0RMDF114LH RSCAN0.RMDF114.uint8_t[LH]
#define RSCAN0RMDF114H RSCAN0.RMDF114.uint16_t[H]
#define RSCAN0RMDF114HL RSCAN0.RMDF114.uint8_t[HL]
#define RSCAN0RMDF114HH RSCAN0.RMDF114.uint8_t[HH]
#define RSCAN0RMID15 RSCAN0.RMID15.uint32_t
#define RSCAN0RMID15L RSCAN0.RMID15.uint16_t[L]
#define RSCAN0RMID15LL RSCAN0.RMID15.uint8_t[LL]
#define RSCAN0RMID15LH RSCAN0.RMID15.uint8_t[LH]
#define RSCAN0RMID15H RSCAN0.RMID15.uint16_t[H]
#define RSCAN0RMID15HL RSCAN0.RMID15.uint8_t[HL]
#define RSCAN0RMID15HH RSCAN0.RMID15.uint8_t[HH]
#define RSCAN0RMPTR15 RSCAN0.RMPTR15.uint32_t
#define RSCAN0RMPTR15L RSCAN0.RMPTR15.uint16_t[L]
#define RSCAN0RMPTR15LL RSCAN0.RMPTR15.uint8_t[LL]
#define RSCAN0RMPTR15LH RSCAN0.RMPTR15.uint8_t[LH]
#define RSCAN0RMPTR15H RSCAN0.RMPTR15.uint16_t[H]
#define RSCAN0RMPTR15HL RSCAN0.RMPTR15.uint8_t[HL]
#define RSCAN0RMPTR15HH RSCAN0.RMPTR15.uint8_t[HH]
#define RSCAN0RMDF015 RSCAN0.RMDF015.uint32_t
#define RSCAN0RMDF015L RSCAN0.RMDF015.uint16_t[L]
#define RSCAN0RMDF015LL RSCAN0.RMDF015.uint8_t[LL]
#define RSCAN0RMDF015LH RSCAN0.RMDF015.uint8_t[LH]
#define RSCAN0RMDF015H RSCAN0.RMDF015.uint16_t[H]
#define RSCAN0RMDF015HL RSCAN0.RMDF015.uint8_t[HL]
#define RSCAN0RMDF015HH RSCAN0.RMDF015.uint8_t[HH]
#define RSCAN0RMDF115 RSCAN0.RMDF115.uint32_t
#define RSCAN0RMDF115L RSCAN0.RMDF115.uint16_t[L]
#define RSCAN0RMDF115LL RSCAN0.RMDF115.uint8_t[LL]
#define RSCAN0RMDF115LH RSCAN0.RMDF115.uint8_t[LH]
#define RSCAN0RMDF115H RSCAN0.RMDF115.uint16_t[H]
#define RSCAN0RMDF115HL RSCAN0.RMDF115.uint8_t[HL]
#define RSCAN0RMDF115HH RSCAN0.RMDF115.uint8_t[HH]
#define RSCAN0RMID16 RSCAN0.RMID16.uint32_t
#define RSCAN0RMID16L RSCAN0.RMID16.uint16_t[L]
#define RSCAN0RMID16LL RSCAN0.RMID16.uint8_t[LL]
#define RSCAN0RMID16LH RSCAN0.RMID16.uint8_t[LH]
#define RSCAN0RMID16H RSCAN0.RMID16.uint16_t[H]
#define RSCAN0RMID16HL RSCAN0.RMID16.uint8_t[HL]
#define RSCAN0RMID16HH RSCAN0.RMID16.uint8_t[HH]
#define RSCAN0RMPTR16 RSCAN0.RMPTR16.uint32_t
#define RSCAN0RMPTR16L RSCAN0.RMPTR16.uint16_t[L]
#define RSCAN0RMPTR16LL RSCAN0.RMPTR16.uint8_t[LL]
#define RSCAN0RMPTR16LH RSCAN0.RMPTR16.uint8_t[LH]
#define RSCAN0RMPTR16H RSCAN0.RMPTR16.uint16_t[H]
#define RSCAN0RMPTR16HL RSCAN0.RMPTR16.uint8_t[HL]
#define RSCAN0RMPTR16HH RSCAN0.RMPTR16.uint8_t[HH]
#define RSCAN0RMDF016 RSCAN0.RMDF016.uint32_t
#define RSCAN0RMDF016L RSCAN0.RMDF016.uint16_t[L]
#define RSCAN0RMDF016LL RSCAN0.RMDF016.uint8_t[LL]
#define RSCAN0RMDF016LH RSCAN0.RMDF016.uint8_t[LH]
#define RSCAN0RMDF016H RSCAN0.RMDF016.uint16_t[H]
#define RSCAN0RMDF016HL RSCAN0.RMDF016.uint8_t[HL]
#define RSCAN0RMDF016HH RSCAN0.RMDF016.uint8_t[HH]
#define RSCAN0RMDF116 RSCAN0.RMDF116.uint32_t
#define RSCAN0RMDF116L RSCAN0.RMDF116.uint16_t[L]
#define RSCAN0RMDF116LL RSCAN0.RMDF116.uint8_t[LL]
#define RSCAN0RMDF116LH RSCAN0.RMDF116.uint8_t[LH]
#define RSCAN0RMDF116H RSCAN0.RMDF116.uint16_t[H]
#define RSCAN0RMDF116HL RSCAN0.RMDF116.uint8_t[HL]
#define RSCAN0RMDF116HH RSCAN0.RMDF116.uint8_t[HH]
#define RSCAN0RMID17 RSCAN0.RMID17.uint32_t
#define RSCAN0RMID17L RSCAN0.RMID17.uint16_t[L]
#define RSCAN0RMID17LL RSCAN0.RMID17.uint8_t[LL]
#define RSCAN0RMID17LH RSCAN0.RMID17.uint8_t[LH]
#define RSCAN0RMID17H RSCAN0.RMID17.uint16_t[H]
#define RSCAN0RMID17HL RSCAN0.RMID17.uint8_t[HL]
#define RSCAN0RMID17HH RSCAN0.RMID17.uint8_t[HH]
#define RSCAN0RMPTR17 RSCAN0.RMPTR17.uint32_t
#define RSCAN0RMPTR17L RSCAN0.RMPTR17.uint16_t[L]
#define RSCAN0RMPTR17LL RSCAN0.RMPTR17.uint8_t[LL]
#define RSCAN0RMPTR17LH RSCAN0.RMPTR17.uint8_t[LH]
#define RSCAN0RMPTR17H RSCAN0.RMPTR17.uint16_t[H]
#define RSCAN0RMPTR17HL RSCAN0.RMPTR17.uint8_t[HL]
#define RSCAN0RMPTR17HH RSCAN0.RMPTR17.uint8_t[HH]
#define RSCAN0RMDF017 RSCAN0.RMDF017.uint32_t
#define RSCAN0RMDF017L RSCAN0.RMDF017.uint16_t[L]
#define RSCAN0RMDF017LL RSCAN0.RMDF017.uint8_t[LL]
#define RSCAN0RMDF017LH RSCAN0.RMDF017.uint8_t[LH]
#define RSCAN0RMDF017H RSCAN0.RMDF017.uint16_t[H]
#define RSCAN0RMDF017HL RSCAN0.RMDF017.uint8_t[HL]
#define RSCAN0RMDF017HH RSCAN0.RMDF017.uint8_t[HH]
#define RSCAN0RMDF117 RSCAN0.RMDF117.uint32_t
#define RSCAN0RMDF117L RSCAN0.RMDF117.uint16_t[L]
#define RSCAN0RMDF117LL RSCAN0.RMDF117.uint8_t[LL]
#define RSCAN0RMDF117LH RSCAN0.RMDF117.uint8_t[LH]
#define RSCAN0RMDF117H RSCAN0.RMDF117.uint16_t[H]
#define RSCAN0RMDF117HL RSCAN0.RMDF117.uint8_t[HL]
#define RSCAN0RMDF117HH RSCAN0.RMDF117.uint8_t[HH]
#define RSCAN0RMID18 RSCAN0.RMID18.uint32_t
#define RSCAN0RMID18L RSCAN0.RMID18.uint16_t[L]
#define RSCAN0RMID18LL RSCAN0.RMID18.uint8_t[LL]
#define RSCAN0RMID18LH RSCAN0.RMID18.uint8_t[LH]
#define RSCAN0RMID18H RSCAN0.RMID18.uint16_t[H]
#define RSCAN0RMID18HL RSCAN0.RMID18.uint8_t[HL]
#define RSCAN0RMID18HH RSCAN0.RMID18.uint8_t[HH]
#define RSCAN0RMPTR18 RSCAN0.RMPTR18.uint32_t
#define RSCAN0RMPTR18L RSCAN0.RMPTR18.uint16_t[L]
#define RSCAN0RMPTR18LL RSCAN0.RMPTR18.uint8_t[LL]
#define RSCAN0RMPTR18LH RSCAN0.RMPTR18.uint8_t[LH]
#define RSCAN0RMPTR18H RSCAN0.RMPTR18.uint16_t[H]
#define RSCAN0RMPTR18HL RSCAN0.RMPTR18.uint8_t[HL]
#define RSCAN0RMPTR18HH RSCAN0.RMPTR18.uint8_t[HH]
#define RSCAN0RMDF018 RSCAN0.RMDF018.uint32_t
#define RSCAN0RMDF018L RSCAN0.RMDF018.uint16_t[L]
#define RSCAN0RMDF018LL RSCAN0.RMDF018.uint8_t[LL]
#define RSCAN0RMDF018LH RSCAN0.RMDF018.uint8_t[LH]
#define RSCAN0RMDF018H RSCAN0.RMDF018.uint16_t[H]
#define RSCAN0RMDF018HL RSCAN0.RMDF018.uint8_t[HL]
#define RSCAN0RMDF018HH RSCAN0.RMDF018.uint8_t[HH]
#define RSCAN0RMDF118 RSCAN0.RMDF118.uint32_t
#define RSCAN0RMDF118L RSCAN0.RMDF118.uint16_t[L]
#define RSCAN0RMDF118LL RSCAN0.RMDF118.uint8_t[LL]
#define RSCAN0RMDF118LH RSCAN0.RMDF118.uint8_t[LH]
#define RSCAN0RMDF118H RSCAN0.RMDF118.uint16_t[H]
#define RSCAN0RMDF118HL RSCAN0.RMDF118.uint8_t[HL]
#define RSCAN0RMDF118HH RSCAN0.RMDF118.uint8_t[HH]
#define RSCAN0RMID19 RSCAN0.RMID19.uint32_t
#define RSCAN0RMID19L RSCAN0.RMID19.uint16_t[L]
#define RSCAN0RMID19LL RSCAN0.RMID19.uint8_t[LL]
#define RSCAN0RMID19LH RSCAN0.RMID19.uint8_t[LH]
#define RSCAN0RMID19H RSCAN0.RMID19.uint16_t[H]
#define RSCAN0RMID19HL RSCAN0.RMID19.uint8_t[HL]
#define RSCAN0RMID19HH RSCAN0.RMID19.uint8_t[HH]
#define RSCAN0RMPTR19 RSCAN0.RMPTR19.uint32_t
#define RSCAN0RMPTR19L RSCAN0.RMPTR19.uint16_t[L]
#define RSCAN0RMPTR19LL RSCAN0.RMPTR19.uint8_t[LL]
#define RSCAN0RMPTR19LH RSCAN0.RMPTR19.uint8_t[LH]
#define RSCAN0RMPTR19H RSCAN0.RMPTR19.uint16_t[H]
#define RSCAN0RMPTR19HL RSCAN0.RMPTR19.uint8_t[HL]
#define RSCAN0RMPTR19HH RSCAN0.RMPTR19.uint8_t[HH]
#define RSCAN0RMDF019 RSCAN0.RMDF019.uint32_t
#define RSCAN0RMDF019L RSCAN0.RMDF019.uint16_t[L]
#define RSCAN0RMDF019LL RSCAN0.RMDF019.uint8_t[LL]
#define RSCAN0RMDF019LH RSCAN0.RMDF019.uint8_t[LH]
#define RSCAN0RMDF019H RSCAN0.RMDF019.uint16_t[H]
#define RSCAN0RMDF019HL RSCAN0.RMDF019.uint8_t[HL]
#define RSCAN0RMDF019HH RSCAN0.RMDF019.uint8_t[HH]
#define RSCAN0RMDF119 RSCAN0.RMDF119.uint32_t
#define RSCAN0RMDF119L RSCAN0.RMDF119.uint16_t[L]
#define RSCAN0RMDF119LL RSCAN0.RMDF119.uint8_t[LL]
#define RSCAN0RMDF119LH RSCAN0.RMDF119.uint8_t[LH]
#define RSCAN0RMDF119H RSCAN0.RMDF119.uint16_t[H]
#define RSCAN0RMDF119HL RSCAN0.RMDF119.uint8_t[HL]
#define RSCAN0RMDF119HH RSCAN0.RMDF119.uint8_t[HH]
#define RSCAN0RMID20 RSCAN0.RMID20.uint32_t
#define RSCAN0RMID20L RSCAN0.RMID20.uint16_t[L]
#define RSCAN0RMID20LL RSCAN0.RMID20.uint8_t[LL]
#define RSCAN0RMID20LH RSCAN0.RMID20.uint8_t[LH]
#define RSCAN0RMID20H RSCAN0.RMID20.uint16_t[H]
#define RSCAN0RMID20HL RSCAN0.RMID20.uint8_t[HL]
#define RSCAN0RMID20HH RSCAN0.RMID20.uint8_t[HH]
#define RSCAN0RMPTR20 RSCAN0.RMPTR20.uint32_t
#define RSCAN0RMPTR20L RSCAN0.RMPTR20.uint16_t[L]
#define RSCAN0RMPTR20LL RSCAN0.RMPTR20.uint8_t[LL]
#define RSCAN0RMPTR20LH RSCAN0.RMPTR20.uint8_t[LH]
#define RSCAN0RMPTR20H RSCAN0.RMPTR20.uint16_t[H]
#define RSCAN0RMPTR20HL RSCAN0.RMPTR20.uint8_t[HL]
#define RSCAN0RMPTR20HH RSCAN0.RMPTR20.uint8_t[HH]
#define RSCAN0RMDF020 RSCAN0.RMDF020.uint32_t
#define RSCAN0RMDF020L RSCAN0.RMDF020.uint16_t[L]
#define RSCAN0RMDF020LL RSCAN0.RMDF020.uint8_t[LL]
#define RSCAN0RMDF020LH RSCAN0.RMDF020.uint8_t[LH]
#define RSCAN0RMDF020H RSCAN0.RMDF020.uint16_t[H]
#define RSCAN0RMDF020HL RSCAN0.RMDF020.uint8_t[HL]
#define RSCAN0RMDF020HH RSCAN0.RMDF020.uint8_t[HH]
#define RSCAN0RMDF120 RSCAN0.RMDF120.uint32_t
#define RSCAN0RMDF120L RSCAN0.RMDF120.uint16_t[L]
#define RSCAN0RMDF120LL RSCAN0.RMDF120.uint8_t[LL]
#define RSCAN0RMDF120LH RSCAN0.RMDF120.uint8_t[LH]
#define RSCAN0RMDF120H RSCAN0.RMDF120.uint16_t[H]
#define RSCAN0RMDF120HL RSCAN0.RMDF120.uint8_t[HL]
#define RSCAN0RMDF120HH RSCAN0.RMDF120.uint8_t[HH]
#define RSCAN0RMID21 RSCAN0.RMID21.uint32_t
#define RSCAN0RMID21L RSCAN0.RMID21.uint16_t[L]
#define RSCAN0RMID21LL RSCAN0.RMID21.uint8_t[LL]
#define RSCAN0RMID21LH RSCAN0.RMID21.uint8_t[LH]
#define RSCAN0RMID21H RSCAN0.RMID21.uint16_t[H]
#define RSCAN0RMID21HL RSCAN0.RMID21.uint8_t[HL]
#define RSCAN0RMID21HH RSCAN0.RMID21.uint8_t[HH]
#define RSCAN0RMPTR21 RSCAN0.RMPTR21.uint32_t
#define RSCAN0RMPTR21L RSCAN0.RMPTR21.uint16_t[L]
#define RSCAN0RMPTR21LL RSCAN0.RMPTR21.uint8_t[LL]
#define RSCAN0RMPTR21LH RSCAN0.RMPTR21.uint8_t[LH]
#define RSCAN0RMPTR21H RSCAN0.RMPTR21.uint16_t[H]
#define RSCAN0RMPTR21HL RSCAN0.RMPTR21.uint8_t[HL]
#define RSCAN0RMPTR21HH RSCAN0.RMPTR21.uint8_t[HH]
#define RSCAN0RMDF021 RSCAN0.RMDF021.uint32_t
#define RSCAN0RMDF021L RSCAN0.RMDF021.uint16_t[L]
#define RSCAN0RMDF021LL RSCAN0.RMDF021.uint8_t[LL]
#define RSCAN0RMDF021LH RSCAN0.RMDF021.uint8_t[LH]
#define RSCAN0RMDF021H RSCAN0.RMDF021.uint16_t[H]
#define RSCAN0RMDF021HL RSCAN0.RMDF021.uint8_t[HL]
#define RSCAN0RMDF021HH RSCAN0.RMDF021.uint8_t[HH]
#define RSCAN0RMDF121 RSCAN0.RMDF121.uint32_t
#define RSCAN0RMDF121L RSCAN0.RMDF121.uint16_t[L]
#define RSCAN0RMDF121LL RSCAN0.RMDF121.uint8_t[LL]
#define RSCAN0RMDF121LH RSCAN0.RMDF121.uint8_t[LH]
#define RSCAN0RMDF121H RSCAN0.RMDF121.uint16_t[H]
#define RSCAN0RMDF121HL RSCAN0.RMDF121.uint8_t[HL]
#define RSCAN0RMDF121HH RSCAN0.RMDF121.uint8_t[HH]
#define RSCAN0RMID22 RSCAN0.RMID22.uint32_t
#define RSCAN0RMID22L RSCAN0.RMID22.uint16_t[L]
#define RSCAN0RMID22LL RSCAN0.RMID22.uint8_t[LL]
#define RSCAN0RMID22LH RSCAN0.RMID22.uint8_t[LH]
#define RSCAN0RMID22H RSCAN0.RMID22.uint16_t[H]
#define RSCAN0RMID22HL RSCAN0.RMID22.uint8_t[HL]
#define RSCAN0RMID22HH RSCAN0.RMID22.uint8_t[HH]
#define RSCAN0RMPTR22 RSCAN0.RMPTR22.uint32_t
#define RSCAN0RMPTR22L RSCAN0.RMPTR22.uint16_t[L]
#define RSCAN0RMPTR22LL RSCAN0.RMPTR22.uint8_t[LL]
#define RSCAN0RMPTR22LH RSCAN0.RMPTR22.uint8_t[LH]
#define RSCAN0RMPTR22H RSCAN0.RMPTR22.uint16_t[H]
#define RSCAN0RMPTR22HL RSCAN0.RMPTR22.uint8_t[HL]
#define RSCAN0RMPTR22HH RSCAN0.RMPTR22.uint8_t[HH]
#define RSCAN0RMDF022 RSCAN0.RMDF022.uint32_t
#define RSCAN0RMDF022L RSCAN0.RMDF022.uint16_t[L]
#define RSCAN0RMDF022LL RSCAN0.RMDF022.uint8_t[LL]
#define RSCAN0RMDF022LH RSCAN0.RMDF022.uint8_t[LH]
#define RSCAN0RMDF022H RSCAN0.RMDF022.uint16_t[H]
#define RSCAN0RMDF022HL RSCAN0.RMDF022.uint8_t[HL]
#define RSCAN0RMDF022HH RSCAN0.RMDF022.uint8_t[HH]
#define RSCAN0RMDF122 RSCAN0.RMDF122.uint32_t
#define RSCAN0RMDF122L RSCAN0.RMDF122.uint16_t[L]
#define RSCAN0RMDF122LL RSCAN0.RMDF122.uint8_t[LL]
#define RSCAN0RMDF122LH RSCAN0.RMDF122.uint8_t[LH]
#define RSCAN0RMDF122H RSCAN0.RMDF122.uint16_t[H]
#define RSCAN0RMDF122HL RSCAN0.RMDF122.uint8_t[HL]
#define RSCAN0RMDF122HH RSCAN0.RMDF122.uint8_t[HH]
#define RSCAN0RMID23 RSCAN0.RMID23.uint32_t
#define RSCAN0RMID23L RSCAN0.RMID23.uint16_t[L]
#define RSCAN0RMID23LL RSCAN0.RMID23.uint8_t[LL]
#define RSCAN0RMID23LH RSCAN0.RMID23.uint8_t[LH]
#define RSCAN0RMID23H RSCAN0.RMID23.uint16_t[H]
#define RSCAN0RMID23HL RSCAN0.RMID23.uint8_t[HL]
#define RSCAN0RMID23HH RSCAN0.RMID23.uint8_t[HH]
#define RSCAN0RMPTR23 RSCAN0.RMPTR23.uint32_t
#define RSCAN0RMPTR23L RSCAN0.RMPTR23.uint16_t[L]
#define RSCAN0RMPTR23LL RSCAN0.RMPTR23.uint8_t[LL]
#define RSCAN0RMPTR23LH RSCAN0.RMPTR23.uint8_t[LH]
#define RSCAN0RMPTR23H RSCAN0.RMPTR23.uint16_t[H]
#define RSCAN0RMPTR23HL RSCAN0.RMPTR23.uint8_t[HL]
#define RSCAN0RMPTR23HH RSCAN0.RMPTR23.uint8_t[HH]
#define RSCAN0RMDF023 RSCAN0.RMDF023.uint32_t
#define RSCAN0RMDF023L RSCAN0.RMDF023.uint16_t[L]
#define RSCAN0RMDF023LL RSCAN0.RMDF023.uint8_t[LL]
#define RSCAN0RMDF023LH RSCAN0.RMDF023.uint8_t[LH]
#define RSCAN0RMDF023H RSCAN0.RMDF023.uint16_t[H]
#define RSCAN0RMDF023HL RSCAN0.RMDF023.uint8_t[HL]
#define RSCAN0RMDF023HH RSCAN0.RMDF023.uint8_t[HH]
#define RSCAN0RMDF123 RSCAN0.RMDF123.uint32_t
#define RSCAN0RMDF123L RSCAN0.RMDF123.uint16_t[L]
#define RSCAN0RMDF123LL RSCAN0.RMDF123.uint8_t[LL]
#define RSCAN0RMDF123LH RSCAN0.RMDF123.uint8_t[LH]
#define RSCAN0RMDF123H RSCAN0.RMDF123.uint16_t[H]
#define RSCAN0RMDF123HL RSCAN0.RMDF123.uint8_t[HL]
#define RSCAN0RMDF123HH RSCAN0.RMDF123.uint8_t[HH]
#define RSCAN0RMID24 RSCAN0.RMID24.uint32_t
#define RSCAN0RMID24L RSCAN0.RMID24.uint16_t[L]
#define RSCAN0RMID24LL RSCAN0.RMID24.uint8_t[LL]
#define RSCAN0RMID24LH RSCAN0.RMID24.uint8_t[LH]
#define RSCAN0RMID24H RSCAN0.RMID24.uint16_t[H]
#define RSCAN0RMID24HL RSCAN0.RMID24.uint8_t[HL]
#define RSCAN0RMID24HH RSCAN0.RMID24.uint8_t[HH]
#define RSCAN0RMPTR24 RSCAN0.RMPTR24.uint32_t
#define RSCAN0RMPTR24L RSCAN0.RMPTR24.uint16_t[L]
#define RSCAN0RMPTR24LL RSCAN0.RMPTR24.uint8_t[LL]
#define RSCAN0RMPTR24LH RSCAN0.RMPTR24.uint8_t[LH]
#define RSCAN0RMPTR24H RSCAN0.RMPTR24.uint16_t[H]
#define RSCAN0RMPTR24HL RSCAN0.RMPTR24.uint8_t[HL]
#define RSCAN0RMPTR24HH RSCAN0.RMPTR24.uint8_t[HH]
#define RSCAN0RMDF024 RSCAN0.RMDF024.uint32_t
#define RSCAN0RMDF024L RSCAN0.RMDF024.uint16_t[L]
#define RSCAN0RMDF024LL RSCAN0.RMDF024.uint8_t[LL]
#define RSCAN0RMDF024LH RSCAN0.RMDF024.uint8_t[LH]
#define RSCAN0RMDF024H RSCAN0.RMDF024.uint16_t[H]
#define RSCAN0RMDF024HL RSCAN0.RMDF024.uint8_t[HL]
#define RSCAN0RMDF024HH RSCAN0.RMDF024.uint8_t[HH]
#define RSCAN0RMDF124 RSCAN0.RMDF124.uint32_t
#define RSCAN0RMDF124L RSCAN0.RMDF124.uint16_t[L]
#define RSCAN0RMDF124LL RSCAN0.RMDF124.uint8_t[LL]
#define RSCAN0RMDF124LH RSCAN0.RMDF124.uint8_t[LH]
#define RSCAN0RMDF124H RSCAN0.RMDF124.uint16_t[H]
#define RSCAN0RMDF124HL RSCAN0.RMDF124.uint8_t[HL]
#define RSCAN0RMDF124HH RSCAN0.RMDF124.uint8_t[HH]
#define RSCAN0RMID25 RSCAN0.RMID25.uint32_t
#define RSCAN0RMID25L RSCAN0.RMID25.uint16_t[L]
#define RSCAN0RMID25LL RSCAN0.RMID25.uint8_t[LL]
#define RSCAN0RMID25LH RSCAN0.RMID25.uint8_t[LH]
#define RSCAN0RMID25H RSCAN0.RMID25.uint16_t[H]
#define RSCAN0RMID25HL RSCAN0.RMID25.uint8_t[HL]
#define RSCAN0RMID25HH RSCAN0.RMID25.uint8_t[HH]
#define RSCAN0RMPTR25 RSCAN0.RMPTR25.uint32_t
#define RSCAN0RMPTR25L RSCAN0.RMPTR25.uint16_t[L]
#define RSCAN0RMPTR25LL RSCAN0.RMPTR25.uint8_t[LL]
#define RSCAN0RMPTR25LH RSCAN0.RMPTR25.uint8_t[LH]
#define RSCAN0RMPTR25H RSCAN0.RMPTR25.uint16_t[H]
#define RSCAN0RMPTR25HL RSCAN0.RMPTR25.uint8_t[HL]
#define RSCAN0RMPTR25HH RSCAN0.RMPTR25.uint8_t[HH]
#define RSCAN0RMDF025 RSCAN0.RMDF025.uint32_t
#define RSCAN0RMDF025L RSCAN0.RMDF025.uint16_t[L]
#define RSCAN0RMDF025LL RSCAN0.RMDF025.uint8_t[LL]
#define RSCAN0RMDF025LH RSCAN0.RMDF025.uint8_t[LH]
#define RSCAN0RMDF025H RSCAN0.RMDF025.uint16_t[H]
#define RSCAN0RMDF025HL RSCAN0.RMDF025.uint8_t[HL]
#define RSCAN0RMDF025HH RSCAN0.RMDF025.uint8_t[HH]
#define RSCAN0RMDF125 RSCAN0.RMDF125.uint32_t
#define RSCAN0RMDF125L RSCAN0.RMDF125.uint16_t[L]
#define RSCAN0RMDF125LL RSCAN0.RMDF125.uint8_t[LL]
#define RSCAN0RMDF125LH RSCAN0.RMDF125.uint8_t[LH]
#define RSCAN0RMDF125H RSCAN0.RMDF125.uint16_t[H]
#define RSCAN0RMDF125HL RSCAN0.RMDF125.uint8_t[HL]
#define RSCAN0RMDF125HH RSCAN0.RMDF125.uint8_t[HH]
#define RSCAN0RMID26 RSCAN0.RMID26.uint32_t
#define RSCAN0RMID26L RSCAN0.RMID26.uint16_t[L]
#define RSCAN0RMID26LL RSCAN0.RMID26.uint8_t[LL]
#define RSCAN0RMID26LH RSCAN0.RMID26.uint8_t[LH]
#define RSCAN0RMID26H RSCAN0.RMID26.uint16_t[H]
#define RSCAN0RMID26HL RSCAN0.RMID26.uint8_t[HL]
#define RSCAN0RMID26HH RSCAN0.RMID26.uint8_t[HH]
#define RSCAN0RMPTR26 RSCAN0.RMPTR26.uint32_t
#define RSCAN0RMPTR26L RSCAN0.RMPTR26.uint16_t[L]
#define RSCAN0RMPTR26LL RSCAN0.RMPTR26.uint8_t[LL]
#define RSCAN0RMPTR26LH RSCAN0.RMPTR26.uint8_t[LH]
#define RSCAN0RMPTR26H RSCAN0.RMPTR26.uint16_t[H]
#define RSCAN0RMPTR26HL RSCAN0.RMPTR26.uint8_t[HL]
#define RSCAN0RMPTR26HH RSCAN0.RMPTR26.uint8_t[HH]
#define RSCAN0RMDF026 RSCAN0.RMDF026.uint32_t
#define RSCAN0RMDF026L RSCAN0.RMDF026.uint16_t[L]
#define RSCAN0RMDF026LL RSCAN0.RMDF026.uint8_t[LL]
#define RSCAN0RMDF026LH RSCAN0.RMDF026.uint8_t[LH]
#define RSCAN0RMDF026H RSCAN0.RMDF026.uint16_t[H]
#define RSCAN0RMDF026HL RSCAN0.RMDF026.uint8_t[HL]
#define RSCAN0RMDF026HH RSCAN0.RMDF026.uint8_t[HH]
#define RSCAN0RMDF126 RSCAN0.RMDF126.uint32_t
#define RSCAN0RMDF126L RSCAN0.RMDF126.uint16_t[L]
#define RSCAN0RMDF126LL RSCAN0.RMDF126.uint8_t[LL]
#define RSCAN0RMDF126LH RSCAN0.RMDF126.uint8_t[LH]
#define RSCAN0RMDF126H RSCAN0.RMDF126.uint16_t[H]
#define RSCAN0RMDF126HL RSCAN0.RMDF126.uint8_t[HL]
#define RSCAN0RMDF126HH RSCAN0.RMDF126.uint8_t[HH]
#define RSCAN0RMID27 RSCAN0.RMID27.uint32_t
#define RSCAN0RMID27L RSCAN0.RMID27.uint16_t[L]
#define RSCAN0RMID27LL RSCAN0.RMID27.uint8_t[LL]
#define RSCAN0RMID27LH RSCAN0.RMID27.uint8_t[LH]
#define RSCAN0RMID27H RSCAN0.RMID27.uint16_t[H]
#define RSCAN0RMID27HL RSCAN0.RMID27.uint8_t[HL]
#define RSCAN0RMID27HH RSCAN0.RMID27.uint8_t[HH]
#define RSCAN0RMPTR27 RSCAN0.RMPTR27.uint32_t
#define RSCAN0RMPTR27L RSCAN0.RMPTR27.uint16_t[L]
#define RSCAN0RMPTR27LL RSCAN0.RMPTR27.uint8_t[LL]
#define RSCAN0RMPTR27LH RSCAN0.RMPTR27.uint8_t[LH]
#define RSCAN0RMPTR27H RSCAN0.RMPTR27.uint16_t[H]
#define RSCAN0RMPTR27HL RSCAN0.RMPTR27.uint8_t[HL]
#define RSCAN0RMPTR27HH RSCAN0.RMPTR27.uint8_t[HH]
#define RSCAN0RMDF027 RSCAN0.RMDF027.uint32_t
#define RSCAN0RMDF027L RSCAN0.RMDF027.uint16_t[L]
#define RSCAN0RMDF027LL RSCAN0.RMDF027.uint8_t[LL]
#define RSCAN0RMDF027LH RSCAN0.RMDF027.uint8_t[LH]
#define RSCAN0RMDF027H RSCAN0.RMDF027.uint16_t[H]
#define RSCAN0RMDF027HL RSCAN0.RMDF027.uint8_t[HL]
#define RSCAN0RMDF027HH RSCAN0.RMDF027.uint8_t[HH]
#define RSCAN0RMDF127 RSCAN0.RMDF127.uint32_t
#define RSCAN0RMDF127L RSCAN0.RMDF127.uint16_t[L]
#define RSCAN0RMDF127LL RSCAN0.RMDF127.uint8_t[LL]
#define RSCAN0RMDF127LH RSCAN0.RMDF127.uint8_t[LH]
#define RSCAN0RMDF127H RSCAN0.RMDF127.uint16_t[H]
#define RSCAN0RMDF127HL RSCAN0.RMDF127.uint8_t[HL]
#define RSCAN0RMDF127HH RSCAN0.RMDF127.uint8_t[HH]
#define RSCAN0RMID28 RSCAN0.RMID28.uint32_t
#define RSCAN0RMID28L RSCAN0.RMID28.uint16_t[L]
#define RSCAN0RMID28LL RSCAN0.RMID28.uint8_t[LL]
#define RSCAN0RMID28LH RSCAN0.RMID28.uint8_t[LH]
#define RSCAN0RMID28H RSCAN0.RMID28.uint16_t[H]
#define RSCAN0RMID28HL RSCAN0.RMID28.uint8_t[HL]
#define RSCAN0RMID28HH RSCAN0.RMID28.uint8_t[HH]
#define RSCAN0RMPTR28 RSCAN0.RMPTR28.uint32_t
#define RSCAN0RMPTR28L RSCAN0.RMPTR28.uint16_t[L]
#define RSCAN0RMPTR28LL RSCAN0.RMPTR28.uint8_t[LL]
#define RSCAN0RMPTR28LH RSCAN0.RMPTR28.uint8_t[LH]
#define RSCAN0RMPTR28H RSCAN0.RMPTR28.uint16_t[H]
#define RSCAN0RMPTR28HL RSCAN0.RMPTR28.uint8_t[HL]
#define RSCAN0RMPTR28HH RSCAN0.RMPTR28.uint8_t[HH]
#define RSCAN0RMDF028 RSCAN0.RMDF028.uint32_t
#define RSCAN0RMDF028L RSCAN0.RMDF028.uint16_t[L]
#define RSCAN0RMDF028LL RSCAN0.RMDF028.uint8_t[LL]
#define RSCAN0RMDF028LH RSCAN0.RMDF028.uint8_t[LH]
#define RSCAN0RMDF028H RSCAN0.RMDF028.uint16_t[H]
#define RSCAN0RMDF028HL RSCAN0.RMDF028.uint8_t[HL]
#define RSCAN0RMDF028HH RSCAN0.RMDF028.uint8_t[HH]
#define RSCAN0RMDF128 RSCAN0.RMDF128.uint32_t
#define RSCAN0RMDF128L RSCAN0.RMDF128.uint16_t[L]
#define RSCAN0RMDF128LL RSCAN0.RMDF128.uint8_t[LL]
#define RSCAN0RMDF128LH RSCAN0.RMDF128.uint8_t[LH]
#define RSCAN0RMDF128H RSCAN0.RMDF128.uint16_t[H]
#define RSCAN0RMDF128HL RSCAN0.RMDF128.uint8_t[HL]
#define RSCAN0RMDF128HH RSCAN0.RMDF128.uint8_t[HH]
#define RSCAN0RMID29 RSCAN0.RMID29.uint32_t
#define RSCAN0RMID29L RSCAN0.RMID29.uint16_t[L]
#define RSCAN0RMID29LL RSCAN0.RMID29.uint8_t[LL]
#define RSCAN0RMID29LH RSCAN0.RMID29.uint8_t[LH]
#define RSCAN0RMID29H RSCAN0.RMID29.uint16_t[H]
#define RSCAN0RMID29HL RSCAN0.RMID29.uint8_t[HL]
#define RSCAN0RMID29HH RSCAN0.RMID29.uint8_t[HH]
#define RSCAN0RMPTR29 RSCAN0.RMPTR29.uint32_t
#define RSCAN0RMPTR29L RSCAN0.RMPTR29.uint16_t[L]
#define RSCAN0RMPTR29LL RSCAN0.RMPTR29.uint8_t[LL]
#define RSCAN0RMPTR29LH RSCAN0.RMPTR29.uint8_t[LH]
#define RSCAN0RMPTR29H RSCAN0.RMPTR29.uint16_t[H]
#define RSCAN0RMPTR29HL RSCAN0.RMPTR29.uint8_t[HL]
#define RSCAN0RMPTR29HH RSCAN0.RMPTR29.uint8_t[HH]
#define RSCAN0RMDF029 RSCAN0.RMDF029.uint32_t
#define RSCAN0RMDF029L RSCAN0.RMDF029.uint16_t[L]
#define RSCAN0RMDF029LL RSCAN0.RMDF029.uint8_t[LL]
#define RSCAN0RMDF029LH RSCAN0.RMDF029.uint8_t[LH]
#define RSCAN0RMDF029H RSCAN0.RMDF029.uint16_t[H]
#define RSCAN0RMDF029HL RSCAN0.RMDF029.uint8_t[HL]
#define RSCAN0RMDF029HH RSCAN0.RMDF029.uint8_t[HH]
#define RSCAN0RMDF129 RSCAN0.RMDF129.uint32_t
#define RSCAN0RMDF129L RSCAN0.RMDF129.uint16_t[L]
#define RSCAN0RMDF129LL RSCAN0.RMDF129.uint8_t[LL]
#define RSCAN0RMDF129LH RSCAN0.RMDF129.uint8_t[LH]
#define RSCAN0RMDF129H RSCAN0.RMDF129.uint16_t[H]
#define RSCAN0RMDF129HL RSCAN0.RMDF129.uint8_t[HL]
#define RSCAN0RMDF129HH RSCAN0.RMDF129.uint8_t[HH]
#define RSCAN0RMID30 RSCAN0.RMID30.uint32_t
#define RSCAN0RMID30L RSCAN0.RMID30.uint16_t[L]
#define RSCAN0RMID30LL RSCAN0.RMID30.uint8_t[LL]
#define RSCAN0RMID30LH RSCAN0.RMID30.uint8_t[LH]
#define RSCAN0RMID30H RSCAN0.RMID30.uint16_t[H]
#define RSCAN0RMID30HL RSCAN0.RMID30.uint8_t[HL]
#define RSCAN0RMID30HH RSCAN0.RMID30.uint8_t[HH]
#define RSCAN0RMPTR30 RSCAN0.RMPTR30.uint32_t
#define RSCAN0RMPTR30L RSCAN0.RMPTR30.uint16_t[L]
#define RSCAN0RMPTR30LL RSCAN0.RMPTR30.uint8_t[LL]
#define RSCAN0RMPTR30LH RSCAN0.RMPTR30.uint8_t[LH]
#define RSCAN0RMPTR30H RSCAN0.RMPTR30.uint16_t[H]
#define RSCAN0RMPTR30HL RSCAN0.RMPTR30.uint8_t[HL]
#define RSCAN0RMPTR30HH RSCAN0.RMPTR30.uint8_t[HH]
#define RSCAN0RMDF030 RSCAN0.RMDF030.uint32_t
#define RSCAN0RMDF030L RSCAN0.RMDF030.uint16_t[L]
#define RSCAN0RMDF030LL RSCAN0.RMDF030.uint8_t[LL]
#define RSCAN0RMDF030LH RSCAN0.RMDF030.uint8_t[LH]
#define RSCAN0RMDF030H RSCAN0.RMDF030.uint16_t[H]
#define RSCAN0RMDF030HL RSCAN0.RMDF030.uint8_t[HL]
#define RSCAN0RMDF030HH RSCAN0.RMDF030.uint8_t[HH]
#define RSCAN0RMDF130 RSCAN0.RMDF130.uint32_t
#define RSCAN0RMDF130L RSCAN0.RMDF130.uint16_t[L]
#define RSCAN0RMDF130LL RSCAN0.RMDF130.uint8_t[LL]
#define RSCAN0RMDF130LH RSCAN0.RMDF130.uint8_t[LH]
#define RSCAN0RMDF130H RSCAN0.RMDF130.uint16_t[H]
#define RSCAN0RMDF130HL RSCAN0.RMDF130.uint8_t[HL]
#define RSCAN0RMDF130HH RSCAN0.RMDF130.uint8_t[HH]
#define RSCAN0RMID31 RSCAN0.RMID31.uint32_t
#define RSCAN0RMID31L RSCAN0.RMID31.uint16_t[L]
#define RSCAN0RMID31LL RSCAN0.RMID31.uint8_t[LL]
#define RSCAN0RMID31LH RSCAN0.RMID31.uint8_t[LH]
#define RSCAN0RMID31H RSCAN0.RMID31.uint16_t[H]
#define RSCAN0RMID31HL RSCAN0.RMID31.uint8_t[HL]
#define RSCAN0RMID31HH RSCAN0.RMID31.uint8_t[HH]
#define RSCAN0RMPTR31 RSCAN0.RMPTR31.uint32_t
#define RSCAN0RMPTR31L RSCAN0.RMPTR31.uint16_t[L]
#define RSCAN0RMPTR31LL RSCAN0.RMPTR31.uint8_t[LL]
#define RSCAN0RMPTR31LH RSCAN0.RMPTR31.uint8_t[LH]
#define RSCAN0RMPTR31H RSCAN0.RMPTR31.uint16_t[H]
#define RSCAN0RMPTR31HL RSCAN0.RMPTR31.uint8_t[HL]
#define RSCAN0RMPTR31HH RSCAN0.RMPTR31.uint8_t[HH]
#define RSCAN0RMDF031 RSCAN0.RMDF031.uint32_t
#define RSCAN0RMDF031L RSCAN0.RMDF031.uint16_t[L]
#define RSCAN0RMDF031LL RSCAN0.RMDF031.uint8_t[LL]
#define RSCAN0RMDF031LH RSCAN0.RMDF031.uint8_t[LH]
#define RSCAN0RMDF031H RSCAN0.RMDF031.uint16_t[H]
#define RSCAN0RMDF031HL RSCAN0.RMDF031.uint8_t[HL]
#define RSCAN0RMDF031HH RSCAN0.RMDF031.uint8_t[HH]
#define RSCAN0RMDF131 RSCAN0.RMDF131.uint32_t
#define RSCAN0RMDF131L RSCAN0.RMDF131.uint16_t[L]
#define RSCAN0RMDF131LL RSCAN0.RMDF131.uint8_t[LL]
#define RSCAN0RMDF131LH RSCAN0.RMDF131.uint8_t[LH]
#define RSCAN0RMDF131H RSCAN0.RMDF131.uint16_t[H]
#define RSCAN0RMDF131HL RSCAN0.RMDF131.uint8_t[HL]
#define RSCAN0RMDF131HH RSCAN0.RMDF131.uint8_t[HH]
#define RSCAN0RMID32 RSCAN0.RMID32.uint32_t
#define RSCAN0RMID32L RSCAN0.RMID32.uint16_t[L]
#define RSCAN0RMID32LL RSCAN0.RMID32.uint8_t[LL]
#define RSCAN0RMID32LH RSCAN0.RMID32.uint8_t[LH]
#define RSCAN0RMID32H RSCAN0.RMID32.uint16_t[H]
#define RSCAN0RMID32HL RSCAN0.RMID32.uint8_t[HL]
#define RSCAN0RMID32HH RSCAN0.RMID32.uint8_t[HH]
#define RSCAN0RMPTR32 RSCAN0.RMPTR32.uint32_t
#define RSCAN0RMPTR32L RSCAN0.RMPTR32.uint16_t[L]
#define RSCAN0RMPTR32LL RSCAN0.RMPTR32.uint8_t[LL]
#define RSCAN0RMPTR32LH RSCAN0.RMPTR32.uint8_t[LH]
#define RSCAN0RMPTR32H RSCAN0.RMPTR32.uint16_t[H]
#define RSCAN0RMPTR32HL RSCAN0.RMPTR32.uint8_t[HL]
#define RSCAN0RMPTR32HH RSCAN0.RMPTR32.uint8_t[HH]
#define RSCAN0RMDF032 RSCAN0.RMDF032.uint32_t
#define RSCAN0RMDF032L RSCAN0.RMDF032.uint16_t[L]
#define RSCAN0RMDF032LL RSCAN0.RMDF032.uint8_t[LL]
#define RSCAN0RMDF032LH RSCAN0.RMDF032.uint8_t[LH]
#define RSCAN0RMDF032H RSCAN0.RMDF032.uint16_t[H]
#define RSCAN0RMDF032HL RSCAN0.RMDF032.uint8_t[HL]
#define RSCAN0RMDF032HH RSCAN0.RMDF032.uint8_t[HH]
#define RSCAN0RMDF132 RSCAN0.RMDF132.uint32_t
#define RSCAN0RMDF132L RSCAN0.RMDF132.uint16_t[L]
#define RSCAN0RMDF132LL RSCAN0.RMDF132.uint8_t[LL]
#define RSCAN0RMDF132LH RSCAN0.RMDF132.uint8_t[LH]
#define RSCAN0RMDF132H RSCAN0.RMDF132.uint16_t[H]
#define RSCAN0RMDF132HL RSCAN0.RMDF132.uint8_t[HL]
#define RSCAN0RMDF132HH RSCAN0.RMDF132.uint8_t[HH]
#define RSCAN0RMID33 RSCAN0.RMID33.uint32_t
#define RSCAN0RMID33L RSCAN0.RMID33.uint16_t[L]
#define RSCAN0RMID33LL RSCAN0.RMID33.uint8_t[LL]
#define RSCAN0RMID33LH RSCAN0.RMID33.uint8_t[LH]
#define RSCAN0RMID33H RSCAN0.RMID33.uint16_t[H]
#define RSCAN0RMID33HL RSCAN0.RMID33.uint8_t[HL]
#define RSCAN0RMID33HH RSCAN0.RMID33.uint8_t[HH]
#define RSCAN0RMPTR33 RSCAN0.RMPTR33.uint32_t
#define RSCAN0RMPTR33L RSCAN0.RMPTR33.uint16_t[L]
#define RSCAN0RMPTR33LL RSCAN0.RMPTR33.uint8_t[LL]
#define RSCAN0RMPTR33LH RSCAN0.RMPTR33.uint8_t[LH]
#define RSCAN0RMPTR33H RSCAN0.RMPTR33.uint16_t[H]
#define RSCAN0RMPTR33HL RSCAN0.RMPTR33.uint8_t[HL]
#define RSCAN0RMPTR33HH RSCAN0.RMPTR33.uint8_t[HH]
#define RSCAN0RMDF033 RSCAN0.RMDF033.uint32_t
#define RSCAN0RMDF033L RSCAN0.RMDF033.uint16_t[L]
#define RSCAN0RMDF033LL RSCAN0.RMDF033.uint8_t[LL]
#define RSCAN0RMDF033LH RSCAN0.RMDF033.uint8_t[LH]
#define RSCAN0RMDF033H RSCAN0.RMDF033.uint16_t[H]
#define RSCAN0RMDF033HL RSCAN0.RMDF033.uint8_t[HL]
#define RSCAN0RMDF033HH RSCAN0.RMDF033.uint8_t[HH]
#define RSCAN0RMDF133 RSCAN0.RMDF133.uint32_t
#define RSCAN0RMDF133L RSCAN0.RMDF133.uint16_t[L]
#define RSCAN0RMDF133LL RSCAN0.RMDF133.uint8_t[LL]
#define RSCAN0RMDF133LH RSCAN0.RMDF133.uint8_t[LH]
#define RSCAN0RMDF133H RSCAN0.RMDF133.uint16_t[H]
#define RSCAN0RMDF133HL RSCAN0.RMDF133.uint8_t[HL]
#define RSCAN0RMDF133HH RSCAN0.RMDF133.uint8_t[HH]
#define RSCAN0RMID34 RSCAN0.RMID34.uint32_t
#define RSCAN0RMID34L RSCAN0.RMID34.uint16_t[L]
#define RSCAN0RMID34LL RSCAN0.RMID34.uint8_t[LL]
#define RSCAN0RMID34LH RSCAN0.RMID34.uint8_t[LH]
#define RSCAN0RMID34H RSCAN0.RMID34.uint16_t[H]
#define RSCAN0RMID34HL RSCAN0.RMID34.uint8_t[HL]
#define RSCAN0RMID34HH RSCAN0.RMID34.uint8_t[HH]
#define RSCAN0RMPTR34 RSCAN0.RMPTR34.uint32_t
#define RSCAN0RMPTR34L RSCAN0.RMPTR34.uint16_t[L]
#define RSCAN0RMPTR34LL RSCAN0.RMPTR34.uint8_t[LL]
#define RSCAN0RMPTR34LH RSCAN0.RMPTR34.uint8_t[LH]
#define RSCAN0RMPTR34H RSCAN0.RMPTR34.uint16_t[H]
#define RSCAN0RMPTR34HL RSCAN0.RMPTR34.uint8_t[HL]
#define RSCAN0RMPTR34HH RSCAN0.RMPTR34.uint8_t[HH]
#define RSCAN0RMDF034 RSCAN0.RMDF034.uint32_t
#define RSCAN0RMDF034L RSCAN0.RMDF034.uint16_t[L]
#define RSCAN0RMDF034LL RSCAN0.RMDF034.uint8_t[LL]
#define RSCAN0RMDF034LH RSCAN0.RMDF034.uint8_t[LH]
#define RSCAN0RMDF034H RSCAN0.RMDF034.uint16_t[H]
#define RSCAN0RMDF034HL RSCAN0.RMDF034.uint8_t[HL]
#define RSCAN0RMDF034HH RSCAN0.RMDF034.uint8_t[HH]
#define RSCAN0RMDF134 RSCAN0.RMDF134.uint32_t
#define RSCAN0RMDF134L RSCAN0.RMDF134.uint16_t[L]
#define RSCAN0RMDF134LL RSCAN0.RMDF134.uint8_t[LL]
#define RSCAN0RMDF134LH RSCAN0.RMDF134.uint8_t[LH]
#define RSCAN0RMDF134H RSCAN0.RMDF134.uint16_t[H]
#define RSCAN0RMDF134HL RSCAN0.RMDF134.uint8_t[HL]
#define RSCAN0RMDF134HH RSCAN0.RMDF134.uint8_t[HH]
#define RSCAN0RMID35 RSCAN0.RMID35.uint32_t
#define RSCAN0RMID35L RSCAN0.RMID35.uint16_t[L]
#define RSCAN0RMID35LL RSCAN0.RMID35.uint8_t[LL]
#define RSCAN0RMID35LH RSCAN0.RMID35.uint8_t[LH]
#define RSCAN0RMID35H RSCAN0.RMID35.uint16_t[H]
#define RSCAN0RMID35HL RSCAN0.RMID35.uint8_t[HL]
#define RSCAN0RMID35HH RSCAN0.RMID35.uint8_t[HH]
#define RSCAN0RMPTR35 RSCAN0.RMPTR35.uint32_t
#define RSCAN0RMPTR35L RSCAN0.RMPTR35.uint16_t[L]
#define RSCAN0RMPTR35LL RSCAN0.RMPTR35.uint8_t[LL]
#define RSCAN0RMPTR35LH RSCAN0.RMPTR35.uint8_t[LH]
#define RSCAN0RMPTR35H RSCAN0.RMPTR35.uint16_t[H]
#define RSCAN0RMPTR35HL RSCAN0.RMPTR35.uint8_t[HL]
#define RSCAN0RMPTR35HH RSCAN0.RMPTR35.uint8_t[HH]
#define RSCAN0RMDF035 RSCAN0.RMDF035.uint32_t
#define RSCAN0RMDF035L RSCAN0.RMDF035.uint16_t[L]
#define RSCAN0RMDF035LL RSCAN0.RMDF035.uint8_t[LL]
#define RSCAN0RMDF035LH RSCAN0.RMDF035.uint8_t[LH]
#define RSCAN0RMDF035H RSCAN0.RMDF035.uint16_t[H]
#define RSCAN0RMDF035HL RSCAN0.RMDF035.uint8_t[HL]
#define RSCAN0RMDF035HH RSCAN0.RMDF035.uint8_t[HH]
#define RSCAN0RMDF135 RSCAN0.RMDF135.uint32_t
#define RSCAN0RMDF135L RSCAN0.RMDF135.uint16_t[L]
#define RSCAN0RMDF135LL RSCAN0.RMDF135.uint8_t[LL]
#define RSCAN0RMDF135LH RSCAN0.RMDF135.uint8_t[LH]
#define RSCAN0RMDF135H RSCAN0.RMDF135.uint16_t[H]
#define RSCAN0RMDF135HL RSCAN0.RMDF135.uint8_t[HL]
#define RSCAN0RMDF135HH RSCAN0.RMDF135.uint8_t[HH]
#define RSCAN0RMID36 RSCAN0.RMID36.uint32_t
#define RSCAN0RMID36L RSCAN0.RMID36.uint16_t[L]
#define RSCAN0RMID36LL RSCAN0.RMID36.uint8_t[LL]
#define RSCAN0RMID36LH RSCAN0.RMID36.uint8_t[LH]
#define RSCAN0RMID36H RSCAN0.RMID36.uint16_t[H]
#define RSCAN0RMID36HL RSCAN0.RMID36.uint8_t[HL]
#define RSCAN0RMID36HH RSCAN0.RMID36.uint8_t[HH]
#define RSCAN0RMPTR36 RSCAN0.RMPTR36.uint32_t
#define RSCAN0RMPTR36L RSCAN0.RMPTR36.uint16_t[L]
#define RSCAN0RMPTR36LL RSCAN0.RMPTR36.uint8_t[LL]
#define RSCAN0RMPTR36LH RSCAN0.RMPTR36.uint8_t[LH]
#define RSCAN0RMPTR36H RSCAN0.RMPTR36.uint16_t[H]
#define RSCAN0RMPTR36HL RSCAN0.RMPTR36.uint8_t[HL]
#define RSCAN0RMPTR36HH RSCAN0.RMPTR36.uint8_t[HH]
#define RSCAN0RMDF036 RSCAN0.RMDF036.uint32_t
#define RSCAN0RMDF036L RSCAN0.RMDF036.uint16_t[L]
#define RSCAN0RMDF036LL RSCAN0.RMDF036.uint8_t[LL]
#define RSCAN0RMDF036LH RSCAN0.RMDF036.uint8_t[LH]
#define RSCAN0RMDF036H RSCAN0.RMDF036.uint16_t[H]
#define RSCAN0RMDF036HL RSCAN0.RMDF036.uint8_t[HL]
#define RSCAN0RMDF036HH RSCAN0.RMDF036.uint8_t[HH]
#define RSCAN0RMDF136 RSCAN0.RMDF136.uint32_t
#define RSCAN0RMDF136L RSCAN0.RMDF136.uint16_t[L]
#define RSCAN0RMDF136LL RSCAN0.RMDF136.uint8_t[LL]
#define RSCAN0RMDF136LH RSCAN0.RMDF136.uint8_t[LH]
#define RSCAN0RMDF136H RSCAN0.RMDF136.uint16_t[H]
#define RSCAN0RMDF136HL RSCAN0.RMDF136.uint8_t[HL]
#define RSCAN0RMDF136HH RSCAN0.RMDF136.uint8_t[HH]
#define RSCAN0RMID37 RSCAN0.RMID37.uint32_t
#define RSCAN0RMID37L RSCAN0.RMID37.uint16_t[L]
#define RSCAN0RMID37LL RSCAN0.RMID37.uint8_t[LL]
#define RSCAN0RMID37LH RSCAN0.RMID37.uint8_t[LH]
#define RSCAN0RMID37H RSCAN0.RMID37.uint16_t[H]
#define RSCAN0RMID37HL RSCAN0.RMID37.uint8_t[HL]
#define RSCAN0RMID37HH RSCAN0.RMID37.uint8_t[HH]
#define RSCAN0RMPTR37 RSCAN0.RMPTR37.uint32_t
#define RSCAN0RMPTR37L RSCAN0.RMPTR37.uint16_t[L]
#define RSCAN0RMPTR37LL RSCAN0.RMPTR37.uint8_t[LL]
#define RSCAN0RMPTR37LH RSCAN0.RMPTR37.uint8_t[LH]
#define RSCAN0RMPTR37H RSCAN0.RMPTR37.uint16_t[H]
#define RSCAN0RMPTR37HL RSCAN0.RMPTR37.uint8_t[HL]
#define RSCAN0RMPTR37HH RSCAN0.RMPTR37.uint8_t[HH]
#define RSCAN0RMDF037 RSCAN0.RMDF037.uint32_t
#define RSCAN0RMDF037L RSCAN0.RMDF037.uint16_t[L]
#define RSCAN0RMDF037LL RSCAN0.RMDF037.uint8_t[LL]
#define RSCAN0RMDF037LH RSCAN0.RMDF037.uint8_t[LH]
#define RSCAN0RMDF037H RSCAN0.RMDF037.uint16_t[H]
#define RSCAN0RMDF037HL RSCAN0.RMDF037.uint8_t[HL]
#define RSCAN0RMDF037HH RSCAN0.RMDF037.uint8_t[HH]
#define RSCAN0RMDF137 RSCAN0.RMDF137.uint32_t
#define RSCAN0RMDF137L RSCAN0.RMDF137.uint16_t[L]
#define RSCAN0RMDF137LL RSCAN0.RMDF137.uint8_t[LL]
#define RSCAN0RMDF137LH RSCAN0.RMDF137.uint8_t[LH]
#define RSCAN0RMDF137H RSCAN0.RMDF137.uint16_t[H]
#define RSCAN0RMDF137HL RSCAN0.RMDF137.uint8_t[HL]
#define RSCAN0RMDF137HH RSCAN0.RMDF137.uint8_t[HH]
#define RSCAN0RMID38 RSCAN0.RMID38.uint32_t
#define RSCAN0RMID38L RSCAN0.RMID38.uint16_t[L]
#define RSCAN0RMID38LL RSCAN0.RMID38.uint8_t[LL]
#define RSCAN0RMID38LH RSCAN0.RMID38.uint8_t[LH]
#define RSCAN0RMID38H RSCAN0.RMID38.uint16_t[H]
#define RSCAN0RMID38HL RSCAN0.RMID38.uint8_t[HL]
#define RSCAN0RMID38HH RSCAN0.RMID38.uint8_t[HH]
#define RSCAN0RMPTR38 RSCAN0.RMPTR38.uint32_t
#define RSCAN0RMPTR38L RSCAN0.RMPTR38.uint16_t[L]
#define RSCAN0RMPTR38LL RSCAN0.RMPTR38.uint8_t[LL]
#define RSCAN0RMPTR38LH RSCAN0.RMPTR38.uint8_t[LH]
#define RSCAN0RMPTR38H RSCAN0.RMPTR38.uint16_t[H]
#define RSCAN0RMPTR38HL RSCAN0.RMPTR38.uint8_t[HL]
#define RSCAN0RMPTR38HH RSCAN0.RMPTR38.uint8_t[HH]
#define RSCAN0RMDF038 RSCAN0.RMDF038.uint32_t
#define RSCAN0RMDF038L RSCAN0.RMDF038.uint16_t[L]
#define RSCAN0RMDF038LL RSCAN0.RMDF038.uint8_t[LL]
#define RSCAN0RMDF038LH RSCAN0.RMDF038.uint8_t[LH]
#define RSCAN0RMDF038H RSCAN0.RMDF038.uint16_t[H]
#define RSCAN0RMDF038HL RSCAN0.RMDF038.uint8_t[HL]
#define RSCAN0RMDF038HH RSCAN0.RMDF038.uint8_t[HH]
#define RSCAN0RMDF138 RSCAN0.RMDF138.uint32_t
#define RSCAN0RMDF138L RSCAN0.RMDF138.uint16_t[L]
#define RSCAN0RMDF138LL RSCAN0.RMDF138.uint8_t[LL]
#define RSCAN0RMDF138LH RSCAN0.RMDF138.uint8_t[LH]
#define RSCAN0RMDF138H RSCAN0.RMDF138.uint16_t[H]
#define RSCAN0RMDF138HL RSCAN0.RMDF138.uint8_t[HL]
#define RSCAN0RMDF138HH RSCAN0.RMDF138.uint8_t[HH]
#define RSCAN0RMID39 RSCAN0.RMID39.uint32_t
#define RSCAN0RMID39L RSCAN0.RMID39.uint16_t[L]
#define RSCAN0RMID39LL RSCAN0.RMID39.uint8_t[LL]
#define RSCAN0RMID39LH RSCAN0.RMID39.uint8_t[LH]
#define RSCAN0RMID39H RSCAN0.RMID39.uint16_t[H]
#define RSCAN0RMID39HL RSCAN0.RMID39.uint8_t[HL]
#define RSCAN0RMID39HH RSCAN0.RMID39.uint8_t[HH]
#define RSCAN0RMPTR39 RSCAN0.RMPTR39.uint32_t
#define RSCAN0RMPTR39L RSCAN0.RMPTR39.uint16_t[L]
#define RSCAN0RMPTR39LL RSCAN0.RMPTR39.uint8_t[LL]
#define RSCAN0RMPTR39LH RSCAN0.RMPTR39.uint8_t[LH]
#define RSCAN0RMPTR39H RSCAN0.RMPTR39.uint16_t[H]
#define RSCAN0RMPTR39HL RSCAN0.RMPTR39.uint8_t[HL]
#define RSCAN0RMPTR39HH RSCAN0.RMPTR39.uint8_t[HH]
#define RSCAN0RMDF039 RSCAN0.RMDF039.uint32_t
#define RSCAN0RMDF039L RSCAN0.RMDF039.uint16_t[L]
#define RSCAN0RMDF039LL RSCAN0.RMDF039.uint8_t[LL]
#define RSCAN0RMDF039LH RSCAN0.RMDF039.uint8_t[LH]
#define RSCAN0RMDF039H RSCAN0.RMDF039.uint16_t[H]
#define RSCAN0RMDF039HL RSCAN0.RMDF039.uint8_t[HL]
#define RSCAN0RMDF039HH RSCAN0.RMDF039.uint8_t[HH]
#define RSCAN0RMDF139 RSCAN0.RMDF139.uint32_t
#define RSCAN0RMDF139L RSCAN0.RMDF139.uint16_t[L]
#define RSCAN0RMDF139LL RSCAN0.RMDF139.uint8_t[LL]
#define RSCAN0RMDF139LH RSCAN0.RMDF139.uint8_t[LH]
#define RSCAN0RMDF139H RSCAN0.RMDF139.uint16_t[H]
#define RSCAN0RMDF139HL RSCAN0.RMDF139.uint8_t[HL]
#define RSCAN0RMDF139HH RSCAN0.RMDF139.uint8_t[HH]
#define RSCAN0RMID40 RSCAN0.RMID40.uint32_t
#define RSCAN0RMID40L RSCAN0.RMID40.uint16_t[L]
#define RSCAN0RMID40LL RSCAN0.RMID40.uint8_t[LL]
#define RSCAN0RMID40LH RSCAN0.RMID40.uint8_t[LH]
#define RSCAN0RMID40H RSCAN0.RMID40.uint16_t[H]
#define RSCAN0RMID40HL RSCAN0.RMID40.uint8_t[HL]
#define RSCAN0RMID40HH RSCAN0.RMID40.uint8_t[HH]
#define RSCAN0RMPTR40 RSCAN0.RMPTR40.uint32_t
#define RSCAN0RMPTR40L RSCAN0.RMPTR40.uint16_t[L]
#define RSCAN0RMPTR40LL RSCAN0.RMPTR40.uint8_t[LL]
#define RSCAN0RMPTR40LH RSCAN0.RMPTR40.uint8_t[LH]
#define RSCAN0RMPTR40H RSCAN0.RMPTR40.uint16_t[H]
#define RSCAN0RMPTR40HL RSCAN0.RMPTR40.uint8_t[HL]
#define RSCAN0RMPTR40HH RSCAN0.RMPTR40.uint8_t[HH]
#define RSCAN0RMDF040 RSCAN0.RMDF040.uint32_t
#define RSCAN0RMDF040L RSCAN0.RMDF040.uint16_t[L]
#define RSCAN0RMDF040LL RSCAN0.RMDF040.uint8_t[LL]
#define RSCAN0RMDF040LH RSCAN0.RMDF040.uint8_t[LH]
#define RSCAN0RMDF040H RSCAN0.RMDF040.uint16_t[H]
#define RSCAN0RMDF040HL RSCAN0.RMDF040.uint8_t[HL]
#define RSCAN0RMDF040HH RSCAN0.RMDF040.uint8_t[HH]
#define RSCAN0RMDF140 RSCAN0.RMDF140.uint32_t
#define RSCAN0RMDF140L RSCAN0.RMDF140.uint16_t[L]
#define RSCAN0RMDF140LL RSCAN0.RMDF140.uint8_t[LL]
#define RSCAN0RMDF140LH RSCAN0.RMDF140.uint8_t[LH]
#define RSCAN0RMDF140H RSCAN0.RMDF140.uint16_t[H]
#define RSCAN0RMDF140HL RSCAN0.RMDF140.uint8_t[HL]
#define RSCAN0RMDF140HH RSCAN0.RMDF140.uint8_t[HH]
#define RSCAN0RMID41 RSCAN0.RMID41.uint32_t
#define RSCAN0RMID41L RSCAN0.RMID41.uint16_t[L]
#define RSCAN0RMID41LL RSCAN0.RMID41.uint8_t[LL]
#define RSCAN0RMID41LH RSCAN0.RMID41.uint8_t[LH]
#define RSCAN0RMID41H RSCAN0.RMID41.uint16_t[H]
#define RSCAN0RMID41HL RSCAN0.RMID41.uint8_t[HL]
#define RSCAN0RMID41HH RSCAN0.RMID41.uint8_t[HH]
#define RSCAN0RMPTR41 RSCAN0.RMPTR41.uint32_t
#define RSCAN0RMPTR41L RSCAN0.RMPTR41.uint16_t[L]
#define RSCAN0RMPTR41LL RSCAN0.RMPTR41.uint8_t[LL]
#define RSCAN0RMPTR41LH RSCAN0.RMPTR41.uint8_t[LH]
#define RSCAN0RMPTR41H RSCAN0.RMPTR41.uint16_t[H]
#define RSCAN0RMPTR41HL RSCAN0.RMPTR41.uint8_t[HL]
#define RSCAN0RMPTR41HH RSCAN0.RMPTR41.uint8_t[HH]
#define RSCAN0RMDF041 RSCAN0.RMDF041.uint32_t
#define RSCAN0RMDF041L RSCAN0.RMDF041.uint16_t[L]
#define RSCAN0RMDF041LL RSCAN0.RMDF041.uint8_t[LL]
#define RSCAN0RMDF041LH RSCAN0.RMDF041.uint8_t[LH]
#define RSCAN0RMDF041H RSCAN0.RMDF041.uint16_t[H]
#define RSCAN0RMDF041HL RSCAN0.RMDF041.uint8_t[HL]
#define RSCAN0RMDF041HH RSCAN0.RMDF041.uint8_t[HH]
#define RSCAN0RMDF141 RSCAN0.RMDF141.uint32_t
#define RSCAN0RMDF141L RSCAN0.RMDF141.uint16_t[L]
#define RSCAN0RMDF141LL RSCAN0.RMDF141.uint8_t[LL]
#define RSCAN0RMDF141LH RSCAN0.RMDF141.uint8_t[LH]
#define RSCAN0RMDF141H RSCAN0.RMDF141.uint16_t[H]
#define RSCAN0RMDF141HL RSCAN0.RMDF141.uint8_t[HL]
#define RSCAN0RMDF141HH RSCAN0.RMDF141.uint8_t[HH]
#define RSCAN0RMID42 RSCAN0.RMID42.uint32_t
#define RSCAN0RMID42L RSCAN0.RMID42.uint16_t[L]
#define RSCAN0RMID42LL RSCAN0.RMID42.uint8_t[LL]
#define RSCAN0RMID42LH RSCAN0.RMID42.uint8_t[LH]
#define RSCAN0RMID42H RSCAN0.RMID42.uint16_t[H]
#define RSCAN0RMID42HL RSCAN0.RMID42.uint8_t[HL]
#define RSCAN0RMID42HH RSCAN0.RMID42.uint8_t[HH]
#define RSCAN0RMPTR42 RSCAN0.RMPTR42.uint32_t
#define RSCAN0RMPTR42L RSCAN0.RMPTR42.uint16_t[L]
#define RSCAN0RMPTR42LL RSCAN0.RMPTR42.uint8_t[LL]
#define RSCAN0RMPTR42LH RSCAN0.RMPTR42.uint8_t[LH]
#define RSCAN0RMPTR42H RSCAN0.RMPTR42.uint16_t[H]
#define RSCAN0RMPTR42HL RSCAN0.RMPTR42.uint8_t[HL]
#define RSCAN0RMPTR42HH RSCAN0.RMPTR42.uint8_t[HH]
#define RSCAN0RMDF042 RSCAN0.RMDF042.uint32_t
#define RSCAN0RMDF042L RSCAN0.RMDF042.uint16_t[L]
#define RSCAN0RMDF042LL RSCAN0.RMDF042.uint8_t[LL]
#define RSCAN0RMDF042LH RSCAN0.RMDF042.uint8_t[LH]
#define RSCAN0RMDF042H RSCAN0.RMDF042.uint16_t[H]
#define RSCAN0RMDF042HL RSCAN0.RMDF042.uint8_t[HL]
#define RSCAN0RMDF042HH RSCAN0.RMDF042.uint8_t[HH]
#define RSCAN0RMDF142 RSCAN0.RMDF142.uint32_t
#define RSCAN0RMDF142L RSCAN0.RMDF142.uint16_t[L]
#define RSCAN0RMDF142LL RSCAN0.RMDF142.uint8_t[LL]
#define RSCAN0RMDF142LH RSCAN0.RMDF142.uint8_t[LH]
#define RSCAN0RMDF142H RSCAN0.RMDF142.uint16_t[H]
#define RSCAN0RMDF142HL RSCAN0.RMDF142.uint8_t[HL]
#define RSCAN0RMDF142HH RSCAN0.RMDF142.uint8_t[HH]
#define RSCAN0RMID43 RSCAN0.RMID43.uint32_t
#define RSCAN0RMID43L RSCAN0.RMID43.uint16_t[L]
#define RSCAN0RMID43LL RSCAN0.RMID43.uint8_t[LL]
#define RSCAN0RMID43LH RSCAN0.RMID43.uint8_t[LH]
#define RSCAN0RMID43H RSCAN0.RMID43.uint16_t[H]
#define RSCAN0RMID43HL RSCAN0.RMID43.uint8_t[HL]
#define RSCAN0RMID43HH RSCAN0.RMID43.uint8_t[HH]
#define RSCAN0RMPTR43 RSCAN0.RMPTR43.uint32_t
#define RSCAN0RMPTR43L RSCAN0.RMPTR43.uint16_t[L]
#define RSCAN0RMPTR43LL RSCAN0.RMPTR43.uint8_t[LL]
#define RSCAN0RMPTR43LH RSCAN0.RMPTR43.uint8_t[LH]
#define RSCAN0RMPTR43H RSCAN0.RMPTR43.uint16_t[H]
#define RSCAN0RMPTR43HL RSCAN0.RMPTR43.uint8_t[HL]
#define RSCAN0RMPTR43HH RSCAN0.RMPTR43.uint8_t[HH]
#define RSCAN0RMDF043 RSCAN0.RMDF043.uint32_t
#define RSCAN0RMDF043L RSCAN0.RMDF043.uint16_t[L]
#define RSCAN0RMDF043LL RSCAN0.RMDF043.uint8_t[LL]
#define RSCAN0RMDF043LH RSCAN0.RMDF043.uint8_t[LH]
#define RSCAN0RMDF043H RSCAN0.RMDF043.uint16_t[H]
#define RSCAN0RMDF043HL RSCAN0.RMDF043.uint8_t[HL]
#define RSCAN0RMDF043HH RSCAN0.RMDF043.uint8_t[HH]
#define RSCAN0RMDF143 RSCAN0.RMDF143.uint32_t
#define RSCAN0RMDF143L RSCAN0.RMDF143.uint16_t[L]
#define RSCAN0RMDF143LL RSCAN0.RMDF143.uint8_t[LL]
#define RSCAN0RMDF143LH RSCAN0.RMDF143.uint8_t[LH]
#define RSCAN0RMDF143H RSCAN0.RMDF143.uint16_t[H]
#define RSCAN0RMDF143HL RSCAN0.RMDF143.uint8_t[HL]
#define RSCAN0RMDF143HH RSCAN0.RMDF143.uint8_t[HH]
#define RSCAN0RMID44 RSCAN0.RMID44.uint32_t
#define RSCAN0RMID44L RSCAN0.RMID44.uint16_t[L]
#define RSCAN0RMID44LL RSCAN0.RMID44.uint8_t[LL]
#define RSCAN0RMID44LH RSCAN0.RMID44.uint8_t[LH]
#define RSCAN0RMID44H RSCAN0.RMID44.uint16_t[H]
#define RSCAN0RMID44HL RSCAN0.RMID44.uint8_t[HL]
#define RSCAN0RMID44HH RSCAN0.RMID44.uint8_t[HH]
#define RSCAN0RMPTR44 RSCAN0.RMPTR44.uint32_t
#define RSCAN0RMPTR44L RSCAN0.RMPTR44.uint16_t[L]
#define RSCAN0RMPTR44LL RSCAN0.RMPTR44.uint8_t[LL]
#define RSCAN0RMPTR44LH RSCAN0.RMPTR44.uint8_t[LH]
#define RSCAN0RMPTR44H RSCAN0.RMPTR44.uint16_t[H]
#define RSCAN0RMPTR44HL RSCAN0.RMPTR44.uint8_t[HL]
#define RSCAN0RMPTR44HH RSCAN0.RMPTR44.uint8_t[HH]
#define RSCAN0RMDF044 RSCAN0.RMDF044.uint32_t
#define RSCAN0RMDF044L RSCAN0.RMDF044.uint16_t[L]
#define RSCAN0RMDF044LL RSCAN0.RMDF044.uint8_t[LL]
#define RSCAN0RMDF044LH RSCAN0.RMDF044.uint8_t[LH]
#define RSCAN0RMDF044H RSCAN0.RMDF044.uint16_t[H]
#define RSCAN0RMDF044HL RSCAN0.RMDF044.uint8_t[HL]
#define RSCAN0RMDF044HH RSCAN0.RMDF044.uint8_t[HH]
#define RSCAN0RMDF144 RSCAN0.RMDF144.uint32_t
#define RSCAN0RMDF144L RSCAN0.RMDF144.uint16_t[L]
#define RSCAN0RMDF144LL RSCAN0.RMDF144.uint8_t[LL]
#define RSCAN0RMDF144LH RSCAN0.RMDF144.uint8_t[LH]
#define RSCAN0RMDF144H RSCAN0.RMDF144.uint16_t[H]
#define RSCAN0RMDF144HL RSCAN0.RMDF144.uint8_t[HL]
#define RSCAN0RMDF144HH RSCAN0.RMDF144.uint8_t[HH]
#define RSCAN0RMID45 RSCAN0.RMID45.uint32_t
#define RSCAN0RMID45L RSCAN0.RMID45.uint16_t[L]
#define RSCAN0RMID45LL RSCAN0.RMID45.uint8_t[LL]
#define RSCAN0RMID45LH RSCAN0.RMID45.uint8_t[LH]
#define RSCAN0RMID45H RSCAN0.RMID45.uint16_t[H]
#define RSCAN0RMID45HL RSCAN0.RMID45.uint8_t[HL]
#define RSCAN0RMID45HH RSCAN0.RMID45.uint8_t[HH]
#define RSCAN0RMPTR45 RSCAN0.RMPTR45.uint32_t
#define RSCAN0RMPTR45L RSCAN0.RMPTR45.uint16_t[L]
#define RSCAN0RMPTR45LL RSCAN0.RMPTR45.uint8_t[LL]
#define RSCAN0RMPTR45LH RSCAN0.RMPTR45.uint8_t[LH]
#define RSCAN0RMPTR45H RSCAN0.RMPTR45.uint16_t[H]
#define RSCAN0RMPTR45HL RSCAN0.RMPTR45.uint8_t[HL]
#define RSCAN0RMPTR45HH RSCAN0.RMPTR45.uint8_t[HH]
#define RSCAN0RMDF045 RSCAN0.RMDF045.uint32_t
#define RSCAN0RMDF045L RSCAN0.RMDF045.uint16_t[L]
#define RSCAN0RMDF045LL RSCAN0.RMDF045.uint8_t[LL]
#define RSCAN0RMDF045LH RSCAN0.RMDF045.uint8_t[LH]
#define RSCAN0RMDF045H RSCAN0.RMDF045.uint16_t[H]
#define RSCAN0RMDF045HL RSCAN0.RMDF045.uint8_t[HL]
#define RSCAN0RMDF045HH RSCAN0.RMDF045.uint8_t[HH]
#define RSCAN0RMDF145 RSCAN0.RMDF145.uint32_t
#define RSCAN0RMDF145L RSCAN0.RMDF145.uint16_t[L]
#define RSCAN0RMDF145LL RSCAN0.RMDF145.uint8_t[LL]
#define RSCAN0RMDF145LH RSCAN0.RMDF145.uint8_t[LH]
#define RSCAN0RMDF145H RSCAN0.RMDF145.uint16_t[H]
#define RSCAN0RMDF145HL RSCAN0.RMDF145.uint8_t[HL]
#define RSCAN0RMDF145HH RSCAN0.RMDF145.uint8_t[HH]
#define RSCAN0RMID46 RSCAN0.RMID46.uint32_t
#define RSCAN0RMID46L RSCAN0.RMID46.uint16_t[L]
#define RSCAN0RMID46LL RSCAN0.RMID46.uint8_t[LL]
#define RSCAN0RMID46LH RSCAN0.RMID46.uint8_t[LH]
#define RSCAN0RMID46H RSCAN0.RMID46.uint16_t[H]
#define RSCAN0RMID46HL RSCAN0.RMID46.uint8_t[HL]
#define RSCAN0RMID46HH RSCAN0.RMID46.uint8_t[HH]
#define RSCAN0RMPTR46 RSCAN0.RMPTR46.uint32_t
#define RSCAN0RMPTR46L RSCAN0.RMPTR46.uint16_t[L]
#define RSCAN0RMPTR46LL RSCAN0.RMPTR46.uint8_t[LL]
#define RSCAN0RMPTR46LH RSCAN0.RMPTR46.uint8_t[LH]
#define RSCAN0RMPTR46H RSCAN0.RMPTR46.uint16_t[H]
#define RSCAN0RMPTR46HL RSCAN0.RMPTR46.uint8_t[HL]
#define RSCAN0RMPTR46HH RSCAN0.RMPTR46.uint8_t[HH]
#define RSCAN0RMDF046 RSCAN0.RMDF046.uint32_t
#define RSCAN0RMDF046L RSCAN0.RMDF046.uint16_t[L]
#define RSCAN0RMDF046LL RSCAN0.RMDF046.uint8_t[LL]
#define RSCAN0RMDF046LH RSCAN0.RMDF046.uint8_t[LH]
#define RSCAN0RMDF046H RSCAN0.RMDF046.uint16_t[H]
#define RSCAN0RMDF046HL RSCAN0.RMDF046.uint8_t[HL]
#define RSCAN0RMDF046HH RSCAN0.RMDF046.uint8_t[HH]
#define RSCAN0RMDF146 RSCAN0.RMDF146.uint32_t
#define RSCAN0RMDF146L RSCAN0.RMDF146.uint16_t[L]
#define RSCAN0RMDF146LL RSCAN0.RMDF146.uint8_t[LL]
#define RSCAN0RMDF146LH RSCAN0.RMDF146.uint8_t[LH]
#define RSCAN0RMDF146H RSCAN0.RMDF146.uint16_t[H]
#define RSCAN0RMDF146HL RSCAN0.RMDF146.uint8_t[HL]
#define RSCAN0RMDF146HH RSCAN0.RMDF146.uint8_t[HH]
#define RSCAN0RMID47 RSCAN0.RMID47.uint32_t
#define RSCAN0RMID47L RSCAN0.RMID47.uint16_t[L]
#define RSCAN0RMID47LL RSCAN0.RMID47.uint8_t[LL]
#define RSCAN0RMID47LH RSCAN0.RMID47.uint8_t[LH]
#define RSCAN0RMID47H RSCAN0.RMID47.uint16_t[H]
#define RSCAN0RMID47HL RSCAN0.RMID47.uint8_t[HL]
#define RSCAN0RMID47HH RSCAN0.RMID47.uint8_t[HH]
#define RSCAN0RMPTR47 RSCAN0.RMPTR47.uint32_t
#define RSCAN0RMPTR47L RSCAN0.RMPTR47.uint16_t[L]
#define RSCAN0RMPTR47LL RSCAN0.RMPTR47.uint8_t[LL]
#define RSCAN0RMPTR47LH RSCAN0.RMPTR47.uint8_t[LH]
#define RSCAN0RMPTR47H RSCAN0.RMPTR47.uint16_t[H]
#define RSCAN0RMPTR47HL RSCAN0.RMPTR47.uint8_t[HL]
#define RSCAN0RMPTR47HH RSCAN0.RMPTR47.uint8_t[HH]
#define RSCAN0RMDF047 RSCAN0.RMDF047.uint32_t
#define RSCAN0RMDF047L RSCAN0.RMDF047.uint16_t[L]
#define RSCAN0RMDF047LL RSCAN0.RMDF047.uint8_t[LL]
#define RSCAN0RMDF047LH RSCAN0.RMDF047.uint8_t[LH]
#define RSCAN0RMDF047H RSCAN0.RMDF047.uint16_t[H]
#define RSCAN0RMDF047HL RSCAN0.RMDF047.uint8_t[HL]
#define RSCAN0RMDF047HH RSCAN0.RMDF047.uint8_t[HH]
#define RSCAN0RMDF147 RSCAN0.RMDF147.uint32_t
#define RSCAN0RMDF147L RSCAN0.RMDF147.uint16_t[L]
#define RSCAN0RMDF147LL RSCAN0.RMDF147.uint8_t[LL]
#define RSCAN0RMDF147LH RSCAN0.RMDF147.uint8_t[LH]
#define RSCAN0RMDF147H RSCAN0.RMDF147.uint16_t[H]
#define RSCAN0RMDF147HL RSCAN0.RMDF147.uint8_t[HL]
#define RSCAN0RMDF147HH RSCAN0.RMDF147.uint8_t[HH]
#define RSCAN0RMID48 RSCAN0.RMID48.uint32_t
#define RSCAN0RMID48L RSCAN0.RMID48.uint16_t[L]
#define RSCAN0RMID48LL RSCAN0.RMID48.uint8_t[LL]
#define RSCAN0RMID48LH RSCAN0.RMID48.uint8_t[LH]
#define RSCAN0RMID48H RSCAN0.RMID48.uint16_t[H]
#define RSCAN0RMID48HL RSCAN0.RMID48.uint8_t[HL]
#define RSCAN0RMID48HH RSCAN0.RMID48.uint8_t[HH]
#define RSCAN0RMPTR48 RSCAN0.RMPTR48.uint32_t
#define RSCAN0RMPTR48L RSCAN0.RMPTR48.uint16_t[L]
#define RSCAN0RMPTR48LL RSCAN0.RMPTR48.uint8_t[LL]
#define RSCAN0RMPTR48LH RSCAN0.RMPTR48.uint8_t[LH]
#define RSCAN0RMPTR48H RSCAN0.RMPTR48.uint16_t[H]
#define RSCAN0RMPTR48HL RSCAN0.RMPTR48.uint8_t[HL]
#define RSCAN0RMPTR48HH RSCAN0.RMPTR48.uint8_t[HH]
#define RSCAN0RMDF048 RSCAN0.RMDF048.uint32_t
#define RSCAN0RMDF048L RSCAN0.RMDF048.uint16_t[L]
#define RSCAN0RMDF048LL RSCAN0.RMDF048.uint8_t[LL]
#define RSCAN0RMDF048LH RSCAN0.RMDF048.uint8_t[LH]
#define RSCAN0RMDF048H RSCAN0.RMDF048.uint16_t[H]
#define RSCAN0RMDF048HL RSCAN0.RMDF048.uint8_t[HL]
#define RSCAN0RMDF048HH RSCAN0.RMDF048.uint8_t[HH]
#define RSCAN0RMDF148 RSCAN0.RMDF148.uint32_t
#define RSCAN0RMDF148L RSCAN0.RMDF148.uint16_t[L]
#define RSCAN0RMDF148LL RSCAN0.RMDF148.uint8_t[LL]
#define RSCAN0RMDF148LH RSCAN0.RMDF148.uint8_t[LH]
#define RSCAN0RMDF148H RSCAN0.RMDF148.uint16_t[H]
#define RSCAN0RMDF148HL RSCAN0.RMDF148.uint8_t[HL]
#define RSCAN0RMDF148HH RSCAN0.RMDF148.uint8_t[HH]
#define RSCAN0RMID49 RSCAN0.RMID49.uint32_t
#define RSCAN0RMID49L RSCAN0.RMID49.uint16_t[L]
#define RSCAN0RMID49LL RSCAN0.RMID49.uint8_t[LL]
#define RSCAN0RMID49LH RSCAN0.RMID49.uint8_t[LH]
#define RSCAN0RMID49H RSCAN0.RMID49.uint16_t[H]
#define RSCAN0RMID49HL RSCAN0.RMID49.uint8_t[HL]
#define RSCAN0RMID49HH RSCAN0.RMID49.uint8_t[HH]
#define RSCAN0RMPTR49 RSCAN0.RMPTR49.uint32_t
#define RSCAN0RMPTR49L RSCAN0.RMPTR49.uint16_t[L]
#define RSCAN0RMPTR49LL RSCAN0.RMPTR49.uint8_t[LL]
#define RSCAN0RMPTR49LH RSCAN0.RMPTR49.uint8_t[LH]
#define RSCAN0RMPTR49H RSCAN0.RMPTR49.uint16_t[H]
#define RSCAN0RMPTR49HL RSCAN0.RMPTR49.uint8_t[HL]
#define RSCAN0RMPTR49HH RSCAN0.RMPTR49.uint8_t[HH]
#define RSCAN0RMDF049 RSCAN0.RMDF049.uint32_t
#define RSCAN0RMDF049L RSCAN0.RMDF049.uint16_t[L]
#define RSCAN0RMDF049LL RSCAN0.RMDF049.uint8_t[LL]
#define RSCAN0RMDF049LH RSCAN0.RMDF049.uint8_t[LH]
#define RSCAN0RMDF049H RSCAN0.RMDF049.uint16_t[H]
#define RSCAN0RMDF049HL RSCAN0.RMDF049.uint8_t[HL]
#define RSCAN0RMDF049HH RSCAN0.RMDF049.uint8_t[HH]
#define RSCAN0RMDF149 RSCAN0.RMDF149.uint32_t
#define RSCAN0RMDF149L RSCAN0.RMDF149.uint16_t[L]
#define RSCAN0RMDF149LL RSCAN0.RMDF149.uint8_t[LL]
#define RSCAN0RMDF149LH RSCAN0.RMDF149.uint8_t[LH]
#define RSCAN0RMDF149H RSCAN0.RMDF149.uint16_t[H]
#define RSCAN0RMDF149HL RSCAN0.RMDF149.uint8_t[HL]
#define RSCAN0RMDF149HH RSCAN0.RMDF149.uint8_t[HH]
#define RSCAN0RMID50 RSCAN0.RMID50.uint32_t
#define RSCAN0RMID50L RSCAN0.RMID50.uint16_t[L]
#define RSCAN0RMID50LL RSCAN0.RMID50.uint8_t[LL]
#define RSCAN0RMID50LH RSCAN0.RMID50.uint8_t[LH]
#define RSCAN0RMID50H RSCAN0.RMID50.uint16_t[H]
#define RSCAN0RMID50HL RSCAN0.RMID50.uint8_t[HL]
#define RSCAN0RMID50HH RSCAN0.RMID50.uint8_t[HH]
#define RSCAN0RMPTR50 RSCAN0.RMPTR50.uint32_t
#define RSCAN0RMPTR50L RSCAN0.RMPTR50.uint16_t[L]
#define RSCAN0RMPTR50LL RSCAN0.RMPTR50.uint8_t[LL]
#define RSCAN0RMPTR50LH RSCAN0.RMPTR50.uint8_t[LH]
#define RSCAN0RMPTR50H RSCAN0.RMPTR50.uint16_t[H]
#define RSCAN0RMPTR50HL RSCAN0.RMPTR50.uint8_t[HL]
#define RSCAN0RMPTR50HH RSCAN0.RMPTR50.uint8_t[HH]
#define RSCAN0RMDF050 RSCAN0.RMDF050.uint32_t
#define RSCAN0RMDF050L RSCAN0.RMDF050.uint16_t[L]
#define RSCAN0RMDF050LL RSCAN0.RMDF050.uint8_t[LL]
#define RSCAN0RMDF050LH RSCAN0.RMDF050.uint8_t[LH]
#define RSCAN0RMDF050H RSCAN0.RMDF050.uint16_t[H]
#define RSCAN0RMDF050HL RSCAN0.RMDF050.uint8_t[HL]
#define RSCAN0RMDF050HH RSCAN0.RMDF050.uint8_t[HH]
#define RSCAN0RMDF150 RSCAN0.RMDF150.uint32_t
#define RSCAN0RMDF150L RSCAN0.RMDF150.uint16_t[L]
#define RSCAN0RMDF150LL RSCAN0.RMDF150.uint8_t[LL]
#define RSCAN0RMDF150LH RSCAN0.RMDF150.uint8_t[LH]
#define RSCAN0RMDF150H RSCAN0.RMDF150.uint16_t[H]
#define RSCAN0RMDF150HL RSCAN0.RMDF150.uint8_t[HL]
#define RSCAN0RMDF150HH RSCAN0.RMDF150.uint8_t[HH]
#define RSCAN0RMID51 RSCAN0.RMID51.uint32_t
#define RSCAN0RMID51L RSCAN0.RMID51.uint16_t[L]
#define RSCAN0RMID51LL RSCAN0.RMID51.uint8_t[LL]
#define RSCAN0RMID51LH RSCAN0.RMID51.uint8_t[LH]
#define RSCAN0RMID51H RSCAN0.RMID51.uint16_t[H]
#define RSCAN0RMID51HL RSCAN0.RMID51.uint8_t[HL]
#define RSCAN0RMID51HH RSCAN0.RMID51.uint8_t[HH]
#define RSCAN0RMPTR51 RSCAN0.RMPTR51.uint32_t
#define RSCAN0RMPTR51L RSCAN0.RMPTR51.uint16_t[L]
#define RSCAN0RMPTR51LL RSCAN0.RMPTR51.uint8_t[LL]
#define RSCAN0RMPTR51LH RSCAN0.RMPTR51.uint8_t[LH]
#define RSCAN0RMPTR51H RSCAN0.RMPTR51.uint16_t[H]
#define RSCAN0RMPTR51HL RSCAN0.RMPTR51.uint8_t[HL]
#define RSCAN0RMPTR51HH RSCAN0.RMPTR51.uint8_t[HH]
#define RSCAN0RMDF051 RSCAN0.RMDF051.uint32_t
#define RSCAN0RMDF051L RSCAN0.RMDF051.uint16_t[L]
#define RSCAN0RMDF051LL RSCAN0.RMDF051.uint8_t[LL]
#define RSCAN0RMDF051LH RSCAN0.RMDF051.uint8_t[LH]
#define RSCAN0RMDF051H RSCAN0.RMDF051.uint16_t[H]
#define RSCAN0RMDF051HL RSCAN0.RMDF051.uint8_t[HL]
#define RSCAN0RMDF051HH RSCAN0.RMDF051.uint8_t[HH]
#define RSCAN0RMDF151 RSCAN0.RMDF151.uint32_t
#define RSCAN0RMDF151L RSCAN0.RMDF151.uint16_t[L]
#define RSCAN0RMDF151LL RSCAN0.RMDF151.uint8_t[LL]
#define RSCAN0RMDF151LH RSCAN0.RMDF151.uint8_t[LH]
#define RSCAN0RMDF151H RSCAN0.RMDF151.uint16_t[H]
#define RSCAN0RMDF151HL RSCAN0.RMDF151.uint8_t[HL]
#define RSCAN0RMDF151HH RSCAN0.RMDF151.uint8_t[HH]
#define RSCAN0RMID52 RSCAN0.RMID52.uint32_t
#define RSCAN0RMID52L RSCAN0.RMID52.uint16_t[L]
#define RSCAN0RMID52LL RSCAN0.RMID52.uint8_t[LL]
#define RSCAN0RMID52LH RSCAN0.RMID52.uint8_t[LH]
#define RSCAN0RMID52H RSCAN0.RMID52.uint16_t[H]
#define RSCAN0RMID52HL RSCAN0.RMID52.uint8_t[HL]
#define RSCAN0RMID52HH RSCAN0.RMID52.uint8_t[HH]
#define RSCAN0RMPTR52 RSCAN0.RMPTR52.uint32_t
#define RSCAN0RMPTR52L RSCAN0.RMPTR52.uint16_t[L]
#define RSCAN0RMPTR52LL RSCAN0.RMPTR52.uint8_t[LL]
#define RSCAN0RMPTR52LH RSCAN0.RMPTR52.uint8_t[LH]
#define RSCAN0RMPTR52H RSCAN0.RMPTR52.uint16_t[H]
#define RSCAN0RMPTR52HL RSCAN0.RMPTR52.uint8_t[HL]
#define RSCAN0RMPTR52HH RSCAN0.RMPTR52.uint8_t[HH]
#define RSCAN0RMDF052 RSCAN0.RMDF052.uint32_t
#define RSCAN0RMDF052L RSCAN0.RMDF052.uint16_t[L]
#define RSCAN0RMDF052LL RSCAN0.RMDF052.uint8_t[LL]
#define RSCAN0RMDF052LH RSCAN0.RMDF052.uint8_t[LH]
#define RSCAN0RMDF052H RSCAN0.RMDF052.uint16_t[H]
#define RSCAN0RMDF052HL RSCAN0.RMDF052.uint8_t[HL]
#define RSCAN0RMDF052HH RSCAN0.RMDF052.uint8_t[HH]
#define RSCAN0RMDF152 RSCAN0.RMDF152.uint32_t
#define RSCAN0RMDF152L RSCAN0.RMDF152.uint16_t[L]
#define RSCAN0RMDF152LL RSCAN0.RMDF152.uint8_t[LL]
#define RSCAN0RMDF152LH RSCAN0.RMDF152.uint8_t[LH]
#define RSCAN0RMDF152H RSCAN0.RMDF152.uint16_t[H]
#define RSCAN0RMDF152HL RSCAN0.RMDF152.uint8_t[HL]
#define RSCAN0RMDF152HH RSCAN0.RMDF152.uint8_t[HH]
#define RSCAN0RMID53 RSCAN0.RMID53.uint32_t
#define RSCAN0RMID53L RSCAN0.RMID53.uint16_t[L]
#define RSCAN0RMID53LL RSCAN0.RMID53.uint8_t[LL]
#define RSCAN0RMID53LH RSCAN0.RMID53.uint8_t[LH]
#define RSCAN0RMID53H RSCAN0.RMID53.uint16_t[H]
#define RSCAN0RMID53HL RSCAN0.RMID53.uint8_t[HL]
#define RSCAN0RMID53HH RSCAN0.RMID53.uint8_t[HH]
#define RSCAN0RMPTR53 RSCAN0.RMPTR53.uint32_t
#define RSCAN0RMPTR53L RSCAN0.RMPTR53.uint16_t[L]
#define RSCAN0RMPTR53LL RSCAN0.RMPTR53.uint8_t[LL]
#define RSCAN0RMPTR53LH RSCAN0.RMPTR53.uint8_t[LH]
#define RSCAN0RMPTR53H RSCAN0.RMPTR53.uint16_t[H]
#define RSCAN0RMPTR53HL RSCAN0.RMPTR53.uint8_t[HL]
#define RSCAN0RMPTR53HH RSCAN0.RMPTR53.uint8_t[HH]
#define RSCAN0RMDF053 RSCAN0.RMDF053.uint32_t
#define RSCAN0RMDF053L RSCAN0.RMDF053.uint16_t[L]
#define RSCAN0RMDF053LL RSCAN0.RMDF053.uint8_t[LL]
#define RSCAN0RMDF053LH RSCAN0.RMDF053.uint8_t[LH]
#define RSCAN0RMDF053H RSCAN0.RMDF053.uint16_t[H]
#define RSCAN0RMDF053HL RSCAN0.RMDF053.uint8_t[HL]
#define RSCAN0RMDF053HH RSCAN0.RMDF053.uint8_t[HH]
#define RSCAN0RMDF153 RSCAN0.RMDF153.uint32_t
#define RSCAN0RMDF153L RSCAN0.RMDF153.uint16_t[L]
#define RSCAN0RMDF153LL RSCAN0.RMDF153.uint8_t[LL]
#define RSCAN0RMDF153LH RSCAN0.RMDF153.uint8_t[LH]
#define RSCAN0RMDF153H RSCAN0.RMDF153.uint16_t[H]
#define RSCAN0RMDF153HL RSCAN0.RMDF153.uint8_t[HL]
#define RSCAN0RMDF153HH RSCAN0.RMDF153.uint8_t[HH]
#define RSCAN0RMID54 RSCAN0.RMID54.uint32_t
#define RSCAN0RMID54L RSCAN0.RMID54.uint16_t[L]
#define RSCAN0RMID54LL RSCAN0.RMID54.uint8_t[LL]
#define RSCAN0RMID54LH RSCAN0.RMID54.uint8_t[LH]
#define RSCAN0RMID54H RSCAN0.RMID54.uint16_t[H]
#define RSCAN0RMID54HL RSCAN0.RMID54.uint8_t[HL]
#define RSCAN0RMID54HH RSCAN0.RMID54.uint8_t[HH]
#define RSCAN0RMPTR54 RSCAN0.RMPTR54.uint32_t
#define RSCAN0RMPTR54L RSCAN0.RMPTR54.uint16_t[L]
#define RSCAN0RMPTR54LL RSCAN0.RMPTR54.uint8_t[LL]
#define RSCAN0RMPTR54LH RSCAN0.RMPTR54.uint8_t[LH]
#define RSCAN0RMPTR54H RSCAN0.RMPTR54.uint16_t[H]
#define RSCAN0RMPTR54HL RSCAN0.RMPTR54.uint8_t[HL]
#define RSCAN0RMPTR54HH RSCAN0.RMPTR54.uint8_t[HH]
#define RSCAN0RMDF054 RSCAN0.RMDF054.uint32_t
#define RSCAN0RMDF054L RSCAN0.RMDF054.uint16_t[L]
#define RSCAN0RMDF054LL RSCAN0.RMDF054.uint8_t[LL]
#define RSCAN0RMDF054LH RSCAN0.RMDF054.uint8_t[LH]
#define RSCAN0RMDF054H RSCAN0.RMDF054.uint16_t[H]
#define RSCAN0RMDF054HL RSCAN0.RMDF054.uint8_t[HL]
#define RSCAN0RMDF054HH RSCAN0.RMDF054.uint8_t[HH]
#define RSCAN0RMDF154 RSCAN0.RMDF154.uint32_t
#define RSCAN0RMDF154L RSCAN0.RMDF154.uint16_t[L]
#define RSCAN0RMDF154LL RSCAN0.RMDF154.uint8_t[LL]
#define RSCAN0RMDF154LH RSCAN0.RMDF154.uint8_t[LH]
#define RSCAN0RMDF154H RSCAN0.RMDF154.uint16_t[H]
#define RSCAN0RMDF154HL RSCAN0.RMDF154.uint8_t[HL]
#define RSCAN0RMDF154HH RSCAN0.RMDF154.uint8_t[HH]
#define RSCAN0RMID55 RSCAN0.RMID55.uint32_t
#define RSCAN0RMID55L RSCAN0.RMID55.uint16_t[L]
#define RSCAN0RMID55LL RSCAN0.RMID55.uint8_t[LL]
#define RSCAN0RMID55LH RSCAN0.RMID55.uint8_t[LH]
#define RSCAN0RMID55H RSCAN0.RMID55.uint16_t[H]
#define RSCAN0RMID55HL RSCAN0.RMID55.uint8_t[HL]
#define RSCAN0RMID55HH RSCAN0.RMID55.uint8_t[HH]
#define RSCAN0RMPTR55 RSCAN0.RMPTR55.uint32_t
#define RSCAN0RMPTR55L RSCAN0.RMPTR55.uint16_t[L]
#define RSCAN0RMPTR55LL RSCAN0.RMPTR55.uint8_t[LL]
#define RSCAN0RMPTR55LH RSCAN0.RMPTR55.uint8_t[LH]
#define RSCAN0RMPTR55H RSCAN0.RMPTR55.uint16_t[H]
#define RSCAN0RMPTR55HL RSCAN0.RMPTR55.uint8_t[HL]
#define RSCAN0RMPTR55HH RSCAN0.RMPTR55.uint8_t[HH]
#define RSCAN0RMDF055 RSCAN0.RMDF055.uint32_t
#define RSCAN0RMDF055L RSCAN0.RMDF055.uint16_t[L]
#define RSCAN0RMDF055LL RSCAN0.RMDF055.uint8_t[LL]
#define RSCAN0RMDF055LH RSCAN0.RMDF055.uint8_t[LH]
#define RSCAN0RMDF055H RSCAN0.RMDF055.uint16_t[H]
#define RSCAN0RMDF055HL RSCAN0.RMDF055.uint8_t[HL]
#define RSCAN0RMDF055HH RSCAN0.RMDF055.uint8_t[HH]
#define RSCAN0RMDF155 RSCAN0.RMDF155.uint32_t
#define RSCAN0RMDF155L RSCAN0.RMDF155.uint16_t[L]
#define RSCAN0RMDF155LL RSCAN0.RMDF155.uint8_t[LL]
#define RSCAN0RMDF155LH RSCAN0.RMDF155.uint8_t[LH]
#define RSCAN0RMDF155H RSCAN0.RMDF155.uint16_t[H]
#define RSCAN0RMDF155HL RSCAN0.RMDF155.uint8_t[HL]
#define RSCAN0RMDF155HH RSCAN0.RMDF155.uint8_t[HH]
#define RSCAN0RMID56 RSCAN0.RMID56.uint32_t
#define RSCAN0RMID56L RSCAN0.RMID56.uint16_t[L]
#define RSCAN0RMID56LL RSCAN0.RMID56.uint8_t[LL]
#define RSCAN0RMID56LH RSCAN0.RMID56.uint8_t[LH]
#define RSCAN0RMID56H RSCAN0.RMID56.uint16_t[H]
#define RSCAN0RMID56HL RSCAN0.RMID56.uint8_t[HL]
#define RSCAN0RMID56HH RSCAN0.RMID56.uint8_t[HH]
#define RSCAN0RMPTR56 RSCAN0.RMPTR56.uint32_t
#define RSCAN0RMPTR56L RSCAN0.RMPTR56.uint16_t[L]
#define RSCAN0RMPTR56LL RSCAN0.RMPTR56.uint8_t[LL]
#define RSCAN0RMPTR56LH RSCAN0.RMPTR56.uint8_t[LH]
#define RSCAN0RMPTR56H RSCAN0.RMPTR56.uint16_t[H]
#define RSCAN0RMPTR56HL RSCAN0.RMPTR56.uint8_t[HL]
#define RSCAN0RMPTR56HH RSCAN0.RMPTR56.uint8_t[HH]
#define RSCAN0RMDF056 RSCAN0.RMDF056.uint32_t
#define RSCAN0RMDF056L RSCAN0.RMDF056.uint16_t[L]
#define RSCAN0RMDF056LL RSCAN0.RMDF056.uint8_t[LL]
#define RSCAN0RMDF056LH RSCAN0.RMDF056.uint8_t[LH]
#define RSCAN0RMDF056H RSCAN0.RMDF056.uint16_t[H]
#define RSCAN0RMDF056HL RSCAN0.RMDF056.uint8_t[HL]
#define RSCAN0RMDF056HH RSCAN0.RMDF056.uint8_t[HH]
#define RSCAN0RMDF156 RSCAN0.RMDF156.uint32_t
#define RSCAN0RMDF156L RSCAN0.RMDF156.uint16_t[L]
#define RSCAN0RMDF156LL RSCAN0.RMDF156.uint8_t[LL]
#define RSCAN0RMDF156LH RSCAN0.RMDF156.uint8_t[LH]
#define RSCAN0RMDF156H RSCAN0.RMDF156.uint16_t[H]
#define RSCAN0RMDF156HL RSCAN0.RMDF156.uint8_t[HL]
#define RSCAN0RMDF156HH RSCAN0.RMDF156.uint8_t[HH]
#define RSCAN0RMID57 RSCAN0.RMID57.uint32_t
#define RSCAN0RMID57L RSCAN0.RMID57.uint16_t[L]
#define RSCAN0RMID57LL RSCAN0.RMID57.uint8_t[LL]
#define RSCAN0RMID57LH RSCAN0.RMID57.uint8_t[LH]
#define RSCAN0RMID57H RSCAN0.RMID57.uint16_t[H]
#define RSCAN0RMID57HL RSCAN0.RMID57.uint8_t[HL]
#define RSCAN0RMID57HH RSCAN0.RMID57.uint8_t[HH]
#define RSCAN0RMPTR57 RSCAN0.RMPTR57.uint32_t
#define RSCAN0RMPTR57L RSCAN0.RMPTR57.uint16_t[L]
#define RSCAN0RMPTR57LL RSCAN0.RMPTR57.uint8_t[LL]
#define RSCAN0RMPTR57LH RSCAN0.RMPTR57.uint8_t[LH]
#define RSCAN0RMPTR57H RSCAN0.RMPTR57.uint16_t[H]
#define RSCAN0RMPTR57HL RSCAN0.RMPTR57.uint8_t[HL]
#define RSCAN0RMPTR57HH RSCAN0.RMPTR57.uint8_t[HH]
#define RSCAN0RMDF057 RSCAN0.RMDF057.uint32_t
#define RSCAN0RMDF057L RSCAN0.RMDF057.uint16_t[L]
#define RSCAN0RMDF057LL RSCAN0.RMDF057.uint8_t[LL]
#define RSCAN0RMDF057LH RSCAN0.RMDF057.uint8_t[LH]
#define RSCAN0RMDF057H RSCAN0.RMDF057.uint16_t[H]
#define RSCAN0RMDF057HL RSCAN0.RMDF057.uint8_t[HL]
#define RSCAN0RMDF057HH RSCAN0.RMDF057.uint8_t[HH]
#define RSCAN0RMDF157 RSCAN0.RMDF157.uint32_t
#define RSCAN0RMDF157L RSCAN0.RMDF157.uint16_t[L]
#define RSCAN0RMDF157LL RSCAN0.RMDF157.uint8_t[LL]
#define RSCAN0RMDF157LH RSCAN0.RMDF157.uint8_t[LH]
#define RSCAN0RMDF157H RSCAN0.RMDF157.uint16_t[H]
#define RSCAN0RMDF157HL RSCAN0.RMDF157.uint8_t[HL]
#define RSCAN0RMDF157HH RSCAN0.RMDF157.uint8_t[HH]
#define RSCAN0RMID58 RSCAN0.RMID58.uint32_t
#define RSCAN0RMID58L RSCAN0.RMID58.uint16_t[L]
#define RSCAN0RMID58LL RSCAN0.RMID58.uint8_t[LL]
#define RSCAN0RMID58LH RSCAN0.RMID58.uint8_t[LH]
#define RSCAN0RMID58H RSCAN0.RMID58.uint16_t[H]
#define RSCAN0RMID58HL RSCAN0.RMID58.uint8_t[HL]
#define RSCAN0RMID58HH RSCAN0.RMID58.uint8_t[HH]
#define RSCAN0RMPTR58 RSCAN0.RMPTR58.uint32_t
#define RSCAN0RMPTR58L RSCAN0.RMPTR58.uint16_t[L]
#define RSCAN0RMPTR58LL RSCAN0.RMPTR58.uint8_t[LL]
#define RSCAN0RMPTR58LH RSCAN0.RMPTR58.uint8_t[LH]
#define RSCAN0RMPTR58H RSCAN0.RMPTR58.uint16_t[H]
#define RSCAN0RMPTR58HL RSCAN0.RMPTR58.uint8_t[HL]
#define RSCAN0RMPTR58HH RSCAN0.RMPTR58.uint8_t[HH]
#define RSCAN0RMDF058 RSCAN0.RMDF058.uint32_t
#define RSCAN0RMDF058L RSCAN0.RMDF058.uint16_t[L]
#define RSCAN0RMDF058LL RSCAN0.RMDF058.uint8_t[LL]
#define RSCAN0RMDF058LH RSCAN0.RMDF058.uint8_t[LH]
#define RSCAN0RMDF058H RSCAN0.RMDF058.uint16_t[H]
#define RSCAN0RMDF058HL RSCAN0.RMDF058.uint8_t[HL]
#define RSCAN0RMDF058HH RSCAN0.RMDF058.uint8_t[HH]
#define RSCAN0RMDF158 RSCAN0.RMDF158.uint32_t
#define RSCAN0RMDF158L RSCAN0.RMDF158.uint16_t[L]
#define RSCAN0RMDF158LL RSCAN0.RMDF158.uint8_t[LL]
#define RSCAN0RMDF158LH RSCAN0.RMDF158.uint8_t[LH]
#define RSCAN0RMDF158H RSCAN0.RMDF158.uint16_t[H]
#define RSCAN0RMDF158HL RSCAN0.RMDF158.uint8_t[HL]
#define RSCAN0RMDF158HH RSCAN0.RMDF158.uint8_t[HH]
#define RSCAN0RMID59 RSCAN0.RMID59.uint32_t
#define RSCAN0RMID59L RSCAN0.RMID59.uint16_t[L]
#define RSCAN0RMID59LL RSCAN0.RMID59.uint8_t[LL]
#define RSCAN0RMID59LH RSCAN0.RMID59.uint8_t[LH]
#define RSCAN0RMID59H RSCAN0.RMID59.uint16_t[H]
#define RSCAN0RMID59HL RSCAN0.RMID59.uint8_t[HL]
#define RSCAN0RMID59HH RSCAN0.RMID59.uint8_t[HH]
#define RSCAN0RMPTR59 RSCAN0.RMPTR59.uint32_t
#define RSCAN0RMPTR59L RSCAN0.RMPTR59.uint16_t[L]
#define RSCAN0RMPTR59LL RSCAN0.RMPTR59.uint8_t[LL]
#define RSCAN0RMPTR59LH RSCAN0.RMPTR59.uint8_t[LH]
#define RSCAN0RMPTR59H RSCAN0.RMPTR59.uint16_t[H]
#define RSCAN0RMPTR59HL RSCAN0.RMPTR59.uint8_t[HL]
#define RSCAN0RMPTR59HH RSCAN0.RMPTR59.uint8_t[HH]
#define RSCAN0RMDF059 RSCAN0.RMDF059.uint32_t
#define RSCAN0RMDF059L RSCAN0.RMDF059.uint16_t[L]
#define RSCAN0RMDF059LL RSCAN0.RMDF059.uint8_t[LL]
#define RSCAN0RMDF059LH RSCAN0.RMDF059.uint8_t[LH]
#define RSCAN0RMDF059H RSCAN0.RMDF059.uint16_t[H]
#define RSCAN0RMDF059HL RSCAN0.RMDF059.uint8_t[HL]
#define RSCAN0RMDF059HH RSCAN0.RMDF059.uint8_t[HH]
#define RSCAN0RMDF159 RSCAN0.RMDF159.uint32_t
#define RSCAN0RMDF159L RSCAN0.RMDF159.uint16_t[L]
#define RSCAN0RMDF159LL RSCAN0.RMDF159.uint8_t[LL]
#define RSCAN0RMDF159LH RSCAN0.RMDF159.uint8_t[LH]
#define RSCAN0RMDF159H RSCAN0.RMDF159.uint16_t[H]
#define RSCAN0RMDF159HL RSCAN0.RMDF159.uint8_t[HL]
#define RSCAN0RMDF159HH RSCAN0.RMDF159.uint8_t[HH]
#define RSCAN0RMID60 RSCAN0.RMID60.uint32_t
#define RSCAN0RMID60L RSCAN0.RMID60.uint16_t[L]
#define RSCAN0RMID60LL RSCAN0.RMID60.uint8_t[LL]
#define RSCAN0RMID60LH RSCAN0.RMID60.uint8_t[LH]
#define RSCAN0RMID60H RSCAN0.RMID60.uint16_t[H]
#define RSCAN0RMID60HL RSCAN0.RMID60.uint8_t[HL]
#define RSCAN0RMID60HH RSCAN0.RMID60.uint8_t[HH]
#define RSCAN0RMPTR60 RSCAN0.RMPTR60.uint32_t
#define RSCAN0RMPTR60L RSCAN0.RMPTR60.uint16_t[L]
#define RSCAN0RMPTR60LL RSCAN0.RMPTR60.uint8_t[LL]
#define RSCAN0RMPTR60LH RSCAN0.RMPTR60.uint8_t[LH]
#define RSCAN0RMPTR60H RSCAN0.RMPTR60.uint16_t[H]
#define RSCAN0RMPTR60HL RSCAN0.RMPTR60.uint8_t[HL]
#define RSCAN0RMPTR60HH RSCAN0.RMPTR60.uint8_t[HH]
#define RSCAN0RMDF060 RSCAN0.RMDF060.uint32_t
#define RSCAN0RMDF060L RSCAN0.RMDF060.uint16_t[L]
#define RSCAN0RMDF060LL RSCAN0.RMDF060.uint8_t[LL]
#define RSCAN0RMDF060LH RSCAN0.RMDF060.uint8_t[LH]
#define RSCAN0RMDF060H RSCAN0.RMDF060.uint16_t[H]
#define RSCAN0RMDF060HL RSCAN0.RMDF060.uint8_t[HL]
#define RSCAN0RMDF060HH RSCAN0.RMDF060.uint8_t[HH]
#define RSCAN0RMDF160 RSCAN0.RMDF160.uint32_t
#define RSCAN0RMDF160L RSCAN0.RMDF160.uint16_t[L]
#define RSCAN0RMDF160LL RSCAN0.RMDF160.uint8_t[LL]
#define RSCAN0RMDF160LH RSCAN0.RMDF160.uint8_t[LH]
#define RSCAN0RMDF160H RSCAN0.RMDF160.uint16_t[H]
#define RSCAN0RMDF160HL RSCAN0.RMDF160.uint8_t[HL]
#define RSCAN0RMDF160HH RSCAN0.RMDF160.uint8_t[HH]
#define RSCAN0RMID61 RSCAN0.RMID61.uint32_t
#define RSCAN0RMID61L RSCAN0.RMID61.uint16_t[L]
#define RSCAN0RMID61LL RSCAN0.RMID61.uint8_t[LL]
#define RSCAN0RMID61LH RSCAN0.RMID61.uint8_t[LH]
#define RSCAN0RMID61H RSCAN0.RMID61.uint16_t[H]
#define RSCAN0RMID61HL RSCAN0.RMID61.uint8_t[HL]
#define RSCAN0RMID61HH RSCAN0.RMID61.uint8_t[HH]
#define RSCAN0RMPTR61 RSCAN0.RMPTR61.uint32_t
#define RSCAN0RMPTR61L RSCAN0.RMPTR61.uint16_t[L]
#define RSCAN0RMPTR61LL RSCAN0.RMPTR61.uint8_t[LL]
#define RSCAN0RMPTR61LH RSCAN0.RMPTR61.uint8_t[LH]
#define RSCAN0RMPTR61H RSCAN0.RMPTR61.uint16_t[H]
#define RSCAN0RMPTR61HL RSCAN0.RMPTR61.uint8_t[HL]
#define RSCAN0RMPTR61HH RSCAN0.RMPTR61.uint8_t[HH]
#define RSCAN0RMDF061 RSCAN0.RMDF061.uint32_t
#define RSCAN0RMDF061L RSCAN0.RMDF061.uint16_t[L]
#define RSCAN0RMDF061LL RSCAN0.RMDF061.uint8_t[LL]
#define RSCAN0RMDF061LH RSCAN0.RMDF061.uint8_t[LH]
#define RSCAN0RMDF061H RSCAN0.RMDF061.uint16_t[H]
#define RSCAN0RMDF061HL RSCAN0.RMDF061.uint8_t[HL]
#define RSCAN0RMDF061HH RSCAN0.RMDF061.uint8_t[HH]
#define RSCAN0RMDF161 RSCAN0.RMDF161.uint32_t
#define RSCAN0RMDF161L RSCAN0.RMDF161.uint16_t[L]
#define RSCAN0RMDF161LL RSCAN0.RMDF161.uint8_t[LL]
#define RSCAN0RMDF161LH RSCAN0.RMDF161.uint8_t[LH]
#define RSCAN0RMDF161H RSCAN0.RMDF161.uint16_t[H]
#define RSCAN0RMDF161HL RSCAN0.RMDF161.uint8_t[HL]
#define RSCAN0RMDF161HH RSCAN0.RMDF161.uint8_t[HH]
#define RSCAN0RMID62 RSCAN0.RMID62.uint32_t
#define RSCAN0RMID62L RSCAN0.RMID62.uint16_t[L]
#define RSCAN0RMID62LL RSCAN0.RMID62.uint8_t[LL]
#define RSCAN0RMID62LH RSCAN0.RMID62.uint8_t[LH]
#define RSCAN0RMID62H RSCAN0.RMID62.uint16_t[H]
#define RSCAN0RMID62HL RSCAN0.RMID62.uint8_t[HL]
#define RSCAN0RMID62HH RSCAN0.RMID62.uint8_t[HH]
#define RSCAN0RMPTR62 RSCAN0.RMPTR62.uint32_t
#define RSCAN0RMPTR62L RSCAN0.RMPTR62.uint16_t[L]
#define RSCAN0RMPTR62LL RSCAN0.RMPTR62.uint8_t[LL]
#define RSCAN0RMPTR62LH RSCAN0.RMPTR62.uint8_t[LH]
#define RSCAN0RMPTR62H RSCAN0.RMPTR62.uint16_t[H]
#define RSCAN0RMPTR62HL RSCAN0.RMPTR62.uint8_t[HL]
#define RSCAN0RMPTR62HH RSCAN0.RMPTR62.uint8_t[HH]
#define RSCAN0RMDF062 RSCAN0.RMDF062.uint32_t
#define RSCAN0RMDF062L RSCAN0.RMDF062.uint16_t[L]
#define RSCAN0RMDF062LL RSCAN0.RMDF062.uint8_t[LL]
#define RSCAN0RMDF062LH RSCAN0.RMDF062.uint8_t[LH]
#define RSCAN0RMDF062H RSCAN0.RMDF062.uint16_t[H]
#define RSCAN0RMDF062HL RSCAN0.RMDF062.uint8_t[HL]
#define RSCAN0RMDF062HH RSCAN0.RMDF062.uint8_t[HH]
#define RSCAN0RMDF162 RSCAN0.RMDF162.uint32_t
#define RSCAN0RMDF162L RSCAN0.RMDF162.uint16_t[L]
#define RSCAN0RMDF162LL RSCAN0.RMDF162.uint8_t[LL]
#define RSCAN0RMDF162LH RSCAN0.RMDF162.uint8_t[LH]
#define RSCAN0RMDF162H RSCAN0.RMDF162.uint16_t[H]
#define RSCAN0RMDF162HL RSCAN0.RMDF162.uint8_t[HL]
#define RSCAN0RMDF162HH RSCAN0.RMDF162.uint8_t[HH]
#define RSCAN0RMID63 RSCAN0.RMID63.uint32_t
#define RSCAN0RMID63L RSCAN0.RMID63.uint16_t[L]
#define RSCAN0RMID63LL RSCAN0.RMID63.uint8_t[LL]
#define RSCAN0RMID63LH RSCAN0.RMID63.uint8_t[LH]
#define RSCAN0RMID63H RSCAN0.RMID63.uint16_t[H]
#define RSCAN0RMID63HL RSCAN0.RMID63.uint8_t[HL]
#define RSCAN0RMID63HH RSCAN0.RMID63.uint8_t[HH]
#define RSCAN0RMPTR63 RSCAN0.RMPTR63.uint32_t
#define RSCAN0RMPTR63L RSCAN0.RMPTR63.uint16_t[L]
#define RSCAN0RMPTR63LL RSCAN0.RMPTR63.uint8_t[LL]
#define RSCAN0RMPTR63LH RSCAN0.RMPTR63.uint8_t[LH]
#define RSCAN0RMPTR63H RSCAN0.RMPTR63.uint16_t[H]
#define RSCAN0RMPTR63HL RSCAN0.RMPTR63.uint8_t[HL]
#define RSCAN0RMPTR63HH RSCAN0.RMPTR63.uint8_t[HH]
#define RSCAN0RMDF063 RSCAN0.RMDF063.uint32_t
#define RSCAN0RMDF063L RSCAN0.RMDF063.uint16_t[L]
#define RSCAN0RMDF063LL RSCAN0.RMDF063.uint8_t[LL]
#define RSCAN0RMDF063LH RSCAN0.RMDF063.uint8_t[LH]
#define RSCAN0RMDF063H RSCAN0.RMDF063.uint16_t[H]
#define RSCAN0RMDF063HL RSCAN0.RMDF063.uint8_t[HL]
#define RSCAN0RMDF063HH RSCAN0.RMDF063.uint8_t[HH]
#define RSCAN0RMDF163 RSCAN0.RMDF163.uint32_t
#define RSCAN0RMDF163L RSCAN0.RMDF163.uint16_t[L]
#define RSCAN0RMDF163LL RSCAN0.RMDF163.uint8_t[LL]
#define RSCAN0RMDF163LH RSCAN0.RMDF163.uint8_t[LH]
#define RSCAN0RMDF163H RSCAN0.RMDF163.uint16_t[H]
#define RSCAN0RMDF163HL RSCAN0.RMDF163.uint8_t[HL]
#define RSCAN0RMDF163HH RSCAN0.RMDF163.uint8_t[HH]
#define RSCAN0RMID64 RSCAN0.RMID64.uint32_t
#define RSCAN0RMID64L RSCAN0.RMID64.uint16_t[L]
#define RSCAN0RMID64LL RSCAN0.RMID64.uint8_t[LL]
#define RSCAN0RMID64LH RSCAN0.RMID64.uint8_t[LH]
#define RSCAN0RMID64H RSCAN0.RMID64.uint16_t[H]
#define RSCAN0RMID64HL RSCAN0.RMID64.uint8_t[HL]
#define RSCAN0RMID64HH RSCAN0.RMID64.uint8_t[HH]
#define RSCAN0RMPTR64 RSCAN0.RMPTR64.uint32_t
#define RSCAN0RMPTR64L RSCAN0.RMPTR64.uint16_t[L]
#define RSCAN0RMPTR64LL RSCAN0.RMPTR64.uint8_t[LL]
#define RSCAN0RMPTR64LH RSCAN0.RMPTR64.uint8_t[LH]
#define RSCAN0RMPTR64H RSCAN0.RMPTR64.uint16_t[H]
#define RSCAN0RMPTR64HL RSCAN0.RMPTR64.uint8_t[HL]
#define RSCAN0RMPTR64HH RSCAN0.RMPTR64.uint8_t[HH]
#define RSCAN0RMDF064 RSCAN0.RMDF064.uint32_t
#define RSCAN0RMDF064L RSCAN0.RMDF064.uint16_t[L]
#define RSCAN0RMDF064LL RSCAN0.RMDF064.uint8_t[LL]
#define RSCAN0RMDF064LH RSCAN0.RMDF064.uint8_t[LH]
#define RSCAN0RMDF064H RSCAN0.RMDF064.uint16_t[H]
#define RSCAN0RMDF064HL RSCAN0.RMDF064.uint8_t[HL]
#define RSCAN0RMDF064HH RSCAN0.RMDF064.uint8_t[HH]
#define RSCAN0RMDF164 RSCAN0.RMDF164.uint32_t
#define RSCAN0RMDF164L RSCAN0.RMDF164.uint16_t[L]
#define RSCAN0RMDF164LL RSCAN0.RMDF164.uint8_t[LL]
#define RSCAN0RMDF164LH RSCAN0.RMDF164.uint8_t[LH]
#define RSCAN0RMDF164H RSCAN0.RMDF164.uint16_t[H]
#define RSCAN0RMDF164HL RSCAN0.RMDF164.uint8_t[HL]
#define RSCAN0RMDF164HH RSCAN0.RMDF164.uint8_t[HH]
#define RSCAN0RMID65 RSCAN0.RMID65.uint32_t
#define RSCAN0RMID65L RSCAN0.RMID65.uint16_t[L]
#define RSCAN0RMID65LL RSCAN0.RMID65.uint8_t[LL]
#define RSCAN0RMID65LH RSCAN0.RMID65.uint8_t[LH]
#define RSCAN0RMID65H RSCAN0.RMID65.uint16_t[H]
#define RSCAN0RMID65HL RSCAN0.RMID65.uint8_t[HL]
#define RSCAN0RMID65HH RSCAN0.RMID65.uint8_t[HH]
#define RSCAN0RMPTR65 RSCAN0.RMPTR65.uint32_t
#define RSCAN0RMPTR65L RSCAN0.RMPTR65.uint16_t[L]
#define RSCAN0RMPTR65LL RSCAN0.RMPTR65.uint8_t[LL]
#define RSCAN0RMPTR65LH RSCAN0.RMPTR65.uint8_t[LH]
#define RSCAN0RMPTR65H RSCAN0.RMPTR65.uint16_t[H]
#define RSCAN0RMPTR65HL RSCAN0.RMPTR65.uint8_t[HL]
#define RSCAN0RMPTR65HH RSCAN0.RMPTR65.uint8_t[HH]
#define RSCAN0RMDF065 RSCAN0.RMDF065.uint32_t
#define RSCAN0RMDF065L RSCAN0.RMDF065.uint16_t[L]
#define RSCAN0RMDF065LL RSCAN0.RMDF065.uint8_t[LL]
#define RSCAN0RMDF065LH RSCAN0.RMDF065.uint8_t[LH]
#define RSCAN0RMDF065H RSCAN0.RMDF065.uint16_t[H]
#define RSCAN0RMDF065HL RSCAN0.RMDF065.uint8_t[HL]
#define RSCAN0RMDF065HH RSCAN0.RMDF065.uint8_t[HH]
#define RSCAN0RMDF165 RSCAN0.RMDF165.uint32_t
#define RSCAN0RMDF165L RSCAN0.RMDF165.uint16_t[L]
#define RSCAN0RMDF165LL RSCAN0.RMDF165.uint8_t[LL]
#define RSCAN0RMDF165LH RSCAN0.RMDF165.uint8_t[LH]
#define RSCAN0RMDF165H RSCAN0.RMDF165.uint16_t[H]
#define RSCAN0RMDF165HL RSCAN0.RMDF165.uint8_t[HL]
#define RSCAN0RMDF165HH RSCAN0.RMDF165.uint8_t[HH]
#define RSCAN0RMID66 RSCAN0.RMID66.uint32_t
#define RSCAN0RMID66L RSCAN0.RMID66.uint16_t[L]
#define RSCAN0RMID66LL RSCAN0.RMID66.uint8_t[LL]
#define RSCAN0RMID66LH RSCAN0.RMID66.uint8_t[LH]
#define RSCAN0RMID66H RSCAN0.RMID66.uint16_t[H]
#define RSCAN0RMID66HL RSCAN0.RMID66.uint8_t[HL]
#define RSCAN0RMID66HH RSCAN0.RMID66.uint8_t[HH]
#define RSCAN0RMPTR66 RSCAN0.RMPTR66.uint32_t
#define RSCAN0RMPTR66L RSCAN0.RMPTR66.uint16_t[L]
#define RSCAN0RMPTR66LL RSCAN0.RMPTR66.uint8_t[LL]
#define RSCAN0RMPTR66LH RSCAN0.RMPTR66.uint8_t[LH]
#define RSCAN0RMPTR66H RSCAN0.RMPTR66.uint16_t[H]
#define RSCAN0RMPTR66HL RSCAN0.RMPTR66.uint8_t[HL]
#define RSCAN0RMPTR66HH RSCAN0.RMPTR66.uint8_t[HH]
#define RSCAN0RMDF066 RSCAN0.RMDF066.uint32_t
#define RSCAN0RMDF066L RSCAN0.RMDF066.uint16_t[L]
#define RSCAN0RMDF066LL RSCAN0.RMDF066.uint8_t[LL]
#define RSCAN0RMDF066LH RSCAN0.RMDF066.uint8_t[LH]
#define RSCAN0RMDF066H RSCAN0.RMDF066.uint16_t[H]
#define RSCAN0RMDF066HL RSCAN0.RMDF066.uint8_t[HL]
#define RSCAN0RMDF066HH RSCAN0.RMDF066.uint8_t[HH]
#define RSCAN0RMDF166 RSCAN0.RMDF166.uint32_t
#define RSCAN0RMDF166L RSCAN0.RMDF166.uint16_t[L]
#define RSCAN0RMDF166LL RSCAN0.RMDF166.uint8_t[LL]
#define RSCAN0RMDF166LH RSCAN0.RMDF166.uint8_t[LH]
#define RSCAN0RMDF166H RSCAN0.RMDF166.uint16_t[H]
#define RSCAN0RMDF166HL RSCAN0.RMDF166.uint8_t[HL]
#define RSCAN0RMDF166HH RSCAN0.RMDF166.uint8_t[HH]
#define RSCAN0RMID67 RSCAN0.RMID67.uint32_t
#define RSCAN0RMID67L RSCAN0.RMID67.uint16_t[L]
#define RSCAN0RMID67LL RSCAN0.RMID67.uint8_t[LL]
#define RSCAN0RMID67LH RSCAN0.RMID67.uint8_t[LH]
#define RSCAN0RMID67H RSCAN0.RMID67.uint16_t[H]
#define RSCAN0RMID67HL RSCAN0.RMID67.uint8_t[HL]
#define RSCAN0RMID67HH RSCAN0.RMID67.uint8_t[HH]
#define RSCAN0RMPTR67 RSCAN0.RMPTR67.uint32_t
#define RSCAN0RMPTR67L RSCAN0.RMPTR67.uint16_t[L]
#define RSCAN0RMPTR67LL RSCAN0.RMPTR67.uint8_t[LL]
#define RSCAN0RMPTR67LH RSCAN0.RMPTR67.uint8_t[LH]
#define RSCAN0RMPTR67H RSCAN0.RMPTR67.uint16_t[H]
#define RSCAN0RMPTR67HL RSCAN0.RMPTR67.uint8_t[HL]
#define RSCAN0RMPTR67HH RSCAN0.RMPTR67.uint8_t[HH]
#define RSCAN0RMDF067 RSCAN0.RMDF067.uint32_t
#define RSCAN0RMDF067L RSCAN0.RMDF067.uint16_t[L]
#define RSCAN0RMDF067LL RSCAN0.RMDF067.uint8_t[LL]
#define RSCAN0RMDF067LH RSCAN0.RMDF067.uint8_t[LH]
#define RSCAN0RMDF067H RSCAN0.RMDF067.uint16_t[H]
#define RSCAN0RMDF067HL RSCAN0.RMDF067.uint8_t[HL]
#define RSCAN0RMDF067HH RSCAN0.RMDF067.uint8_t[HH]
#define RSCAN0RMDF167 RSCAN0.RMDF167.uint32_t
#define RSCAN0RMDF167L RSCAN0.RMDF167.uint16_t[L]
#define RSCAN0RMDF167LL RSCAN0.RMDF167.uint8_t[LL]
#define RSCAN0RMDF167LH RSCAN0.RMDF167.uint8_t[LH]
#define RSCAN0RMDF167H RSCAN0.RMDF167.uint16_t[H]
#define RSCAN0RMDF167HL RSCAN0.RMDF167.uint8_t[HL]
#define RSCAN0RMDF167HH RSCAN0.RMDF167.uint8_t[HH]
#define RSCAN0RMID68 RSCAN0.RMID68.uint32_t
#define RSCAN0RMID68L RSCAN0.RMID68.uint16_t[L]
#define RSCAN0RMID68LL RSCAN0.RMID68.uint8_t[LL]
#define RSCAN0RMID68LH RSCAN0.RMID68.uint8_t[LH]
#define RSCAN0RMID68H RSCAN0.RMID68.uint16_t[H]
#define RSCAN0RMID68HL RSCAN0.RMID68.uint8_t[HL]
#define RSCAN0RMID68HH RSCAN0.RMID68.uint8_t[HH]
#define RSCAN0RMPTR68 RSCAN0.RMPTR68.uint32_t
#define RSCAN0RMPTR68L RSCAN0.RMPTR68.uint16_t[L]
#define RSCAN0RMPTR68LL RSCAN0.RMPTR68.uint8_t[LL]
#define RSCAN0RMPTR68LH RSCAN0.RMPTR68.uint8_t[LH]
#define RSCAN0RMPTR68H RSCAN0.RMPTR68.uint16_t[H]
#define RSCAN0RMPTR68HL RSCAN0.RMPTR68.uint8_t[HL]
#define RSCAN0RMPTR68HH RSCAN0.RMPTR68.uint8_t[HH]
#define RSCAN0RMDF068 RSCAN0.RMDF068.uint32_t
#define RSCAN0RMDF068L RSCAN0.RMDF068.uint16_t[L]
#define RSCAN0RMDF068LL RSCAN0.RMDF068.uint8_t[LL]
#define RSCAN0RMDF068LH RSCAN0.RMDF068.uint8_t[LH]
#define RSCAN0RMDF068H RSCAN0.RMDF068.uint16_t[H]
#define RSCAN0RMDF068HL RSCAN0.RMDF068.uint8_t[HL]
#define RSCAN0RMDF068HH RSCAN0.RMDF068.uint8_t[HH]
#define RSCAN0RMDF168 RSCAN0.RMDF168.uint32_t
#define RSCAN0RMDF168L RSCAN0.RMDF168.uint16_t[L]
#define RSCAN0RMDF168LL RSCAN0.RMDF168.uint8_t[LL]
#define RSCAN0RMDF168LH RSCAN0.RMDF168.uint8_t[LH]
#define RSCAN0RMDF168H RSCAN0.RMDF168.uint16_t[H]
#define RSCAN0RMDF168HL RSCAN0.RMDF168.uint8_t[HL]
#define RSCAN0RMDF168HH RSCAN0.RMDF168.uint8_t[HH]
#define RSCAN0RMID69 RSCAN0.RMID69.uint32_t
#define RSCAN0RMID69L RSCAN0.RMID69.uint16_t[L]
#define RSCAN0RMID69LL RSCAN0.RMID69.uint8_t[LL]
#define RSCAN0RMID69LH RSCAN0.RMID69.uint8_t[LH]
#define RSCAN0RMID69H RSCAN0.RMID69.uint16_t[H]
#define RSCAN0RMID69HL RSCAN0.RMID69.uint8_t[HL]
#define RSCAN0RMID69HH RSCAN0.RMID69.uint8_t[HH]
#define RSCAN0RMPTR69 RSCAN0.RMPTR69.uint32_t
#define RSCAN0RMPTR69L RSCAN0.RMPTR69.uint16_t[L]
#define RSCAN0RMPTR69LL RSCAN0.RMPTR69.uint8_t[LL]
#define RSCAN0RMPTR69LH RSCAN0.RMPTR69.uint8_t[LH]
#define RSCAN0RMPTR69H RSCAN0.RMPTR69.uint16_t[H]
#define RSCAN0RMPTR69HL RSCAN0.RMPTR69.uint8_t[HL]
#define RSCAN0RMPTR69HH RSCAN0.RMPTR69.uint8_t[HH]
#define RSCAN0RMDF069 RSCAN0.RMDF069.uint32_t
#define RSCAN0RMDF069L RSCAN0.RMDF069.uint16_t[L]
#define RSCAN0RMDF069LL RSCAN0.RMDF069.uint8_t[LL]
#define RSCAN0RMDF069LH RSCAN0.RMDF069.uint8_t[LH]
#define RSCAN0RMDF069H RSCAN0.RMDF069.uint16_t[H]
#define RSCAN0RMDF069HL RSCAN0.RMDF069.uint8_t[HL]
#define RSCAN0RMDF069HH RSCAN0.RMDF069.uint8_t[HH]
#define RSCAN0RMDF169 RSCAN0.RMDF169.uint32_t
#define RSCAN0RMDF169L RSCAN0.RMDF169.uint16_t[L]
#define RSCAN0RMDF169LL RSCAN0.RMDF169.uint8_t[LL]
#define RSCAN0RMDF169LH RSCAN0.RMDF169.uint8_t[LH]
#define RSCAN0RMDF169H RSCAN0.RMDF169.uint16_t[H]
#define RSCAN0RMDF169HL RSCAN0.RMDF169.uint8_t[HL]
#define RSCAN0RMDF169HH RSCAN0.RMDF169.uint8_t[HH]
#define RSCAN0RMID70 RSCAN0.RMID70.uint32_t
#define RSCAN0RMID70L RSCAN0.RMID70.uint16_t[L]
#define RSCAN0RMID70LL RSCAN0.RMID70.uint8_t[LL]
#define RSCAN0RMID70LH RSCAN0.RMID70.uint8_t[LH]
#define RSCAN0RMID70H RSCAN0.RMID70.uint16_t[H]
#define RSCAN0RMID70HL RSCAN0.RMID70.uint8_t[HL]
#define RSCAN0RMID70HH RSCAN0.RMID70.uint8_t[HH]
#define RSCAN0RMPTR70 RSCAN0.RMPTR70.uint32_t
#define RSCAN0RMPTR70L RSCAN0.RMPTR70.uint16_t[L]
#define RSCAN0RMPTR70LL RSCAN0.RMPTR70.uint8_t[LL]
#define RSCAN0RMPTR70LH RSCAN0.RMPTR70.uint8_t[LH]
#define RSCAN0RMPTR70H RSCAN0.RMPTR70.uint16_t[H]
#define RSCAN0RMPTR70HL RSCAN0.RMPTR70.uint8_t[HL]
#define RSCAN0RMPTR70HH RSCAN0.RMPTR70.uint8_t[HH]
#define RSCAN0RMDF070 RSCAN0.RMDF070.uint32_t
#define RSCAN0RMDF070L RSCAN0.RMDF070.uint16_t[L]
#define RSCAN0RMDF070LL RSCAN0.RMDF070.uint8_t[LL]
#define RSCAN0RMDF070LH RSCAN0.RMDF070.uint8_t[LH]
#define RSCAN0RMDF070H RSCAN0.RMDF070.uint16_t[H]
#define RSCAN0RMDF070HL RSCAN0.RMDF070.uint8_t[HL]
#define RSCAN0RMDF070HH RSCAN0.RMDF070.uint8_t[HH]
#define RSCAN0RMDF170 RSCAN0.RMDF170.uint32_t
#define RSCAN0RMDF170L RSCAN0.RMDF170.uint16_t[L]
#define RSCAN0RMDF170LL RSCAN0.RMDF170.uint8_t[LL]
#define RSCAN0RMDF170LH RSCAN0.RMDF170.uint8_t[LH]
#define RSCAN0RMDF170H RSCAN0.RMDF170.uint16_t[H]
#define RSCAN0RMDF170HL RSCAN0.RMDF170.uint8_t[HL]
#define RSCAN0RMDF170HH RSCAN0.RMDF170.uint8_t[HH]
#define RSCAN0RMID71 RSCAN0.RMID71.uint32_t
#define RSCAN0RMID71L RSCAN0.RMID71.uint16_t[L]
#define RSCAN0RMID71LL RSCAN0.RMID71.uint8_t[LL]
#define RSCAN0RMID71LH RSCAN0.RMID71.uint8_t[LH]
#define RSCAN0RMID71H RSCAN0.RMID71.uint16_t[H]
#define RSCAN0RMID71HL RSCAN0.RMID71.uint8_t[HL]
#define RSCAN0RMID71HH RSCAN0.RMID71.uint8_t[HH]
#define RSCAN0RMPTR71 RSCAN0.RMPTR71.uint32_t
#define RSCAN0RMPTR71L RSCAN0.RMPTR71.uint16_t[L]
#define RSCAN0RMPTR71LL RSCAN0.RMPTR71.uint8_t[LL]
#define RSCAN0RMPTR71LH RSCAN0.RMPTR71.uint8_t[LH]
#define RSCAN0RMPTR71H RSCAN0.RMPTR71.uint16_t[H]
#define RSCAN0RMPTR71HL RSCAN0.RMPTR71.uint8_t[HL]
#define RSCAN0RMPTR71HH RSCAN0.RMPTR71.uint8_t[HH]
#define RSCAN0RMDF071 RSCAN0.RMDF071.uint32_t
#define RSCAN0RMDF071L RSCAN0.RMDF071.uint16_t[L]
#define RSCAN0RMDF071LL RSCAN0.RMDF071.uint8_t[LL]
#define RSCAN0RMDF071LH RSCAN0.RMDF071.uint8_t[LH]
#define RSCAN0RMDF071H RSCAN0.RMDF071.uint16_t[H]
#define RSCAN0RMDF071HL RSCAN0.RMDF071.uint8_t[HL]
#define RSCAN0RMDF071HH RSCAN0.RMDF071.uint8_t[HH]
#define RSCAN0RMDF171 RSCAN0.RMDF171.uint32_t
#define RSCAN0RMDF171L RSCAN0.RMDF171.uint16_t[L]
#define RSCAN0RMDF171LL RSCAN0.RMDF171.uint8_t[LL]
#define RSCAN0RMDF171LH RSCAN0.RMDF171.uint8_t[LH]
#define RSCAN0RMDF171H RSCAN0.RMDF171.uint16_t[H]
#define RSCAN0RMDF171HL RSCAN0.RMDF171.uint8_t[HL]
#define RSCAN0RMDF171HH RSCAN0.RMDF171.uint8_t[HH]
#define RSCAN0RMID72 RSCAN0.RMID72.uint32_t
#define RSCAN0RMID72L RSCAN0.RMID72.uint16_t[L]
#define RSCAN0RMID72LL RSCAN0.RMID72.uint8_t[LL]
#define RSCAN0RMID72LH RSCAN0.RMID72.uint8_t[LH]
#define RSCAN0RMID72H RSCAN0.RMID72.uint16_t[H]
#define RSCAN0RMID72HL RSCAN0.RMID72.uint8_t[HL]
#define RSCAN0RMID72HH RSCAN0.RMID72.uint8_t[HH]
#define RSCAN0RMPTR72 RSCAN0.RMPTR72.uint32_t
#define RSCAN0RMPTR72L RSCAN0.RMPTR72.uint16_t[L]
#define RSCAN0RMPTR72LL RSCAN0.RMPTR72.uint8_t[LL]
#define RSCAN0RMPTR72LH RSCAN0.RMPTR72.uint8_t[LH]
#define RSCAN0RMPTR72H RSCAN0.RMPTR72.uint16_t[H]
#define RSCAN0RMPTR72HL RSCAN0.RMPTR72.uint8_t[HL]
#define RSCAN0RMPTR72HH RSCAN0.RMPTR72.uint8_t[HH]
#define RSCAN0RMDF072 RSCAN0.RMDF072.uint32_t
#define RSCAN0RMDF072L RSCAN0.RMDF072.uint16_t[L]
#define RSCAN0RMDF072LL RSCAN0.RMDF072.uint8_t[LL]
#define RSCAN0RMDF072LH RSCAN0.RMDF072.uint8_t[LH]
#define RSCAN0RMDF072H RSCAN0.RMDF072.uint16_t[H]
#define RSCAN0RMDF072HL RSCAN0.RMDF072.uint8_t[HL]
#define RSCAN0RMDF072HH RSCAN0.RMDF072.uint8_t[HH]
#define RSCAN0RMDF172 RSCAN0.RMDF172.uint32_t
#define RSCAN0RMDF172L RSCAN0.RMDF172.uint16_t[L]
#define RSCAN0RMDF172LL RSCAN0.RMDF172.uint8_t[LL]
#define RSCAN0RMDF172LH RSCAN0.RMDF172.uint8_t[LH]
#define RSCAN0RMDF172H RSCAN0.RMDF172.uint16_t[H]
#define RSCAN0RMDF172HL RSCAN0.RMDF172.uint8_t[HL]
#define RSCAN0RMDF172HH RSCAN0.RMDF172.uint8_t[HH]
#define RSCAN0RMID73 RSCAN0.RMID73.uint32_t
#define RSCAN0RMID73L RSCAN0.RMID73.uint16_t[L]
#define RSCAN0RMID73LL RSCAN0.RMID73.uint8_t[LL]
#define RSCAN0RMID73LH RSCAN0.RMID73.uint8_t[LH]
#define RSCAN0RMID73H RSCAN0.RMID73.uint16_t[H]
#define RSCAN0RMID73HL RSCAN0.RMID73.uint8_t[HL]
#define RSCAN0RMID73HH RSCAN0.RMID73.uint8_t[HH]
#define RSCAN0RMPTR73 RSCAN0.RMPTR73.uint32_t
#define RSCAN0RMPTR73L RSCAN0.RMPTR73.uint16_t[L]
#define RSCAN0RMPTR73LL RSCAN0.RMPTR73.uint8_t[LL]
#define RSCAN0RMPTR73LH RSCAN0.RMPTR73.uint8_t[LH]
#define RSCAN0RMPTR73H RSCAN0.RMPTR73.uint16_t[H]
#define RSCAN0RMPTR73HL RSCAN0.RMPTR73.uint8_t[HL]
#define RSCAN0RMPTR73HH RSCAN0.RMPTR73.uint8_t[HH]
#define RSCAN0RMDF073 RSCAN0.RMDF073.uint32_t
#define RSCAN0RMDF073L RSCAN0.RMDF073.uint16_t[L]
#define RSCAN0RMDF073LL RSCAN0.RMDF073.uint8_t[LL]
#define RSCAN0RMDF073LH RSCAN0.RMDF073.uint8_t[LH]
#define RSCAN0RMDF073H RSCAN0.RMDF073.uint16_t[H]
#define RSCAN0RMDF073HL RSCAN0.RMDF073.uint8_t[HL]
#define RSCAN0RMDF073HH RSCAN0.RMDF073.uint8_t[HH]
#define RSCAN0RMDF173 RSCAN0.RMDF173.uint32_t
#define RSCAN0RMDF173L RSCAN0.RMDF173.uint16_t[L]
#define RSCAN0RMDF173LL RSCAN0.RMDF173.uint8_t[LL]
#define RSCAN0RMDF173LH RSCAN0.RMDF173.uint8_t[LH]
#define RSCAN0RMDF173H RSCAN0.RMDF173.uint16_t[H]
#define RSCAN0RMDF173HL RSCAN0.RMDF173.uint8_t[HL]
#define RSCAN0RMDF173HH RSCAN0.RMDF173.uint8_t[HH]
#define RSCAN0RMID74 RSCAN0.RMID74.uint32_t
#define RSCAN0RMID74L RSCAN0.RMID74.uint16_t[L]
#define RSCAN0RMID74LL RSCAN0.RMID74.uint8_t[LL]
#define RSCAN0RMID74LH RSCAN0.RMID74.uint8_t[LH]
#define RSCAN0RMID74H RSCAN0.RMID74.uint16_t[H]
#define RSCAN0RMID74HL RSCAN0.RMID74.uint8_t[HL]
#define RSCAN0RMID74HH RSCAN0.RMID74.uint8_t[HH]
#define RSCAN0RMPTR74 RSCAN0.RMPTR74.uint32_t
#define RSCAN0RMPTR74L RSCAN0.RMPTR74.uint16_t[L]
#define RSCAN0RMPTR74LL RSCAN0.RMPTR74.uint8_t[LL]
#define RSCAN0RMPTR74LH RSCAN0.RMPTR74.uint8_t[LH]
#define RSCAN0RMPTR74H RSCAN0.RMPTR74.uint16_t[H]
#define RSCAN0RMPTR74HL RSCAN0.RMPTR74.uint8_t[HL]
#define RSCAN0RMPTR74HH RSCAN0.RMPTR74.uint8_t[HH]
#define RSCAN0RMDF074 RSCAN0.RMDF074.uint32_t
#define RSCAN0RMDF074L RSCAN0.RMDF074.uint16_t[L]
#define RSCAN0RMDF074LL RSCAN0.RMDF074.uint8_t[LL]
#define RSCAN0RMDF074LH RSCAN0.RMDF074.uint8_t[LH]
#define RSCAN0RMDF074H RSCAN0.RMDF074.uint16_t[H]
#define RSCAN0RMDF074HL RSCAN0.RMDF074.uint8_t[HL]
#define RSCAN0RMDF074HH RSCAN0.RMDF074.uint8_t[HH]
#define RSCAN0RMDF174 RSCAN0.RMDF174.uint32_t
#define RSCAN0RMDF174L RSCAN0.RMDF174.uint16_t[L]
#define RSCAN0RMDF174LL RSCAN0.RMDF174.uint8_t[LL]
#define RSCAN0RMDF174LH RSCAN0.RMDF174.uint8_t[LH]
#define RSCAN0RMDF174H RSCAN0.RMDF174.uint16_t[H]
#define RSCAN0RMDF174HL RSCAN0.RMDF174.uint8_t[HL]
#define RSCAN0RMDF174HH RSCAN0.RMDF174.uint8_t[HH]
#define RSCAN0RMID75 RSCAN0.RMID75.uint32_t
#define RSCAN0RMID75L RSCAN0.RMID75.uint16_t[L]
#define RSCAN0RMID75LL RSCAN0.RMID75.uint8_t[LL]
#define RSCAN0RMID75LH RSCAN0.RMID75.uint8_t[LH]
#define RSCAN0RMID75H RSCAN0.RMID75.uint16_t[H]
#define RSCAN0RMID75HL RSCAN0.RMID75.uint8_t[HL]
#define RSCAN0RMID75HH RSCAN0.RMID75.uint8_t[HH]
#define RSCAN0RMPTR75 RSCAN0.RMPTR75.uint32_t
#define RSCAN0RMPTR75L RSCAN0.RMPTR75.uint16_t[L]
#define RSCAN0RMPTR75LL RSCAN0.RMPTR75.uint8_t[LL]
#define RSCAN0RMPTR75LH RSCAN0.RMPTR75.uint8_t[LH]
#define RSCAN0RMPTR75H RSCAN0.RMPTR75.uint16_t[H]
#define RSCAN0RMPTR75HL RSCAN0.RMPTR75.uint8_t[HL]
#define RSCAN0RMPTR75HH RSCAN0.RMPTR75.uint8_t[HH]
#define RSCAN0RMDF075 RSCAN0.RMDF075.uint32_t
#define RSCAN0RMDF075L RSCAN0.RMDF075.uint16_t[L]
#define RSCAN0RMDF075LL RSCAN0.RMDF075.uint8_t[LL]
#define RSCAN0RMDF075LH RSCAN0.RMDF075.uint8_t[LH]
#define RSCAN0RMDF075H RSCAN0.RMDF075.uint16_t[H]
#define RSCAN0RMDF075HL RSCAN0.RMDF075.uint8_t[HL]
#define RSCAN0RMDF075HH RSCAN0.RMDF075.uint8_t[HH]
#define RSCAN0RMDF175 RSCAN0.RMDF175.uint32_t
#define RSCAN0RMDF175L RSCAN0.RMDF175.uint16_t[L]
#define RSCAN0RMDF175LL RSCAN0.RMDF175.uint8_t[LL]
#define RSCAN0RMDF175LH RSCAN0.RMDF175.uint8_t[LH]
#define RSCAN0RMDF175H RSCAN0.RMDF175.uint16_t[H]
#define RSCAN0RMDF175HL RSCAN0.RMDF175.uint8_t[HL]
#define RSCAN0RMDF175HH RSCAN0.RMDF175.uint8_t[HH]
#define RSCAN0RMID76 RSCAN0.RMID76.uint32_t
#define RSCAN0RMID76L RSCAN0.RMID76.uint16_t[L]
#define RSCAN0RMID76LL RSCAN0.RMID76.uint8_t[LL]
#define RSCAN0RMID76LH RSCAN0.RMID76.uint8_t[LH]
#define RSCAN0RMID76H RSCAN0.RMID76.uint16_t[H]
#define RSCAN0RMID76HL RSCAN0.RMID76.uint8_t[HL]
#define RSCAN0RMID76HH RSCAN0.RMID76.uint8_t[HH]
#define RSCAN0RMPTR76 RSCAN0.RMPTR76.uint32_t
#define RSCAN0RMPTR76L RSCAN0.RMPTR76.uint16_t[L]
#define RSCAN0RMPTR76LL RSCAN0.RMPTR76.uint8_t[LL]
#define RSCAN0RMPTR76LH RSCAN0.RMPTR76.uint8_t[LH]
#define RSCAN0RMPTR76H RSCAN0.RMPTR76.uint16_t[H]
#define RSCAN0RMPTR76HL RSCAN0.RMPTR76.uint8_t[HL]
#define RSCAN0RMPTR76HH RSCAN0.RMPTR76.uint8_t[HH]
#define RSCAN0RMDF076 RSCAN0.RMDF076.uint32_t
#define RSCAN0RMDF076L RSCAN0.RMDF076.uint16_t[L]
#define RSCAN0RMDF076LL RSCAN0.RMDF076.uint8_t[LL]
#define RSCAN0RMDF076LH RSCAN0.RMDF076.uint8_t[LH]
#define RSCAN0RMDF076H RSCAN0.RMDF076.uint16_t[H]
#define RSCAN0RMDF076HL RSCAN0.RMDF076.uint8_t[HL]
#define RSCAN0RMDF076HH RSCAN0.RMDF076.uint8_t[HH]
#define RSCAN0RMDF176 RSCAN0.RMDF176.uint32_t
#define RSCAN0RMDF176L RSCAN0.RMDF176.uint16_t[L]
#define RSCAN0RMDF176LL RSCAN0.RMDF176.uint8_t[LL]
#define RSCAN0RMDF176LH RSCAN0.RMDF176.uint8_t[LH]
#define RSCAN0RMDF176H RSCAN0.RMDF176.uint16_t[H]
#define RSCAN0RMDF176HL RSCAN0.RMDF176.uint8_t[HL]
#define RSCAN0RMDF176HH RSCAN0.RMDF176.uint8_t[HH]
#define RSCAN0RMID77 RSCAN0.RMID77.uint32_t
#define RSCAN0RMID77L RSCAN0.RMID77.uint16_t[L]
#define RSCAN0RMID77LL RSCAN0.RMID77.uint8_t[LL]
#define RSCAN0RMID77LH RSCAN0.RMID77.uint8_t[LH]
#define RSCAN0RMID77H RSCAN0.RMID77.uint16_t[H]
#define RSCAN0RMID77HL RSCAN0.RMID77.uint8_t[HL]
#define RSCAN0RMID77HH RSCAN0.RMID77.uint8_t[HH]
#define RSCAN0RMPTR77 RSCAN0.RMPTR77.uint32_t
#define RSCAN0RMPTR77L RSCAN0.RMPTR77.uint16_t[L]
#define RSCAN0RMPTR77LL RSCAN0.RMPTR77.uint8_t[LL]
#define RSCAN0RMPTR77LH RSCAN0.RMPTR77.uint8_t[LH]
#define RSCAN0RMPTR77H RSCAN0.RMPTR77.uint16_t[H]
#define RSCAN0RMPTR77HL RSCAN0.RMPTR77.uint8_t[HL]
#define RSCAN0RMPTR77HH RSCAN0.RMPTR77.uint8_t[HH]
#define RSCAN0RMDF077 RSCAN0.RMDF077.uint32_t
#define RSCAN0RMDF077L RSCAN0.RMDF077.uint16_t[L]
#define RSCAN0RMDF077LL RSCAN0.RMDF077.uint8_t[LL]
#define RSCAN0RMDF077LH RSCAN0.RMDF077.uint8_t[LH]
#define RSCAN0RMDF077H RSCAN0.RMDF077.uint16_t[H]
#define RSCAN0RMDF077HL RSCAN0.RMDF077.uint8_t[HL]
#define RSCAN0RMDF077HH RSCAN0.RMDF077.uint8_t[HH]
#define RSCAN0RMDF177 RSCAN0.RMDF177.uint32_t
#define RSCAN0RMDF177L RSCAN0.RMDF177.uint16_t[L]
#define RSCAN0RMDF177LL RSCAN0.RMDF177.uint8_t[LL]
#define RSCAN0RMDF177LH RSCAN0.RMDF177.uint8_t[LH]
#define RSCAN0RMDF177H RSCAN0.RMDF177.uint16_t[H]
#define RSCAN0RMDF177HL RSCAN0.RMDF177.uint8_t[HL]
#define RSCAN0RMDF177HH RSCAN0.RMDF177.uint8_t[HH]
#define RSCAN0RMID78 RSCAN0.RMID78.uint32_t
#define RSCAN0RMID78L RSCAN0.RMID78.uint16_t[L]
#define RSCAN0RMID78LL RSCAN0.RMID78.uint8_t[LL]
#define RSCAN0RMID78LH RSCAN0.RMID78.uint8_t[LH]
#define RSCAN0RMID78H RSCAN0.RMID78.uint16_t[H]
#define RSCAN0RMID78HL RSCAN0.RMID78.uint8_t[HL]
#define RSCAN0RMID78HH RSCAN0.RMID78.uint8_t[HH]
#define RSCAN0RMPTR78 RSCAN0.RMPTR78.uint32_t
#define RSCAN0RMPTR78L RSCAN0.RMPTR78.uint16_t[L]
#define RSCAN0RMPTR78LL RSCAN0.RMPTR78.uint8_t[LL]
#define RSCAN0RMPTR78LH RSCAN0.RMPTR78.uint8_t[LH]
#define RSCAN0RMPTR78H RSCAN0.RMPTR78.uint16_t[H]
#define RSCAN0RMPTR78HL RSCAN0.RMPTR78.uint8_t[HL]
#define RSCAN0RMPTR78HH RSCAN0.RMPTR78.uint8_t[HH]
#define RSCAN0RMDF078 RSCAN0.RMDF078.uint32_t
#define RSCAN0RMDF078L RSCAN0.RMDF078.uint16_t[L]
#define RSCAN0RMDF078LL RSCAN0.RMDF078.uint8_t[LL]
#define RSCAN0RMDF078LH RSCAN0.RMDF078.uint8_t[LH]
#define RSCAN0RMDF078H RSCAN0.RMDF078.uint16_t[H]
#define RSCAN0RMDF078HL RSCAN0.RMDF078.uint8_t[HL]
#define RSCAN0RMDF078HH RSCAN0.RMDF078.uint8_t[HH]
#define RSCAN0RMDF178 RSCAN0.RMDF178.uint32_t
#define RSCAN0RMDF178L RSCAN0.RMDF178.uint16_t[L]
#define RSCAN0RMDF178LL RSCAN0.RMDF178.uint8_t[LL]
#define RSCAN0RMDF178LH RSCAN0.RMDF178.uint8_t[LH]
#define RSCAN0RMDF178H RSCAN0.RMDF178.uint16_t[H]
#define RSCAN0RMDF178HL RSCAN0.RMDF178.uint8_t[HL]
#define RSCAN0RMDF178HH RSCAN0.RMDF178.uint8_t[HH]
#define RSCAN0RMID79 RSCAN0.RMID79.uint32_t
#define RSCAN0RMID79L RSCAN0.RMID79.uint16_t[L]
#define RSCAN0RMID79LL RSCAN0.RMID79.uint8_t[LL]
#define RSCAN0RMID79LH RSCAN0.RMID79.uint8_t[LH]
#define RSCAN0RMID79H RSCAN0.RMID79.uint16_t[H]
#define RSCAN0RMID79HL RSCAN0.RMID79.uint8_t[HL]
#define RSCAN0RMID79HH RSCAN0.RMID79.uint8_t[HH]
#define RSCAN0RMPTR79 RSCAN0.RMPTR79.uint32_t
#define RSCAN0RMPTR79L RSCAN0.RMPTR79.uint16_t[L]
#define RSCAN0RMPTR79LL RSCAN0.RMPTR79.uint8_t[LL]
#define RSCAN0RMPTR79LH RSCAN0.RMPTR79.uint8_t[LH]
#define RSCAN0RMPTR79H RSCAN0.RMPTR79.uint16_t[H]
#define RSCAN0RMPTR79HL RSCAN0.RMPTR79.uint8_t[HL]
#define RSCAN0RMPTR79HH RSCAN0.RMPTR79.uint8_t[HH]
#define RSCAN0RMDF079 RSCAN0.RMDF079.uint32_t
#define RSCAN0RMDF079L RSCAN0.RMDF079.uint16_t[L]
#define RSCAN0RMDF079LL RSCAN0.RMDF079.uint8_t[LL]
#define RSCAN0RMDF079LH RSCAN0.RMDF079.uint8_t[LH]
#define RSCAN0RMDF079H RSCAN0.RMDF079.uint16_t[H]
#define RSCAN0RMDF079HL RSCAN0.RMDF079.uint8_t[HL]
#define RSCAN0RMDF079HH RSCAN0.RMDF079.uint8_t[HH]
#define RSCAN0RMDF179 RSCAN0.RMDF179.uint32_t
#define RSCAN0RMDF179L RSCAN0.RMDF179.uint16_t[L]
#define RSCAN0RMDF179LL RSCAN0.RMDF179.uint8_t[LL]
#define RSCAN0RMDF179LH RSCAN0.RMDF179.uint8_t[LH]
#define RSCAN0RMDF179H RSCAN0.RMDF179.uint16_t[H]
#define RSCAN0RMDF179HL RSCAN0.RMDF179.uint8_t[HL]
#define RSCAN0RMDF179HH RSCAN0.RMDF179.uint8_t[HH]
#define RSCAN0RMID80 RSCAN0.RMID80.uint32_t
#define RSCAN0RMID80L RSCAN0.RMID80.uint16_t[L]
#define RSCAN0RMID80LL RSCAN0.RMID80.uint8_t[LL]
#define RSCAN0RMID80LH RSCAN0.RMID80.uint8_t[LH]
#define RSCAN0RMID80H RSCAN0.RMID80.uint16_t[H]
#define RSCAN0RMID80HL RSCAN0.RMID80.uint8_t[HL]
#define RSCAN0RMID80HH RSCAN0.RMID80.uint8_t[HH]
#define RSCAN0RMPTR80 RSCAN0.RMPTR80.uint32_t
#define RSCAN0RMPTR80L RSCAN0.RMPTR80.uint16_t[L]
#define RSCAN0RMPTR80LL RSCAN0.RMPTR80.uint8_t[LL]
#define RSCAN0RMPTR80LH RSCAN0.RMPTR80.uint8_t[LH]
#define RSCAN0RMPTR80H RSCAN0.RMPTR80.uint16_t[H]
#define RSCAN0RMPTR80HL RSCAN0.RMPTR80.uint8_t[HL]
#define RSCAN0RMPTR80HH RSCAN0.RMPTR80.uint8_t[HH]
#define RSCAN0RMDF080 RSCAN0.RMDF080.uint32_t
#define RSCAN0RMDF080L RSCAN0.RMDF080.uint16_t[L]
#define RSCAN0RMDF080LL RSCAN0.RMDF080.uint8_t[LL]
#define RSCAN0RMDF080LH RSCAN0.RMDF080.uint8_t[LH]
#define RSCAN0RMDF080H RSCAN0.RMDF080.uint16_t[H]
#define RSCAN0RMDF080HL RSCAN0.RMDF080.uint8_t[HL]
#define RSCAN0RMDF080HH RSCAN0.RMDF080.uint8_t[HH]
#define RSCAN0RMDF180 RSCAN0.RMDF180.uint32_t
#define RSCAN0RMDF180L RSCAN0.RMDF180.uint16_t[L]
#define RSCAN0RMDF180LL RSCAN0.RMDF180.uint8_t[LL]
#define RSCAN0RMDF180LH RSCAN0.RMDF180.uint8_t[LH]
#define RSCAN0RMDF180H RSCAN0.RMDF180.uint16_t[H]
#define RSCAN0RMDF180HL RSCAN0.RMDF180.uint8_t[HL]
#define RSCAN0RMDF180HH RSCAN0.RMDF180.uint8_t[HH]
#define RSCAN0RMID81 RSCAN0.RMID81.uint32_t
#define RSCAN0RMID81L RSCAN0.RMID81.uint16_t[L]
#define RSCAN0RMID81LL RSCAN0.RMID81.uint8_t[LL]
#define RSCAN0RMID81LH RSCAN0.RMID81.uint8_t[LH]
#define RSCAN0RMID81H RSCAN0.RMID81.uint16_t[H]
#define RSCAN0RMID81HL RSCAN0.RMID81.uint8_t[HL]
#define RSCAN0RMID81HH RSCAN0.RMID81.uint8_t[HH]
#define RSCAN0RMPTR81 RSCAN0.RMPTR81.uint32_t
#define RSCAN0RMPTR81L RSCAN0.RMPTR81.uint16_t[L]
#define RSCAN0RMPTR81LL RSCAN0.RMPTR81.uint8_t[LL]
#define RSCAN0RMPTR81LH RSCAN0.RMPTR81.uint8_t[LH]
#define RSCAN0RMPTR81H RSCAN0.RMPTR81.uint16_t[H]
#define RSCAN0RMPTR81HL RSCAN0.RMPTR81.uint8_t[HL]
#define RSCAN0RMPTR81HH RSCAN0.RMPTR81.uint8_t[HH]
#define RSCAN0RMDF081 RSCAN0.RMDF081.uint32_t
#define RSCAN0RMDF081L RSCAN0.RMDF081.uint16_t[L]
#define RSCAN0RMDF081LL RSCAN0.RMDF081.uint8_t[LL]
#define RSCAN0RMDF081LH RSCAN0.RMDF081.uint8_t[LH]
#define RSCAN0RMDF081H RSCAN0.RMDF081.uint16_t[H]
#define RSCAN0RMDF081HL RSCAN0.RMDF081.uint8_t[HL]
#define RSCAN0RMDF081HH RSCAN0.RMDF081.uint8_t[HH]
#define RSCAN0RMDF181 RSCAN0.RMDF181.uint32_t
#define RSCAN0RMDF181L RSCAN0.RMDF181.uint16_t[L]
#define RSCAN0RMDF181LL RSCAN0.RMDF181.uint8_t[LL]
#define RSCAN0RMDF181LH RSCAN0.RMDF181.uint8_t[LH]
#define RSCAN0RMDF181H RSCAN0.RMDF181.uint16_t[H]
#define RSCAN0RMDF181HL RSCAN0.RMDF181.uint8_t[HL]
#define RSCAN0RMDF181HH RSCAN0.RMDF181.uint8_t[HH]
#define RSCAN0RMID82 RSCAN0.RMID82.uint32_t
#define RSCAN0RMID82L RSCAN0.RMID82.uint16_t[L]
#define RSCAN0RMID82LL RSCAN0.RMID82.uint8_t[LL]
#define RSCAN0RMID82LH RSCAN0.RMID82.uint8_t[LH]
#define RSCAN0RMID82H RSCAN0.RMID82.uint16_t[H]
#define RSCAN0RMID82HL RSCAN0.RMID82.uint8_t[HL]
#define RSCAN0RMID82HH RSCAN0.RMID82.uint8_t[HH]
#define RSCAN0RMPTR82 RSCAN0.RMPTR82.uint32_t
#define RSCAN0RMPTR82L RSCAN0.RMPTR82.uint16_t[L]
#define RSCAN0RMPTR82LL RSCAN0.RMPTR82.uint8_t[LL]
#define RSCAN0RMPTR82LH RSCAN0.RMPTR82.uint8_t[LH]
#define RSCAN0RMPTR82H RSCAN0.RMPTR82.uint16_t[H]
#define RSCAN0RMPTR82HL RSCAN0.RMPTR82.uint8_t[HL]
#define RSCAN0RMPTR82HH RSCAN0.RMPTR82.uint8_t[HH]
#define RSCAN0RMDF082 RSCAN0.RMDF082.uint32_t
#define RSCAN0RMDF082L RSCAN0.RMDF082.uint16_t[L]
#define RSCAN0RMDF082LL RSCAN0.RMDF082.uint8_t[LL]
#define RSCAN0RMDF082LH RSCAN0.RMDF082.uint8_t[LH]
#define RSCAN0RMDF082H RSCAN0.RMDF082.uint16_t[H]
#define RSCAN0RMDF082HL RSCAN0.RMDF082.uint8_t[HL]
#define RSCAN0RMDF082HH RSCAN0.RMDF082.uint8_t[HH]
#define RSCAN0RMDF182 RSCAN0.RMDF182.uint32_t
#define RSCAN0RMDF182L RSCAN0.RMDF182.uint16_t[L]
#define RSCAN0RMDF182LL RSCAN0.RMDF182.uint8_t[LL]
#define RSCAN0RMDF182LH RSCAN0.RMDF182.uint8_t[LH]
#define RSCAN0RMDF182H RSCAN0.RMDF182.uint16_t[H]
#define RSCAN0RMDF182HL RSCAN0.RMDF182.uint8_t[HL]
#define RSCAN0RMDF182HH RSCAN0.RMDF182.uint8_t[HH]
#define RSCAN0RMID83 RSCAN0.RMID83.uint32_t
#define RSCAN0RMID83L RSCAN0.RMID83.uint16_t[L]
#define RSCAN0RMID83LL RSCAN0.RMID83.uint8_t[LL]
#define RSCAN0RMID83LH RSCAN0.RMID83.uint8_t[LH]
#define RSCAN0RMID83H RSCAN0.RMID83.uint16_t[H]
#define RSCAN0RMID83HL RSCAN0.RMID83.uint8_t[HL]
#define RSCAN0RMID83HH RSCAN0.RMID83.uint8_t[HH]
#define RSCAN0RMPTR83 RSCAN0.RMPTR83.uint32_t
#define RSCAN0RMPTR83L RSCAN0.RMPTR83.uint16_t[L]
#define RSCAN0RMPTR83LL RSCAN0.RMPTR83.uint8_t[LL]
#define RSCAN0RMPTR83LH RSCAN0.RMPTR83.uint8_t[LH]
#define RSCAN0RMPTR83H RSCAN0.RMPTR83.uint16_t[H]
#define RSCAN0RMPTR83HL RSCAN0.RMPTR83.uint8_t[HL]
#define RSCAN0RMPTR83HH RSCAN0.RMPTR83.uint8_t[HH]
#define RSCAN0RMDF083 RSCAN0.RMDF083.uint32_t
#define RSCAN0RMDF083L RSCAN0.RMDF083.uint16_t[L]
#define RSCAN0RMDF083LL RSCAN0.RMDF083.uint8_t[LL]
#define RSCAN0RMDF083LH RSCAN0.RMDF083.uint8_t[LH]
#define RSCAN0RMDF083H RSCAN0.RMDF083.uint16_t[H]
#define RSCAN0RMDF083HL RSCAN0.RMDF083.uint8_t[HL]
#define RSCAN0RMDF083HH RSCAN0.RMDF083.uint8_t[HH]
#define RSCAN0RMDF183 RSCAN0.RMDF183.uint32_t
#define RSCAN0RMDF183L RSCAN0.RMDF183.uint16_t[L]
#define RSCAN0RMDF183LL RSCAN0.RMDF183.uint8_t[LL]
#define RSCAN0RMDF183LH RSCAN0.RMDF183.uint8_t[LH]
#define RSCAN0RMDF183H RSCAN0.RMDF183.uint16_t[H]
#define RSCAN0RMDF183HL RSCAN0.RMDF183.uint8_t[HL]
#define RSCAN0RMDF183HH RSCAN0.RMDF183.uint8_t[HH]
#define RSCAN0RMID84 RSCAN0.RMID84.uint32_t
#define RSCAN0RMID84L RSCAN0.RMID84.uint16_t[L]
#define RSCAN0RMID84LL RSCAN0.RMID84.uint8_t[LL]
#define RSCAN0RMID84LH RSCAN0.RMID84.uint8_t[LH]
#define RSCAN0RMID84H RSCAN0.RMID84.uint16_t[H]
#define RSCAN0RMID84HL RSCAN0.RMID84.uint8_t[HL]
#define RSCAN0RMID84HH RSCAN0.RMID84.uint8_t[HH]
#define RSCAN0RMPTR84 RSCAN0.RMPTR84.uint32_t
#define RSCAN0RMPTR84L RSCAN0.RMPTR84.uint16_t[L]
#define RSCAN0RMPTR84LL RSCAN0.RMPTR84.uint8_t[LL]
#define RSCAN0RMPTR84LH RSCAN0.RMPTR84.uint8_t[LH]
#define RSCAN0RMPTR84H RSCAN0.RMPTR84.uint16_t[H]
#define RSCAN0RMPTR84HL RSCAN0.RMPTR84.uint8_t[HL]
#define RSCAN0RMPTR84HH RSCAN0.RMPTR84.uint8_t[HH]
#define RSCAN0RMDF084 RSCAN0.RMDF084.uint32_t
#define RSCAN0RMDF084L RSCAN0.RMDF084.uint16_t[L]
#define RSCAN0RMDF084LL RSCAN0.RMDF084.uint8_t[LL]
#define RSCAN0RMDF084LH RSCAN0.RMDF084.uint8_t[LH]
#define RSCAN0RMDF084H RSCAN0.RMDF084.uint16_t[H]
#define RSCAN0RMDF084HL RSCAN0.RMDF084.uint8_t[HL]
#define RSCAN0RMDF084HH RSCAN0.RMDF084.uint8_t[HH]
#define RSCAN0RMDF184 RSCAN0.RMDF184.uint32_t
#define RSCAN0RMDF184L RSCAN0.RMDF184.uint16_t[L]
#define RSCAN0RMDF184LL RSCAN0.RMDF184.uint8_t[LL]
#define RSCAN0RMDF184LH RSCAN0.RMDF184.uint8_t[LH]
#define RSCAN0RMDF184H RSCAN0.RMDF184.uint16_t[H]
#define RSCAN0RMDF184HL RSCAN0.RMDF184.uint8_t[HL]
#define RSCAN0RMDF184HH RSCAN0.RMDF184.uint8_t[HH]
#define RSCAN0RMID85 RSCAN0.RMID85.uint32_t
#define RSCAN0RMID85L RSCAN0.RMID85.uint16_t[L]
#define RSCAN0RMID85LL RSCAN0.RMID85.uint8_t[LL]
#define RSCAN0RMID85LH RSCAN0.RMID85.uint8_t[LH]
#define RSCAN0RMID85H RSCAN0.RMID85.uint16_t[H]
#define RSCAN0RMID85HL RSCAN0.RMID85.uint8_t[HL]
#define RSCAN0RMID85HH RSCAN0.RMID85.uint8_t[HH]
#define RSCAN0RMPTR85 RSCAN0.RMPTR85.uint32_t
#define RSCAN0RMPTR85L RSCAN0.RMPTR85.uint16_t[L]
#define RSCAN0RMPTR85LL RSCAN0.RMPTR85.uint8_t[LL]
#define RSCAN0RMPTR85LH RSCAN0.RMPTR85.uint8_t[LH]
#define RSCAN0RMPTR85H RSCAN0.RMPTR85.uint16_t[H]
#define RSCAN0RMPTR85HL RSCAN0.RMPTR85.uint8_t[HL]
#define RSCAN0RMPTR85HH RSCAN0.RMPTR85.uint8_t[HH]
#define RSCAN0RMDF085 RSCAN0.RMDF085.uint32_t
#define RSCAN0RMDF085L RSCAN0.RMDF085.uint16_t[L]
#define RSCAN0RMDF085LL RSCAN0.RMDF085.uint8_t[LL]
#define RSCAN0RMDF085LH RSCAN0.RMDF085.uint8_t[LH]
#define RSCAN0RMDF085H RSCAN0.RMDF085.uint16_t[H]
#define RSCAN0RMDF085HL RSCAN0.RMDF085.uint8_t[HL]
#define RSCAN0RMDF085HH RSCAN0.RMDF085.uint8_t[HH]
#define RSCAN0RMDF185 RSCAN0.RMDF185.uint32_t
#define RSCAN0RMDF185L RSCAN0.RMDF185.uint16_t[L]
#define RSCAN0RMDF185LL RSCAN0.RMDF185.uint8_t[LL]
#define RSCAN0RMDF185LH RSCAN0.RMDF185.uint8_t[LH]
#define RSCAN0RMDF185H RSCAN0.RMDF185.uint16_t[H]
#define RSCAN0RMDF185HL RSCAN0.RMDF185.uint8_t[HL]
#define RSCAN0RMDF185HH RSCAN0.RMDF185.uint8_t[HH]
#define RSCAN0RMID86 RSCAN0.RMID86.uint32_t
#define RSCAN0RMID86L RSCAN0.RMID86.uint16_t[L]
#define RSCAN0RMID86LL RSCAN0.RMID86.uint8_t[LL]
#define RSCAN0RMID86LH RSCAN0.RMID86.uint8_t[LH]
#define RSCAN0RMID86H RSCAN0.RMID86.uint16_t[H]
#define RSCAN0RMID86HL RSCAN0.RMID86.uint8_t[HL]
#define RSCAN0RMID86HH RSCAN0.RMID86.uint8_t[HH]
#define RSCAN0RMPTR86 RSCAN0.RMPTR86.uint32_t
#define RSCAN0RMPTR86L RSCAN0.RMPTR86.uint16_t[L]
#define RSCAN0RMPTR86LL RSCAN0.RMPTR86.uint8_t[LL]
#define RSCAN0RMPTR86LH RSCAN0.RMPTR86.uint8_t[LH]
#define RSCAN0RMPTR86H RSCAN0.RMPTR86.uint16_t[H]
#define RSCAN0RMPTR86HL RSCAN0.RMPTR86.uint8_t[HL]
#define RSCAN0RMPTR86HH RSCAN0.RMPTR86.uint8_t[HH]
#define RSCAN0RMDF086 RSCAN0.RMDF086.uint32_t
#define RSCAN0RMDF086L RSCAN0.RMDF086.uint16_t[L]
#define RSCAN0RMDF086LL RSCAN0.RMDF086.uint8_t[LL]
#define RSCAN0RMDF086LH RSCAN0.RMDF086.uint8_t[LH]
#define RSCAN0RMDF086H RSCAN0.RMDF086.uint16_t[H]
#define RSCAN0RMDF086HL RSCAN0.RMDF086.uint8_t[HL]
#define RSCAN0RMDF086HH RSCAN0.RMDF086.uint8_t[HH]
#define RSCAN0RMDF186 RSCAN0.RMDF186.uint32_t
#define RSCAN0RMDF186L RSCAN0.RMDF186.uint16_t[L]
#define RSCAN0RMDF186LL RSCAN0.RMDF186.uint8_t[LL]
#define RSCAN0RMDF186LH RSCAN0.RMDF186.uint8_t[LH]
#define RSCAN0RMDF186H RSCAN0.RMDF186.uint16_t[H]
#define RSCAN0RMDF186HL RSCAN0.RMDF186.uint8_t[HL]
#define RSCAN0RMDF186HH RSCAN0.RMDF186.uint8_t[HH]
#define RSCAN0RMID87 RSCAN0.RMID87.uint32_t
#define RSCAN0RMID87L RSCAN0.RMID87.uint16_t[L]
#define RSCAN0RMID87LL RSCAN0.RMID87.uint8_t[LL]
#define RSCAN0RMID87LH RSCAN0.RMID87.uint8_t[LH]
#define RSCAN0RMID87H RSCAN0.RMID87.uint16_t[H]
#define RSCAN0RMID87HL RSCAN0.RMID87.uint8_t[HL]
#define RSCAN0RMID87HH RSCAN0.RMID87.uint8_t[HH]
#define RSCAN0RMPTR87 RSCAN0.RMPTR87.uint32_t
#define RSCAN0RMPTR87L RSCAN0.RMPTR87.uint16_t[L]
#define RSCAN0RMPTR87LL RSCAN0.RMPTR87.uint8_t[LL]
#define RSCAN0RMPTR87LH RSCAN0.RMPTR87.uint8_t[LH]
#define RSCAN0RMPTR87H RSCAN0.RMPTR87.uint16_t[H]
#define RSCAN0RMPTR87HL RSCAN0.RMPTR87.uint8_t[HL]
#define RSCAN0RMPTR87HH RSCAN0.RMPTR87.uint8_t[HH]
#define RSCAN0RMDF087 RSCAN0.RMDF087.uint32_t
#define RSCAN0RMDF087L RSCAN0.RMDF087.uint16_t[L]
#define RSCAN0RMDF087LL RSCAN0.RMDF087.uint8_t[LL]
#define RSCAN0RMDF087LH RSCAN0.RMDF087.uint8_t[LH]
#define RSCAN0RMDF087H RSCAN0.RMDF087.uint16_t[H]
#define RSCAN0RMDF087HL RSCAN0.RMDF087.uint8_t[HL]
#define RSCAN0RMDF087HH RSCAN0.RMDF087.uint8_t[HH]
#define RSCAN0RMDF187 RSCAN0.RMDF187.uint32_t
#define RSCAN0RMDF187L RSCAN0.RMDF187.uint16_t[L]
#define RSCAN0RMDF187LL RSCAN0.RMDF187.uint8_t[LL]
#define RSCAN0RMDF187LH RSCAN0.RMDF187.uint8_t[LH]
#define RSCAN0RMDF187H RSCAN0.RMDF187.uint16_t[H]
#define RSCAN0RMDF187HL RSCAN0.RMDF187.uint8_t[HL]
#define RSCAN0RMDF187HH RSCAN0.RMDF187.uint8_t[HH]
#define RSCAN0RMID88 RSCAN0.RMID88.uint32_t
#define RSCAN0RMID88L RSCAN0.RMID88.uint16_t[L]
#define RSCAN0RMID88LL RSCAN0.RMID88.uint8_t[LL]
#define RSCAN0RMID88LH RSCAN0.RMID88.uint8_t[LH]
#define RSCAN0RMID88H RSCAN0.RMID88.uint16_t[H]
#define RSCAN0RMID88HL RSCAN0.RMID88.uint8_t[HL]
#define RSCAN0RMID88HH RSCAN0.RMID88.uint8_t[HH]
#define RSCAN0RMPTR88 RSCAN0.RMPTR88.uint32_t
#define RSCAN0RMPTR88L RSCAN0.RMPTR88.uint16_t[L]
#define RSCAN0RMPTR88LL RSCAN0.RMPTR88.uint8_t[LL]
#define RSCAN0RMPTR88LH RSCAN0.RMPTR88.uint8_t[LH]
#define RSCAN0RMPTR88H RSCAN0.RMPTR88.uint16_t[H]
#define RSCAN0RMPTR88HL RSCAN0.RMPTR88.uint8_t[HL]
#define RSCAN0RMPTR88HH RSCAN0.RMPTR88.uint8_t[HH]
#define RSCAN0RMDF088 RSCAN0.RMDF088.uint32_t
#define RSCAN0RMDF088L RSCAN0.RMDF088.uint16_t[L]
#define RSCAN0RMDF088LL RSCAN0.RMDF088.uint8_t[LL]
#define RSCAN0RMDF088LH RSCAN0.RMDF088.uint8_t[LH]
#define RSCAN0RMDF088H RSCAN0.RMDF088.uint16_t[H]
#define RSCAN0RMDF088HL RSCAN0.RMDF088.uint8_t[HL]
#define RSCAN0RMDF088HH RSCAN0.RMDF088.uint8_t[HH]
#define RSCAN0RMDF188 RSCAN0.RMDF188.uint32_t
#define RSCAN0RMDF188L RSCAN0.RMDF188.uint16_t[L]
#define RSCAN0RMDF188LL RSCAN0.RMDF188.uint8_t[LL]
#define RSCAN0RMDF188LH RSCAN0.RMDF188.uint8_t[LH]
#define RSCAN0RMDF188H RSCAN0.RMDF188.uint16_t[H]
#define RSCAN0RMDF188HL RSCAN0.RMDF188.uint8_t[HL]
#define RSCAN0RMDF188HH RSCAN0.RMDF188.uint8_t[HH]
#define RSCAN0RMID89 RSCAN0.RMID89.uint32_t
#define RSCAN0RMID89L RSCAN0.RMID89.uint16_t[L]
#define RSCAN0RMID89LL RSCAN0.RMID89.uint8_t[LL]
#define RSCAN0RMID89LH RSCAN0.RMID89.uint8_t[LH]
#define RSCAN0RMID89H RSCAN0.RMID89.uint16_t[H]
#define RSCAN0RMID89HL RSCAN0.RMID89.uint8_t[HL]
#define RSCAN0RMID89HH RSCAN0.RMID89.uint8_t[HH]
#define RSCAN0RMPTR89 RSCAN0.RMPTR89.uint32_t
#define RSCAN0RMPTR89L RSCAN0.RMPTR89.uint16_t[L]
#define RSCAN0RMPTR89LL RSCAN0.RMPTR89.uint8_t[LL]
#define RSCAN0RMPTR89LH RSCAN0.RMPTR89.uint8_t[LH]
#define RSCAN0RMPTR89H RSCAN0.RMPTR89.uint16_t[H]
#define RSCAN0RMPTR89HL RSCAN0.RMPTR89.uint8_t[HL]
#define RSCAN0RMPTR89HH RSCAN0.RMPTR89.uint8_t[HH]
#define RSCAN0RMDF089 RSCAN0.RMDF089.uint32_t
#define RSCAN0RMDF089L RSCAN0.RMDF089.uint16_t[L]
#define RSCAN0RMDF089LL RSCAN0.RMDF089.uint8_t[LL]
#define RSCAN0RMDF089LH RSCAN0.RMDF089.uint8_t[LH]
#define RSCAN0RMDF089H RSCAN0.RMDF089.uint16_t[H]
#define RSCAN0RMDF089HL RSCAN0.RMDF089.uint8_t[HL]
#define RSCAN0RMDF089HH RSCAN0.RMDF089.uint8_t[HH]
#define RSCAN0RMDF189 RSCAN0.RMDF189.uint32_t
#define RSCAN0RMDF189L RSCAN0.RMDF189.uint16_t[L]
#define RSCAN0RMDF189LL RSCAN0.RMDF189.uint8_t[LL]
#define RSCAN0RMDF189LH RSCAN0.RMDF189.uint8_t[LH]
#define RSCAN0RMDF189H RSCAN0.RMDF189.uint16_t[H]
#define RSCAN0RMDF189HL RSCAN0.RMDF189.uint8_t[HL]
#define RSCAN0RMDF189HH RSCAN0.RMDF189.uint8_t[HH]
#define RSCAN0RMID90 RSCAN0.RMID90.uint32_t
#define RSCAN0RMID90L RSCAN0.RMID90.uint16_t[L]
#define RSCAN0RMID90LL RSCAN0.RMID90.uint8_t[LL]
#define RSCAN0RMID90LH RSCAN0.RMID90.uint8_t[LH]
#define RSCAN0RMID90H RSCAN0.RMID90.uint16_t[H]
#define RSCAN0RMID90HL RSCAN0.RMID90.uint8_t[HL]
#define RSCAN0RMID90HH RSCAN0.RMID90.uint8_t[HH]
#define RSCAN0RMPTR90 RSCAN0.RMPTR90.uint32_t
#define RSCAN0RMPTR90L RSCAN0.RMPTR90.uint16_t[L]
#define RSCAN0RMPTR90LL RSCAN0.RMPTR90.uint8_t[LL]
#define RSCAN0RMPTR90LH RSCAN0.RMPTR90.uint8_t[LH]
#define RSCAN0RMPTR90H RSCAN0.RMPTR90.uint16_t[H]
#define RSCAN0RMPTR90HL RSCAN0.RMPTR90.uint8_t[HL]
#define RSCAN0RMPTR90HH RSCAN0.RMPTR90.uint8_t[HH]
#define RSCAN0RMDF090 RSCAN0.RMDF090.uint32_t
#define RSCAN0RMDF090L RSCAN0.RMDF090.uint16_t[L]
#define RSCAN0RMDF090LL RSCAN0.RMDF090.uint8_t[LL]
#define RSCAN0RMDF090LH RSCAN0.RMDF090.uint8_t[LH]
#define RSCAN0RMDF090H RSCAN0.RMDF090.uint16_t[H]
#define RSCAN0RMDF090HL RSCAN0.RMDF090.uint8_t[HL]
#define RSCAN0RMDF090HH RSCAN0.RMDF090.uint8_t[HH]
#define RSCAN0RMDF190 RSCAN0.RMDF190.uint32_t
#define RSCAN0RMDF190L RSCAN0.RMDF190.uint16_t[L]
#define RSCAN0RMDF190LL RSCAN0.RMDF190.uint8_t[LL]
#define RSCAN0RMDF190LH RSCAN0.RMDF190.uint8_t[LH]
#define RSCAN0RMDF190H RSCAN0.RMDF190.uint16_t[H]
#define RSCAN0RMDF190HL RSCAN0.RMDF190.uint8_t[HL]
#define RSCAN0RMDF190HH RSCAN0.RMDF190.uint8_t[HH]
#define RSCAN0RMID91 RSCAN0.RMID91.uint32_t
#define RSCAN0RMID91L RSCAN0.RMID91.uint16_t[L]
#define RSCAN0RMID91LL RSCAN0.RMID91.uint8_t[LL]
#define RSCAN0RMID91LH RSCAN0.RMID91.uint8_t[LH]
#define RSCAN0RMID91H RSCAN0.RMID91.uint16_t[H]
#define RSCAN0RMID91HL RSCAN0.RMID91.uint8_t[HL]
#define RSCAN0RMID91HH RSCAN0.RMID91.uint8_t[HH]
#define RSCAN0RMPTR91 RSCAN0.RMPTR91.uint32_t
#define RSCAN0RMPTR91L RSCAN0.RMPTR91.uint16_t[L]
#define RSCAN0RMPTR91LL RSCAN0.RMPTR91.uint8_t[LL]
#define RSCAN0RMPTR91LH RSCAN0.RMPTR91.uint8_t[LH]
#define RSCAN0RMPTR91H RSCAN0.RMPTR91.uint16_t[H]
#define RSCAN0RMPTR91HL RSCAN0.RMPTR91.uint8_t[HL]
#define RSCAN0RMPTR91HH RSCAN0.RMPTR91.uint8_t[HH]
#define RSCAN0RMDF091 RSCAN0.RMDF091.uint32_t
#define RSCAN0RMDF091L RSCAN0.RMDF091.uint16_t[L]
#define RSCAN0RMDF091LL RSCAN0.RMDF091.uint8_t[LL]
#define RSCAN0RMDF091LH RSCAN0.RMDF091.uint8_t[LH]
#define RSCAN0RMDF091H RSCAN0.RMDF091.uint16_t[H]
#define RSCAN0RMDF091HL RSCAN0.RMDF091.uint8_t[HL]
#define RSCAN0RMDF091HH RSCAN0.RMDF091.uint8_t[HH]
#define RSCAN0RMDF191 RSCAN0.RMDF191.uint32_t
#define RSCAN0RMDF191L RSCAN0.RMDF191.uint16_t[L]
#define RSCAN0RMDF191LL RSCAN0.RMDF191.uint8_t[LL]
#define RSCAN0RMDF191LH RSCAN0.RMDF191.uint8_t[LH]
#define RSCAN0RMDF191H RSCAN0.RMDF191.uint16_t[H]
#define RSCAN0RMDF191HL RSCAN0.RMDF191.uint8_t[HL]
#define RSCAN0RMDF191HH RSCAN0.RMDF191.uint8_t[HH]
#define RSCAN0RMID92 RSCAN0.RMID92.uint32_t
#define RSCAN0RMID92L RSCAN0.RMID92.uint16_t[L]
#define RSCAN0RMID92LL RSCAN0.RMID92.uint8_t[LL]
#define RSCAN0RMID92LH RSCAN0.RMID92.uint8_t[LH]
#define RSCAN0RMID92H RSCAN0.RMID92.uint16_t[H]
#define RSCAN0RMID92HL RSCAN0.RMID92.uint8_t[HL]
#define RSCAN0RMID92HH RSCAN0.RMID92.uint8_t[HH]
#define RSCAN0RMPTR92 RSCAN0.RMPTR92.uint32_t
#define RSCAN0RMPTR92L RSCAN0.RMPTR92.uint16_t[L]
#define RSCAN0RMPTR92LL RSCAN0.RMPTR92.uint8_t[LL]
#define RSCAN0RMPTR92LH RSCAN0.RMPTR92.uint8_t[LH]
#define RSCAN0RMPTR92H RSCAN0.RMPTR92.uint16_t[H]
#define RSCAN0RMPTR92HL RSCAN0.RMPTR92.uint8_t[HL]
#define RSCAN0RMPTR92HH RSCAN0.RMPTR92.uint8_t[HH]
#define RSCAN0RMDF092 RSCAN0.RMDF092.uint32_t
#define RSCAN0RMDF092L RSCAN0.RMDF092.uint16_t[L]
#define RSCAN0RMDF092LL RSCAN0.RMDF092.uint8_t[LL]
#define RSCAN0RMDF092LH RSCAN0.RMDF092.uint8_t[LH]
#define RSCAN0RMDF092H RSCAN0.RMDF092.uint16_t[H]
#define RSCAN0RMDF092HL RSCAN0.RMDF092.uint8_t[HL]
#define RSCAN0RMDF092HH RSCAN0.RMDF092.uint8_t[HH]
#define RSCAN0RMDF192 RSCAN0.RMDF192.uint32_t
#define RSCAN0RMDF192L RSCAN0.RMDF192.uint16_t[L]
#define RSCAN0RMDF192LL RSCAN0.RMDF192.uint8_t[LL]
#define RSCAN0RMDF192LH RSCAN0.RMDF192.uint8_t[LH]
#define RSCAN0RMDF192H RSCAN0.RMDF192.uint16_t[H]
#define RSCAN0RMDF192HL RSCAN0.RMDF192.uint8_t[HL]
#define RSCAN0RMDF192HH RSCAN0.RMDF192.uint8_t[HH]
#define RSCAN0RMID93 RSCAN0.RMID93.uint32_t
#define RSCAN0RMID93L RSCAN0.RMID93.uint16_t[L]
#define RSCAN0RMID93LL RSCAN0.RMID93.uint8_t[LL]
#define RSCAN0RMID93LH RSCAN0.RMID93.uint8_t[LH]
#define RSCAN0RMID93H RSCAN0.RMID93.uint16_t[H]
#define RSCAN0RMID93HL RSCAN0.RMID93.uint8_t[HL]
#define RSCAN0RMID93HH RSCAN0.RMID93.uint8_t[HH]
#define RSCAN0RMPTR93 RSCAN0.RMPTR93.uint32_t
#define RSCAN0RMPTR93L RSCAN0.RMPTR93.uint16_t[L]
#define RSCAN0RMPTR93LL RSCAN0.RMPTR93.uint8_t[LL]
#define RSCAN0RMPTR93LH RSCAN0.RMPTR93.uint8_t[LH]
#define RSCAN0RMPTR93H RSCAN0.RMPTR93.uint16_t[H]
#define RSCAN0RMPTR93HL RSCAN0.RMPTR93.uint8_t[HL]
#define RSCAN0RMPTR93HH RSCAN0.RMPTR93.uint8_t[HH]
#define RSCAN0RMDF093 RSCAN0.RMDF093.uint32_t
#define RSCAN0RMDF093L RSCAN0.RMDF093.uint16_t[L]
#define RSCAN0RMDF093LL RSCAN0.RMDF093.uint8_t[LL]
#define RSCAN0RMDF093LH RSCAN0.RMDF093.uint8_t[LH]
#define RSCAN0RMDF093H RSCAN0.RMDF093.uint16_t[H]
#define RSCAN0RMDF093HL RSCAN0.RMDF093.uint8_t[HL]
#define RSCAN0RMDF093HH RSCAN0.RMDF093.uint8_t[HH]
#define RSCAN0RMDF193 RSCAN0.RMDF193.uint32_t
#define RSCAN0RMDF193L RSCAN0.RMDF193.uint16_t[L]
#define RSCAN0RMDF193LL RSCAN0.RMDF193.uint8_t[LL]
#define RSCAN0RMDF193LH RSCAN0.RMDF193.uint8_t[LH]
#define RSCAN0RMDF193H RSCAN0.RMDF193.uint16_t[H]
#define RSCAN0RMDF193HL RSCAN0.RMDF193.uint8_t[HL]
#define RSCAN0RMDF193HH RSCAN0.RMDF193.uint8_t[HH]
#define RSCAN0RMID94 RSCAN0.RMID94.uint32_t
#define RSCAN0RMID94L RSCAN0.RMID94.uint16_t[L]
#define RSCAN0RMID94LL RSCAN0.RMID94.uint8_t[LL]
#define RSCAN0RMID94LH RSCAN0.RMID94.uint8_t[LH]
#define RSCAN0RMID94H RSCAN0.RMID94.uint16_t[H]
#define RSCAN0RMID94HL RSCAN0.RMID94.uint8_t[HL]
#define RSCAN0RMID94HH RSCAN0.RMID94.uint8_t[HH]
#define RSCAN0RMPTR94 RSCAN0.RMPTR94.uint32_t
#define RSCAN0RMPTR94L RSCAN0.RMPTR94.uint16_t[L]
#define RSCAN0RMPTR94LL RSCAN0.RMPTR94.uint8_t[LL]
#define RSCAN0RMPTR94LH RSCAN0.RMPTR94.uint8_t[LH]
#define RSCAN0RMPTR94H RSCAN0.RMPTR94.uint16_t[H]
#define RSCAN0RMPTR94HL RSCAN0.RMPTR94.uint8_t[HL]
#define RSCAN0RMPTR94HH RSCAN0.RMPTR94.uint8_t[HH]
#define RSCAN0RMDF094 RSCAN0.RMDF094.uint32_t
#define RSCAN0RMDF094L RSCAN0.RMDF094.uint16_t[L]
#define RSCAN0RMDF094LL RSCAN0.RMDF094.uint8_t[LL]
#define RSCAN0RMDF094LH RSCAN0.RMDF094.uint8_t[LH]
#define RSCAN0RMDF094H RSCAN0.RMDF094.uint16_t[H]
#define RSCAN0RMDF094HL RSCAN0.RMDF094.uint8_t[HL]
#define RSCAN0RMDF094HH RSCAN0.RMDF094.uint8_t[HH]
#define RSCAN0RMDF194 RSCAN0.RMDF194.uint32_t
#define RSCAN0RMDF194L RSCAN0.RMDF194.uint16_t[L]
#define RSCAN0RMDF194LL RSCAN0.RMDF194.uint8_t[LL]
#define RSCAN0RMDF194LH RSCAN0.RMDF194.uint8_t[LH]
#define RSCAN0RMDF194H RSCAN0.RMDF194.uint16_t[H]
#define RSCAN0RMDF194HL RSCAN0.RMDF194.uint8_t[HL]
#define RSCAN0RMDF194HH RSCAN0.RMDF194.uint8_t[HH]
#define RSCAN0RMID95 RSCAN0.RMID95.uint32_t
#define RSCAN0RMID95L RSCAN0.RMID95.uint16_t[L]
#define RSCAN0RMID95LL RSCAN0.RMID95.uint8_t[LL]
#define RSCAN0RMID95LH RSCAN0.RMID95.uint8_t[LH]
#define RSCAN0RMID95H RSCAN0.RMID95.uint16_t[H]
#define RSCAN0RMID95HL RSCAN0.RMID95.uint8_t[HL]
#define RSCAN0RMID95HH RSCAN0.RMID95.uint8_t[HH]
#define RSCAN0RMPTR95 RSCAN0.RMPTR95.uint32_t
#define RSCAN0RMPTR95L RSCAN0.RMPTR95.uint16_t[L]
#define RSCAN0RMPTR95LL RSCAN0.RMPTR95.uint8_t[LL]
#define RSCAN0RMPTR95LH RSCAN0.RMPTR95.uint8_t[LH]
#define RSCAN0RMPTR95H RSCAN0.RMPTR95.uint16_t[H]
#define RSCAN0RMPTR95HL RSCAN0.RMPTR95.uint8_t[HL]
#define RSCAN0RMPTR95HH RSCAN0.RMPTR95.uint8_t[HH]
#define RSCAN0RMDF095 RSCAN0.RMDF095.uint32_t
#define RSCAN0RMDF095L RSCAN0.RMDF095.uint16_t[L]
#define RSCAN0RMDF095LL RSCAN0.RMDF095.uint8_t[LL]
#define RSCAN0RMDF095LH RSCAN0.RMDF095.uint8_t[LH]
#define RSCAN0RMDF095H RSCAN0.RMDF095.uint16_t[H]
#define RSCAN0RMDF095HL RSCAN0.RMDF095.uint8_t[HL]
#define RSCAN0RMDF095HH RSCAN0.RMDF095.uint8_t[HH]
#define RSCAN0RMDF195 RSCAN0.RMDF195.uint32_t
#define RSCAN0RMDF195L RSCAN0.RMDF195.uint16_t[L]
#define RSCAN0RMDF195LL RSCAN0.RMDF195.uint8_t[LL]
#define RSCAN0RMDF195LH RSCAN0.RMDF195.uint8_t[LH]
#define RSCAN0RMDF195H RSCAN0.RMDF195.uint16_t[H]
#define RSCAN0RMDF195HL RSCAN0.RMDF195.uint8_t[HL]
#define RSCAN0RMDF195HH RSCAN0.RMDF195.uint8_t[HH]
#define RSCAN0RFID0 RSCAN0.RFID0.uint32_t
#define RSCAN0RFID0L RSCAN0.RFID0.uint16_t[L]
#define RSCAN0RFID0LL RSCAN0.RFID0.uint8_t[LL]
#define RSCAN0RFID0LH RSCAN0.RFID0.uint8_t[LH]
#define RSCAN0RFID0H RSCAN0.RFID0.uint16_t[H]
#define RSCAN0RFID0HL RSCAN0.RFID0.uint8_t[HL]
#define RSCAN0RFID0HH RSCAN0.RFID0.uint8_t[HH]
#define RSCAN0RFPTR0 RSCAN0.RFPTR0.uint32_t
#define RSCAN0RFPTR0L RSCAN0.RFPTR0.uint16_t[L]
#define RSCAN0RFPTR0LL RSCAN0.RFPTR0.uint8_t[LL]
#define RSCAN0RFPTR0LH RSCAN0.RFPTR0.uint8_t[LH]
#define RSCAN0RFPTR0H RSCAN0.RFPTR0.uint16_t[H]
#define RSCAN0RFPTR0HL RSCAN0.RFPTR0.uint8_t[HL]
#define RSCAN0RFPTR0HH RSCAN0.RFPTR0.uint8_t[HH]
#define RSCAN0RFDF00 RSCAN0.RFDF00.uint32_t
#define RSCAN0RFDF00L RSCAN0.RFDF00.uint16_t[L]
#define RSCAN0RFDF00LL RSCAN0.RFDF00.uint8_t[LL]
#define RSCAN0RFDF00LH RSCAN0.RFDF00.uint8_t[LH]
#define RSCAN0RFDF00H RSCAN0.RFDF00.uint16_t[H]
#define RSCAN0RFDF00HL RSCAN0.RFDF00.uint8_t[HL]
#define RSCAN0RFDF00HH RSCAN0.RFDF00.uint8_t[HH]
#define RSCAN0RFDF10 RSCAN0.RFDF10.uint32_t
#define RSCAN0RFDF10L RSCAN0.RFDF10.uint16_t[L]
#define RSCAN0RFDF10LL RSCAN0.RFDF10.uint8_t[LL]
#define RSCAN0RFDF10LH RSCAN0.RFDF10.uint8_t[LH]
#define RSCAN0RFDF10H RSCAN0.RFDF10.uint16_t[H]
#define RSCAN0RFDF10HL RSCAN0.RFDF10.uint8_t[HL]
#define RSCAN0RFDF10HH RSCAN0.RFDF10.uint8_t[HH]
#define RSCAN0RFID1 RSCAN0.RFID1.uint32_t
#define RSCAN0RFID1L RSCAN0.RFID1.uint16_t[L]
#define RSCAN0RFID1LL RSCAN0.RFID1.uint8_t[LL]
#define RSCAN0RFID1LH RSCAN0.RFID1.uint8_t[LH]
#define RSCAN0RFID1H RSCAN0.RFID1.uint16_t[H]
#define RSCAN0RFID1HL RSCAN0.RFID1.uint8_t[HL]
#define RSCAN0RFID1HH RSCAN0.RFID1.uint8_t[HH]
#define RSCAN0RFPTR1 RSCAN0.RFPTR1.uint32_t
#define RSCAN0RFPTR1L RSCAN0.RFPTR1.uint16_t[L]
#define RSCAN0RFPTR1LL RSCAN0.RFPTR1.uint8_t[LL]
#define RSCAN0RFPTR1LH RSCAN0.RFPTR1.uint8_t[LH]
#define RSCAN0RFPTR1H RSCAN0.RFPTR1.uint16_t[H]
#define RSCAN0RFPTR1HL RSCAN0.RFPTR1.uint8_t[HL]
#define RSCAN0RFPTR1HH RSCAN0.RFPTR1.uint8_t[HH]
#define RSCAN0RFDF01 RSCAN0.RFDF01.uint32_t
#define RSCAN0RFDF01L RSCAN0.RFDF01.uint16_t[L]
#define RSCAN0RFDF01LL RSCAN0.RFDF01.uint8_t[LL]
#define RSCAN0RFDF01LH RSCAN0.RFDF01.uint8_t[LH]
#define RSCAN0RFDF01H RSCAN0.RFDF01.uint16_t[H]
#define RSCAN0RFDF01HL RSCAN0.RFDF01.uint8_t[HL]
#define RSCAN0RFDF01HH RSCAN0.RFDF01.uint8_t[HH]
#define RSCAN0RFDF11 RSCAN0.RFDF11.uint32_t
#define RSCAN0RFDF11L RSCAN0.RFDF11.uint16_t[L]
#define RSCAN0RFDF11LL RSCAN0.RFDF11.uint8_t[LL]
#define RSCAN0RFDF11LH RSCAN0.RFDF11.uint8_t[LH]
#define RSCAN0RFDF11H RSCAN0.RFDF11.uint16_t[H]
#define RSCAN0RFDF11HL RSCAN0.RFDF11.uint8_t[HL]
#define RSCAN0RFDF11HH RSCAN0.RFDF11.uint8_t[HH]
#define RSCAN0RFID2 RSCAN0.RFID2.uint32_t
#define RSCAN0RFID2L RSCAN0.RFID2.uint16_t[L]
#define RSCAN0RFID2LL RSCAN0.RFID2.uint8_t[LL]
#define RSCAN0RFID2LH RSCAN0.RFID2.uint8_t[LH]
#define RSCAN0RFID2H RSCAN0.RFID2.uint16_t[H]
#define RSCAN0RFID2HL RSCAN0.RFID2.uint8_t[HL]
#define RSCAN0RFID2HH RSCAN0.RFID2.uint8_t[HH]
#define RSCAN0RFPTR2 RSCAN0.RFPTR2.uint32_t
#define RSCAN0RFPTR2L RSCAN0.RFPTR2.uint16_t[L]
#define RSCAN0RFPTR2LL RSCAN0.RFPTR2.uint8_t[LL]
#define RSCAN0RFPTR2LH RSCAN0.RFPTR2.uint8_t[LH]
#define RSCAN0RFPTR2H RSCAN0.RFPTR2.uint16_t[H]
#define RSCAN0RFPTR2HL RSCAN0.RFPTR2.uint8_t[HL]
#define RSCAN0RFPTR2HH RSCAN0.RFPTR2.uint8_t[HH]
#define RSCAN0RFDF02 RSCAN0.RFDF02.uint32_t
#define RSCAN0RFDF02L RSCAN0.RFDF02.uint16_t[L]
#define RSCAN0RFDF02LL RSCAN0.RFDF02.uint8_t[LL]
#define RSCAN0RFDF02LH RSCAN0.RFDF02.uint8_t[LH]
#define RSCAN0RFDF02H RSCAN0.RFDF02.uint16_t[H]
#define RSCAN0RFDF02HL RSCAN0.RFDF02.uint8_t[HL]
#define RSCAN0RFDF02HH RSCAN0.RFDF02.uint8_t[HH]
#define RSCAN0RFDF12 RSCAN0.RFDF12.uint32_t
#define RSCAN0RFDF12L RSCAN0.RFDF12.uint16_t[L]
#define RSCAN0RFDF12LL RSCAN0.RFDF12.uint8_t[LL]
#define RSCAN0RFDF12LH RSCAN0.RFDF12.uint8_t[LH]
#define RSCAN0RFDF12H RSCAN0.RFDF12.uint16_t[H]
#define RSCAN0RFDF12HL RSCAN0.RFDF12.uint8_t[HL]
#define RSCAN0RFDF12HH RSCAN0.RFDF12.uint8_t[HH]
#define RSCAN0RFID3 RSCAN0.RFID3.uint32_t
#define RSCAN0RFID3L RSCAN0.RFID3.uint16_t[L]
#define RSCAN0RFID3LL RSCAN0.RFID3.uint8_t[LL]
#define RSCAN0RFID3LH RSCAN0.RFID3.uint8_t[LH]
#define RSCAN0RFID3H RSCAN0.RFID3.uint16_t[H]
#define RSCAN0RFID3HL RSCAN0.RFID3.uint8_t[HL]
#define RSCAN0RFID3HH RSCAN0.RFID3.uint8_t[HH]
#define RSCAN0RFPTR3 RSCAN0.RFPTR3.uint32_t
#define RSCAN0RFPTR3L RSCAN0.RFPTR3.uint16_t[L]
#define RSCAN0RFPTR3LL RSCAN0.RFPTR3.uint8_t[LL]
#define RSCAN0RFPTR3LH RSCAN0.RFPTR3.uint8_t[LH]
#define RSCAN0RFPTR3H RSCAN0.RFPTR3.uint16_t[H]
#define RSCAN0RFPTR3HL RSCAN0.RFPTR3.uint8_t[HL]
#define RSCAN0RFPTR3HH RSCAN0.RFPTR3.uint8_t[HH]
#define RSCAN0RFDF03 RSCAN0.RFDF03.uint32_t
#define RSCAN0RFDF03L RSCAN0.RFDF03.uint16_t[L]
#define RSCAN0RFDF03LL RSCAN0.RFDF03.uint8_t[LL]
#define RSCAN0RFDF03LH RSCAN0.RFDF03.uint8_t[LH]
#define RSCAN0RFDF03H RSCAN0.RFDF03.uint16_t[H]
#define RSCAN0RFDF03HL RSCAN0.RFDF03.uint8_t[HL]
#define RSCAN0RFDF03HH RSCAN0.RFDF03.uint8_t[HH]
#define RSCAN0RFDF13 RSCAN0.RFDF13.uint32_t
#define RSCAN0RFDF13L RSCAN0.RFDF13.uint16_t[L]
#define RSCAN0RFDF13LL RSCAN0.RFDF13.uint8_t[LL]
#define RSCAN0RFDF13LH RSCAN0.RFDF13.uint8_t[LH]
#define RSCAN0RFDF13H RSCAN0.RFDF13.uint16_t[H]
#define RSCAN0RFDF13HL RSCAN0.RFDF13.uint8_t[HL]
#define RSCAN0RFDF13HH RSCAN0.RFDF13.uint8_t[HH]
#define RSCAN0RFID4 RSCAN0.RFID4.uint32_t
#define RSCAN0RFID4L RSCAN0.RFID4.uint16_t[L]
#define RSCAN0RFID4LL RSCAN0.RFID4.uint8_t[LL]
#define RSCAN0RFID4LH RSCAN0.RFID4.uint8_t[LH]
#define RSCAN0RFID4H RSCAN0.RFID4.uint16_t[H]
#define RSCAN0RFID4HL RSCAN0.RFID4.uint8_t[HL]
#define RSCAN0RFID4HH RSCAN0.RFID4.uint8_t[HH]
#define RSCAN0RFPTR4 RSCAN0.RFPTR4.uint32_t
#define RSCAN0RFPTR4L RSCAN0.RFPTR4.uint16_t[L]
#define RSCAN0RFPTR4LL RSCAN0.RFPTR4.uint8_t[LL]
#define RSCAN0RFPTR4LH RSCAN0.RFPTR4.uint8_t[LH]
#define RSCAN0RFPTR4H RSCAN0.RFPTR4.uint16_t[H]
#define RSCAN0RFPTR4HL RSCAN0.RFPTR4.uint8_t[HL]
#define RSCAN0RFPTR4HH RSCAN0.RFPTR4.uint8_t[HH]
#define RSCAN0RFDF04 RSCAN0.RFDF04.uint32_t
#define RSCAN0RFDF04L RSCAN0.RFDF04.uint16_t[L]
#define RSCAN0RFDF04LL RSCAN0.RFDF04.uint8_t[LL]
#define RSCAN0RFDF04LH RSCAN0.RFDF04.uint8_t[LH]
#define RSCAN0RFDF04H RSCAN0.RFDF04.uint16_t[H]
#define RSCAN0RFDF04HL RSCAN0.RFDF04.uint8_t[HL]
#define RSCAN0RFDF04HH RSCAN0.RFDF04.uint8_t[HH]
#define RSCAN0RFDF14 RSCAN0.RFDF14.uint32_t
#define RSCAN0RFDF14L RSCAN0.RFDF14.uint16_t[L]
#define RSCAN0RFDF14LL RSCAN0.RFDF14.uint8_t[LL]
#define RSCAN0RFDF14LH RSCAN0.RFDF14.uint8_t[LH]
#define RSCAN0RFDF14H RSCAN0.RFDF14.uint16_t[H]
#define RSCAN0RFDF14HL RSCAN0.RFDF14.uint8_t[HL]
#define RSCAN0RFDF14HH RSCAN0.RFDF14.uint8_t[HH]
#define RSCAN0RFID5 RSCAN0.RFID5.uint32_t
#define RSCAN0RFID5L RSCAN0.RFID5.uint16_t[L]
#define RSCAN0RFID5LL RSCAN0.RFID5.uint8_t[LL]
#define RSCAN0RFID5LH RSCAN0.RFID5.uint8_t[LH]
#define RSCAN0RFID5H RSCAN0.RFID5.uint16_t[H]
#define RSCAN0RFID5HL RSCAN0.RFID5.uint8_t[HL]
#define RSCAN0RFID5HH RSCAN0.RFID5.uint8_t[HH]
#define RSCAN0RFPTR5 RSCAN0.RFPTR5.uint32_t
#define RSCAN0RFPTR5L RSCAN0.RFPTR5.uint16_t[L]
#define RSCAN0RFPTR5LL RSCAN0.RFPTR5.uint8_t[LL]
#define RSCAN0RFPTR5LH RSCAN0.RFPTR5.uint8_t[LH]
#define RSCAN0RFPTR5H RSCAN0.RFPTR5.uint16_t[H]
#define RSCAN0RFPTR5HL RSCAN0.RFPTR5.uint8_t[HL]
#define RSCAN0RFPTR5HH RSCAN0.RFPTR5.uint8_t[HH]
#define RSCAN0RFDF05 RSCAN0.RFDF05.uint32_t
#define RSCAN0RFDF05L RSCAN0.RFDF05.uint16_t[L]
#define RSCAN0RFDF05LL RSCAN0.RFDF05.uint8_t[LL]
#define RSCAN0RFDF05LH RSCAN0.RFDF05.uint8_t[LH]
#define RSCAN0RFDF05H RSCAN0.RFDF05.uint16_t[H]
#define RSCAN0RFDF05HL RSCAN0.RFDF05.uint8_t[HL]
#define RSCAN0RFDF05HH RSCAN0.RFDF05.uint8_t[HH]
#define RSCAN0RFDF15 RSCAN0.RFDF15.uint32_t
#define RSCAN0RFDF15L RSCAN0.RFDF15.uint16_t[L]
#define RSCAN0RFDF15LL RSCAN0.RFDF15.uint8_t[LL]
#define RSCAN0RFDF15LH RSCAN0.RFDF15.uint8_t[LH]
#define RSCAN0RFDF15H RSCAN0.RFDF15.uint16_t[H]
#define RSCAN0RFDF15HL RSCAN0.RFDF15.uint8_t[HL]
#define RSCAN0RFDF15HH RSCAN0.RFDF15.uint8_t[HH]
#define RSCAN0RFID6 RSCAN0.RFID6.uint32_t
#define RSCAN0RFID6L RSCAN0.RFID6.uint16_t[L]
#define RSCAN0RFID6LL RSCAN0.RFID6.uint8_t[LL]
#define RSCAN0RFID6LH RSCAN0.RFID6.uint8_t[LH]
#define RSCAN0RFID6H RSCAN0.RFID6.uint16_t[H]
#define RSCAN0RFID6HL RSCAN0.RFID6.uint8_t[HL]
#define RSCAN0RFID6HH RSCAN0.RFID6.uint8_t[HH]
#define RSCAN0RFPTR6 RSCAN0.RFPTR6.uint32_t
#define RSCAN0RFPTR6L RSCAN0.RFPTR6.uint16_t[L]
#define RSCAN0RFPTR6LL RSCAN0.RFPTR6.uint8_t[LL]
#define RSCAN0RFPTR6LH RSCAN0.RFPTR6.uint8_t[LH]
#define RSCAN0RFPTR6H RSCAN0.RFPTR6.uint16_t[H]
#define RSCAN0RFPTR6HL RSCAN0.RFPTR6.uint8_t[HL]
#define RSCAN0RFPTR6HH RSCAN0.RFPTR6.uint8_t[HH]
#define RSCAN0RFDF06 RSCAN0.RFDF06.uint32_t
#define RSCAN0RFDF06L RSCAN0.RFDF06.uint16_t[L]
#define RSCAN0RFDF06LL RSCAN0.RFDF06.uint8_t[LL]
#define RSCAN0RFDF06LH RSCAN0.RFDF06.uint8_t[LH]
#define RSCAN0RFDF06H RSCAN0.RFDF06.uint16_t[H]
#define RSCAN0RFDF06HL RSCAN0.RFDF06.uint8_t[HL]
#define RSCAN0RFDF06HH RSCAN0.RFDF06.uint8_t[HH]
#define RSCAN0RFDF16 RSCAN0.RFDF16.uint32_t
#define RSCAN0RFDF16L RSCAN0.RFDF16.uint16_t[L]
#define RSCAN0RFDF16LL RSCAN0.RFDF16.uint8_t[LL]
#define RSCAN0RFDF16LH RSCAN0.RFDF16.uint8_t[LH]
#define RSCAN0RFDF16H RSCAN0.RFDF16.uint16_t[H]
#define RSCAN0RFDF16HL RSCAN0.RFDF16.uint8_t[HL]
#define RSCAN0RFDF16HH RSCAN0.RFDF16.uint8_t[HH]
#define RSCAN0RFID7 RSCAN0.RFID7.uint32_t
#define RSCAN0RFID7L RSCAN0.RFID7.uint16_t[L]
#define RSCAN0RFID7LL RSCAN0.RFID7.uint8_t[LL]
#define RSCAN0RFID7LH RSCAN0.RFID7.uint8_t[LH]
#define RSCAN0RFID7H RSCAN0.RFID7.uint16_t[H]
#define RSCAN0RFID7HL RSCAN0.RFID7.uint8_t[HL]
#define RSCAN0RFID7HH RSCAN0.RFID7.uint8_t[HH]
#define RSCAN0RFPTR7 RSCAN0.RFPTR7.uint32_t
#define RSCAN0RFPTR7L RSCAN0.RFPTR7.uint16_t[L]
#define RSCAN0RFPTR7LL RSCAN0.RFPTR7.uint8_t[LL]
#define RSCAN0RFPTR7LH RSCAN0.RFPTR7.uint8_t[LH]
#define RSCAN0RFPTR7H RSCAN0.RFPTR7.uint16_t[H]
#define RSCAN0RFPTR7HL RSCAN0.RFPTR7.uint8_t[HL]
#define RSCAN0RFPTR7HH RSCAN0.RFPTR7.uint8_t[HH]
#define RSCAN0RFDF07 RSCAN0.RFDF07.uint32_t
#define RSCAN0RFDF07L RSCAN0.RFDF07.uint16_t[L]
#define RSCAN0RFDF07LL RSCAN0.RFDF07.uint8_t[LL]
#define RSCAN0RFDF07LH RSCAN0.RFDF07.uint8_t[LH]
#define RSCAN0RFDF07H RSCAN0.RFDF07.uint16_t[H]
#define RSCAN0RFDF07HL RSCAN0.RFDF07.uint8_t[HL]
#define RSCAN0RFDF07HH RSCAN0.RFDF07.uint8_t[HH]
#define RSCAN0RFDF17 RSCAN0.RFDF17.uint32_t
#define RSCAN0RFDF17L RSCAN0.RFDF17.uint16_t[L]
#define RSCAN0RFDF17LL RSCAN0.RFDF17.uint8_t[LL]
#define RSCAN0RFDF17LH RSCAN0.RFDF17.uint8_t[LH]
#define RSCAN0RFDF17H RSCAN0.RFDF17.uint16_t[H]
#define RSCAN0RFDF17HL RSCAN0.RFDF17.uint8_t[HL]
#define RSCAN0RFDF17HH RSCAN0.RFDF17.uint8_t[HH]
#define RSCAN0CFID0 RSCAN0.CFID0.uint32_t
#define RSCAN0CFID0L RSCAN0.CFID0.uint16_t[L]
#define RSCAN0CFID0LL RSCAN0.CFID0.uint8_t[LL]
#define RSCAN0CFID0LH RSCAN0.CFID0.uint8_t[LH]
#define RSCAN0CFID0H RSCAN0.CFID0.uint16_t[H]
#define RSCAN0CFID0HL RSCAN0.CFID0.uint8_t[HL]
#define RSCAN0CFID0HH RSCAN0.CFID0.uint8_t[HH]
#define RSCAN0CFPTR0 RSCAN0.CFPTR0.uint32_t
#define RSCAN0CFPTR0L RSCAN0.CFPTR0.uint16_t[L]
#define RSCAN0CFPTR0LL RSCAN0.CFPTR0.uint8_t[LL]
#define RSCAN0CFPTR0LH RSCAN0.CFPTR0.uint8_t[LH]
#define RSCAN0CFPTR0H RSCAN0.CFPTR0.uint16_t[H]
#define RSCAN0CFPTR0HL RSCAN0.CFPTR0.uint8_t[HL]
#define RSCAN0CFPTR0HH RSCAN0.CFPTR0.uint8_t[HH]
#define RSCAN0CFDF00 RSCAN0.CFDF00.uint32_t
#define RSCAN0CFDF00L RSCAN0.CFDF00.uint16_t[L]
#define RSCAN0CFDF00LL RSCAN0.CFDF00.uint8_t[LL]
#define RSCAN0CFDF00LH RSCAN0.CFDF00.uint8_t[LH]
#define RSCAN0CFDF00H RSCAN0.CFDF00.uint16_t[H]
#define RSCAN0CFDF00HL RSCAN0.CFDF00.uint8_t[HL]
#define RSCAN0CFDF00HH RSCAN0.CFDF00.uint8_t[HH]
#define RSCAN0CFDF10 RSCAN0.CFDF10.uint32_t
#define RSCAN0CFDF10L RSCAN0.CFDF10.uint16_t[L]
#define RSCAN0CFDF10LL RSCAN0.CFDF10.uint8_t[LL]
#define RSCAN0CFDF10LH RSCAN0.CFDF10.uint8_t[LH]
#define RSCAN0CFDF10H RSCAN0.CFDF10.uint16_t[H]
#define RSCAN0CFDF10HL RSCAN0.CFDF10.uint8_t[HL]
#define RSCAN0CFDF10HH RSCAN0.CFDF10.uint8_t[HH]
#define RSCAN0CFID1 RSCAN0.CFID1.uint32_t
#define RSCAN0CFID1L RSCAN0.CFID1.uint16_t[L]
#define RSCAN0CFID1LL RSCAN0.CFID1.uint8_t[LL]
#define RSCAN0CFID1LH RSCAN0.CFID1.uint8_t[LH]
#define RSCAN0CFID1H RSCAN0.CFID1.uint16_t[H]
#define RSCAN0CFID1HL RSCAN0.CFID1.uint8_t[HL]
#define RSCAN0CFID1HH RSCAN0.CFID1.uint8_t[HH]
#define RSCAN0CFPTR1 RSCAN0.CFPTR1.uint32_t
#define RSCAN0CFPTR1L RSCAN0.CFPTR1.uint16_t[L]
#define RSCAN0CFPTR1LL RSCAN0.CFPTR1.uint8_t[LL]
#define RSCAN0CFPTR1LH RSCAN0.CFPTR1.uint8_t[LH]
#define RSCAN0CFPTR1H RSCAN0.CFPTR1.uint16_t[H]
#define RSCAN0CFPTR1HL RSCAN0.CFPTR1.uint8_t[HL]
#define RSCAN0CFPTR1HH RSCAN0.CFPTR1.uint8_t[HH]
#define RSCAN0CFDF01 RSCAN0.CFDF01.uint32_t
#define RSCAN0CFDF01L RSCAN0.CFDF01.uint16_t[L]
#define RSCAN0CFDF01LL RSCAN0.CFDF01.uint8_t[LL]
#define RSCAN0CFDF01LH RSCAN0.CFDF01.uint8_t[LH]
#define RSCAN0CFDF01H RSCAN0.CFDF01.uint16_t[H]
#define RSCAN0CFDF01HL RSCAN0.CFDF01.uint8_t[HL]
#define RSCAN0CFDF01HH RSCAN0.CFDF01.uint8_t[HH]
#define RSCAN0CFDF11 RSCAN0.CFDF11.uint32_t
#define RSCAN0CFDF11L RSCAN0.CFDF11.uint16_t[L]
#define RSCAN0CFDF11LL RSCAN0.CFDF11.uint8_t[LL]
#define RSCAN0CFDF11LH RSCAN0.CFDF11.uint8_t[LH]
#define RSCAN0CFDF11H RSCAN0.CFDF11.uint16_t[H]
#define RSCAN0CFDF11HL RSCAN0.CFDF11.uint8_t[HL]
#define RSCAN0CFDF11HH RSCAN0.CFDF11.uint8_t[HH]
#define RSCAN0CFID2 RSCAN0.CFID2.uint32_t
#define RSCAN0CFID2L RSCAN0.CFID2.uint16_t[L]
#define RSCAN0CFID2LL RSCAN0.CFID2.uint8_t[LL]
#define RSCAN0CFID2LH RSCAN0.CFID2.uint8_t[LH]
#define RSCAN0CFID2H RSCAN0.CFID2.uint16_t[H]
#define RSCAN0CFID2HL RSCAN0.CFID2.uint8_t[HL]
#define RSCAN0CFID2HH RSCAN0.CFID2.uint8_t[HH]
#define RSCAN0CFPTR2 RSCAN0.CFPTR2.uint32_t
#define RSCAN0CFPTR2L RSCAN0.CFPTR2.uint16_t[L]
#define RSCAN0CFPTR2LL RSCAN0.CFPTR2.uint8_t[LL]
#define RSCAN0CFPTR2LH RSCAN0.CFPTR2.uint8_t[LH]
#define RSCAN0CFPTR2H RSCAN0.CFPTR2.uint16_t[H]
#define RSCAN0CFPTR2HL RSCAN0.CFPTR2.uint8_t[HL]
#define RSCAN0CFPTR2HH RSCAN0.CFPTR2.uint8_t[HH]
#define RSCAN0CFDF02 RSCAN0.CFDF02.uint32_t
#define RSCAN0CFDF02L RSCAN0.CFDF02.uint16_t[L]
#define RSCAN0CFDF02LL RSCAN0.CFDF02.uint8_t[LL]
#define RSCAN0CFDF02LH RSCAN0.CFDF02.uint8_t[LH]
#define RSCAN0CFDF02H RSCAN0.CFDF02.uint16_t[H]
#define RSCAN0CFDF02HL RSCAN0.CFDF02.uint8_t[HL]
#define RSCAN0CFDF02HH RSCAN0.CFDF02.uint8_t[HH]
#define RSCAN0CFDF12 RSCAN0.CFDF12.uint32_t
#define RSCAN0CFDF12L RSCAN0.CFDF12.uint16_t[L]
#define RSCAN0CFDF12LL RSCAN0.CFDF12.uint8_t[LL]
#define RSCAN0CFDF12LH RSCAN0.CFDF12.uint8_t[LH]
#define RSCAN0CFDF12H RSCAN0.CFDF12.uint16_t[H]
#define RSCAN0CFDF12HL RSCAN0.CFDF12.uint8_t[HL]
#define RSCAN0CFDF12HH RSCAN0.CFDF12.uint8_t[HH]
#define RSCAN0CFID3 RSCAN0.CFID3.uint32_t
#define RSCAN0CFID3L RSCAN0.CFID3.uint16_t[L]
#define RSCAN0CFID3LL RSCAN0.CFID3.uint8_t[LL]
#define RSCAN0CFID3LH RSCAN0.CFID3.uint8_t[LH]
#define RSCAN0CFID3H RSCAN0.CFID3.uint16_t[H]
#define RSCAN0CFID3HL RSCAN0.CFID3.uint8_t[HL]
#define RSCAN0CFID3HH RSCAN0.CFID3.uint8_t[HH]
#define RSCAN0CFPTR3 RSCAN0.CFPTR3.uint32_t
#define RSCAN0CFPTR3L RSCAN0.CFPTR3.uint16_t[L]
#define RSCAN0CFPTR3LL RSCAN0.CFPTR3.uint8_t[LL]
#define RSCAN0CFPTR3LH RSCAN0.CFPTR3.uint8_t[LH]
#define RSCAN0CFPTR3H RSCAN0.CFPTR3.uint16_t[H]
#define RSCAN0CFPTR3HL RSCAN0.CFPTR3.uint8_t[HL]
#define RSCAN0CFPTR3HH RSCAN0.CFPTR3.uint8_t[HH]
#define RSCAN0CFDF03 RSCAN0.CFDF03.uint32_t
#define RSCAN0CFDF03L RSCAN0.CFDF03.uint16_t[L]
#define RSCAN0CFDF03LL RSCAN0.CFDF03.uint8_t[LL]
#define RSCAN0CFDF03LH RSCAN0.CFDF03.uint8_t[LH]
#define RSCAN0CFDF03H RSCAN0.CFDF03.uint16_t[H]
#define RSCAN0CFDF03HL RSCAN0.CFDF03.uint8_t[HL]
#define RSCAN0CFDF03HH RSCAN0.CFDF03.uint8_t[HH]
#define RSCAN0CFDF13 RSCAN0.CFDF13.uint32_t
#define RSCAN0CFDF13L RSCAN0.CFDF13.uint16_t[L]
#define RSCAN0CFDF13LL RSCAN0.CFDF13.uint8_t[LL]
#define RSCAN0CFDF13LH RSCAN0.CFDF13.uint8_t[LH]
#define RSCAN0CFDF13H RSCAN0.CFDF13.uint16_t[H]
#define RSCAN0CFDF13HL RSCAN0.CFDF13.uint8_t[HL]
#define RSCAN0CFDF13HH RSCAN0.CFDF13.uint8_t[HH]
#define RSCAN0CFID4 RSCAN0.CFID4.uint32_t
#define RSCAN0CFID4L RSCAN0.CFID4.uint16_t[L]
#define RSCAN0CFID4LL RSCAN0.CFID4.uint8_t[LL]
#define RSCAN0CFID4LH RSCAN0.CFID4.uint8_t[LH]
#define RSCAN0CFID4H RSCAN0.CFID4.uint16_t[H]
#define RSCAN0CFID4HL RSCAN0.CFID4.uint8_t[HL]
#define RSCAN0CFID4HH RSCAN0.CFID4.uint8_t[HH]
#define RSCAN0CFPTR4 RSCAN0.CFPTR4.uint32_t
#define RSCAN0CFPTR4L RSCAN0.CFPTR4.uint16_t[L]
#define RSCAN0CFPTR4LL RSCAN0.CFPTR4.uint8_t[LL]
#define RSCAN0CFPTR4LH RSCAN0.CFPTR4.uint8_t[LH]
#define RSCAN0CFPTR4H RSCAN0.CFPTR4.uint16_t[H]
#define RSCAN0CFPTR4HL RSCAN0.CFPTR4.uint8_t[HL]
#define RSCAN0CFPTR4HH RSCAN0.CFPTR4.uint8_t[HH]
#define RSCAN0CFDF04 RSCAN0.CFDF04.uint32_t
#define RSCAN0CFDF04L RSCAN0.CFDF04.uint16_t[L]
#define RSCAN0CFDF04LL RSCAN0.CFDF04.uint8_t[LL]
#define RSCAN0CFDF04LH RSCAN0.CFDF04.uint8_t[LH]
#define RSCAN0CFDF04H RSCAN0.CFDF04.uint16_t[H]
#define RSCAN0CFDF04HL RSCAN0.CFDF04.uint8_t[HL]
#define RSCAN0CFDF04HH RSCAN0.CFDF04.uint8_t[HH]
#define RSCAN0CFDF14 RSCAN0.CFDF14.uint32_t
#define RSCAN0CFDF14L RSCAN0.CFDF14.uint16_t[L]
#define RSCAN0CFDF14LL RSCAN0.CFDF14.uint8_t[LL]
#define RSCAN0CFDF14LH RSCAN0.CFDF14.uint8_t[LH]
#define RSCAN0CFDF14H RSCAN0.CFDF14.uint16_t[H]
#define RSCAN0CFDF14HL RSCAN0.CFDF14.uint8_t[HL]
#define RSCAN0CFDF14HH RSCAN0.CFDF14.uint8_t[HH]
#define RSCAN0CFID5 RSCAN0.CFID5.uint32_t
#define RSCAN0CFID5L RSCAN0.CFID5.uint16_t[L]
#define RSCAN0CFID5LL RSCAN0.CFID5.uint8_t[LL]
#define RSCAN0CFID5LH RSCAN0.CFID5.uint8_t[LH]
#define RSCAN0CFID5H RSCAN0.CFID5.uint16_t[H]
#define RSCAN0CFID5HL RSCAN0.CFID5.uint8_t[HL]
#define RSCAN0CFID5HH RSCAN0.CFID5.uint8_t[HH]
#define RSCAN0CFPTR5 RSCAN0.CFPTR5.uint32_t
#define RSCAN0CFPTR5L RSCAN0.CFPTR5.uint16_t[L]
#define RSCAN0CFPTR5LL RSCAN0.CFPTR5.uint8_t[LL]
#define RSCAN0CFPTR5LH RSCAN0.CFPTR5.uint8_t[LH]
#define RSCAN0CFPTR5H RSCAN0.CFPTR5.uint16_t[H]
#define RSCAN0CFPTR5HL RSCAN0.CFPTR5.uint8_t[HL]
#define RSCAN0CFPTR5HH RSCAN0.CFPTR5.uint8_t[HH]
#define RSCAN0CFDF05 RSCAN0.CFDF05.uint32_t
#define RSCAN0CFDF05L RSCAN0.CFDF05.uint16_t[L]
#define RSCAN0CFDF05LL RSCAN0.CFDF05.uint8_t[LL]
#define RSCAN0CFDF05LH RSCAN0.CFDF05.uint8_t[LH]
#define RSCAN0CFDF05H RSCAN0.CFDF05.uint16_t[H]
#define RSCAN0CFDF05HL RSCAN0.CFDF05.uint8_t[HL]
#define RSCAN0CFDF05HH RSCAN0.CFDF05.uint8_t[HH]
#define RSCAN0CFDF15 RSCAN0.CFDF15.uint32_t
#define RSCAN0CFDF15L RSCAN0.CFDF15.uint16_t[L]
#define RSCAN0CFDF15LL RSCAN0.CFDF15.uint8_t[LL]
#define RSCAN0CFDF15LH RSCAN0.CFDF15.uint8_t[LH]
#define RSCAN0CFDF15H RSCAN0.CFDF15.uint16_t[H]
#define RSCAN0CFDF15HL RSCAN0.CFDF15.uint8_t[HL]
#define RSCAN0CFDF15HH RSCAN0.CFDF15.uint8_t[HH]
#define RSCAN0CFID6 RSCAN0.CFID6.uint32_t
#define RSCAN0CFID6L RSCAN0.CFID6.uint16_t[L]
#define RSCAN0CFID6LL RSCAN0.CFID6.uint8_t[LL]
#define RSCAN0CFID6LH RSCAN0.CFID6.uint8_t[LH]
#define RSCAN0CFID6H RSCAN0.CFID6.uint16_t[H]
#define RSCAN0CFID6HL RSCAN0.CFID6.uint8_t[HL]
#define RSCAN0CFID6HH RSCAN0.CFID6.uint8_t[HH]
#define RSCAN0CFPTR6 RSCAN0.CFPTR6.uint32_t
#define RSCAN0CFPTR6L RSCAN0.CFPTR6.uint16_t[L]
#define RSCAN0CFPTR6LL RSCAN0.CFPTR6.uint8_t[LL]
#define RSCAN0CFPTR6LH RSCAN0.CFPTR6.uint8_t[LH]
#define RSCAN0CFPTR6H RSCAN0.CFPTR6.uint16_t[H]
#define RSCAN0CFPTR6HL RSCAN0.CFPTR6.uint8_t[HL]
#define RSCAN0CFPTR6HH RSCAN0.CFPTR6.uint8_t[HH]
#define RSCAN0CFDF06 RSCAN0.CFDF06.uint32_t
#define RSCAN0CFDF06L RSCAN0.CFDF06.uint16_t[L]
#define RSCAN0CFDF06LL RSCAN0.CFDF06.uint8_t[LL]
#define RSCAN0CFDF06LH RSCAN0.CFDF06.uint8_t[LH]
#define RSCAN0CFDF06H RSCAN0.CFDF06.uint16_t[H]
#define RSCAN0CFDF06HL RSCAN0.CFDF06.uint8_t[HL]
#define RSCAN0CFDF06HH RSCAN0.CFDF06.uint8_t[HH]
#define RSCAN0CFDF16 RSCAN0.CFDF16.uint32_t
#define RSCAN0CFDF16L RSCAN0.CFDF16.uint16_t[L]
#define RSCAN0CFDF16LL RSCAN0.CFDF16.uint8_t[LL]
#define RSCAN0CFDF16LH RSCAN0.CFDF16.uint8_t[LH]
#define RSCAN0CFDF16H RSCAN0.CFDF16.uint16_t[H]
#define RSCAN0CFDF16HL RSCAN0.CFDF16.uint8_t[HL]
#define RSCAN0CFDF16HH RSCAN0.CFDF16.uint8_t[HH]
#define RSCAN0CFID7 RSCAN0.CFID7.uint32_t
#define RSCAN0CFID7L RSCAN0.CFID7.uint16_t[L]
#define RSCAN0CFID7LL RSCAN0.CFID7.uint8_t[LL]
#define RSCAN0CFID7LH RSCAN0.CFID7.uint8_t[LH]
#define RSCAN0CFID7H RSCAN0.CFID7.uint16_t[H]
#define RSCAN0CFID7HL RSCAN0.CFID7.uint8_t[HL]
#define RSCAN0CFID7HH RSCAN0.CFID7.uint8_t[HH]
#define RSCAN0CFPTR7 RSCAN0.CFPTR7.uint32_t
#define RSCAN0CFPTR7L RSCAN0.CFPTR7.uint16_t[L]
#define RSCAN0CFPTR7LL RSCAN0.CFPTR7.uint8_t[LL]
#define RSCAN0CFPTR7LH RSCAN0.CFPTR7.uint8_t[LH]
#define RSCAN0CFPTR7H RSCAN0.CFPTR7.uint16_t[H]
#define RSCAN0CFPTR7HL RSCAN0.CFPTR7.uint8_t[HL]
#define RSCAN0CFPTR7HH RSCAN0.CFPTR7.uint8_t[HH]
#define RSCAN0CFDF07 RSCAN0.CFDF07.uint32_t
#define RSCAN0CFDF07L RSCAN0.CFDF07.uint16_t[L]
#define RSCAN0CFDF07LL RSCAN0.CFDF07.uint8_t[LL]
#define RSCAN0CFDF07LH RSCAN0.CFDF07.uint8_t[LH]
#define RSCAN0CFDF07H RSCAN0.CFDF07.uint16_t[H]
#define RSCAN0CFDF07HL RSCAN0.CFDF07.uint8_t[HL]
#define RSCAN0CFDF07HH RSCAN0.CFDF07.uint8_t[HH]
#define RSCAN0CFDF17 RSCAN0.CFDF17.uint32_t
#define RSCAN0CFDF17L RSCAN0.CFDF17.uint16_t[L]
#define RSCAN0CFDF17LL RSCAN0.CFDF17.uint8_t[LL]
#define RSCAN0CFDF17LH RSCAN0.CFDF17.uint8_t[LH]
#define RSCAN0CFDF17H RSCAN0.CFDF17.uint16_t[H]
#define RSCAN0CFDF17HL RSCAN0.CFDF17.uint8_t[HL]
#define RSCAN0CFDF17HH RSCAN0.CFDF17.uint8_t[HH]
#define RSCAN0CFID8 RSCAN0.CFID8.uint32_t
#define RSCAN0CFID8L RSCAN0.CFID8.uint16_t[L]
#define RSCAN0CFID8LL RSCAN0.CFID8.uint8_t[LL]
#define RSCAN0CFID8LH RSCAN0.CFID8.uint8_t[LH]
#define RSCAN0CFID8H RSCAN0.CFID8.uint16_t[H]
#define RSCAN0CFID8HL RSCAN0.CFID8.uint8_t[HL]
#define RSCAN0CFID8HH RSCAN0.CFID8.uint8_t[HH]
#define RSCAN0CFPTR8 RSCAN0.CFPTR8.uint32_t
#define RSCAN0CFPTR8L RSCAN0.CFPTR8.uint16_t[L]
#define RSCAN0CFPTR8LL RSCAN0.CFPTR8.uint8_t[LL]
#define RSCAN0CFPTR8LH RSCAN0.CFPTR8.uint8_t[LH]
#define RSCAN0CFPTR8H RSCAN0.CFPTR8.uint16_t[H]
#define RSCAN0CFPTR8HL RSCAN0.CFPTR8.uint8_t[HL]
#define RSCAN0CFPTR8HH RSCAN0.CFPTR8.uint8_t[HH]
#define RSCAN0CFDF08 RSCAN0.CFDF08.uint32_t
#define RSCAN0CFDF08L RSCAN0.CFDF08.uint16_t[L]
#define RSCAN0CFDF08LL RSCAN0.CFDF08.uint8_t[LL]
#define RSCAN0CFDF08LH RSCAN0.CFDF08.uint8_t[LH]
#define RSCAN0CFDF08H RSCAN0.CFDF08.uint16_t[H]
#define RSCAN0CFDF08HL RSCAN0.CFDF08.uint8_t[HL]
#define RSCAN0CFDF08HH RSCAN0.CFDF08.uint8_t[HH]
#define RSCAN0CFDF18 RSCAN0.CFDF18.uint32_t
#define RSCAN0CFDF18L RSCAN0.CFDF18.uint16_t[L]
#define RSCAN0CFDF18LL RSCAN0.CFDF18.uint8_t[LL]
#define RSCAN0CFDF18LH RSCAN0.CFDF18.uint8_t[LH]
#define RSCAN0CFDF18H RSCAN0.CFDF18.uint16_t[H]
#define RSCAN0CFDF18HL RSCAN0.CFDF18.uint8_t[HL]
#define RSCAN0CFDF18HH RSCAN0.CFDF18.uint8_t[HH]
#define RSCAN0CFID9 RSCAN0.CFID9.uint32_t
#define RSCAN0CFID9L RSCAN0.CFID9.uint16_t[L]
#define RSCAN0CFID9LL RSCAN0.CFID9.uint8_t[LL]
#define RSCAN0CFID9LH RSCAN0.CFID9.uint8_t[LH]
#define RSCAN0CFID9H RSCAN0.CFID9.uint16_t[H]
#define RSCAN0CFID9HL RSCAN0.CFID9.uint8_t[HL]
#define RSCAN0CFID9HH RSCAN0.CFID9.uint8_t[HH]
#define RSCAN0CFPTR9 RSCAN0.CFPTR9.uint32_t
#define RSCAN0CFPTR9L RSCAN0.CFPTR9.uint16_t[L]
#define RSCAN0CFPTR9LL RSCAN0.CFPTR9.uint8_t[LL]
#define RSCAN0CFPTR9LH RSCAN0.CFPTR9.uint8_t[LH]
#define RSCAN0CFPTR9H RSCAN0.CFPTR9.uint16_t[H]
#define RSCAN0CFPTR9HL RSCAN0.CFPTR9.uint8_t[HL]
#define RSCAN0CFPTR9HH RSCAN0.CFPTR9.uint8_t[HH]
#define RSCAN0CFDF09 RSCAN0.CFDF09.uint32_t
#define RSCAN0CFDF09L RSCAN0.CFDF09.uint16_t[L]
#define RSCAN0CFDF09LL RSCAN0.CFDF09.uint8_t[LL]
#define RSCAN0CFDF09LH RSCAN0.CFDF09.uint8_t[LH]
#define RSCAN0CFDF09H RSCAN0.CFDF09.uint16_t[H]
#define RSCAN0CFDF09HL RSCAN0.CFDF09.uint8_t[HL]
#define RSCAN0CFDF09HH RSCAN0.CFDF09.uint8_t[HH]
#define RSCAN0CFDF19 RSCAN0.CFDF19.uint32_t
#define RSCAN0CFDF19L RSCAN0.CFDF19.uint16_t[L]
#define RSCAN0CFDF19LL RSCAN0.CFDF19.uint8_t[LL]
#define RSCAN0CFDF19LH RSCAN0.CFDF19.uint8_t[LH]
#define RSCAN0CFDF19H RSCAN0.CFDF19.uint16_t[H]
#define RSCAN0CFDF19HL RSCAN0.CFDF19.uint8_t[HL]
#define RSCAN0CFDF19HH RSCAN0.CFDF19.uint8_t[HH]
#define RSCAN0CFID10 RSCAN0.CFID10.uint32_t
#define RSCAN0CFID10L RSCAN0.CFID10.uint16_t[L]
#define RSCAN0CFID10LL RSCAN0.CFID10.uint8_t[LL]
#define RSCAN0CFID10LH RSCAN0.CFID10.uint8_t[LH]
#define RSCAN0CFID10H RSCAN0.CFID10.uint16_t[H]
#define RSCAN0CFID10HL RSCAN0.CFID10.uint8_t[HL]
#define RSCAN0CFID10HH RSCAN0.CFID10.uint8_t[HH]
#define RSCAN0CFPTR10 RSCAN0.CFPTR10.uint32_t
#define RSCAN0CFPTR10L RSCAN0.CFPTR10.uint16_t[L]
#define RSCAN0CFPTR10LL RSCAN0.CFPTR10.uint8_t[LL]
#define RSCAN0CFPTR10LH RSCAN0.CFPTR10.uint8_t[LH]
#define RSCAN0CFPTR10H RSCAN0.CFPTR10.uint16_t[H]
#define RSCAN0CFPTR10HL RSCAN0.CFPTR10.uint8_t[HL]
#define RSCAN0CFPTR10HH RSCAN0.CFPTR10.uint8_t[HH]
#define RSCAN0CFDF010 RSCAN0.CFDF010.uint32_t
#define RSCAN0CFDF010L RSCAN0.CFDF010.uint16_t[L]
#define RSCAN0CFDF010LL RSCAN0.CFDF010.uint8_t[LL]
#define RSCAN0CFDF010LH RSCAN0.CFDF010.uint8_t[LH]
#define RSCAN0CFDF010H RSCAN0.CFDF010.uint16_t[H]
#define RSCAN0CFDF010HL RSCAN0.CFDF010.uint8_t[HL]
#define RSCAN0CFDF010HH RSCAN0.CFDF010.uint8_t[HH]
#define RSCAN0CFDF110 RSCAN0.CFDF110.uint32_t
#define RSCAN0CFDF110L RSCAN0.CFDF110.uint16_t[L]
#define RSCAN0CFDF110LL RSCAN0.CFDF110.uint8_t[LL]
#define RSCAN0CFDF110LH RSCAN0.CFDF110.uint8_t[LH]
#define RSCAN0CFDF110H RSCAN0.CFDF110.uint16_t[H]
#define RSCAN0CFDF110HL RSCAN0.CFDF110.uint8_t[HL]
#define RSCAN0CFDF110HH RSCAN0.CFDF110.uint8_t[HH]
#define RSCAN0CFID11 RSCAN0.CFID11.uint32_t
#define RSCAN0CFID11L RSCAN0.CFID11.uint16_t[L]
#define RSCAN0CFID11LL RSCAN0.CFID11.uint8_t[LL]
#define RSCAN0CFID11LH RSCAN0.CFID11.uint8_t[LH]
#define RSCAN0CFID11H RSCAN0.CFID11.uint16_t[H]
#define RSCAN0CFID11HL RSCAN0.CFID11.uint8_t[HL]
#define RSCAN0CFID11HH RSCAN0.CFID11.uint8_t[HH]
#define RSCAN0CFPTR11 RSCAN0.CFPTR11.uint32_t
#define RSCAN0CFPTR11L RSCAN0.CFPTR11.uint16_t[L]
#define RSCAN0CFPTR11LL RSCAN0.CFPTR11.uint8_t[LL]
#define RSCAN0CFPTR11LH RSCAN0.CFPTR11.uint8_t[LH]
#define RSCAN0CFPTR11H RSCAN0.CFPTR11.uint16_t[H]
#define RSCAN0CFPTR11HL RSCAN0.CFPTR11.uint8_t[HL]
#define RSCAN0CFPTR11HH RSCAN0.CFPTR11.uint8_t[HH]
#define RSCAN0CFDF011 RSCAN0.CFDF011.uint32_t
#define RSCAN0CFDF011L RSCAN0.CFDF011.uint16_t[L]
#define RSCAN0CFDF011LL RSCAN0.CFDF011.uint8_t[LL]
#define RSCAN0CFDF011LH RSCAN0.CFDF011.uint8_t[LH]
#define RSCAN0CFDF011H RSCAN0.CFDF011.uint16_t[H]
#define RSCAN0CFDF011HL RSCAN0.CFDF011.uint8_t[HL]
#define RSCAN0CFDF011HH RSCAN0.CFDF011.uint8_t[HH]
#define RSCAN0CFDF111 RSCAN0.CFDF111.uint32_t
#define RSCAN0CFDF111L RSCAN0.CFDF111.uint16_t[L]
#define RSCAN0CFDF111LL RSCAN0.CFDF111.uint8_t[LL]
#define RSCAN0CFDF111LH RSCAN0.CFDF111.uint8_t[LH]
#define RSCAN0CFDF111H RSCAN0.CFDF111.uint16_t[H]
#define RSCAN0CFDF111HL RSCAN0.CFDF111.uint8_t[HL]
#define RSCAN0CFDF111HH RSCAN0.CFDF111.uint8_t[HH]
#define RSCAN0CFID12 RSCAN0.CFID12.uint32_t
#define RSCAN0CFID12L RSCAN0.CFID12.uint16_t[L]
#define RSCAN0CFID12LL RSCAN0.CFID12.uint8_t[LL]
#define RSCAN0CFID12LH RSCAN0.CFID12.uint8_t[LH]
#define RSCAN0CFID12H RSCAN0.CFID12.uint16_t[H]
#define RSCAN0CFID12HL RSCAN0.CFID12.uint8_t[HL]
#define RSCAN0CFID12HH RSCAN0.CFID12.uint8_t[HH]
#define RSCAN0CFPTR12 RSCAN0.CFPTR12.uint32_t
#define RSCAN0CFPTR12L RSCAN0.CFPTR12.uint16_t[L]
#define RSCAN0CFPTR12LL RSCAN0.CFPTR12.uint8_t[LL]
#define RSCAN0CFPTR12LH RSCAN0.CFPTR12.uint8_t[LH]
#define RSCAN0CFPTR12H RSCAN0.CFPTR12.uint16_t[H]
#define RSCAN0CFPTR12HL RSCAN0.CFPTR12.uint8_t[HL]
#define RSCAN0CFPTR12HH RSCAN0.CFPTR12.uint8_t[HH]
#define RSCAN0CFDF012 RSCAN0.CFDF012.uint32_t
#define RSCAN0CFDF012L RSCAN0.CFDF012.uint16_t[L]
#define RSCAN0CFDF012LL RSCAN0.CFDF012.uint8_t[LL]
#define RSCAN0CFDF012LH RSCAN0.CFDF012.uint8_t[LH]
#define RSCAN0CFDF012H RSCAN0.CFDF012.uint16_t[H]
#define RSCAN0CFDF012HL RSCAN0.CFDF012.uint8_t[HL]
#define RSCAN0CFDF012HH RSCAN0.CFDF012.uint8_t[HH]
#define RSCAN0CFDF112 RSCAN0.CFDF112.uint32_t
#define RSCAN0CFDF112L RSCAN0.CFDF112.uint16_t[L]
#define RSCAN0CFDF112LL RSCAN0.CFDF112.uint8_t[LL]
#define RSCAN0CFDF112LH RSCAN0.CFDF112.uint8_t[LH]
#define RSCAN0CFDF112H RSCAN0.CFDF112.uint16_t[H]
#define RSCAN0CFDF112HL RSCAN0.CFDF112.uint8_t[HL]
#define RSCAN0CFDF112HH RSCAN0.CFDF112.uint8_t[HH]
#define RSCAN0CFID13 RSCAN0.CFID13.uint32_t
#define RSCAN0CFID13L RSCAN0.CFID13.uint16_t[L]
#define RSCAN0CFID13LL RSCAN0.CFID13.uint8_t[LL]
#define RSCAN0CFID13LH RSCAN0.CFID13.uint8_t[LH]
#define RSCAN0CFID13H RSCAN0.CFID13.uint16_t[H]
#define RSCAN0CFID13HL RSCAN0.CFID13.uint8_t[HL]
#define RSCAN0CFID13HH RSCAN0.CFID13.uint8_t[HH]
#define RSCAN0CFPTR13 RSCAN0.CFPTR13.uint32_t
#define RSCAN0CFPTR13L RSCAN0.CFPTR13.uint16_t[L]
#define RSCAN0CFPTR13LL RSCAN0.CFPTR13.uint8_t[LL]
#define RSCAN0CFPTR13LH RSCAN0.CFPTR13.uint8_t[LH]
#define RSCAN0CFPTR13H RSCAN0.CFPTR13.uint16_t[H]
#define RSCAN0CFPTR13HL RSCAN0.CFPTR13.uint8_t[HL]
#define RSCAN0CFPTR13HH RSCAN0.CFPTR13.uint8_t[HH]
#define RSCAN0CFDF013 RSCAN0.CFDF013.uint32_t
#define RSCAN0CFDF013L RSCAN0.CFDF013.uint16_t[L]
#define RSCAN0CFDF013LL RSCAN0.CFDF013.uint8_t[LL]
#define RSCAN0CFDF013LH RSCAN0.CFDF013.uint8_t[LH]
#define RSCAN0CFDF013H RSCAN0.CFDF013.uint16_t[H]
#define RSCAN0CFDF013HL RSCAN0.CFDF013.uint8_t[HL]
#define RSCAN0CFDF013HH RSCAN0.CFDF013.uint8_t[HH]
#define RSCAN0CFDF113 RSCAN0.CFDF113.uint32_t
#define RSCAN0CFDF113L RSCAN0.CFDF113.uint16_t[L]
#define RSCAN0CFDF113LL RSCAN0.CFDF113.uint8_t[LL]
#define RSCAN0CFDF113LH RSCAN0.CFDF113.uint8_t[LH]
#define RSCAN0CFDF113H RSCAN0.CFDF113.uint16_t[H]
#define RSCAN0CFDF113HL RSCAN0.CFDF113.uint8_t[HL]
#define RSCAN0CFDF113HH RSCAN0.CFDF113.uint8_t[HH]
#define RSCAN0CFID14 RSCAN0.CFID14.uint32_t
#define RSCAN0CFID14L RSCAN0.CFID14.uint16_t[L]
#define RSCAN0CFID14LL RSCAN0.CFID14.uint8_t[LL]
#define RSCAN0CFID14LH RSCAN0.CFID14.uint8_t[LH]
#define RSCAN0CFID14H RSCAN0.CFID14.uint16_t[H]
#define RSCAN0CFID14HL RSCAN0.CFID14.uint8_t[HL]
#define RSCAN0CFID14HH RSCAN0.CFID14.uint8_t[HH]
#define RSCAN0CFPTR14 RSCAN0.CFPTR14.uint32_t
#define RSCAN0CFPTR14L RSCAN0.CFPTR14.uint16_t[L]
#define RSCAN0CFPTR14LL RSCAN0.CFPTR14.uint8_t[LL]
#define RSCAN0CFPTR14LH RSCAN0.CFPTR14.uint8_t[LH]
#define RSCAN0CFPTR14H RSCAN0.CFPTR14.uint16_t[H]
#define RSCAN0CFPTR14HL RSCAN0.CFPTR14.uint8_t[HL]
#define RSCAN0CFPTR14HH RSCAN0.CFPTR14.uint8_t[HH]
#define RSCAN0CFDF014 RSCAN0.CFDF014.uint32_t
#define RSCAN0CFDF014L RSCAN0.CFDF014.uint16_t[L]
#define RSCAN0CFDF014LL RSCAN0.CFDF014.uint8_t[LL]
#define RSCAN0CFDF014LH RSCAN0.CFDF014.uint8_t[LH]
#define RSCAN0CFDF014H RSCAN0.CFDF014.uint16_t[H]
#define RSCAN0CFDF014HL RSCAN0.CFDF014.uint8_t[HL]
#define RSCAN0CFDF014HH RSCAN0.CFDF014.uint8_t[HH]
#define RSCAN0CFDF114 RSCAN0.CFDF114.uint32_t
#define RSCAN0CFDF114L RSCAN0.CFDF114.uint16_t[L]
#define RSCAN0CFDF114LL RSCAN0.CFDF114.uint8_t[LL]
#define RSCAN0CFDF114LH RSCAN0.CFDF114.uint8_t[LH]
#define RSCAN0CFDF114H RSCAN0.CFDF114.uint16_t[H]
#define RSCAN0CFDF114HL RSCAN0.CFDF114.uint8_t[HL]
#define RSCAN0CFDF114HH RSCAN0.CFDF114.uint8_t[HH]
#define RSCAN0CFID15 RSCAN0.CFID15.uint32_t
#define RSCAN0CFID15L RSCAN0.CFID15.uint16_t[L]
#define RSCAN0CFID15LL RSCAN0.CFID15.uint8_t[LL]
#define RSCAN0CFID15LH RSCAN0.CFID15.uint8_t[LH]
#define RSCAN0CFID15H RSCAN0.CFID15.uint16_t[H]
#define RSCAN0CFID15HL RSCAN0.CFID15.uint8_t[HL]
#define RSCAN0CFID15HH RSCAN0.CFID15.uint8_t[HH]
#define RSCAN0CFPTR15 RSCAN0.CFPTR15.uint32_t
#define RSCAN0CFPTR15L RSCAN0.CFPTR15.uint16_t[L]
#define RSCAN0CFPTR15LL RSCAN0.CFPTR15.uint8_t[LL]
#define RSCAN0CFPTR15LH RSCAN0.CFPTR15.uint8_t[LH]
#define RSCAN0CFPTR15H RSCAN0.CFPTR15.uint16_t[H]
#define RSCAN0CFPTR15HL RSCAN0.CFPTR15.uint8_t[HL]
#define RSCAN0CFPTR15HH RSCAN0.CFPTR15.uint8_t[HH]
#define RSCAN0CFDF015 RSCAN0.CFDF015.uint32_t
#define RSCAN0CFDF015L RSCAN0.CFDF015.uint16_t[L]
#define RSCAN0CFDF015LL RSCAN0.CFDF015.uint8_t[LL]
#define RSCAN0CFDF015LH RSCAN0.CFDF015.uint8_t[LH]
#define RSCAN0CFDF015H RSCAN0.CFDF015.uint16_t[H]
#define RSCAN0CFDF015HL RSCAN0.CFDF015.uint8_t[HL]
#define RSCAN0CFDF015HH RSCAN0.CFDF015.uint8_t[HH]
#define RSCAN0CFDF115 RSCAN0.CFDF115.uint32_t
#define RSCAN0CFDF115L RSCAN0.CFDF115.uint16_t[L]
#define RSCAN0CFDF115LL RSCAN0.CFDF115.uint8_t[LL]
#define RSCAN0CFDF115LH RSCAN0.CFDF115.uint8_t[LH]
#define RSCAN0CFDF115H RSCAN0.CFDF115.uint16_t[H]
#define RSCAN0CFDF115HL RSCAN0.CFDF115.uint8_t[HL]
#define RSCAN0CFDF115HH RSCAN0.CFDF115.uint8_t[HH]
#define RSCAN0CFID16 RSCAN0.CFID16.uint32_t
#define RSCAN0CFID16L RSCAN0.CFID16.uint16_t[L]
#define RSCAN0CFID16LL RSCAN0.CFID16.uint8_t[LL]
#define RSCAN0CFID16LH RSCAN0.CFID16.uint8_t[LH]
#define RSCAN0CFID16H RSCAN0.CFID16.uint16_t[H]
#define RSCAN0CFID16HL RSCAN0.CFID16.uint8_t[HL]
#define RSCAN0CFID16HH RSCAN0.CFID16.uint8_t[HH]
#define RSCAN0CFPTR16 RSCAN0.CFPTR16.uint32_t
#define RSCAN0CFPTR16L RSCAN0.CFPTR16.uint16_t[L]
#define RSCAN0CFPTR16LL RSCAN0.CFPTR16.uint8_t[LL]
#define RSCAN0CFPTR16LH RSCAN0.CFPTR16.uint8_t[LH]
#define RSCAN0CFPTR16H RSCAN0.CFPTR16.uint16_t[H]
#define RSCAN0CFPTR16HL RSCAN0.CFPTR16.uint8_t[HL]
#define RSCAN0CFPTR16HH RSCAN0.CFPTR16.uint8_t[HH]
#define RSCAN0CFDF016 RSCAN0.CFDF016.uint32_t
#define RSCAN0CFDF016L RSCAN0.CFDF016.uint16_t[L]
#define RSCAN0CFDF016LL RSCAN0.CFDF016.uint8_t[LL]
#define RSCAN0CFDF016LH RSCAN0.CFDF016.uint8_t[LH]
#define RSCAN0CFDF016H RSCAN0.CFDF016.uint16_t[H]
#define RSCAN0CFDF016HL RSCAN0.CFDF016.uint8_t[HL]
#define RSCAN0CFDF016HH RSCAN0.CFDF016.uint8_t[HH]
#define RSCAN0CFDF116 RSCAN0.CFDF116.uint32_t
#define RSCAN0CFDF116L RSCAN0.CFDF116.uint16_t[L]
#define RSCAN0CFDF116LL RSCAN0.CFDF116.uint8_t[LL]
#define RSCAN0CFDF116LH RSCAN0.CFDF116.uint8_t[LH]
#define RSCAN0CFDF116H RSCAN0.CFDF116.uint16_t[H]
#define RSCAN0CFDF116HL RSCAN0.CFDF116.uint8_t[HL]
#define RSCAN0CFDF116HH RSCAN0.CFDF116.uint8_t[HH]
#define RSCAN0CFID17 RSCAN0.CFID17.uint32_t
#define RSCAN0CFID17L RSCAN0.CFID17.uint16_t[L]
#define RSCAN0CFID17LL RSCAN0.CFID17.uint8_t[LL]
#define RSCAN0CFID17LH RSCAN0.CFID17.uint8_t[LH]
#define RSCAN0CFID17H RSCAN0.CFID17.uint16_t[H]
#define RSCAN0CFID17HL RSCAN0.CFID17.uint8_t[HL]
#define RSCAN0CFID17HH RSCAN0.CFID17.uint8_t[HH]
#define RSCAN0CFPTR17 RSCAN0.CFPTR17.uint32_t
#define RSCAN0CFPTR17L RSCAN0.CFPTR17.uint16_t[L]
#define RSCAN0CFPTR17LL RSCAN0.CFPTR17.uint8_t[LL]
#define RSCAN0CFPTR17LH RSCAN0.CFPTR17.uint8_t[LH]
#define RSCAN0CFPTR17H RSCAN0.CFPTR17.uint16_t[H]
#define RSCAN0CFPTR17HL RSCAN0.CFPTR17.uint8_t[HL]
#define RSCAN0CFPTR17HH RSCAN0.CFPTR17.uint8_t[HH]
#define RSCAN0CFDF017 RSCAN0.CFDF017.uint32_t
#define RSCAN0CFDF017L RSCAN0.CFDF017.uint16_t[L]
#define RSCAN0CFDF017LL RSCAN0.CFDF017.uint8_t[LL]
#define RSCAN0CFDF017LH RSCAN0.CFDF017.uint8_t[LH]
#define RSCAN0CFDF017H RSCAN0.CFDF017.uint16_t[H]
#define RSCAN0CFDF017HL RSCAN0.CFDF017.uint8_t[HL]
#define RSCAN0CFDF017HH RSCAN0.CFDF017.uint8_t[HH]
#define RSCAN0CFDF117 RSCAN0.CFDF117.uint32_t
#define RSCAN0CFDF117L RSCAN0.CFDF117.uint16_t[L]
#define RSCAN0CFDF117LL RSCAN0.CFDF117.uint8_t[LL]
#define RSCAN0CFDF117LH RSCAN0.CFDF117.uint8_t[LH]
#define RSCAN0CFDF117H RSCAN0.CFDF117.uint16_t[H]
#define RSCAN0CFDF117HL RSCAN0.CFDF117.uint8_t[HL]
#define RSCAN0CFDF117HH RSCAN0.CFDF117.uint8_t[HH]
#define RSCAN0TMID0 RSCAN0.TMID0.uint32_t
#define RSCAN0TMID0L RSCAN0.TMID0.uint16_t[L]
#define RSCAN0TMID0LL RSCAN0.TMID0.uint8_t[LL]
#define RSCAN0TMID0LH RSCAN0.TMID0.uint8_t[LH]
#define RSCAN0TMID0H RSCAN0.TMID0.uint16_t[H]
#define RSCAN0TMID0HL RSCAN0.TMID0.uint8_t[HL]
#define RSCAN0TMID0HH RSCAN0.TMID0.uint8_t[HH]
#define RSCAN0TMPTR0 RSCAN0.TMPTR0.uint32_t
#define RSCAN0TMPTR0H RSCAN0.TMPTR0.uint16_t[H]
#define RSCAN0TMPTR0HL RSCAN0.TMPTR0.uint8_t[HL]
#define RSCAN0TMPTR0HH RSCAN0.TMPTR0.uint8_t[HH]
#define RSCAN0TMDF00 RSCAN0.TMDF00.uint32_t
#define RSCAN0TMDF00L RSCAN0.TMDF00.uint16_t[L]
#define RSCAN0TMDF00LL RSCAN0.TMDF00.uint8_t[LL]
#define RSCAN0TMDF00LH RSCAN0.TMDF00.uint8_t[LH]
#define RSCAN0TMDF00H RSCAN0.TMDF00.uint16_t[H]
#define RSCAN0TMDF00HL RSCAN0.TMDF00.uint8_t[HL]
#define RSCAN0TMDF00HH RSCAN0.TMDF00.uint8_t[HH]
#define RSCAN0TMDF10 RSCAN0.TMDF10.uint32_t
#define RSCAN0TMDF10L RSCAN0.TMDF10.uint16_t[L]
#define RSCAN0TMDF10LL RSCAN0.TMDF10.uint8_t[LL]
#define RSCAN0TMDF10LH RSCAN0.TMDF10.uint8_t[LH]
#define RSCAN0TMDF10H RSCAN0.TMDF10.uint16_t[H]
#define RSCAN0TMDF10HL RSCAN0.TMDF10.uint8_t[HL]
#define RSCAN0TMDF10HH RSCAN0.TMDF10.uint8_t[HH]
#define RSCAN0TMID1 RSCAN0.TMID1.uint32_t
#define RSCAN0TMID1L RSCAN0.TMID1.uint16_t[L]
#define RSCAN0TMID1LL RSCAN0.TMID1.uint8_t[LL]
#define RSCAN0TMID1LH RSCAN0.TMID1.uint8_t[LH]
#define RSCAN0TMID1H RSCAN0.TMID1.uint16_t[H]
#define RSCAN0TMID1HL RSCAN0.TMID1.uint8_t[HL]
#define RSCAN0TMID1HH RSCAN0.TMID1.uint8_t[HH]
#define RSCAN0TMPTR1 RSCAN0.TMPTR1.uint32_t
#define RSCAN0TMPTR1H RSCAN0.TMPTR1.uint16_t[H]
#define RSCAN0TMPTR1HL RSCAN0.TMPTR1.uint8_t[HL]
#define RSCAN0TMPTR1HH RSCAN0.TMPTR1.uint8_t[HH]
#define RSCAN0TMDF01 RSCAN0.TMDF01.uint32_t
#define RSCAN0TMDF01L RSCAN0.TMDF01.uint16_t[L]
#define RSCAN0TMDF01LL RSCAN0.TMDF01.uint8_t[LL]
#define RSCAN0TMDF01LH RSCAN0.TMDF01.uint8_t[LH]
#define RSCAN0TMDF01H RSCAN0.TMDF01.uint16_t[H]
#define RSCAN0TMDF01HL RSCAN0.TMDF01.uint8_t[HL]
#define RSCAN0TMDF01HH RSCAN0.TMDF01.uint8_t[HH]
#define RSCAN0TMDF11 RSCAN0.TMDF11.uint32_t
#define RSCAN0TMDF11L RSCAN0.TMDF11.uint16_t[L]
#define RSCAN0TMDF11LL RSCAN0.TMDF11.uint8_t[LL]
#define RSCAN0TMDF11LH RSCAN0.TMDF11.uint8_t[LH]
#define RSCAN0TMDF11H RSCAN0.TMDF11.uint16_t[H]
#define RSCAN0TMDF11HL RSCAN0.TMDF11.uint8_t[HL]
#define RSCAN0TMDF11HH RSCAN0.TMDF11.uint8_t[HH]
#define RSCAN0TMID2 RSCAN0.TMID2.uint32_t
#define RSCAN0TMID2L RSCAN0.TMID2.uint16_t[L]
#define RSCAN0TMID2LL RSCAN0.TMID2.uint8_t[LL]
#define RSCAN0TMID2LH RSCAN0.TMID2.uint8_t[LH]
#define RSCAN0TMID2H RSCAN0.TMID2.uint16_t[H]
#define RSCAN0TMID2HL RSCAN0.TMID2.uint8_t[HL]
#define RSCAN0TMID2HH RSCAN0.TMID2.uint8_t[HH]
#define RSCAN0TMPTR2 RSCAN0.TMPTR2.uint32_t
#define RSCAN0TMPTR2H RSCAN0.TMPTR2.uint16_t[H]
#define RSCAN0TMPTR2HL RSCAN0.TMPTR2.uint8_t[HL]
#define RSCAN0TMPTR2HH RSCAN0.TMPTR2.uint8_t[HH]
#define RSCAN0TMDF02 RSCAN0.TMDF02.uint32_t
#define RSCAN0TMDF02L RSCAN0.TMDF02.uint16_t[L]
#define RSCAN0TMDF02LL RSCAN0.TMDF02.uint8_t[LL]
#define RSCAN0TMDF02LH RSCAN0.TMDF02.uint8_t[LH]
#define RSCAN0TMDF02H RSCAN0.TMDF02.uint16_t[H]
#define RSCAN0TMDF02HL RSCAN0.TMDF02.uint8_t[HL]
#define RSCAN0TMDF02HH RSCAN0.TMDF02.uint8_t[HH]
#define RSCAN0TMDF12 RSCAN0.TMDF12.uint32_t
#define RSCAN0TMDF12L RSCAN0.TMDF12.uint16_t[L]
#define RSCAN0TMDF12LL RSCAN0.TMDF12.uint8_t[LL]
#define RSCAN0TMDF12LH RSCAN0.TMDF12.uint8_t[LH]
#define RSCAN0TMDF12H RSCAN0.TMDF12.uint16_t[H]
#define RSCAN0TMDF12HL RSCAN0.TMDF12.uint8_t[HL]
#define RSCAN0TMDF12HH RSCAN0.TMDF12.uint8_t[HH]
#define RSCAN0TMID3 RSCAN0.TMID3.uint32_t
#define RSCAN0TMID3L RSCAN0.TMID3.uint16_t[L]
#define RSCAN0TMID3LL RSCAN0.TMID3.uint8_t[LL]
#define RSCAN0TMID3LH RSCAN0.TMID3.uint8_t[LH]
#define RSCAN0TMID3H RSCAN0.TMID3.uint16_t[H]
#define RSCAN0TMID3HL RSCAN0.TMID3.uint8_t[HL]
#define RSCAN0TMID3HH RSCAN0.TMID3.uint8_t[HH]
#define RSCAN0TMPTR3 RSCAN0.TMPTR3.uint32_t
#define RSCAN0TMPTR3H RSCAN0.TMPTR3.uint16_t[H]
#define RSCAN0TMPTR3HL RSCAN0.TMPTR3.uint8_t[HL]
#define RSCAN0TMPTR3HH RSCAN0.TMPTR3.uint8_t[HH]
#define RSCAN0TMDF03 RSCAN0.TMDF03.uint32_t
#define RSCAN0TMDF03L RSCAN0.TMDF03.uint16_t[L]
#define RSCAN0TMDF03LL RSCAN0.TMDF03.uint8_t[LL]
#define RSCAN0TMDF03LH RSCAN0.TMDF03.uint8_t[LH]
#define RSCAN0TMDF03H RSCAN0.TMDF03.uint16_t[H]
#define RSCAN0TMDF03HL RSCAN0.TMDF03.uint8_t[HL]
#define RSCAN0TMDF03HH RSCAN0.TMDF03.uint8_t[HH]
#define RSCAN0TMDF13 RSCAN0.TMDF13.uint32_t
#define RSCAN0TMDF13L RSCAN0.TMDF13.uint16_t[L]
#define RSCAN0TMDF13LL RSCAN0.TMDF13.uint8_t[LL]
#define RSCAN0TMDF13LH RSCAN0.TMDF13.uint8_t[LH]
#define RSCAN0TMDF13H RSCAN0.TMDF13.uint16_t[H]
#define RSCAN0TMDF13HL RSCAN0.TMDF13.uint8_t[HL]
#define RSCAN0TMDF13HH RSCAN0.TMDF13.uint8_t[HH]
#define RSCAN0TMID4 RSCAN0.TMID4.uint32_t
#define RSCAN0TMID4L RSCAN0.TMID4.uint16_t[L]
#define RSCAN0TMID4LL RSCAN0.TMID4.uint8_t[LL]
#define RSCAN0TMID4LH RSCAN0.TMID4.uint8_t[LH]
#define RSCAN0TMID4H RSCAN0.TMID4.uint16_t[H]
#define RSCAN0TMID4HL RSCAN0.TMID4.uint8_t[HL]
#define RSCAN0TMID4HH RSCAN0.TMID4.uint8_t[HH]
#define RSCAN0TMPTR4 RSCAN0.TMPTR4.uint32_t
#define RSCAN0TMPTR4H RSCAN0.TMPTR4.uint16_t[H]
#define RSCAN0TMPTR4HL RSCAN0.TMPTR4.uint8_t[HL]
#define RSCAN0TMPTR4HH RSCAN0.TMPTR4.uint8_t[HH]
#define RSCAN0TMDF04 RSCAN0.TMDF04.uint32_t
#define RSCAN0TMDF04L RSCAN0.TMDF04.uint16_t[L]
#define RSCAN0TMDF04LL RSCAN0.TMDF04.uint8_t[LL]
#define RSCAN0TMDF04LH RSCAN0.TMDF04.uint8_t[LH]
#define RSCAN0TMDF04H RSCAN0.TMDF04.uint16_t[H]
#define RSCAN0TMDF04HL RSCAN0.TMDF04.uint8_t[HL]
#define RSCAN0TMDF04HH RSCAN0.TMDF04.uint8_t[HH]
#define RSCAN0TMDF14 RSCAN0.TMDF14.uint32_t
#define RSCAN0TMDF14L RSCAN0.TMDF14.uint16_t[L]
#define RSCAN0TMDF14LL RSCAN0.TMDF14.uint8_t[LL]
#define RSCAN0TMDF14LH RSCAN0.TMDF14.uint8_t[LH]
#define RSCAN0TMDF14H RSCAN0.TMDF14.uint16_t[H]
#define RSCAN0TMDF14HL RSCAN0.TMDF14.uint8_t[HL]
#define RSCAN0TMDF14HH RSCAN0.TMDF14.uint8_t[HH]
#define RSCAN0TMID5 RSCAN0.TMID5.uint32_t
#define RSCAN0TMID5L RSCAN0.TMID5.uint16_t[L]
#define RSCAN0TMID5LL RSCAN0.TMID5.uint8_t[LL]
#define RSCAN0TMID5LH RSCAN0.TMID5.uint8_t[LH]
#define RSCAN0TMID5H RSCAN0.TMID5.uint16_t[H]
#define RSCAN0TMID5HL RSCAN0.TMID5.uint8_t[HL]
#define RSCAN0TMID5HH RSCAN0.TMID5.uint8_t[HH]
#define RSCAN0TMPTR5 RSCAN0.TMPTR5.uint32_t
#define RSCAN0TMPTR5H RSCAN0.TMPTR5.uint16_t[H]
#define RSCAN0TMPTR5HL RSCAN0.TMPTR5.uint8_t[HL]
#define RSCAN0TMPTR5HH RSCAN0.TMPTR5.uint8_t[HH]
#define RSCAN0TMDF05 RSCAN0.TMDF05.uint32_t
#define RSCAN0TMDF05L RSCAN0.TMDF05.uint16_t[L]
#define RSCAN0TMDF05LL RSCAN0.TMDF05.uint8_t[LL]
#define RSCAN0TMDF05LH RSCAN0.TMDF05.uint8_t[LH]
#define RSCAN0TMDF05H RSCAN0.TMDF05.uint16_t[H]
#define RSCAN0TMDF05HL RSCAN0.TMDF05.uint8_t[HL]
#define RSCAN0TMDF05HH RSCAN0.TMDF05.uint8_t[HH]
#define RSCAN0TMDF15 RSCAN0.TMDF15.uint32_t
#define RSCAN0TMDF15L RSCAN0.TMDF15.uint16_t[L]
#define RSCAN0TMDF15LL RSCAN0.TMDF15.uint8_t[LL]
#define RSCAN0TMDF15LH RSCAN0.TMDF15.uint8_t[LH]
#define RSCAN0TMDF15H RSCAN0.TMDF15.uint16_t[H]
#define RSCAN0TMDF15HL RSCAN0.TMDF15.uint8_t[HL]
#define RSCAN0TMDF15HH RSCAN0.TMDF15.uint8_t[HH]
#define RSCAN0TMID6 RSCAN0.TMID6.uint32_t
#define RSCAN0TMID6L RSCAN0.TMID6.uint16_t[L]
#define RSCAN0TMID6LL RSCAN0.TMID6.uint8_t[LL]
#define RSCAN0TMID6LH RSCAN0.TMID6.uint8_t[LH]
#define RSCAN0TMID6H RSCAN0.TMID6.uint16_t[H]
#define RSCAN0TMID6HL RSCAN0.TMID6.uint8_t[HL]
#define RSCAN0TMID6HH RSCAN0.TMID6.uint8_t[HH]
#define RSCAN0TMPTR6 RSCAN0.TMPTR6.uint32_t
#define RSCAN0TMPTR6H RSCAN0.TMPTR6.uint16_t[H]
#define RSCAN0TMPTR6HL RSCAN0.TMPTR6.uint8_t[HL]
#define RSCAN0TMPTR6HH RSCAN0.TMPTR6.uint8_t[HH]
#define RSCAN0TMDF06 RSCAN0.TMDF06.uint32_t
#define RSCAN0TMDF06L RSCAN0.TMDF06.uint16_t[L]
#define RSCAN0TMDF06LL RSCAN0.TMDF06.uint8_t[LL]
#define RSCAN0TMDF06LH RSCAN0.TMDF06.uint8_t[LH]
#define RSCAN0TMDF06H RSCAN0.TMDF06.uint16_t[H]
#define RSCAN0TMDF06HL RSCAN0.TMDF06.uint8_t[HL]
#define RSCAN0TMDF06HH RSCAN0.TMDF06.uint8_t[HH]
#define RSCAN0TMDF16 RSCAN0.TMDF16.uint32_t
#define RSCAN0TMDF16L RSCAN0.TMDF16.uint16_t[L]
#define RSCAN0TMDF16LL RSCAN0.TMDF16.uint8_t[LL]
#define RSCAN0TMDF16LH RSCAN0.TMDF16.uint8_t[LH]
#define RSCAN0TMDF16H RSCAN0.TMDF16.uint16_t[H]
#define RSCAN0TMDF16HL RSCAN0.TMDF16.uint8_t[HL]
#define RSCAN0TMDF16HH RSCAN0.TMDF16.uint8_t[HH]
#define RSCAN0TMID7 RSCAN0.TMID7.uint32_t
#define RSCAN0TMID7L RSCAN0.TMID7.uint16_t[L]
#define RSCAN0TMID7LL RSCAN0.TMID7.uint8_t[LL]
#define RSCAN0TMID7LH RSCAN0.TMID7.uint8_t[LH]
#define RSCAN0TMID7H RSCAN0.TMID7.uint16_t[H]
#define RSCAN0TMID7HL RSCAN0.TMID7.uint8_t[HL]
#define RSCAN0TMID7HH RSCAN0.TMID7.uint8_t[HH]
#define RSCAN0TMPTR7 RSCAN0.TMPTR7.uint32_t
#define RSCAN0TMPTR7H RSCAN0.TMPTR7.uint16_t[H]
#define RSCAN0TMPTR7HL RSCAN0.TMPTR7.uint8_t[HL]
#define RSCAN0TMPTR7HH RSCAN0.TMPTR7.uint8_t[HH]
#define RSCAN0TMDF07 RSCAN0.TMDF07.uint32_t
#define RSCAN0TMDF07L RSCAN0.TMDF07.uint16_t[L]
#define RSCAN0TMDF07LL RSCAN0.TMDF07.uint8_t[LL]
#define RSCAN0TMDF07LH RSCAN0.TMDF07.uint8_t[LH]
#define RSCAN0TMDF07H RSCAN0.TMDF07.uint16_t[H]
#define RSCAN0TMDF07HL RSCAN0.TMDF07.uint8_t[HL]
#define RSCAN0TMDF07HH RSCAN0.TMDF07.uint8_t[HH]
#define RSCAN0TMDF17 RSCAN0.TMDF17.uint32_t
#define RSCAN0TMDF17L RSCAN0.TMDF17.uint16_t[L]
#define RSCAN0TMDF17LL RSCAN0.TMDF17.uint8_t[LL]
#define RSCAN0TMDF17LH RSCAN0.TMDF17.uint8_t[LH]
#define RSCAN0TMDF17H RSCAN0.TMDF17.uint16_t[H]
#define RSCAN0TMDF17HL RSCAN0.TMDF17.uint8_t[HL]
#define RSCAN0TMDF17HH RSCAN0.TMDF17.uint8_t[HH]
#define RSCAN0TMID8 RSCAN0.TMID8.uint32_t
#define RSCAN0TMID8L RSCAN0.TMID8.uint16_t[L]
#define RSCAN0TMID8LL RSCAN0.TMID8.uint8_t[LL]
#define RSCAN0TMID8LH RSCAN0.TMID8.uint8_t[LH]
#define RSCAN0TMID8H RSCAN0.TMID8.uint16_t[H]
#define RSCAN0TMID8HL RSCAN0.TMID8.uint8_t[HL]
#define RSCAN0TMID8HH RSCAN0.TMID8.uint8_t[HH]
#define RSCAN0TMPTR8 RSCAN0.TMPTR8.uint32_t
#define RSCAN0TMPTR8H RSCAN0.TMPTR8.uint16_t[H]
#define RSCAN0TMPTR8HL RSCAN0.TMPTR8.uint8_t[HL]
#define RSCAN0TMPTR8HH RSCAN0.TMPTR8.uint8_t[HH]
#define RSCAN0TMDF08 RSCAN0.TMDF08.uint32_t
#define RSCAN0TMDF08L RSCAN0.TMDF08.uint16_t[L]
#define RSCAN0TMDF08LL RSCAN0.TMDF08.uint8_t[LL]
#define RSCAN0TMDF08LH RSCAN0.TMDF08.uint8_t[LH]
#define RSCAN0TMDF08H RSCAN0.TMDF08.uint16_t[H]
#define RSCAN0TMDF08HL RSCAN0.TMDF08.uint8_t[HL]
#define RSCAN0TMDF08HH RSCAN0.TMDF08.uint8_t[HH]
#define RSCAN0TMDF18 RSCAN0.TMDF18.uint32_t
#define RSCAN0TMDF18L RSCAN0.TMDF18.uint16_t[L]
#define RSCAN0TMDF18LL RSCAN0.TMDF18.uint8_t[LL]
#define RSCAN0TMDF18LH RSCAN0.TMDF18.uint8_t[LH]
#define RSCAN0TMDF18H RSCAN0.TMDF18.uint16_t[H]
#define RSCAN0TMDF18HL RSCAN0.TMDF18.uint8_t[HL]
#define RSCAN0TMDF18HH RSCAN0.TMDF18.uint8_t[HH]
#define RSCAN0TMID9 RSCAN0.TMID9.uint32_t
#define RSCAN0TMID9L RSCAN0.TMID9.uint16_t[L]
#define RSCAN0TMID9LL RSCAN0.TMID9.uint8_t[LL]
#define RSCAN0TMID9LH RSCAN0.TMID9.uint8_t[LH]
#define RSCAN0TMID9H RSCAN0.TMID9.uint16_t[H]
#define RSCAN0TMID9HL RSCAN0.TMID9.uint8_t[HL]
#define RSCAN0TMID9HH RSCAN0.TMID9.uint8_t[HH]
#define RSCAN0TMPTR9 RSCAN0.TMPTR9.uint32_t
#define RSCAN0TMPTR9H RSCAN0.TMPTR9.uint16_t[H]
#define RSCAN0TMPTR9HL RSCAN0.TMPTR9.uint8_t[HL]
#define RSCAN0TMPTR9HH RSCAN0.TMPTR9.uint8_t[HH]
#define RSCAN0TMDF09 RSCAN0.TMDF09.uint32_t
#define RSCAN0TMDF09L RSCAN0.TMDF09.uint16_t[L]
#define RSCAN0TMDF09LL RSCAN0.TMDF09.uint8_t[LL]
#define RSCAN0TMDF09LH RSCAN0.TMDF09.uint8_t[LH]
#define RSCAN0TMDF09H RSCAN0.TMDF09.uint16_t[H]
#define RSCAN0TMDF09HL RSCAN0.TMDF09.uint8_t[HL]
#define RSCAN0TMDF09HH RSCAN0.TMDF09.uint8_t[HH]
#define RSCAN0TMDF19 RSCAN0.TMDF19.uint32_t
#define RSCAN0TMDF19L RSCAN0.TMDF19.uint16_t[L]
#define RSCAN0TMDF19LL RSCAN0.TMDF19.uint8_t[LL]
#define RSCAN0TMDF19LH RSCAN0.TMDF19.uint8_t[LH]
#define RSCAN0TMDF19H RSCAN0.TMDF19.uint16_t[H]
#define RSCAN0TMDF19HL RSCAN0.TMDF19.uint8_t[HL]
#define RSCAN0TMDF19HH RSCAN0.TMDF19.uint8_t[HH]
#define RSCAN0TMID10 RSCAN0.TMID10.uint32_t
#define RSCAN0TMID10L RSCAN0.TMID10.uint16_t[L]
#define RSCAN0TMID10LL RSCAN0.TMID10.uint8_t[LL]
#define RSCAN0TMID10LH RSCAN0.TMID10.uint8_t[LH]
#define RSCAN0TMID10H RSCAN0.TMID10.uint16_t[H]
#define RSCAN0TMID10HL RSCAN0.TMID10.uint8_t[HL]
#define RSCAN0TMID10HH RSCAN0.TMID10.uint8_t[HH]
#define RSCAN0TMPTR10 RSCAN0.TMPTR10.uint32_t
#define RSCAN0TMPTR10H RSCAN0.TMPTR10.uint16_t[H]
#define RSCAN0TMPTR10HL RSCAN0.TMPTR10.uint8_t[HL]
#define RSCAN0TMPTR10HH RSCAN0.TMPTR10.uint8_t[HH]
#define RSCAN0TMDF010 RSCAN0.TMDF010.uint32_t
#define RSCAN0TMDF010L RSCAN0.TMDF010.uint16_t[L]
#define RSCAN0TMDF010LL RSCAN0.TMDF010.uint8_t[LL]
#define RSCAN0TMDF010LH RSCAN0.TMDF010.uint8_t[LH]
#define RSCAN0TMDF010H RSCAN0.TMDF010.uint16_t[H]
#define RSCAN0TMDF010HL RSCAN0.TMDF010.uint8_t[HL]
#define RSCAN0TMDF010HH RSCAN0.TMDF010.uint8_t[HH]
#define RSCAN0TMDF110 RSCAN0.TMDF110.uint32_t
#define RSCAN0TMDF110L RSCAN0.TMDF110.uint16_t[L]
#define RSCAN0TMDF110LL RSCAN0.TMDF110.uint8_t[LL]
#define RSCAN0TMDF110LH RSCAN0.TMDF110.uint8_t[LH]
#define RSCAN0TMDF110H RSCAN0.TMDF110.uint16_t[H]
#define RSCAN0TMDF110HL RSCAN0.TMDF110.uint8_t[HL]
#define RSCAN0TMDF110HH RSCAN0.TMDF110.uint8_t[HH]
#define RSCAN0TMID11 RSCAN0.TMID11.uint32_t
#define RSCAN0TMID11L RSCAN0.TMID11.uint16_t[L]
#define RSCAN0TMID11LL RSCAN0.TMID11.uint8_t[LL]
#define RSCAN0TMID11LH RSCAN0.TMID11.uint8_t[LH]
#define RSCAN0TMID11H RSCAN0.TMID11.uint16_t[H]
#define RSCAN0TMID11HL RSCAN0.TMID11.uint8_t[HL]
#define RSCAN0TMID11HH RSCAN0.TMID11.uint8_t[HH]
#define RSCAN0TMPTR11 RSCAN0.TMPTR11.uint32_t
#define RSCAN0TMPTR11H RSCAN0.TMPTR11.uint16_t[H]
#define RSCAN0TMPTR11HL RSCAN0.TMPTR11.uint8_t[HL]
#define RSCAN0TMPTR11HH RSCAN0.TMPTR11.uint8_t[HH]
#define RSCAN0TMDF011 RSCAN0.TMDF011.uint32_t
#define RSCAN0TMDF011L RSCAN0.TMDF011.uint16_t[L]
#define RSCAN0TMDF011LL RSCAN0.TMDF011.uint8_t[LL]
#define RSCAN0TMDF011LH RSCAN0.TMDF011.uint8_t[LH]
#define RSCAN0TMDF011H RSCAN0.TMDF011.uint16_t[H]
#define RSCAN0TMDF011HL RSCAN0.TMDF011.uint8_t[HL]
#define RSCAN0TMDF011HH RSCAN0.TMDF011.uint8_t[HH]
#define RSCAN0TMDF111 RSCAN0.TMDF111.uint32_t
#define RSCAN0TMDF111L RSCAN0.TMDF111.uint16_t[L]
#define RSCAN0TMDF111LL RSCAN0.TMDF111.uint8_t[LL]
#define RSCAN0TMDF111LH RSCAN0.TMDF111.uint8_t[LH]
#define RSCAN0TMDF111H RSCAN0.TMDF111.uint16_t[H]
#define RSCAN0TMDF111HL RSCAN0.TMDF111.uint8_t[HL]
#define RSCAN0TMDF111HH RSCAN0.TMDF111.uint8_t[HH]
#define RSCAN0TMID12 RSCAN0.TMID12.uint32_t
#define RSCAN0TMID12L RSCAN0.TMID12.uint16_t[L]
#define RSCAN0TMID12LL RSCAN0.TMID12.uint8_t[LL]
#define RSCAN0TMID12LH RSCAN0.TMID12.uint8_t[LH]
#define RSCAN0TMID12H RSCAN0.TMID12.uint16_t[H]
#define RSCAN0TMID12HL RSCAN0.TMID12.uint8_t[HL]
#define RSCAN0TMID12HH RSCAN0.TMID12.uint8_t[HH]
#define RSCAN0TMPTR12 RSCAN0.TMPTR12.uint32_t
#define RSCAN0TMPTR12H RSCAN0.TMPTR12.uint16_t[H]
#define RSCAN0TMPTR12HL RSCAN0.TMPTR12.uint8_t[HL]
#define RSCAN0TMPTR12HH RSCAN0.TMPTR12.uint8_t[HH]
#define RSCAN0TMDF012 RSCAN0.TMDF012.uint32_t
#define RSCAN0TMDF012L RSCAN0.TMDF012.uint16_t[L]
#define RSCAN0TMDF012LL RSCAN0.TMDF012.uint8_t[LL]
#define RSCAN0TMDF012LH RSCAN0.TMDF012.uint8_t[LH]
#define RSCAN0TMDF012H RSCAN0.TMDF012.uint16_t[H]
#define RSCAN0TMDF012HL RSCAN0.TMDF012.uint8_t[HL]
#define RSCAN0TMDF012HH RSCAN0.TMDF012.uint8_t[HH]
#define RSCAN0TMDF112 RSCAN0.TMDF112.uint32_t
#define RSCAN0TMDF112L RSCAN0.TMDF112.uint16_t[L]
#define RSCAN0TMDF112LL RSCAN0.TMDF112.uint8_t[LL]
#define RSCAN0TMDF112LH RSCAN0.TMDF112.uint8_t[LH]
#define RSCAN0TMDF112H RSCAN0.TMDF112.uint16_t[H]
#define RSCAN0TMDF112HL RSCAN0.TMDF112.uint8_t[HL]
#define RSCAN0TMDF112HH RSCAN0.TMDF112.uint8_t[HH]
#define RSCAN0TMID13 RSCAN0.TMID13.uint32_t
#define RSCAN0TMID13L RSCAN0.TMID13.uint16_t[L]
#define RSCAN0TMID13LL RSCAN0.TMID13.uint8_t[LL]
#define RSCAN0TMID13LH RSCAN0.TMID13.uint8_t[LH]
#define RSCAN0TMID13H RSCAN0.TMID13.uint16_t[H]
#define RSCAN0TMID13HL RSCAN0.TMID13.uint8_t[HL]
#define RSCAN0TMID13HH RSCAN0.TMID13.uint8_t[HH]
#define RSCAN0TMPTR13 RSCAN0.TMPTR13.uint32_t
#define RSCAN0TMPTR13H RSCAN0.TMPTR13.uint16_t[H]
#define RSCAN0TMPTR13HL RSCAN0.TMPTR13.uint8_t[HL]
#define RSCAN0TMPTR13HH RSCAN0.TMPTR13.uint8_t[HH]
#define RSCAN0TMDF013 RSCAN0.TMDF013.uint32_t
#define RSCAN0TMDF013L RSCAN0.TMDF013.uint16_t[L]
#define RSCAN0TMDF013LL RSCAN0.TMDF013.uint8_t[LL]
#define RSCAN0TMDF013LH RSCAN0.TMDF013.uint8_t[LH]
#define RSCAN0TMDF013H RSCAN0.TMDF013.uint16_t[H]
#define RSCAN0TMDF013HL RSCAN0.TMDF013.uint8_t[HL]
#define RSCAN0TMDF013HH RSCAN0.TMDF013.uint8_t[HH]
#define RSCAN0TMDF113 RSCAN0.TMDF113.uint32_t
#define RSCAN0TMDF113L RSCAN0.TMDF113.uint16_t[L]
#define RSCAN0TMDF113LL RSCAN0.TMDF113.uint8_t[LL]
#define RSCAN0TMDF113LH RSCAN0.TMDF113.uint8_t[LH]
#define RSCAN0TMDF113H RSCAN0.TMDF113.uint16_t[H]
#define RSCAN0TMDF113HL RSCAN0.TMDF113.uint8_t[HL]
#define RSCAN0TMDF113HH RSCAN0.TMDF113.uint8_t[HH]
#define RSCAN0TMID14 RSCAN0.TMID14.uint32_t
#define RSCAN0TMID14L RSCAN0.TMID14.uint16_t[L]
#define RSCAN0TMID14LL RSCAN0.TMID14.uint8_t[LL]
#define RSCAN0TMID14LH RSCAN0.TMID14.uint8_t[LH]
#define RSCAN0TMID14H RSCAN0.TMID14.uint16_t[H]
#define RSCAN0TMID14HL RSCAN0.TMID14.uint8_t[HL]
#define RSCAN0TMID14HH RSCAN0.TMID14.uint8_t[HH]
#define RSCAN0TMPTR14 RSCAN0.TMPTR14.uint32_t
#define RSCAN0TMPTR14H RSCAN0.TMPTR14.uint16_t[H]
#define RSCAN0TMPTR14HL RSCAN0.TMPTR14.uint8_t[HL]
#define RSCAN0TMPTR14HH RSCAN0.TMPTR14.uint8_t[HH]
#define RSCAN0TMDF014 RSCAN0.TMDF014.uint32_t
#define RSCAN0TMDF014L RSCAN0.TMDF014.uint16_t[L]
#define RSCAN0TMDF014LL RSCAN0.TMDF014.uint8_t[LL]
#define RSCAN0TMDF014LH RSCAN0.TMDF014.uint8_t[LH]
#define RSCAN0TMDF014H RSCAN0.TMDF014.uint16_t[H]
#define RSCAN0TMDF014HL RSCAN0.TMDF014.uint8_t[HL]
#define RSCAN0TMDF014HH RSCAN0.TMDF014.uint8_t[HH]
#define RSCAN0TMDF114 RSCAN0.TMDF114.uint32_t
#define RSCAN0TMDF114L RSCAN0.TMDF114.uint16_t[L]
#define RSCAN0TMDF114LL RSCAN0.TMDF114.uint8_t[LL]
#define RSCAN0TMDF114LH RSCAN0.TMDF114.uint8_t[LH]
#define RSCAN0TMDF114H RSCAN0.TMDF114.uint16_t[H]
#define RSCAN0TMDF114HL RSCAN0.TMDF114.uint8_t[HL]
#define RSCAN0TMDF114HH RSCAN0.TMDF114.uint8_t[HH]
#define RSCAN0TMID15 RSCAN0.TMID15.uint32_t
#define RSCAN0TMID15L RSCAN0.TMID15.uint16_t[L]
#define RSCAN0TMID15LL RSCAN0.TMID15.uint8_t[LL]
#define RSCAN0TMID15LH RSCAN0.TMID15.uint8_t[LH]
#define RSCAN0TMID15H RSCAN0.TMID15.uint16_t[H]
#define RSCAN0TMID15HL RSCAN0.TMID15.uint8_t[HL]
#define RSCAN0TMID15HH RSCAN0.TMID15.uint8_t[HH]
#define RSCAN0TMPTR15 RSCAN0.TMPTR15.uint32_t
#define RSCAN0TMPTR15H RSCAN0.TMPTR15.uint16_t[H]
#define RSCAN0TMPTR15HL RSCAN0.TMPTR15.uint8_t[HL]
#define RSCAN0TMPTR15HH RSCAN0.TMPTR15.uint8_t[HH]
#define RSCAN0TMDF015 RSCAN0.TMDF015.uint32_t
#define RSCAN0TMDF015L RSCAN0.TMDF015.uint16_t[L]
#define RSCAN0TMDF015LL RSCAN0.TMDF015.uint8_t[LL]
#define RSCAN0TMDF015LH RSCAN0.TMDF015.uint8_t[LH]
#define RSCAN0TMDF015H RSCAN0.TMDF015.uint16_t[H]
#define RSCAN0TMDF015HL RSCAN0.TMDF015.uint8_t[HL]
#define RSCAN0TMDF015HH RSCAN0.TMDF015.uint8_t[HH]
#define RSCAN0TMDF115 RSCAN0.TMDF115.uint32_t
#define RSCAN0TMDF115L RSCAN0.TMDF115.uint16_t[L]
#define RSCAN0TMDF115LL RSCAN0.TMDF115.uint8_t[LL]
#define RSCAN0TMDF115LH RSCAN0.TMDF115.uint8_t[LH]
#define RSCAN0TMDF115H RSCAN0.TMDF115.uint16_t[H]
#define RSCAN0TMDF115HL RSCAN0.TMDF115.uint8_t[HL]
#define RSCAN0TMDF115HH RSCAN0.TMDF115.uint8_t[HH]
#define RSCAN0TMID16 RSCAN0.TMID16.uint32_t
#define RSCAN0TMID16L RSCAN0.TMID16.uint16_t[L]
#define RSCAN0TMID16LL RSCAN0.TMID16.uint8_t[LL]
#define RSCAN0TMID16LH RSCAN0.TMID16.uint8_t[LH]
#define RSCAN0TMID16H RSCAN0.TMID16.uint16_t[H]
#define RSCAN0TMID16HL RSCAN0.TMID16.uint8_t[HL]
#define RSCAN0TMID16HH RSCAN0.TMID16.uint8_t[HH]
#define RSCAN0TMPTR16 RSCAN0.TMPTR16.uint32_t
#define RSCAN0TMPTR16H RSCAN0.TMPTR16.uint16_t[H]
#define RSCAN0TMPTR16HL RSCAN0.TMPTR16.uint8_t[HL]
#define RSCAN0TMPTR16HH RSCAN0.TMPTR16.uint8_t[HH]
#define RSCAN0TMDF016 RSCAN0.TMDF016.uint32_t
#define RSCAN0TMDF016L RSCAN0.TMDF016.uint16_t[L]
#define RSCAN0TMDF016LL RSCAN0.TMDF016.uint8_t[LL]
#define RSCAN0TMDF016LH RSCAN0.TMDF016.uint8_t[LH]
#define RSCAN0TMDF016H RSCAN0.TMDF016.uint16_t[H]
#define RSCAN0TMDF016HL RSCAN0.TMDF016.uint8_t[HL]
#define RSCAN0TMDF016HH RSCAN0.TMDF016.uint8_t[HH]
#define RSCAN0TMDF116 RSCAN0.TMDF116.uint32_t
#define RSCAN0TMDF116L RSCAN0.TMDF116.uint16_t[L]
#define RSCAN0TMDF116LL RSCAN0.TMDF116.uint8_t[LL]
#define RSCAN0TMDF116LH RSCAN0.TMDF116.uint8_t[LH]
#define RSCAN0TMDF116H RSCAN0.TMDF116.uint16_t[H]
#define RSCAN0TMDF116HL RSCAN0.TMDF116.uint8_t[HL]
#define RSCAN0TMDF116HH RSCAN0.TMDF116.uint8_t[HH]
#define RSCAN0TMID17 RSCAN0.TMID17.uint32_t
#define RSCAN0TMID17L RSCAN0.TMID17.uint16_t[L]
#define RSCAN0TMID17LL RSCAN0.TMID17.uint8_t[LL]
#define RSCAN0TMID17LH RSCAN0.TMID17.uint8_t[LH]
#define RSCAN0TMID17H RSCAN0.TMID17.uint16_t[H]
#define RSCAN0TMID17HL RSCAN0.TMID17.uint8_t[HL]
#define RSCAN0TMID17HH RSCAN0.TMID17.uint8_t[HH]
#define RSCAN0TMPTR17 RSCAN0.TMPTR17.uint32_t
#define RSCAN0TMPTR17H RSCAN0.TMPTR17.uint16_t[H]
#define RSCAN0TMPTR17HL RSCAN0.TMPTR17.uint8_t[HL]
#define RSCAN0TMPTR17HH RSCAN0.TMPTR17.uint8_t[HH]
#define RSCAN0TMDF017 RSCAN0.TMDF017.uint32_t
#define RSCAN0TMDF017L RSCAN0.TMDF017.uint16_t[L]
#define RSCAN0TMDF017LL RSCAN0.TMDF017.uint8_t[LL]
#define RSCAN0TMDF017LH RSCAN0.TMDF017.uint8_t[LH]
#define RSCAN0TMDF017H RSCAN0.TMDF017.uint16_t[H]
#define RSCAN0TMDF017HL RSCAN0.TMDF017.uint8_t[HL]
#define RSCAN0TMDF017HH RSCAN0.TMDF017.uint8_t[HH]
#define RSCAN0TMDF117 RSCAN0.TMDF117.uint32_t
#define RSCAN0TMDF117L RSCAN0.TMDF117.uint16_t[L]
#define RSCAN0TMDF117LL RSCAN0.TMDF117.uint8_t[LL]
#define RSCAN0TMDF117LH RSCAN0.TMDF117.uint8_t[LH]
#define RSCAN0TMDF117H RSCAN0.TMDF117.uint16_t[H]
#define RSCAN0TMDF117HL RSCAN0.TMDF117.uint8_t[HL]
#define RSCAN0TMDF117HH RSCAN0.TMDF117.uint8_t[HH]
#define RSCAN0TMID18 RSCAN0.TMID18.uint32_t
#define RSCAN0TMID18L RSCAN0.TMID18.uint16_t[L]
#define RSCAN0TMID18LL RSCAN0.TMID18.uint8_t[LL]
#define RSCAN0TMID18LH RSCAN0.TMID18.uint8_t[LH]
#define RSCAN0TMID18H RSCAN0.TMID18.uint16_t[H]
#define RSCAN0TMID18HL RSCAN0.TMID18.uint8_t[HL]
#define RSCAN0TMID18HH RSCAN0.TMID18.uint8_t[HH]
#define RSCAN0TMPTR18 RSCAN0.TMPTR18.uint32_t
#define RSCAN0TMPTR18H RSCAN0.TMPTR18.uint16_t[H]
#define RSCAN0TMPTR18HL RSCAN0.TMPTR18.uint8_t[HL]
#define RSCAN0TMPTR18HH RSCAN0.TMPTR18.uint8_t[HH]
#define RSCAN0TMDF018 RSCAN0.TMDF018.uint32_t
#define RSCAN0TMDF018L RSCAN0.TMDF018.uint16_t[L]
#define RSCAN0TMDF018LL RSCAN0.TMDF018.uint8_t[LL]
#define RSCAN0TMDF018LH RSCAN0.TMDF018.uint8_t[LH]
#define RSCAN0TMDF018H RSCAN0.TMDF018.uint16_t[H]
#define RSCAN0TMDF018HL RSCAN0.TMDF018.uint8_t[HL]
#define RSCAN0TMDF018HH RSCAN0.TMDF018.uint8_t[HH]
#define RSCAN0TMDF118 RSCAN0.TMDF118.uint32_t
#define RSCAN0TMDF118L RSCAN0.TMDF118.uint16_t[L]
#define RSCAN0TMDF118LL RSCAN0.TMDF118.uint8_t[LL]
#define RSCAN0TMDF118LH RSCAN0.TMDF118.uint8_t[LH]
#define RSCAN0TMDF118H RSCAN0.TMDF118.uint16_t[H]
#define RSCAN0TMDF118HL RSCAN0.TMDF118.uint8_t[HL]
#define RSCAN0TMDF118HH RSCAN0.TMDF118.uint8_t[HH]
#define RSCAN0TMID19 RSCAN0.TMID19.uint32_t
#define RSCAN0TMID19L RSCAN0.TMID19.uint16_t[L]
#define RSCAN0TMID19LL RSCAN0.TMID19.uint8_t[LL]
#define RSCAN0TMID19LH RSCAN0.TMID19.uint8_t[LH]
#define RSCAN0TMID19H RSCAN0.TMID19.uint16_t[H]
#define RSCAN0TMID19HL RSCAN0.TMID19.uint8_t[HL]
#define RSCAN0TMID19HH RSCAN0.TMID19.uint8_t[HH]
#define RSCAN0TMPTR19 RSCAN0.TMPTR19.uint32_t
#define RSCAN0TMPTR19H RSCAN0.TMPTR19.uint16_t[H]
#define RSCAN0TMPTR19HL RSCAN0.TMPTR19.uint8_t[HL]
#define RSCAN0TMPTR19HH RSCAN0.TMPTR19.uint8_t[HH]
#define RSCAN0TMDF019 RSCAN0.TMDF019.uint32_t
#define RSCAN0TMDF019L RSCAN0.TMDF019.uint16_t[L]
#define RSCAN0TMDF019LL RSCAN0.TMDF019.uint8_t[LL]
#define RSCAN0TMDF019LH RSCAN0.TMDF019.uint8_t[LH]
#define RSCAN0TMDF019H RSCAN0.TMDF019.uint16_t[H]
#define RSCAN0TMDF019HL RSCAN0.TMDF019.uint8_t[HL]
#define RSCAN0TMDF019HH RSCAN0.TMDF019.uint8_t[HH]
#define RSCAN0TMDF119 RSCAN0.TMDF119.uint32_t
#define RSCAN0TMDF119L RSCAN0.TMDF119.uint16_t[L]
#define RSCAN0TMDF119LL RSCAN0.TMDF119.uint8_t[LL]
#define RSCAN0TMDF119LH RSCAN0.TMDF119.uint8_t[LH]
#define RSCAN0TMDF119H RSCAN0.TMDF119.uint16_t[H]
#define RSCAN0TMDF119HL RSCAN0.TMDF119.uint8_t[HL]
#define RSCAN0TMDF119HH RSCAN0.TMDF119.uint8_t[HH]
#define RSCAN0TMID20 RSCAN0.TMID20.uint32_t
#define RSCAN0TMID20L RSCAN0.TMID20.uint16_t[L]
#define RSCAN0TMID20LL RSCAN0.TMID20.uint8_t[LL]
#define RSCAN0TMID20LH RSCAN0.TMID20.uint8_t[LH]
#define RSCAN0TMID20H RSCAN0.TMID20.uint16_t[H]
#define RSCAN0TMID20HL RSCAN0.TMID20.uint8_t[HL]
#define RSCAN0TMID20HH RSCAN0.TMID20.uint8_t[HH]
#define RSCAN0TMPTR20 RSCAN0.TMPTR20.uint32_t
#define RSCAN0TMPTR20H RSCAN0.TMPTR20.uint16_t[H]
#define RSCAN0TMPTR20HL RSCAN0.TMPTR20.uint8_t[HL]
#define RSCAN0TMPTR20HH RSCAN0.TMPTR20.uint8_t[HH]
#define RSCAN0TMDF020 RSCAN0.TMDF020.uint32_t
#define RSCAN0TMDF020L RSCAN0.TMDF020.uint16_t[L]
#define RSCAN0TMDF020LL RSCAN0.TMDF020.uint8_t[LL]
#define RSCAN0TMDF020LH RSCAN0.TMDF020.uint8_t[LH]
#define RSCAN0TMDF020H RSCAN0.TMDF020.uint16_t[H]
#define RSCAN0TMDF020HL RSCAN0.TMDF020.uint8_t[HL]
#define RSCAN0TMDF020HH RSCAN0.TMDF020.uint8_t[HH]
#define RSCAN0TMDF120 RSCAN0.TMDF120.uint32_t
#define RSCAN0TMDF120L RSCAN0.TMDF120.uint16_t[L]
#define RSCAN0TMDF120LL RSCAN0.TMDF120.uint8_t[LL]
#define RSCAN0TMDF120LH RSCAN0.TMDF120.uint8_t[LH]
#define RSCAN0TMDF120H RSCAN0.TMDF120.uint16_t[H]
#define RSCAN0TMDF120HL RSCAN0.TMDF120.uint8_t[HL]
#define RSCAN0TMDF120HH RSCAN0.TMDF120.uint8_t[HH]
#define RSCAN0TMID21 RSCAN0.TMID21.uint32_t
#define RSCAN0TMID21L RSCAN0.TMID21.uint16_t[L]
#define RSCAN0TMID21LL RSCAN0.TMID21.uint8_t[LL]
#define RSCAN0TMID21LH RSCAN0.TMID21.uint8_t[LH]
#define RSCAN0TMID21H RSCAN0.TMID21.uint16_t[H]
#define RSCAN0TMID21HL RSCAN0.TMID21.uint8_t[HL]
#define RSCAN0TMID21HH RSCAN0.TMID21.uint8_t[HH]
#define RSCAN0TMPTR21 RSCAN0.TMPTR21.uint32_t
#define RSCAN0TMPTR21H RSCAN0.TMPTR21.uint16_t[H]
#define RSCAN0TMPTR21HL RSCAN0.TMPTR21.uint8_t[HL]
#define RSCAN0TMPTR21HH RSCAN0.TMPTR21.uint8_t[HH]
#define RSCAN0TMDF021 RSCAN0.TMDF021.uint32_t
#define RSCAN0TMDF021L RSCAN0.TMDF021.uint16_t[L]
#define RSCAN0TMDF021LL RSCAN0.TMDF021.uint8_t[LL]
#define RSCAN0TMDF021LH RSCAN0.TMDF021.uint8_t[LH]
#define RSCAN0TMDF021H RSCAN0.TMDF021.uint16_t[H]
#define RSCAN0TMDF021HL RSCAN0.TMDF021.uint8_t[HL]
#define RSCAN0TMDF021HH RSCAN0.TMDF021.uint8_t[HH]
#define RSCAN0TMDF121 RSCAN0.TMDF121.uint32_t
#define RSCAN0TMDF121L RSCAN0.TMDF121.uint16_t[L]
#define RSCAN0TMDF121LL RSCAN0.TMDF121.uint8_t[LL]
#define RSCAN0TMDF121LH RSCAN0.TMDF121.uint8_t[LH]
#define RSCAN0TMDF121H RSCAN0.TMDF121.uint16_t[H]
#define RSCAN0TMDF121HL RSCAN0.TMDF121.uint8_t[HL]
#define RSCAN0TMDF121HH RSCAN0.TMDF121.uint8_t[HH]
#define RSCAN0TMID22 RSCAN0.TMID22.uint32_t
#define RSCAN0TMID22L RSCAN0.TMID22.uint16_t[L]
#define RSCAN0TMID22LL RSCAN0.TMID22.uint8_t[LL]
#define RSCAN0TMID22LH RSCAN0.TMID22.uint8_t[LH]
#define RSCAN0TMID22H RSCAN0.TMID22.uint16_t[H]
#define RSCAN0TMID22HL RSCAN0.TMID22.uint8_t[HL]
#define RSCAN0TMID22HH RSCAN0.TMID22.uint8_t[HH]
#define RSCAN0TMPTR22 RSCAN0.TMPTR22.uint32_t
#define RSCAN0TMPTR22H RSCAN0.TMPTR22.uint16_t[H]
#define RSCAN0TMPTR22HL RSCAN0.TMPTR22.uint8_t[HL]
#define RSCAN0TMPTR22HH RSCAN0.TMPTR22.uint8_t[HH]
#define RSCAN0TMDF022 RSCAN0.TMDF022.uint32_t
#define RSCAN0TMDF022L RSCAN0.TMDF022.uint16_t[L]
#define RSCAN0TMDF022LL RSCAN0.TMDF022.uint8_t[LL]
#define RSCAN0TMDF022LH RSCAN0.TMDF022.uint8_t[LH]
#define RSCAN0TMDF022H RSCAN0.TMDF022.uint16_t[H]
#define RSCAN0TMDF022HL RSCAN0.TMDF022.uint8_t[HL]
#define RSCAN0TMDF022HH RSCAN0.TMDF022.uint8_t[HH]
#define RSCAN0TMDF122 RSCAN0.TMDF122.uint32_t
#define RSCAN0TMDF122L RSCAN0.TMDF122.uint16_t[L]
#define RSCAN0TMDF122LL RSCAN0.TMDF122.uint8_t[LL]
#define RSCAN0TMDF122LH RSCAN0.TMDF122.uint8_t[LH]
#define RSCAN0TMDF122H RSCAN0.TMDF122.uint16_t[H]
#define RSCAN0TMDF122HL RSCAN0.TMDF122.uint8_t[HL]
#define RSCAN0TMDF122HH RSCAN0.TMDF122.uint8_t[HH]
#define RSCAN0TMID23 RSCAN0.TMID23.uint32_t
#define RSCAN0TMID23L RSCAN0.TMID23.uint16_t[L]
#define RSCAN0TMID23LL RSCAN0.TMID23.uint8_t[LL]
#define RSCAN0TMID23LH RSCAN0.TMID23.uint8_t[LH]
#define RSCAN0TMID23H RSCAN0.TMID23.uint16_t[H]
#define RSCAN0TMID23HL RSCAN0.TMID23.uint8_t[HL]
#define RSCAN0TMID23HH RSCAN0.TMID23.uint8_t[HH]
#define RSCAN0TMPTR23 RSCAN0.TMPTR23.uint32_t
#define RSCAN0TMPTR23H RSCAN0.TMPTR23.uint16_t[H]
#define RSCAN0TMPTR23HL RSCAN0.TMPTR23.uint8_t[HL]
#define RSCAN0TMPTR23HH RSCAN0.TMPTR23.uint8_t[HH]
#define RSCAN0TMDF023 RSCAN0.TMDF023.uint32_t
#define RSCAN0TMDF023L RSCAN0.TMDF023.uint16_t[L]
#define RSCAN0TMDF023LL RSCAN0.TMDF023.uint8_t[LL]
#define RSCAN0TMDF023LH RSCAN0.TMDF023.uint8_t[LH]
#define RSCAN0TMDF023H RSCAN0.TMDF023.uint16_t[H]
#define RSCAN0TMDF023HL RSCAN0.TMDF023.uint8_t[HL]
#define RSCAN0TMDF023HH RSCAN0.TMDF023.uint8_t[HH]
#define RSCAN0TMDF123 RSCAN0.TMDF123.uint32_t
#define RSCAN0TMDF123L RSCAN0.TMDF123.uint16_t[L]
#define RSCAN0TMDF123LL RSCAN0.TMDF123.uint8_t[LL]
#define RSCAN0TMDF123LH RSCAN0.TMDF123.uint8_t[LH]
#define RSCAN0TMDF123H RSCAN0.TMDF123.uint16_t[H]
#define RSCAN0TMDF123HL RSCAN0.TMDF123.uint8_t[HL]
#define RSCAN0TMDF123HH RSCAN0.TMDF123.uint8_t[HH]
#define RSCAN0TMID24 RSCAN0.TMID24.uint32_t
#define RSCAN0TMID24L RSCAN0.TMID24.uint16_t[L]
#define RSCAN0TMID24LL RSCAN0.TMID24.uint8_t[LL]
#define RSCAN0TMID24LH RSCAN0.TMID24.uint8_t[LH]
#define RSCAN0TMID24H RSCAN0.TMID24.uint16_t[H]
#define RSCAN0TMID24HL RSCAN0.TMID24.uint8_t[HL]
#define RSCAN0TMID24HH RSCAN0.TMID24.uint8_t[HH]
#define RSCAN0TMPTR24 RSCAN0.TMPTR24.uint32_t
#define RSCAN0TMPTR24H RSCAN0.TMPTR24.uint16_t[H]
#define RSCAN0TMPTR24HL RSCAN0.TMPTR24.uint8_t[HL]
#define RSCAN0TMPTR24HH RSCAN0.TMPTR24.uint8_t[HH]
#define RSCAN0TMDF024 RSCAN0.TMDF024.uint32_t
#define RSCAN0TMDF024L RSCAN0.TMDF024.uint16_t[L]
#define RSCAN0TMDF024LL RSCAN0.TMDF024.uint8_t[LL]
#define RSCAN0TMDF024LH RSCAN0.TMDF024.uint8_t[LH]
#define RSCAN0TMDF024H RSCAN0.TMDF024.uint16_t[H]
#define RSCAN0TMDF024HL RSCAN0.TMDF024.uint8_t[HL]
#define RSCAN0TMDF024HH RSCAN0.TMDF024.uint8_t[HH]
#define RSCAN0TMDF124 RSCAN0.TMDF124.uint32_t
#define RSCAN0TMDF124L RSCAN0.TMDF124.uint16_t[L]
#define RSCAN0TMDF124LL RSCAN0.TMDF124.uint8_t[LL]
#define RSCAN0TMDF124LH RSCAN0.TMDF124.uint8_t[LH]
#define RSCAN0TMDF124H RSCAN0.TMDF124.uint16_t[H]
#define RSCAN0TMDF124HL RSCAN0.TMDF124.uint8_t[HL]
#define RSCAN0TMDF124HH RSCAN0.TMDF124.uint8_t[HH]
#define RSCAN0TMID25 RSCAN0.TMID25.uint32_t
#define RSCAN0TMID25L RSCAN0.TMID25.uint16_t[L]
#define RSCAN0TMID25LL RSCAN0.TMID25.uint8_t[LL]
#define RSCAN0TMID25LH RSCAN0.TMID25.uint8_t[LH]
#define RSCAN0TMID25H RSCAN0.TMID25.uint16_t[H]
#define RSCAN0TMID25HL RSCAN0.TMID25.uint8_t[HL]
#define RSCAN0TMID25HH RSCAN0.TMID25.uint8_t[HH]
#define RSCAN0TMPTR25 RSCAN0.TMPTR25.uint32_t
#define RSCAN0TMPTR25H RSCAN0.TMPTR25.uint16_t[H]
#define RSCAN0TMPTR25HL RSCAN0.TMPTR25.uint8_t[HL]
#define RSCAN0TMPTR25HH RSCAN0.TMPTR25.uint8_t[HH]
#define RSCAN0TMDF025 RSCAN0.TMDF025.uint32_t
#define RSCAN0TMDF025L RSCAN0.TMDF025.uint16_t[L]
#define RSCAN0TMDF025LL RSCAN0.TMDF025.uint8_t[LL]
#define RSCAN0TMDF025LH RSCAN0.TMDF025.uint8_t[LH]
#define RSCAN0TMDF025H RSCAN0.TMDF025.uint16_t[H]
#define RSCAN0TMDF025HL RSCAN0.TMDF025.uint8_t[HL]
#define RSCAN0TMDF025HH RSCAN0.TMDF025.uint8_t[HH]
#define RSCAN0TMDF125 RSCAN0.TMDF125.uint32_t
#define RSCAN0TMDF125L RSCAN0.TMDF125.uint16_t[L]
#define RSCAN0TMDF125LL RSCAN0.TMDF125.uint8_t[LL]
#define RSCAN0TMDF125LH RSCAN0.TMDF125.uint8_t[LH]
#define RSCAN0TMDF125H RSCAN0.TMDF125.uint16_t[H]
#define RSCAN0TMDF125HL RSCAN0.TMDF125.uint8_t[HL]
#define RSCAN0TMDF125HH RSCAN0.TMDF125.uint8_t[HH]
#define RSCAN0TMID26 RSCAN0.TMID26.uint32_t
#define RSCAN0TMID26L RSCAN0.TMID26.uint16_t[L]
#define RSCAN0TMID26LL RSCAN0.TMID26.uint8_t[LL]
#define RSCAN0TMID26LH RSCAN0.TMID26.uint8_t[LH]
#define RSCAN0TMID26H RSCAN0.TMID26.uint16_t[H]
#define RSCAN0TMID26HL RSCAN0.TMID26.uint8_t[HL]
#define RSCAN0TMID26HH RSCAN0.TMID26.uint8_t[HH]
#define RSCAN0TMPTR26 RSCAN0.TMPTR26.uint32_t
#define RSCAN0TMPTR26H RSCAN0.TMPTR26.uint16_t[H]
#define RSCAN0TMPTR26HL RSCAN0.TMPTR26.uint8_t[HL]
#define RSCAN0TMPTR26HH RSCAN0.TMPTR26.uint8_t[HH]
#define RSCAN0TMDF026 RSCAN0.TMDF026.uint32_t
#define RSCAN0TMDF026L RSCAN0.TMDF026.uint16_t[L]
#define RSCAN0TMDF026LL RSCAN0.TMDF026.uint8_t[LL]
#define RSCAN0TMDF026LH RSCAN0.TMDF026.uint8_t[LH]
#define RSCAN0TMDF026H RSCAN0.TMDF026.uint16_t[H]
#define RSCAN0TMDF026HL RSCAN0.TMDF026.uint8_t[HL]
#define RSCAN0TMDF026HH RSCAN0.TMDF026.uint8_t[HH]
#define RSCAN0TMDF126 RSCAN0.TMDF126.uint32_t
#define RSCAN0TMDF126L RSCAN0.TMDF126.uint16_t[L]
#define RSCAN0TMDF126LL RSCAN0.TMDF126.uint8_t[LL]
#define RSCAN0TMDF126LH RSCAN0.TMDF126.uint8_t[LH]
#define RSCAN0TMDF126H RSCAN0.TMDF126.uint16_t[H]
#define RSCAN0TMDF126HL RSCAN0.TMDF126.uint8_t[HL]
#define RSCAN0TMDF126HH RSCAN0.TMDF126.uint8_t[HH]
#define RSCAN0TMID27 RSCAN0.TMID27.uint32_t
#define RSCAN0TMID27L RSCAN0.TMID27.uint16_t[L]
#define RSCAN0TMID27LL RSCAN0.TMID27.uint8_t[LL]
#define RSCAN0TMID27LH RSCAN0.TMID27.uint8_t[LH]
#define RSCAN0TMID27H RSCAN0.TMID27.uint16_t[H]
#define RSCAN0TMID27HL RSCAN0.TMID27.uint8_t[HL]
#define RSCAN0TMID27HH RSCAN0.TMID27.uint8_t[HH]
#define RSCAN0TMPTR27 RSCAN0.TMPTR27.uint32_t
#define RSCAN0TMPTR27H RSCAN0.TMPTR27.uint16_t[H]
#define RSCAN0TMPTR27HL RSCAN0.TMPTR27.uint8_t[HL]
#define RSCAN0TMPTR27HH RSCAN0.TMPTR27.uint8_t[HH]
#define RSCAN0TMDF027 RSCAN0.TMDF027.uint32_t
#define RSCAN0TMDF027L RSCAN0.TMDF027.uint16_t[L]
#define RSCAN0TMDF027LL RSCAN0.TMDF027.uint8_t[LL]
#define RSCAN0TMDF027LH RSCAN0.TMDF027.uint8_t[LH]
#define RSCAN0TMDF027H RSCAN0.TMDF027.uint16_t[H]
#define RSCAN0TMDF027HL RSCAN0.TMDF027.uint8_t[HL]
#define RSCAN0TMDF027HH RSCAN0.TMDF027.uint8_t[HH]
#define RSCAN0TMDF127 RSCAN0.TMDF127.uint32_t
#define RSCAN0TMDF127L RSCAN0.TMDF127.uint16_t[L]
#define RSCAN0TMDF127LL RSCAN0.TMDF127.uint8_t[LL]
#define RSCAN0TMDF127LH RSCAN0.TMDF127.uint8_t[LH]
#define RSCAN0TMDF127H RSCAN0.TMDF127.uint16_t[H]
#define RSCAN0TMDF127HL RSCAN0.TMDF127.uint8_t[HL]
#define RSCAN0TMDF127HH RSCAN0.TMDF127.uint8_t[HH]
#define RSCAN0TMID28 RSCAN0.TMID28.uint32_t
#define RSCAN0TMID28L RSCAN0.TMID28.uint16_t[L]
#define RSCAN0TMID28LL RSCAN0.TMID28.uint8_t[LL]
#define RSCAN0TMID28LH RSCAN0.TMID28.uint8_t[LH]
#define RSCAN0TMID28H RSCAN0.TMID28.uint16_t[H]
#define RSCAN0TMID28HL RSCAN0.TMID28.uint8_t[HL]
#define RSCAN0TMID28HH RSCAN0.TMID28.uint8_t[HH]
#define RSCAN0TMPTR28 RSCAN0.TMPTR28.uint32_t
#define RSCAN0TMPTR28H RSCAN0.TMPTR28.uint16_t[H]
#define RSCAN0TMPTR28HL RSCAN0.TMPTR28.uint8_t[HL]
#define RSCAN0TMPTR28HH RSCAN0.TMPTR28.uint8_t[HH]
#define RSCAN0TMDF028 RSCAN0.TMDF028.uint32_t
#define RSCAN0TMDF028L RSCAN0.TMDF028.uint16_t[L]
#define RSCAN0TMDF028LL RSCAN0.TMDF028.uint8_t[LL]
#define RSCAN0TMDF028LH RSCAN0.TMDF028.uint8_t[LH]
#define RSCAN0TMDF028H RSCAN0.TMDF028.uint16_t[H]
#define RSCAN0TMDF028HL RSCAN0.TMDF028.uint8_t[HL]
#define RSCAN0TMDF028HH RSCAN0.TMDF028.uint8_t[HH]
#define RSCAN0TMDF128 RSCAN0.TMDF128.uint32_t
#define RSCAN0TMDF128L RSCAN0.TMDF128.uint16_t[L]
#define RSCAN0TMDF128LL RSCAN0.TMDF128.uint8_t[LL]
#define RSCAN0TMDF128LH RSCAN0.TMDF128.uint8_t[LH]
#define RSCAN0TMDF128H RSCAN0.TMDF128.uint16_t[H]
#define RSCAN0TMDF128HL RSCAN0.TMDF128.uint8_t[HL]
#define RSCAN0TMDF128HH RSCAN0.TMDF128.uint8_t[HH]
#define RSCAN0TMID29 RSCAN0.TMID29.uint32_t
#define RSCAN0TMID29L RSCAN0.TMID29.uint16_t[L]
#define RSCAN0TMID29LL RSCAN0.TMID29.uint8_t[LL]
#define RSCAN0TMID29LH RSCAN0.TMID29.uint8_t[LH]
#define RSCAN0TMID29H RSCAN0.TMID29.uint16_t[H]
#define RSCAN0TMID29HL RSCAN0.TMID29.uint8_t[HL]
#define RSCAN0TMID29HH RSCAN0.TMID29.uint8_t[HH]
#define RSCAN0TMPTR29 RSCAN0.TMPTR29.uint32_t
#define RSCAN0TMPTR29H RSCAN0.TMPTR29.uint16_t[H]
#define RSCAN0TMPTR29HL RSCAN0.TMPTR29.uint8_t[HL]
#define RSCAN0TMPTR29HH RSCAN0.TMPTR29.uint8_t[HH]
#define RSCAN0TMDF029 RSCAN0.TMDF029.uint32_t
#define RSCAN0TMDF029L RSCAN0.TMDF029.uint16_t[L]
#define RSCAN0TMDF029LL RSCAN0.TMDF029.uint8_t[LL]
#define RSCAN0TMDF029LH RSCAN0.TMDF029.uint8_t[LH]
#define RSCAN0TMDF029H RSCAN0.TMDF029.uint16_t[H]
#define RSCAN0TMDF029HL RSCAN0.TMDF029.uint8_t[HL]
#define RSCAN0TMDF029HH RSCAN0.TMDF029.uint8_t[HH]
#define RSCAN0TMDF129 RSCAN0.TMDF129.uint32_t
#define RSCAN0TMDF129L RSCAN0.TMDF129.uint16_t[L]
#define RSCAN0TMDF129LL RSCAN0.TMDF129.uint8_t[LL]
#define RSCAN0TMDF129LH RSCAN0.TMDF129.uint8_t[LH]
#define RSCAN0TMDF129H RSCAN0.TMDF129.uint16_t[H]
#define RSCAN0TMDF129HL RSCAN0.TMDF129.uint8_t[HL]
#define RSCAN0TMDF129HH RSCAN0.TMDF129.uint8_t[HH]
#define RSCAN0TMID30 RSCAN0.TMID30.uint32_t
#define RSCAN0TMID30L RSCAN0.TMID30.uint16_t[L]
#define RSCAN0TMID30LL RSCAN0.TMID30.uint8_t[LL]
#define RSCAN0TMID30LH RSCAN0.TMID30.uint8_t[LH]
#define RSCAN0TMID30H RSCAN0.TMID30.uint16_t[H]
#define RSCAN0TMID30HL RSCAN0.TMID30.uint8_t[HL]
#define RSCAN0TMID30HH RSCAN0.TMID30.uint8_t[HH]
#define RSCAN0TMPTR30 RSCAN0.TMPTR30.uint32_t
#define RSCAN0TMPTR30H RSCAN0.TMPTR30.uint16_t[H]
#define RSCAN0TMPTR30HL RSCAN0.TMPTR30.uint8_t[HL]
#define RSCAN0TMPTR30HH RSCAN0.TMPTR30.uint8_t[HH]
#define RSCAN0TMDF030 RSCAN0.TMDF030.uint32_t
#define RSCAN0TMDF030L RSCAN0.TMDF030.uint16_t[L]
#define RSCAN0TMDF030LL RSCAN0.TMDF030.uint8_t[LL]
#define RSCAN0TMDF030LH RSCAN0.TMDF030.uint8_t[LH]
#define RSCAN0TMDF030H RSCAN0.TMDF030.uint16_t[H]
#define RSCAN0TMDF030HL RSCAN0.TMDF030.uint8_t[HL]
#define RSCAN0TMDF030HH RSCAN0.TMDF030.uint8_t[HH]
#define RSCAN0TMDF130 RSCAN0.TMDF130.uint32_t
#define RSCAN0TMDF130L RSCAN0.TMDF130.uint16_t[L]
#define RSCAN0TMDF130LL RSCAN0.TMDF130.uint8_t[LL]
#define RSCAN0TMDF130LH RSCAN0.TMDF130.uint8_t[LH]
#define RSCAN0TMDF130H RSCAN0.TMDF130.uint16_t[H]
#define RSCAN0TMDF130HL RSCAN0.TMDF130.uint8_t[HL]
#define RSCAN0TMDF130HH RSCAN0.TMDF130.uint8_t[HH]
#define RSCAN0TMID31 RSCAN0.TMID31.uint32_t
#define RSCAN0TMID31L RSCAN0.TMID31.uint16_t[L]
#define RSCAN0TMID31LL RSCAN0.TMID31.uint8_t[LL]
#define RSCAN0TMID31LH RSCAN0.TMID31.uint8_t[LH]
#define RSCAN0TMID31H RSCAN0.TMID31.uint16_t[H]
#define RSCAN0TMID31HL RSCAN0.TMID31.uint8_t[HL]
#define RSCAN0TMID31HH RSCAN0.TMID31.uint8_t[HH]
#define RSCAN0TMPTR31 RSCAN0.TMPTR31.uint32_t
#define RSCAN0TMPTR31H RSCAN0.TMPTR31.uint16_t[H]
#define RSCAN0TMPTR31HL RSCAN0.TMPTR31.uint8_t[HL]
#define RSCAN0TMPTR31HH RSCAN0.TMPTR31.uint8_t[HH]
#define RSCAN0TMDF031 RSCAN0.TMDF031.uint32_t
#define RSCAN0TMDF031L RSCAN0.TMDF031.uint16_t[L]
#define RSCAN0TMDF031LL RSCAN0.TMDF031.uint8_t[LL]
#define RSCAN0TMDF031LH RSCAN0.TMDF031.uint8_t[LH]
#define RSCAN0TMDF031H RSCAN0.TMDF031.uint16_t[H]
#define RSCAN0TMDF031HL RSCAN0.TMDF031.uint8_t[HL]
#define RSCAN0TMDF031HH RSCAN0.TMDF031.uint8_t[HH]
#define RSCAN0TMDF131 RSCAN0.TMDF131.uint32_t
#define RSCAN0TMDF131L RSCAN0.TMDF131.uint16_t[L]
#define RSCAN0TMDF131LL RSCAN0.TMDF131.uint8_t[LL]
#define RSCAN0TMDF131LH RSCAN0.TMDF131.uint8_t[LH]
#define RSCAN0TMDF131H RSCAN0.TMDF131.uint16_t[H]
#define RSCAN0TMDF131HL RSCAN0.TMDF131.uint8_t[HL]
#define RSCAN0TMDF131HH RSCAN0.TMDF131.uint8_t[HH]
#define RSCAN0TMID32 RSCAN0.TMID32.uint32_t
#define RSCAN0TMID32L RSCAN0.TMID32.uint16_t[L]
#define RSCAN0TMID32LL RSCAN0.TMID32.uint8_t[LL]
#define RSCAN0TMID32LH RSCAN0.TMID32.uint8_t[LH]
#define RSCAN0TMID32H RSCAN0.TMID32.uint16_t[H]
#define RSCAN0TMID32HL RSCAN0.TMID32.uint8_t[HL]
#define RSCAN0TMID32HH RSCAN0.TMID32.uint8_t[HH]
#define RSCAN0TMPTR32 RSCAN0.TMPTR32.uint32_t
#define RSCAN0TMPTR32H RSCAN0.TMPTR32.uint16_t[H]
#define RSCAN0TMPTR32HL RSCAN0.TMPTR32.uint8_t[HL]
#define RSCAN0TMPTR32HH RSCAN0.TMPTR32.uint8_t[HH]
#define RSCAN0TMDF032 RSCAN0.TMDF032.uint32_t
#define RSCAN0TMDF032L RSCAN0.TMDF032.uint16_t[L]
#define RSCAN0TMDF032LL RSCAN0.TMDF032.uint8_t[LL]
#define RSCAN0TMDF032LH RSCAN0.TMDF032.uint8_t[LH]
#define RSCAN0TMDF032H RSCAN0.TMDF032.uint16_t[H]
#define RSCAN0TMDF032HL RSCAN0.TMDF032.uint8_t[HL]
#define RSCAN0TMDF032HH RSCAN0.TMDF032.uint8_t[HH]
#define RSCAN0TMDF132 RSCAN0.TMDF132.uint32_t
#define RSCAN0TMDF132L RSCAN0.TMDF132.uint16_t[L]
#define RSCAN0TMDF132LL RSCAN0.TMDF132.uint8_t[LL]
#define RSCAN0TMDF132LH RSCAN0.TMDF132.uint8_t[LH]
#define RSCAN0TMDF132H RSCAN0.TMDF132.uint16_t[H]
#define RSCAN0TMDF132HL RSCAN0.TMDF132.uint8_t[HL]
#define RSCAN0TMDF132HH RSCAN0.TMDF132.uint8_t[HH]
#define RSCAN0TMID33 RSCAN0.TMID33.uint32_t
#define RSCAN0TMID33L RSCAN0.TMID33.uint16_t[L]
#define RSCAN0TMID33LL RSCAN0.TMID33.uint8_t[LL]
#define RSCAN0TMID33LH RSCAN0.TMID33.uint8_t[LH]
#define RSCAN0TMID33H RSCAN0.TMID33.uint16_t[H]
#define RSCAN0TMID33HL RSCAN0.TMID33.uint8_t[HL]
#define RSCAN0TMID33HH RSCAN0.TMID33.uint8_t[HH]
#define RSCAN0TMPTR33 RSCAN0.TMPTR33.uint32_t
#define RSCAN0TMPTR33H RSCAN0.TMPTR33.uint16_t[H]
#define RSCAN0TMPTR33HL RSCAN0.TMPTR33.uint8_t[HL]
#define RSCAN0TMPTR33HH RSCAN0.TMPTR33.uint8_t[HH]
#define RSCAN0TMDF033 RSCAN0.TMDF033.uint32_t
#define RSCAN0TMDF033L RSCAN0.TMDF033.uint16_t[L]
#define RSCAN0TMDF033LL RSCAN0.TMDF033.uint8_t[LL]
#define RSCAN0TMDF033LH RSCAN0.TMDF033.uint8_t[LH]
#define RSCAN0TMDF033H RSCAN0.TMDF033.uint16_t[H]
#define RSCAN0TMDF033HL RSCAN0.TMDF033.uint8_t[HL]
#define RSCAN0TMDF033HH RSCAN0.TMDF033.uint8_t[HH]
#define RSCAN0TMDF133 RSCAN0.TMDF133.uint32_t
#define RSCAN0TMDF133L RSCAN0.TMDF133.uint16_t[L]
#define RSCAN0TMDF133LL RSCAN0.TMDF133.uint8_t[LL]
#define RSCAN0TMDF133LH RSCAN0.TMDF133.uint8_t[LH]
#define RSCAN0TMDF133H RSCAN0.TMDF133.uint16_t[H]
#define RSCAN0TMDF133HL RSCAN0.TMDF133.uint8_t[HL]
#define RSCAN0TMDF133HH RSCAN0.TMDF133.uint8_t[HH]
#define RSCAN0TMID34 RSCAN0.TMID34.uint32_t
#define RSCAN0TMID34L RSCAN0.TMID34.uint16_t[L]
#define RSCAN0TMID34LL RSCAN0.TMID34.uint8_t[LL]
#define RSCAN0TMID34LH RSCAN0.TMID34.uint8_t[LH]
#define RSCAN0TMID34H RSCAN0.TMID34.uint16_t[H]
#define RSCAN0TMID34HL RSCAN0.TMID34.uint8_t[HL]
#define RSCAN0TMID34HH RSCAN0.TMID34.uint8_t[HH]
#define RSCAN0TMPTR34 RSCAN0.TMPTR34.uint32_t
#define RSCAN0TMPTR34H RSCAN0.TMPTR34.uint16_t[H]
#define RSCAN0TMPTR34HL RSCAN0.TMPTR34.uint8_t[HL]
#define RSCAN0TMPTR34HH RSCAN0.TMPTR34.uint8_t[HH]
#define RSCAN0TMDF034 RSCAN0.TMDF034.uint32_t
#define RSCAN0TMDF034L RSCAN0.TMDF034.uint16_t[L]
#define RSCAN0TMDF034LL RSCAN0.TMDF034.uint8_t[LL]
#define RSCAN0TMDF034LH RSCAN0.TMDF034.uint8_t[LH]
#define RSCAN0TMDF034H RSCAN0.TMDF034.uint16_t[H]
#define RSCAN0TMDF034HL RSCAN0.TMDF034.uint8_t[HL]
#define RSCAN0TMDF034HH RSCAN0.TMDF034.uint8_t[HH]
#define RSCAN0TMDF134 RSCAN0.TMDF134.uint32_t
#define RSCAN0TMDF134L RSCAN0.TMDF134.uint16_t[L]
#define RSCAN0TMDF134LL RSCAN0.TMDF134.uint8_t[LL]
#define RSCAN0TMDF134LH RSCAN0.TMDF134.uint8_t[LH]
#define RSCAN0TMDF134H RSCAN0.TMDF134.uint16_t[H]
#define RSCAN0TMDF134HL RSCAN0.TMDF134.uint8_t[HL]
#define RSCAN0TMDF134HH RSCAN0.TMDF134.uint8_t[HH]
#define RSCAN0TMID35 RSCAN0.TMID35.uint32_t
#define RSCAN0TMID35L RSCAN0.TMID35.uint16_t[L]
#define RSCAN0TMID35LL RSCAN0.TMID35.uint8_t[LL]
#define RSCAN0TMID35LH RSCAN0.TMID35.uint8_t[LH]
#define RSCAN0TMID35H RSCAN0.TMID35.uint16_t[H]
#define RSCAN0TMID35HL RSCAN0.TMID35.uint8_t[HL]
#define RSCAN0TMID35HH RSCAN0.TMID35.uint8_t[HH]
#define RSCAN0TMPTR35 RSCAN0.TMPTR35.uint32_t
#define RSCAN0TMPTR35H RSCAN0.TMPTR35.uint16_t[H]
#define RSCAN0TMPTR35HL RSCAN0.TMPTR35.uint8_t[HL]
#define RSCAN0TMPTR35HH RSCAN0.TMPTR35.uint8_t[HH]
#define RSCAN0TMDF035 RSCAN0.TMDF035.uint32_t
#define RSCAN0TMDF035L RSCAN0.TMDF035.uint16_t[L]
#define RSCAN0TMDF035LL RSCAN0.TMDF035.uint8_t[LL]
#define RSCAN0TMDF035LH RSCAN0.TMDF035.uint8_t[LH]
#define RSCAN0TMDF035H RSCAN0.TMDF035.uint16_t[H]
#define RSCAN0TMDF035HL RSCAN0.TMDF035.uint8_t[HL]
#define RSCAN0TMDF035HH RSCAN0.TMDF035.uint8_t[HH]
#define RSCAN0TMDF135 RSCAN0.TMDF135.uint32_t
#define RSCAN0TMDF135L RSCAN0.TMDF135.uint16_t[L]
#define RSCAN0TMDF135LL RSCAN0.TMDF135.uint8_t[LL]
#define RSCAN0TMDF135LH RSCAN0.TMDF135.uint8_t[LH]
#define RSCAN0TMDF135H RSCAN0.TMDF135.uint16_t[H]
#define RSCAN0TMDF135HL RSCAN0.TMDF135.uint8_t[HL]
#define RSCAN0TMDF135HH RSCAN0.TMDF135.uint8_t[HH]
#define RSCAN0TMID36 RSCAN0.TMID36.uint32_t
#define RSCAN0TMID36L RSCAN0.TMID36.uint16_t[L]
#define RSCAN0TMID36LL RSCAN0.TMID36.uint8_t[LL]
#define RSCAN0TMID36LH RSCAN0.TMID36.uint8_t[LH]
#define RSCAN0TMID36H RSCAN0.TMID36.uint16_t[H]
#define RSCAN0TMID36HL RSCAN0.TMID36.uint8_t[HL]
#define RSCAN0TMID36HH RSCAN0.TMID36.uint8_t[HH]
#define RSCAN0TMPTR36 RSCAN0.TMPTR36.uint32_t
#define RSCAN0TMPTR36H RSCAN0.TMPTR36.uint16_t[H]
#define RSCAN0TMPTR36HL RSCAN0.TMPTR36.uint8_t[HL]
#define RSCAN0TMPTR36HH RSCAN0.TMPTR36.uint8_t[HH]
#define RSCAN0TMDF036 RSCAN0.TMDF036.uint32_t
#define RSCAN0TMDF036L RSCAN0.TMDF036.uint16_t[L]
#define RSCAN0TMDF036LL RSCAN0.TMDF036.uint8_t[LL]
#define RSCAN0TMDF036LH RSCAN0.TMDF036.uint8_t[LH]
#define RSCAN0TMDF036H RSCAN0.TMDF036.uint16_t[H]
#define RSCAN0TMDF036HL RSCAN0.TMDF036.uint8_t[HL]
#define RSCAN0TMDF036HH RSCAN0.TMDF036.uint8_t[HH]
#define RSCAN0TMDF136 RSCAN0.TMDF136.uint32_t
#define RSCAN0TMDF136L RSCAN0.TMDF136.uint16_t[L]
#define RSCAN0TMDF136LL RSCAN0.TMDF136.uint8_t[LL]
#define RSCAN0TMDF136LH RSCAN0.TMDF136.uint8_t[LH]
#define RSCAN0TMDF136H RSCAN0.TMDF136.uint16_t[H]
#define RSCAN0TMDF136HL RSCAN0.TMDF136.uint8_t[HL]
#define RSCAN0TMDF136HH RSCAN0.TMDF136.uint8_t[HH]
#define RSCAN0TMID37 RSCAN0.TMID37.uint32_t
#define RSCAN0TMID37L RSCAN0.TMID37.uint16_t[L]
#define RSCAN0TMID37LL RSCAN0.TMID37.uint8_t[LL]
#define RSCAN0TMID37LH RSCAN0.TMID37.uint8_t[LH]
#define RSCAN0TMID37H RSCAN0.TMID37.uint16_t[H]
#define RSCAN0TMID37HL RSCAN0.TMID37.uint8_t[HL]
#define RSCAN0TMID37HH RSCAN0.TMID37.uint8_t[HH]
#define RSCAN0TMPTR37 RSCAN0.TMPTR37.uint32_t
#define RSCAN0TMPTR37H RSCAN0.TMPTR37.uint16_t[H]
#define RSCAN0TMPTR37HL RSCAN0.TMPTR37.uint8_t[HL]
#define RSCAN0TMPTR37HH RSCAN0.TMPTR37.uint8_t[HH]
#define RSCAN0TMDF037 RSCAN0.TMDF037.uint32_t
#define RSCAN0TMDF037L RSCAN0.TMDF037.uint16_t[L]
#define RSCAN0TMDF037LL RSCAN0.TMDF037.uint8_t[LL]
#define RSCAN0TMDF037LH RSCAN0.TMDF037.uint8_t[LH]
#define RSCAN0TMDF037H RSCAN0.TMDF037.uint16_t[H]
#define RSCAN0TMDF037HL RSCAN0.TMDF037.uint8_t[HL]
#define RSCAN0TMDF037HH RSCAN0.TMDF037.uint8_t[HH]
#define RSCAN0TMDF137 RSCAN0.TMDF137.uint32_t
#define RSCAN0TMDF137L RSCAN0.TMDF137.uint16_t[L]
#define RSCAN0TMDF137LL RSCAN0.TMDF137.uint8_t[LL]
#define RSCAN0TMDF137LH RSCAN0.TMDF137.uint8_t[LH]
#define RSCAN0TMDF137H RSCAN0.TMDF137.uint16_t[H]
#define RSCAN0TMDF137HL RSCAN0.TMDF137.uint8_t[HL]
#define RSCAN0TMDF137HH RSCAN0.TMDF137.uint8_t[HH]
#define RSCAN0TMID38 RSCAN0.TMID38.uint32_t
#define RSCAN0TMID38L RSCAN0.TMID38.uint16_t[L]
#define RSCAN0TMID38LL RSCAN0.TMID38.uint8_t[LL]
#define RSCAN0TMID38LH RSCAN0.TMID38.uint8_t[LH]
#define RSCAN0TMID38H RSCAN0.TMID38.uint16_t[H]
#define RSCAN0TMID38HL RSCAN0.TMID38.uint8_t[HL]
#define RSCAN0TMID38HH RSCAN0.TMID38.uint8_t[HH]
#define RSCAN0TMPTR38 RSCAN0.TMPTR38.uint32_t
#define RSCAN0TMPTR38H RSCAN0.TMPTR38.uint16_t[H]
#define RSCAN0TMPTR38HL RSCAN0.TMPTR38.uint8_t[HL]
#define RSCAN0TMPTR38HH RSCAN0.TMPTR38.uint8_t[HH]
#define RSCAN0TMDF038 RSCAN0.TMDF038.uint32_t
#define RSCAN0TMDF038L RSCAN0.TMDF038.uint16_t[L]
#define RSCAN0TMDF038LL RSCAN0.TMDF038.uint8_t[LL]
#define RSCAN0TMDF038LH RSCAN0.TMDF038.uint8_t[LH]
#define RSCAN0TMDF038H RSCAN0.TMDF038.uint16_t[H]
#define RSCAN0TMDF038HL RSCAN0.TMDF038.uint8_t[HL]
#define RSCAN0TMDF038HH RSCAN0.TMDF038.uint8_t[HH]
#define RSCAN0TMDF138 RSCAN0.TMDF138.uint32_t
#define RSCAN0TMDF138L RSCAN0.TMDF138.uint16_t[L]
#define RSCAN0TMDF138LL RSCAN0.TMDF138.uint8_t[LL]
#define RSCAN0TMDF138LH RSCAN0.TMDF138.uint8_t[LH]
#define RSCAN0TMDF138H RSCAN0.TMDF138.uint16_t[H]
#define RSCAN0TMDF138HL RSCAN0.TMDF138.uint8_t[HL]
#define RSCAN0TMDF138HH RSCAN0.TMDF138.uint8_t[HH]
#define RSCAN0TMID39 RSCAN0.TMID39.uint32_t
#define RSCAN0TMID39L RSCAN0.TMID39.uint16_t[L]
#define RSCAN0TMID39LL RSCAN0.TMID39.uint8_t[LL]
#define RSCAN0TMID39LH RSCAN0.TMID39.uint8_t[LH]
#define RSCAN0TMID39H RSCAN0.TMID39.uint16_t[H]
#define RSCAN0TMID39HL RSCAN0.TMID39.uint8_t[HL]
#define RSCAN0TMID39HH RSCAN0.TMID39.uint8_t[HH]
#define RSCAN0TMPTR39 RSCAN0.TMPTR39.uint32_t
#define RSCAN0TMPTR39H RSCAN0.TMPTR39.uint16_t[H]
#define RSCAN0TMPTR39HL RSCAN0.TMPTR39.uint8_t[HL]
#define RSCAN0TMPTR39HH RSCAN0.TMPTR39.uint8_t[HH]
#define RSCAN0TMDF039 RSCAN0.TMDF039.uint32_t
#define RSCAN0TMDF039L RSCAN0.TMDF039.uint16_t[L]
#define RSCAN0TMDF039LL RSCAN0.TMDF039.uint8_t[LL]
#define RSCAN0TMDF039LH RSCAN0.TMDF039.uint8_t[LH]
#define RSCAN0TMDF039H RSCAN0.TMDF039.uint16_t[H]
#define RSCAN0TMDF039HL RSCAN0.TMDF039.uint8_t[HL]
#define RSCAN0TMDF039HH RSCAN0.TMDF039.uint8_t[HH]
#define RSCAN0TMDF139 RSCAN0.TMDF139.uint32_t
#define RSCAN0TMDF139L RSCAN0.TMDF139.uint16_t[L]
#define RSCAN0TMDF139LL RSCAN0.TMDF139.uint8_t[LL]
#define RSCAN0TMDF139LH RSCAN0.TMDF139.uint8_t[LH]
#define RSCAN0TMDF139H RSCAN0.TMDF139.uint16_t[H]
#define RSCAN0TMDF139HL RSCAN0.TMDF139.uint8_t[HL]
#define RSCAN0TMDF139HH RSCAN0.TMDF139.uint8_t[HH]
#define RSCAN0TMID40 RSCAN0.TMID40.uint32_t
#define RSCAN0TMID40L RSCAN0.TMID40.uint16_t[L]
#define RSCAN0TMID40LL RSCAN0.TMID40.uint8_t[LL]
#define RSCAN0TMID40LH RSCAN0.TMID40.uint8_t[LH]
#define RSCAN0TMID40H RSCAN0.TMID40.uint16_t[H]
#define RSCAN0TMID40HL RSCAN0.TMID40.uint8_t[HL]
#define RSCAN0TMID40HH RSCAN0.TMID40.uint8_t[HH]
#define RSCAN0TMPTR40 RSCAN0.TMPTR40.uint32_t
#define RSCAN0TMPTR40H RSCAN0.TMPTR40.uint16_t[H]
#define RSCAN0TMPTR40HL RSCAN0.TMPTR40.uint8_t[HL]
#define RSCAN0TMPTR40HH RSCAN0.TMPTR40.uint8_t[HH]
#define RSCAN0TMDF040 RSCAN0.TMDF040.uint32_t
#define RSCAN0TMDF040L RSCAN0.TMDF040.uint16_t[L]
#define RSCAN0TMDF040LL RSCAN0.TMDF040.uint8_t[LL]
#define RSCAN0TMDF040LH RSCAN0.TMDF040.uint8_t[LH]
#define RSCAN0TMDF040H RSCAN0.TMDF040.uint16_t[H]
#define RSCAN0TMDF040HL RSCAN0.TMDF040.uint8_t[HL]
#define RSCAN0TMDF040HH RSCAN0.TMDF040.uint8_t[HH]
#define RSCAN0TMDF140 RSCAN0.TMDF140.uint32_t
#define RSCAN0TMDF140L RSCAN0.TMDF140.uint16_t[L]
#define RSCAN0TMDF140LL RSCAN0.TMDF140.uint8_t[LL]
#define RSCAN0TMDF140LH RSCAN0.TMDF140.uint8_t[LH]
#define RSCAN0TMDF140H RSCAN0.TMDF140.uint16_t[H]
#define RSCAN0TMDF140HL RSCAN0.TMDF140.uint8_t[HL]
#define RSCAN0TMDF140HH RSCAN0.TMDF140.uint8_t[HH]
#define RSCAN0TMID41 RSCAN0.TMID41.uint32_t
#define RSCAN0TMID41L RSCAN0.TMID41.uint16_t[L]
#define RSCAN0TMID41LL RSCAN0.TMID41.uint8_t[LL]
#define RSCAN0TMID41LH RSCAN0.TMID41.uint8_t[LH]
#define RSCAN0TMID41H RSCAN0.TMID41.uint16_t[H]
#define RSCAN0TMID41HL RSCAN0.TMID41.uint8_t[HL]
#define RSCAN0TMID41HH RSCAN0.TMID41.uint8_t[HH]
#define RSCAN0TMPTR41 RSCAN0.TMPTR41.uint32_t
#define RSCAN0TMPTR41H RSCAN0.TMPTR41.uint16_t[H]
#define RSCAN0TMPTR41HL RSCAN0.TMPTR41.uint8_t[HL]
#define RSCAN0TMPTR41HH RSCAN0.TMPTR41.uint8_t[HH]
#define RSCAN0TMDF041 RSCAN0.TMDF041.uint32_t
#define RSCAN0TMDF041L RSCAN0.TMDF041.uint16_t[L]
#define RSCAN0TMDF041LL RSCAN0.TMDF041.uint8_t[LL]
#define RSCAN0TMDF041LH RSCAN0.TMDF041.uint8_t[LH]
#define RSCAN0TMDF041H RSCAN0.TMDF041.uint16_t[H]
#define RSCAN0TMDF041HL RSCAN0.TMDF041.uint8_t[HL]
#define RSCAN0TMDF041HH RSCAN0.TMDF041.uint8_t[HH]
#define RSCAN0TMDF141 RSCAN0.TMDF141.uint32_t
#define RSCAN0TMDF141L RSCAN0.TMDF141.uint16_t[L]
#define RSCAN0TMDF141LL RSCAN0.TMDF141.uint8_t[LL]
#define RSCAN0TMDF141LH RSCAN0.TMDF141.uint8_t[LH]
#define RSCAN0TMDF141H RSCAN0.TMDF141.uint16_t[H]
#define RSCAN0TMDF141HL RSCAN0.TMDF141.uint8_t[HL]
#define RSCAN0TMDF141HH RSCAN0.TMDF141.uint8_t[HH]
#define RSCAN0TMID42 RSCAN0.TMID42.uint32_t
#define RSCAN0TMID42L RSCAN0.TMID42.uint16_t[L]
#define RSCAN0TMID42LL RSCAN0.TMID42.uint8_t[LL]
#define RSCAN0TMID42LH RSCAN0.TMID42.uint8_t[LH]
#define RSCAN0TMID42H RSCAN0.TMID42.uint16_t[H]
#define RSCAN0TMID42HL RSCAN0.TMID42.uint8_t[HL]
#define RSCAN0TMID42HH RSCAN0.TMID42.uint8_t[HH]
#define RSCAN0TMPTR42 RSCAN0.TMPTR42.uint32_t
#define RSCAN0TMPTR42H RSCAN0.TMPTR42.uint16_t[H]
#define RSCAN0TMPTR42HL RSCAN0.TMPTR42.uint8_t[HL]
#define RSCAN0TMPTR42HH RSCAN0.TMPTR42.uint8_t[HH]
#define RSCAN0TMDF042 RSCAN0.TMDF042.uint32_t
#define RSCAN0TMDF042L RSCAN0.TMDF042.uint16_t[L]
#define RSCAN0TMDF042LL RSCAN0.TMDF042.uint8_t[LL]
#define RSCAN0TMDF042LH RSCAN0.TMDF042.uint8_t[LH]
#define RSCAN0TMDF042H RSCAN0.TMDF042.uint16_t[H]
#define RSCAN0TMDF042HL RSCAN0.TMDF042.uint8_t[HL]
#define RSCAN0TMDF042HH RSCAN0.TMDF042.uint8_t[HH]
#define RSCAN0TMDF142 RSCAN0.TMDF142.uint32_t
#define RSCAN0TMDF142L RSCAN0.TMDF142.uint16_t[L]
#define RSCAN0TMDF142LL RSCAN0.TMDF142.uint8_t[LL]
#define RSCAN0TMDF142LH RSCAN0.TMDF142.uint8_t[LH]
#define RSCAN0TMDF142H RSCAN0.TMDF142.uint16_t[H]
#define RSCAN0TMDF142HL RSCAN0.TMDF142.uint8_t[HL]
#define RSCAN0TMDF142HH RSCAN0.TMDF142.uint8_t[HH]
#define RSCAN0TMID43 RSCAN0.TMID43.uint32_t
#define RSCAN0TMID43L RSCAN0.TMID43.uint16_t[L]
#define RSCAN0TMID43LL RSCAN0.TMID43.uint8_t[LL]
#define RSCAN0TMID43LH RSCAN0.TMID43.uint8_t[LH]
#define RSCAN0TMID43H RSCAN0.TMID43.uint16_t[H]
#define RSCAN0TMID43HL RSCAN0.TMID43.uint8_t[HL]
#define RSCAN0TMID43HH RSCAN0.TMID43.uint8_t[HH]
#define RSCAN0TMPTR43 RSCAN0.TMPTR43.uint32_t
#define RSCAN0TMPTR43H RSCAN0.TMPTR43.uint16_t[H]
#define RSCAN0TMPTR43HL RSCAN0.TMPTR43.uint8_t[HL]
#define RSCAN0TMPTR43HH RSCAN0.TMPTR43.uint8_t[HH]
#define RSCAN0TMDF043 RSCAN0.TMDF043.uint32_t
#define RSCAN0TMDF043L RSCAN0.TMDF043.uint16_t[L]
#define RSCAN0TMDF043LL RSCAN0.TMDF043.uint8_t[LL]
#define RSCAN0TMDF043LH RSCAN0.TMDF043.uint8_t[LH]
#define RSCAN0TMDF043H RSCAN0.TMDF043.uint16_t[H]
#define RSCAN0TMDF043HL RSCAN0.TMDF043.uint8_t[HL]
#define RSCAN0TMDF043HH RSCAN0.TMDF043.uint8_t[HH]
#define RSCAN0TMDF143 RSCAN0.TMDF143.uint32_t
#define RSCAN0TMDF143L RSCAN0.TMDF143.uint16_t[L]
#define RSCAN0TMDF143LL RSCAN0.TMDF143.uint8_t[LL]
#define RSCAN0TMDF143LH RSCAN0.TMDF143.uint8_t[LH]
#define RSCAN0TMDF143H RSCAN0.TMDF143.uint16_t[H]
#define RSCAN0TMDF143HL RSCAN0.TMDF143.uint8_t[HL]
#define RSCAN0TMDF143HH RSCAN0.TMDF143.uint8_t[HH]
#define RSCAN0TMID44 RSCAN0.TMID44.uint32_t
#define RSCAN0TMID44L RSCAN0.TMID44.uint16_t[L]
#define RSCAN0TMID44LL RSCAN0.TMID44.uint8_t[LL]
#define RSCAN0TMID44LH RSCAN0.TMID44.uint8_t[LH]
#define RSCAN0TMID44H RSCAN0.TMID44.uint16_t[H]
#define RSCAN0TMID44HL RSCAN0.TMID44.uint8_t[HL]
#define RSCAN0TMID44HH RSCAN0.TMID44.uint8_t[HH]
#define RSCAN0TMPTR44 RSCAN0.TMPTR44.uint32_t
#define RSCAN0TMPTR44H RSCAN0.TMPTR44.uint16_t[H]
#define RSCAN0TMPTR44HL RSCAN0.TMPTR44.uint8_t[HL]
#define RSCAN0TMPTR44HH RSCAN0.TMPTR44.uint8_t[HH]
#define RSCAN0TMDF044 RSCAN0.TMDF044.uint32_t
#define RSCAN0TMDF044L RSCAN0.TMDF044.uint16_t[L]
#define RSCAN0TMDF044LL RSCAN0.TMDF044.uint8_t[LL]
#define RSCAN0TMDF044LH RSCAN0.TMDF044.uint8_t[LH]
#define RSCAN0TMDF044H RSCAN0.TMDF044.uint16_t[H]
#define RSCAN0TMDF044HL RSCAN0.TMDF044.uint8_t[HL]
#define RSCAN0TMDF044HH RSCAN0.TMDF044.uint8_t[HH]
#define RSCAN0TMDF144 RSCAN0.TMDF144.uint32_t
#define RSCAN0TMDF144L RSCAN0.TMDF144.uint16_t[L]
#define RSCAN0TMDF144LL RSCAN0.TMDF144.uint8_t[LL]
#define RSCAN0TMDF144LH RSCAN0.TMDF144.uint8_t[LH]
#define RSCAN0TMDF144H RSCAN0.TMDF144.uint16_t[H]
#define RSCAN0TMDF144HL RSCAN0.TMDF144.uint8_t[HL]
#define RSCAN0TMDF144HH RSCAN0.TMDF144.uint8_t[HH]
#define RSCAN0TMID45 RSCAN0.TMID45.uint32_t
#define RSCAN0TMID45L RSCAN0.TMID45.uint16_t[L]
#define RSCAN0TMID45LL RSCAN0.TMID45.uint8_t[LL]
#define RSCAN0TMID45LH RSCAN0.TMID45.uint8_t[LH]
#define RSCAN0TMID45H RSCAN0.TMID45.uint16_t[H]
#define RSCAN0TMID45HL RSCAN0.TMID45.uint8_t[HL]
#define RSCAN0TMID45HH RSCAN0.TMID45.uint8_t[HH]
#define RSCAN0TMPTR45 RSCAN0.TMPTR45.uint32_t
#define RSCAN0TMPTR45H RSCAN0.TMPTR45.uint16_t[H]
#define RSCAN0TMPTR45HL RSCAN0.TMPTR45.uint8_t[HL]
#define RSCAN0TMPTR45HH RSCAN0.TMPTR45.uint8_t[HH]
#define RSCAN0TMDF045 RSCAN0.TMDF045.uint32_t
#define RSCAN0TMDF045L RSCAN0.TMDF045.uint16_t[L]
#define RSCAN0TMDF045LL RSCAN0.TMDF045.uint8_t[LL]
#define RSCAN0TMDF045LH RSCAN0.TMDF045.uint8_t[LH]
#define RSCAN0TMDF045H RSCAN0.TMDF045.uint16_t[H]
#define RSCAN0TMDF045HL RSCAN0.TMDF045.uint8_t[HL]
#define RSCAN0TMDF045HH RSCAN0.TMDF045.uint8_t[HH]
#define RSCAN0TMDF145 RSCAN0.TMDF145.uint32_t
#define RSCAN0TMDF145L RSCAN0.TMDF145.uint16_t[L]
#define RSCAN0TMDF145LL RSCAN0.TMDF145.uint8_t[LL]
#define RSCAN0TMDF145LH RSCAN0.TMDF145.uint8_t[LH]
#define RSCAN0TMDF145H RSCAN0.TMDF145.uint16_t[H]
#define RSCAN0TMDF145HL RSCAN0.TMDF145.uint8_t[HL]
#define RSCAN0TMDF145HH RSCAN0.TMDF145.uint8_t[HH]
#define RSCAN0TMID46 RSCAN0.TMID46.uint32_t
#define RSCAN0TMID46L RSCAN0.TMID46.uint16_t[L]
#define RSCAN0TMID46LL RSCAN0.TMID46.uint8_t[LL]
#define RSCAN0TMID46LH RSCAN0.TMID46.uint8_t[LH]
#define RSCAN0TMID46H RSCAN0.TMID46.uint16_t[H]
#define RSCAN0TMID46HL RSCAN0.TMID46.uint8_t[HL]
#define RSCAN0TMID46HH RSCAN0.TMID46.uint8_t[HH]
#define RSCAN0TMPTR46 RSCAN0.TMPTR46.uint32_t
#define RSCAN0TMPTR46H RSCAN0.TMPTR46.uint16_t[H]
#define RSCAN0TMPTR46HL RSCAN0.TMPTR46.uint8_t[HL]
#define RSCAN0TMPTR46HH RSCAN0.TMPTR46.uint8_t[HH]
#define RSCAN0TMDF046 RSCAN0.TMDF046.uint32_t
#define RSCAN0TMDF046L RSCAN0.TMDF046.uint16_t[L]
#define RSCAN0TMDF046LL RSCAN0.TMDF046.uint8_t[LL]
#define RSCAN0TMDF046LH RSCAN0.TMDF046.uint8_t[LH]
#define RSCAN0TMDF046H RSCAN0.TMDF046.uint16_t[H]
#define RSCAN0TMDF046HL RSCAN0.TMDF046.uint8_t[HL]
#define RSCAN0TMDF046HH RSCAN0.TMDF046.uint8_t[HH]
#define RSCAN0TMDF146 RSCAN0.TMDF146.uint32_t
#define RSCAN0TMDF146L RSCAN0.TMDF146.uint16_t[L]
#define RSCAN0TMDF146LL RSCAN0.TMDF146.uint8_t[LL]
#define RSCAN0TMDF146LH RSCAN0.TMDF146.uint8_t[LH]
#define RSCAN0TMDF146H RSCAN0.TMDF146.uint16_t[H]
#define RSCAN0TMDF146HL RSCAN0.TMDF146.uint8_t[HL]
#define RSCAN0TMDF146HH RSCAN0.TMDF146.uint8_t[HH]
#define RSCAN0TMID47 RSCAN0.TMID47.uint32_t
#define RSCAN0TMID47L RSCAN0.TMID47.uint16_t[L]
#define RSCAN0TMID47LL RSCAN0.TMID47.uint8_t[LL]
#define RSCAN0TMID47LH RSCAN0.TMID47.uint8_t[LH]
#define RSCAN0TMID47H RSCAN0.TMID47.uint16_t[H]
#define RSCAN0TMID47HL RSCAN0.TMID47.uint8_t[HL]
#define RSCAN0TMID47HH RSCAN0.TMID47.uint8_t[HH]
#define RSCAN0TMPTR47 RSCAN0.TMPTR47.uint32_t
#define RSCAN0TMPTR47H RSCAN0.TMPTR47.uint16_t[H]
#define RSCAN0TMPTR47HL RSCAN0.TMPTR47.uint8_t[HL]
#define RSCAN0TMPTR47HH RSCAN0.TMPTR47.uint8_t[HH]
#define RSCAN0TMDF047 RSCAN0.TMDF047.uint32_t
#define RSCAN0TMDF047L RSCAN0.TMDF047.uint16_t[L]
#define RSCAN0TMDF047LL RSCAN0.TMDF047.uint8_t[LL]
#define RSCAN0TMDF047LH RSCAN0.TMDF047.uint8_t[LH]
#define RSCAN0TMDF047H RSCAN0.TMDF047.uint16_t[H]
#define RSCAN0TMDF047HL RSCAN0.TMDF047.uint8_t[HL]
#define RSCAN0TMDF047HH RSCAN0.TMDF047.uint8_t[HH]
#define RSCAN0TMDF147 RSCAN0.TMDF147.uint32_t
#define RSCAN0TMDF147L RSCAN0.TMDF147.uint16_t[L]
#define RSCAN0TMDF147LL RSCAN0.TMDF147.uint8_t[LL]
#define RSCAN0TMDF147LH RSCAN0.TMDF147.uint8_t[LH]
#define RSCAN0TMDF147H RSCAN0.TMDF147.uint16_t[H]
#define RSCAN0TMDF147HL RSCAN0.TMDF147.uint8_t[HL]
#define RSCAN0TMDF147HH RSCAN0.TMDF147.uint8_t[HH]
#define RSCAN0TMID48 RSCAN0.TMID48.uint32_t
#define RSCAN0TMID48L RSCAN0.TMID48.uint16_t[L]
#define RSCAN0TMID48LL RSCAN0.TMID48.uint8_t[LL]
#define RSCAN0TMID48LH RSCAN0.TMID48.uint8_t[LH]
#define RSCAN0TMID48H RSCAN0.TMID48.uint16_t[H]
#define RSCAN0TMID48HL RSCAN0.TMID48.uint8_t[HL]
#define RSCAN0TMID48HH RSCAN0.TMID48.uint8_t[HH]
#define RSCAN0TMPTR48 RSCAN0.TMPTR48.uint32_t
#define RSCAN0TMPTR48H RSCAN0.TMPTR48.uint16_t[H]
#define RSCAN0TMPTR48HL RSCAN0.TMPTR48.uint8_t[HL]
#define RSCAN0TMPTR48HH RSCAN0.TMPTR48.uint8_t[HH]
#define RSCAN0TMDF048 RSCAN0.TMDF048.uint32_t
#define RSCAN0TMDF048L RSCAN0.TMDF048.uint16_t[L]
#define RSCAN0TMDF048LL RSCAN0.TMDF048.uint8_t[LL]
#define RSCAN0TMDF048LH RSCAN0.TMDF048.uint8_t[LH]
#define RSCAN0TMDF048H RSCAN0.TMDF048.uint16_t[H]
#define RSCAN0TMDF048HL RSCAN0.TMDF048.uint8_t[HL]
#define RSCAN0TMDF048HH RSCAN0.TMDF048.uint8_t[HH]
#define RSCAN0TMDF148 RSCAN0.TMDF148.uint32_t
#define RSCAN0TMDF148L RSCAN0.TMDF148.uint16_t[L]
#define RSCAN0TMDF148LL RSCAN0.TMDF148.uint8_t[LL]
#define RSCAN0TMDF148LH RSCAN0.TMDF148.uint8_t[LH]
#define RSCAN0TMDF148H RSCAN0.TMDF148.uint16_t[H]
#define RSCAN0TMDF148HL RSCAN0.TMDF148.uint8_t[HL]
#define RSCAN0TMDF148HH RSCAN0.TMDF148.uint8_t[HH]
#define RSCAN0TMID49 RSCAN0.TMID49.uint32_t
#define RSCAN0TMID49L RSCAN0.TMID49.uint16_t[L]
#define RSCAN0TMID49LL RSCAN0.TMID49.uint8_t[LL]
#define RSCAN0TMID49LH RSCAN0.TMID49.uint8_t[LH]
#define RSCAN0TMID49H RSCAN0.TMID49.uint16_t[H]
#define RSCAN0TMID49HL RSCAN0.TMID49.uint8_t[HL]
#define RSCAN0TMID49HH RSCAN0.TMID49.uint8_t[HH]
#define RSCAN0TMPTR49 RSCAN0.TMPTR49.uint32_t
#define RSCAN0TMPTR49H RSCAN0.TMPTR49.uint16_t[H]
#define RSCAN0TMPTR49HL RSCAN0.TMPTR49.uint8_t[HL]
#define RSCAN0TMPTR49HH RSCAN0.TMPTR49.uint8_t[HH]
#define RSCAN0TMDF049 RSCAN0.TMDF049.uint32_t
#define RSCAN0TMDF049L RSCAN0.TMDF049.uint16_t[L]
#define RSCAN0TMDF049LL RSCAN0.TMDF049.uint8_t[LL]
#define RSCAN0TMDF049LH RSCAN0.TMDF049.uint8_t[LH]
#define RSCAN0TMDF049H RSCAN0.TMDF049.uint16_t[H]
#define RSCAN0TMDF049HL RSCAN0.TMDF049.uint8_t[HL]
#define RSCAN0TMDF049HH RSCAN0.TMDF049.uint8_t[HH]
#define RSCAN0TMDF149 RSCAN0.TMDF149.uint32_t
#define RSCAN0TMDF149L RSCAN0.TMDF149.uint16_t[L]
#define RSCAN0TMDF149LL RSCAN0.TMDF149.uint8_t[LL]
#define RSCAN0TMDF149LH RSCAN0.TMDF149.uint8_t[LH]
#define RSCAN0TMDF149H RSCAN0.TMDF149.uint16_t[H]
#define RSCAN0TMDF149HL RSCAN0.TMDF149.uint8_t[HL]
#define RSCAN0TMDF149HH RSCAN0.TMDF149.uint8_t[HH]
#define RSCAN0TMID50 RSCAN0.TMID50.uint32_t
#define RSCAN0TMID50L RSCAN0.TMID50.uint16_t[L]
#define RSCAN0TMID50LL RSCAN0.TMID50.uint8_t[LL]
#define RSCAN0TMID50LH RSCAN0.TMID50.uint8_t[LH]
#define RSCAN0TMID50H RSCAN0.TMID50.uint16_t[H]
#define RSCAN0TMID50HL RSCAN0.TMID50.uint8_t[HL]
#define RSCAN0TMID50HH RSCAN0.TMID50.uint8_t[HH]
#define RSCAN0TMPTR50 RSCAN0.TMPTR50.uint32_t
#define RSCAN0TMPTR50H RSCAN0.TMPTR50.uint16_t[H]
#define RSCAN0TMPTR50HL RSCAN0.TMPTR50.uint8_t[HL]
#define RSCAN0TMPTR50HH RSCAN0.TMPTR50.uint8_t[HH]
#define RSCAN0TMDF050 RSCAN0.TMDF050.uint32_t
#define RSCAN0TMDF050L RSCAN0.TMDF050.uint16_t[L]
#define RSCAN0TMDF050LL RSCAN0.TMDF050.uint8_t[LL]
#define RSCAN0TMDF050LH RSCAN0.TMDF050.uint8_t[LH]
#define RSCAN0TMDF050H RSCAN0.TMDF050.uint16_t[H]
#define RSCAN0TMDF050HL RSCAN0.TMDF050.uint8_t[HL]
#define RSCAN0TMDF050HH RSCAN0.TMDF050.uint8_t[HH]
#define RSCAN0TMDF150 RSCAN0.TMDF150.uint32_t
#define RSCAN0TMDF150L RSCAN0.TMDF150.uint16_t[L]
#define RSCAN0TMDF150LL RSCAN0.TMDF150.uint8_t[LL]
#define RSCAN0TMDF150LH RSCAN0.TMDF150.uint8_t[LH]
#define RSCAN0TMDF150H RSCAN0.TMDF150.uint16_t[H]
#define RSCAN0TMDF150HL RSCAN0.TMDF150.uint8_t[HL]
#define RSCAN0TMDF150HH RSCAN0.TMDF150.uint8_t[HH]
#define RSCAN0TMID51 RSCAN0.TMID51.uint32_t
#define RSCAN0TMID51L RSCAN0.TMID51.uint16_t[L]
#define RSCAN0TMID51LL RSCAN0.TMID51.uint8_t[LL]
#define RSCAN0TMID51LH RSCAN0.TMID51.uint8_t[LH]
#define RSCAN0TMID51H RSCAN0.TMID51.uint16_t[H]
#define RSCAN0TMID51HL RSCAN0.TMID51.uint8_t[HL]
#define RSCAN0TMID51HH RSCAN0.TMID51.uint8_t[HH]
#define RSCAN0TMPTR51 RSCAN0.TMPTR51.uint32_t
#define RSCAN0TMPTR51H RSCAN0.TMPTR51.uint16_t[H]
#define RSCAN0TMPTR51HL RSCAN0.TMPTR51.uint8_t[HL]
#define RSCAN0TMPTR51HH RSCAN0.TMPTR51.uint8_t[HH]
#define RSCAN0TMDF051 RSCAN0.TMDF051.uint32_t
#define RSCAN0TMDF051L RSCAN0.TMDF051.uint16_t[L]
#define RSCAN0TMDF051LL RSCAN0.TMDF051.uint8_t[LL]
#define RSCAN0TMDF051LH RSCAN0.TMDF051.uint8_t[LH]
#define RSCAN0TMDF051H RSCAN0.TMDF051.uint16_t[H]
#define RSCAN0TMDF051HL RSCAN0.TMDF051.uint8_t[HL]
#define RSCAN0TMDF051HH RSCAN0.TMDF051.uint8_t[HH]
#define RSCAN0TMDF151 RSCAN0.TMDF151.uint32_t
#define RSCAN0TMDF151L RSCAN0.TMDF151.uint16_t[L]
#define RSCAN0TMDF151LL RSCAN0.TMDF151.uint8_t[LL]
#define RSCAN0TMDF151LH RSCAN0.TMDF151.uint8_t[LH]
#define RSCAN0TMDF151H RSCAN0.TMDF151.uint16_t[H]
#define RSCAN0TMDF151HL RSCAN0.TMDF151.uint8_t[HL]
#define RSCAN0TMDF151HH RSCAN0.TMDF151.uint8_t[HH]
#define RSCAN0TMID52 RSCAN0.TMID52.uint32_t
#define RSCAN0TMID52L RSCAN0.TMID52.uint16_t[L]
#define RSCAN0TMID52LL RSCAN0.TMID52.uint8_t[LL]
#define RSCAN0TMID52LH RSCAN0.TMID52.uint8_t[LH]
#define RSCAN0TMID52H RSCAN0.TMID52.uint16_t[H]
#define RSCAN0TMID52HL RSCAN0.TMID52.uint8_t[HL]
#define RSCAN0TMID52HH RSCAN0.TMID52.uint8_t[HH]
#define RSCAN0TMPTR52 RSCAN0.TMPTR52.uint32_t
#define RSCAN0TMPTR52H RSCAN0.TMPTR52.uint16_t[H]
#define RSCAN0TMPTR52HL RSCAN0.TMPTR52.uint8_t[HL]
#define RSCAN0TMPTR52HH RSCAN0.TMPTR52.uint8_t[HH]
#define RSCAN0TMDF052 RSCAN0.TMDF052.uint32_t
#define RSCAN0TMDF052L RSCAN0.TMDF052.uint16_t[L]
#define RSCAN0TMDF052LL RSCAN0.TMDF052.uint8_t[LL]
#define RSCAN0TMDF052LH RSCAN0.TMDF052.uint8_t[LH]
#define RSCAN0TMDF052H RSCAN0.TMDF052.uint16_t[H]
#define RSCAN0TMDF052HL RSCAN0.TMDF052.uint8_t[HL]
#define RSCAN0TMDF052HH RSCAN0.TMDF052.uint8_t[HH]
#define RSCAN0TMDF152 RSCAN0.TMDF152.uint32_t
#define RSCAN0TMDF152L RSCAN0.TMDF152.uint16_t[L]
#define RSCAN0TMDF152LL RSCAN0.TMDF152.uint8_t[LL]
#define RSCAN0TMDF152LH RSCAN0.TMDF152.uint8_t[LH]
#define RSCAN0TMDF152H RSCAN0.TMDF152.uint16_t[H]
#define RSCAN0TMDF152HL RSCAN0.TMDF152.uint8_t[HL]
#define RSCAN0TMDF152HH RSCAN0.TMDF152.uint8_t[HH]
#define RSCAN0TMID53 RSCAN0.TMID53.uint32_t
#define RSCAN0TMID53L RSCAN0.TMID53.uint16_t[L]
#define RSCAN0TMID53LL RSCAN0.TMID53.uint8_t[LL]
#define RSCAN0TMID53LH RSCAN0.TMID53.uint8_t[LH]
#define RSCAN0TMID53H RSCAN0.TMID53.uint16_t[H]
#define RSCAN0TMID53HL RSCAN0.TMID53.uint8_t[HL]
#define RSCAN0TMID53HH RSCAN0.TMID53.uint8_t[HH]
#define RSCAN0TMPTR53 RSCAN0.TMPTR53.uint32_t
#define RSCAN0TMPTR53H RSCAN0.TMPTR53.uint16_t[H]
#define RSCAN0TMPTR53HL RSCAN0.TMPTR53.uint8_t[HL]
#define RSCAN0TMPTR53HH RSCAN0.TMPTR53.uint8_t[HH]
#define RSCAN0TMDF053 RSCAN0.TMDF053.uint32_t
#define RSCAN0TMDF053L RSCAN0.TMDF053.uint16_t[L]
#define RSCAN0TMDF053LL RSCAN0.TMDF053.uint8_t[LL]
#define RSCAN0TMDF053LH RSCAN0.TMDF053.uint8_t[LH]
#define RSCAN0TMDF053H RSCAN0.TMDF053.uint16_t[H]
#define RSCAN0TMDF053HL RSCAN0.TMDF053.uint8_t[HL]
#define RSCAN0TMDF053HH RSCAN0.TMDF053.uint8_t[HH]
#define RSCAN0TMDF153 RSCAN0.TMDF153.uint32_t
#define RSCAN0TMDF153L RSCAN0.TMDF153.uint16_t[L]
#define RSCAN0TMDF153LL RSCAN0.TMDF153.uint8_t[LL]
#define RSCAN0TMDF153LH RSCAN0.TMDF153.uint8_t[LH]
#define RSCAN0TMDF153H RSCAN0.TMDF153.uint16_t[H]
#define RSCAN0TMDF153HL RSCAN0.TMDF153.uint8_t[HL]
#define RSCAN0TMDF153HH RSCAN0.TMDF153.uint8_t[HH]
#define RSCAN0TMID54 RSCAN0.TMID54.uint32_t
#define RSCAN0TMID54L RSCAN0.TMID54.uint16_t[L]
#define RSCAN0TMID54LL RSCAN0.TMID54.uint8_t[LL]
#define RSCAN0TMID54LH RSCAN0.TMID54.uint8_t[LH]
#define RSCAN0TMID54H RSCAN0.TMID54.uint16_t[H]
#define RSCAN0TMID54HL RSCAN0.TMID54.uint8_t[HL]
#define RSCAN0TMID54HH RSCAN0.TMID54.uint8_t[HH]
#define RSCAN0TMPTR54 RSCAN0.TMPTR54.uint32_t
#define RSCAN0TMPTR54H RSCAN0.TMPTR54.uint16_t[H]
#define RSCAN0TMPTR54HL RSCAN0.TMPTR54.uint8_t[HL]
#define RSCAN0TMPTR54HH RSCAN0.TMPTR54.uint8_t[HH]
#define RSCAN0TMDF054 RSCAN0.TMDF054.uint32_t
#define RSCAN0TMDF054L RSCAN0.TMDF054.uint16_t[L]
#define RSCAN0TMDF054LL RSCAN0.TMDF054.uint8_t[LL]
#define RSCAN0TMDF054LH RSCAN0.TMDF054.uint8_t[LH]
#define RSCAN0TMDF054H RSCAN0.TMDF054.uint16_t[H]
#define RSCAN0TMDF054HL RSCAN0.TMDF054.uint8_t[HL]
#define RSCAN0TMDF054HH RSCAN0.TMDF054.uint8_t[HH]
#define RSCAN0TMDF154 RSCAN0.TMDF154.uint32_t
#define RSCAN0TMDF154L RSCAN0.TMDF154.uint16_t[L]
#define RSCAN0TMDF154LL RSCAN0.TMDF154.uint8_t[LL]
#define RSCAN0TMDF154LH RSCAN0.TMDF154.uint8_t[LH]
#define RSCAN0TMDF154H RSCAN0.TMDF154.uint16_t[H]
#define RSCAN0TMDF154HL RSCAN0.TMDF154.uint8_t[HL]
#define RSCAN0TMDF154HH RSCAN0.TMDF154.uint8_t[HH]
#define RSCAN0TMID55 RSCAN0.TMID55.uint32_t
#define RSCAN0TMID55L RSCAN0.TMID55.uint16_t[L]
#define RSCAN0TMID55LL RSCAN0.TMID55.uint8_t[LL]
#define RSCAN0TMID55LH RSCAN0.TMID55.uint8_t[LH]
#define RSCAN0TMID55H RSCAN0.TMID55.uint16_t[H]
#define RSCAN0TMID55HL RSCAN0.TMID55.uint8_t[HL]
#define RSCAN0TMID55HH RSCAN0.TMID55.uint8_t[HH]
#define RSCAN0TMPTR55 RSCAN0.TMPTR55.uint32_t
#define RSCAN0TMPTR55H RSCAN0.TMPTR55.uint16_t[H]
#define RSCAN0TMPTR55HL RSCAN0.TMPTR55.uint8_t[HL]
#define RSCAN0TMPTR55HH RSCAN0.TMPTR55.uint8_t[HH]
#define RSCAN0TMDF055 RSCAN0.TMDF055.uint32_t
#define RSCAN0TMDF055L RSCAN0.TMDF055.uint16_t[L]
#define RSCAN0TMDF055LL RSCAN0.TMDF055.uint8_t[LL]
#define RSCAN0TMDF055LH RSCAN0.TMDF055.uint8_t[LH]
#define RSCAN0TMDF055H RSCAN0.TMDF055.uint16_t[H]
#define RSCAN0TMDF055HL RSCAN0.TMDF055.uint8_t[HL]
#define RSCAN0TMDF055HH RSCAN0.TMDF055.uint8_t[HH]
#define RSCAN0TMDF155 RSCAN0.TMDF155.uint32_t
#define RSCAN0TMDF155L RSCAN0.TMDF155.uint16_t[L]
#define RSCAN0TMDF155LL RSCAN0.TMDF155.uint8_t[LL]
#define RSCAN0TMDF155LH RSCAN0.TMDF155.uint8_t[LH]
#define RSCAN0TMDF155H RSCAN0.TMDF155.uint16_t[H]
#define RSCAN0TMDF155HL RSCAN0.TMDF155.uint8_t[HL]
#define RSCAN0TMDF155HH RSCAN0.TMDF155.uint8_t[HH]
#define RSCAN0TMID56 RSCAN0.TMID56.uint32_t
#define RSCAN0TMID56L RSCAN0.TMID56.uint16_t[L]
#define RSCAN0TMID56LL RSCAN0.TMID56.uint8_t[LL]
#define RSCAN0TMID56LH RSCAN0.TMID56.uint8_t[LH]
#define RSCAN0TMID56H RSCAN0.TMID56.uint16_t[H]
#define RSCAN0TMID56HL RSCAN0.TMID56.uint8_t[HL]
#define RSCAN0TMID56HH RSCAN0.TMID56.uint8_t[HH]
#define RSCAN0TMPTR56 RSCAN0.TMPTR56.uint32_t
#define RSCAN0TMPTR56H RSCAN0.TMPTR56.uint16_t[H]
#define RSCAN0TMPTR56HL RSCAN0.TMPTR56.uint8_t[HL]
#define RSCAN0TMPTR56HH RSCAN0.TMPTR56.uint8_t[HH]
#define RSCAN0TMDF056 RSCAN0.TMDF056.uint32_t
#define RSCAN0TMDF056L RSCAN0.TMDF056.uint16_t[L]
#define RSCAN0TMDF056LL RSCAN0.TMDF056.uint8_t[LL]
#define RSCAN0TMDF056LH RSCAN0.TMDF056.uint8_t[LH]
#define RSCAN0TMDF056H RSCAN0.TMDF056.uint16_t[H]
#define RSCAN0TMDF056HL RSCAN0.TMDF056.uint8_t[HL]
#define RSCAN0TMDF056HH RSCAN0.TMDF056.uint8_t[HH]
#define RSCAN0TMDF156 RSCAN0.TMDF156.uint32_t
#define RSCAN0TMDF156L RSCAN0.TMDF156.uint16_t[L]
#define RSCAN0TMDF156LL RSCAN0.TMDF156.uint8_t[LL]
#define RSCAN0TMDF156LH RSCAN0.TMDF156.uint8_t[LH]
#define RSCAN0TMDF156H RSCAN0.TMDF156.uint16_t[H]
#define RSCAN0TMDF156HL RSCAN0.TMDF156.uint8_t[HL]
#define RSCAN0TMDF156HH RSCAN0.TMDF156.uint8_t[HH]
#define RSCAN0TMID57 RSCAN0.TMID57.uint32_t
#define RSCAN0TMID57L RSCAN0.TMID57.uint16_t[L]
#define RSCAN0TMID57LL RSCAN0.TMID57.uint8_t[LL]
#define RSCAN0TMID57LH RSCAN0.TMID57.uint8_t[LH]
#define RSCAN0TMID57H RSCAN0.TMID57.uint16_t[H]
#define RSCAN0TMID57HL RSCAN0.TMID57.uint8_t[HL]
#define RSCAN0TMID57HH RSCAN0.TMID57.uint8_t[HH]
#define RSCAN0TMPTR57 RSCAN0.TMPTR57.uint32_t
#define RSCAN0TMPTR57H RSCAN0.TMPTR57.uint16_t[H]
#define RSCAN0TMPTR57HL RSCAN0.TMPTR57.uint8_t[HL]
#define RSCAN0TMPTR57HH RSCAN0.TMPTR57.uint8_t[HH]
#define RSCAN0TMDF057 RSCAN0.TMDF057.uint32_t
#define RSCAN0TMDF057L RSCAN0.TMDF057.uint16_t[L]
#define RSCAN0TMDF057LL RSCAN0.TMDF057.uint8_t[LL]
#define RSCAN0TMDF057LH RSCAN0.TMDF057.uint8_t[LH]
#define RSCAN0TMDF057H RSCAN0.TMDF057.uint16_t[H]
#define RSCAN0TMDF057HL RSCAN0.TMDF057.uint8_t[HL]
#define RSCAN0TMDF057HH RSCAN0.TMDF057.uint8_t[HH]
#define RSCAN0TMDF157 RSCAN0.TMDF157.uint32_t
#define RSCAN0TMDF157L RSCAN0.TMDF157.uint16_t[L]
#define RSCAN0TMDF157LL RSCAN0.TMDF157.uint8_t[LL]
#define RSCAN0TMDF157LH RSCAN0.TMDF157.uint8_t[LH]
#define RSCAN0TMDF157H RSCAN0.TMDF157.uint16_t[H]
#define RSCAN0TMDF157HL RSCAN0.TMDF157.uint8_t[HL]
#define RSCAN0TMDF157HH RSCAN0.TMDF157.uint8_t[HH]
#define RSCAN0TMID58 RSCAN0.TMID58.uint32_t
#define RSCAN0TMID58L RSCAN0.TMID58.uint16_t[L]
#define RSCAN0TMID58LL RSCAN0.TMID58.uint8_t[LL]
#define RSCAN0TMID58LH RSCAN0.TMID58.uint8_t[LH]
#define RSCAN0TMID58H RSCAN0.TMID58.uint16_t[H]
#define RSCAN0TMID58HL RSCAN0.TMID58.uint8_t[HL]
#define RSCAN0TMID58HH RSCAN0.TMID58.uint8_t[HH]
#define RSCAN0TMPTR58 RSCAN0.TMPTR58.uint32_t
#define RSCAN0TMPTR58H RSCAN0.TMPTR58.uint16_t[H]
#define RSCAN0TMPTR58HL RSCAN0.TMPTR58.uint8_t[HL]
#define RSCAN0TMPTR58HH RSCAN0.TMPTR58.uint8_t[HH]
#define RSCAN0TMDF058 RSCAN0.TMDF058.uint32_t
#define RSCAN0TMDF058L RSCAN0.TMDF058.uint16_t[L]
#define RSCAN0TMDF058LL RSCAN0.TMDF058.uint8_t[LL]
#define RSCAN0TMDF058LH RSCAN0.TMDF058.uint8_t[LH]
#define RSCAN0TMDF058H RSCAN0.TMDF058.uint16_t[H]
#define RSCAN0TMDF058HL RSCAN0.TMDF058.uint8_t[HL]
#define RSCAN0TMDF058HH RSCAN0.TMDF058.uint8_t[HH]
#define RSCAN0TMDF158 RSCAN0.TMDF158.uint32_t
#define RSCAN0TMDF158L RSCAN0.TMDF158.uint16_t[L]
#define RSCAN0TMDF158LL RSCAN0.TMDF158.uint8_t[LL]
#define RSCAN0TMDF158LH RSCAN0.TMDF158.uint8_t[LH]
#define RSCAN0TMDF158H RSCAN0.TMDF158.uint16_t[H]
#define RSCAN0TMDF158HL RSCAN0.TMDF158.uint8_t[HL]
#define RSCAN0TMDF158HH RSCAN0.TMDF158.uint8_t[HH]
#define RSCAN0TMID59 RSCAN0.TMID59.uint32_t
#define RSCAN0TMID59L RSCAN0.TMID59.uint16_t[L]
#define RSCAN0TMID59LL RSCAN0.TMID59.uint8_t[LL]
#define RSCAN0TMID59LH RSCAN0.TMID59.uint8_t[LH]
#define RSCAN0TMID59H RSCAN0.TMID59.uint16_t[H]
#define RSCAN0TMID59HL RSCAN0.TMID59.uint8_t[HL]
#define RSCAN0TMID59HH RSCAN0.TMID59.uint8_t[HH]
#define RSCAN0TMPTR59 RSCAN0.TMPTR59.uint32_t
#define RSCAN0TMPTR59H RSCAN0.TMPTR59.uint16_t[H]
#define RSCAN0TMPTR59HL RSCAN0.TMPTR59.uint8_t[HL]
#define RSCAN0TMPTR59HH RSCAN0.TMPTR59.uint8_t[HH]
#define RSCAN0TMDF059 RSCAN0.TMDF059.uint32_t
#define RSCAN0TMDF059L RSCAN0.TMDF059.uint16_t[L]
#define RSCAN0TMDF059LL RSCAN0.TMDF059.uint8_t[LL]
#define RSCAN0TMDF059LH RSCAN0.TMDF059.uint8_t[LH]
#define RSCAN0TMDF059H RSCAN0.TMDF059.uint16_t[H]
#define RSCAN0TMDF059HL RSCAN0.TMDF059.uint8_t[HL]
#define RSCAN0TMDF059HH RSCAN0.TMDF059.uint8_t[HH]
#define RSCAN0TMDF159 RSCAN0.TMDF159.uint32_t
#define RSCAN0TMDF159L RSCAN0.TMDF159.uint16_t[L]
#define RSCAN0TMDF159LL RSCAN0.TMDF159.uint8_t[LL]
#define RSCAN0TMDF159LH RSCAN0.TMDF159.uint8_t[LH]
#define RSCAN0TMDF159H RSCAN0.TMDF159.uint16_t[H]
#define RSCAN0TMDF159HL RSCAN0.TMDF159.uint8_t[HL]
#define RSCAN0TMDF159HH RSCAN0.TMDF159.uint8_t[HH]
#define RSCAN0TMID60 RSCAN0.TMID60.uint32_t
#define RSCAN0TMID60L RSCAN0.TMID60.uint16_t[L]
#define RSCAN0TMID60LL RSCAN0.TMID60.uint8_t[LL]
#define RSCAN0TMID60LH RSCAN0.TMID60.uint8_t[LH]
#define RSCAN0TMID60H RSCAN0.TMID60.uint16_t[H]
#define RSCAN0TMID60HL RSCAN0.TMID60.uint8_t[HL]
#define RSCAN0TMID60HH RSCAN0.TMID60.uint8_t[HH]
#define RSCAN0TMPTR60 RSCAN0.TMPTR60.uint32_t
#define RSCAN0TMPTR60H RSCAN0.TMPTR60.uint16_t[H]
#define RSCAN0TMPTR60HL RSCAN0.TMPTR60.uint8_t[HL]
#define RSCAN0TMPTR60HH RSCAN0.TMPTR60.uint8_t[HH]
#define RSCAN0TMDF060 RSCAN0.TMDF060.uint32_t
#define RSCAN0TMDF060L RSCAN0.TMDF060.uint16_t[L]
#define RSCAN0TMDF060LL RSCAN0.TMDF060.uint8_t[LL]
#define RSCAN0TMDF060LH RSCAN0.TMDF060.uint8_t[LH]
#define RSCAN0TMDF060H RSCAN0.TMDF060.uint16_t[H]
#define RSCAN0TMDF060HL RSCAN0.TMDF060.uint8_t[HL]
#define RSCAN0TMDF060HH RSCAN0.TMDF060.uint8_t[HH]
#define RSCAN0TMDF160 RSCAN0.TMDF160.uint32_t
#define RSCAN0TMDF160L RSCAN0.TMDF160.uint16_t[L]
#define RSCAN0TMDF160LL RSCAN0.TMDF160.uint8_t[LL]
#define RSCAN0TMDF160LH RSCAN0.TMDF160.uint8_t[LH]
#define RSCAN0TMDF160H RSCAN0.TMDF160.uint16_t[H]
#define RSCAN0TMDF160HL RSCAN0.TMDF160.uint8_t[HL]
#define RSCAN0TMDF160HH RSCAN0.TMDF160.uint8_t[HH]
#define RSCAN0TMID61 RSCAN0.TMID61.uint32_t
#define RSCAN0TMID61L RSCAN0.TMID61.uint16_t[L]
#define RSCAN0TMID61LL RSCAN0.TMID61.uint8_t[LL]
#define RSCAN0TMID61LH RSCAN0.TMID61.uint8_t[LH]
#define RSCAN0TMID61H RSCAN0.TMID61.uint16_t[H]
#define RSCAN0TMID61HL RSCAN0.TMID61.uint8_t[HL]
#define RSCAN0TMID61HH RSCAN0.TMID61.uint8_t[HH]
#define RSCAN0TMPTR61 RSCAN0.TMPTR61.uint32_t
#define RSCAN0TMPTR61H RSCAN0.TMPTR61.uint16_t[H]
#define RSCAN0TMPTR61HL RSCAN0.TMPTR61.uint8_t[HL]
#define RSCAN0TMPTR61HH RSCAN0.TMPTR61.uint8_t[HH]
#define RSCAN0TMDF061 RSCAN0.TMDF061.uint32_t
#define RSCAN0TMDF061L RSCAN0.TMDF061.uint16_t[L]
#define RSCAN0TMDF061LL RSCAN0.TMDF061.uint8_t[LL]
#define RSCAN0TMDF061LH RSCAN0.TMDF061.uint8_t[LH]
#define RSCAN0TMDF061H RSCAN0.TMDF061.uint16_t[H]
#define RSCAN0TMDF061HL RSCAN0.TMDF061.uint8_t[HL]
#define RSCAN0TMDF061HH RSCAN0.TMDF061.uint8_t[HH]
#define RSCAN0TMDF161 RSCAN0.TMDF161.uint32_t
#define RSCAN0TMDF161L RSCAN0.TMDF161.uint16_t[L]
#define RSCAN0TMDF161LL RSCAN0.TMDF161.uint8_t[LL]
#define RSCAN0TMDF161LH RSCAN0.TMDF161.uint8_t[LH]
#define RSCAN0TMDF161H RSCAN0.TMDF161.uint16_t[H]
#define RSCAN0TMDF161HL RSCAN0.TMDF161.uint8_t[HL]
#define RSCAN0TMDF161HH RSCAN0.TMDF161.uint8_t[HH]
#define RSCAN0TMID62 RSCAN0.TMID62.uint32_t
#define RSCAN0TMID62L RSCAN0.TMID62.uint16_t[L]
#define RSCAN0TMID62LL RSCAN0.TMID62.uint8_t[LL]
#define RSCAN0TMID62LH RSCAN0.TMID62.uint8_t[LH]
#define RSCAN0TMID62H RSCAN0.TMID62.uint16_t[H]
#define RSCAN0TMID62HL RSCAN0.TMID62.uint8_t[HL]
#define RSCAN0TMID62HH RSCAN0.TMID62.uint8_t[HH]
#define RSCAN0TMPTR62 RSCAN0.TMPTR62.uint32_t
#define RSCAN0TMPTR62H RSCAN0.TMPTR62.uint16_t[H]
#define RSCAN0TMPTR62HL RSCAN0.TMPTR62.uint8_t[HL]
#define RSCAN0TMPTR62HH RSCAN0.TMPTR62.uint8_t[HH]
#define RSCAN0TMDF062 RSCAN0.TMDF062.uint32_t
#define RSCAN0TMDF062L RSCAN0.TMDF062.uint16_t[L]
#define RSCAN0TMDF062LL RSCAN0.TMDF062.uint8_t[LL]
#define RSCAN0TMDF062LH RSCAN0.TMDF062.uint8_t[LH]
#define RSCAN0TMDF062H RSCAN0.TMDF062.uint16_t[H]
#define RSCAN0TMDF062HL RSCAN0.TMDF062.uint8_t[HL]
#define RSCAN0TMDF062HH RSCAN0.TMDF062.uint8_t[HH]
#define RSCAN0TMDF162 RSCAN0.TMDF162.uint32_t
#define RSCAN0TMDF162L RSCAN0.TMDF162.uint16_t[L]
#define RSCAN0TMDF162LL RSCAN0.TMDF162.uint8_t[LL]
#define RSCAN0TMDF162LH RSCAN0.TMDF162.uint8_t[LH]
#define RSCAN0TMDF162H RSCAN0.TMDF162.uint16_t[H]
#define RSCAN0TMDF162HL RSCAN0.TMDF162.uint8_t[HL]
#define RSCAN0TMDF162HH RSCAN0.TMDF162.uint8_t[HH]
#define RSCAN0TMID63 RSCAN0.TMID63.uint32_t
#define RSCAN0TMID63L RSCAN0.TMID63.uint16_t[L]
#define RSCAN0TMID63LL RSCAN0.TMID63.uint8_t[LL]
#define RSCAN0TMID63LH RSCAN0.TMID63.uint8_t[LH]
#define RSCAN0TMID63H RSCAN0.TMID63.uint16_t[H]
#define RSCAN0TMID63HL RSCAN0.TMID63.uint8_t[HL]
#define RSCAN0TMID63HH RSCAN0.TMID63.uint8_t[HH]
#define RSCAN0TMPTR63 RSCAN0.TMPTR63.uint32_t
#define RSCAN0TMPTR63H RSCAN0.TMPTR63.uint16_t[H]
#define RSCAN0TMPTR63HL RSCAN0.TMPTR63.uint8_t[HL]
#define RSCAN0TMPTR63HH RSCAN0.TMPTR63.uint8_t[HH]
#define RSCAN0TMDF063 RSCAN0.TMDF063.uint32_t
#define RSCAN0TMDF063L RSCAN0.TMDF063.uint16_t[L]
#define RSCAN0TMDF063LL RSCAN0.TMDF063.uint8_t[LL]
#define RSCAN0TMDF063LH RSCAN0.TMDF063.uint8_t[LH]
#define RSCAN0TMDF063H RSCAN0.TMDF063.uint16_t[H]
#define RSCAN0TMDF063HL RSCAN0.TMDF063.uint8_t[HL]
#define RSCAN0TMDF063HH RSCAN0.TMDF063.uint8_t[HH]
#define RSCAN0TMDF163 RSCAN0.TMDF163.uint32_t
#define RSCAN0TMDF163L RSCAN0.TMDF163.uint16_t[L]
#define RSCAN0TMDF163LL RSCAN0.TMDF163.uint8_t[LL]
#define RSCAN0TMDF163LH RSCAN0.TMDF163.uint8_t[LH]
#define RSCAN0TMDF163H RSCAN0.TMDF163.uint16_t[H]
#define RSCAN0TMDF163HL RSCAN0.TMDF163.uint8_t[HL]
#define RSCAN0TMDF163HH RSCAN0.TMDF163.uint8_t[HH]
#define RSCAN0TMID64 RSCAN0.TMID64.uint32_t
#define RSCAN0TMID64L RSCAN0.TMID64.uint16_t[L]
#define RSCAN0TMID64LL RSCAN0.TMID64.uint8_t[LL]
#define RSCAN0TMID64LH RSCAN0.TMID64.uint8_t[LH]
#define RSCAN0TMID64H RSCAN0.TMID64.uint16_t[H]
#define RSCAN0TMID64HL RSCAN0.TMID64.uint8_t[HL]
#define RSCAN0TMID64HH RSCAN0.TMID64.uint8_t[HH]
#define RSCAN0TMPTR64 RSCAN0.TMPTR64.uint32_t
#define RSCAN0TMPTR64H RSCAN0.TMPTR64.uint16_t[H]
#define RSCAN0TMPTR64HL RSCAN0.TMPTR64.uint8_t[HL]
#define RSCAN0TMPTR64HH RSCAN0.TMPTR64.uint8_t[HH]
#define RSCAN0TMDF064 RSCAN0.TMDF064.uint32_t
#define RSCAN0TMDF064L RSCAN0.TMDF064.uint16_t[L]
#define RSCAN0TMDF064LL RSCAN0.TMDF064.uint8_t[LL]
#define RSCAN0TMDF064LH RSCAN0.TMDF064.uint8_t[LH]
#define RSCAN0TMDF064H RSCAN0.TMDF064.uint16_t[H]
#define RSCAN0TMDF064HL RSCAN0.TMDF064.uint8_t[HL]
#define RSCAN0TMDF064HH RSCAN0.TMDF064.uint8_t[HH]
#define RSCAN0TMDF164 RSCAN0.TMDF164.uint32_t
#define RSCAN0TMDF164L RSCAN0.TMDF164.uint16_t[L]
#define RSCAN0TMDF164LL RSCAN0.TMDF164.uint8_t[LL]
#define RSCAN0TMDF164LH RSCAN0.TMDF164.uint8_t[LH]
#define RSCAN0TMDF164H RSCAN0.TMDF164.uint16_t[H]
#define RSCAN0TMDF164HL RSCAN0.TMDF164.uint8_t[HL]
#define RSCAN0TMDF164HH RSCAN0.TMDF164.uint8_t[HH]
#define RSCAN0TMID65 RSCAN0.TMID65.uint32_t
#define RSCAN0TMID65L RSCAN0.TMID65.uint16_t[L]
#define RSCAN0TMID65LL RSCAN0.TMID65.uint8_t[LL]
#define RSCAN0TMID65LH RSCAN0.TMID65.uint8_t[LH]
#define RSCAN0TMID65H RSCAN0.TMID65.uint16_t[H]
#define RSCAN0TMID65HL RSCAN0.TMID65.uint8_t[HL]
#define RSCAN0TMID65HH RSCAN0.TMID65.uint8_t[HH]
#define RSCAN0TMPTR65 RSCAN0.TMPTR65.uint32_t
#define RSCAN0TMPTR65H RSCAN0.TMPTR65.uint16_t[H]
#define RSCAN0TMPTR65HL RSCAN0.TMPTR65.uint8_t[HL]
#define RSCAN0TMPTR65HH RSCAN0.TMPTR65.uint8_t[HH]
#define RSCAN0TMDF065 RSCAN0.TMDF065.uint32_t
#define RSCAN0TMDF065L RSCAN0.TMDF065.uint16_t[L]
#define RSCAN0TMDF065LL RSCAN0.TMDF065.uint8_t[LL]
#define RSCAN0TMDF065LH RSCAN0.TMDF065.uint8_t[LH]
#define RSCAN0TMDF065H RSCAN0.TMDF065.uint16_t[H]
#define RSCAN0TMDF065HL RSCAN0.TMDF065.uint8_t[HL]
#define RSCAN0TMDF065HH RSCAN0.TMDF065.uint8_t[HH]
#define RSCAN0TMDF165 RSCAN0.TMDF165.uint32_t
#define RSCAN0TMDF165L RSCAN0.TMDF165.uint16_t[L]
#define RSCAN0TMDF165LL RSCAN0.TMDF165.uint8_t[LL]
#define RSCAN0TMDF165LH RSCAN0.TMDF165.uint8_t[LH]
#define RSCAN0TMDF165H RSCAN0.TMDF165.uint16_t[H]
#define RSCAN0TMDF165HL RSCAN0.TMDF165.uint8_t[HL]
#define RSCAN0TMDF165HH RSCAN0.TMDF165.uint8_t[HH]
#define RSCAN0TMID66 RSCAN0.TMID66.uint32_t
#define RSCAN0TMID66L RSCAN0.TMID66.uint16_t[L]
#define RSCAN0TMID66LL RSCAN0.TMID66.uint8_t[LL]
#define RSCAN0TMID66LH RSCAN0.TMID66.uint8_t[LH]
#define RSCAN0TMID66H RSCAN0.TMID66.uint16_t[H]
#define RSCAN0TMID66HL RSCAN0.TMID66.uint8_t[HL]
#define RSCAN0TMID66HH RSCAN0.TMID66.uint8_t[HH]
#define RSCAN0TMPTR66 RSCAN0.TMPTR66.uint32_t
#define RSCAN0TMPTR66H RSCAN0.TMPTR66.uint16_t[H]
#define RSCAN0TMPTR66HL RSCAN0.TMPTR66.uint8_t[HL]
#define RSCAN0TMPTR66HH RSCAN0.TMPTR66.uint8_t[HH]
#define RSCAN0TMDF066 RSCAN0.TMDF066.uint32_t
#define RSCAN0TMDF066L RSCAN0.TMDF066.uint16_t[L]
#define RSCAN0TMDF066LL RSCAN0.TMDF066.uint8_t[LL]
#define RSCAN0TMDF066LH RSCAN0.TMDF066.uint8_t[LH]
#define RSCAN0TMDF066H RSCAN0.TMDF066.uint16_t[H]
#define RSCAN0TMDF066HL RSCAN0.TMDF066.uint8_t[HL]
#define RSCAN0TMDF066HH RSCAN0.TMDF066.uint8_t[HH]
#define RSCAN0TMDF166 RSCAN0.TMDF166.uint32_t
#define RSCAN0TMDF166L RSCAN0.TMDF166.uint16_t[L]
#define RSCAN0TMDF166LL RSCAN0.TMDF166.uint8_t[LL]
#define RSCAN0TMDF166LH RSCAN0.TMDF166.uint8_t[LH]
#define RSCAN0TMDF166H RSCAN0.TMDF166.uint16_t[H]
#define RSCAN0TMDF166HL RSCAN0.TMDF166.uint8_t[HL]
#define RSCAN0TMDF166HH RSCAN0.TMDF166.uint8_t[HH]
#define RSCAN0TMID67 RSCAN0.TMID67.uint32_t
#define RSCAN0TMID67L RSCAN0.TMID67.uint16_t[L]
#define RSCAN0TMID67LL RSCAN0.TMID67.uint8_t[LL]
#define RSCAN0TMID67LH RSCAN0.TMID67.uint8_t[LH]
#define RSCAN0TMID67H RSCAN0.TMID67.uint16_t[H]
#define RSCAN0TMID67HL RSCAN0.TMID67.uint8_t[HL]
#define RSCAN0TMID67HH RSCAN0.TMID67.uint8_t[HH]
#define RSCAN0TMPTR67 RSCAN0.TMPTR67.uint32_t
#define RSCAN0TMPTR67H RSCAN0.TMPTR67.uint16_t[H]
#define RSCAN0TMPTR67HL RSCAN0.TMPTR67.uint8_t[HL]
#define RSCAN0TMPTR67HH RSCAN0.TMPTR67.uint8_t[HH]
#define RSCAN0TMDF067 RSCAN0.TMDF067.uint32_t
#define RSCAN0TMDF067L RSCAN0.TMDF067.uint16_t[L]
#define RSCAN0TMDF067LL RSCAN0.TMDF067.uint8_t[LL]
#define RSCAN0TMDF067LH RSCAN0.TMDF067.uint8_t[LH]
#define RSCAN0TMDF067H RSCAN0.TMDF067.uint16_t[H]
#define RSCAN0TMDF067HL RSCAN0.TMDF067.uint8_t[HL]
#define RSCAN0TMDF067HH RSCAN0.TMDF067.uint8_t[HH]
#define RSCAN0TMDF167 RSCAN0.TMDF167.uint32_t
#define RSCAN0TMDF167L RSCAN0.TMDF167.uint16_t[L]
#define RSCAN0TMDF167LL RSCAN0.TMDF167.uint8_t[LL]
#define RSCAN0TMDF167LH RSCAN0.TMDF167.uint8_t[LH]
#define RSCAN0TMDF167H RSCAN0.TMDF167.uint16_t[H]
#define RSCAN0TMDF167HL RSCAN0.TMDF167.uint8_t[HL]
#define RSCAN0TMDF167HH RSCAN0.TMDF167.uint8_t[HH]
#define RSCAN0TMID68 RSCAN0.TMID68.uint32_t
#define RSCAN0TMID68L RSCAN0.TMID68.uint16_t[L]
#define RSCAN0TMID68LL RSCAN0.TMID68.uint8_t[LL]
#define RSCAN0TMID68LH RSCAN0.TMID68.uint8_t[LH]
#define RSCAN0TMID68H RSCAN0.TMID68.uint16_t[H]
#define RSCAN0TMID68HL RSCAN0.TMID68.uint8_t[HL]
#define RSCAN0TMID68HH RSCAN0.TMID68.uint8_t[HH]
#define RSCAN0TMPTR68 RSCAN0.TMPTR68.uint32_t
#define RSCAN0TMPTR68H RSCAN0.TMPTR68.uint16_t[H]
#define RSCAN0TMPTR68HL RSCAN0.TMPTR68.uint8_t[HL]
#define RSCAN0TMPTR68HH RSCAN0.TMPTR68.uint8_t[HH]
#define RSCAN0TMDF068 RSCAN0.TMDF068.uint32_t
#define RSCAN0TMDF068L RSCAN0.TMDF068.uint16_t[L]
#define RSCAN0TMDF068LL RSCAN0.TMDF068.uint8_t[LL]
#define RSCAN0TMDF068LH RSCAN0.TMDF068.uint8_t[LH]
#define RSCAN0TMDF068H RSCAN0.TMDF068.uint16_t[H]
#define RSCAN0TMDF068HL RSCAN0.TMDF068.uint8_t[HL]
#define RSCAN0TMDF068HH RSCAN0.TMDF068.uint8_t[HH]
#define RSCAN0TMDF168 RSCAN0.TMDF168.uint32_t
#define RSCAN0TMDF168L RSCAN0.TMDF168.uint16_t[L]
#define RSCAN0TMDF168LL RSCAN0.TMDF168.uint8_t[LL]
#define RSCAN0TMDF168LH RSCAN0.TMDF168.uint8_t[LH]
#define RSCAN0TMDF168H RSCAN0.TMDF168.uint16_t[H]
#define RSCAN0TMDF168HL RSCAN0.TMDF168.uint8_t[HL]
#define RSCAN0TMDF168HH RSCAN0.TMDF168.uint8_t[HH]
#define RSCAN0TMID69 RSCAN0.TMID69.uint32_t
#define RSCAN0TMID69L RSCAN0.TMID69.uint16_t[L]
#define RSCAN0TMID69LL RSCAN0.TMID69.uint8_t[LL]
#define RSCAN0TMID69LH RSCAN0.TMID69.uint8_t[LH]
#define RSCAN0TMID69H RSCAN0.TMID69.uint16_t[H]
#define RSCAN0TMID69HL RSCAN0.TMID69.uint8_t[HL]
#define RSCAN0TMID69HH RSCAN0.TMID69.uint8_t[HH]
#define RSCAN0TMPTR69 RSCAN0.TMPTR69.uint32_t
#define RSCAN0TMPTR69H RSCAN0.TMPTR69.uint16_t[H]
#define RSCAN0TMPTR69HL RSCAN0.TMPTR69.uint8_t[HL]
#define RSCAN0TMPTR69HH RSCAN0.TMPTR69.uint8_t[HH]
#define RSCAN0TMDF069 RSCAN0.TMDF069.uint32_t
#define RSCAN0TMDF069L RSCAN0.TMDF069.uint16_t[L]
#define RSCAN0TMDF069LL RSCAN0.TMDF069.uint8_t[LL]
#define RSCAN0TMDF069LH RSCAN0.TMDF069.uint8_t[LH]
#define RSCAN0TMDF069H RSCAN0.TMDF069.uint16_t[H]
#define RSCAN0TMDF069HL RSCAN0.TMDF069.uint8_t[HL]
#define RSCAN0TMDF069HH RSCAN0.TMDF069.uint8_t[HH]
#define RSCAN0TMDF169 RSCAN0.TMDF169.uint32_t
#define RSCAN0TMDF169L RSCAN0.TMDF169.uint16_t[L]
#define RSCAN0TMDF169LL RSCAN0.TMDF169.uint8_t[LL]
#define RSCAN0TMDF169LH RSCAN0.TMDF169.uint8_t[LH]
#define RSCAN0TMDF169H RSCAN0.TMDF169.uint16_t[H]
#define RSCAN0TMDF169HL RSCAN0.TMDF169.uint8_t[HL]
#define RSCAN0TMDF169HH RSCAN0.TMDF169.uint8_t[HH]
#define RSCAN0TMID70 RSCAN0.TMID70.uint32_t
#define RSCAN0TMID70L RSCAN0.TMID70.uint16_t[L]
#define RSCAN0TMID70LL RSCAN0.TMID70.uint8_t[LL]
#define RSCAN0TMID70LH RSCAN0.TMID70.uint8_t[LH]
#define RSCAN0TMID70H RSCAN0.TMID70.uint16_t[H]
#define RSCAN0TMID70HL RSCAN0.TMID70.uint8_t[HL]
#define RSCAN0TMID70HH RSCAN0.TMID70.uint8_t[HH]
#define RSCAN0TMPTR70 RSCAN0.TMPTR70.uint32_t
#define RSCAN0TMPTR70H RSCAN0.TMPTR70.uint16_t[H]
#define RSCAN0TMPTR70HL RSCAN0.TMPTR70.uint8_t[HL]
#define RSCAN0TMPTR70HH RSCAN0.TMPTR70.uint8_t[HH]
#define RSCAN0TMDF070 RSCAN0.TMDF070.uint32_t
#define RSCAN0TMDF070L RSCAN0.TMDF070.uint16_t[L]
#define RSCAN0TMDF070LL RSCAN0.TMDF070.uint8_t[LL]
#define RSCAN0TMDF070LH RSCAN0.TMDF070.uint8_t[LH]
#define RSCAN0TMDF070H RSCAN0.TMDF070.uint16_t[H]
#define RSCAN0TMDF070HL RSCAN0.TMDF070.uint8_t[HL]
#define RSCAN0TMDF070HH RSCAN0.TMDF070.uint8_t[HH]
#define RSCAN0TMDF170 RSCAN0.TMDF170.uint32_t
#define RSCAN0TMDF170L RSCAN0.TMDF170.uint16_t[L]
#define RSCAN0TMDF170LL RSCAN0.TMDF170.uint8_t[LL]
#define RSCAN0TMDF170LH RSCAN0.TMDF170.uint8_t[LH]
#define RSCAN0TMDF170H RSCAN0.TMDF170.uint16_t[H]
#define RSCAN0TMDF170HL RSCAN0.TMDF170.uint8_t[HL]
#define RSCAN0TMDF170HH RSCAN0.TMDF170.uint8_t[HH]
#define RSCAN0TMID71 RSCAN0.TMID71.uint32_t
#define RSCAN0TMID71L RSCAN0.TMID71.uint16_t[L]
#define RSCAN0TMID71LL RSCAN0.TMID71.uint8_t[LL]
#define RSCAN0TMID71LH RSCAN0.TMID71.uint8_t[LH]
#define RSCAN0TMID71H RSCAN0.TMID71.uint16_t[H]
#define RSCAN0TMID71HL RSCAN0.TMID71.uint8_t[HL]
#define RSCAN0TMID71HH RSCAN0.TMID71.uint8_t[HH]
#define RSCAN0TMPTR71 RSCAN0.TMPTR71.uint32_t
#define RSCAN0TMPTR71H RSCAN0.TMPTR71.uint16_t[H]
#define RSCAN0TMPTR71HL RSCAN0.TMPTR71.uint8_t[HL]
#define RSCAN0TMPTR71HH RSCAN0.TMPTR71.uint8_t[HH]
#define RSCAN0TMDF071 RSCAN0.TMDF071.uint32_t
#define RSCAN0TMDF071L RSCAN0.TMDF071.uint16_t[L]
#define RSCAN0TMDF071LL RSCAN0.TMDF071.uint8_t[LL]
#define RSCAN0TMDF071LH RSCAN0.TMDF071.uint8_t[LH]
#define RSCAN0TMDF071H RSCAN0.TMDF071.uint16_t[H]
#define RSCAN0TMDF071HL RSCAN0.TMDF071.uint8_t[HL]
#define RSCAN0TMDF071HH RSCAN0.TMDF071.uint8_t[HH]
#define RSCAN0TMDF171 RSCAN0.TMDF171.uint32_t
#define RSCAN0TMDF171L RSCAN0.TMDF171.uint16_t[L]
#define RSCAN0TMDF171LL RSCAN0.TMDF171.uint8_t[LL]
#define RSCAN0TMDF171LH RSCAN0.TMDF171.uint8_t[LH]
#define RSCAN0TMDF171H RSCAN0.TMDF171.uint16_t[H]
#define RSCAN0TMDF171HL RSCAN0.TMDF171.uint8_t[HL]
#define RSCAN0TMDF171HH RSCAN0.TMDF171.uint8_t[HH]
#define RSCAN0TMID72 RSCAN0.TMID72.uint32_t
#define RSCAN0TMID72L RSCAN0.TMID72.uint16_t[L]
#define RSCAN0TMID72LL RSCAN0.TMID72.uint8_t[LL]
#define RSCAN0TMID72LH RSCAN0.TMID72.uint8_t[LH]
#define RSCAN0TMID72H RSCAN0.TMID72.uint16_t[H]
#define RSCAN0TMID72HL RSCAN0.TMID72.uint8_t[HL]
#define RSCAN0TMID72HH RSCAN0.TMID72.uint8_t[HH]
#define RSCAN0TMPTR72 RSCAN0.TMPTR72.uint32_t
#define RSCAN0TMPTR72H RSCAN0.TMPTR72.uint16_t[H]
#define RSCAN0TMPTR72HL RSCAN0.TMPTR72.uint8_t[HL]
#define RSCAN0TMPTR72HH RSCAN0.TMPTR72.uint8_t[HH]
#define RSCAN0TMDF072 RSCAN0.TMDF072.uint32_t
#define RSCAN0TMDF072L RSCAN0.TMDF072.uint16_t[L]
#define RSCAN0TMDF072LL RSCAN0.TMDF072.uint8_t[LL]
#define RSCAN0TMDF072LH RSCAN0.TMDF072.uint8_t[LH]
#define RSCAN0TMDF072H RSCAN0.TMDF072.uint16_t[H]
#define RSCAN0TMDF072HL RSCAN0.TMDF072.uint8_t[HL]
#define RSCAN0TMDF072HH RSCAN0.TMDF072.uint8_t[HH]
#define RSCAN0TMDF172 RSCAN0.TMDF172.uint32_t
#define RSCAN0TMDF172L RSCAN0.TMDF172.uint16_t[L]
#define RSCAN0TMDF172LL RSCAN0.TMDF172.uint8_t[LL]
#define RSCAN0TMDF172LH RSCAN0.TMDF172.uint8_t[LH]
#define RSCAN0TMDF172H RSCAN0.TMDF172.uint16_t[H]
#define RSCAN0TMDF172HL RSCAN0.TMDF172.uint8_t[HL]
#define RSCAN0TMDF172HH RSCAN0.TMDF172.uint8_t[HH]
#define RSCAN0TMID73 RSCAN0.TMID73.uint32_t
#define RSCAN0TMID73L RSCAN0.TMID73.uint16_t[L]
#define RSCAN0TMID73LL RSCAN0.TMID73.uint8_t[LL]
#define RSCAN0TMID73LH RSCAN0.TMID73.uint8_t[LH]
#define RSCAN0TMID73H RSCAN0.TMID73.uint16_t[H]
#define RSCAN0TMID73HL RSCAN0.TMID73.uint8_t[HL]
#define RSCAN0TMID73HH RSCAN0.TMID73.uint8_t[HH]
#define RSCAN0TMPTR73 RSCAN0.TMPTR73.uint32_t
#define RSCAN0TMPTR73H RSCAN0.TMPTR73.uint16_t[H]
#define RSCAN0TMPTR73HL RSCAN0.TMPTR73.uint8_t[HL]
#define RSCAN0TMPTR73HH RSCAN0.TMPTR73.uint8_t[HH]
#define RSCAN0TMDF073 RSCAN0.TMDF073.uint32_t
#define RSCAN0TMDF073L RSCAN0.TMDF073.uint16_t[L]
#define RSCAN0TMDF073LL RSCAN0.TMDF073.uint8_t[LL]
#define RSCAN0TMDF073LH RSCAN0.TMDF073.uint8_t[LH]
#define RSCAN0TMDF073H RSCAN0.TMDF073.uint16_t[H]
#define RSCAN0TMDF073HL RSCAN0.TMDF073.uint8_t[HL]
#define RSCAN0TMDF073HH RSCAN0.TMDF073.uint8_t[HH]
#define RSCAN0TMDF173 RSCAN0.TMDF173.uint32_t
#define RSCAN0TMDF173L RSCAN0.TMDF173.uint16_t[L]
#define RSCAN0TMDF173LL RSCAN0.TMDF173.uint8_t[LL]
#define RSCAN0TMDF173LH RSCAN0.TMDF173.uint8_t[LH]
#define RSCAN0TMDF173H RSCAN0.TMDF173.uint16_t[H]
#define RSCAN0TMDF173HL RSCAN0.TMDF173.uint8_t[HL]
#define RSCAN0TMDF173HH RSCAN0.TMDF173.uint8_t[HH]
#define RSCAN0TMID74 RSCAN0.TMID74.uint32_t
#define RSCAN0TMID74L RSCAN0.TMID74.uint16_t[L]
#define RSCAN0TMID74LL RSCAN0.TMID74.uint8_t[LL]
#define RSCAN0TMID74LH RSCAN0.TMID74.uint8_t[LH]
#define RSCAN0TMID74H RSCAN0.TMID74.uint16_t[H]
#define RSCAN0TMID74HL RSCAN0.TMID74.uint8_t[HL]
#define RSCAN0TMID74HH RSCAN0.TMID74.uint8_t[HH]
#define RSCAN0TMPTR74 RSCAN0.TMPTR74.uint32_t
#define RSCAN0TMPTR74H RSCAN0.TMPTR74.uint16_t[H]
#define RSCAN0TMPTR74HL RSCAN0.TMPTR74.uint8_t[HL]
#define RSCAN0TMPTR74HH RSCAN0.TMPTR74.uint8_t[HH]
#define RSCAN0TMDF074 RSCAN0.TMDF074.uint32_t
#define RSCAN0TMDF074L RSCAN0.TMDF074.uint16_t[L]
#define RSCAN0TMDF074LL RSCAN0.TMDF074.uint8_t[LL]
#define RSCAN0TMDF074LH RSCAN0.TMDF074.uint8_t[LH]
#define RSCAN0TMDF074H RSCAN0.TMDF074.uint16_t[H]
#define RSCAN0TMDF074HL RSCAN0.TMDF074.uint8_t[HL]
#define RSCAN0TMDF074HH RSCAN0.TMDF074.uint8_t[HH]
#define RSCAN0TMDF174 RSCAN0.TMDF174.uint32_t
#define RSCAN0TMDF174L RSCAN0.TMDF174.uint16_t[L]
#define RSCAN0TMDF174LL RSCAN0.TMDF174.uint8_t[LL]
#define RSCAN0TMDF174LH RSCAN0.TMDF174.uint8_t[LH]
#define RSCAN0TMDF174H RSCAN0.TMDF174.uint16_t[H]
#define RSCAN0TMDF174HL RSCAN0.TMDF174.uint8_t[HL]
#define RSCAN0TMDF174HH RSCAN0.TMDF174.uint8_t[HH]
#define RSCAN0TMID75 RSCAN0.TMID75.uint32_t
#define RSCAN0TMID75L RSCAN0.TMID75.uint16_t[L]
#define RSCAN0TMID75LL RSCAN0.TMID75.uint8_t[LL]
#define RSCAN0TMID75LH RSCAN0.TMID75.uint8_t[LH]
#define RSCAN0TMID75H RSCAN0.TMID75.uint16_t[H]
#define RSCAN0TMID75HL RSCAN0.TMID75.uint8_t[HL]
#define RSCAN0TMID75HH RSCAN0.TMID75.uint8_t[HH]
#define RSCAN0TMPTR75 RSCAN0.TMPTR75.uint32_t
#define RSCAN0TMPTR75H RSCAN0.TMPTR75.uint16_t[H]
#define RSCAN0TMPTR75HL RSCAN0.TMPTR75.uint8_t[HL]
#define RSCAN0TMPTR75HH RSCAN0.TMPTR75.uint8_t[HH]
#define RSCAN0TMDF075 RSCAN0.TMDF075.uint32_t
#define RSCAN0TMDF075L RSCAN0.TMDF075.uint16_t[L]
#define RSCAN0TMDF075LL RSCAN0.TMDF075.uint8_t[LL]
#define RSCAN0TMDF075LH RSCAN0.TMDF075.uint8_t[LH]
#define RSCAN0TMDF075H RSCAN0.TMDF075.uint16_t[H]
#define RSCAN0TMDF075HL RSCAN0.TMDF075.uint8_t[HL]
#define RSCAN0TMDF075HH RSCAN0.TMDF075.uint8_t[HH]
#define RSCAN0TMDF175 RSCAN0.TMDF175.uint32_t
#define RSCAN0TMDF175L RSCAN0.TMDF175.uint16_t[L]
#define RSCAN0TMDF175LL RSCAN0.TMDF175.uint8_t[LL]
#define RSCAN0TMDF175LH RSCAN0.TMDF175.uint8_t[LH]
#define RSCAN0TMDF175H RSCAN0.TMDF175.uint16_t[H]
#define RSCAN0TMDF175HL RSCAN0.TMDF175.uint8_t[HL]
#define RSCAN0TMDF175HH RSCAN0.TMDF175.uint8_t[HH]
#define RSCAN0TMID76 RSCAN0.TMID76.uint32_t
#define RSCAN0TMID76L RSCAN0.TMID76.uint16_t[L]
#define RSCAN0TMID76LL RSCAN0.TMID76.uint8_t[LL]
#define RSCAN0TMID76LH RSCAN0.TMID76.uint8_t[LH]
#define RSCAN0TMID76H RSCAN0.TMID76.uint16_t[H]
#define RSCAN0TMID76HL RSCAN0.TMID76.uint8_t[HL]
#define RSCAN0TMID76HH RSCAN0.TMID76.uint8_t[HH]
#define RSCAN0TMPTR76 RSCAN0.TMPTR76.uint32_t
#define RSCAN0TMPTR76H RSCAN0.TMPTR76.uint16_t[H]
#define RSCAN0TMPTR76HL RSCAN0.TMPTR76.uint8_t[HL]
#define RSCAN0TMPTR76HH RSCAN0.TMPTR76.uint8_t[HH]
#define RSCAN0TMDF076 RSCAN0.TMDF076.uint32_t
#define RSCAN0TMDF076L RSCAN0.TMDF076.uint16_t[L]
#define RSCAN0TMDF076LL RSCAN0.TMDF076.uint8_t[LL]
#define RSCAN0TMDF076LH RSCAN0.TMDF076.uint8_t[LH]
#define RSCAN0TMDF076H RSCAN0.TMDF076.uint16_t[H]
#define RSCAN0TMDF076HL RSCAN0.TMDF076.uint8_t[HL]
#define RSCAN0TMDF076HH RSCAN0.TMDF076.uint8_t[HH]
#define RSCAN0TMDF176 RSCAN0.TMDF176.uint32_t
#define RSCAN0TMDF176L RSCAN0.TMDF176.uint16_t[L]
#define RSCAN0TMDF176LL RSCAN0.TMDF176.uint8_t[LL]
#define RSCAN0TMDF176LH RSCAN0.TMDF176.uint8_t[LH]
#define RSCAN0TMDF176H RSCAN0.TMDF176.uint16_t[H]
#define RSCAN0TMDF176HL RSCAN0.TMDF176.uint8_t[HL]
#define RSCAN0TMDF176HH RSCAN0.TMDF176.uint8_t[HH]
#define RSCAN0TMID77 RSCAN0.TMID77.uint32_t
#define RSCAN0TMID77L RSCAN0.TMID77.uint16_t[L]
#define RSCAN0TMID77LL RSCAN0.TMID77.uint8_t[LL]
#define RSCAN0TMID77LH RSCAN0.TMID77.uint8_t[LH]
#define RSCAN0TMID77H RSCAN0.TMID77.uint16_t[H]
#define RSCAN0TMID77HL RSCAN0.TMID77.uint8_t[HL]
#define RSCAN0TMID77HH RSCAN0.TMID77.uint8_t[HH]
#define RSCAN0TMPTR77 RSCAN0.TMPTR77.uint32_t
#define RSCAN0TMPTR77H RSCAN0.TMPTR77.uint16_t[H]
#define RSCAN0TMPTR77HL RSCAN0.TMPTR77.uint8_t[HL]
#define RSCAN0TMPTR77HH RSCAN0.TMPTR77.uint8_t[HH]
#define RSCAN0TMDF077 RSCAN0.TMDF077.uint32_t
#define RSCAN0TMDF077L RSCAN0.TMDF077.uint16_t[L]
#define RSCAN0TMDF077LL RSCAN0.TMDF077.uint8_t[LL]
#define RSCAN0TMDF077LH RSCAN0.TMDF077.uint8_t[LH]
#define RSCAN0TMDF077H RSCAN0.TMDF077.uint16_t[H]
#define RSCAN0TMDF077HL RSCAN0.TMDF077.uint8_t[HL]
#define RSCAN0TMDF077HH RSCAN0.TMDF077.uint8_t[HH]
#define RSCAN0TMDF177 RSCAN0.TMDF177.uint32_t
#define RSCAN0TMDF177L RSCAN0.TMDF177.uint16_t[L]
#define RSCAN0TMDF177LL RSCAN0.TMDF177.uint8_t[LL]
#define RSCAN0TMDF177LH RSCAN0.TMDF177.uint8_t[LH]
#define RSCAN0TMDF177H RSCAN0.TMDF177.uint16_t[H]
#define RSCAN0TMDF177HL RSCAN0.TMDF177.uint8_t[HL]
#define RSCAN0TMDF177HH RSCAN0.TMDF177.uint8_t[HH]
#define RSCAN0TMID78 RSCAN0.TMID78.uint32_t
#define RSCAN0TMID78L RSCAN0.TMID78.uint16_t[L]
#define RSCAN0TMID78LL RSCAN0.TMID78.uint8_t[LL]
#define RSCAN0TMID78LH RSCAN0.TMID78.uint8_t[LH]
#define RSCAN0TMID78H RSCAN0.TMID78.uint16_t[H]
#define RSCAN0TMID78HL RSCAN0.TMID78.uint8_t[HL]
#define RSCAN0TMID78HH RSCAN0.TMID78.uint8_t[HH]
#define RSCAN0TMPTR78 RSCAN0.TMPTR78.uint32_t
#define RSCAN0TMPTR78H RSCAN0.TMPTR78.uint16_t[H]
#define RSCAN0TMPTR78HL RSCAN0.TMPTR78.uint8_t[HL]
#define RSCAN0TMPTR78HH RSCAN0.TMPTR78.uint8_t[HH]
#define RSCAN0TMDF078 RSCAN0.TMDF078.uint32_t
#define RSCAN0TMDF078L RSCAN0.TMDF078.uint16_t[L]
#define RSCAN0TMDF078LL RSCAN0.TMDF078.uint8_t[LL]
#define RSCAN0TMDF078LH RSCAN0.TMDF078.uint8_t[LH]
#define RSCAN0TMDF078H RSCAN0.TMDF078.uint16_t[H]
#define RSCAN0TMDF078HL RSCAN0.TMDF078.uint8_t[HL]
#define RSCAN0TMDF078HH RSCAN0.TMDF078.uint8_t[HH]
#define RSCAN0TMDF178 RSCAN0.TMDF178.uint32_t
#define RSCAN0TMDF178L RSCAN0.TMDF178.uint16_t[L]
#define RSCAN0TMDF178LL RSCAN0.TMDF178.uint8_t[LL]
#define RSCAN0TMDF178LH RSCAN0.TMDF178.uint8_t[LH]
#define RSCAN0TMDF178H RSCAN0.TMDF178.uint16_t[H]
#define RSCAN0TMDF178HL RSCAN0.TMDF178.uint8_t[HL]
#define RSCAN0TMDF178HH RSCAN0.TMDF178.uint8_t[HH]
#define RSCAN0TMID79 RSCAN0.TMID79.uint32_t
#define RSCAN0TMID79L RSCAN0.TMID79.uint16_t[L]
#define RSCAN0TMID79LL RSCAN0.TMID79.uint8_t[LL]
#define RSCAN0TMID79LH RSCAN0.TMID79.uint8_t[LH]
#define RSCAN0TMID79H RSCAN0.TMID79.uint16_t[H]
#define RSCAN0TMID79HL RSCAN0.TMID79.uint8_t[HL]
#define RSCAN0TMID79HH RSCAN0.TMID79.uint8_t[HH]
#define RSCAN0TMPTR79 RSCAN0.TMPTR79.uint32_t
#define RSCAN0TMPTR79H RSCAN0.TMPTR79.uint16_t[H]
#define RSCAN0TMPTR79HL RSCAN0.TMPTR79.uint8_t[HL]
#define RSCAN0TMPTR79HH RSCAN0.TMPTR79.uint8_t[HH]
#define RSCAN0TMDF079 RSCAN0.TMDF079.uint32_t
#define RSCAN0TMDF079L RSCAN0.TMDF079.uint16_t[L]
#define RSCAN0TMDF079LL RSCAN0.TMDF079.uint8_t[LL]
#define RSCAN0TMDF079LH RSCAN0.TMDF079.uint8_t[LH]
#define RSCAN0TMDF079H RSCAN0.TMDF079.uint16_t[H]
#define RSCAN0TMDF079HL RSCAN0.TMDF079.uint8_t[HL]
#define RSCAN0TMDF079HH RSCAN0.TMDF079.uint8_t[HH]
#define RSCAN0TMDF179 RSCAN0.TMDF179.uint32_t
#define RSCAN0TMDF179L RSCAN0.TMDF179.uint16_t[L]
#define RSCAN0TMDF179LL RSCAN0.TMDF179.uint8_t[LL]
#define RSCAN0TMDF179LH RSCAN0.TMDF179.uint8_t[LH]
#define RSCAN0TMDF179H RSCAN0.TMDF179.uint16_t[H]
#define RSCAN0TMDF179HL RSCAN0.TMDF179.uint8_t[HL]
#define RSCAN0TMDF179HH RSCAN0.TMDF179.uint8_t[HH]
#define RSCAN0TMID80 RSCAN0.TMID80.uint32_t
#define RSCAN0TMID80L RSCAN0.TMID80.uint16_t[L]
#define RSCAN0TMID80LL RSCAN0.TMID80.uint8_t[LL]
#define RSCAN0TMID80LH RSCAN0.TMID80.uint8_t[LH]
#define RSCAN0TMID80H RSCAN0.TMID80.uint16_t[H]
#define RSCAN0TMID80HL RSCAN0.TMID80.uint8_t[HL]
#define RSCAN0TMID80HH RSCAN0.TMID80.uint8_t[HH]
#define RSCAN0TMPTR80 RSCAN0.TMPTR80.uint32_t
#define RSCAN0TMPTR80H RSCAN0.TMPTR80.uint16_t[H]
#define RSCAN0TMPTR80HL RSCAN0.TMPTR80.uint8_t[HL]
#define RSCAN0TMPTR80HH RSCAN0.TMPTR80.uint8_t[HH]
#define RSCAN0TMDF080 RSCAN0.TMDF080.uint32_t
#define RSCAN0TMDF080L RSCAN0.TMDF080.uint16_t[L]
#define RSCAN0TMDF080LL RSCAN0.TMDF080.uint8_t[LL]
#define RSCAN0TMDF080LH RSCAN0.TMDF080.uint8_t[LH]
#define RSCAN0TMDF080H RSCAN0.TMDF080.uint16_t[H]
#define RSCAN0TMDF080HL RSCAN0.TMDF080.uint8_t[HL]
#define RSCAN0TMDF080HH RSCAN0.TMDF080.uint8_t[HH]
#define RSCAN0TMDF180 RSCAN0.TMDF180.uint32_t
#define RSCAN0TMDF180L RSCAN0.TMDF180.uint16_t[L]
#define RSCAN0TMDF180LL RSCAN0.TMDF180.uint8_t[LL]
#define RSCAN0TMDF180LH RSCAN0.TMDF180.uint8_t[LH]
#define RSCAN0TMDF180H RSCAN0.TMDF180.uint16_t[H]
#define RSCAN0TMDF180HL RSCAN0.TMDF180.uint8_t[HL]
#define RSCAN0TMDF180HH RSCAN0.TMDF180.uint8_t[HH]
#define RSCAN0TMID81 RSCAN0.TMID81.uint32_t
#define RSCAN0TMID81L RSCAN0.TMID81.uint16_t[L]
#define RSCAN0TMID81LL RSCAN0.TMID81.uint8_t[LL]
#define RSCAN0TMID81LH RSCAN0.TMID81.uint8_t[LH]
#define RSCAN0TMID81H RSCAN0.TMID81.uint16_t[H]
#define RSCAN0TMID81HL RSCAN0.TMID81.uint8_t[HL]
#define RSCAN0TMID81HH RSCAN0.TMID81.uint8_t[HH]
#define RSCAN0TMPTR81 RSCAN0.TMPTR81.uint32_t
#define RSCAN0TMPTR81H RSCAN0.TMPTR81.uint16_t[H]
#define RSCAN0TMPTR81HL RSCAN0.TMPTR81.uint8_t[HL]
#define RSCAN0TMPTR81HH RSCAN0.TMPTR81.uint8_t[HH]
#define RSCAN0TMDF081 RSCAN0.TMDF081.uint32_t
#define RSCAN0TMDF081L RSCAN0.TMDF081.uint16_t[L]
#define RSCAN0TMDF081LL RSCAN0.TMDF081.uint8_t[LL]
#define RSCAN0TMDF081LH RSCAN0.TMDF081.uint8_t[LH]
#define RSCAN0TMDF081H RSCAN0.TMDF081.uint16_t[H]
#define RSCAN0TMDF081HL RSCAN0.TMDF081.uint8_t[HL]
#define RSCAN0TMDF081HH RSCAN0.TMDF081.uint8_t[HH]
#define RSCAN0TMDF181 RSCAN0.TMDF181.uint32_t
#define RSCAN0TMDF181L RSCAN0.TMDF181.uint16_t[L]
#define RSCAN0TMDF181LL RSCAN0.TMDF181.uint8_t[LL]
#define RSCAN0TMDF181LH RSCAN0.TMDF181.uint8_t[LH]
#define RSCAN0TMDF181H RSCAN0.TMDF181.uint16_t[H]
#define RSCAN0TMDF181HL RSCAN0.TMDF181.uint8_t[HL]
#define RSCAN0TMDF181HH RSCAN0.TMDF181.uint8_t[HH]
#define RSCAN0TMID82 RSCAN0.TMID82.uint32_t
#define RSCAN0TMID82L RSCAN0.TMID82.uint16_t[L]
#define RSCAN0TMID82LL RSCAN0.TMID82.uint8_t[LL]
#define RSCAN0TMID82LH RSCAN0.TMID82.uint8_t[LH]
#define RSCAN0TMID82H RSCAN0.TMID82.uint16_t[H]
#define RSCAN0TMID82HL RSCAN0.TMID82.uint8_t[HL]
#define RSCAN0TMID82HH RSCAN0.TMID82.uint8_t[HH]
#define RSCAN0TMPTR82 RSCAN0.TMPTR82.uint32_t
#define RSCAN0TMPTR82H RSCAN0.TMPTR82.uint16_t[H]
#define RSCAN0TMPTR82HL RSCAN0.TMPTR82.uint8_t[HL]
#define RSCAN0TMPTR82HH RSCAN0.TMPTR82.uint8_t[HH]
#define RSCAN0TMDF082 RSCAN0.TMDF082.uint32_t
#define RSCAN0TMDF082L RSCAN0.TMDF082.uint16_t[L]
#define RSCAN0TMDF082LL RSCAN0.TMDF082.uint8_t[LL]
#define RSCAN0TMDF082LH RSCAN0.TMDF082.uint8_t[LH]
#define RSCAN0TMDF082H RSCAN0.TMDF082.uint16_t[H]
#define RSCAN0TMDF082HL RSCAN0.TMDF082.uint8_t[HL]
#define RSCAN0TMDF082HH RSCAN0.TMDF082.uint8_t[HH]
#define RSCAN0TMDF182 RSCAN0.TMDF182.uint32_t
#define RSCAN0TMDF182L RSCAN0.TMDF182.uint16_t[L]
#define RSCAN0TMDF182LL RSCAN0.TMDF182.uint8_t[LL]
#define RSCAN0TMDF182LH RSCAN0.TMDF182.uint8_t[LH]
#define RSCAN0TMDF182H RSCAN0.TMDF182.uint16_t[H]
#define RSCAN0TMDF182HL RSCAN0.TMDF182.uint8_t[HL]
#define RSCAN0TMDF182HH RSCAN0.TMDF182.uint8_t[HH]
#define RSCAN0TMID83 RSCAN0.TMID83.uint32_t
#define RSCAN0TMID83L RSCAN0.TMID83.uint16_t[L]
#define RSCAN0TMID83LL RSCAN0.TMID83.uint8_t[LL]
#define RSCAN0TMID83LH RSCAN0.TMID83.uint8_t[LH]
#define RSCAN0TMID83H RSCAN0.TMID83.uint16_t[H]
#define RSCAN0TMID83HL RSCAN0.TMID83.uint8_t[HL]
#define RSCAN0TMID83HH RSCAN0.TMID83.uint8_t[HH]
#define RSCAN0TMPTR83 RSCAN0.TMPTR83.uint32_t
#define RSCAN0TMPTR83H RSCAN0.TMPTR83.uint16_t[H]
#define RSCAN0TMPTR83HL RSCAN0.TMPTR83.uint8_t[HL]
#define RSCAN0TMPTR83HH RSCAN0.TMPTR83.uint8_t[HH]
#define RSCAN0TMDF083 RSCAN0.TMDF083.uint32_t
#define RSCAN0TMDF083L RSCAN0.TMDF083.uint16_t[L]
#define RSCAN0TMDF083LL RSCAN0.TMDF083.uint8_t[LL]
#define RSCAN0TMDF083LH RSCAN0.TMDF083.uint8_t[LH]
#define RSCAN0TMDF083H RSCAN0.TMDF083.uint16_t[H]
#define RSCAN0TMDF083HL RSCAN0.TMDF083.uint8_t[HL]
#define RSCAN0TMDF083HH RSCAN0.TMDF083.uint8_t[HH]
#define RSCAN0TMDF183 RSCAN0.TMDF183.uint32_t
#define RSCAN0TMDF183L RSCAN0.TMDF183.uint16_t[L]
#define RSCAN0TMDF183LL RSCAN0.TMDF183.uint8_t[LL]
#define RSCAN0TMDF183LH RSCAN0.TMDF183.uint8_t[LH]
#define RSCAN0TMDF183H RSCAN0.TMDF183.uint16_t[H]
#define RSCAN0TMDF183HL RSCAN0.TMDF183.uint8_t[HL]
#define RSCAN0TMDF183HH RSCAN0.TMDF183.uint8_t[HH]
#define RSCAN0TMID84 RSCAN0.TMID84.uint32_t
#define RSCAN0TMID84L RSCAN0.TMID84.uint16_t[L]
#define RSCAN0TMID84LL RSCAN0.TMID84.uint8_t[LL]
#define RSCAN0TMID84LH RSCAN0.TMID84.uint8_t[LH]
#define RSCAN0TMID84H RSCAN0.TMID84.uint16_t[H]
#define RSCAN0TMID84HL RSCAN0.TMID84.uint8_t[HL]
#define RSCAN0TMID84HH RSCAN0.TMID84.uint8_t[HH]
#define RSCAN0TMPTR84 RSCAN0.TMPTR84.uint32_t
#define RSCAN0TMPTR84H RSCAN0.TMPTR84.uint16_t[H]
#define RSCAN0TMPTR84HL RSCAN0.TMPTR84.uint8_t[HL]
#define RSCAN0TMPTR84HH RSCAN0.TMPTR84.uint8_t[HH]
#define RSCAN0TMDF084 RSCAN0.TMDF084.uint32_t
#define RSCAN0TMDF084L RSCAN0.TMDF084.uint16_t[L]
#define RSCAN0TMDF084LL RSCAN0.TMDF084.uint8_t[LL]
#define RSCAN0TMDF084LH RSCAN0.TMDF084.uint8_t[LH]
#define RSCAN0TMDF084H RSCAN0.TMDF084.uint16_t[H]
#define RSCAN0TMDF084HL RSCAN0.TMDF084.uint8_t[HL]
#define RSCAN0TMDF084HH RSCAN0.TMDF084.uint8_t[HH]
#define RSCAN0TMDF184 RSCAN0.TMDF184.uint32_t
#define RSCAN0TMDF184L RSCAN0.TMDF184.uint16_t[L]
#define RSCAN0TMDF184LL RSCAN0.TMDF184.uint8_t[LL]
#define RSCAN0TMDF184LH RSCAN0.TMDF184.uint8_t[LH]
#define RSCAN0TMDF184H RSCAN0.TMDF184.uint16_t[H]
#define RSCAN0TMDF184HL RSCAN0.TMDF184.uint8_t[HL]
#define RSCAN0TMDF184HH RSCAN0.TMDF184.uint8_t[HH]
#define RSCAN0TMID85 RSCAN0.TMID85.uint32_t
#define RSCAN0TMID85L RSCAN0.TMID85.uint16_t[L]
#define RSCAN0TMID85LL RSCAN0.TMID85.uint8_t[LL]
#define RSCAN0TMID85LH RSCAN0.TMID85.uint8_t[LH]
#define RSCAN0TMID85H RSCAN0.TMID85.uint16_t[H]
#define RSCAN0TMID85HL RSCAN0.TMID85.uint8_t[HL]
#define RSCAN0TMID85HH RSCAN0.TMID85.uint8_t[HH]
#define RSCAN0TMPTR85 RSCAN0.TMPTR85.uint32_t
#define RSCAN0TMPTR85H RSCAN0.TMPTR85.uint16_t[H]
#define RSCAN0TMPTR85HL RSCAN0.TMPTR85.uint8_t[HL]
#define RSCAN0TMPTR85HH RSCAN0.TMPTR85.uint8_t[HH]
#define RSCAN0TMDF085 RSCAN0.TMDF085.uint32_t
#define RSCAN0TMDF085L RSCAN0.TMDF085.uint16_t[L]
#define RSCAN0TMDF085LL RSCAN0.TMDF085.uint8_t[LL]
#define RSCAN0TMDF085LH RSCAN0.TMDF085.uint8_t[LH]
#define RSCAN0TMDF085H RSCAN0.TMDF085.uint16_t[H]
#define RSCAN0TMDF085HL RSCAN0.TMDF085.uint8_t[HL]
#define RSCAN0TMDF085HH RSCAN0.TMDF085.uint8_t[HH]
#define RSCAN0TMDF185 RSCAN0.TMDF185.uint32_t
#define RSCAN0TMDF185L RSCAN0.TMDF185.uint16_t[L]
#define RSCAN0TMDF185LL RSCAN0.TMDF185.uint8_t[LL]
#define RSCAN0TMDF185LH RSCAN0.TMDF185.uint8_t[LH]
#define RSCAN0TMDF185H RSCAN0.TMDF185.uint16_t[H]
#define RSCAN0TMDF185HL RSCAN0.TMDF185.uint8_t[HL]
#define RSCAN0TMDF185HH RSCAN0.TMDF185.uint8_t[HH]
#define RSCAN0TMID86 RSCAN0.TMID86.uint32_t
#define RSCAN0TMID86L RSCAN0.TMID86.uint16_t[L]
#define RSCAN0TMID86LL RSCAN0.TMID86.uint8_t[LL]
#define RSCAN0TMID86LH RSCAN0.TMID86.uint8_t[LH]
#define RSCAN0TMID86H RSCAN0.TMID86.uint16_t[H]
#define RSCAN0TMID86HL RSCAN0.TMID86.uint8_t[HL]
#define RSCAN0TMID86HH RSCAN0.TMID86.uint8_t[HH]
#define RSCAN0TMPTR86 RSCAN0.TMPTR86.uint32_t
#define RSCAN0TMPTR86H RSCAN0.TMPTR86.uint16_t[H]
#define RSCAN0TMPTR86HL RSCAN0.TMPTR86.uint8_t[HL]
#define RSCAN0TMPTR86HH RSCAN0.TMPTR86.uint8_t[HH]
#define RSCAN0TMDF086 RSCAN0.TMDF086.uint32_t
#define RSCAN0TMDF086L RSCAN0.TMDF086.uint16_t[L]
#define RSCAN0TMDF086LL RSCAN0.TMDF086.uint8_t[LL]
#define RSCAN0TMDF086LH RSCAN0.TMDF086.uint8_t[LH]
#define RSCAN0TMDF086H RSCAN0.TMDF086.uint16_t[H]
#define RSCAN0TMDF086HL RSCAN0.TMDF086.uint8_t[HL]
#define RSCAN0TMDF086HH RSCAN0.TMDF086.uint8_t[HH]
#define RSCAN0TMDF186 RSCAN0.TMDF186.uint32_t
#define RSCAN0TMDF186L RSCAN0.TMDF186.uint16_t[L]
#define RSCAN0TMDF186LL RSCAN0.TMDF186.uint8_t[LL]
#define RSCAN0TMDF186LH RSCAN0.TMDF186.uint8_t[LH]
#define RSCAN0TMDF186H RSCAN0.TMDF186.uint16_t[H]
#define RSCAN0TMDF186HL RSCAN0.TMDF186.uint8_t[HL]
#define RSCAN0TMDF186HH RSCAN0.TMDF186.uint8_t[HH]
#define RSCAN0TMID87 RSCAN0.TMID87.uint32_t
#define RSCAN0TMID87L RSCAN0.TMID87.uint16_t[L]
#define RSCAN0TMID87LL RSCAN0.TMID87.uint8_t[LL]
#define RSCAN0TMID87LH RSCAN0.TMID87.uint8_t[LH]
#define RSCAN0TMID87H RSCAN0.TMID87.uint16_t[H]
#define RSCAN0TMID87HL RSCAN0.TMID87.uint8_t[HL]
#define RSCAN0TMID87HH RSCAN0.TMID87.uint8_t[HH]
#define RSCAN0TMPTR87 RSCAN0.TMPTR87.uint32_t
#define RSCAN0TMPTR87H RSCAN0.TMPTR87.uint16_t[H]
#define RSCAN0TMPTR87HL RSCAN0.TMPTR87.uint8_t[HL]
#define RSCAN0TMPTR87HH RSCAN0.TMPTR87.uint8_t[HH]
#define RSCAN0TMDF087 RSCAN0.TMDF087.uint32_t
#define RSCAN0TMDF087L RSCAN0.TMDF087.uint16_t[L]
#define RSCAN0TMDF087LL RSCAN0.TMDF087.uint8_t[LL]
#define RSCAN0TMDF087LH RSCAN0.TMDF087.uint8_t[LH]
#define RSCAN0TMDF087H RSCAN0.TMDF087.uint16_t[H]
#define RSCAN0TMDF087HL RSCAN0.TMDF087.uint8_t[HL]
#define RSCAN0TMDF087HH RSCAN0.TMDF087.uint8_t[HH]
#define RSCAN0TMDF187 RSCAN0.TMDF187.uint32_t
#define RSCAN0TMDF187L RSCAN0.TMDF187.uint16_t[L]
#define RSCAN0TMDF187LL RSCAN0.TMDF187.uint8_t[LL]
#define RSCAN0TMDF187LH RSCAN0.TMDF187.uint8_t[LH]
#define RSCAN0TMDF187H RSCAN0.TMDF187.uint16_t[H]
#define RSCAN0TMDF187HL RSCAN0.TMDF187.uint8_t[HL]
#define RSCAN0TMDF187HH RSCAN0.TMDF187.uint8_t[HH]
#define RSCAN0TMID88 RSCAN0.TMID88.uint32_t
#define RSCAN0TMID88L RSCAN0.TMID88.uint16_t[L]
#define RSCAN0TMID88LL RSCAN0.TMID88.uint8_t[LL]
#define RSCAN0TMID88LH RSCAN0.TMID88.uint8_t[LH]
#define RSCAN0TMID88H RSCAN0.TMID88.uint16_t[H]
#define RSCAN0TMID88HL RSCAN0.TMID88.uint8_t[HL]
#define RSCAN0TMID88HH RSCAN0.TMID88.uint8_t[HH]
#define RSCAN0TMPTR88 RSCAN0.TMPTR88.uint32_t
#define RSCAN0TMPTR88H RSCAN0.TMPTR88.uint16_t[H]
#define RSCAN0TMPTR88HL RSCAN0.TMPTR88.uint8_t[HL]
#define RSCAN0TMPTR88HH RSCAN0.TMPTR88.uint8_t[HH]
#define RSCAN0TMDF088 RSCAN0.TMDF088.uint32_t
#define RSCAN0TMDF088L RSCAN0.TMDF088.uint16_t[L]
#define RSCAN0TMDF088LL RSCAN0.TMDF088.uint8_t[LL]
#define RSCAN0TMDF088LH RSCAN0.TMDF088.uint8_t[LH]
#define RSCAN0TMDF088H RSCAN0.TMDF088.uint16_t[H]
#define RSCAN0TMDF088HL RSCAN0.TMDF088.uint8_t[HL]
#define RSCAN0TMDF088HH RSCAN0.TMDF088.uint8_t[HH]
#define RSCAN0TMDF188 RSCAN0.TMDF188.uint32_t
#define RSCAN0TMDF188L RSCAN0.TMDF188.uint16_t[L]
#define RSCAN0TMDF188LL RSCAN0.TMDF188.uint8_t[LL]
#define RSCAN0TMDF188LH RSCAN0.TMDF188.uint8_t[LH]
#define RSCAN0TMDF188H RSCAN0.TMDF188.uint16_t[H]
#define RSCAN0TMDF188HL RSCAN0.TMDF188.uint8_t[HL]
#define RSCAN0TMDF188HH RSCAN0.TMDF188.uint8_t[HH]
#define RSCAN0TMID89 RSCAN0.TMID89.uint32_t
#define RSCAN0TMID89L RSCAN0.TMID89.uint16_t[L]
#define RSCAN0TMID89LL RSCAN0.TMID89.uint8_t[LL]
#define RSCAN0TMID89LH RSCAN0.TMID89.uint8_t[LH]
#define RSCAN0TMID89H RSCAN0.TMID89.uint16_t[H]
#define RSCAN0TMID89HL RSCAN0.TMID89.uint8_t[HL]
#define RSCAN0TMID89HH RSCAN0.TMID89.uint8_t[HH]
#define RSCAN0TMPTR89 RSCAN0.TMPTR89.uint32_t
#define RSCAN0TMPTR89H RSCAN0.TMPTR89.uint16_t[H]
#define RSCAN0TMPTR89HL RSCAN0.TMPTR89.uint8_t[HL]
#define RSCAN0TMPTR89HH RSCAN0.TMPTR89.uint8_t[HH]
#define RSCAN0TMDF089 RSCAN0.TMDF089.uint32_t
#define RSCAN0TMDF089L RSCAN0.TMDF089.uint16_t[L]
#define RSCAN0TMDF089LL RSCAN0.TMDF089.uint8_t[LL]
#define RSCAN0TMDF089LH RSCAN0.TMDF089.uint8_t[LH]
#define RSCAN0TMDF089H RSCAN0.TMDF089.uint16_t[H]
#define RSCAN0TMDF089HL RSCAN0.TMDF089.uint8_t[HL]
#define RSCAN0TMDF089HH RSCAN0.TMDF089.uint8_t[HH]
#define RSCAN0TMDF189 RSCAN0.TMDF189.uint32_t
#define RSCAN0TMDF189L RSCAN0.TMDF189.uint16_t[L]
#define RSCAN0TMDF189LL RSCAN0.TMDF189.uint8_t[LL]
#define RSCAN0TMDF189LH RSCAN0.TMDF189.uint8_t[LH]
#define RSCAN0TMDF189H RSCAN0.TMDF189.uint16_t[H]
#define RSCAN0TMDF189HL RSCAN0.TMDF189.uint8_t[HL]
#define RSCAN0TMDF189HH RSCAN0.TMDF189.uint8_t[HH]
#define RSCAN0TMID90 RSCAN0.TMID90.uint32_t
#define RSCAN0TMID90L RSCAN0.TMID90.uint16_t[L]
#define RSCAN0TMID90LL RSCAN0.TMID90.uint8_t[LL]
#define RSCAN0TMID90LH RSCAN0.TMID90.uint8_t[LH]
#define RSCAN0TMID90H RSCAN0.TMID90.uint16_t[H]
#define RSCAN0TMID90HL RSCAN0.TMID90.uint8_t[HL]
#define RSCAN0TMID90HH RSCAN0.TMID90.uint8_t[HH]
#define RSCAN0TMPTR90 RSCAN0.TMPTR90.uint32_t
#define RSCAN0TMPTR90H RSCAN0.TMPTR90.uint16_t[H]
#define RSCAN0TMPTR90HL RSCAN0.TMPTR90.uint8_t[HL]
#define RSCAN0TMPTR90HH RSCAN0.TMPTR90.uint8_t[HH]
#define RSCAN0TMDF090 RSCAN0.TMDF090.uint32_t
#define RSCAN0TMDF090L RSCAN0.TMDF090.uint16_t[L]
#define RSCAN0TMDF090LL RSCAN0.TMDF090.uint8_t[LL]
#define RSCAN0TMDF090LH RSCAN0.TMDF090.uint8_t[LH]
#define RSCAN0TMDF090H RSCAN0.TMDF090.uint16_t[H]
#define RSCAN0TMDF090HL RSCAN0.TMDF090.uint8_t[HL]
#define RSCAN0TMDF090HH RSCAN0.TMDF090.uint8_t[HH]
#define RSCAN0TMDF190 RSCAN0.TMDF190.uint32_t
#define RSCAN0TMDF190L RSCAN0.TMDF190.uint16_t[L]
#define RSCAN0TMDF190LL RSCAN0.TMDF190.uint8_t[LL]
#define RSCAN0TMDF190LH RSCAN0.TMDF190.uint8_t[LH]
#define RSCAN0TMDF190H RSCAN0.TMDF190.uint16_t[H]
#define RSCAN0TMDF190HL RSCAN0.TMDF190.uint8_t[HL]
#define RSCAN0TMDF190HH RSCAN0.TMDF190.uint8_t[HH]
#define RSCAN0TMID91 RSCAN0.TMID91.uint32_t
#define RSCAN0TMID91L RSCAN0.TMID91.uint16_t[L]
#define RSCAN0TMID91LL RSCAN0.TMID91.uint8_t[LL]
#define RSCAN0TMID91LH RSCAN0.TMID91.uint8_t[LH]
#define RSCAN0TMID91H RSCAN0.TMID91.uint16_t[H]
#define RSCAN0TMID91HL RSCAN0.TMID91.uint8_t[HL]
#define RSCAN0TMID91HH RSCAN0.TMID91.uint8_t[HH]
#define RSCAN0TMPTR91 RSCAN0.TMPTR91.uint32_t
#define RSCAN0TMPTR91H RSCAN0.TMPTR91.uint16_t[H]
#define RSCAN0TMPTR91HL RSCAN0.TMPTR91.uint8_t[HL]
#define RSCAN0TMPTR91HH RSCAN0.TMPTR91.uint8_t[HH]
#define RSCAN0TMDF091 RSCAN0.TMDF091.uint32_t
#define RSCAN0TMDF091L RSCAN0.TMDF091.uint16_t[L]
#define RSCAN0TMDF091LL RSCAN0.TMDF091.uint8_t[LL]
#define RSCAN0TMDF091LH RSCAN0.TMDF091.uint8_t[LH]
#define RSCAN0TMDF091H RSCAN0.TMDF091.uint16_t[H]
#define RSCAN0TMDF091HL RSCAN0.TMDF091.uint8_t[HL]
#define RSCAN0TMDF091HH RSCAN0.TMDF091.uint8_t[HH]
#define RSCAN0TMDF191 RSCAN0.TMDF191.uint32_t
#define RSCAN0TMDF191L RSCAN0.TMDF191.uint16_t[L]
#define RSCAN0TMDF191LL RSCAN0.TMDF191.uint8_t[LL]
#define RSCAN0TMDF191LH RSCAN0.TMDF191.uint8_t[LH]
#define RSCAN0TMDF191H RSCAN0.TMDF191.uint16_t[H]
#define RSCAN0TMDF191HL RSCAN0.TMDF191.uint8_t[HL]
#define RSCAN0TMDF191HH RSCAN0.TMDF191.uint8_t[HH]
#define RSCAN0TMID92 RSCAN0.TMID92.uint32_t
#define RSCAN0TMID92L RSCAN0.TMID92.uint16_t[L]
#define RSCAN0TMID92LL RSCAN0.TMID92.uint8_t[LL]
#define RSCAN0TMID92LH RSCAN0.TMID92.uint8_t[LH]
#define RSCAN0TMID92H RSCAN0.TMID92.uint16_t[H]
#define RSCAN0TMID92HL RSCAN0.TMID92.uint8_t[HL]
#define RSCAN0TMID92HH RSCAN0.TMID92.uint8_t[HH]
#define RSCAN0TMPTR92 RSCAN0.TMPTR92.uint32_t
#define RSCAN0TMPTR92H RSCAN0.TMPTR92.uint16_t[H]
#define RSCAN0TMPTR92HL RSCAN0.TMPTR92.uint8_t[HL]
#define RSCAN0TMPTR92HH RSCAN0.TMPTR92.uint8_t[HH]
#define RSCAN0TMDF092 RSCAN0.TMDF092.uint32_t
#define RSCAN0TMDF092L RSCAN0.TMDF092.uint16_t[L]
#define RSCAN0TMDF092LL RSCAN0.TMDF092.uint8_t[LL]
#define RSCAN0TMDF092LH RSCAN0.TMDF092.uint8_t[LH]
#define RSCAN0TMDF092H RSCAN0.TMDF092.uint16_t[H]
#define RSCAN0TMDF092HL RSCAN0.TMDF092.uint8_t[HL]
#define RSCAN0TMDF092HH RSCAN0.TMDF092.uint8_t[HH]
#define RSCAN0TMDF192 RSCAN0.TMDF192.uint32_t
#define RSCAN0TMDF192L RSCAN0.TMDF192.uint16_t[L]
#define RSCAN0TMDF192LL RSCAN0.TMDF192.uint8_t[LL]
#define RSCAN0TMDF192LH RSCAN0.TMDF192.uint8_t[LH]
#define RSCAN0TMDF192H RSCAN0.TMDF192.uint16_t[H]
#define RSCAN0TMDF192HL RSCAN0.TMDF192.uint8_t[HL]
#define RSCAN0TMDF192HH RSCAN0.TMDF192.uint8_t[HH]
#define RSCAN0TMID93 RSCAN0.TMID93.uint32_t
#define RSCAN0TMID93L RSCAN0.TMID93.uint16_t[L]
#define RSCAN0TMID93LL RSCAN0.TMID93.uint8_t[LL]
#define RSCAN0TMID93LH RSCAN0.TMID93.uint8_t[LH]
#define RSCAN0TMID93H RSCAN0.TMID93.uint16_t[H]
#define RSCAN0TMID93HL RSCAN0.TMID93.uint8_t[HL]
#define RSCAN0TMID93HH RSCAN0.TMID93.uint8_t[HH]
#define RSCAN0TMPTR93 RSCAN0.TMPTR93.uint32_t
#define RSCAN0TMPTR93H RSCAN0.TMPTR93.uint16_t[H]
#define RSCAN0TMPTR93HL RSCAN0.TMPTR93.uint8_t[HL]
#define RSCAN0TMPTR93HH RSCAN0.TMPTR93.uint8_t[HH]
#define RSCAN0TMDF093 RSCAN0.TMDF093.uint32_t
#define RSCAN0TMDF093L RSCAN0.TMDF093.uint16_t[L]
#define RSCAN0TMDF093LL RSCAN0.TMDF093.uint8_t[LL]
#define RSCAN0TMDF093LH RSCAN0.TMDF093.uint8_t[LH]
#define RSCAN0TMDF093H RSCAN0.TMDF093.uint16_t[H]
#define RSCAN0TMDF093HL RSCAN0.TMDF093.uint8_t[HL]
#define RSCAN0TMDF093HH RSCAN0.TMDF093.uint8_t[HH]
#define RSCAN0TMDF193 RSCAN0.TMDF193.uint32_t
#define RSCAN0TMDF193L RSCAN0.TMDF193.uint16_t[L]
#define RSCAN0TMDF193LL RSCAN0.TMDF193.uint8_t[LL]
#define RSCAN0TMDF193LH RSCAN0.TMDF193.uint8_t[LH]
#define RSCAN0TMDF193H RSCAN0.TMDF193.uint16_t[H]
#define RSCAN0TMDF193HL RSCAN0.TMDF193.uint8_t[HL]
#define RSCAN0TMDF193HH RSCAN0.TMDF193.uint8_t[HH]
#define RSCAN0TMID94 RSCAN0.TMID94.uint32_t
#define RSCAN0TMID94L RSCAN0.TMID94.uint16_t[L]
#define RSCAN0TMID94LL RSCAN0.TMID94.uint8_t[LL]
#define RSCAN0TMID94LH RSCAN0.TMID94.uint8_t[LH]
#define RSCAN0TMID94H RSCAN0.TMID94.uint16_t[H]
#define RSCAN0TMID94HL RSCAN0.TMID94.uint8_t[HL]
#define RSCAN0TMID94HH RSCAN0.TMID94.uint8_t[HH]
#define RSCAN0TMPTR94 RSCAN0.TMPTR94.uint32_t
#define RSCAN0TMPTR94H RSCAN0.TMPTR94.uint16_t[H]
#define RSCAN0TMPTR94HL RSCAN0.TMPTR94.uint8_t[HL]
#define RSCAN0TMPTR94HH RSCAN0.TMPTR94.uint8_t[HH]
#define RSCAN0TMDF094 RSCAN0.TMDF094.uint32_t
#define RSCAN0TMDF094L RSCAN0.TMDF094.uint16_t[L]
#define RSCAN0TMDF094LL RSCAN0.TMDF094.uint8_t[LL]
#define RSCAN0TMDF094LH RSCAN0.TMDF094.uint8_t[LH]
#define RSCAN0TMDF094H RSCAN0.TMDF094.uint16_t[H]
#define RSCAN0TMDF094HL RSCAN0.TMDF094.uint8_t[HL]
#define RSCAN0TMDF094HH RSCAN0.TMDF094.uint8_t[HH]
#define RSCAN0TMDF194 RSCAN0.TMDF194.uint32_t
#define RSCAN0TMDF194L RSCAN0.TMDF194.uint16_t[L]
#define RSCAN0TMDF194LL RSCAN0.TMDF194.uint8_t[LL]
#define RSCAN0TMDF194LH RSCAN0.TMDF194.uint8_t[LH]
#define RSCAN0TMDF194H RSCAN0.TMDF194.uint16_t[H]
#define RSCAN0TMDF194HL RSCAN0.TMDF194.uint8_t[HL]
#define RSCAN0TMDF194HH RSCAN0.TMDF194.uint8_t[HH]
#define RSCAN0TMID95 RSCAN0.TMID95.uint32_t
#define RSCAN0TMID95L RSCAN0.TMID95.uint16_t[L]
#define RSCAN0TMID95LL RSCAN0.TMID95.uint8_t[LL]
#define RSCAN0TMID95LH RSCAN0.TMID95.uint8_t[LH]
#define RSCAN0TMID95H RSCAN0.TMID95.uint16_t[H]
#define RSCAN0TMID95HL RSCAN0.TMID95.uint8_t[HL]
#define RSCAN0TMID95HH RSCAN0.TMID95.uint8_t[HH]
#define RSCAN0TMPTR95 RSCAN0.TMPTR95.uint32_t
#define RSCAN0TMPTR95H RSCAN0.TMPTR95.uint16_t[H]
#define RSCAN0TMPTR95HL RSCAN0.TMPTR95.uint8_t[HL]
#define RSCAN0TMPTR95HH RSCAN0.TMPTR95.uint8_t[HH]
#define RSCAN0TMDF095 RSCAN0.TMDF095.uint32_t
#define RSCAN0TMDF095L RSCAN0.TMDF095.uint16_t[L]
#define RSCAN0TMDF095LL RSCAN0.TMDF095.uint8_t[LL]
#define RSCAN0TMDF095LH RSCAN0.TMDF095.uint8_t[LH]
#define RSCAN0TMDF095H RSCAN0.TMDF095.uint16_t[H]
#define RSCAN0TMDF095HL RSCAN0.TMDF095.uint8_t[HL]
#define RSCAN0TMDF095HH RSCAN0.TMDF095.uint8_t[HH]
#define RSCAN0TMDF195 RSCAN0.TMDF195.uint32_t
#define RSCAN0TMDF195L RSCAN0.TMDF195.uint16_t[L]
#define RSCAN0TMDF195LL RSCAN0.TMDF195.uint8_t[LL]
#define RSCAN0TMDF195LH RSCAN0.TMDF195.uint8_t[LH]
#define RSCAN0TMDF195H RSCAN0.TMDF195.uint16_t[H]
#define RSCAN0TMDF195HL RSCAN0.TMDF195.uint8_t[HL]
#define RSCAN0TMDF195HH RSCAN0.TMDF195.uint8_t[HH]
#define RSCAN0THLACC0 RSCAN0.THLACC0.uint32_t
#define RSCAN0THLACC0L RSCAN0.THLACC0.uint16_t[L]
#define RSCAN0THLACC0LL RSCAN0.THLACC0.uint8_t[LL]
#define RSCAN0THLACC0LH RSCAN0.THLACC0.uint8_t[LH]
#define RSCAN0THLACC1 RSCAN0.THLACC1.uint32_t
#define RSCAN0THLACC1L RSCAN0.THLACC1.uint16_t[L]
#define RSCAN0THLACC1LL RSCAN0.THLACC1.uint8_t[LL]
#define RSCAN0THLACC1LH RSCAN0.THLACC1.uint8_t[LH]
#define RSCAN0THLACC2 RSCAN0.THLACC2.uint32_t
#define RSCAN0THLACC2L RSCAN0.THLACC2.uint16_t[L]
#define RSCAN0THLACC2LL RSCAN0.THLACC2.uint8_t[LL]
#define RSCAN0THLACC2LH RSCAN0.THLACC2.uint8_t[LH]
#define RSCAN0THLACC3 RSCAN0.THLACC3.uint32_t
#define RSCAN0THLACC3L RSCAN0.THLACC3.uint16_t[L]
#define RSCAN0THLACC3LL RSCAN0.THLACC3.uint8_t[LL]
#define RSCAN0THLACC3LH RSCAN0.THLACC3.uint8_t[LH]
#define RSCAN0THLACC4 RSCAN0.THLACC4.uint32_t
#define RSCAN0THLACC4L RSCAN0.THLACC4.uint16_t[L]
#define RSCAN0THLACC4LL RSCAN0.THLACC4.uint8_t[LL]
#define RSCAN0THLACC4LH RSCAN0.THLACC4.uint8_t[LH]
#define RSCAN0THLACC5 RSCAN0.THLACC5.uint32_t
#define RSCAN0THLACC5L RSCAN0.THLACC5.uint16_t[L]
#define RSCAN0THLACC5LL RSCAN0.THLACC5.uint8_t[LL]
#define RSCAN0THLACC5LH RSCAN0.THLACC5.uint8_t[LH]
#define RSCAN0RPGACC0 RSCAN0.RPGACC0.uint32_t
#define RSCAN0RPGACC0L RSCAN0.RPGACC0.uint16_t[L]
#define RSCAN0RPGACC0LL RSCAN0.RPGACC0.uint8_t[LL]
#define RSCAN0RPGACC0LH RSCAN0.RPGACC0.uint8_t[LH]
#define RSCAN0RPGACC0H RSCAN0.RPGACC0.uint16_t[H]
#define RSCAN0RPGACC0HL RSCAN0.RPGACC0.uint8_t[HL]
#define RSCAN0RPGACC0HH RSCAN0.RPGACC0.uint8_t[HH]
#define RSCAN0RPGACC1 RSCAN0.RPGACC1.uint32_t
#define RSCAN0RPGACC1L RSCAN0.RPGACC1.uint16_t[L]
#define RSCAN0RPGACC1LL RSCAN0.RPGACC1.uint8_t[LL]
#define RSCAN0RPGACC1LH RSCAN0.RPGACC1.uint8_t[LH]
#define RSCAN0RPGACC1H RSCAN0.RPGACC1.uint16_t[H]
#define RSCAN0RPGACC1HL RSCAN0.RPGACC1.uint8_t[HL]
#define RSCAN0RPGACC1HH RSCAN0.RPGACC1.uint8_t[HH]
#define RSCAN0RPGACC2 RSCAN0.RPGACC2.uint32_t
#define RSCAN0RPGACC2L RSCAN0.RPGACC2.uint16_t[L]
#define RSCAN0RPGACC2LL RSCAN0.RPGACC2.uint8_t[LL]
#define RSCAN0RPGACC2LH RSCAN0.RPGACC2.uint8_t[LH]
#define RSCAN0RPGACC2H RSCAN0.RPGACC2.uint16_t[H]
#define RSCAN0RPGACC2HL RSCAN0.RPGACC2.uint8_t[HL]
#define RSCAN0RPGACC2HH RSCAN0.RPGACC2.uint8_t[HH]
#define RSCAN0RPGACC3 RSCAN0.RPGACC3.uint32_t
#define RSCAN0RPGACC3L RSCAN0.RPGACC3.uint16_t[L]
#define RSCAN0RPGACC3LL RSCAN0.RPGACC3.uint8_t[LL]
#define RSCAN0RPGACC3LH RSCAN0.RPGACC3.uint8_t[LH]
#define RSCAN0RPGACC3H RSCAN0.RPGACC3.uint16_t[H]
#define RSCAN0RPGACC3HL RSCAN0.RPGACC3.uint8_t[HL]
#define RSCAN0RPGACC3HH RSCAN0.RPGACC3.uint8_t[HH]
#define RSCAN0RPGACC4 RSCAN0.RPGACC4.uint32_t
#define RSCAN0RPGACC4L RSCAN0.RPGACC4.uint16_t[L]
#define RSCAN0RPGACC4LL RSCAN0.RPGACC4.uint8_t[LL]
#define RSCAN0RPGACC4LH RSCAN0.RPGACC4.uint8_t[LH]
#define RSCAN0RPGACC4H RSCAN0.RPGACC4.uint16_t[H]
#define RSCAN0RPGACC4HL RSCAN0.RPGACC4.uint8_t[HL]
#define RSCAN0RPGACC4HH RSCAN0.RPGACC4.uint8_t[HH]
#define RSCAN0RPGACC5 RSCAN0.RPGACC5.uint32_t
#define RSCAN0RPGACC5L RSCAN0.RPGACC5.uint16_t[L]
#define RSCAN0RPGACC5LL RSCAN0.RPGACC5.uint8_t[LL]
#define RSCAN0RPGACC5LH RSCAN0.RPGACC5.uint8_t[LH]
#define RSCAN0RPGACC5H RSCAN0.RPGACC5.uint16_t[H]
#define RSCAN0RPGACC5HL RSCAN0.RPGACC5.uint8_t[HL]
#define RSCAN0RPGACC5HH RSCAN0.RPGACC5.uint8_t[HH]
#define RSCAN0RPGACC6 RSCAN0.RPGACC6.uint32_t
#define RSCAN0RPGACC6L RSCAN0.RPGACC6.uint16_t[L]
#define RSCAN0RPGACC6LL RSCAN0.RPGACC6.uint8_t[LL]
#define RSCAN0RPGACC6LH RSCAN0.RPGACC6.uint8_t[LH]
#define RSCAN0RPGACC6H RSCAN0.RPGACC6.uint16_t[H]
#define RSCAN0RPGACC6HL RSCAN0.RPGACC6.uint8_t[HL]
#define RSCAN0RPGACC6HH RSCAN0.RPGACC6.uint8_t[HH]
#define RSCAN0RPGACC7 RSCAN0.RPGACC7.uint32_t
#define RSCAN0RPGACC7L RSCAN0.RPGACC7.uint16_t[L]
#define RSCAN0RPGACC7LL RSCAN0.RPGACC7.uint8_t[LL]
#define RSCAN0RPGACC7LH RSCAN0.RPGACC7.uint8_t[LH]
#define RSCAN0RPGACC7H RSCAN0.RPGACC7.uint16_t[H]
#define RSCAN0RPGACC7HL RSCAN0.RPGACC7.uint8_t[HL]
#define RSCAN0RPGACC7HH RSCAN0.RPGACC7.uint8_t[HH]
#define RSCAN0RPGACC8 RSCAN0.RPGACC8.uint32_t
#define RSCAN0RPGACC8L RSCAN0.RPGACC8.uint16_t[L]
#define RSCAN0RPGACC8LL RSCAN0.RPGACC8.uint8_t[LL]
#define RSCAN0RPGACC8LH RSCAN0.RPGACC8.uint8_t[LH]
#define RSCAN0RPGACC8H RSCAN0.RPGACC8.uint16_t[H]
#define RSCAN0RPGACC8HL RSCAN0.RPGACC8.uint8_t[HL]
#define RSCAN0RPGACC8HH RSCAN0.RPGACC8.uint8_t[HH]
#define RSCAN0RPGACC9 RSCAN0.RPGACC9.uint32_t
#define RSCAN0RPGACC9L RSCAN0.RPGACC9.uint16_t[L]
#define RSCAN0RPGACC9LL RSCAN0.RPGACC9.uint8_t[LL]
#define RSCAN0RPGACC9LH RSCAN0.RPGACC9.uint8_t[LH]
#define RSCAN0RPGACC9H RSCAN0.RPGACC9.uint16_t[H]
#define RSCAN0RPGACC9HL RSCAN0.RPGACC9.uint8_t[HL]
#define RSCAN0RPGACC9HH RSCAN0.RPGACC9.uint8_t[HH]
#define RSCAN0RPGACC10 RSCAN0.RPGACC10.uint32_t
#define RSCAN0RPGACC10L RSCAN0.RPGACC10.uint16_t[L]
#define RSCAN0RPGACC10LL RSCAN0.RPGACC10.uint8_t[LL]
#define RSCAN0RPGACC10LH RSCAN0.RPGACC10.uint8_t[LH]
#define RSCAN0RPGACC10H RSCAN0.RPGACC10.uint16_t[H]
#define RSCAN0RPGACC10HL RSCAN0.RPGACC10.uint8_t[HL]
#define RSCAN0RPGACC10HH RSCAN0.RPGACC10.uint8_t[HH]
#define RSCAN0RPGACC11 RSCAN0.RPGACC11.uint32_t
#define RSCAN0RPGACC11L RSCAN0.RPGACC11.uint16_t[L]
#define RSCAN0RPGACC11LL RSCAN0.RPGACC11.uint8_t[LL]
#define RSCAN0RPGACC11LH RSCAN0.RPGACC11.uint8_t[LH]
#define RSCAN0RPGACC11H RSCAN0.RPGACC11.uint16_t[H]
#define RSCAN0RPGACC11HL RSCAN0.RPGACC11.uint8_t[HL]
#define RSCAN0RPGACC11HH RSCAN0.RPGACC11.uint8_t[HH]
#define RSCAN0RPGACC12 RSCAN0.RPGACC12.uint32_t
#define RSCAN0RPGACC12L RSCAN0.RPGACC12.uint16_t[L]
#define RSCAN0RPGACC12LL RSCAN0.RPGACC12.uint8_t[LL]
#define RSCAN0RPGACC12LH RSCAN0.RPGACC12.uint8_t[LH]
#define RSCAN0RPGACC12H RSCAN0.RPGACC12.uint16_t[H]
#define RSCAN0RPGACC12HL RSCAN0.RPGACC12.uint8_t[HL]
#define RSCAN0RPGACC12HH RSCAN0.RPGACC12.uint8_t[HH]
#define RSCAN0RPGACC13 RSCAN0.RPGACC13.uint32_t
#define RSCAN0RPGACC13L RSCAN0.RPGACC13.uint16_t[L]
#define RSCAN0RPGACC13LL RSCAN0.RPGACC13.uint8_t[LL]
#define RSCAN0RPGACC13LH RSCAN0.RPGACC13.uint8_t[LH]
#define RSCAN0RPGACC13H RSCAN0.RPGACC13.uint16_t[H]
#define RSCAN0RPGACC13HL RSCAN0.RPGACC13.uint8_t[HL]
#define RSCAN0RPGACC13HH RSCAN0.RPGACC13.uint8_t[HH]
#define RSCAN0RPGACC14 RSCAN0.RPGACC14.uint32_t
#define RSCAN0RPGACC14L RSCAN0.RPGACC14.uint16_t[L]
#define RSCAN0RPGACC14LL RSCAN0.RPGACC14.uint8_t[LL]
#define RSCAN0RPGACC14LH RSCAN0.RPGACC14.uint8_t[LH]
#define RSCAN0RPGACC14H RSCAN0.RPGACC14.uint16_t[H]
#define RSCAN0RPGACC14HL RSCAN0.RPGACC14.uint8_t[HL]
#define RSCAN0RPGACC14HH RSCAN0.RPGACC14.uint8_t[HH]
#define RSCAN0RPGACC15 RSCAN0.RPGACC15.uint32_t
#define RSCAN0RPGACC15L RSCAN0.RPGACC15.uint16_t[L]
#define RSCAN0RPGACC15LL RSCAN0.RPGACC15.uint8_t[LL]
#define RSCAN0RPGACC15LH RSCAN0.RPGACC15.uint8_t[LH]
#define RSCAN0RPGACC15H RSCAN0.RPGACC15.uint16_t[H]
#define RSCAN0RPGACC15HL RSCAN0.RPGACC15.uint8_t[HL]
#define RSCAN0RPGACC15HH RSCAN0.RPGACC15.uint8_t[HH]
#define RSCAN0RPGACC16 RSCAN0.RPGACC16.uint32_t
#define RSCAN0RPGACC16L RSCAN0.RPGACC16.uint16_t[L]
#define RSCAN0RPGACC16LL RSCAN0.RPGACC16.uint8_t[LL]
#define RSCAN0RPGACC16LH RSCAN0.RPGACC16.uint8_t[LH]
#define RSCAN0RPGACC16H RSCAN0.RPGACC16.uint16_t[H]
#define RSCAN0RPGACC16HL RSCAN0.RPGACC16.uint8_t[HL]
#define RSCAN0RPGACC16HH RSCAN0.RPGACC16.uint8_t[HH]
#define RSCAN0RPGACC17 RSCAN0.RPGACC17.uint32_t
#define RSCAN0RPGACC17L RSCAN0.RPGACC17.uint16_t[L]
#define RSCAN0RPGACC17LL RSCAN0.RPGACC17.uint8_t[LL]
#define RSCAN0RPGACC17LH RSCAN0.RPGACC17.uint8_t[LH]
#define RSCAN0RPGACC17H RSCAN0.RPGACC17.uint16_t[H]
#define RSCAN0RPGACC17HL RSCAN0.RPGACC17.uint8_t[HL]
#define RSCAN0RPGACC17HH RSCAN0.RPGACC17.uint8_t[HH]
#define RSCAN0RPGACC18 RSCAN0.RPGACC18.uint32_t
#define RSCAN0RPGACC18L RSCAN0.RPGACC18.uint16_t[L]
#define RSCAN0RPGACC18LL RSCAN0.RPGACC18.uint8_t[LL]
#define RSCAN0RPGACC18LH RSCAN0.RPGACC18.uint8_t[LH]
#define RSCAN0RPGACC18H RSCAN0.RPGACC18.uint16_t[H]
#define RSCAN0RPGACC18HL RSCAN0.RPGACC18.uint8_t[HL]
#define RSCAN0RPGACC18HH RSCAN0.RPGACC18.uint8_t[HH]
#define RSCAN0RPGACC19 RSCAN0.RPGACC19.uint32_t
#define RSCAN0RPGACC19L RSCAN0.RPGACC19.uint16_t[L]
#define RSCAN0RPGACC19LL RSCAN0.RPGACC19.uint8_t[LL]
#define RSCAN0RPGACC19LH RSCAN0.RPGACC19.uint8_t[LH]
#define RSCAN0RPGACC19H RSCAN0.RPGACC19.uint16_t[H]
#define RSCAN0RPGACC19HL RSCAN0.RPGACC19.uint8_t[HL]
#define RSCAN0RPGACC19HH RSCAN0.RPGACC19.uint8_t[HH]
#define RSCAN0RPGACC20 RSCAN0.RPGACC20.uint32_t
#define RSCAN0RPGACC20L RSCAN0.RPGACC20.uint16_t[L]
#define RSCAN0RPGACC20LL RSCAN0.RPGACC20.uint8_t[LL]
#define RSCAN0RPGACC20LH RSCAN0.RPGACC20.uint8_t[LH]
#define RSCAN0RPGACC20H RSCAN0.RPGACC20.uint16_t[H]
#define RSCAN0RPGACC20HL RSCAN0.RPGACC20.uint8_t[HL]
#define RSCAN0RPGACC20HH RSCAN0.RPGACC20.uint8_t[HH]
#define RSCAN0RPGACC21 RSCAN0.RPGACC21.uint32_t
#define RSCAN0RPGACC21L RSCAN0.RPGACC21.uint16_t[L]
#define RSCAN0RPGACC21LL RSCAN0.RPGACC21.uint8_t[LL]
#define RSCAN0RPGACC21LH RSCAN0.RPGACC21.uint8_t[LH]
#define RSCAN0RPGACC21H RSCAN0.RPGACC21.uint16_t[H]
#define RSCAN0RPGACC21HL RSCAN0.RPGACC21.uint8_t[HL]
#define RSCAN0RPGACC21HH RSCAN0.RPGACC21.uint8_t[HH]
#define RSCAN0RPGACC22 RSCAN0.RPGACC22.uint32_t
#define RSCAN0RPGACC22L RSCAN0.RPGACC22.uint16_t[L]
#define RSCAN0RPGACC22LL RSCAN0.RPGACC22.uint8_t[LL]
#define RSCAN0RPGACC22LH RSCAN0.RPGACC22.uint8_t[LH]
#define RSCAN0RPGACC22H RSCAN0.RPGACC22.uint16_t[H]
#define RSCAN0RPGACC22HL RSCAN0.RPGACC22.uint8_t[HL]
#define RSCAN0RPGACC22HH RSCAN0.RPGACC22.uint8_t[HH]
#define RSCAN0RPGACC23 RSCAN0.RPGACC23.uint32_t
#define RSCAN0RPGACC23L RSCAN0.RPGACC23.uint16_t[L]
#define RSCAN0RPGACC23LL RSCAN0.RPGACC23.uint8_t[LL]
#define RSCAN0RPGACC23LH RSCAN0.RPGACC23.uint8_t[LH]
#define RSCAN0RPGACC23H RSCAN0.RPGACC23.uint16_t[H]
#define RSCAN0RPGACC23HL RSCAN0.RPGACC23.uint8_t[HL]
#define RSCAN0RPGACC23HH RSCAN0.RPGACC23.uint8_t[HH]
#define RSCAN0RPGACC24 RSCAN0.RPGACC24.uint32_t
#define RSCAN0RPGACC24L RSCAN0.RPGACC24.uint16_t[L]
#define RSCAN0RPGACC24LL RSCAN0.RPGACC24.uint8_t[LL]
#define RSCAN0RPGACC24LH RSCAN0.RPGACC24.uint8_t[LH]
#define RSCAN0RPGACC24H RSCAN0.RPGACC24.uint16_t[H]
#define RSCAN0RPGACC24HL RSCAN0.RPGACC24.uint8_t[HL]
#define RSCAN0RPGACC24HH RSCAN0.RPGACC24.uint8_t[HH]
#define RSCAN0RPGACC25 RSCAN0.RPGACC25.uint32_t
#define RSCAN0RPGACC25L RSCAN0.RPGACC25.uint16_t[L]
#define RSCAN0RPGACC25LL RSCAN0.RPGACC25.uint8_t[LL]
#define RSCAN0RPGACC25LH RSCAN0.RPGACC25.uint8_t[LH]
#define RSCAN0RPGACC25H RSCAN0.RPGACC25.uint16_t[H]
#define RSCAN0RPGACC25HL RSCAN0.RPGACC25.uint8_t[HL]
#define RSCAN0RPGACC25HH RSCAN0.RPGACC25.uint8_t[HH]
#define RSCAN0RPGACC26 RSCAN0.RPGACC26.uint32_t
#define RSCAN0RPGACC26L RSCAN0.RPGACC26.uint16_t[L]
#define RSCAN0RPGACC26LL RSCAN0.RPGACC26.uint8_t[LL]
#define RSCAN0RPGACC26LH RSCAN0.RPGACC26.uint8_t[LH]
#define RSCAN0RPGACC26H RSCAN0.RPGACC26.uint16_t[H]
#define RSCAN0RPGACC26HL RSCAN0.RPGACC26.uint8_t[HL]
#define RSCAN0RPGACC26HH RSCAN0.RPGACC26.uint8_t[HH]
#define RSCAN0RPGACC27 RSCAN0.RPGACC27.uint32_t
#define RSCAN0RPGACC27L RSCAN0.RPGACC27.uint16_t[L]
#define RSCAN0RPGACC27LL RSCAN0.RPGACC27.uint8_t[LL]
#define RSCAN0RPGACC27LH RSCAN0.RPGACC27.uint8_t[LH]
#define RSCAN0RPGACC27H RSCAN0.RPGACC27.uint16_t[H]
#define RSCAN0RPGACC27HL RSCAN0.RPGACC27.uint8_t[HL]
#define RSCAN0RPGACC27HH RSCAN0.RPGACC27.uint8_t[HH]
#define RSCAN0RPGACC28 RSCAN0.RPGACC28.uint32_t
#define RSCAN0RPGACC28L RSCAN0.RPGACC28.uint16_t[L]
#define RSCAN0RPGACC28LL RSCAN0.RPGACC28.uint8_t[LL]
#define RSCAN0RPGACC28LH RSCAN0.RPGACC28.uint8_t[LH]
#define RSCAN0RPGACC28H RSCAN0.RPGACC28.uint16_t[H]
#define RSCAN0RPGACC28HL RSCAN0.RPGACC28.uint8_t[HL]
#define RSCAN0RPGACC28HH RSCAN0.RPGACC28.uint8_t[HH]
#define RSCAN0RPGACC29 RSCAN0.RPGACC29.uint32_t
#define RSCAN0RPGACC29L RSCAN0.RPGACC29.uint16_t[L]
#define RSCAN0RPGACC29LL RSCAN0.RPGACC29.uint8_t[LL]
#define RSCAN0RPGACC29LH RSCAN0.RPGACC29.uint8_t[LH]
#define RSCAN0RPGACC29H RSCAN0.RPGACC29.uint16_t[H]
#define RSCAN0RPGACC29HL RSCAN0.RPGACC29.uint8_t[HL]
#define RSCAN0RPGACC29HH RSCAN0.RPGACC29.uint8_t[HH]
#define RSCAN0RPGACC30 RSCAN0.RPGACC30.uint32_t
#define RSCAN0RPGACC30L RSCAN0.RPGACC30.uint16_t[L]
#define RSCAN0RPGACC30LL RSCAN0.RPGACC30.uint8_t[LL]
#define RSCAN0RPGACC30LH RSCAN0.RPGACC30.uint8_t[LH]
#define RSCAN0RPGACC30H RSCAN0.RPGACC30.uint16_t[H]
#define RSCAN0RPGACC30HL RSCAN0.RPGACC30.uint8_t[HL]
#define RSCAN0RPGACC30HH RSCAN0.RPGACC30.uint8_t[HH]
#define RSCAN0RPGACC31 RSCAN0.RPGACC31.uint32_t
#define RSCAN0RPGACC31L RSCAN0.RPGACC31.uint16_t[L]
#define RSCAN0RPGACC31LL RSCAN0.RPGACC31.uint8_t[LL]
#define RSCAN0RPGACC31LH RSCAN0.RPGACC31.uint8_t[LH]
#define RSCAN0RPGACC31H RSCAN0.RPGACC31.uint16_t[H]
#define RSCAN0RPGACC31HL RSCAN0.RPGACC31.uint8_t[HL]
#define RSCAN0RPGACC31HH RSCAN0.RPGACC31.uint8_t[HH]
#define RSCAN0RPGACC32 RSCAN0.RPGACC32.uint32_t
#define RSCAN0RPGACC32L RSCAN0.RPGACC32.uint16_t[L]
#define RSCAN0RPGACC32LL RSCAN0.RPGACC32.uint8_t[LL]
#define RSCAN0RPGACC32LH RSCAN0.RPGACC32.uint8_t[LH]
#define RSCAN0RPGACC32H RSCAN0.RPGACC32.uint16_t[H]
#define RSCAN0RPGACC32HL RSCAN0.RPGACC32.uint8_t[HL]
#define RSCAN0RPGACC32HH RSCAN0.RPGACC32.uint8_t[HH]
#define RSCAN0RPGACC33 RSCAN0.RPGACC33.uint32_t
#define RSCAN0RPGACC33L RSCAN0.RPGACC33.uint16_t[L]
#define RSCAN0RPGACC33LL RSCAN0.RPGACC33.uint8_t[LL]
#define RSCAN0RPGACC33LH RSCAN0.RPGACC33.uint8_t[LH]
#define RSCAN0RPGACC33H RSCAN0.RPGACC33.uint16_t[H]
#define RSCAN0RPGACC33HL RSCAN0.RPGACC33.uint8_t[HL]
#define RSCAN0RPGACC33HH RSCAN0.RPGACC33.uint8_t[HH]
#define RSCAN0RPGACC34 RSCAN0.RPGACC34.uint32_t
#define RSCAN0RPGACC34L RSCAN0.RPGACC34.uint16_t[L]
#define RSCAN0RPGACC34LL RSCAN0.RPGACC34.uint8_t[LL]
#define RSCAN0RPGACC34LH RSCAN0.RPGACC34.uint8_t[LH]
#define RSCAN0RPGACC34H RSCAN0.RPGACC34.uint16_t[H]
#define RSCAN0RPGACC34HL RSCAN0.RPGACC34.uint8_t[HL]
#define RSCAN0RPGACC34HH RSCAN0.RPGACC34.uint8_t[HH]
#define RSCAN0RPGACC35 RSCAN0.RPGACC35.uint32_t
#define RSCAN0RPGACC35L RSCAN0.RPGACC35.uint16_t[L]
#define RSCAN0RPGACC35LL RSCAN0.RPGACC35.uint8_t[LL]
#define RSCAN0RPGACC35LH RSCAN0.RPGACC35.uint8_t[LH]
#define RSCAN0RPGACC35H RSCAN0.RPGACC35.uint16_t[H]
#define RSCAN0RPGACC35HL RSCAN0.RPGACC35.uint8_t[HL]
#define RSCAN0RPGACC35HH RSCAN0.RPGACC35.uint8_t[HH]
#define RSCAN0RPGACC36 RSCAN0.RPGACC36.uint32_t
#define RSCAN0RPGACC36L RSCAN0.RPGACC36.uint16_t[L]
#define RSCAN0RPGACC36LL RSCAN0.RPGACC36.uint8_t[LL]
#define RSCAN0RPGACC36LH RSCAN0.RPGACC36.uint8_t[LH]
#define RSCAN0RPGACC36H RSCAN0.RPGACC36.uint16_t[H]
#define RSCAN0RPGACC36HL RSCAN0.RPGACC36.uint8_t[HL]
#define RSCAN0RPGACC36HH RSCAN0.RPGACC36.uint8_t[HH]
#define RSCAN0RPGACC37 RSCAN0.RPGACC37.uint32_t
#define RSCAN0RPGACC37L RSCAN0.RPGACC37.uint16_t[L]
#define RSCAN0RPGACC37LL RSCAN0.RPGACC37.uint8_t[LL]
#define RSCAN0RPGACC37LH RSCAN0.RPGACC37.uint8_t[LH]
#define RSCAN0RPGACC37H RSCAN0.RPGACC37.uint16_t[H]
#define RSCAN0RPGACC37HL RSCAN0.RPGACC37.uint8_t[HL]
#define RSCAN0RPGACC37HH RSCAN0.RPGACC37.uint8_t[HH]
#define RSCAN0RPGACC38 RSCAN0.RPGACC38.uint32_t
#define RSCAN0RPGACC38L RSCAN0.RPGACC38.uint16_t[L]
#define RSCAN0RPGACC38LL RSCAN0.RPGACC38.uint8_t[LL]
#define RSCAN0RPGACC38LH RSCAN0.RPGACC38.uint8_t[LH]
#define RSCAN0RPGACC38H RSCAN0.RPGACC38.uint16_t[H]
#define RSCAN0RPGACC38HL RSCAN0.RPGACC38.uint8_t[HL]
#define RSCAN0RPGACC38HH RSCAN0.RPGACC38.uint8_t[HH]
#define RSCAN0RPGACC39 RSCAN0.RPGACC39.uint32_t
#define RSCAN0RPGACC39L RSCAN0.RPGACC39.uint16_t[L]
#define RSCAN0RPGACC39LL RSCAN0.RPGACC39.uint8_t[LL]
#define RSCAN0RPGACC39LH RSCAN0.RPGACC39.uint8_t[LH]
#define RSCAN0RPGACC39H RSCAN0.RPGACC39.uint16_t[H]
#define RSCAN0RPGACC39HL RSCAN0.RPGACC39.uint8_t[HL]
#define RSCAN0RPGACC39HH RSCAN0.RPGACC39.uint8_t[HH]
#define RSCAN0RPGACC40 RSCAN0.RPGACC40.uint32_t
#define RSCAN0RPGACC40L RSCAN0.RPGACC40.uint16_t[L]
#define RSCAN0RPGACC40LL RSCAN0.RPGACC40.uint8_t[LL]
#define RSCAN0RPGACC40LH RSCAN0.RPGACC40.uint8_t[LH]
#define RSCAN0RPGACC40H RSCAN0.RPGACC40.uint16_t[H]
#define RSCAN0RPGACC40HL RSCAN0.RPGACC40.uint8_t[HL]
#define RSCAN0RPGACC40HH RSCAN0.RPGACC40.uint8_t[HH]
#define RSCAN0RPGACC41 RSCAN0.RPGACC41.uint32_t
#define RSCAN0RPGACC41L RSCAN0.RPGACC41.uint16_t[L]
#define RSCAN0RPGACC41LL RSCAN0.RPGACC41.uint8_t[LL]
#define RSCAN0RPGACC41LH RSCAN0.RPGACC41.uint8_t[LH]
#define RSCAN0RPGACC41H RSCAN0.RPGACC41.uint16_t[H]
#define RSCAN0RPGACC41HL RSCAN0.RPGACC41.uint8_t[HL]
#define RSCAN0RPGACC41HH RSCAN0.RPGACC41.uint8_t[HH]
#define RSCAN0RPGACC42 RSCAN0.RPGACC42.uint32_t
#define RSCAN0RPGACC42L RSCAN0.RPGACC42.uint16_t[L]
#define RSCAN0RPGACC42LL RSCAN0.RPGACC42.uint8_t[LL]
#define RSCAN0RPGACC42LH RSCAN0.RPGACC42.uint8_t[LH]
#define RSCAN0RPGACC42H RSCAN0.RPGACC42.uint16_t[H]
#define RSCAN0RPGACC42HL RSCAN0.RPGACC42.uint8_t[HL]
#define RSCAN0RPGACC42HH RSCAN0.RPGACC42.uint8_t[HH]
#define RSCAN0RPGACC43 RSCAN0.RPGACC43.uint32_t
#define RSCAN0RPGACC43L RSCAN0.RPGACC43.uint16_t[L]
#define RSCAN0RPGACC43LL RSCAN0.RPGACC43.uint8_t[LL]
#define RSCAN0RPGACC43LH RSCAN0.RPGACC43.uint8_t[LH]
#define RSCAN0RPGACC43H RSCAN0.RPGACC43.uint16_t[H]
#define RSCAN0RPGACC43HL RSCAN0.RPGACC43.uint8_t[HL]
#define RSCAN0RPGACC43HH RSCAN0.RPGACC43.uint8_t[HH]
#define RSCAN0RPGACC44 RSCAN0.RPGACC44.uint32_t
#define RSCAN0RPGACC44L RSCAN0.RPGACC44.uint16_t[L]
#define RSCAN0RPGACC44LL RSCAN0.RPGACC44.uint8_t[LL]
#define RSCAN0RPGACC44LH RSCAN0.RPGACC44.uint8_t[LH]
#define RSCAN0RPGACC44H RSCAN0.RPGACC44.uint16_t[H]
#define RSCAN0RPGACC44HL RSCAN0.RPGACC44.uint8_t[HL]
#define RSCAN0RPGACC44HH RSCAN0.RPGACC44.uint8_t[HH]
#define RSCAN0RPGACC45 RSCAN0.RPGACC45.uint32_t
#define RSCAN0RPGACC45L RSCAN0.RPGACC45.uint16_t[L]
#define RSCAN0RPGACC45LL RSCAN0.RPGACC45.uint8_t[LL]
#define RSCAN0RPGACC45LH RSCAN0.RPGACC45.uint8_t[LH]
#define RSCAN0RPGACC45H RSCAN0.RPGACC45.uint16_t[H]
#define RSCAN0RPGACC45HL RSCAN0.RPGACC45.uint8_t[HL]
#define RSCAN0RPGACC45HH RSCAN0.RPGACC45.uint8_t[HH]
#define RSCAN0RPGACC46 RSCAN0.RPGACC46.uint32_t
#define RSCAN0RPGACC46L RSCAN0.RPGACC46.uint16_t[L]
#define RSCAN0RPGACC46LL RSCAN0.RPGACC46.uint8_t[LL]
#define RSCAN0RPGACC46LH RSCAN0.RPGACC46.uint8_t[LH]
#define RSCAN0RPGACC46H RSCAN0.RPGACC46.uint16_t[H]
#define RSCAN0RPGACC46HL RSCAN0.RPGACC46.uint8_t[HL]
#define RSCAN0RPGACC46HH RSCAN0.RPGACC46.uint8_t[HH]
#define RSCAN0RPGACC47 RSCAN0.RPGACC47.uint32_t
#define RSCAN0RPGACC47L RSCAN0.RPGACC47.uint16_t[L]
#define RSCAN0RPGACC47LL RSCAN0.RPGACC47.uint8_t[LL]
#define RSCAN0RPGACC47LH RSCAN0.RPGACC47.uint8_t[LH]
#define RSCAN0RPGACC47H RSCAN0.RPGACC47.uint16_t[H]
#define RSCAN0RPGACC47HL RSCAN0.RPGACC47.uint8_t[HL]
#define RSCAN0RPGACC47HH RSCAN0.RPGACC47.uint8_t[HH]
#define RSCAN0RPGACC48 RSCAN0.RPGACC48.uint32_t
#define RSCAN0RPGACC48L RSCAN0.RPGACC48.uint16_t[L]
#define RSCAN0RPGACC48LL RSCAN0.RPGACC48.uint8_t[LL]
#define RSCAN0RPGACC48LH RSCAN0.RPGACC48.uint8_t[LH]
#define RSCAN0RPGACC48H RSCAN0.RPGACC48.uint16_t[H]
#define RSCAN0RPGACC48HL RSCAN0.RPGACC48.uint8_t[HL]
#define RSCAN0RPGACC48HH RSCAN0.RPGACC48.uint8_t[HH]
#define RSCAN0RPGACC49 RSCAN0.RPGACC49.uint32_t
#define RSCAN0RPGACC49L RSCAN0.RPGACC49.uint16_t[L]
#define RSCAN0RPGACC49LL RSCAN0.RPGACC49.uint8_t[LL]
#define RSCAN0RPGACC49LH RSCAN0.RPGACC49.uint8_t[LH]
#define RSCAN0RPGACC49H RSCAN0.RPGACC49.uint16_t[H]
#define RSCAN0RPGACC49HL RSCAN0.RPGACC49.uint8_t[HL]
#define RSCAN0RPGACC49HH RSCAN0.RPGACC49.uint8_t[HH]
#define RSCAN0RPGACC50 RSCAN0.RPGACC50.uint32_t
#define RSCAN0RPGACC50L RSCAN0.RPGACC50.uint16_t[L]
#define RSCAN0RPGACC50LL RSCAN0.RPGACC50.uint8_t[LL]
#define RSCAN0RPGACC50LH RSCAN0.RPGACC50.uint8_t[LH]
#define RSCAN0RPGACC50H RSCAN0.RPGACC50.uint16_t[H]
#define RSCAN0RPGACC50HL RSCAN0.RPGACC50.uint8_t[HL]
#define RSCAN0RPGACC50HH RSCAN0.RPGACC50.uint8_t[HH]
#define RSCAN0RPGACC51 RSCAN0.RPGACC51.uint32_t
#define RSCAN0RPGACC51L RSCAN0.RPGACC51.uint16_t[L]
#define RSCAN0RPGACC51LL RSCAN0.RPGACC51.uint8_t[LL]
#define RSCAN0RPGACC51LH RSCAN0.RPGACC51.uint8_t[LH]
#define RSCAN0RPGACC51H RSCAN0.RPGACC51.uint16_t[H]
#define RSCAN0RPGACC51HL RSCAN0.RPGACC51.uint8_t[HL]
#define RSCAN0RPGACC51HH RSCAN0.RPGACC51.uint8_t[HH]
#define RSCAN0RPGACC52 RSCAN0.RPGACC52.uint32_t
#define RSCAN0RPGACC52L RSCAN0.RPGACC52.uint16_t[L]
#define RSCAN0RPGACC52LL RSCAN0.RPGACC52.uint8_t[LL]
#define RSCAN0RPGACC52LH RSCAN0.RPGACC52.uint8_t[LH]
#define RSCAN0RPGACC52H RSCAN0.RPGACC52.uint16_t[H]
#define RSCAN0RPGACC52HL RSCAN0.RPGACC52.uint8_t[HL]
#define RSCAN0RPGACC52HH RSCAN0.RPGACC52.uint8_t[HH]
#define RSCAN0RPGACC53 RSCAN0.RPGACC53.uint32_t
#define RSCAN0RPGACC53L RSCAN0.RPGACC53.uint16_t[L]
#define RSCAN0RPGACC53LL RSCAN0.RPGACC53.uint8_t[LL]
#define RSCAN0RPGACC53LH RSCAN0.RPGACC53.uint8_t[LH]
#define RSCAN0RPGACC53H RSCAN0.RPGACC53.uint16_t[H]
#define RSCAN0RPGACC53HL RSCAN0.RPGACC53.uint8_t[HL]
#define RSCAN0RPGACC53HH RSCAN0.RPGACC53.uint8_t[HH]
#define RSCAN0RPGACC54 RSCAN0.RPGACC54.uint32_t
#define RSCAN0RPGACC54L RSCAN0.RPGACC54.uint16_t[L]
#define RSCAN0RPGACC54LL RSCAN0.RPGACC54.uint8_t[LL]
#define RSCAN0RPGACC54LH RSCAN0.RPGACC54.uint8_t[LH]
#define RSCAN0RPGACC54H RSCAN0.RPGACC54.uint16_t[H]
#define RSCAN0RPGACC54HL RSCAN0.RPGACC54.uint8_t[HL]
#define RSCAN0RPGACC54HH RSCAN0.RPGACC54.uint8_t[HH]
#define RSCAN0RPGACC55 RSCAN0.RPGACC55.uint32_t
#define RSCAN0RPGACC55L RSCAN0.RPGACC55.uint16_t[L]
#define RSCAN0RPGACC55LL RSCAN0.RPGACC55.uint8_t[LL]
#define RSCAN0RPGACC55LH RSCAN0.RPGACC55.uint8_t[LH]
#define RSCAN0RPGACC55H RSCAN0.RPGACC55.uint16_t[H]
#define RSCAN0RPGACC55HL RSCAN0.RPGACC55.uint8_t[HL]
#define RSCAN0RPGACC55HH RSCAN0.RPGACC55.uint8_t[HH]
#define RSCAN0RPGACC56 RSCAN0.RPGACC56.uint32_t
#define RSCAN0RPGACC56L RSCAN0.RPGACC56.uint16_t[L]
#define RSCAN0RPGACC56LL RSCAN0.RPGACC56.uint8_t[LL]
#define RSCAN0RPGACC56LH RSCAN0.RPGACC56.uint8_t[LH]
#define RSCAN0RPGACC56H RSCAN0.RPGACC56.uint16_t[H]
#define RSCAN0RPGACC56HL RSCAN0.RPGACC56.uint8_t[HL]
#define RSCAN0RPGACC56HH RSCAN0.RPGACC56.uint8_t[HH]
#define RSCAN0RPGACC57 RSCAN0.RPGACC57.uint32_t
#define RSCAN0RPGACC57L RSCAN0.RPGACC57.uint16_t[L]
#define RSCAN0RPGACC57LL RSCAN0.RPGACC57.uint8_t[LL]
#define RSCAN0RPGACC57LH RSCAN0.RPGACC57.uint8_t[LH]
#define RSCAN0RPGACC57H RSCAN0.RPGACC57.uint16_t[H]
#define RSCAN0RPGACC57HL RSCAN0.RPGACC57.uint8_t[HL]
#define RSCAN0RPGACC57HH RSCAN0.RPGACC57.uint8_t[HH]
#define RSCAN0RPGACC58 RSCAN0.RPGACC58.uint32_t
#define RSCAN0RPGACC58L RSCAN0.RPGACC58.uint16_t[L]
#define RSCAN0RPGACC58LL RSCAN0.RPGACC58.uint8_t[LL]
#define RSCAN0RPGACC58LH RSCAN0.RPGACC58.uint8_t[LH]
#define RSCAN0RPGACC58H RSCAN0.RPGACC58.uint16_t[H]
#define RSCAN0RPGACC58HL RSCAN0.RPGACC58.uint8_t[HL]
#define RSCAN0RPGACC58HH RSCAN0.RPGACC58.uint8_t[HH]
#define RSCAN0RPGACC59 RSCAN0.RPGACC59.uint32_t
#define RSCAN0RPGACC59L RSCAN0.RPGACC59.uint16_t[L]
#define RSCAN0RPGACC59LL RSCAN0.RPGACC59.uint8_t[LL]
#define RSCAN0RPGACC59LH RSCAN0.RPGACC59.uint8_t[LH]
#define RSCAN0RPGACC59H RSCAN0.RPGACC59.uint16_t[H]
#define RSCAN0RPGACC59HL RSCAN0.RPGACC59.uint8_t[HL]
#define RSCAN0RPGACC59HH RSCAN0.RPGACC59.uint8_t[HH]
#define RSCAN0RPGACC60 RSCAN0.RPGACC60.uint32_t
#define RSCAN0RPGACC60L RSCAN0.RPGACC60.uint16_t[L]
#define RSCAN0RPGACC60LL RSCAN0.RPGACC60.uint8_t[LL]
#define RSCAN0RPGACC60LH RSCAN0.RPGACC60.uint8_t[LH]
#define RSCAN0RPGACC60H RSCAN0.RPGACC60.uint16_t[H]
#define RSCAN0RPGACC60HL RSCAN0.RPGACC60.uint8_t[HL]
#define RSCAN0RPGACC60HH RSCAN0.RPGACC60.uint8_t[HH]
#define RSCAN0RPGACC61 RSCAN0.RPGACC61.uint32_t
#define RSCAN0RPGACC61L RSCAN0.RPGACC61.uint16_t[L]
#define RSCAN0RPGACC61LL RSCAN0.RPGACC61.uint8_t[LL]
#define RSCAN0RPGACC61LH RSCAN0.RPGACC61.uint8_t[LH]
#define RSCAN0RPGACC61H RSCAN0.RPGACC61.uint16_t[H]
#define RSCAN0RPGACC61HL RSCAN0.RPGACC61.uint8_t[HL]
#define RSCAN0RPGACC61HH RSCAN0.RPGACC61.uint8_t[HH]
#define RSCAN0RPGACC62 RSCAN0.RPGACC62.uint32_t
#define RSCAN0RPGACC62L RSCAN0.RPGACC62.uint16_t[L]
#define RSCAN0RPGACC62LL RSCAN0.RPGACC62.uint8_t[LL]
#define RSCAN0RPGACC62LH RSCAN0.RPGACC62.uint8_t[LH]
#define RSCAN0RPGACC62H RSCAN0.RPGACC62.uint16_t[H]
#define RSCAN0RPGACC62HL RSCAN0.RPGACC62.uint8_t[HL]
#define RSCAN0RPGACC62HH RSCAN0.RPGACC62.uint8_t[HH]
#define RSCAN0RPGACC63 RSCAN0.RPGACC63.uint32_t
#define RSCAN0RPGACC63L RSCAN0.RPGACC63.uint16_t[L]
#define RSCAN0RPGACC63LL RSCAN0.RPGACC63.uint8_t[LL]
#define RSCAN0RPGACC63LH RSCAN0.RPGACC63.uint8_t[LH]
#define RSCAN0RPGACC63H RSCAN0.RPGACC63.uint16_t[H]
#define RSCAN0RPGACC63HL RSCAN0.RPGACC63.uint8_t[HL]
#define RSCAN0RPGACC63HH RSCAN0.RPGACC63.uint8_t[HH]
#define CSIH0CTL0 CSIH0.CTL0.uint8_t
#define CSIH0MBS CSIH0.CTL0.MBS
#define CSIH0JOBE CSIH0.CTL0.JOBE
#define CSIH0RXE CSIH0.CTL0.RXE
#define CSIH0TXE CSIH0.CTL0.TXE
#define CSIH0PWR CSIH0.CTL0.PWR
#define CSIH0STR0 CSIH0.STR0
#define CSIH0STCR0 CSIH0.STCR0
#define CSIH0CTL1 CSIH0.CTL1
#define CSIH0CTL2 CSIH0.CTL2
#define CSIH0EMU CSIH0.EMU.uint8_t
#define CSIH0SVSDIS CSIH0.EMU.SVSDIS
#define CSIH0MCTL1 CSIH0.MCTL1
#define CSIH0MCTL2 CSIH0.MCTL2
#define CSIH0TX0W CSIH0.TX0W
#define CSIH0TX0H CSIH0.TX0H
#define CSIH0RX0W CSIH0.RX0W
#define CSIH0RX0H CSIH0.RX0H
#define CSIH0MRWP0 CSIH0.MRWP0
#define CSIH0MCTL0 CSIH0.MCTL0
#define CSIH0CFG0 CSIH0.CFG0
#define CSIH0CFG1 CSIH0.CFG1
#define CSIH0CFG2 CSIH0.CFG2
#define CSIH0CFG3 CSIH0.CFG3
#define CSIH0CFG4 CSIH0.CFG4
#define CSIH0CFG5 CSIH0.CFG5
#define CSIH0CFG6 CSIH0.CFG6
#define CSIH0CFG7 CSIH0.CFG7
#define CSIH0BRS0 CSIH0.BRS0
#define CSIH0BRS1 CSIH0.BRS1
#define CSIH0BRS2 CSIH0.BRS2
#define CSIH0BRS3 CSIH0.BRS3
#define CSIH1CTL0 CSIH1.CTL0.uint8_t
#define CSIH1MBS CSIH1.CTL0.MBS
#define CSIH1JOBE CSIH1.CTL0.JOBE
#define CSIH1RXE CSIH1.CTL0.RXE
#define CSIH1TXE CSIH1.CTL0.TXE
#define CSIH1PWR CSIH1.CTL0.PWR
#define CSIH1STR0 CSIH1.STR0
#define CSIH1STCR0 CSIH1.STCR0
#define CSIH1CTL1 CSIH1.CTL1
#define CSIH1CTL2 CSIH1.CTL2
#define CSIH1EMU CSIH1.EMU.uint8_t
#define CSIH1SVSDIS CSIH1.EMU.SVSDIS
#define CSIH1MCTL1 CSIH1.MCTL1
#define CSIH1MCTL2 CSIH1.MCTL2
#define CSIH1TX0W CSIH1.TX0W
#define CSIH1TX0H CSIH1.TX0H
#define CSIH1RX0W CSIH1.RX0W
#define CSIH1RX0H CSIH1.RX0H
#define CSIH1MRWP0 CSIH1.MRWP0
#define CSIH1MCTL0 CSIH1.MCTL0
#define CSIH1CFG0 CSIH1.CFG0
#define CSIH1CFG1 CSIH1.CFG1
#define CSIH1CFG2 CSIH1.CFG2
#define CSIH1CFG3 CSIH1.CFG3
#define CSIH1CFG4 CSIH1.CFG4
#define CSIH1CFG5 CSIH1.CFG5
#define CSIH1BRS0 CSIH1.BRS0
#define CSIH1BRS1 CSIH1.BRS1
#define CSIH1BRS2 CSIH1.BRS2
#define CSIH1BRS3 CSIH1.BRS3
#define CSIH2CTL0 CSIH2.CTL0.uint8_t
#define CSIH2MBS CSIH2.CTL0.MBS
#define CSIH2JOBE CSIH2.CTL0.JOBE
#define CSIH2RXE CSIH2.CTL0.RXE
#define CSIH2TXE CSIH2.CTL0.TXE
#define CSIH2PWR CSIH2.CTL0.PWR
#define CSIH2STR0 CSIH2.STR0
#define CSIH2STCR0 CSIH2.STCR0
#define CSIH2CTL1 CSIH2.CTL1
#define CSIH2CTL2 CSIH2.CTL2
#define CSIH2EMU CSIH2.EMU.uint8_t
#define CSIH2SVSDIS CSIH2.EMU.SVSDIS
#define CSIH2MCTL1 CSIH2.MCTL1
#define CSIH2MCTL2 CSIH2.MCTL2
#define CSIH2TX0W CSIH2.TX0W
#define CSIH2TX0H CSIH2.TX0H
#define CSIH2RX0W CSIH2.RX0W
#define CSIH2RX0H CSIH2.RX0H
#define CSIH2MRWP0 CSIH2.MRWP0
#define CSIH2MCTL0 CSIH2.MCTL0
#define CSIH2CFG0 CSIH2.CFG0
#define CSIH2CFG1 CSIH2.CFG1
#define CSIH2CFG2 CSIH2.CFG2
#define CSIH2CFG3 CSIH2.CFG3
#define CSIH2CFG4 CSIH2.CFG4
#define CSIH2CFG5 CSIH2.CFG5
#define CSIH2BRS0 CSIH2.BRS0
#define CSIH2BRS1 CSIH2.BRS1
#define CSIH2BRS2 CSIH2.BRS2
#define CSIH2BRS3 CSIH2.BRS3
#define CSIH3CTL0 CSIH3.CTL0.uint8_t
#define CSIH3MBS CSIH3.CTL0.MBS
#define CSIH3JOBE CSIH3.CTL0.JOBE
#define CSIH3RXE CSIH3.CTL0.RXE
#define CSIH3TXE CSIH3.CTL0.TXE
#define CSIH3PWR CSIH3.CTL0.PWR
#define CSIH3STR0 CSIH3.STR0
#define CSIH3STCR0 CSIH3.STCR0
#define CSIH3CTL1 CSIH3.CTL1
#define CSIH3CTL2 CSIH3.CTL2
#define CSIH3EMU CSIH3.EMU.uint8_t
#define CSIH3SVSDIS CSIH3.EMU.SVSDIS
#define CSIH3MCTL1 CSIH3.MCTL1
#define CSIH3MCTL2 CSIH3.MCTL2
#define CSIH3TX0W CSIH3.TX0W
#define CSIH3TX0H CSIH3.TX0H
#define CSIH3RX0W CSIH3.RX0W
#define CSIH3RX0H CSIH3.RX0H
#define CSIH3MRWP0 CSIH3.MRWP0
#define CSIH3MCTL0 CSIH3.MCTL0
#define CSIH3CFG0 CSIH3.CFG0
#define CSIH3CFG1 CSIH3.CFG1
#define CSIH3CFG2 CSIH3.CFG2
#define CSIH3CFG3 CSIH3.CFG3
#define CSIH3BRS0 CSIH3.BRS0
#define CSIH3BRS1 CSIH3.BRS1
#define CSIH3BRS2 CSIH3.BRS2
#define CSIH3BRS3 CSIH3.BRS3
#define CSIG0CTL0 CSIG0.CTL0.uint8_t
#define CSIG0RXE CSIG0.CTL0.RXE
#define CSIG0TXE CSIG0.CTL0.TXE
#define CSIG0PWR CSIG0.CTL0.PWR
#define CSIG0STR0 CSIG0.STR0
#define CSIG0STCR0 CSIG0.STCR0
#define CSIG0CTL1 CSIG0.CTL1
#define CSIG0CTL2 CSIG0.CTL2
#define CSIG0EMU CSIG0.EMU.uint8_t
#define CSIG0SVSDIS CSIG0.EMU.SVSDIS
#define CSIG0BCTL0 CSIG0.BCTL0.uint8_t
#define CSIG0SCE CSIG0.BCTL0.SCE
#define CSIG0TX0W CSIG0.TX0W
#define CSIG0TX0H CSIG0.TX0H
#define CSIG0RX0 CSIG0.RX0
#define CSIG0CFG0 CSIG0.CFG0
#define CSIG1CTL0 CSIG1.CTL0.uint8_t
#define CSIG1RXE CSIG1.CTL0.RXE
#define CSIG1TXE CSIG1.CTL0.TXE
#define CSIG1PWR CSIG1.CTL0.PWR
#define CSIG1STR0 CSIG1.STR0
#define CSIG1STCR0 CSIG1.STCR0
#define CSIG1CTL1 CSIG1.CTL1
#define CSIG1CTL2 CSIG1.CTL2
#define CSIG1EMU CSIG1.EMU.uint8_t
#define CSIG1SVSDIS CSIG1.EMU.SVSDIS
#define CSIG1BCTL0 CSIG1.BCTL0.uint8_t
#define CSIG1SCE CSIG1.BCTL0.SCE
#define CSIG1TX0W CSIG1.TX0W
#define CSIG1TX0H CSIG1.TX0H
#define CSIG1RX0 CSIG1.RX0
#define CSIG1CFG0 CSIG1.CFG0
#define PIC0SST PIC0.SST
#define PIC0SSER0 PIC0.SSER0
#define PIC0SSER2 PIC0.SSER2
#define PIC0HIZCEN0 PIC0.HIZCEN0
#define PIC0ADTEN400 PIC0.ADTEN400
#define PIC0ADTEN401 PIC0.ADTEN401
#define PIC0ADTEN402 PIC0.ADTEN402
#define PIC0REG200 PIC0.REG200
#define PIC0REG201 PIC0.REG201
#define PIC0REG202 PIC0.REG202
#define PIC0REG203 PIC0.REG203
#define PIC0REG30 PIC0.REG30
#define PIC0REG31 PIC0.REG31
#define TAUD0CDR0 TAUD0.CDR0
#define TAUD0CDR1 TAUD0.CDR1
#define TAUD0CDR2 TAUD0.CDR2
#define TAUD0CDR3 TAUD0.CDR3
#define TAUD0CDR4 TAUD0.CDR4
#define TAUD0CDR5 TAUD0.CDR5
#define TAUD0CDR6 TAUD0.CDR6
#define TAUD0CDR7 TAUD0.CDR7
#define TAUD0CDR8 TAUD0.CDR8
#define TAUD0CDR9 TAUD0.CDR9
#define TAUD0CDR10 TAUD0.CDR10
#define TAUD0CDR11 TAUD0.CDR11
#define TAUD0CDR12 TAUD0.CDR12
#define TAUD0CDR13 TAUD0.CDR13
#define TAUD0CDR14 TAUD0.CDR14
#define TAUD0CDR15 TAUD0.CDR15
#define TAUD0TOL TAUD0.TOL
#define TAUD0RDT TAUD0.RDT
#define TAUD0RSF TAUD0.RSF
#define TAUD0TRO TAUD0.TRO
#define TAUD0TME TAUD0.TME
#define TAUD0TDL TAUD0.TDL
#define TAUD0TO TAUD0.TO
#define TAUD0TOE TAUD0.TOE
#define TAUD0CNT0 TAUD0.CNT0
#define TAUD0CNT1 TAUD0.CNT1
#define TAUD0CNT2 TAUD0.CNT2
#define TAUD0CNT3 TAUD0.CNT3
#define TAUD0CNT4 TAUD0.CNT4
#define TAUD0CNT5 TAUD0.CNT5
#define TAUD0CNT6 TAUD0.CNT6
#define TAUD0CNT7 TAUD0.CNT7
#define TAUD0CNT8 TAUD0.CNT8
#define TAUD0CNT9 TAUD0.CNT9
#define TAUD0CNT10 TAUD0.CNT10
#define TAUD0CNT11 TAUD0.CNT11
#define TAUD0CNT12 TAUD0.CNT12
#define TAUD0CNT13 TAUD0.CNT13
#define TAUD0CNT14 TAUD0.CNT14
#define TAUD0CNT15 TAUD0.CNT15
#define TAUD0CMUR0 TAUD0.CMUR0
#define TAUD0CMUR1 TAUD0.CMUR1
#define TAUD0CMUR2 TAUD0.CMUR2
#define TAUD0CMUR3 TAUD0.CMUR3
#define TAUD0CMUR4 TAUD0.CMUR4
#define TAUD0CMUR5 TAUD0.CMUR5
#define TAUD0CMUR6 TAUD0.CMUR6
#define TAUD0CMUR7 TAUD0.CMUR7
#define TAUD0CMUR8 TAUD0.CMUR8
#define TAUD0CMUR9 TAUD0.CMUR9
#define TAUD0CMUR10 TAUD0.CMUR10
#define TAUD0CMUR11 TAUD0.CMUR11
#define TAUD0CMUR12 TAUD0.CMUR12
#define TAUD0CMUR13 TAUD0.CMUR13
#define TAUD0CMUR14 TAUD0.CMUR14
#define TAUD0CMUR15 TAUD0.CMUR15
#define TAUD0CSR0 TAUD0.CSR0
#define TAUD0CSR1 TAUD0.CSR1
#define TAUD0CSR2 TAUD0.CSR2
#define TAUD0CSR3 TAUD0.CSR3
#define TAUD0CSR4 TAUD0.CSR4
#define TAUD0CSR5 TAUD0.CSR5
#define TAUD0CSR6 TAUD0.CSR6
#define TAUD0CSR7 TAUD0.CSR7
#define TAUD0CSR8 TAUD0.CSR8
#define TAUD0CSR9 TAUD0.CSR9
#define TAUD0CSR10 TAUD0.CSR10
#define TAUD0CSR11 TAUD0.CSR11
#define TAUD0CSR12 TAUD0.CSR12
#define TAUD0CSR13 TAUD0.CSR13
#define TAUD0CSR14 TAUD0.CSR14
#define TAUD0CSR15 TAUD0.CSR15
#define TAUD0CSC0 TAUD0.CSC0
#define TAUD0CSC1 TAUD0.CSC1
#define TAUD0CSC2 TAUD0.CSC2
#define TAUD0CSC3 TAUD0.CSC3
#define TAUD0CSC4 TAUD0.CSC4
#define TAUD0CSC5 TAUD0.CSC5
#define TAUD0CSC6 TAUD0.CSC6
#define TAUD0CSC7 TAUD0.CSC7
#define TAUD0CSC8 TAUD0.CSC8
#define TAUD0CSC9 TAUD0.CSC9
#define TAUD0CSC10 TAUD0.CSC10
#define TAUD0CSC11 TAUD0.CSC11
#define TAUD0CSC12 TAUD0.CSC12
#define TAUD0CSC13 TAUD0.CSC13
#define TAUD0CSC14 TAUD0.CSC14
#define TAUD0CSC15 TAUD0.CSC15
#define TAUD0TE TAUD0.TE
#define TAUD0TS TAUD0.TS
#define TAUD0TT TAUD0.TT
#define TAUD0CMOR0 TAUD0.CMOR0
#define TAUD0CMOR1 TAUD0.CMOR1
#define TAUD0CMOR2 TAUD0.CMOR2
#define TAUD0CMOR3 TAUD0.CMOR3
#define TAUD0CMOR4 TAUD0.CMOR4
#define TAUD0CMOR5 TAUD0.CMOR5
#define TAUD0CMOR6 TAUD0.CMOR6
#define TAUD0CMOR7 TAUD0.CMOR7
#define TAUD0CMOR8 TAUD0.CMOR8
#define TAUD0CMOR9 TAUD0.CMOR9
#define TAUD0CMOR10 TAUD0.CMOR10
#define TAUD0CMOR11 TAUD0.CMOR11
#define TAUD0CMOR12 TAUD0.CMOR12
#define TAUD0CMOR13 TAUD0.CMOR13
#define TAUD0CMOR14 TAUD0.CMOR14
#define TAUD0CMOR15 TAUD0.CMOR15
#define TAUD0TPS TAUD0.TPS
#define TAUD0BRS TAUD0.BRS
#define TAUD0TOM TAUD0.TOM
#define TAUD0TOC TAUD0.TOC
#define TAUD0TDE TAUD0.TDE
#define TAUD0TDM TAUD0.TDM
#define TAUD0TRE TAUD0.TRE
#define TAUD0TRC TAUD0.TRC
#define TAUD0RDE TAUD0.RDE
#define TAUD0RDM TAUD0.RDM
#define TAUD0RDS TAUD0.RDS
#define TAUD0RDC TAUD0.RDC
#define TAUD0EMU TAUD0.EMU
#define TAUB0CDR0 TAUB0.CDR0
#define TAUB0CDR1 TAUB0.CDR1
#define TAUB0CDR2 TAUB0.CDR2
#define TAUB0CDR3 TAUB0.CDR3
#define TAUB0CDR4 TAUB0.CDR4
#define TAUB0CDR5 TAUB0.CDR5
#define TAUB0CDR6 TAUB0.CDR6
#define TAUB0CDR7 TAUB0.CDR7
#define TAUB0CDR8 TAUB0.CDR8
#define TAUB0CDR9 TAUB0.CDR9
#define TAUB0CDR10 TAUB0.CDR10
#define TAUB0CDR11 TAUB0.CDR11
#define TAUB0CDR12 TAUB0.CDR12
#define TAUB0CDR13 TAUB0.CDR13
#define TAUB0CDR14 TAUB0.CDR14
#define TAUB0CDR15 TAUB0.CDR15
#define TAUB0TOL TAUB0.TOL
#define TAUB0RDT TAUB0.RDT
#define TAUB0RSF TAUB0.RSF
#define TAUB0TDL TAUB0.TDL
#define TAUB0TO TAUB0.TO
#define TAUB0TOE TAUB0.TOE
#define TAUB0CNT0 TAUB0.CNT0
#define TAUB0CNT1 TAUB0.CNT1
#define TAUB0CNT2 TAUB0.CNT2
#define TAUB0CNT3 TAUB0.CNT3
#define TAUB0CNT4 TAUB0.CNT4
#define TAUB0CNT5 TAUB0.CNT5
#define TAUB0CNT6 TAUB0.CNT6
#define TAUB0CNT7 TAUB0.CNT7
#define TAUB0CNT8 TAUB0.CNT8
#define TAUB0CNT9 TAUB0.CNT9
#define TAUB0CNT10 TAUB0.CNT10
#define TAUB0CNT11 TAUB0.CNT11
#define TAUB0CNT12 TAUB0.CNT12
#define TAUB0CNT13 TAUB0.CNT13
#define TAUB0CNT14 TAUB0.CNT14
#define TAUB0CNT15 TAUB0.CNT15
#define TAUB0CMUR0 TAUB0.CMUR0
#define TAUB0CMUR1 TAUB0.CMUR1
#define TAUB0CMUR2 TAUB0.CMUR2
#define TAUB0CMUR3 TAUB0.CMUR3
#define TAUB0CMUR4 TAUB0.CMUR4
#define TAUB0CMUR5 TAUB0.CMUR5
#define TAUB0CMUR6 TAUB0.CMUR6
#define TAUB0CMUR7 TAUB0.CMUR7
#define TAUB0CMUR8 TAUB0.CMUR8
#define TAUB0CMUR9 TAUB0.CMUR9
#define TAUB0CMUR10 TAUB0.CMUR10
#define TAUB0CMUR11 TAUB0.CMUR11
#define TAUB0CMUR12 TAUB0.CMUR12
#define TAUB0CMUR13 TAUB0.CMUR13
#define TAUB0CMUR14 TAUB0.CMUR14
#define TAUB0CMUR15 TAUB0.CMUR15
#define TAUB0CSR0 TAUB0.CSR0
#define TAUB0CSR1 TAUB0.CSR1
#define TAUB0CSR2 TAUB0.CSR2
#define TAUB0CSR3 TAUB0.CSR3
#define TAUB0CSR4 TAUB0.CSR4
#define TAUB0CSR5 TAUB0.CSR5
#define TAUB0CSR6 TAUB0.CSR6
#define TAUB0CSR7 TAUB0.CSR7
#define TAUB0CSR8 TAUB0.CSR8
#define TAUB0CSR9 TAUB0.CSR9
#define TAUB0CSR10 TAUB0.CSR10
#define TAUB0CSR11 TAUB0.CSR11
#define TAUB0CSR12 TAUB0.CSR12
#define TAUB0CSR13 TAUB0.CSR13
#define TAUB0CSR14 TAUB0.CSR14
#define TAUB0CSR15 TAUB0.CSR15
#define TAUB0CSC0 TAUB0.CSC0
#define TAUB0CSC1 TAUB0.CSC1
#define TAUB0CSC2 TAUB0.CSC2
#define TAUB0CSC3 TAUB0.CSC3
#define TAUB0CSC4 TAUB0.CSC4
#define TAUB0CSC5 TAUB0.CSC5
#define TAUB0CSC6 TAUB0.CSC6
#define TAUB0CSC7 TAUB0.CSC7
#define TAUB0CSC8 TAUB0.CSC8
#define TAUB0CSC9 TAUB0.CSC9
#define TAUB0CSC10 TAUB0.CSC10
#define TAUB0CSC11 TAUB0.CSC11
#define TAUB0CSC12 TAUB0.CSC12
#define TAUB0CSC13 TAUB0.CSC13
#define TAUB0CSC14 TAUB0.CSC14
#define TAUB0CSC15 TAUB0.CSC15
#define TAUB0TE TAUB0.TE
#define TAUB0TS TAUB0.TS
#define TAUB0TT TAUB0.TT
#define TAUB0CMOR0 TAUB0.CMOR0
#define TAUB0CMOR1 TAUB0.CMOR1
#define TAUB0CMOR2 TAUB0.CMOR2
#define TAUB0CMOR3 TAUB0.CMOR3
#define TAUB0CMOR4 TAUB0.CMOR4
#define TAUB0CMOR5 TAUB0.CMOR5
#define TAUB0CMOR6 TAUB0.CMOR6
#define TAUB0CMOR7 TAUB0.CMOR7
#define TAUB0CMOR8 TAUB0.CMOR8
#define TAUB0CMOR9 TAUB0.CMOR9
#define TAUB0CMOR10 TAUB0.CMOR10
#define TAUB0CMOR11 TAUB0.CMOR11
#define TAUB0CMOR12 TAUB0.CMOR12
#define TAUB0CMOR13 TAUB0.CMOR13
#define TAUB0CMOR14 TAUB0.CMOR14
#define TAUB0CMOR15 TAUB0.CMOR15
#define TAUB0TPS TAUB0.TPS
#define TAUB0TOM TAUB0.TOM
#define TAUB0TOC TAUB0.TOC
#define TAUB0TDE TAUB0.TDE
#define TAUB0RDE TAUB0.RDE
#define TAUB0RDM TAUB0.RDM
#define TAUB0RDS TAUB0.RDS
#define TAUB0RDC TAUB0.RDC
#define TAUB0EMU TAUB0.EMU
#define TAUB1CDR0 TAUB1.CDR0
#define TAUB1CDR1 TAUB1.CDR1
#define TAUB1CDR2 TAUB1.CDR2
#define TAUB1CDR3 TAUB1.CDR3
#define TAUB1CDR4 TAUB1.CDR4
#define TAUB1CDR5 TAUB1.CDR5
#define TAUB1CDR6 TAUB1.CDR6
#define TAUB1CDR7 TAUB1.CDR7
#define TAUB1CDR8 TAUB1.CDR8
#define TAUB1CDR9 TAUB1.CDR9
#define TAUB1CDR10 TAUB1.CDR10
#define TAUB1CDR11 TAUB1.CDR11
#define TAUB1CDR12 TAUB1.CDR12
#define TAUB1CDR13 TAUB1.CDR13
#define TAUB1CDR14 TAUB1.CDR14
#define TAUB1CDR15 TAUB1.CDR15
#define TAUB1TOL TAUB1.TOL
#define TAUB1RDT TAUB1.RDT
#define TAUB1RSF TAUB1.RSF
#define TAUB1TDL TAUB1.TDL
#define TAUB1TO TAUB1.TO
#define TAUB1TOE TAUB1.TOE
#define TAUB1CNT0 TAUB1.CNT0
#define TAUB1CNT1 TAUB1.CNT1
#define TAUB1CNT2 TAUB1.CNT2
#define TAUB1CNT3 TAUB1.CNT3
#define TAUB1CNT4 TAUB1.CNT4
#define TAUB1CNT5 TAUB1.CNT5
#define TAUB1CNT6 TAUB1.CNT6
#define TAUB1CNT7 TAUB1.CNT7
#define TAUB1CNT8 TAUB1.CNT8
#define TAUB1CNT9 TAUB1.CNT9
#define TAUB1CNT10 TAUB1.CNT10
#define TAUB1CNT11 TAUB1.CNT11
#define TAUB1CNT12 TAUB1.CNT12
#define TAUB1CNT13 TAUB1.CNT13
#define TAUB1CNT14 TAUB1.CNT14
#define TAUB1CNT15 TAUB1.CNT15
#define TAUB1CMUR0 TAUB1.CMUR0
#define TAUB1CMUR1 TAUB1.CMUR1
#define TAUB1CMUR2 TAUB1.CMUR2
#define TAUB1CMUR3 TAUB1.CMUR3
#define TAUB1CMUR4 TAUB1.CMUR4
#define TAUB1CMUR5 TAUB1.CMUR5
#define TAUB1CMUR6 TAUB1.CMUR6
#define TAUB1CMUR7 TAUB1.CMUR7
#define TAUB1CMUR8 TAUB1.CMUR8
#define TAUB1CMUR9 TAUB1.CMUR9
#define TAUB1CMUR10 TAUB1.CMUR10
#define TAUB1CMUR11 TAUB1.CMUR11
#define TAUB1CMUR12 TAUB1.CMUR12
#define TAUB1CMUR13 TAUB1.CMUR13
#define TAUB1CMUR14 TAUB1.CMUR14
#define TAUB1CMUR15 TAUB1.CMUR15
#define TAUB1CSR0 TAUB1.CSR0
#define TAUB1CSR1 TAUB1.CSR1
#define TAUB1CSR2 TAUB1.CSR2
#define TAUB1CSR3 TAUB1.CSR3
#define TAUB1CSR4 TAUB1.CSR4
#define TAUB1CSR5 TAUB1.CSR5
#define TAUB1CSR6 TAUB1.CSR6
#define TAUB1CSR7 TAUB1.CSR7
#define TAUB1CSR8 TAUB1.CSR8
#define TAUB1CSR9 TAUB1.CSR9
#define TAUB1CSR10 TAUB1.CSR10
#define TAUB1CSR11 TAUB1.CSR11
#define TAUB1CSR12 TAUB1.CSR12
#define TAUB1CSR13 TAUB1.CSR13
#define TAUB1CSR14 TAUB1.CSR14
#define TAUB1CSR15 TAUB1.CSR15
#define TAUB1CSC0 TAUB1.CSC0
#define TAUB1CSC1 TAUB1.CSC1
#define TAUB1CSC2 TAUB1.CSC2
#define TAUB1CSC3 TAUB1.CSC3
#define TAUB1CSC4 TAUB1.CSC4
#define TAUB1CSC5 TAUB1.CSC5
#define TAUB1CSC6 TAUB1.CSC6
#define TAUB1CSC7 TAUB1.CSC7
#define TAUB1CSC8 TAUB1.CSC8
#define TAUB1CSC9 TAUB1.CSC9
#define TAUB1CSC10 TAUB1.CSC10
#define TAUB1CSC11 TAUB1.CSC11
#define TAUB1CSC12 TAUB1.CSC12
#define TAUB1CSC13 TAUB1.CSC13
#define TAUB1CSC14 TAUB1.CSC14
#define TAUB1CSC15 TAUB1.CSC15
#define TAUB1TE TAUB1.TE
#define TAUB1TS TAUB1.TS
#define TAUB1TT TAUB1.TT
#define TAUB1CMOR0 TAUB1.CMOR0
#define TAUB1CMOR1 TAUB1.CMOR1
#define TAUB1CMOR2 TAUB1.CMOR2
#define TAUB1CMOR3 TAUB1.CMOR3
#define TAUB1CMOR4 TAUB1.CMOR4
#define TAUB1CMOR5 TAUB1.CMOR5
#define TAUB1CMOR6 TAUB1.CMOR6
#define TAUB1CMOR7 TAUB1.CMOR7
#define TAUB1CMOR8 TAUB1.CMOR8
#define TAUB1CMOR9 TAUB1.CMOR9
#define TAUB1CMOR10 TAUB1.CMOR10
#define TAUB1CMOR11 TAUB1.CMOR11
#define TAUB1CMOR12 TAUB1.CMOR12
#define TAUB1CMOR13 TAUB1.CMOR13
#define TAUB1CMOR14 TAUB1.CMOR14
#define TAUB1CMOR15 TAUB1.CMOR15
#define TAUB1TPS TAUB1.TPS
#define TAUB1TOM TAUB1.TOM
#define TAUB1TOC TAUB1.TOC
#define TAUB1TDE TAUB1.TDE
#define TAUB1RDE TAUB1.RDE
#define TAUB1RDM TAUB1.RDM
#define TAUB1RDS TAUB1.RDS
#define TAUB1RDC TAUB1.RDC
#define TAUB1EMU TAUB1.EMU
#define TAUJ0CDR0 TAUJ0.CDR0
#define TAUJ0CDR1 TAUJ0.CDR1
#define TAUJ0CDR2 TAUJ0.CDR2
#define TAUJ0CDR3 TAUJ0.CDR3
#define TAUJ0CNT0 TAUJ0.CNT0
#define TAUJ0CNT1 TAUJ0.CNT1
#define TAUJ0CNT2 TAUJ0.CNT2
#define TAUJ0CNT3 TAUJ0.CNT3
#define TAUJ0CMUR0 TAUJ0.CMUR0
#define TAUJ0CMUR1 TAUJ0.CMUR1
#define TAUJ0CMUR2 TAUJ0.CMUR2
#define TAUJ0CMUR3 TAUJ0.CMUR3
#define TAUJ0CSR0 TAUJ0.CSR0
#define TAUJ0CSR1 TAUJ0.CSR1
#define TAUJ0CSR2 TAUJ0.CSR2
#define TAUJ0CSR3 TAUJ0.CSR3
#define TAUJ0CSC0 TAUJ0.CSC0
#define TAUJ0CSC1 TAUJ0.CSC1
#define TAUJ0CSC2 TAUJ0.CSC2
#define TAUJ0CSC3 TAUJ0.CSC3
#define TAUJ0TE TAUJ0.TE
#define TAUJ0TS TAUJ0.TS
#define TAUJ0TT TAUJ0.TT
#define TAUJ0TO TAUJ0.TO
#define TAUJ0TOE TAUJ0.TOE
#define TAUJ0TOL TAUJ0.TOL
#define TAUJ0RDT TAUJ0.RDT
#define TAUJ0RSF TAUJ0.RSF
#define TAUJ0CMOR0 TAUJ0.CMOR0
#define TAUJ0CMOR1 TAUJ0.CMOR1
#define TAUJ0CMOR2 TAUJ0.CMOR2
#define TAUJ0CMOR3 TAUJ0.CMOR3
#define TAUJ0TPS TAUJ0.TPS
#define TAUJ0BRS TAUJ0.BRS
#define TAUJ0TOM TAUJ0.TOM
#define TAUJ0TOC TAUJ0.TOC
#define TAUJ0RDE TAUJ0.RDE
#define TAUJ0RDM TAUJ0.RDM
#define TAUJ0EMU TAUJ0.EMU
#define TAUJ1CDR0 TAUJ1.CDR0
#define TAUJ1CDR1 TAUJ1.CDR1
#define TAUJ1CDR2 TAUJ1.CDR2
#define TAUJ1CDR3 TAUJ1.CDR3
#define TAUJ1CNT0 TAUJ1.CNT0
#define TAUJ1CNT1 TAUJ1.CNT1
#define TAUJ1CNT2 TAUJ1.CNT2
#define TAUJ1CNT3 TAUJ1.CNT3
#define TAUJ1CMUR0 TAUJ1.CMUR0
#define TAUJ1CMUR1 TAUJ1.CMUR1
#define TAUJ1CMUR2 TAUJ1.CMUR2
#define TAUJ1CMUR3 TAUJ1.CMUR3
#define TAUJ1CSR0 TAUJ1.CSR0
#define TAUJ1CSR1 TAUJ1.CSR1
#define TAUJ1CSR2 TAUJ1.CSR2
#define TAUJ1CSR3 TAUJ1.CSR3
#define TAUJ1CSC0 TAUJ1.CSC0
#define TAUJ1CSC1 TAUJ1.CSC1
#define TAUJ1CSC2 TAUJ1.CSC2
#define TAUJ1CSC3 TAUJ1.CSC3
#define TAUJ1TE TAUJ1.TE
#define TAUJ1TS TAUJ1.TS
#define TAUJ1TT TAUJ1.TT
#define TAUJ1TO TAUJ1.TO
#define TAUJ1TOE TAUJ1.TOE
#define TAUJ1TOL TAUJ1.TOL
#define TAUJ1RDT TAUJ1.RDT
#define TAUJ1RSF TAUJ1.RSF
#define TAUJ1CMOR0 TAUJ1.CMOR0
#define TAUJ1CMOR1 TAUJ1.CMOR1
#define TAUJ1CMOR2 TAUJ1.CMOR2
#define TAUJ1CMOR3 TAUJ1.CMOR3
#define TAUJ1TPS TAUJ1.TPS
#define TAUJ1BRS TAUJ1.BRS
#define TAUJ1TOM TAUJ1.TOM
#define TAUJ1TOC TAUJ1.TOC
#define TAUJ1RDE TAUJ1.RDE
#define TAUJ1RDM TAUJ1.RDM
#define TAUJ1EMU TAUJ1.EMU
#define PWSA0CTL PWSA0.CTL
#define PWSA0STR PWSA0.STR
#define PWSA0STC PWSA0.STC
#define PWSA0EMU PWSA0.EMU
#define PWSA0QUE0 PWSA0.QUE0
#define PWSA0QUE1 PWSA0.QUE1
#define PWSA0QUE2 PWSA0.QUE2
#define PWSA0QUE3 PWSA0.QUE3
#define PWSA0QUE4 PWSA0.QUE4
#define PWSA0QUE5 PWSA0.QUE5
#define PWSA0QUE6 PWSA0.QUE6
#define PWSA0QUE7 PWSA0.QUE7
#define PWSA0PVCR00_01 PWSA0.PVCR00_01
#define PWSA0PVCR02_03 PWSA0.PVCR02_03
#define PWSA0PVCR04_05 PWSA0.PVCR04_05
#define PWSA0PVCR06_07 PWSA0.PVCR06_07
#define PWSA0PVCR08_09 PWSA0.PVCR08_09
#define PWSA0PVCR10_11 PWSA0.PVCR10_11
#define PWSA0PVCR12_13 PWSA0.PVCR12_13
#define PWSA0PVCR14_15 PWSA0.PVCR14_15
#define PWSA0PVCR16_17 PWSA0.PVCR16_17
#define PWSA0PVCR18_19 PWSA0.PVCR18_19
#define PWSA0PVCR20_21 PWSA0.PVCR20_21
#define PWSA0PVCR22_23 PWSA0.PVCR22_23
#define PWSA0PVCR24_25 PWSA0.PVCR24_25
#define PWSA0PVCR26_27 PWSA0.PVCR26_27
#define PWSA0PVCR28_29 PWSA0.PVCR28_29
#define PWSA0PVCR30_31 PWSA0.PVCR30_31
#define PWSA0PVCR32_33 PWSA0.PVCR32_33
#define PWSA0PVCR34_35 PWSA0.PVCR34_35
#define PWSA0PVCR36_37 PWSA0.PVCR36_37
#define PWSA0PVCR38_39 PWSA0.PVCR38_39
#define PWSA0PVCR40_41 PWSA0.PVCR40_41
#define PWSA0PVCR42_43 PWSA0.PVCR42_43
#define PWSA0PVCR44_45 PWSA0.PVCR44_45
#define PWSA0PVCR46_47 PWSA0.PVCR46_47
#define PWSA0PVCR48_49 PWSA0.PVCR48_49
#define PWSA0PVCR50_51 PWSA0.PVCR50_51
#define PWSA0PVCR52_53 PWSA0.PVCR52_53
#define PWSA0PVCR54_55 PWSA0.PVCR54_55
#define PWSA0PVCR56_57 PWSA0.PVCR56_57
#define PWSA0PVCR58_59 PWSA0.PVCR58_59
#define PWSA0PVCR60_61 PWSA0.PVCR60_61
#define PWSA0PVCR62_63 PWSA0.PVCR62_63
#define PWSA0PVCR64_65 PWSA0.PVCR64_65
#define PWSA0PVCR66_67 PWSA0.PVCR66_67
#define PWSA0PVCR68_69 PWSA0.PVCR68_69
#define PWSA0PVCR70_71 PWSA0.PVCR70_71
#define PWGA0CSDR PWGA0.CSDR
#define PWGA0CRDR PWGA0.CRDR
#define PWGA0CTDR PWGA0.CTDR
#define PWGA0RDT PWGA0.RDT
#define PWGA0RSF PWGA0.RSF
#define PWGA0CNT PWGA0.CNT
#define PWGA0CTL PWGA0.CTL
#define PWGA0CSBR PWGA0.CSBR
#define PWGA0CRBR PWGA0.CRBR
#define PWGA0CTBR PWGA0.CTBR
#define PWGA1CSDR PWGA1.CSDR
#define PWGA1CRDR PWGA1.CRDR
#define PWGA1CTDR PWGA1.CTDR
#define PWGA1RDT PWGA1.RDT
#define PWGA1RSF PWGA1.RSF
#define PWGA1CNT PWGA1.CNT
#define PWGA1CTL PWGA1.CTL
#define PWGA1CSBR PWGA1.CSBR
#define PWGA1CRBR PWGA1.CRBR
#define PWGA1CTBR PWGA1.CTBR
#define PWGA2CSDR PWGA2.CSDR
#define PWGA2CRDR PWGA2.CRDR
#define PWGA2CTDR PWGA2.CTDR
#define PWGA2RDT PWGA2.RDT
#define PWGA2RSF PWGA2.RSF
#define PWGA2CNT PWGA2.CNT
#define PWGA2CTL PWGA2.CTL
#define PWGA2CSBR PWGA2.CSBR
#define PWGA2CRBR PWGA2.CRBR
#define PWGA2CTBR PWGA2.CTBR
#define PWGA3CSDR PWGA3.CSDR
#define PWGA3CRDR PWGA3.CRDR
#define PWGA3CTDR PWGA3.CTDR
#define PWGA3RDT PWGA3.RDT
#define PWGA3RSF PWGA3.RSF
#define PWGA3CNT PWGA3.CNT
#define PWGA3CTL PWGA3.CTL
#define PWGA3CSBR PWGA3.CSBR
#define PWGA3CRBR PWGA3.CRBR
#define PWGA3CTBR PWGA3.CTBR
#define PWGA4CSDR PWGA4.CSDR
#define PWGA4CRDR PWGA4.CRDR
#define PWGA4CTDR PWGA4.CTDR
#define PWGA4RDT PWGA4.RDT
#define PWGA4RSF PWGA4.RSF
#define PWGA4CNT PWGA4.CNT
#define PWGA4CTL PWGA4.CTL
#define PWGA4CSBR PWGA4.CSBR
#define PWGA4CRBR PWGA4.CRBR
#define PWGA4CTBR PWGA4.CTBR
#define PWGA5CSDR PWGA5.CSDR
#define PWGA5CRDR PWGA5.CRDR
#define PWGA5CTDR PWGA5.CTDR
#define PWGA5RDT PWGA5.RDT
#define PWGA5RSF PWGA5.RSF
#define PWGA5CNT PWGA5.CNT
#define PWGA5CTL PWGA5.CTL
#define PWGA5CSBR PWGA5.CSBR
#define PWGA5CRBR PWGA5.CRBR
#define PWGA5CTBR PWGA5.CTBR
#define PWGA6CSDR PWGA6.CSDR
#define PWGA6CRDR PWGA6.CRDR
#define PWGA6CTDR PWGA6.CTDR
#define PWGA6RDT PWGA6.RDT
#define PWGA6RSF PWGA6.RSF
#define PWGA6CNT PWGA6.CNT
#define PWGA6CTL PWGA6.CTL
#define PWGA6CSBR PWGA6.CSBR
#define PWGA6CRBR PWGA6.CRBR
#define PWGA6CTBR PWGA6.CTBR
#define PWGA7CSDR PWGA7.CSDR
#define PWGA7CRDR PWGA7.CRDR
#define PWGA7CTDR PWGA7.CTDR
#define PWGA7RDT PWGA7.RDT
#define PWGA7RSF PWGA7.RSF
#define PWGA7CNT PWGA7.CNT
#define PWGA7CTL PWGA7.CTL
#define PWGA7CSBR PWGA7.CSBR
#define PWGA7CRBR PWGA7.CRBR
#define PWGA7CTBR PWGA7.CTBR
#define PWGA8CSDR PWGA8.CSDR
#define PWGA8CRDR PWGA8.CRDR
#define PWGA8CTDR PWGA8.CTDR
#define PWGA8RDT PWGA8.RDT
#define PWGA8RSF PWGA8.RSF
#define PWGA8CNT PWGA8.CNT
#define PWGA8CTL PWGA8.CTL
#define PWGA8CSBR PWGA8.CSBR
#define PWGA8CRBR PWGA8.CRBR
#define PWGA8CTBR PWGA8.CTBR
#define PWGA9CSDR PWGA9.CSDR
#define PWGA9CRDR PWGA9.CRDR
#define PWGA9CTDR PWGA9.CTDR
#define PWGA9RDT PWGA9.RDT
#define PWGA9RSF PWGA9.RSF
#define PWGA9CNT PWGA9.CNT
#define PWGA9CTL PWGA9.CTL
#define PWGA9CSBR PWGA9.CSBR
#define PWGA9CRBR PWGA9.CRBR
#define PWGA9CTBR PWGA9.CTBR
#define PWGA10CSDR PWGA10.CSDR
#define PWGA10CRDR PWGA10.CRDR
#define PWGA10CTDR PWGA10.CTDR
#define PWGA10RDT PWGA10.RDT
#define PWGA10RSF PWGA10.RSF
#define PWGA10CNT PWGA10.CNT
#define PWGA10CTL PWGA10.CTL
#define PWGA10CSBR PWGA10.CSBR
#define PWGA10CRBR PWGA10.CRBR
#define PWGA10CTBR PWGA10.CTBR
#define PWGA11CSDR PWGA11.CSDR
#define PWGA11CRDR PWGA11.CRDR
#define PWGA11CTDR PWGA11.CTDR
#define PWGA11RDT PWGA11.RDT
#define PWGA11RSF PWGA11.RSF
#define PWGA11CNT PWGA11.CNT
#define PWGA11CTL PWGA11.CTL
#define PWGA11CSBR PWGA11.CSBR
#define PWGA11CRBR PWGA11.CRBR
#define PWGA11CTBR PWGA11.CTBR
#define PWGA12CSDR PWGA12.CSDR
#define PWGA12CRDR PWGA12.CRDR
#define PWGA12CTDR PWGA12.CTDR
#define PWGA12RDT PWGA12.RDT
#define PWGA12RSF PWGA12.RSF
#define PWGA12CNT PWGA12.CNT
#define PWGA12CTL PWGA12.CTL
#define PWGA12CSBR PWGA12.CSBR
#define PWGA12CRBR PWGA12.CRBR
#define PWGA12CTBR PWGA12.CTBR
#define PWGA13CSDR PWGA13.CSDR
#define PWGA13CRDR PWGA13.CRDR
#define PWGA13CTDR PWGA13.CTDR
#define PWGA13RDT PWGA13.RDT
#define PWGA13RSF PWGA13.RSF
#define PWGA13CNT PWGA13.CNT
#define PWGA13CTL PWGA13.CTL
#define PWGA13CSBR PWGA13.CSBR
#define PWGA13CRBR PWGA13.CRBR
#define PWGA13CTBR PWGA13.CTBR
#define PWGA14CSDR PWGA14.CSDR
#define PWGA14CRDR PWGA14.CRDR
#define PWGA14CTDR PWGA14.CTDR
#define PWGA14RDT PWGA14.RDT
#define PWGA14RSF PWGA14.RSF
#define PWGA14CNT PWGA14.CNT
#define PWGA14CTL PWGA14.CTL
#define PWGA14CSBR PWGA14.CSBR
#define PWGA14CRBR PWGA14.CRBR
#define PWGA14CTBR PWGA14.CTBR
#define PWGA15CSDR PWGA15.CSDR
#define PWGA15CRDR PWGA15.CRDR
#define PWGA15CTDR PWGA15.CTDR
#define PWGA15RDT PWGA15.RDT
#define PWGA15RSF PWGA15.RSF
#define PWGA15CNT PWGA15.CNT
#define PWGA15CTL PWGA15.CTL
#define PWGA15CSBR PWGA15.CSBR
#define PWGA15CRBR PWGA15.CRBR
#define PWGA15CTBR PWGA15.CTBR
#define PWGA16CSDR PWGA16.CSDR
#define PWGA16CRDR PWGA16.CRDR
#define PWGA16CTDR PWGA16.CTDR
#define PWGA16RDT PWGA16.RDT
#define PWGA16RSF PWGA16.RSF
#define PWGA16CNT PWGA16.CNT
#define PWGA16CTL PWGA16.CTL
#define PWGA16CSBR PWGA16.CSBR
#define PWGA16CRBR PWGA16.CRBR
#define PWGA16CTBR PWGA16.CTBR
#define PWGA17CSDR PWGA17.CSDR
#define PWGA17CRDR PWGA17.CRDR
#define PWGA17CTDR PWGA17.CTDR
#define PWGA17RDT PWGA17.RDT
#define PWGA17RSF PWGA17.RSF
#define PWGA17CNT PWGA17.CNT
#define PWGA17CTL PWGA17.CTL
#define PWGA17CSBR PWGA17.CSBR
#define PWGA17CRBR PWGA17.CRBR
#define PWGA17CTBR PWGA17.CTBR
#define PWGA18CSDR PWGA18.CSDR
#define PWGA18CRDR PWGA18.CRDR
#define PWGA18CTDR PWGA18.CTDR
#define PWGA18RDT PWGA18.RDT
#define PWGA18RSF PWGA18.RSF
#define PWGA18CNT PWGA18.CNT
#define PWGA18CTL PWGA18.CTL
#define PWGA18CSBR PWGA18.CSBR
#define PWGA18CRBR PWGA18.CRBR
#define PWGA18CTBR PWGA18.CTBR
#define PWGA19CSDR PWGA19.CSDR
#define PWGA19CRDR PWGA19.CRDR
#define PWGA19CTDR PWGA19.CTDR
#define PWGA19RDT PWGA19.RDT
#define PWGA19RSF PWGA19.RSF
#define PWGA19CNT PWGA19.CNT
#define PWGA19CTL PWGA19.CTL
#define PWGA19CSBR PWGA19.CSBR
#define PWGA19CRBR PWGA19.CRBR
#define PWGA19CTBR PWGA19.CTBR
#define PWGA20CSDR PWGA20.CSDR
#define PWGA20CRDR PWGA20.CRDR
#define PWGA20CTDR PWGA20.CTDR
#define PWGA20RDT PWGA20.RDT
#define PWGA20RSF PWGA20.RSF
#define PWGA20CNT PWGA20.CNT
#define PWGA20CTL PWGA20.CTL
#define PWGA20CSBR PWGA20.CSBR
#define PWGA20CRBR PWGA20.CRBR
#define PWGA20CTBR PWGA20.CTBR
#define PWGA21CSDR PWGA21.CSDR
#define PWGA21CRDR PWGA21.CRDR
#define PWGA21CTDR PWGA21.CTDR
#define PWGA21RDT PWGA21.RDT
#define PWGA21RSF PWGA21.RSF
#define PWGA21CNT PWGA21.CNT
#define PWGA21CTL PWGA21.CTL
#define PWGA21CSBR PWGA21.CSBR
#define PWGA21CRBR PWGA21.CRBR
#define PWGA21CTBR PWGA21.CTBR
#define PWGA22CSDR PWGA22.CSDR
#define PWGA22CRDR PWGA22.CRDR
#define PWGA22CTDR PWGA22.CTDR
#define PWGA22RDT PWGA22.RDT
#define PWGA22RSF PWGA22.RSF
#define PWGA22CNT PWGA22.CNT
#define PWGA22CTL PWGA22.CTL
#define PWGA22CSBR PWGA22.CSBR
#define PWGA22CRBR PWGA22.CRBR
#define PWGA22CTBR PWGA22.CTBR
#define PWGA23CSDR PWGA23.CSDR
#define PWGA23CRDR PWGA23.CRDR
#define PWGA23CTDR PWGA23.CTDR
#define PWGA23RDT PWGA23.RDT
#define PWGA23RSF PWGA23.RSF
#define PWGA23CNT PWGA23.CNT
#define PWGA23CTL PWGA23.CTL
#define PWGA23CSBR PWGA23.CSBR
#define PWGA23CRBR PWGA23.CRBR
#define PWGA23CTBR PWGA23.CTBR
#define PWGA24CSDR PWGA24.CSDR
#define PWGA24CRDR PWGA24.CRDR
#define PWGA24CTDR PWGA24.CTDR
#define PWGA24RDT PWGA24.RDT
#define PWGA24RSF PWGA24.RSF
#define PWGA24CNT PWGA24.CNT
#define PWGA24CTL PWGA24.CTL
#define PWGA24CSBR PWGA24.CSBR
#define PWGA24CRBR PWGA24.CRBR
#define PWGA24CTBR PWGA24.CTBR
#define PWGA25CSDR PWGA25.CSDR
#define PWGA25CRDR PWGA25.CRDR
#define PWGA25CTDR PWGA25.CTDR
#define PWGA25RDT PWGA25.RDT
#define PWGA25RSF PWGA25.RSF
#define PWGA25CNT PWGA25.CNT
#define PWGA25CTL PWGA25.CTL
#define PWGA25CSBR PWGA25.CSBR
#define PWGA25CRBR PWGA25.CRBR
#define PWGA25CTBR PWGA25.CTBR
#define PWGA26CSDR PWGA26.CSDR
#define PWGA26CRDR PWGA26.CRDR
#define PWGA26CTDR PWGA26.CTDR
#define PWGA26RDT PWGA26.RDT
#define PWGA26RSF PWGA26.RSF
#define PWGA26CNT PWGA26.CNT
#define PWGA26CTL PWGA26.CTL
#define PWGA26CSBR PWGA26.CSBR
#define PWGA26CRBR PWGA26.CRBR
#define PWGA26CTBR PWGA26.CTBR
#define PWGA27CSDR PWGA27.CSDR
#define PWGA27CRDR PWGA27.CRDR
#define PWGA27CTDR PWGA27.CTDR
#define PWGA27RDT PWGA27.RDT
#define PWGA27RSF PWGA27.RSF
#define PWGA27CNT PWGA27.CNT
#define PWGA27CTL PWGA27.CTL
#define PWGA27CSBR PWGA27.CSBR
#define PWGA27CRBR PWGA27.CRBR
#define PWGA27CTBR PWGA27.CTBR
#define PWGA28CSDR PWGA28.CSDR
#define PWGA28CRDR PWGA28.CRDR
#define PWGA28CTDR PWGA28.CTDR
#define PWGA28RDT PWGA28.RDT
#define PWGA28RSF PWGA28.RSF
#define PWGA28CNT PWGA28.CNT
#define PWGA28CTL PWGA28.CTL
#define PWGA28CSBR PWGA28.CSBR
#define PWGA28CRBR PWGA28.CRBR
#define PWGA28CTBR PWGA28.CTBR
#define PWGA29CSDR PWGA29.CSDR
#define PWGA29CRDR PWGA29.CRDR
#define PWGA29CTDR PWGA29.CTDR
#define PWGA29RDT PWGA29.RDT
#define PWGA29RSF PWGA29.RSF
#define PWGA29CNT PWGA29.CNT
#define PWGA29CTL PWGA29.CTL
#define PWGA29CSBR PWGA29.CSBR
#define PWGA29CRBR PWGA29.CRBR
#define PWGA29CTBR PWGA29.CTBR
#define PWGA30CSDR PWGA30.CSDR
#define PWGA30CRDR PWGA30.CRDR
#define PWGA30CTDR PWGA30.CTDR
#define PWGA30RDT PWGA30.RDT
#define PWGA30RSF PWGA30.RSF
#define PWGA30CNT PWGA30.CNT
#define PWGA30CTL PWGA30.CTL
#define PWGA30CSBR PWGA30.CSBR
#define PWGA30CRBR PWGA30.CRBR
#define PWGA30CTBR PWGA30.CTBR
#define PWGA31CSDR PWGA31.CSDR
#define PWGA31CRDR PWGA31.CRDR
#define PWGA31CTDR PWGA31.CTDR
#define PWGA31RDT PWGA31.RDT
#define PWGA31RSF PWGA31.RSF
#define PWGA31CNT PWGA31.CNT
#define PWGA31CTL PWGA31.CTL
#define PWGA31CSBR PWGA31.CSBR
#define PWGA31CRBR PWGA31.CRBR
#define PWGA31CTBR PWGA31.CTBR
#define PWGA32CSDR PWGA32.CSDR
#define PWGA32CRDR PWGA32.CRDR
#define PWGA32CTDR PWGA32.CTDR
#define PWGA32RDT PWGA32.RDT
#define PWGA32RSF PWGA32.RSF
#define PWGA32CNT PWGA32.CNT
#define PWGA32CTL PWGA32.CTL
#define PWGA32CSBR PWGA32.CSBR
#define PWGA32CRBR PWGA32.CRBR
#define PWGA32CTBR PWGA32.CTBR
#define PWGA33CSDR PWGA33.CSDR
#define PWGA33CRDR PWGA33.CRDR
#define PWGA33CTDR PWGA33.CTDR
#define PWGA33RDT PWGA33.RDT
#define PWGA33RSF PWGA33.RSF
#define PWGA33CNT PWGA33.CNT
#define PWGA33CTL PWGA33.CTL
#define PWGA33CSBR PWGA33.CSBR
#define PWGA33CRBR PWGA33.CRBR
#define PWGA33CTBR PWGA33.CTBR
#define PWGA34CSDR PWGA34.CSDR
#define PWGA34CRDR PWGA34.CRDR
#define PWGA34CTDR PWGA34.CTDR
#define PWGA34RDT PWGA34.RDT
#define PWGA34RSF PWGA34.RSF
#define PWGA34CNT PWGA34.CNT
#define PWGA34CTL PWGA34.CTL
#define PWGA34CSBR PWGA34.CSBR
#define PWGA34CRBR PWGA34.CRBR
#define PWGA34CTBR PWGA34.CTBR
#define PWGA35CSDR PWGA35.CSDR
#define PWGA35CRDR PWGA35.CRDR
#define PWGA35CTDR PWGA35.CTDR
#define PWGA35RDT PWGA35.RDT
#define PWGA35RSF PWGA35.RSF
#define PWGA35CNT PWGA35.CNT
#define PWGA35CTL PWGA35.CTL
#define PWGA35CSBR PWGA35.CSBR
#define PWGA35CRBR PWGA35.CRBR
#define PWGA35CTBR PWGA35.CTBR
#define PWGA36CSDR PWGA36.CSDR
#define PWGA36CRDR PWGA36.CRDR
#define PWGA36CTDR PWGA36.CTDR
#define PWGA36RDT PWGA36.RDT
#define PWGA36RSF PWGA36.RSF
#define PWGA36CNT PWGA36.CNT
#define PWGA36CTL PWGA36.CTL
#define PWGA36CSBR PWGA36.CSBR
#define PWGA36CRBR PWGA36.CRBR
#define PWGA36CTBR PWGA36.CTBR
#define PWGA37CSDR PWGA37.CSDR
#define PWGA37CRDR PWGA37.CRDR
#define PWGA37CTDR PWGA37.CTDR
#define PWGA37RDT PWGA37.RDT
#define PWGA37RSF PWGA37.RSF
#define PWGA37CNT PWGA37.CNT
#define PWGA37CTL PWGA37.CTL
#define PWGA37CSBR PWGA37.CSBR
#define PWGA37CRBR PWGA37.CRBR
#define PWGA37CTBR PWGA37.CTBR
#define PWGA38CSDR PWGA38.CSDR
#define PWGA38CRDR PWGA38.CRDR
#define PWGA38CTDR PWGA38.CTDR
#define PWGA38RDT PWGA38.RDT
#define PWGA38RSF PWGA38.RSF
#define PWGA38CNT PWGA38.CNT
#define PWGA38CTL PWGA38.CTL
#define PWGA38CSBR PWGA38.CSBR
#define PWGA38CRBR PWGA38.CRBR
#define PWGA38CTBR PWGA38.CTBR
#define PWGA39CSDR PWGA39.CSDR
#define PWGA39CRDR PWGA39.CRDR
#define PWGA39CTDR PWGA39.CTDR
#define PWGA39RDT PWGA39.RDT
#define PWGA39RSF PWGA39.RSF
#define PWGA39CNT PWGA39.CNT
#define PWGA39CTL PWGA39.CTL
#define PWGA39CSBR PWGA39.CSBR
#define PWGA39CRBR PWGA39.CRBR
#define PWGA39CTBR PWGA39.CTBR
#define PWGA40CSDR PWGA40.CSDR
#define PWGA40CRDR PWGA40.CRDR
#define PWGA40CTDR PWGA40.CTDR
#define PWGA40RDT PWGA40.RDT
#define PWGA40RSF PWGA40.RSF
#define PWGA40CNT PWGA40.CNT
#define PWGA40CTL PWGA40.CTL
#define PWGA40CSBR PWGA40.CSBR
#define PWGA40CRBR PWGA40.CRBR
#define PWGA40CTBR PWGA40.CTBR
#define PWGA41CSDR PWGA41.CSDR
#define PWGA41CRDR PWGA41.CRDR
#define PWGA41CTDR PWGA41.CTDR
#define PWGA41RDT PWGA41.RDT
#define PWGA41RSF PWGA41.RSF
#define PWGA41CNT PWGA41.CNT
#define PWGA41CTL PWGA41.CTL
#define PWGA41CSBR PWGA41.CSBR
#define PWGA41CRBR PWGA41.CRBR
#define PWGA41CTBR PWGA41.CTBR
#define PWGA42CSDR PWGA42.CSDR
#define PWGA42CRDR PWGA42.CRDR
#define PWGA42CTDR PWGA42.CTDR
#define PWGA42RDT PWGA42.RDT
#define PWGA42RSF PWGA42.RSF
#define PWGA42CNT PWGA42.CNT
#define PWGA42CTL PWGA42.CTL
#define PWGA42CSBR PWGA42.CSBR
#define PWGA42CRBR PWGA42.CRBR
#define PWGA42CTBR PWGA42.CTBR
#define PWGA43CSDR PWGA43.CSDR
#define PWGA43CRDR PWGA43.CRDR
#define PWGA43CTDR PWGA43.CTDR
#define PWGA43RDT PWGA43.RDT
#define PWGA43RSF PWGA43.RSF
#define PWGA43CNT PWGA43.CNT
#define PWGA43CTL PWGA43.CTL
#define PWGA43CSBR PWGA43.CSBR
#define PWGA43CRBR PWGA43.CRBR
#define PWGA43CTBR PWGA43.CTBR
#define PWGA44CSDR PWGA44.CSDR
#define PWGA44CRDR PWGA44.CRDR
#define PWGA44CTDR PWGA44.CTDR
#define PWGA44RDT PWGA44.RDT
#define PWGA44RSF PWGA44.RSF
#define PWGA44CNT PWGA44.CNT
#define PWGA44CTL PWGA44.CTL
#define PWGA44CSBR PWGA44.CSBR
#define PWGA44CRBR PWGA44.CRBR
#define PWGA44CTBR PWGA44.CTBR
#define PWGA45CSDR PWGA45.CSDR
#define PWGA45CRDR PWGA45.CRDR
#define PWGA45CTDR PWGA45.CTDR
#define PWGA45RDT PWGA45.RDT
#define PWGA45RSF PWGA45.RSF
#define PWGA45CNT PWGA45.CNT
#define PWGA45CTL PWGA45.CTL
#define PWGA45CSBR PWGA45.CSBR
#define PWGA45CRBR PWGA45.CRBR
#define PWGA45CTBR PWGA45.CTBR
#define PWGA46CSDR PWGA46.CSDR
#define PWGA46CRDR PWGA46.CRDR
#define PWGA46CTDR PWGA46.CTDR
#define PWGA46RDT PWGA46.RDT
#define PWGA46RSF PWGA46.RSF
#define PWGA46CNT PWGA46.CNT
#define PWGA46CTL PWGA46.CTL
#define PWGA46CSBR PWGA46.CSBR
#define PWGA46CRBR PWGA46.CRBR
#define PWGA46CTBR PWGA46.CTBR
#define PWGA47CSDR PWGA47.CSDR
#define PWGA47CRDR PWGA47.CRDR
#define PWGA47CTDR PWGA47.CTDR
#define PWGA47RDT PWGA47.RDT
#define PWGA47RSF PWGA47.RSF
#define PWGA47CNT PWGA47.CNT
#define PWGA47CTL PWGA47.CTL
#define PWGA47CSBR PWGA47.CSBR
#define PWGA47CRBR PWGA47.CRBR
#define PWGA47CTBR PWGA47.CTBR
#define PWGA48CSDR PWGA48.CSDR
#define PWGA48CRDR PWGA48.CRDR
#define PWGA48CTDR PWGA48.CTDR
#define PWGA48RDT PWGA48.RDT
#define PWGA48RSF PWGA48.RSF
#define PWGA48CNT PWGA48.CNT
#define PWGA48CTL PWGA48.CTL
#define PWGA48CSBR PWGA48.CSBR
#define PWGA48CRBR PWGA48.CRBR
#define PWGA48CTBR PWGA48.CTBR
#define PWGA49CSDR PWGA49.CSDR
#define PWGA49CRDR PWGA49.CRDR
#define PWGA49CTDR PWGA49.CTDR
#define PWGA49RDT PWGA49.RDT
#define PWGA49RSF PWGA49.RSF
#define PWGA49CNT PWGA49.CNT
#define PWGA49CTL PWGA49.CTL
#define PWGA49CSBR PWGA49.CSBR
#define PWGA49CRBR PWGA49.CRBR
#define PWGA49CTBR PWGA49.CTBR
#define PWGA50CSDR PWGA50.CSDR
#define PWGA50CRDR PWGA50.CRDR
#define PWGA50CTDR PWGA50.CTDR
#define PWGA50RDT PWGA50.RDT
#define PWGA50RSF PWGA50.RSF
#define PWGA50CNT PWGA50.CNT
#define PWGA50CTL PWGA50.CTL
#define PWGA50CSBR PWGA50.CSBR
#define PWGA50CRBR PWGA50.CRBR
#define PWGA50CTBR PWGA50.CTBR
#define PWGA51CSDR PWGA51.CSDR
#define PWGA51CRDR PWGA51.CRDR
#define PWGA51CTDR PWGA51.CTDR
#define PWGA51RDT PWGA51.RDT
#define PWGA51RSF PWGA51.RSF
#define PWGA51CNT PWGA51.CNT
#define PWGA51CTL PWGA51.CTL
#define PWGA51CSBR PWGA51.CSBR
#define PWGA51CRBR PWGA51.CRBR
#define PWGA51CTBR PWGA51.CTBR
#define PWGA52CSDR PWGA52.CSDR
#define PWGA52CRDR PWGA52.CRDR
#define PWGA52CTDR PWGA52.CTDR
#define PWGA52RDT PWGA52.RDT
#define PWGA52RSF PWGA52.RSF
#define PWGA52CNT PWGA52.CNT
#define PWGA52CTL PWGA52.CTL
#define PWGA52CSBR PWGA52.CSBR
#define PWGA52CRBR PWGA52.CRBR
#define PWGA52CTBR PWGA52.CTBR
#define PWGA53CSDR PWGA53.CSDR
#define PWGA53CRDR PWGA53.CRDR
#define PWGA53CTDR PWGA53.CTDR
#define PWGA53RDT PWGA53.RDT
#define PWGA53RSF PWGA53.RSF
#define PWGA53CNT PWGA53.CNT
#define PWGA53CTL PWGA53.CTL
#define PWGA53CSBR PWGA53.CSBR
#define PWGA53CRBR PWGA53.CRBR
#define PWGA53CTBR PWGA53.CTBR
#define PWGA54CSDR PWGA54.CSDR
#define PWGA54CRDR PWGA54.CRDR
#define PWGA54CTDR PWGA54.CTDR
#define PWGA54RDT PWGA54.RDT
#define PWGA54RSF PWGA54.RSF
#define PWGA54CNT PWGA54.CNT
#define PWGA54CTL PWGA54.CTL
#define PWGA54CSBR PWGA54.CSBR
#define PWGA54CRBR PWGA54.CRBR
#define PWGA54CTBR PWGA54.CTBR
#define PWGA55CSDR PWGA55.CSDR
#define PWGA55CRDR PWGA55.CRDR
#define PWGA55CTDR PWGA55.CTDR
#define PWGA55RDT PWGA55.RDT
#define PWGA55RSF PWGA55.RSF
#define PWGA55CNT PWGA55.CNT
#define PWGA55CTL PWGA55.CTL
#define PWGA55CSBR PWGA55.CSBR
#define PWGA55CRBR PWGA55.CRBR
#define PWGA55CTBR PWGA55.CTBR
#define PWGA56CSDR PWGA56.CSDR
#define PWGA56CRDR PWGA56.CRDR
#define PWGA56CTDR PWGA56.CTDR
#define PWGA56RDT PWGA56.RDT
#define PWGA56RSF PWGA56.RSF
#define PWGA56CNT PWGA56.CNT
#define PWGA56CTL PWGA56.CTL
#define PWGA56CSBR PWGA56.CSBR
#define PWGA56CRBR PWGA56.CRBR
#define PWGA56CTBR PWGA56.CTBR
#define PWGA57CSDR PWGA57.CSDR
#define PWGA57CRDR PWGA57.CRDR
#define PWGA57CTDR PWGA57.CTDR
#define PWGA57RDT PWGA57.RDT
#define PWGA57RSF PWGA57.RSF
#define PWGA57CNT PWGA57.CNT
#define PWGA57CTL PWGA57.CTL
#define PWGA57CSBR PWGA57.CSBR
#define PWGA57CRBR PWGA57.CRBR
#define PWGA57CTBR PWGA57.CTBR
#define PWGA58CSDR PWGA58.CSDR
#define PWGA58CRDR PWGA58.CRDR
#define PWGA58CTDR PWGA58.CTDR
#define PWGA58RDT PWGA58.RDT
#define PWGA58RSF PWGA58.RSF
#define PWGA58CNT PWGA58.CNT
#define PWGA58CTL PWGA58.CTL
#define PWGA58CSBR PWGA58.CSBR
#define PWGA58CRBR PWGA58.CRBR
#define PWGA58CTBR PWGA58.CTBR
#define PWGA59CSDR PWGA59.CSDR
#define PWGA59CRDR PWGA59.CRDR
#define PWGA59CTDR PWGA59.CTDR
#define PWGA59RDT PWGA59.RDT
#define PWGA59RSF PWGA59.RSF
#define PWGA59CNT PWGA59.CNT
#define PWGA59CTL PWGA59.CTL
#define PWGA59CSBR PWGA59.CSBR
#define PWGA59CRBR PWGA59.CRBR
#define PWGA59CTBR PWGA59.CTBR
#define PWGA60CSDR PWGA60.CSDR
#define PWGA60CRDR PWGA60.CRDR
#define PWGA60CTDR PWGA60.CTDR
#define PWGA60RDT PWGA60.RDT
#define PWGA60RSF PWGA60.RSF
#define PWGA60CNT PWGA60.CNT
#define PWGA60CTL PWGA60.CTL
#define PWGA60CSBR PWGA60.CSBR
#define PWGA60CRBR PWGA60.CRBR
#define PWGA60CTBR PWGA60.CTBR
#define PWGA61CSDR PWGA61.CSDR
#define PWGA61CRDR PWGA61.CRDR
#define PWGA61CTDR PWGA61.CTDR
#define PWGA61RDT PWGA61.RDT
#define PWGA61RSF PWGA61.RSF
#define PWGA61CNT PWGA61.CNT
#define PWGA61CTL PWGA61.CTL
#define PWGA61CSBR PWGA61.CSBR
#define PWGA61CRBR PWGA61.CRBR
#define PWGA61CTBR PWGA61.CTBR
#define PWGA62CSDR PWGA62.CSDR
#define PWGA62CRDR PWGA62.CRDR
#define PWGA62CTDR PWGA62.CTDR
#define PWGA62RDT PWGA62.RDT
#define PWGA62RSF PWGA62.RSF
#define PWGA62CNT PWGA62.CNT
#define PWGA62CTL PWGA62.CTL
#define PWGA62CSBR PWGA62.CSBR
#define PWGA62CRBR PWGA62.CRBR
#define PWGA62CTBR PWGA62.CTBR
#define PWGA63CSDR PWGA63.CSDR
#define PWGA63CRDR PWGA63.CRDR
#define PWGA63CTDR PWGA63.CTDR
#define PWGA63RDT PWGA63.RDT
#define PWGA63RSF PWGA63.RSF
#define PWGA63CNT PWGA63.CNT
#define PWGA63CTL PWGA63.CTL
#define PWGA63CSBR PWGA63.CSBR
#define PWGA63CRBR PWGA63.CRBR
#define PWGA63CTBR PWGA63.CTBR
#define PWGA64CSDR PWGA64.CSDR
#define PWGA64CRDR PWGA64.CRDR
#define PWGA64CTDR PWGA64.CTDR
#define PWGA64RDT PWGA64.RDT
#define PWGA64RSF PWGA64.RSF
#define PWGA64CNT PWGA64.CNT
#define PWGA64CTL PWGA64.CTL
#define PWGA64CSBR PWGA64.CSBR
#define PWGA64CRBR PWGA64.CRBR
#define PWGA64CTBR PWGA64.CTBR
#define PWGA65CSDR PWGA65.CSDR
#define PWGA65CRDR PWGA65.CRDR
#define PWGA65CTDR PWGA65.CTDR
#define PWGA65RDT PWGA65.RDT
#define PWGA65RSF PWGA65.RSF
#define PWGA65CNT PWGA65.CNT
#define PWGA65CTL PWGA65.CTL
#define PWGA65CSBR PWGA65.CSBR
#define PWGA65CRBR PWGA65.CRBR
#define PWGA65CTBR PWGA65.CTBR
#define PWGA66CSDR PWGA66.CSDR
#define PWGA66CRDR PWGA66.CRDR
#define PWGA66CTDR PWGA66.CTDR
#define PWGA66RDT PWGA66.RDT
#define PWGA66RSF PWGA66.RSF
#define PWGA66CNT PWGA66.CNT
#define PWGA66CTL PWGA66.CTL
#define PWGA66CSBR PWGA66.CSBR
#define PWGA66CRBR PWGA66.CRBR
#define PWGA66CTBR PWGA66.CTBR
#define PWGA67CSDR PWGA67.CSDR
#define PWGA67CRDR PWGA67.CRDR
#define PWGA67CTDR PWGA67.CTDR
#define PWGA67RDT PWGA67.RDT
#define PWGA67RSF PWGA67.RSF
#define PWGA67CNT PWGA67.CNT
#define PWGA67CTL PWGA67.CTL
#define PWGA67CSBR PWGA67.CSBR
#define PWGA67CRBR PWGA67.CRBR
#define PWGA67CTBR PWGA67.CTBR
#define PWGA68CSDR PWGA68.CSDR
#define PWGA68CRDR PWGA68.CRDR
#define PWGA68CTDR PWGA68.CTDR
#define PWGA68RDT PWGA68.RDT
#define PWGA68RSF PWGA68.RSF
#define PWGA68CNT PWGA68.CNT
#define PWGA68CTL PWGA68.CTL
#define PWGA68CSBR PWGA68.CSBR
#define PWGA68CRBR PWGA68.CRBR
#define PWGA68CTBR PWGA68.CTBR
#define PWGA69CSDR PWGA69.CSDR
#define PWGA69CRDR PWGA69.CRDR
#define PWGA69CTDR PWGA69.CTDR
#define PWGA69RDT PWGA69.RDT
#define PWGA69RSF PWGA69.RSF
#define PWGA69CNT PWGA69.CNT
#define PWGA69CTL PWGA69.CTL
#define PWGA69CSBR PWGA69.CSBR
#define PWGA69CRBR PWGA69.CRBR
#define PWGA69CTBR PWGA69.CTBR
#define PWGA70CSDR PWGA70.CSDR
#define PWGA70CRDR PWGA70.CRDR
#define PWGA70CTDR PWGA70.CTDR
#define PWGA70RDT PWGA70.RDT
#define PWGA70RSF PWGA70.RSF
#define PWGA70CNT PWGA70.CNT
#define PWGA70CTL PWGA70.CTL
#define PWGA70CSBR PWGA70.CSBR
#define PWGA70CRBR PWGA70.CRBR
#define PWGA70CTBR PWGA70.CTBR
#define PWGA71CSDR PWGA71.CSDR
#define PWGA71CRDR PWGA71.CRDR
#define PWGA71CTDR PWGA71.CTDR
#define PWGA71RDT PWGA71.RDT
#define PWGA71RSF PWGA71.RSF
#define PWGA71CNT PWGA71.CNT
#define PWGA71CTL PWGA71.CTL
#define PWGA71CSBR PWGA71.CSBR
#define PWGA71CRBR PWGA71.CRBR
#define PWGA71CTBR PWGA71.CTBR
#define PWBA0BRS0 PWBA0.BRS0
#define PWBA0BRS1 PWBA0.BRS1
#define PWBA0BRS2 PWBA0.BRS2
#define PWBA0BRS3 PWBA0.BRS3
#define PWBA0TE PWBA0.TE
#define PWBA0TS PWBA0.TS
#define PWBA0TT PWBA0.TT
#define PWBA0EMU PWBA0.EMU
#define RTCA0CTL0 RTCA0.CTL0.uint8_t
#define RTCA0SLSB RTCA0.CTL0.SLSB
#define RTCA0AMPM RTCA0.CTL0.AMPM
#define RTCA0CEST RTCA0.CTL0.CEST
#define RTCA0CE RTCA0.CTL0.CE
#define RTCA0CTL1 RTCA0.CTL1.uint8_t
#define RTCA0CT0 RTCA0.CTL1.CT0
#define RTCA0CT1 RTCA0.CTL1.CT1
#define RTCA0CT2 RTCA0.CTL1.CT2
#define RTCA0EN1S RTCA0.CTL1.EN1S
#define RTCA0ENALM RTCA0.CTL1.ENALM
#define RTCA0EN1HZ RTCA0.CTL1.EN1HZ
#define RTCA0CTL2 RTCA0.CTL2.uint8_t
#define RTCA0WAIT RTCA0.CTL2.WAIT
#define RTCA0WST RTCA0.CTL2.WST
#define RTCA0RSUB RTCA0.CTL2.RSUB
#define RTCA0RSST RTCA0.CTL2.RSST
#define RTCA0WSST RTCA0.CTL2.WSST
#define RTCA0WUST RTCA0.CTL2.WUST
#define RTCA0SUBC RTCA0.SUBC
#define RTCA0SRBU RTCA0.SRBU
#define RTCA0SEC RTCA0.SEC
#define RTCA0MIN RTCA0.MIN
#define RTCA0HOUR RTCA0.HOUR
#define RTCA0WEEK RTCA0.WEEK
#define RTCA0DAY RTCA0.DAY
#define RTCA0MONTH RTCA0.MONTH
#define RTCA0YEAR RTCA0.YEAR
#define RTCA0TIME RTCA0.TIME
#define RTCA0CAL RTCA0.CAL
#define RTCA0SUBU RTCA0.SUBU
#define RTCA0SCMP RTCA0.SCMP
#define RTCA0ALM RTCA0.ALM
#define RTCA0ALH RTCA0.ALH
#define RTCA0ALW RTCA0.ALW
#define RTCA0SECC RTCA0.SECC
#define RTCA0MINC RTCA0.MINC
#define RTCA0HOURC RTCA0.HOURC
#define RTCA0WEEKC RTCA0.WEEKC
#define RTCA0DAYC RTCA0.DAYC
#define RTCA0MONC RTCA0.MONC
#define RTCA0YEARC RTCA0.YEARC
#define RTCA0TIMEC RTCA0.TIMEC
#define RTCA0CALC RTCA0.CALC
#define RTCA0EMU RTCA0.EMU.uint8_t
#define RTCA0SVSDIS RTCA0.EMU.SVSDIS
#define ENCA0CCR0 ENCA0.CCR0
#define ENCA0CCR1 ENCA0.CCR1
#define ENCA0CNT ENCA0.CNT
#define ENCA0FLG ENCA0.FLG
#define ENCA0FGC ENCA0.FGC
#define ENCA0TE ENCA0.TE
#define ENCA0TS ENCA0.TS
#define ENCA0TT ENCA0.TT
#define ENCA0IOC0 ENCA0.IOC0
#define ENCA0CTL ENCA0.CTL
#define ENCA0IOC1 ENCA0.IOC1
#define ENCA0EMU ENCA0.EMU
#define TAPA0FLG TAPA0.FLG
#define TAPA0ACWE TAPA0.ACWE
#define TAPA0ACTS TAPA0.ACTS
#define TAPA0ACTT TAPA0.ACTT
#define TAPA0OPHS TAPA0.OPHS
#define TAPA0OPHT TAPA0.OPHT
#define TAPA0CTL0 TAPA0.CTL0
#define TAPA0CTL1 TAPA0.CTL1
#define TAPA0EMU TAPA0.EMU
#define OSTM0CMP OSTM0.CMP
#define OSTM0CNT OSTM0.CNT
#define OSTM0TE OSTM0.TE
#define OSTM0TS OSTM0.TS
#define OSTM0TT OSTM0.TT
#define OSTM0CTL OSTM0.CTL
#define OSTM0EMU OSTM0.EMU
#define WDTA0WDTE WDTA0.WDTE
#define WDTA0EVAC WDTA0.EVAC
#define WDTA0REF WDTA0.REF
#define WDTA0MD WDTA0.MD
#define WDTA1WDTE WDTA1.WDTE
#define WDTA1EVAC WDTA1.EVAC
#define WDTA1REF WDTA1.REF
#define WDTA1MD WDTA1.MD
#define ADCA0VCR00 ADCA0.VCR00.uint32_t
#define ADCA0VCR00L ADCA0.VCR00.uint16_t[L]
#define ADCA0VCR00LL ADCA0.VCR00.uint8_t[LL]
#define ADCA0VCR00LH ADCA0.VCR00.uint8_t[LH]
#define ADCA0VCR01 ADCA0.VCR01.uint32_t
#define ADCA0VCR01L ADCA0.VCR01.uint16_t[L]
#define ADCA0VCR01LL ADCA0.VCR01.uint8_t[LL]
#define ADCA0VCR01LH ADCA0.VCR01.uint8_t[LH]
#define ADCA0VCR02 ADCA0.VCR02.uint32_t
#define ADCA0VCR02L ADCA0.VCR02.uint16_t[L]
#define ADCA0VCR02LL ADCA0.VCR02.uint8_t[LL]
#define ADCA0VCR02LH ADCA0.VCR02.uint8_t[LH]
#define ADCA0VCR03 ADCA0.VCR03.uint32_t
#define ADCA0VCR03L ADCA0.VCR03.uint16_t[L]
#define ADCA0VCR03LL ADCA0.VCR03.uint8_t[LL]
#define ADCA0VCR03LH ADCA0.VCR03.uint8_t[LH]
#define ADCA0VCR04 ADCA0.VCR04.uint32_t
#define ADCA0VCR04L ADCA0.VCR04.uint16_t[L]
#define ADCA0VCR04LL ADCA0.VCR04.uint8_t[LL]
#define ADCA0VCR04LH ADCA0.VCR04.uint8_t[LH]
#define ADCA0VCR05 ADCA0.VCR05.uint32_t
#define ADCA0VCR05L ADCA0.VCR05.uint16_t[L]
#define ADCA0VCR05LL ADCA0.VCR05.uint8_t[LL]
#define ADCA0VCR05LH ADCA0.VCR05.uint8_t[LH]
#define ADCA0VCR06 ADCA0.VCR06.uint32_t
#define ADCA0VCR06L ADCA0.VCR06.uint16_t[L]
#define ADCA0VCR06LL ADCA0.VCR06.uint8_t[LL]
#define ADCA0VCR06LH ADCA0.VCR06.uint8_t[LH]
#define ADCA0VCR07 ADCA0.VCR07.uint32_t
#define ADCA0VCR07L ADCA0.VCR07.uint16_t[L]
#define ADCA0VCR07LL ADCA0.VCR07.uint8_t[LL]
#define ADCA0VCR07LH ADCA0.VCR07.uint8_t[LH]
#define ADCA0VCR08 ADCA0.VCR08.uint32_t
#define ADCA0VCR08L ADCA0.VCR08.uint16_t[L]
#define ADCA0VCR08LL ADCA0.VCR08.uint8_t[LL]
#define ADCA0VCR08LH ADCA0.VCR08.uint8_t[LH]
#define ADCA0VCR09 ADCA0.VCR09.uint32_t
#define ADCA0VCR09L ADCA0.VCR09.uint16_t[L]
#define ADCA0VCR09LL ADCA0.VCR09.uint8_t[LL]
#define ADCA0VCR09LH ADCA0.VCR09.uint8_t[LH]
#define ADCA0VCR10 ADCA0.VCR10.uint32_t
#define ADCA0VCR10L ADCA0.VCR10.uint16_t[L]
#define ADCA0VCR10LL ADCA0.VCR10.uint8_t[LL]
#define ADCA0VCR10LH ADCA0.VCR10.uint8_t[LH]
#define ADCA0VCR11 ADCA0.VCR11.uint32_t
#define ADCA0VCR11L ADCA0.VCR11.uint16_t[L]
#define ADCA0VCR11LL ADCA0.VCR11.uint8_t[LL]
#define ADCA0VCR11LH ADCA0.VCR11.uint8_t[LH]
#define ADCA0VCR12 ADCA0.VCR12.uint32_t
#define ADCA0VCR12L ADCA0.VCR12.uint16_t[L]
#define ADCA0VCR12LL ADCA0.VCR12.uint8_t[LL]
#define ADCA0VCR12LH ADCA0.VCR12.uint8_t[LH]
#define ADCA0VCR13 ADCA0.VCR13.uint32_t
#define ADCA0VCR13L ADCA0.VCR13.uint16_t[L]
#define ADCA0VCR13LL ADCA0.VCR13.uint8_t[LL]
#define ADCA0VCR13LH ADCA0.VCR13.uint8_t[LH]
#define ADCA0VCR14 ADCA0.VCR14.uint32_t
#define ADCA0VCR14L ADCA0.VCR14.uint16_t[L]
#define ADCA0VCR14LL ADCA0.VCR14.uint8_t[LL]
#define ADCA0VCR14LH ADCA0.VCR14.uint8_t[LH]
#define ADCA0VCR15 ADCA0.VCR15.uint32_t
#define ADCA0VCR15L ADCA0.VCR15.uint16_t[L]
#define ADCA0VCR15LL ADCA0.VCR15.uint8_t[LL]
#define ADCA0VCR15LH ADCA0.VCR15.uint8_t[LH]
#define ADCA0VCR16 ADCA0.VCR16.uint32_t
#define ADCA0VCR16L ADCA0.VCR16.uint16_t[L]
#define ADCA0VCR16LL ADCA0.VCR16.uint8_t[LL]
#define ADCA0VCR16LH ADCA0.VCR16.uint8_t[LH]
#define ADCA0VCR17 ADCA0.VCR17.uint32_t
#define ADCA0VCR17L ADCA0.VCR17.uint16_t[L]
#define ADCA0VCR17LL ADCA0.VCR17.uint8_t[LL]
#define ADCA0VCR17LH ADCA0.VCR17.uint8_t[LH]
#define ADCA0VCR18 ADCA0.VCR18.uint32_t
#define ADCA0VCR18L ADCA0.VCR18.uint16_t[L]
#define ADCA0VCR18LL ADCA0.VCR18.uint8_t[LL]
#define ADCA0VCR18LH ADCA0.VCR18.uint8_t[LH]
#define ADCA0VCR19 ADCA0.VCR19.uint32_t
#define ADCA0VCR19L ADCA0.VCR19.uint16_t[L]
#define ADCA0VCR19LL ADCA0.VCR19.uint8_t[LL]
#define ADCA0VCR19LH ADCA0.VCR19.uint8_t[LH]
#define ADCA0VCR20 ADCA0.VCR20.uint32_t
#define ADCA0VCR20L ADCA0.VCR20.uint16_t[L]
#define ADCA0VCR20LL ADCA0.VCR20.uint8_t[LL]
#define ADCA0VCR20LH ADCA0.VCR20.uint8_t[LH]
#define ADCA0VCR21 ADCA0.VCR21.uint32_t
#define ADCA0VCR21L ADCA0.VCR21.uint16_t[L]
#define ADCA0VCR21LL ADCA0.VCR21.uint8_t[LL]
#define ADCA0VCR21LH ADCA0.VCR21.uint8_t[LH]
#define ADCA0VCR22 ADCA0.VCR22.uint32_t
#define ADCA0VCR22L ADCA0.VCR22.uint16_t[L]
#define ADCA0VCR22LL ADCA0.VCR22.uint8_t[LL]
#define ADCA0VCR22LH ADCA0.VCR22.uint8_t[LH]
#define ADCA0VCR23 ADCA0.VCR23.uint32_t
#define ADCA0VCR23L ADCA0.VCR23.uint16_t[L]
#define ADCA0VCR23LL ADCA0.VCR23.uint8_t[LL]
#define ADCA0VCR23LH ADCA0.VCR23.uint8_t[LH]
#define ADCA0VCR24 ADCA0.VCR24.uint32_t
#define ADCA0VCR24L ADCA0.VCR24.uint16_t[L]
#define ADCA0VCR24LL ADCA0.VCR24.uint8_t[LL]
#define ADCA0VCR24LH ADCA0.VCR24.uint8_t[LH]
#define ADCA0VCR25 ADCA0.VCR25.uint32_t
#define ADCA0VCR25L ADCA0.VCR25.uint16_t[L]
#define ADCA0VCR25LL ADCA0.VCR25.uint8_t[LL]
#define ADCA0VCR25LH ADCA0.VCR25.uint8_t[LH]
#define ADCA0VCR26 ADCA0.VCR26.uint32_t
#define ADCA0VCR26L ADCA0.VCR26.uint16_t[L]
#define ADCA0VCR26LL ADCA0.VCR26.uint8_t[LL]
#define ADCA0VCR26LH ADCA0.VCR26.uint8_t[LH]
#define ADCA0VCR27 ADCA0.VCR27.uint32_t
#define ADCA0VCR27L ADCA0.VCR27.uint16_t[L]
#define ADCA0VCR27LL ADCA0.VCR27.uint8_t[LL]
#define ADCA0VCR27LH ADCA0.VCR27.uint8_t[LH]
#define ADCA0VCR28 ADCA0.VCR28.uint32_t
#define ADCA0VCR28L ADCA0.VCR28.uint16_t[L]
#define ADCA0VCR28LL ADCA0.VCR28.uint8_t[LL]
#define ADCA0VCR28LH ADCA0.VCR28.uint8_t[LH]
#define ADCA0VCR29 ADCA0.VCR29.uint32_t
#define ADCA0VCR29L ADCA0.VCR29.uint16_t[L]
#define ADCA0VCR29LL ADCA0.VCR29.uint8_t[LL]
#define ADCA0VCR29LH ADCA0.VCR29.uint8_t[LH]
#define ADCA0VCR30 ADCA0.VCR30.uint32_t
#define ADCA0VCR30L ADCA0.VCR30.uint16_t[L]
#define ADCA0VCR30LL ADCA0.VCR30.uint8_t[LL]
#define ADCA0VCR30LH ADCA0.VCR30.uint8_t[LH]
#define ADCA0VCR31 ADCA0.VCR31.uint32_t
#define ADCA0VCR31L ADCA0.VCR31.uint16_t[L]
#define ADCA0VCR31LL ADCA0.VCR31.uint8_t[LL]
#define ADCA0VCR31LH ADCA0.VCR31.uint8_t[LH]
#define ADCA0VCR32 ADCA0.VCR32.uint32_t
#define ADCA0VCR32L ADCA0.VCR32.uint16_t[L]
#define ADCA0VCR32LL ADCA0.VCR32.uint8_t[LL]
#define ADCA0VCR32LH ADCA0.VCR32.uint8_t[LH]
#define ADCA0VCR33 ADCA0.VCR33.uint32_t
#define ADCA0VCR33L ADCA0.VCR33.uint16_t[L]
#define ADCA0VCR33LL ADCA0.VCR33.uint8_t[LL]
#define ADCA0VCR33LH ADCA0.VCR33.uint8_t[LH]
#define ADCA0VCR34 ADCA0.VCR34.uint32_t
#define ADCA0VCR34L ADCA0.VCR34.uint16_t[L]
#define ADCA0VCR34LL ADCA0.VCR34.uint8_t[LL]
#define ADCA0VCR34LH ADCA0.VCR34.uint8_t[LH]
#define ADCA0VCR35 ADCA0.VCR35.uint32_t
#define ADCA0VCR35L ADCA0.VCR35.uint16_t[L]
#define ADCA0VCR35LL ADCA0.VCR35.uint8_t[LL]
#define ADCA0VCR35LH ADCA0.VCR35.uint8_t[LH]
#define ADCA0VCR36 ADCA0.VCR36.uint32_t
#define ADCA0VCR36L ADCA0.VCR36.uint16_t[L]
#define ADCA0VCR36LL ADCA0.VCR36.uint8_t[LL]
#define ADCA0VCR36LH ADCA0.VCR36.uint8_t[LH]
#define ADCA0VCR37 ADCA0.VCR37.uint32_t
#define ADCA0VCR37L ADCA0.VCR37.uint16_t[L]
#define ADCA0VCR37LL ADCA0.VCR37.uint8_t[LL]
#define ADCA0VCR37LH ADCA0.VCR37.uint8_t[LH]
#define ADCA0VCR38 ADCA0.VCR38.uint32_t
#define ADCA0VCR38L ADCA0.VCR38.uint16_t[L]
#define ADCA0VCR38LL ADCA0.VCR38.uint8_t[LL]
#define ADCA0VCR38LH ADCA0.VCR38.uint8_t[LH]
#define ADCA0VCR39 ADCA0.VCR39.uint32_t
#define ADCA0VCR39L ADCA0.VCR39.uint16_t[L]
#define ADCA0VCR39LL ADCA0.VCR39.uint8_t[LL]
#define ADCA0VCR39LH ADCA0.VCR39.uint8_t[LH]
#define ADCA0VCR40 ADCA0.VCR40.uint32_t
#define ADCA0VCR40L ADCA0.VCR40.uint16_t[L]
#define ADCA0VCR40LL ADCA0.VCR40.uint8_t[LL]
#define ADCA0VCR40LH ADCA0.VCR40.uint8_t[LH]
#define ADCA0VCR41 ADCA0.VCR41.uint32_t
#define ADCA0VCR41L ADCA0.VCR41.uint16_t[L]
#define ADCA0VCR41LL ADCA0.VCR41.uint8_t[LL]
#define ADCA0VCR41LH ADCA0.VCR41.uint8_t[LH]
#define ADCA0VCR42 ADCA0.VCR42.uint32_t
#define ADCA0VCR42L ADCA0.VCR42.uint16_t[L]
#define ADCA0VCR42LL ADCA0.VCR42.uint8_t[LL]
#define ADCA0VCR42LH ADCA0.VCR42.uint8_t[LH]
#define ADCA0VCR43 ADCA0.VCR43.uint32_t
#define ADCA0VCR43L ADCA0.VCR43.uint16_t[L]
#define ADCA0VCR43LL ADCA0.VCR43.uint8_t[LL]
#define ADCA0VCR43LH ADCA0.VCR43.uint8_t[LH]
#define ADCA0VCR44 ADCA0.VCR44.uint32_t
#define ADCA0VCR44L ADCA0.VCR44.uint16_t[L]
#define ADCA0VCR44LL ADCA0.VCR44.uint8_t[LL]
#define ADCA0VCR44LH ADCA0.VCR44.uint8_t[LH]
#define ADCA0VCR45 ADCA0.VCR45.uint32_t
#define ADCA0VCR45L ADCA0.VCR45.uint16_t[L]
#define ADCA0VCR45LL ADCA0.VCR45.uint8_t[LL]
#define ADCA0VCR45LH ADCA0.VCR45.uint8_t[LH]
#define ADCA0VCR46 ADCA0.VCR46.uint32_t
#define ADCA0VCR46L ADCA0.VCR46.uint16_t[L]
#define ADCA0VCR46LL ADCA0.VCR46.uint8_t[LL]
#define ADCA0VCR46LH ADCA0.VCR46.uint8_t[LH]
#define ADCA0VCR47 ADCA0.VCR47.uint32_t
#define ADCA0VCR47L ADCA0.VCR47.uint16_t[L]
#define ADCA0VCR47LL ADCA0.VCR47.uint8_t[LL]
#define ADCA0VCR47LH ADCA0.VCR47.uint8_t[LH]
#define ADCA0VCR48 ADCA0.VCR48.uint32_t
#define ADCA0VCR48L ADCA0.VCR48.uint16_t[L]
#define ADCA0VCR48LL ADCA0.VCR48.uint8_t[LL]
#define ADCA0VCR48LH ADCA0.VCR48.uint8_t[LH]
#define ADCA0VCR49 ADCA0.VCR49.uint32_t
#define ADCA0VCR49L ADCA0.VCR49.uint16_t[L]
#define ADCA0VCR49LL ADCA0.VCR49.uint8_t[LL]
#define ADCA0VCR49LH ADCA0.VCR49.uint8_t[LH]
#define ADCA0PWDVCR ADCA0.PWDVCR.uint32_t
#define ADCA0PWDVCRL ADCA0.PWDVCR.uint16_t[L]
#define ADCA0PWDVCRLL ADCA0.PWDVCR.uint8_t[LL]
#define ADCA0PWDVCRLH ADCA0.PWDVCR.uint8_t[LH]
#define ADCA0DR00 ADCA0.DR00.uint32_t
#define ADCA0DR00L ADCA0.DR00.uint16_t[L]
#define ADCA0DR00H ADCA0.DR00.uint16_t[H]
#define ADCA0DR02 ADCA0.DR02.uint32_t
#define ADCA0DR02L ADCA0.DR02.uint16_t[L]
#define ADCA0DR02H ADCA0.DR02.uint16_t[H]
#define ADCA0DR04 ADCA0.DR04.uint32_t
#define ADCA0DR04L ADCA0.DR04.uint16_t[L]
#define ADCA0DR04H ADCA0.DR04.uint16_t[H]
#define ADCA0DR06 ADCA0.DR06.uint32_t
#define ADCA0DR06L ADCA0.DR06.uint16_t[L]
#define ADCA0DR06H ADCA0.DR06.uint16_t[H]
#define ADCA0DR08 ADCA0.DR08.uint32_t
#define ADCA0DR08L ADCA0.DR08.uint16_t[L]
#define ADCA0DR08H ADCA0.DR08.uint16_t[H]
#define ADCA0DR10 ADCA0.DR10.uint32_t
#define ADCA0DR10L ADCA0.DR10.uint16_t[L]
#define ADCA0DR10H ADCA0.DR10.uint16_t[H]
#define ADCA0DR12 ADCA0.DR12.uint32_t
#define ADCA0DR12L ADCA0.DR12.uint16_t[L]
#define ADCA0DR12H ADCA0.DR12.uint16_t[H]
#define ADCA0DR14 ADCA0.DR14.uint32_t
#define ADCA0DR14L ADCA0.DR14.uint16_t[L]
#define ADCA0DR14H ADCA0.DR14.uint16_t[H]
#define ADCA0DR16 ADCA0.DR16.uint32_t
#define ADCA0DR16L ADCA0.DR16.uint16_t[L]
#define ADCA0DR16H ADCA0.DR16.uint16_t[H]
#define ADCA0DR18 ADCA0.DR18.uint32_t
#define ADCA0DR18L ADCA0.DR18.uint16_t[L]
#define ADCA0DR18H ADCA0.DR18.uint16_t[H]
#define ADCA0DR20 ADCA0.DR20.uint32_t
#define ADCA0DR20L ADCA0.DR20.uint16_t[L]
#define ADCA0DR20H ADCA0.DR20.uint16_t[H]
#define ADCA0DR22 ADCA0.DR22.uint32_t
#define ADCA0DR22L ADCA0.DR22.uint16_t[L]
#define ADCA0DR22H ADCA0.DR22.uint16_t[H]
#define ADCA0DR24 ADCA0.DR24.uint32_t
#define ADCA0DR24L ADCA0.DR24.uint16_t[L]
#define ADCA0DR24H ADCA0.DR24.uint16_t[H]
#define ADCA0DR26 ADCA0.DR26.uint32_t
#define ADCA0DR26L ADCA0.DR26.uint16_t[L]
#define ADCA0DR26H ADCA0.DR26.uint16_t[H]
#define ADCA0DR28 ADCA0.DR28.uint32_t
#define ADCA0DR28L ADCA0.DR28.uint16_t[L]
#define ADCA0DR28H ADCA0.DR28.uint16_t[H]
#define ADCA0DR30 ADCA0.DR30.uint32_t
#define ADCA0DR30L ADCA0.DR30.uint16_t[L]
#define ADCA0DR30H ADCA0.DR30.uint16_t[H]
#define ADCA0DR32 ADCA0.DR32.uint32_t
#define ADCA0DR32L ADCA0.DR32.uint16_t[L]
#define ADCA0DR32H ADCA0.DR32.uint16_t[H]
#define ADCA0DR34 ADCA0.DR34.uint32_t
#define ADCA0DR34L ADCA0.DR34.uint16_t[L]
#define ADCA0DR34H ADCA0.DR34.uint16_t[H]
#define ADCA0DR36 ADCA0.DR36.uint32_t
#define ADCA0DR36L ADCA0.DR36.uint16_t[L]
#define ADCA0DR36H ADCA0.DR36.uint16_t[H]
#define ADCA0DR38 ADCA0.DR38.uint32_t
#define ADCA0DR38L ADCA0.DR38.uint16_t[L]
#define ADCA0DR38H ADCA0.DR38.uint16_t[H]
#define ADCA0DR40 ADCA0.DR40.uint32_t
#define ADCA0DR40L ADCA0.DR40.uint16_t[L]
#define ADCA0DR40H ADCA0.DR40.uint16_t[H]
#define ADCA0DR42 ADCA0.DR42.uint32_t
#define ADCA0DR42L ADCA0.DR42.uint16_t[L]
#define ADCA0DR42H ADCA0.DR42.uint16_t[H]
#define ADCA0DR44 ADCA0.DR44.uint32_t
#define ADCA0DR44L ADCA0.DR44.uint16_t[L]
#define ADCA0DR44H ADCA0.DR44.uint16_t[H]
#define ADCA0DR46 ADCA0.DR46.uint32_t
#define ADCA0DR46L ADCA0.DR46.uint16_t[L]
#define ADCA0DR46H ADCA0.DR46.uint16_t[H]
#define ADCA0DR48 ADCA0.DR48.uint32_t
#define ADCA0DR48L ADCA0.DR48.uint16_t[L]
#define ADCA0DR48H ADCA0.DR48.uint16_t[H]
#define ADCA0PWDTSNDR ADCA0.PWDTSNDR.uint32_t
#define ADCA0PWDTSNDRH ADCA0.PWDTSNDR.uint16_t[H]
#define ADCA0DIR00 ADCA0.DIR00
#define ADCA0DIR01 ADCA0.DIR01
#define ADCA0DIR02 ADCA0.DIR02
#define ADCA0DIR03 ADCA0.DIR03
#define ADCA0DIR04 ADCA0.DIR04
#define ADCA0DIR05 ADCA0.DIR05
#define ADCA0DIR06 ADCA0.DIR06
#define ADCA0DIR07 ADCA0.DIR07
#define ADCA0DIR08 ADCA0.DIR08
#define ADCA0DIR09 ADCA0.DIR09
#define ADCA0DIR10 ADCA0.DIR10
#define ADCA0DIR11 ADCA0.DIR11
#define ADCA0DIR12 ADCA0.DIR12
#define ADCA0DIR13 ADCA0.DIR13
#define ADCA0DIR14 ADCA0.DIR14
#define ADCA0DIR15 ADCA0.DIR15
#define ADCA0DIR16 ADCA0.DIR16
#define ADCA0DIR17 ADCA0.DIR17
#define ADCA0DIR18 ADCA0.DIR18
#define ADCA0DIR19 ADCA0.DIR19
#define ADCA0DIR20 ADCA0.DIR20
#define ADCA0DIR21 ADCA0.DIR21
#define ADCA0DIR22 ADCA0.DIR22
#define ADCA0DIR23 ADCA0.DIR23
#define ADCA0DIR24 ADCA0.DIR24
#define ADCA0DIR25 ADCA0.DIR25
#define ADCA0DIR26 ADCA0.DIR26
#define ADCA0DIR27 ADCA0.DIR27
#define ADCA0DIR28 ADCA0.DIR28
#define ADCA0DIR29 ADCA0.DIR29
#define ADCA0DIR30 ADCA0.DIR30
#define ADCA0DIR31 ADCA0.DIR31
#define ADCA0DIR32 ADCA0.DIR32
#define ADCA0DIR33 ADCA0.DIR33
#define ADCA0DIR34 ADCA0.DIR34
#define ADCA0DIR35 ADCA0.DIR35
#define ADCA0DIR36 ADCA0.DIR36
#define ADCA0DIR37 ADCA0.DIR37
#define ADCA0DIR38 ADCA0.DIR38
#define ADCA0DIR39 ADCA0.DIR39
#define ADCA0DIR40 ADCA0.DIR40
#define ADCA0DIR41 ADCA0.DIR41
#define ADCA0DIR42 ADCA0.DIR42
#define ADCA0DIR43 ADCA0.DIR43
#define ADCA0DIR44 ADCA0.DIR44
#define ADCA0DIR45 ADCA0.DIR45
#define ADCA0DIR46 ADCA0.DIR46
#define ADCA0DIR47 ADCA0.DIR47
#define ADCA0DIR48 ADCA0.DIR48
#define ADCA0DIR49 ADCA0.DIR49
#define ADCA0PWDDIR ADCA0.PWDDIR
#define ADCA0ADHALTR ADCA0.ADHALTR.uint32_t
#define ADCA0ADHALTRL ADCA0.ADHALTR.uint16_t[L]
#define ADCA0ADHALTRLL ADCA0.ADHALTR.uint8_t[LL]
#define ADCA0ADCR ADCA0.ADCR.uint32_t
#define ADCA0ADCRL ADCA0.ADCR.uint16_t[L]
#define ADCA0ADCRLL ADCA0.ADCR.uint8_t[LL]
#define ADCA0SGSTR ADCA0.SGSTR.uint32_t
#define ADCA0SGSTRL ADCA0.SGSTR.uint16_t[L]
#define ADCA0MPXCURR ADCA0.MPXCURR.uint32_t
#define ADCA0MPXCURRL ADCA0.MPXCURR.uint16_t[L]
#define ADCA0MPXCURRLL ADCA0.MPXCURR.uint8_t[LL]
#define ADCA0THSMPSTCR ADCA0.THSMPSTCR.uint32_t
#define ADCA0THSMPSTCRL ADCA0.THSMPSTCR.uint16_t[L]
#define ADCA0THSMPSTCRLL ADCA0.THSMPSTCR.uint8_t[LL]
#define ADCA0THCR ADCA0.THCR.uint32_t
#define ADCA0THCRL ADCA0.THCR.uint16_t[L]
#define ADCA0THCRLL ADCA0.THCR.uint8_t[LL]
#define ADCA0THAHLDSTCR ADCA0.THAHLDSTCR.uint32_t
#define ADCA0THAHLDSTCRL ADCA0.THAHLDSTCR.uint16_t[L]
#define ADCA0THAHLDSTCRLL ADCA0.THAHLDSTCR.uint8_t[LL]
#define ADCA0THBHLDSTCR ADCA0.THBHLDSTCR.uint32_t
#define ADCA0THBHLDSTCRL ADCA0.THBHLDSTCR.uint16_t[L]
#define ADCA0THBHLDSTCRLL ADCA0.THBHLDSTCR.uint8_t[LL]
#define ADCA0THACR ADCA0.THACR.uint32_t
#define ADCA0THACRL ADCA0.THACR.uint16_t[L]
#define ADCA0THACRLL ADCA0.THACR.uint8_t[LL]
#define ADCA0THBCR ADCA0.THBCR.uint32_t
#define ADCA0THBCRL ADCA0.THBCR.uint16_t[L]
#define ADCA0THBCRLL ADCA0.THBCR.uint8_t[LL]
#define ADCA0THER ADCA0.THER.uint32_t
#define ADCA0THERL ADCA0.THER.uint16_t[L]
#define ADCA0THERLL ADCA0.THER.uint8_t[LL]
#define ADCA0THGSR ADCA0.THGSR.uint32_t
#define ADCA0THGSRL ADCA0.THGSR.uint16_t[L]
#define ADCA0THGSRLL ADCA0.THGSR.uint8_t[LL]
#define ADCA0SFTCR ADCA0.SFTCR.uint32_t
#define ADCA0SFTCRL ADCA0.SFTCR.uint16_t[L]
#define ADCA0SFTCRLL ADCA0.SFTCR.uint8_t[LL]
#define ADCA0ULLMTBR0 ADCA0.ULLMTBR0.uint32_t
#define ADCA0ULLMTBR0L ADCA0.ULLMTBR0.uint16_t[L]
#define ADCA0ULLMTBR0H ADCA0.ULLMTBR0.uint16_t[H]
#define ADCA0ULLMTBR1 ADCA0.ULLMTBR1.uint32_t
#define ADCA0ULLMTBR1L ADCA0.ULLMTBR1.uint16_t[L]
#define ADCA0ULLMTBR1H ADCA0.ULLMTBR1.uint16_t[H]
#define ADCA0ULLMTBR2 ADCA0.ULLMTBR2.uint32_t
#define ADCA0ULLMTBR2L ADCA0.ULLMTBR2.uint16_t[L]
#define ADCA0ULLMTBR2H ADCA0.ULLMTBR2.uint16_t[H]
#define ADCA0ECR ADCA0.ECR.uint32_t
#define ADCA0ECRL ADCA0.ECR.uint16_t[L]
#define ADCA0ECRLL ADCA0.ECR.uint8_t[LL]
#define ADCA0ULER ADCA0.ULER.uint32_t
#define ADCA0ULERL ADCA0.ULER.uint16_t[L]
#define ADCA0ULERLL ADCA0.ULER.uint8_t[LL]
#define ADCA0ULERLH ADCA0.ULER.uint8_t[LH]
#define ADCA0OWER ADCA0.OWER.uint32_t
#define ADCA0OWERL ADCA0.OWER.uint16_t[L]
#define ADCA0OWERLL ADCA0.OWER.uint8_t[LL]
#define ADCA0DGCTL0 ADCA0.DGCTL0.uint32_t
#define ADCA0DGCTL0L ADCA0.DGCTL0.uint16_t[L]
#define ADCA0DGCTL0LL ADCA0.DGCTL0.uint8_t[LL]
#define ADCA0DGCTL1 ADCA0.DGCTL1.uint32_t
#define ADCA0DGCTL1L ADCA0.DGCTL1.uint16_t[L]
#define ADCA0PDCTL1 ADCA0.PDCTL1.uint32_t
#define ADCA0PDCTL1L ADCA0.PDCTL1.uint16_t[L]
#define ADCA0PDCTL2 ADCA0.PDCTL2.uint32_t
#define ADCA0PDCTL2L ADCA0.PDCTL2.uint16_t[L]
#define ADCA0PDCTL2LL ADCA0.PDCTL2.uint8_t[LL]
#define ADCA0PDCTL2LH ADCA0.PDCTL2.uint8_t[LH]
#define ADCA0PDCTL2H ADCA0.PDCTL2.uint16_t[H]
#define ADCA0PDCTL2HL ADCA0.PDCTL2.uint8_t[HL]
#define ADCA0SMPCR ADCA0.SMPCR.uint32_t
#define ADCA0SMPCRL ADCA0.SMPCR.uint16_t[L]
#define ADCA0SMPCRLL ADCA0.SMPCR.uint8_t[LL]
#define ADCA0EMU ADCA0.EMU
#define ADCA0SGSTCR1 ADCA0.SGSTCR1.uint32_t
#define ADCA0SGSTCR1L ADCA0.SGSTCR1.uint16_t[L]
#define ADCA0SGSTCR1LL ADCA0.SGSTCR1.uint8_t[LL]
#define ADCA0SGCR1 ADCA0.SGCR1.uint32_t
#define ADCA0SGCR1L ADCA0.SGCR1.uint16_t[L]
#define ADCA0SGCR1LL ADCA0.SGCR1.uint8_t[LL]
#define ADCA0SGVCSP1 ADCA0.SGVCSP1.uint32_t
#define ADCA0SGVCSP1L ADCA0.SGVCSP1.uint16_t[L]
#define ADCA0SGVCSP1LL ADCA0.SGVCSP1.uint8_t[LL]
#define ADCA0SGVCEP1 ADCA0.SGVCEP1.uint32_t
#define ADCA0SGVCEP1L ADCA0.SGVCEP1.uint16_t[L]
#define ADCA0SGVCEP1LL ADCA0.SGVCEP1.uint8_t[LL]
#define ADCA0SGMCYCR1 ADCA0.SGMCYCR1.uint32_t
#define ADCA0SGMCYCR1L ADCA0.SGMCYCR1.uint16_t[L]
#define ADCA0SGMCYCR1LL ADCA0.SGMCYCR1.uint8_t[LL]
#define ADCA0SGSEFCR1 ADCA0.SGSEFCR1.uint32_t
#define ADCA0SGSEFCR1L ADCA0.SGSEFCR1.uint16_t[L]
#define ADCA0SGSEFCR1LL ADCA0.SGSEFCR1.uint8_t[LL]
#define ADCA0SGTSEL1 ADCA0.SGTSEL1.uint32_t
#define ADCA0SGTSEL1L ADCA0.SGTSEL1.uint16_t[L]
#define ADCA0SGSTCR2 ADCA0.SGSTCR2.uint32_t
#define ADCA0SGSTCR2L ADCA0.SGSTCR2.uint16_t[L]
#define ADCA0SGSTCR2LL ADCA0.SGSTCR2.uint8_t[LL]
#define ADCA0SGCR2 ADCA0.SGCR2.uint32_t
#define ADCA0SGCR2L ADCA0.SGCR2.uint16_t[L]
#define ADCA0SGCR2LL ADCA0.SGCR2.uint8_t[LL]
#define ADCA0SGVCSP2 ADCA0.SGVCSP2.uint32_t
#define ADCA0SGVCSP2L ADCA0.SGVCSP2.uint16_t[L]
#define ADCA0SGVCSP2LL ADCA0.SGVCSP2.uint8_t[LL]
#define ADCA0SGVCEP2 ADCA0.SGVCEP2.uint32_t
#define ADCA0SGVCEP2L ADCA0.SGVCEP2.uint16_t[L]
#define ADCA0SGVCEP2LL ADCA0.SGVCEP2.uint8_t[LL]
#define ADCA0SGMCYCR2 ADCA0.SGMCYCR2.uint32_t
#define ADCA0SGMCYCR2L ADCA0.SGMCYCR2.uint16_t[L]
#define ADCA0SGMCYCR2LL ADCA0.SGMCYCR2.uint8_t[LL]
#define ADCA0SGSEFCR2 ADCA0.SGSEFCR2.uint32_t
#define ADCA0SGSEFCR2L ADCA0.SGSEFCR2.uint16_t[L]
#define ADCA0SGSEFCR2LL ADCA0.SGSEFCR2.uint8_t[LL]
#define ADCA0SGTSEL2 ADCA0.SGTSEL2.uint32_t
#define ADCA0SGTSEL2L ADCA0.SGTSEL2.uint16_t[L]
#define ADCA0SGSTCR3 ADCA0.SGSTCR3.uint32_t
#define ADCA0SGSTCR3L ADCA0.SGSTCR3.uint16_t[L]
#define ADCA0SGSTCR3LL ADCA0.SGSTCR3.uint8_t[LL]
#define ADCA0SGCR3 ADCA0.SGCR3.uint32_t
#define ADCA0SGCR3L ADCA0.SGCR3.uint16_t[L]
#define ADCA0SGCR3LL ADCA0.SGCR3.uint8_t[LL]
#define ADCA0SGVCSP3 ADCA0.SGVCSP3.uint32_t
#define ADCA0SGVCSP3L ADCA0.SGVCSP3.uint16_t[L]
#define ADCA0SGVCSP3LL ADCA0.SGVCSP3.uint8_t[LL]
#define ADCA0SGVCEP3 ADCA0.SGVCEP3.uint32_t
#define ADCA0SGVCEP3L ADCA0.SGVCEP3.uint16_t[L]
#define ADCA0SGVCEP3LL ADCA0.SGVCEP3.uint8_t[LL]
#define ADCA0SGMCYCR3 ADCA0.SGMCYCR3.uint32_t
#define ADCA0SGMCYCR3L ADCA0.SGMCYCR3.uint16_t[L]
#define ADCA0SGMCYCR3LL ADCA0.SGMCYCR3.uint8_t[LL]
#define ADCA0SGSEFCR3 ADCA0.SGSEFCR3.uint32_t
#define ADCA0SGSEFCR3L ADCA0.SGSEFCR3.uint16_t[L]
#define ADCA0SGSEFCR3LL ADCA0.SGSEFCR3.uint8_t[LL]
#define ADCA0SGTSEL3 ADCA0.SGTSEL3.uint32_t
#define ADCA0SGTSEL3L ADCA0.SGTSEL3.uint16_t[L]
#define ADCA0PWDSGCR ADCA0.PWDSGCR.uint32_t
#define ADCA0PWDSGCRL ADCA0.PWDSGCR.uint16_t[L]
#define ADCA0PWDSGCRLL ADCA0.PWDSGCR.uint8_t[LL]
#define ADCA0PWDSGSEFCR ADCA0.PWDSGSEFCR.uint32_t
#define ADCA0PWDSGSEFCRL ADCA0.PWDSGSEFCR.uint16_t[L]
#define ADCA0PWDSGSEFCRLL ADCA0.PWDSGSEFCR.uint8_t[LL]
#define ADCA1VCR00 ADCA1.VCR00.uint32_t
#define ADCA1VCR00L ADCA1.VCR00.uint16_t[L]
#define ADCA1VCR00LL ADCA1.VCR00.uint8_t[LL]
#define ADCA1VCR00LH ADCA1.VCR00.uint8_t[LH]
#define ADCA1VCR01 ADCA1.VCR01.uint32_t
#define ADCA1VCR01L ADCA1.VCR01.uint16_t[L]
#define ADCA1VCR01LL ADCA1.VCR01.uint8_t[LL]
#define ADCA1VCR01LH ADCA1.VCR01.uint8_t[LH]
#define ADCA1VCR02 ADCA1.VCR02.uint32_t
#define ADCA1VCR02L ADCA1.VCR02.uint16_t[L]
#define ADCA1VCR02LL ADCA1.VCR02.uint8_t[LL]
#define ADCA1VCR02LH ADCA1.VCR02.uint8_t[LH]
#define ADCA1VCR03 ADCA1.VCR03.uint32_t
#define ADCA1VCR03L ADCA1.VCR03.uint16_t[L]
#define ADCA1VCR03LL ADCA1.VCR03.uint8_t[LL]
#define ADCA1VCR03LH ADCA1.VCR03.uint8_t[LH]
#define ADCA1VCR04 ADCA1.VCR04.uint32_t
#define ADCA1VCR04L ADCA1.VCR04.uint16_t[L]
#define ADCA1VCR04LL ADCA1.VCR04.uint8_t[LL]
#define ADCA1VCR04LH ADCA1.VCR04.uint8_t[LH]
#define ADCA1VCR05 ADCA1.VCR05.uint32_t
#define ADCA1VCR05L ADCA1.VCR05.uint16_t[L]
#define ADCA1VCR05LL ADCA1.VCR05.uint8_t[LL]
#define ADCA1VCR05LH ADCA1.VCR05.uint8_t[LH]
#define ADCA1VCR06 ADCA1.VCR06.uint32_t
#define ADCA1VCR06L ADCA1.VCR06.uint16_t[L]
#define ADCA1VCR06LL ADCA1.VCR06.uint8_t[LL]
#define ADCA1VCR06LH ADCA1.VCR06.uint8_t[LH]
#define ADCA1VCR07 ADCA1.VCR07.uint32_t
#define ADCA1VCR07L ADCA1.VCR07.uint16_t[L]
#define ADCA1VCR07LL ADCA1.VCR07.uint8_t[LL]
#define ADCA1VCR07LH ADCA1.VCR07.uint8_t[LH]
#define ADCA1VCR08 ADCA1.VCR08.uint32_t
#define ADCA1VCR08L ADCA1.VCR08.uint16_t[L]
#define ADCA1VCR08LL ADCA1.VCR08.uint8_t[LL]
#define ADCA1VCR08LH ADCA1.VCR08.uint8_t[LH]
#define ADCA1VCR09 ADCA1.VCR09.uint32_t
#define ADCA1VCR09L ADCA1.VCR09.uint16_t[L]
#define ADCA1VCR09LL ADCA1.VCR09.uint8_t[LL]
#define ADCA1VCR09LH ADCA1.VCR09.uint8_t[LH]
#define ADCA1VCR10 ADCA1.VCR10.uint32_t
#define ADCA1VCR10L ADCA1.VCR10.uint16_t[L]
#define ADCA1VCR10LL ADCA1.VCR10.uint8_t[LL]
#define ADCA1VCR10LH ADCA1.VCR10.uint8_t[LH]
#define ADCA1VCR11 ADCA1.VCR11.uint32_t
#define ADCA1VCR11L ADCA1.VCR11.uint16_t[L]
#define ADCA1VCR11LL ADCA1.VCR11.uint8_t[LL]
#define ADCA1VCR11LH ADCA1.VCR11.uint8_t[LH]
#define ADCA1VCR12 ADCA1.VCR12.uint32_t
#define ADCA1VCR12L ADCA1.VCR12.uint16_t[L]
#define ADCA1VCR12LL ADCA1.VCR12.uint8_t[LL]
#define ADCA1VCR12LH ADCA1.VCR12.uint8_t[LH]
#define ADCA1VCR13 ADCA1.VCR13.uint32_t
#define ADCA1VCR13L ADCA1.VCR13.uint16_t[L]
#define ADCA1VCR13LL ADCA1.VCR13.uint8_t[LL]
#define ADCA1VCR13LH ADCA1.VCR13.uint8_t[LH]
#define ADCA1VCR14 ADCA1.VCR14.uint32_t
#define ADCA1VCR14L ADCA1.VCR14.uint16_t[L]
#define ADCA1VCR14LL ADCA1.VCR14.uint8_t[LL]
#define ADCA1VCR14LH ADCA1.VCR14.uint8_t[LH]
#define ADCA1VCR15 ADCA1.VCR15.uint32_t
#define ADCA1VCR15L ADCA1.VCR15.uint16_t[L]
#define ADCA1VCR15LL ADCA1.VCR15.uint8_t[LL]
#define ADCA1VCR15LH ADCA1.VCR15.uint8_t[LH]
#define ADCA1VCR16 ADCA1.VCR16.uint32_t
#define ADCA1VCR16L ADCA1.VCR16.uint16_t[L]
#define ADCA1VCR16LL ADCA1.VCR16.uint8_t[LL]
#define ADCA1VCR16LH ADCA1.VCR16.uint8_t[LH]
#define ADCA1VCR17 ADCA1.VCR17.uint32_t
#define ADCA1VCR17L ADCA1.VCR17.uint16_t[L]
#define ADCA1VCR17LL ADCA1.VCR17.uint8_t[LL]
#define ADCA1VCR17LH ADCA1.VCR17.uint8_t[LH]
#define ADCA1VCR18 ADCA1.VCR18.uint32_t
#define ADCA1VCR18L ADCA1.VCR18.uint16_t[L]
#define ADCA1VCR18LL ADCA1.VCR18.uint8_t[LL]
#define ADCA1VCR18LH ADCA1.VCR18.uint8_t[LH]
#define ADCA1VCR19 ADCA1.VCR19.uint32_t
#define ADCA1VCR19L ADCA1.VCR19.uint16_t[L]
#define ADCA1VCR19LL ADCA1.VCR19.uint8_t[LL]
#define ADCA1VCR19LH ADCA1.VCR19.uint8_t[LH]
#define ADCA1VCR20 ADCA1.VCR20.uint32_t
#define ADCA1VCR20L ADCA1.VCR20.uint16_t[L]
#define ADCA1VCR20LL ADCA1.VCR20.uint8_t[LL]
#define ADCA1VCR20LH ADCA1.VCR20.uint8_t[LH]
#define ADCA1VCR21 ADCA1.VCR21.uint32_t
#define ADCA1VCR21L ADCA1.VCR21.uint16_t[L]
#define ADCA1VCR21LL ADCA1.VCR21.uint8_t[LL]
#define ADCA1VCR21LH ADCA1.VCR21.uint8_t[LH]
#define ADCA1VCR22 ADCA1.VCR22.uint32_t
#define ADCA1VCR22L ADCA1.VCR22.uint16_t[L]
#define ADCA1VCR22LL ADCA1.VCR22.uint8_t[LL]
#define ADCA1VCR22LH ADCA1.VCR22.uint8_t[LH]
#define ADCA1VCR23 ADCA1.VCR23.uint32_t
#define ADCA1VCR23L ADCA1.VCR23.uint16_t[L]
#define ADCA1VCR23LL ADCA1.VCR23.uint8_t[LL]
#define ADCA1VCR23LH ADCA1.VCR23.uint8_t[LH]
#define ADCA1PWDVCR ADCA1.PWDVCR.uint32_t
#define ADCA1PWDVCRL ADCA1.PWDVCR.uint16_t[L]
#define ADCA1PWDVCRLL ADCA1.PWDVCR.uint8_t[LL]
#define ADCA1PWDVCRLH ADCA1.PWDVCR.uint8_t[LH]
#define ADCA1DR00 ADCA1.DR00.uint32_t
#define ADCA1DR00L ADCA1.DR00.uint16_t[L]
#define ADCA1DR00H ADCA1.DR00.uint16_t[H]
#define ADCA1DR02 ADCA1.DR02.uint32_t
#define ADCA1DR02L ADCA1.DR02.uint16_t[L]
#define ADCA1DR02H ADCA1.DR02.uint16_t[H]
#define ADCA1DR04 ADCA1.DR04.uint32_t
#define ADCA1DR04L ADCA1.DR04.uint16_t[L]
#define ADCA1DR04H ADCA1.DR04.uint16_t[H]
#define ADCA1DR06 ADCA1.DR06.uint32_t
#define ADCA1DR06L ADCA1.DR06.uint16_t[L]
#define ADCA1DR06H ADCA1.DR06.uint16_t[H]
#define ADCA1DR08 ADCA1.DR08.uint32_t
#define ADCA1DR08L ADCA1.DR08.uint16_t[L]
#define ADCA1DR08H ADCA1.DR08.uint16_t[H]
#define ADCA1DR10 ADCA1.DR10.uint32_t
#define ADCA1DR10L ADCA1.DR10.uint16_t[L]
#define ADCA1DR10H ADCA1.DR10.uint16_t[H]
#define ADCA1DR12 ADCA1.DR12.uint32_t
#define ADCA1DR12L ADCA1.DR12.uint16_t[L]
#define ADCA1DR12H ADCA1.DR12.uint16_t[H]
#define ADCA1DR14 ADCA1.DR14.uint32_t
#define ADCA1DR14L ADCA1.DR14.uint16_t[L]
#define ADCA1DR14H ADCA1.DR14.uint16_t[H]
#define ADCA1DR16 ADCA1.DR16.uint32_t
#define ADCA1DR16L ADCA1.DR16.uint16_t[L]
#define ADCA1DR16H ADCA1.DR16.uint16_t[H]
#define ADCA1DR18 ADCA1.DR18.uint32_t
#define ADCA1DR18L ADCA1.DR18.uint16_t[L]
#define ADCA1DR18H ADCA1.DR18.uint16_t[H]
#define ADCA1DR20 ADCA1.DR20.uint32_t
#define ADCA1DR20L ADCA1.DR20.uint16_t[L]
#define ADCA1DR20H ADCA1.DR20.uint16_t[H]
#define ADCA1DR22 ADCA1.DR22.uint32_t
#define ADCA1DR22L ADCA1.DR22.uint16_t[L]
#define ADCA1DR22H ADCA1.DR22.uint16_t[H]
#define ADCA1PWDTSNDR ADCA1.PWDTSNDR.uint32_t
#define ADCA1PWDTSNDRH ADCA1.PWDTSNDR.uint16_t[H]
#define ADCA1DIR00 ADCA1.DIR00
#define ADCA1DIR01 ADCA1.DIR01
#define ADCA1DIR02 ADCA1.DIR02
#define ADCA1DIR03 ADCA1.DIR03
#define ADCA1DIR04 ADCA1.DIR04
#define ADCA1DIR05 ADCA1.DIR05
#define ADCA1DIR06 ADCA1.DIR06
#define ADCA1DIR07 ADCA1.DIR07
#define ADCA1DIR08 ADCA1.DIR08
#define ADCA1DIR09 ADCA1.DIR09
#define ADCA1DIR10 ADCA1.DIR10
#define ADCA1DIR11 ADCA1.DIR11
#define ADCA1DIR12 ADCA1.DIR12
#define ADCA1DIR13 ADCA1.DIR13
#define ADCA1DIR14 ADCA1.DIR14
#define ADCA1DIR15 ADCA1.DIR15
#define ADCA1DIR16 ADCA1.DIR16
#define ADCA1DIR17 ADCA1.DIR17
#define ADCA1DIR18 ADCA1.DIR18
#define ADCA1DIR19 ADCA1.DIR19
#define ADCA1DIR20 ADCA1.DIR20
#define ADCA1DIR21 ADCA1.DIR21
#define ADCA1DIR22 ADCA1.DIR22
#define ADCA1DIR23 ADCA1.DIR23
#define ADCA1PWDDIR ADCA1.PWDDIR
#define ADCA1ADHALTR ADCA1.ADHALTR.uint32_t
#define ADCA1ADHALTRL ADCA1.ADHALTR.uint16_t[L]
#define ADCA1ADHALTRLL ADCA1.ADHALTR.uint8_t[LL]
#define ADCA1ADCR ADCA1.ADCR.uint32_t
#define ADCA1ADCRL ADCA1.ADCR.uint16_t[L]
#define ADCA1ADCRLL ADCA1.ADCR.uint8_t[LL]
#define ADCA1SGSTR ADCA1.SGSTR.uint32_t
#define ADCA1SGSTRL ADCA1.SGSTR.uint16_t[L]
#define ADCA1SFTCR ADCA1.SFTCR.uint32_t
#define ADCA1SFTCRL ADCA1.SFTCR.uint16_t[L]
#define ADCA1SFTCRLL ADCA1.SFTCR.uint8_t[LL]
#define ADCA1ULLMTBR0 ADCA1.ULLMTBR0.uint32_t
#define ADCA1ULLMTBR0L ADCA1.ULLMTBR0.uint16_t[L]
#define ADCA1ULLMTBR0H ADCA1.ULLMTBR0.uint16_t[H]
#define ADCA1ULLMTBR1 ADCA1.ULLMTBR1.uint32_t
#define ADCA1ULLMTBR1L ADCA1.ULLMTBR1.uint16_t[L]
#define ADCA1ULLMTBR1H ADCA1.ULLMTBR1.uint16_t[H]
#define ADCA1ULLMTBR2 ADCA1.ULLMTBR2.uint32_t
#define ADCA1ULLMTBR2L ADCA1.ULLMTBR2.uint16_t[L]
#define ADCA1ULLMTBR2H ADCA1.ULLMTBR2.uint16_t[H]
#define ADCA1ECR ADCA1.ECR.uint32_t
#define ADCA1ECRL ADCA1.ECR.uint16_t[L]
#define ADCA1ECRLL ADCA1.ECR.uint8_t[LL]
#define ADCA1ULER ADCA1.ULER.uint32_t
#define ADCA1ULERL ADCA1.ULER.uint16_t[L]
#define ADCA1ULERLL ADCA1.ULER.uint8_t[LL]
#define ADCA1ULERLH ADCA1.ULER.uint8_t[LH]
#define ADCA1OWER ADCA1.OWER.uint32_t
#define ADCA1OWERL ADCA1.OWER.uint16_t[L]
#define ADCA1OWERLL ADCA1.OWER.uint8_t[LL]
#define ADCA1DGCTL0 ADCA1.DGCTL0.uint32_t
#define ADCA1DGCTL0L ADCA1.DGCTL0.uint16_t[L]
#define ADCA1DGCTL0LL ADCA1.DGCTL0.uint8_t[LL]
#define ADCA1DGCTL1 ADCA1.DGCTL1.uint32_t
#define ADCA1DGCTL1L ADCA1.DGCTL1.uint16_t[L]
#define ADCA1PDCTL1 ADCA1.PDCTL1.uint32_t
#define ADCA1PDCTL1L ADCA1.PDCTL1.uint16_t[L]
#define ADCA1PDCTL2 ADCA1.PDCTL2.uint32_t
#define ADCA1PDCTL2L ADCA1.PDCTL2.uint16_t[L]
#define ADCA1PDCTL2LL ADCA1.PDCTL2.uint8_t[LL]
#define ADCA1SMPCR ADCA1.SMPCR.uint32_t
#define ADCA1SMPCRL ADCA1.SMPCR.uint16_t[L]
#define ADCA1SMPCRLL ADCA1.SMPCR.uint8_t[LL]
#define ADCA1EMU ADCA1.EMU
#define ADCA1SGSTCR1 ADCA1.SGSTCR1.uint32_t
#define ADCA1SGSTCR1L ADCA1.SGSTCR1.uint16_t[L]
#define ADCA1SGSTCR1LL ADCA1.SGSTCR1.uint8_t[LL]
#define ADCA1SGCR1 ADCA1.SGCR1.uint32_t
#define ADCA1SGCR1L ADCA1.SGCR1.uint16_t[L]
#define ADCA1SGCR1LL ADCA1.SGCR1.uint8_t[LL]
#define ADCA1SGVCSP1 ADCA1.SGVCSP1.uint32_t
#define ADCA1SGVCSP1L ADCA1.SGVCSP1.uint16_t[L]
#define ADCA1SGVCSP1LL ADCA1.SGVCSP1.uint8_t[LL]
#define ADCA1SGVCEP1 ADCA1.SGVCEP1.uint32_t
#define ADCA1SGVCEP1L ADCA1.SGVCEP1.uint16_t[L]
#define ADCA1SGVCEP1LL ADCA1.SGVCEP1.uint8_t[LL]
#define ADCA1SGMCYCR1 ADCA1.SGMCYCR1.uint32_t
#define ADCA1SGMCYCR1L ADCA1.SGMCYCR1.uint16_t[L]
#define ADCA1SGMCYCR1LL ADCA1.SGMCYCR1.uint8_t[LL]
#define ADCA1SGSEFCR1 ADCA1.SGSEFCR1.uint32_t
#define ADCA1SGSEFCR1L ADCA1.SGSEFCR1.uint16_t[L]
#define ADCA1SGSEFCR1LL ADCA1.SGSEFCR1.uint8_t[LL]
#define ADCA1SGTSEL1 ADCA1.SGTSEL1.uint32_t
#define ADCA1SGTSEL1L ADCA1.SGTSEL1.uint16_t[L]
#define ADCA1SGSTCR2 ADCA1.SGSTCR2.uint32_t
#define ADCA1SGSTCR2L ADCA1.SGSTCR2.uint16_t[L]
#define ADCA1SGSTCR2LL ADCA1.SGSTCR2.uint8_t[LL]
#define ADCA1SGCR2 ADCA1.SGCR2.uint32_t
#define ADCA1SGCR2L ADCA1.SGCR2.uint16_t[L]
#define ADCA1SGCR2LL ADCA1.SGCR2.uint8_t[LL]
#define ADCA1SGVCSP2 ADCA1.SGVCSP2.uint32_t
#define ADCA1SGVCSP2L ADCA1.SGVCSP2.uint16_t[L]
#define ADCA1SGVCSP2LL ADCA1.SGVCSP2.uint8_t[LL]
#define ADCA1SGVCEP2 ADCA1.SGVCEP2.uint32_t
#define ADCA1SGVCEP2L ADCA1.SGVCEP2.uint16_t[L]
#define ADCA1SGVCEP2LL ADCA1.SGVCEP2.uint8_t[LL]
#define ADCA1SGMCYCR2 ADCA1.SGMCYCR2.uint32_t
#define ADCA1SGMCYCR2L ADCA1.SGMCYCR2.uint16_t[L]
#define ADCA1SGMCYCR2LL ADCA1.SGMCYCR2.uint8_t[LL]
#define ADCA1SGSEFCR2 ADCA1.SGSEFCR2.uint32_t
#define ADCA1SGSEFCR2L ADCA1.SGSEFCR2.uint16_t[L]
#define ADCA1SGSEFCR2LL ADCA1.SGSEFCR2.uint8_t[LL]
#define ADCA1SGTSEL2 ADCA1.SGTSEL2.uint32_t
#define ADCA1SGTSEL2L ADCA1.SGTSEL2.uint16_t[L]
#define ADCA1SGSTCR3 ADCA1.SGSTCR3.uint32_t
#define ADCA1SGSTCR3L ADCA1.SGSTCR3.uint16_t[L]
#define ADCA1SGSTCR3LL ADCA1.SGSTCR3.uint8_t[LL]
#define ADCA1SGCR3 ADCA1.SGCR3.uint32_t
#define ADCA1SGCR3L ADCA1.SGCR3.uint16_t[L]
#define ADCA1SGCR3LL ADCA1.SGCR3.uint8_t[LL]
#define ADCA1SGVCSP3 ADCA1.SGVCSP3.uint32_t
#define ADCA1SGVCSP3L ADCA1.SGVCSP3.uint16_t[L]
#define ADCA1SGVCSP3LL ADCA1.SGVCSP3.uint8_t[LL]
#define ADCA1SGVCEP3 ADCA1.SGVCEP3.uint32_t
#define ADCA1SGVCEP3L ADCA1.SGVCEP3.uint16_t[L]
#define ADCA1SGVCEP3LL ADCA1.SGVCEP3.uint8_t[LL]
#define ADCA1SGMCYCR3 ADCA1.SGMCYCR3.uint32_t
#define ADCA1SGMCYCR3L ADCA1.SGMCYCR3.uint16_t[L]
#define ADCA1SGMCYCR3LL ADCA1.SGMCYCR3.uint8_t[LL]
#define ADCA1SGSEFCR3 ADCA1.SGSEFCR3.uint32_t
#define ADCA1SGSEFCR3L ADCA1.SGSEFCR3.uint16_t[L]
#define ADCA1SGSEFCR3LL ADCA1.SGSEFCR3.uint8_t[LL]
#define ADCA1SGTSEL3 ADCA1.SGTSEL3.uint32_t
#define ADCA1SGTSEL3L ADCA1.SGTSEL3.uint16_t[L]
#define ADCA1PWDSGCR ADCA1.PWDSGCR.uint32_t
#define ADCA1PWDSGCRL ADCA1.PWDSGCR.uint16_t[L]
#define ADCA1PWDSGCRLL ADCA1.PWDSGCR.uint8_t[LL]
#define ADCA1PWDSGSEFCR ADCA1.PWDSGSEFCR.uint32_t
#define ADCA1PWDSGSEFCRL ADCA1.PWDSGSEFCR.uint16_t[L]
#define ADCA1PWDSGSEFCRLL ADCA1.PWDSGSEFCR.uint8_t[LL]
#define DCRA0CIN DCRA0.CIN
#define DCRA0COUT DCRA0.COUT
#define DCRA0CTL DCRA0.CTL
#define DCRA1CIN DCRA1.CIN
#define DCRA1COUT DCRA1.COUT
#define DCRA1CTL DCRA1.CTL
#define DCRA2CIN DCRA2.CIN
#define DCRA2COUT DCRA2.COUT
#define DCRA2CTL DCRA2.CTL
#define DCRA3CIN DCRA3.CIN
#define DCRA3COUT DCRA3.COUT
#define DCRA3CTL DCRA3.CTL
#define KR0KRM KR0.KRM.uint8_t
#define KR0KRM0 KR0.KRM.KRM0
#define KR0KRM1 KR0.KRM.KRM1
#define KR0KRM2 KR0.KRM.KRM2
#define KR0KRM3 KR0.KRM.KRM3
#define KR0KRM4 KR0.KRM.KRM4
#define KR0KRM5 KR0.KRM.KRM5
#define KR0KRM6 KR0.KRM.KRM6
#define KR0KRM7 KR0.KRM.KRM7
#define CLMA0CTL0 CLMA0.CTL0
#define CLMA0CMPL CLMA0.CMPL
#define CLMA0CMPH CLMA0.CMPH
#define CLMA0PCMD CLMA0.PCMD
#define CLMA0PS CLMA0.PS
#define CLMA0EMU0 CLMA0.EMU0
#define CLMATEST CLMA.TEST
#define CLMATESTS CLMA.TESTS
#define CLMA1CTL0 CLMA1.CTL0
#define CLMA1CMPL CLMA1.CMPL
#define CLMA1CMPH CLMA1.CMPH
#define CLMA1PCMD CLMA1.PCMD
#define CLMA1PS CLMA1.PS
#define CLMA1EMU0 CLMA1.EMU0
#define CLMA2CTL0 CLMA2.CTL0
#define CLMA2CMPL CLMA2.CMPL
#define CLMA2CMPH CLMA2.CMPH
#define CLMA2PCMD CLMA2.PCMD
#define CLMA2PS CLMA2.PS
#define CLMA2EMU0 CLMA2.EMU0
#endif


