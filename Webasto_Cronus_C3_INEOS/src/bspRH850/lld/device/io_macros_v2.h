#include "hal_data_types.h"

#ifndef __IO_MACROS_H
  #define __IO_MACROS_H

  #if !defined(__STRICT_ANSI__ ) & defined(__ghs__)
    #ifdef __LANGUAGE_ASM__
    #else
     typedef unsigned char     u08_T;
     typedef unsigned short    u16_T;
     typedef unsigned long     u32_T;

     typedef struct bitf08 { 
        u08_T bit00:1;
        u08_T bit01:1;
        u08_T bit02:1;
        u08_T bit03:1;
        u08_T bit04:1;
        u08_T bit05:1;
        u08_T bit06:1;
        u08_T bit07:1;
     } bitf08_T;

     typedef struct bitf16 { 
        u16_T  bit00:1;
        u16_T  bit01:1;
        u16_T  bit02:1;
        u16_T  bit03:1;
        u16_T  bit04:1;
        u16_T  bit05:1;
        u16_T  bit06:1;
        u16_T  bit07:1;
        u16_T  bit08:1;
        u16_T  bit09:1;
        u16_T  bit10:1;
        u16_T  bit11:1;
        u16_T  bit12:1;
        u16_T  bit13:1;
        u16_T  bit14:1;
        u16_T  bit15:1;
     } bitf16_T;

     typedef struct bitf32 { 
        u32_T bit00:1;
        u32_T bit01:1;
        u32_T bit02:1;
        u32_T bit03:1;
        u32_T bit04:1;
        u32_T bit05:1;
        u32_T bit06:1;
        u32_T bit07:1;
        u32_T bit08:1;
        u32_T bit09:1;
        u32_T bit10:1;
        u32_T bit11:1;
        u32_T bit12:1;
        u32_T bit13:1;
        u32_T bit14:1;
        u32_T bit15:1;
        u32_T bit16:1;
        u32_T bit17:1;
        u32_T bit18:1;
        u32_T bit19:1;
        u32_T bit20:1;
        u32_T bit21:1;
        u32_T bit22:1;
        u32_T bit23:1;
        u32_T bit24:1;
        u32_T bit25:1;
        u32_T bit26:1;
        u32_T bit27:1;
        u32_T bit28:1;
        u32_T bit29:1;
        u32_T bit30:1;
        u32_T bit31:1;
     } bitf32_T;

     #define PRAGMA(x)         _Pragma(#x)

     #define __ZNEAR(type, x)  PRAGMA( ghs startzda)                \
         type    x;                                                 \
         PRAGMA( ghs endzda); 

     #define __SNEAR(type, x)  PRAGMA( ghs startsda)                \
         type    x;                                                 \
         PRAGMA( ghs endsda); 

     #define __FAR(type, x)    PRAGMA( ghs startdata)               \
         type    x;                                                 \
         PRAGMA( ghs enddata); 

     #define __READ            volatile const 
     #define __WRITE           volatile
     #define __READ_WRITE      volatile

     #ifndef __IOREG8
       #define __IOREG8(reg,addr,attrib)     PRAGMA( ghs io reg addr) \
       extern attrib u08_T reg; 
     #endif

     #ifndef __IOREG16
       #define __IOREG16(reg,addr,attrib)    PRAGMA( ghs io reg addr) \
       extern attrib u16_T reg; 
     #endif

     #ifndef __IOREG32
       #define __IOREG32(reg,addr,attrib)    PRAGMA( ghs io reg addr) \
       extern attrib u32_T reg; 
     #endif

     #ifndef __IOREG
       #define __IOREG(reg,addr,attrib,type) PRAGMA( ghs io reg addr) \
       extern attrib type reg; 
     #endif

     #ifndef __IOREGARRAY
       #define __IOREGARRAY(reg, array, addr, attrib, type) PRAGMA(ghs io reg addr) \
       extern attrib type reg[array];
     #endif

     #define _GHS_PRAGMA_IO_TYPEDEF_

    #endif 
  #else    
  typedef unsigned char     u08_T;
  typedef unsigned short    u16_T;
  typedef unsigned long     u32_T;

  typedef struct bitf08 { 
     int bit00:1;
     int bit01:1;
     int bit02:1;
     int bit03:1;
     int bit04:1;
     int bit05:1;
     int bit06:1;
     int bit07:1;
  } bitf08_T;

  #endif   
#endif 


