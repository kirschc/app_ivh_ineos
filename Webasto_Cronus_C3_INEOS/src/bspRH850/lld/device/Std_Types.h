
#ifndef STD_TYPES_H
#define STD_TYPES_H


#include "hal_data_types.h"


#define STDTYPES_MAJOR_VERSION  1u
#define STDTYPES_MINOR_VERSION  0u
#define STDTYPES_PATCH_VERSION  0u

#ifndef STATUSTYPEDEFINED
   #define STATUSTYPEDEFINED
   #define E_OK                  0x00u

   typedef unsigned char StatusType;
#endif

#define E_NOT_OK                 0x01u

#define E_CAN_OLD_MSG_DATA       0x02u

#define E_CAN_TIMEOUT            0x03u

#define STD_HIGH                 0x01u

#define STD_LOW                  0x00u

#define STD_ACTIVE               0x01u

#define STD_IDLE                 0x00u

#define STD_ON                   0x01u

#define STD_OFF                  0x00u


#define SIZEOF( Array )          (sizeof((Array)) / sizeof((Array)[0]))


typedef uint8_t Std_ReturnType;

typedef struct
{
   uint16_t vendorID;
   uint8_t moduleID;
   uint8_t sw_major_version;
   uint8_t sw_minor_version;
   uint8_t sw_patch_version;
}
Std_VersionInfoType;




#endif 


