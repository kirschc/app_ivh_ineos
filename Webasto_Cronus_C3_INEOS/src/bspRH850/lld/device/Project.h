
#ifndef PROJECT_H
#define PROJECT_H


#include "dr7f701057.dev.h"
#include "dr7f701057_irq.h"


#define PROJECT_SW_STATUS               "B"

#define ECUM_DEV_ERROR_DETECT
#define FEE_DEV_ERROR_DETECT

#define ADC_DEV_ERROR_DETECT
#define CAN_DEV_ERROR_DETECT
#define DIO_DEV_ERROR_DETECT
#define FLS_DEV_ERROR_DETECT
#define MCU_DEV_ERROR_DETECT
#define PORT_DEV_ERROR_DETECT
#define PWM_DEV_ERROR_DETECT
#define SPI_DEV_ERROR_DETECT
#define TAUB_DEV_ERROR_DETECT

#define ENABLE_ALL_INTERRUPTS()     __asm("ei")

#define DISABLE_ALL_INTERRUPTS()    __asm("di")

#define ENTER_HALT_MODE()           __asm("halt")







#else
    #error Multiple include of "Project.h"
#endif 


