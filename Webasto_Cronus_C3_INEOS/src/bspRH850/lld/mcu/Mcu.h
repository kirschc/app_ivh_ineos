
#ifndef MCU_H
#define MCU_H


#include "Std_Types.h"
#include "Mcu_Cfg.h"


#define MCU_SW_MAJOR_VERSION                       1u

#define MCU_SW_MINOR_VERSION                       0u

#define MCU_SW_PATCH_VERSION                       0u

#define MCU_MODULE_ID                              ((uint8_t) 101u)

#define MCU_INSTANCE_ID0                           ((uint16_t) 0x0000u)

#define MCU_VENDOR_ID                              ((uint16_t) 0u)

#define MCU_MODE_NORMAL                            0x00u

#define MCU_MODE_HALT                              0x01u

#define MCU_NUMBER_OF_MODES                        0x02u


#define Mcu_GetVersionInfo(                                 \
              \
   versioninfo)                                             \
   versioninfo.moduleID = MCU_MODULE_ID;                    \
   versioninfo.vendorID = MCU_VENDOR_ID;                    \
   versioninfo.sw_major_version = MCU_SW_MAJOR_VERSION;     \
   versioninfo.sw_minor_version = MCU_SW_MINOR_VERSION;     \
   versioninfo.sw_patch_version = MCU_SW_PATCH_VERSION;

#define MCU_SET_CLOCK_GEN_REG(                              \
                                     \
   ProtectReg,                                              \
                                        \
   ClockSelReg,                                             \
                              \
   ClockID)                                                 \
                 \
   ProtectReg = MCU_UNPROTECT_REGISTER_CODE;                \
   ClockSelReg =  (ClockID);                                \
   ClockSelReg = ~(ClockID);                                \
   ClockSelReg =  (ClockID);


typedef enum
{
   MCU_POWER_ON_RESET = 0u,
   MCU_WATCHDOG_RESET,
   MCU_SW_RESET,
   MCU_HIGH_SPEED_INT_OSC_MONITOR_RESET,
   MCU_MAIN_OSC_MONITOR_RESET,
   MCU_PLL_MONITOR_RESET,
   MCU_LOW_VOLTAGE_RESET,
   MCU_CORE_VOLTAGE_MONITOR_RESET,
   MCU_EXT_RESET,
   MCU_POWER_UP_RESET,
   MCU_DEEPSTOP_RESET,
   MCU_RESET_UNDEFINED
}
Mcu_ResetType;

typedef struct
{
   Mcu_ResetType Mcu_ResetSetting;
   uint16_t Mcu_WakeUpSources_IMR0;
   uint16_t Mcu_WakeUpSources_IMR1;
   uint16_t Mcu_WakeUpSources_IMR2;
   uint16_t Mcu_WakeUpSources_IMR3;
   uint16_t Mcu_WakeUpSources_IMR4;
   uint16_t Mcu_WakeUpSources_IMR5;
   uint16_t Mcu_WakeUpSources_IMR6;
   uint16_t Mcu_WakeUpSources_IMR7;
   uint16_t Mcu_WakeUpSources_IMR8;
   uint16_t Mcu_WakeUpSources_IMR9;
   uint16_t Mcu_WakeUpSources_IMR10;
   uint16_t Mcu_WakeUpSources_IMR11;
   uint16_t Mcu_WakeUpSources_IMR12;
   uint16_t Mcu_WakeUpSources_IMR13;
   uint16_t Mcu_WakeUpSources_IMR14;
   uint16_t Mcu_WakeUpSources_IMR15;
}
Mcu_ConfigType;

typedef enum
{
   MCU_PLL_LOCKED = 0u,
   MCU_PLL_UNLOCKED,
   MCU_PLL_STATUS_UNDEFINED
}
Mcu_PllStatusType;

typedef uint8_t Mcu_ClockType;

typedef uint16_t Mcu_RawResetType;

typedef struct
{
   boolean Mcu_DeepStop;
   boolean Mcu_PowerUp;
   boolean Mcu_ExtReset;
   boolean Mcu_CoreVoltageMonitorReset;
   boolean Mcu_LowVoltageReset;
   boolean Mcu_PLLMonitorReset;
   boolean Mcu_HighSpeedIntOscMonitorReset;
   boolean Mcu_MainOscMonitorReset;
   boolean Mcu_WDTA1Reset;
   boolean Mcu_WDTA0Reset;
   boolean Mcu_SoftwareReset;
}
Mcu_SupportedResetsType;

typedef uint8_t Mcu_ModeType;

typedef uint8_t Mcu_RamSectionType;


void Mcu_Init(
   const Mcu_ConfigType* ConfigPtr);

Std_ReturnType Mcu_InitRamSection(
   Mcu_RamSectionType RamSection);

Std_ReturnType Mcu_InitClock(
   Mcu_ClockType ClockSetting);

void Mcu_DistributePllClock(void);

Mcu_PllStatusType Mcu_GetPllStatus(void);

Mcu_ResetType Mcu_GetResetReason(void);

Mcu_RawResetType Mcu_GetResetRawValue(void);

void Mcu_PerformReset(void);

void Mcu_SetMode(
   Mcu_ModeType McuMode);


extern const Mcu_ConfigType Mcu_Config;

extern const Mcu_ClockType Mcu_ClockConfig;

extern const Mcu_RamSectionType Mcu_RamSectionConfig;


#else
    #error Multiple include of "Mcu.h"
#endif 


