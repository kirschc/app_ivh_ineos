

#ifndef FDL_DESCRIPTOR_H
#define FDL_DESCRIPTOR_H


    #define CPU_FREQUENCY_MHZ       1                             

    #define AUTHENTICATION_ID       { 0xFFFFFFFF, \
                                      0xFFFFFFFF, \
                                      0xFFFFFFFF, \
                                      0xFFFFFFFF }                  

    #define EEL_VIRTUALBLOCKSIZE    32u                     

    #define FDL_POOL_SIZE           (16u * EEL_VIRTUALBLOCKSIZE)    
    #define EEL_POOL_START          (1u * EEL_VIRTUALBLOCKSIZE)     
    #define EEL_POOL_SIZE           (6u * EEL_VIRTUALBLOCKSIZE)          



    extern const r_fdl_descriptor_t sampleApp_fdlConfig_enu;           


#endif  


