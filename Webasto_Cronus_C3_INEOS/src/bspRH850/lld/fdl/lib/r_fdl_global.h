#ifdef ENABLE_QAC_TEST
    #pragma PRQA_MESSAGES_OFF 0292
#endif


#ifdef ENABLE_QAC_TEST
    #pragma PRQA_MESSAGES_ON 0292
#endif


#ifndef R_FDL_GLOBAL_H
#define R_FDL_GLOBAL_H

#define R_FDL_COMP_GHS 1
#define R_FDL_COMP_IAR 2
#define R_FDL_COMP_REC 3

#if defined (__IAR_SYSTEMS_ASM__)
    #define R_FDL_COMPILER R_FDL_COMP_IAR
#elif defined (__IAR_SYSTEMS_ICC__)
    #define R_FDL_COMPILER R_FDL_COMP_IAR
#elif defined (__v850e3v5__)
    #define R_FDL_COMPILER R_FDL_COMP_REC
#else 
    #define R_FDL_COMPILER R_FDL_COMP_GHS
#endif


#include "fdl_cfg.h"
#include "hal_data_types.h"
#include "r_fdl.h"
#include "r_fdl_env.h"

#define R_FDL_VERSION_STRING                "DH850T01xxxxxxV103"

#define R_FDL_REQUEST_POINTER_UNDEFINED     (r_fdl_request_t *)(0x00000000uL)
#define R_FDL_DESCRIPTOR_POINTER_UNDEFINED  (r_fdl_descriptor_t *)(0x00000000uL)
#define R_FDL_NULL                          (0x00000000uL)


typedef enum R_FDL_FLAG_T
{
    R_FDL_FALSE,                                    
    R_FDL_TRUE                                      
} r_fdl_flag_t;

typedef enum R_FDL_INT_STATUS_T
{
    R_FDL_ISTAT_NOINIT          = 0x00000000uL,     
    R_FDL_ISTAT_NORMALOP        = 0x55555555uL,     
    R_FDL_ISTAT_SUSPEND_PR      = 0x55555556uL,     
    R_FDL_ISTAT_SUSPENDED       = 0x55555557uL,     
    R_FDL_ISTAT_STANDBY_PR      = 0x55555558uL,     
    R_FDL_ISTAT_STANDBY         = 0x55555559uL      
} r_fdl_int_status_t;

typedef void (* r_fdl_pFct_ExeInRAM)(uint32_t * param);

typedef struct R_FDL_MULTIOP_T
{
    uint32_t            flashAdd_u32;               
    uint32_t            bufAdd_u32;                 
    uint32_t            flashAddEnd_u32;            
    r_fdl_accessType_t  accessType_enu;             
} r_fdl_multiOp_t;

typedef struct R_FDL_DATA_T
{
    const r_fdl_descriptor_t *  RTCfg_pstr;         
    r_fdl_multiOp_t     mulOp_str;                  
    r_fdl_multiOp_t     spdMulOp_str;               
    r_fdl_request_t *   reqSuspend_pstr;            
    r_fdl_request_t *   reqInt_pstr;                
    uint32_t            opFailAddr_u32;             
    uint32_t            dfSize_u32;                 
    r_fdl_status_t      spdResStatus_enu;           
    r_fdl_flag_t        spdSpdRequest_enu;          
    r_fdl_flag_t        spdResRequest_enu;          
    r_fdl_int_status_t  stByIStatBackUp_enu;        
    r_fdl_int_status_t  iStat_enu;                  
} r_fdl_data_t;


#ifndef R_FDL_STATIC
    #define R_FDL_STATIC static
#endif


r_fdl_status_t  R_FDL_FCUFct_CheckFatalError (void);
r_fdl_flag_t    R_FDL_FCUFct_ChkReady (void);
r_fdl_flag_t    R_FDL_FCUFct_ChkSuspendable (void);
r_fdl_flag_t    R_FDL_FCUFct_ChkStartable (r_fdl_command_t cmd);
void            R_FDL_FCUFct_ClearStatus (void);
r_fdl_status_t  R_FDL_FCUFct_GetStat (void);
r_fdl_status_t  R_FDL_FCUFct_InitRAM (void);
r_fdl_status_t  R_FDL_FCUFct_ReadOperation (volatile uint32_t * pAddSrc, uint32_t addDest, uint32_t cnt);
r_fdl_status_t  R_FDL_FCUFct_Reset (void);
r_fdl_flag_t    R_FDL_FCUFct_ResumeChkNeed (void);
void            R_FDL_FCUFct_Resume (void);
r_fdl_status_t  R_FDL_FCUFct_SetFrequency (uint16_t fCpu);
uint32_t        R_FDL_FCUFct_GetDFSize (void);
r_fdl_status_t  R_FDL_FCUFct_StartBCEraseOperation (uint32_t addStart, uint32_t addEnd,
                                                    uint8_t fcuCmd,     r_fdl_accessType_t accType);
r_fdl_status_t  R_FDL_FCUFct_StartWriteOperation (uint32_t addSrc,
                                                  uint32_t addDest,
                                                  uint32_t cnt,
                                                  r_fdl_accessType_t accType);
void            R_FDL_FCUFct_Suspend (void);
r_fdl_status_t  R_FDL_FCUFct_SwitchMode (uint16_t mode);
void            R_FDL_FCUFct_VerifyID (const r_fdl_descriptor_t * descriptor_pstr);
r_fdl_status_t  R_FDL_IFct_ChkAccessBoundaries (uint32_t addStart, uint32_t bCnt,
                                                r_fdl_accessType_t accType, uint32_t granularity_u32);
void            R_FDL_IFct_WriteMemoryU08 (uint32_t add, uint8_t val);
void            R_FDL_IFct_CalcFOpUnitCnt_BC (uint32_t * cnt, uint32_t add);

    #define R_FDL_START_SEC_VAR
    #include "r_fdl_mem_map.h"

    #if R_FDL_COMPILER == R_FDL_COMP_GHS
        #define R_FDL_NOINIT
    #elif R_FDL_COMPILER == R_FDL_COMP_IAR
        #define R_FDL_NOINIT __no_init
    #elif R_FDL_COMPILER == R_FDL_COMP_REC
        #define R_FDL_NOINIT
    #endif

    extern R_FDL_NOINIT r_fdl_data_t   g_fdl_str;     

    #define R_FDL_STOP_SEC_VAR
    #include "r_fdl_mem_map.h"



#ifdef PATCH_TO_SIMULATE_ERRORS
    #include "FDL-tc_common.h"
#endif

    #ifdef _BullseyeCoverage
        #define R_FDL_COV_SAVEOFF _Pragma ("BullseyeCoverage save off")
        #define R_FDL_COV_RESTORE _Pragma ("BullseyeCoverage restore")
    #else
        #define R_FDL_COV_SAVEOFF
        #define R_FDL_COV_RESTORE
    #endif
#endif       



