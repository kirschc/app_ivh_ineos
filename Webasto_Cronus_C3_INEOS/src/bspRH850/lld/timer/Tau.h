#ifndef TAU_H
#define TAU_H


#include "hal_data_types.h"
#include "Tau_Cfg.h"


#define TAU_MODULE_ID                       ((uint8_t)255)
#define TAU_INSTANCE_ID_0                   ((uint8_t)0)

#define TAU_INIT_ID                         0x00u 
#define TAU_START_STOP_TIMER_SINGLE_ID      0x01u 
#define TAU_START_STOP_TIMER_GROUP_ID       0x02u 
#define TAU_SET_CHANNEL_MODE_ID             0x03u 
#define TAU_SET_OUTPUT_CHANNEL_MODE_ID      0x04u 
#define TAU_SET_PWM_FREQUENCY_ID            0x05u 
#define TAU_GET_PWM_FREQUENCY_ID            0x06u 
#define TAU_SET_PWM_DUTY_CYCLE_ID           0x07u 
#define TAU_SET_PWM_DUTY_CYCLE_GROUP_ID     0x08u 
#define TAU_GET_PWM_DUTY_CYCLE_ID           0x09u 

#define TAU_E_CONFIG                        0x10u  
#define TAU_E_UNIT                          0x11u  

#define CMOR_MASK_MD0                       0x0001u
#define CMOR_MASK_MD1_MD4                   0x001Eu
#define CMOR_MASK_COS1                      0x0040u
#define CMOR_MASK_COS2                      0x0080u
#define CMOR_MASK_COS3                      0x00C0u
#define CMOR_MASK_STS1                      0x0100u
#define CMOR_MASK_STS2                      0x0200u
#define CMOR_MASK_STS3                      0x0300u
#define CMOR_MASK_STS4                      0x0400u
#define CMOR_MASK_STS5                      0x0500u
#define CMOR_MASK_STS6                      0x0600u
#define CMOR_MASK_STS7                      0x0700u
#define CMOR_MASK_MAS                       0x0800u
#define CMOR_MASK_CCS0                      0x1000u
#define CMOR_MASK_CKS1                      0x4000u
#define CMOR_MASK_CKS2                      0x8000u
#define CMOR_MASK_CKS3                      0xC000u

#define RESOLUTION_1_10                     1u
#define RESOLUTION_10_100                   10u
#define RESOLUTION_100_1000                 100u
#define RESOLUTION_1000                     1000u

#define PWM_DUTY_MAX                        1000u
#define PWM_FREQ_MAX                        40000u


typedef enum
{
    TAUB_UNIT_0                   = 0u,
    TAUB_UNIT_1                       ,
    TAUD_UNIT_0                       ,
    TAUJ_UNIT_0                       ,
    TAUJ_UNIT_1                       ,
    TAU_UNIT_MAX
}enum_TAU_UNIT;

typedef enum
{
    TAU_UNIT_NO_INIT              = 0u,
    TAU_UNIT_INIT                     ,
    TAU_UNIT_INIT_MAX
}enum_TAU_INIT;

typedef enum
{
    TAU_UNIT_CH0                      = 0u,
    TAU_UNIT_CH1                          ,
    TAU_UNIT_CH2                          ,
    TAU_UNIT_CH3                          ,
    TAU_UNIT_CH4                          ,
    TAU_UNIT_CH5                          ,
    TAU_UNIT_CH6                          ,
    TAU_UNIT_CH7                          ,
    TAU_UNIT_CH8                          ,
    TAU_UNIT_CH9                          ,
    TAU_UNIT_CH10                          ,
    TAU_UNIT_CH11                          ,
    TAU_UNIT_CH12                          ,
    TAU_UNIT_CH13                          ,
    TAU_UNIT_CH14                          ,
    TAU_UNIT_CH15                          ,
    TAU_UNIT_CH_MAX

}enum_TAU_UNIT_CHANNEL;

typedef enum
{

    CLOCK_SOURCE_CK0                 = 0,
    CLOCK_SOURCE_CK1                    ,
    CLOCK_SOURCE_CK2                    ,
    CLOCK_SOURCE_CK3                    ,
    CLOCK_SOURCE_MAX

}enum_TAU_CLOCK_SRC;

typedef enum
{

    CLOCK_PRES_PCLK_2_0              = 0, 
    CLOCK_PRES_PCLK_2_1                 , 
    CLOCK_PRES_PCLK_2_2                 , 
    CLOCK_PRES_PCLK_2_3                 , 
    CLOCK_PRES_PCLK_2_4                 , 
    CLOCK_PRES_PCLK_2_5                 , 
    CLOCK_PRES_PCLK_2_6                 , 
    CLOCK_PRES_PCLK_2_7                 , 
    CLOCK_PRES_PCLK_2_8                 , 
    CLOCK_PRES_PCLK_2_9                 , 
    CLOCK_PRES_PCLK_2_10                , 
    CLOCK_PRES_PCLK_2_11                , 
    CLOCK_PRES_PCLK_2_12                , 
    CLOCK_PRES_PCLK_2_13                , 
    CLOCK_PRES_PCLK_2_14                , 
    CLOCK_PRES_PCLK_2_15                , 
    CLOCK_PRES_PCLK_MAX

}enum_TAU_CLOCK_PRES;

typedef enum
{
    MODE_INTERVAL_TIMER          = 0u,  
    MODE_JUDGE                       ,  
    MODE_CAPTURE                     ,  
    MODE_EVENT_COUNT                 ,  
    MODE_ONE_COUNT                   ,  
    MODE_UNUSED                      ,  
    MODE_CAPTURE_AND_ONE_COUNT       ,  
    MODE_JUDGE_AND_ONE_COUNT         ,  
    MODE_UNSUED2                     ,  
    MODE_COUNT_UP_DOWN               ,  
    MODE_PULSE_ONE_COUNT             ,  
    MODE_COUNT_CAPTURE               ,  
    MODE_GATE_COUNT                  ,  
    MODE_CAPTURE_AND_GATE_COUNT      ,  

}enum_TAU_MODE;

typedef enum
{
    TYPE_MASTER                  = 0u,
    TYPE_SLAVE

}enum_CHANNEL_TYPE;


typedef enum
{
    FUNC_INTERVAL_TIMER           = 0u,
    FUNC_INPUT_INTERVAL_TIMER         ,
    FUNC_CLK_DIVIDE                   ,
    FUNC_EXT_EVENT_CNT                ,
    FUNC_ONE_PULSE_OUT                ,
    FUNC_IN_PULSE_INTERVAL_MEAS       ,
    FUNC_IN_SIGNAL_WIDTH_MEAS         ,
    FUNC_IN_POS_DETECTION             ,
    FUNC_IN_PERIODIC_CNT_DETECTION    ,
    FUNC_IN_PULSE_INTERVAL_JUDGE      ,
    FUNC_IN_SIGNAL_WIDTH_JUDGE        ,
    FUNC_OVERFLOW_INTERRUPT_OUTPUT_1  ,
    FUNC_OVERFLOW_INTERRUPT_OUTPUT_2  ,
    FUNC_SIMULTANOUS_REWRITE_TRIG     ,
    FUNC_OUT_PWM                      ,
    FUNC_OUT_ONE_SHOT_PULSE           ,
    FUNC_OUT_DELAY_PULSE              ,
    FUNC_OUT_AD_CONV_TRIG_TYPE_1      ,
    FUNC_OUT_TRIANGLE                 ,
    FUNC_OUT_TRIANGLE_DEADTIME        ,
    FUNC_OUT_AD_CONV_TRIG_TYPE_2      ,
    FUNC_MAX

}enum_FUNC;

typedef enum
{
    TAU_STOP                    = 0u,
    TAU_START

}enum_OPERATION_MODE;

typedef enum
{   PWM_LEVEL_ACTIVE_LOW              = 0u,
    PWM_LEVEL_ACTIVE_HIGH                 ,
    PWM_LEVEL_NOT_SET

}enum_TAU_PWM_LEVEL;

typedef enum
{
    PWM_GROUP_00                    = 0,
    PWM_GROUP_01                       ,
    PWM_GROUP_NOT_SET

}enum_TAU_PWM_GROUP;

typedef enum
{
    PWM_RES_1_10                = 0u,
    PWM_RES_10_100                  ,
    PWM_RES_100_1000                ,
    PWM_RES_1000                    ,
    PWM_RES_MAX

}enum_PWM_FREQ_RES;

typedef enum
{
    ISR_PRIO_HIGHEST            = 0u,
    ISR_PRIO_LEVEL_1                ,
    ISR_PRIO_LEVEL_2                ,
    ISR_PRIO_LEVEL_3                ,
    ISR_PRIO_LEVEL_4                ,
    ISR_PRIO_LEVEL_5                ,
    ISR_PRIO_LEVEL_6                ,
    ISR_PRIO_LOWEST
}enum_TIMER_ISR_LEVEL;

typedef struct
{
    uint8_t channel;
    enum_TAU_INIT init;
    enum_FUNC func;
    enum_CHANNEL_TYPE channel_type;
    enum_TAU_UNIT unit;
    enum_TAU_UNIT_CHANNEL unit_channel;
    enum_TAU_CLOCK_SRC clk_src;
    enum_TAU_CLOCK_PRES clk_pres;
    uint16_t cdr;
    uint8_t isr_en;
    uint8_t isr_prio;
    enum_TAU_PWM_GROUP group;
    enum_TAU_PWM_LEVEL level;

}tau_cfg_t;

typedef struct
{
    enum_TAU_UNIT unit;
    uint32_t clk;
    enum_TAU_CLOCK_PRES clk_pres[PWM_RES_MAX];

}pwm_freq_cfg_t;


extern const tau_cfg_t tau_cfg[];



void Tau_Init(void);

void Tau_StartStopTimerSingle(enum_OPERATION_MODE mode, uint8_t channel);

void Tau_StartStopTimerGroup(enum_OPERATION_MODE mode, enum_TAU_PWM_GROUP group);

void Tau_SetChannelMode(reg_16bit_t ptr_reg, const tau_cfg_t* ptr_cfg);

void Tau_SetChannelOutputMode (const tau_cfg_t* ptr_cfg);

void Tau_SetPwmFrequency(uint8_t channel, uint32_t freq);

uint32_t Tau_GetPwmFrequency(uint8_t channel);

void Tau_SetPwmDutyCycle(uint8_t channel, uint16_t duty);

uint16_t Tau_GetPwmDutyCycle(uint8_t channel);

void Tau_SetPwmDutyCycleGroup(enum_TAU_PWM_GROUP group);

void Tau_PwmProcessCyclic(void);

uint32_t Tau_GetChannelDataRegister(uint8_t channel);

void Tau_SetChannelDataRegister(uint8_t channel, uint32_t reg_val);

uint8_t Tau_GetMasterOfPwmGroup(enum_TAU_PWM_GROUP group);

void Tau_EnableDisableTauChannelInterrupt(enum_TAU_UNIT unit, enum_TAU_UNIT_CHANNEL unit_channel, uint8_t isr_en, uint8_t isr_prio);

void Tau_SetPclkPrescaler(enum_TAU_UNIT unit, enum_TAU_CLOCK_SRC clk_src, enum_TAU_CLOCK_PRES clk_pres);

#endif


