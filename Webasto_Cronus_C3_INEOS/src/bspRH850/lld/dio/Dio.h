
#ifndef DIO_H
#define DIO_H


#include "Std_Types.h"
#include "Dio_Cfg.h"


#define DIO_SW_MAJOR_VERSION                       1u

#define DIO_SW_MINOR_VERSION                       1u

#define DIO_SW_PATCH_VERSION                       2u

#define DIO_VALID_CONFIG_VERSION                   1u

#define DIO_MODULE_ID                              ((uint8_t)120)

#define DIO_VENDOR_ID                              ((uint16_t)0)

#define DIO_INSTANCE_ID_0                          ((uint8_t)0)

#define DIO_WRITE_CHANNEL                          0x33u

#define DIO_READ_CHANNEL                           0x44u

#define  DIO_READ_PORTS                            13u

#define  DIO_WRITE_PORTS                           13u

#define  DIO_SHIFT_4                               4u

#define  DIO_MASK_0x0F                             0x0F

#define  DIO_MASK_0x01                             0x01


#define Dio_GetVersionInfo(                                 \
              \
   versioninfo)                                             \
   versioninfo.moduleID = DIO_MODULE_ID;                    \
   versioninfo.vendorID = DIO_VENDOR_ID;                    \
   versioninfo.sw_major_version = DIO_SW_MAJOR_VERSION;     \
   versioninfo.sw_minor_version = DIO_SW_MINOR_VERSION;     \
   versioninfo.sw_patch_version = DIO_SW_PATCH_VERSION;

#define Dio_SetPortPin( Port, Pin )                (uint8_t)((((uint8_t)(Port) & 0x0Fu) << 4) | ((uint8_t)(Pin) & 0x0Fu))

#define Dio_GetPort( PortPin )                     (uint8_t)(((uint8_t)(PortPin) >> 4) & 0x0Fu)

#define Dio_GetPin( PortPin )                      (uint8_t)((uint8_t)(PortPin) & 0x0Fu)


typedef enum
{
   DIO_PORT_0 = 0u,
   DIO_PORT_1,
   DIO_PORT_2,
   DIO_PORT_8,
   DIO_PORT_9,
   DIO_PORT_10,
   DIO_PORT_11,
   DIO_PORT_12,
   DIO_PORT_18,
   DIO_PORT_20,
   DIO_PORT_JP,
   DIO_PORT_AP0,
   DIO_PORT_AP1,
   DIO_PORT_MAX_PORT
}
Dio_PortIdTypes;

typedef uint8_t Dio_ChannelType;

typedef uint8_t Dio_PortType;

typedef uint16_t Dio_PortLevelType;

typedef uint8_t Dio_LevelType;

typedef struct
{
   Dio_PortType Dio_PortGroup;
   uint8_t Dio_Offset;
   uint8_t Dio_Mask;
}
Dio_ChannelGroupType;

typedef struct
{
   Dio_PinIdType Dio_PinName;
   uint8_t Dio_PortPin;
   uint8_t Dio_Direction;
}
Dio_ChannelConfigType;


Dio_LevelType Dio_ReadChannel(
   Dio_ChannelType ChannelId);

Dio_LevelType Dio_ReadBackChannel(
   Dio_ChannelType ChannelId);

void Dio_WriteChannel(
   Dio_ChannelType ChannelId,
   Dio_LevelType Level);

Dio_PortLevelType Dio_ReadPort(
   Dio_PortType PortId);

void Dio_WritePort(
   Dio_PortType PortId,
   Dio_PortLevelType Level);

Dio_PortLevelType Dio_ReadChannelGroup(
   const Dio_ChannelGroupType* ChannelGroupIdPtr);

void Dio_WriteChannelGroup(
   const Dio_ChannelGroupType* ChannelGroupIdPtr,
   Dio_PortLevelType Level);




#else
#error Multiple include of "Dio.h"
#endif 


