
#ifndef PWM_H
#define PWM_H


#include "Std_Types.h"
#include "Pwm_Cfg.h"


#define PWM_SW_MAJOR_VERSION                       1u

#define PWM_SW_MINOR_VERSION                       1u

#define PWM_SW_PATCH_VERSION                       1u

#define PWM_MODULE_ID                              ((uint8_t) 121u)

#define PWM_INSTANCE_ID0                           ((uint16_t) 0x0000)

#define PWM_VENDOR_ID                              ((uint16_t) 0u)

#define PWM_0_PERCENT                              0x0000u

#define PWM_100_PERCENT                            0x1000u

#define PWM_NUMBER_OF_CHANNELS                     72u

#define  PWM_SEL_CLK0                              0u

#define  PWM_SEL_CLK1                              1u

#define  PWM_SEL_CLK2                              2u

#define  PWM_CLK0                                  0x01u

#define  PWM_CLK1                                  0x02u

#define PWM_PRESCALE_10                            0x000Au

#define PWM_PRESCALE_0                             0x0000u

#define PWM_PRESCALE_24                            0x0018u

#define PWM_SLPWGA0                                32u

#define PWM_SLPWGA1                                64u

#define PWM_SLPWGA2                                72u

#define PWM_TRIGGER                                0x01u

#define  PWM_WAIT_TIME                             100000u


#define Pwm_GetVersionInfo(                                                \
    \
   versioninfo)                                                            \
   versioninfo.moduleID = PWM_MODULE_ID;                                   \
   versioninfo.vendorID = PWM_VENDOR_ID;                                   \
   versioninfo.sw_major_version = PWM_SW_MAJOR_VERSION;                    \
   versioninfo.sw_minor_version = PWM_SW_MINOR_VERSION;                    \
   versioninfo.sw_patch_version = PWM_SW_PATCH_VERSION;


typedef enum
{
   PWM_203HZ = 0u,
   PWM_9765HZ = 1u
}
Pwm_FrequencyType;

typedef uint16_t Pwm_PeriodType;

typedef enum
{
   PWM_LOW = 0u,
   PWM_HIGH = 1u
}
Pwm_OutputStateType;

typedef enum
{
   PWM_FALLING_EDGE = 0u,
   PWM_RISING_EDGE,
   PWM_BOTH_EDGES
}
Pwm_EdgeNotificationType;

typedef struct
{
   uint8_t channel;
   uint8_t channelActivation;
   uint8_t clkSelection;
   uint16_t defaultDutyCycle;
}
Pwm_ChannelConfigType;

typedef struct
{
   uint16_t *addressPWBAnBRS0;
   uint16_t *addressPWBAnBRS1;
   uint16_t *addressPWBAnBRS2;
   uint16_t *addressPWBAnBRS3;
   uint32_t *addressSLPWGA0;
   uint32_t *addressSLPWGA1;
   uint32_t *addressSLPWGA2;
   uint8_t *addressPWBA0TS;
   uint8_t *addressPWBA0TT;
}
Pwm_RegisterAddressTypeCom;

typedef struct
{
   uint16_t *addressPWGAnCSDR;
   uint16_t *addressPWGAnCRDR;
   uint8_t *addressPWGAnCTL;
   uint8_t *addressPWGAnRSF;
   uint8_t *addressPWGAnRDT;
}
Pwm_RegisterAddressTypeInd;

typedef struct
{
   const uint8_t interruptPriority;
   const Pwm_ChannelConfigType *channelConfig;
}
Pwm_ConfigType;


void Pwm_Init(
   const Pwm_ConfigType* configPtr);

void Pwm_DeInit(void);

void Pwm_SetDutyCycle(
   uint8_t channelNumber,
   uint16_t dutyCycle);

void Pwm_SetPeriodAndDuty(
   uint8_t channelNumber,
   Pwm_FrequencyType frequ,
   uint16_t dutyCycle);


extern const Pwm_ConfigType Pwm_Config;

extern const Pwm_RegisterAddressTypeCom Pwm_RegisterAddressCom;

extern const Pwm_RegisterAddressTypeInd Pwm_RegisterAddressInd[PWM_NUMBER_OF_CHANNELS];


#else
#error Multiple include of "Pwm.h"
#endif 


