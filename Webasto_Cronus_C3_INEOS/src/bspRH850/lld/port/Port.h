
#ifndef PORT_H
#define PORT_H


#include "Port_Cfg.h"
#include "Std_Types.h"


#define PORT_SW_MAJOR_VERSION                      1u

#define PORT_SW_MINOR_VERSION                      0u

#define PORT_SW_PATCH_VERSION                      0u

#define PORT_VALID_CONFIG_VERSION                  1u

#define PORT_MODULE_ID                             ((uint8_t)124)

#define PORT_VENDOR_ID                             ((uint16_t)0)

#define PORT_INSTANCE_ID_0                         ((uint8_t)0)

#define PORT_MAX_PINS                              256u

#define PORT_PIN_IO                                0u

#define PORT_PIN_ALTERNATE                         1u

#define PORT_OFFSET                                1u

#define PORT_WRITE_MODE_REG                        0x33u

#define PORT_READ_MODE_REG                         0x44u

#define PORT_PROTECT_A5                            0x000000A5u

#define PORT_PROTECT_0                             0x00000000u

#define PORT_PROTECT_F                             0xFFFFFFFFu

#define PORT_SHIFT_4                               4u

#define PORT_MASK_0x0F                             0x0F

#define PORT_MASK_1                                1u


#define Port_GetVersionInfo(                                                \
    \
   versioninfo)                                                            \
   versioninfo.moduleID = PORT_MODULE_ID;                                  \
   versioninfo.vendorID = PORT_VENDOR_ID;                                  \
   versioninfo.sw_major_version = PORT_SW_MAJOR_VERSION;                   \
   versioninfo.sw_minor_version = PORT_SW_MINOR_VERSION;                   \
   versioninfo.sw_patch_version = PORT_SW_PATCH_VERSION;

#define PORT_SET_REG_BIT(                                                  \
                                                   \
   Reg,                                                                    \
                                                        \
   Bit,                                                                    \
            \
   Value)                                                                  \
   ((1u == (uint16_t)Value) ? (Reg | ((uint16_t)(1u << Bit)))                  \
   : (Reg & (uint16_t)(~((uint16_t)(1u << Bit)))))


typedef enum
{
   PORT_CHANNEL_0000 = 0u,
   PORT_CHANNEL_0001,
   PORT_CHANNEL_0002,
   PORT_CHANNEL_0003,
   PORT_CHANNEL_0004,
   PORT_CHANNEL_0005,
   PORT_CHANNEL_0006,
   PORT_CHANNEL_0007,
   PORT_CHANNEL_0008,
   PORT_CHANNEL_0009,
   PORT_CHANNEL_0010,
   PORT_CHANNEL_0011,
   PORT_CHANNEL_0012,
   PORT_CHANNEL_0013,
   PORT_CHANNEL_0014,
   PORT_DUMMY_CHANNEL_0015,
   PORT_CHANNEL_0100,
   PORT_CHANNEL_0101,
   PORT_CHANNEL_0102,
   PORT_CHANNEL_0103,
   PORT_CHANNEL_0104,
   PORT_CHANNEL_0105,
   PORT_CHANNEL_0106,
   PORT_CHANNEL_0107,
   PORT_CHANNEL_0108,
   PORT_CHANNEL_0109,
   PORT_CHANNEL_0110,
   PORT_CHANNEL_0111,
   PORT_CHANNEL_0112,
   PORT_CHANNEL_0113,
   PORT_CHANNEL_0114,
   PORT_CHANNEL_0115,
   PORT_CHANNEL_0200,
   PORT_CHANNEL_0201,
   PORT_CHANNEL_0202,
   PORT_CHANNEL_0203,
   PORT_CHANNEL_0204,
   PORT_CHANNEL_0205,
   PORT_CHANNEL_0206,
   PORT_DUMMY_CHANNEL_0207,
   PORT_DUMMY_CHANNEL_0208,
   PORT_DUMMY_CHANNEL_0209,
   PORT_DUMMY_CHANNEL_0210,
   PORT_DUMMY_CHANNEL_0211,
   PORT_DUMMY_CHANNEL_0212,
   PORT_DUMMY_CHANNEL_0213,
   PORT_DUMMY_CHANNEL_0214,
   PORT_DUMMY_CHANNEL_0215,
   PORT_CHANNEL_0800,
   PORT_CHANNEL_0801,
   PORT_CHANNEL_0802,
   PORT_CHANNEL_0803,
   PORT_CHANNEL_0804,
   PORT_CHANNEL_0805,
   PORT_CHANNEL_0806,
   PORT_CHANNEL_0807,
   PORT_CHANNEL_0808,
   PORT_CHANNEL_0809,
   PORT_CHANNEL_0810,
   PORT_CHANNEL_0811,
   PORT_CHANNEL_0812,
   PORT_DUMMY_CHANNEL_0813,
   PORT_DUMMY_CHANNEL_0814,
   PORT_DUMMY_CHANNEL_0815,
   PORT_CHANNEL_0900,
   PORT_CHANNEL_0901,
   PORT_CHANNEL_0902,
   PORT_CHANNEL_0903,
   PORT_CHANNEL_0904,
   PORT_CHANNEL_0905,
   PORT_CHANNEL_0906,
   PORT_DUMMY_CHANNEL_0907,
   PORT_DUMMY_CHANNEL_0908,
   PORT_DUMMY_CHANNEL_0909,
   PORT_DUMMY_CHANNEL_0910,
   PORT_DUMMY_CHANNEL_0911,
   PORT_DUMMY_CHANNEL_0912,
   PORT_DUMMY_CHANNEL_0913,
   PORT_DUMMY_CHANNEL_0914,
   PORT_DUMMY_CHANNEL_0915,
   PORT_CHANNEL_1000,
   PORT_CHANNEL_1001,
   PORT_CHANNEL_1002,
   PORT_CHANNEL_1003,
   PORT_CHANNEL_1004,
   PORT_CHANNEL_1005,
   PORT_CHANNEL_1006,
   PORT_CHANNEL_1007,
   PORT_CHANNEL_1008,
   PORT_CHANNEL_1009,
   PORT_CHANNEL_1010,
   PORT_CHANNEL_1011,
   PORT_CHANNEL_1012,
   PORT_CHANNEL_1013,
   PORT_CHANNEL_1014,
   PORT_CHANNEL_1015,
   PORT_CHANNEL_1100,
   PORT_CHANNEL_1101,
   PORT_CHANNEL_1102,
   PORT_CHANNEL_1103,
   PORT_CHANNEL_1104,
   PORT_CHANNEL_1105,
   PORT_CHANNEL_1106,
   PORT_CHANNEL_1107,
   PORT_CHANNEL_1108,
   PORT_CHANNEL_1109,
   PORT_CHANNEL_1110,
   PORT_CHANNEL_1111,
   PORT_CHANNEL_1112,
   PORT_CHANNEL_1113,
   PORT_CHANNEL_1114,
   PORT_CHANNEL_1115,
   PORT_CHANNEL_1200,
   PORT_CHANNEL_1201,
   PORT_CHANNEL_1202,
   PORT_CHANNEL_1203,
   PORT_CHANNEL_1204,
   PORT_CHANNEL_1205,
   PORT_DUMMY_CHANNEL_1206,
   PORT_DUMMY_CHANNEL_1207,
   PORT_DUMMY_CHANNEL_1208,
   PORT_DUMMY_CHANNEL_1209,
   PORT_DUMMY_CHANNEL_1210,
   PORT_DUMMY_CHANNEL_1211,
   PORT_DUMMY_CHANNEL_1212,
   PORT_DUMMY_CHANNEL_1213,
   PORT_DUMMY_CHANNEL_1214,
   PORT_DUMMY_CHANNEL_1215,
   PORT_CHANNEL_1800,
   PORT_CHANNEL_1801,
   PORT_CHANNEL_1802,
   PORT_CHANNEL_1803,
   PORT_CHANNEL_1804,
   PORT_CHANNEL_1805,
   PORT_CHANNEL_1806,
   PORT_CHANNEL_1807,
   PORT_DUMMY_CHANNEL_1808,
   PORT_DUMMY_CHANNEL_1809,
   PORT_DUMMY_CHANNEL_1810,
   PORT_DUMMY_CHANNEL_1811,
   PORT_DUMMY_CHANNEL_1812,
   PORT_DUMMY_CHANNEL_1813,
   PORT_DUMMY_CHANNEL_1814,
   PORT_DUMMY_CHANNEL_1815,
   PORT_CHANNEL_2000,
   PORT_CHANNEL_2001,
   PORT_CHANNEL_2002,
   PORT_CHANNEL_2003,
   PORT_CHANNEL_2004,
   PORT_CHANNEL_2005,
   PORT_DUMMY_CHANNEL_2006,
   PORT_DUMMY_CHANNEL_2007,
   PORT_DUMMY_CHANNEL_2008,
   PORT_DUMMY_CHANNEL_2009,
   PORT_DUMMY_CHANNEL_2010,
   PORT_DUMMY_CHANNEL_2011,
   PORT_DUMMY_CHANNEL_2012,
   PORT_DUMMY_CHANNEL_2013,
   PORT_DUMMY_CHANNEL_2014,
   PORT_DUMMY_CHANNEL_2015,
   PORT_CHANNEL_JP0,
   PORT_CHANNEL_JP1,
   PORT_CHANNEL_JP2,
   PORT_CHANNEL_JP3,
   PORT_CHANNEL_JP4,
   PORT_CHANNEL_JP5,
   PORT_DUMMY_CHANNEL_JP6,
   PORT_DUMMY_CHANNEL_JP7,
   PORT_DUMMY_CHANNEL_JP8,
   PORT_DUMMY_CHANNEL_JP9,
   PORT_DUMMY_CHANNEL_JP10,
   PORT_DUMMY_CHANNEL_JP11,
   PORT_DUMMY_CHANNEL_JP12,
   PORT_DUMMY_CHANNEL_JP13,
   PORT_DUMMY_CHANNEL_JP14,
   PORT_DUMMY_CHANNEL_JP15,
   PORT_CHANNEL_MAX_PINS
}
Port_PinIdType;

typedef enum
{
   PORT_0 = 0u,
   PORT_1,
   PORT_2,
   PORT_8,
   PORT_9,
   PORT_10,
   PORT_11,
   PORT_12,
   PORT_18,
   PORT_20,
   PORT_JP,
   PORT_AP0,
   PORT_AP1,
   PORT_IP0,
   PORT_MAX_PORTS
}
Port_PortIdType;

typedef uint8_t Port_PinType;

typedef uint16_t Port_LevelType;

typedef uint16_t Port_ModeType;
typedef uint16_t Port_ModeControlType;

typedef uint16_t Port_FunctionType;

typedef uint16_t Port_PullUpType;

typedef uint16_t Port_PullDownType;

typedef uint16_t Port_ExtendedFunctionType;

typedef uint16_t Port_InputBufferType;

typedef uint16_t Port_InputCtrlType;

typedef uint16_t Port_FuncitonCtrlAddExpType;

typedef uint16_t Port_BidirectionCtrlType;

typedef enum
{
   PORT_PIN_OUT = 0u,
   PORT_PIN_IN
}
Port_PinDirectionType;

typedef uint8_t Port_PinModeType;

typedef uint16_t Port_DirConfigType;

typedef struct
{
   Port_LevelType Level;
   Port_BidirectionCtrlType BidirectionCtrlM;
}
Port_Type1;

typedef struct
{
   Port_LevelType Level;
   Port_FunctionType Function;
   Port_BidirectionCtrlType BidirectionCtrlM;
}
Port_Type2;

typedef struct
{
   Port_LevelType Level;
   Port_PullUpType PullUp;
   Port_PullDownType PullDown;
   Port_ExtendedFunctionType FunctionExt;
   Port_FunctionType Function;
   Port_InputCtrlType InputCtrl;
   Port_FuncitonCtrlAddExpType FunctionCtrlAddExp;
   Port_BidirectionCtrlType BidirectionCtrlM;
}
Port_Type3;

typedef struct
{
   Port_ModeControlType ModeControl;
   Port_ModeType Mode;
   Port_InputBufferType Buffer;
   Port_DirConfigType DirChangeable;
}
Port_ModeAndModeControlType;

typedef struct
{
   Port_Type3 Port_0;
   Port_Type2 Port_1;
   Port_Type1 Port_2;
   Port_Type3 Port_8;
   Port_Type3 Port_9;
   Port_Type3 Port_10;
   Port_Type3 Port_11;
   Port_Type3 Port_12;
   Port_Type2 Port_18;
   Port_Type2 Port_20;
   Port_Type2 Port_JP;
   Port_Type1 Port_AP0;
   Port_Type1 Port_AP1;

   Port_ModeAndModeControlType Port_AToA2D[PORT_MAX_PORTS];

   uint8_t Port_CTL0NMI;
   uint8_t Port_CTL0INTPL;
   uint8_t Port_CTL1INTPL;
   uint8_t Port_CTL2INTPL;
   uint8_t Port_CTL3INTPL;
   uint8_t Port_CTL4INTPL;
   uint8_t Port_CTL5INTPL;
   uint8_t Port_CTL6INTPL;
   uint8_t Port_CTL7INTPL;
   uint8_t Port_CTL0INTPH;
   uint8_t Port_CTL1INTPH;
   uint8_t Port_CTL2INTPH;
   uint8_t Port_CTL3INTPH;
   uint8_t Port_CTL4INTPH;
   uint8_t Port_CTL5INTPH;
   uint8_t Port_CTL6INTPH;
   uint8_t Port_CTL7INTPH;

   uint8_t Port_IntConfP0;
   uint8_t Port_IntConfP1;
   uint8_t Port_IntConfP2;
   uint8_t Port_IntConfP3;
   uint8_t Port_IntConfP4;
   uint8_t Port_IntConfP5;
   uint8_t Port_IntConfP6;
   uint8_t Port_IntConfP7;
   uint8_t Port_IntConfP8;
   uint8_t Port_IntConfP9;
   uint8_t Port_IntConfP10;
   uint8_t Port_IntConfP11;
   uint8_t Port_IntConfP12;
   uint8_t Port_IntConfP13;
   uint8_t Port_IntConfP14;
   uint8_t Port_IntConfP15;

}
Port_ConfigType;


void Port_Init(
   const Port_ConfigType* ConfigPtr);

void Port_SetPinDirection(
   Port_PinType Pin,
   Port_PinDirectionType Direction);

void Port_RefreshPortDirection(void);

void Port_SetPinMode(
   Port_PinType Pin,
   Port_PinModeType Mode);


extern const Port_ConfigType Port_Config;


#else
#error Multiple include of "Port.h"
#endif 


