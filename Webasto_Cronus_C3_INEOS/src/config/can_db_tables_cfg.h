#ifndef _CAN_DB_TABLES_CFG_H_
#define _CAN_DB_TABLES_CFG_H_

#include "FreeRTOS.h"
#include "can_db_tables.h"

#define EOL_CAN_DB_TABLES_CFG_ID_OFFSET   100u 

typedef enum
{
    CAN_DB_TABLES_USER       = 0u, 
    CAN_DB_TABLES_EOL            , 
    CAN_DB_TABLES_EOL_OFFSET     , 
    CAN_DB_TABLES_ENUM_MAX         
} enum_CAN_DB_TABLES;

typedef struct
{
    can_bus_id           can_bus_rx; 
    struct_hal_can_frame can_frame;  
} struct_can_db_rx_buffer;
_Static_assert( sizeof(struct_can_db_rx_buffer) == (RTOS_SIZE_OF_STRUCT_CAN_DB_RX_BUFFER), \
                "Size of #struct_can_db_rx_buffer has changed -> adjust value #RTOS_SIZE_OF_STRUCT_CAN_DB_RX_BUFFER in FreeRTOSConfig!" );

extern uint8_t ext_sfl_can_received_any_flag[CAN_BUS_MAX];
extern uint8_t ext_sfl_can_stop_gw_unknown_ids[CAN_BUS_MAX];


void can_db_cfg_get_rx_buf_size(uint16_t *const ptr_var);


#endif


