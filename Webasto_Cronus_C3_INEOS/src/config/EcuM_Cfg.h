
#ifndef ECUM_CFG_H
#define ECUM_CFG_H


#include "Can.h"


#define ECUM_CFG_SW_MAJOR_VERSION                        1u

#define ECUM_CFG_SW_MINOR_VERSION                        0u

#define ECUM_CFG_SW_PATCH_VERSION                        0u

#define ECUM_DEFAULT_USER                                0u

#define ECUM_NUMBER_OF_USER                              1u

#define ECUM_DEFAULT_SOURCES_INIT                        0u

#define ECUM_START_UP_ONE                                EcuM_StartUpOne

#define ECUM_START_UP_TWO                                EcuM_StartUpTwo

#define ECUM_WAKE_UP_ONE                                 EcuM_WakeUpOne

#define ECUM_WAKE_UP_TWO                                 EcuM_WakeUpTwo

#define ECUM_RUN                                         EcuM_Run

#define ECUM_POWERFAIL                                   EcuM_Powerfail

#define ECUM_REVIVE_POWERFAIL                            EcuM_RevivePowerfail

#define ECUM_PREPARE_SHUT_DOWN                           EcuM_PrepShutDown

#define ECUM_SHUT_DOWN                                   EcuM_ShutDown

#define ASW_ID                                           88u








#else
   #error Multiple include of "EcuM_Cfg.h"
#endif 


