
#ifndef SPI_CONFIG_H
#define SPI_CONFIG_H



#define SPI_CFG_SW_MAJOR_VERSION          1u

#define SPI_CFG_SW_MINOR_VERSION          0u

#define SPI_CFG_SW_PATCH_VERSION          0u

#define SPI_TIMEOUT                       50u

#define SPI_MAX_CHANNELS                  16u

#define SPI_MAX_JOBS                      3u

#define SPI_MAX_SEQ                       2u


#define SPI_CSIG_BAUD                     0x6005u

#define SPI_CSIH_BAUD                     0x0014u



#define SPI_CH_ID0_MESSAGE                0x00u

#define SPI_BUFFER_TYPE_0                 SPI_IB

#define SPI_DATA_WIDTH_0                  SPI_BITS_8

#define SPI_BUFFER_WIDTH_0                0x03u

#define SPI_DEFAULT_DATA_0                0x00u


#define SPI_CH_ID1_MESSAGE                0x01u

#define SPI_BUFFER_TYPE_1                 SPI_IB

#define SPI_DATA_WIDTH_1                  SPI_BITS_8

#define SPI_BUFFER_WIDTH_1                0x01u

#define SPI_DEFAULT_DATA_1                0x00u


#define SPI_CH_ID2_MESSAGE      		  0x02u

#define SPI_BUFFER_TYPE_2                 SPI_IB

#define SPI_DATA_WIDTH_2                  SPI_BITS_8

#define SPI_BUFFER_WIDTH_2                0x02u

#define SPI_DEFAULT_DATA_2                0x00u


#define SPI_CH_ID3_MESSAGE      		  0x03u

#define SPI_BUFFER_TYPE_3                 SPI_IB

#define SPI_DATA_WIDTH_3                  SPI_BITS_8

#define SPI_BUFFER_WIDTH_3                0x03u

#define SPI_DEFAULT_DATA_3                0x00u


#define SPI_CH_ID4_MESSAGE      		  0x04u

#define SPI_BUFFER_TYPE_4                 SPI_IB

#define SPI_DATA_WIDTH_4                  SPI_BITS_8

#define SPI_BUFFER_WIDTH_4                0x02u

#define SPI_DEFAULT_DATA_4                0x00u


#define SPI_CH_ID5_MESSAGE      		  0x05u

#define SPI_BUFFER_TYPE_5                 SPI_IB

#define SPI_DATA_WIDTH_5                  SPI_BITS_8

#define SPI_BUFFER_WIDTH_5                0x05u

#define SPI_DEFAULT_DATA_5                0x00u


#define SPI_CH_ID6_MESSAGE                0x06u

#define SPI_BUFFER_TYPE_6                 SPI_IB

#define SPI_DATA_WIDTH_6                  SPI_BITS_8

#define SPI_BUFFER_WIDTH_6                0x06u

#define SPI_DEFAULT_DATA_6                0x00u


#define SPI_CH_ID7_MESSAGE                0x07u

#define SPI_BUFFER_TYPE_7                 SPI_IB

#define SPI_DATA_WIDTH_7                  SPI_BITS_8

#define SPI_BUFFER_WIDTH_7                0x07u

#define SPI_DEFAULT_DATA_7                0x00u


#define SPI_CH_ID8_MESSAGE      		  0x08u

#define SPI_BUFFER_TYPE_8                 SPI_IB

#define SPI_DATA_WIDTH_8                  SPI_BITS_8

#define SPI_BUFFER_WIDTH_8                0x08u

#define SPI_DEFAULT_DATA_8                0x00u


#define SPI_CH_ID9_MESSAGE      		  0x09u

#define SPI_BUFFER_TYPE_9                 SPI_IB

#define SPI_DATA_WIDTH_9                  SPI_BITS_8

#define SPI_BUFFER_WIDTH_9                0x09u

#define SPI_DEFAULT_DATA_9                0x00u


#define SPI_CH_ID10_MESSAGE     		  0x0Au

#define SPI_BUFFER_TYPE_10                SPI_IB

#define SPI_DATA_WIDTH_10                 SPI_BITS_8

#define SPI_BUFFER_WIDTH_10               0x0Au

#define SPI_DEFAULT_DATA_10               0x00u


#define SPI_CH_ID11_MESSAGE     		   0x0Bu

#define SPI_BUFFER_TYPE_11                SPI_IB

#define SPI_DATA_WIDTH_11                 SPI_BITS_8

#define SPI_BUFFER_WIDTH_11               0x0Bu

#define SPI_DEFAULT_DATA_11               0x00u


#define SPI_CH_ID12_MESSAGE               0x0Cu

#define SPI_BUFFER_TYPE_12                SPI_IB

#define SPI_DATA_WIDTH_12                 SPI_BITS_8

#define SPI_BUFFER_WIDTH_12               0x01u

#define SPI_DEFAULT_DATA_12               0x00u


#define SPI_CH_ID13_MESSAGE               0x0Du

#define SPI_BUFFER_TYPE_13                SPI_IB

#define SPI_DATA_WIDTH_13                 SPI_BITS_8

#define SPI_BUFFER_WIDTH_13               0x06u

#define SPI_DEFAULT_DATA_13               0x00u


#define SPI_CH_ID14_MESSAGE               0x0Eu

#define SPI_BUFFER_TYPE_14                SPI_IB

#define SPI_DATA_WIDTH_14                 SPI_BITS_16

#define SPI_BUFFER_WIDTH_14               0x01u

#define SPI_DEFAULT_DATA_14               0x00u


#define SPI_CH_ID15_MESSAGE               0x0Fu

#define SPI_BUFFER_TYPE_15                SPI_IB

#define SPI_DATA_WIDTH_15                 SPI_BITS_8

#define SPI_BUFFER_WIDTH_15               0x06u

#define SPI_DEFAULT_DATA_15               0x00u




#define SPI_JOB_ID0_MESSAGE               0x00u

#define SPI_NUMBER_OF_CHANNELS_0          0x01u

#define SPI_HW_UNIT_0                     SPI_0
#define SPI_BAUDRATE_0                    SPI_CSIG_BAUD
#define SPI_DIRECTION_MODE_0              MSB_FIRST

#define SPI_CLOCK_IDLE_0                  SPI_CLOCK_IDLE_LOW

#define SPI_EDGE_TIMING_0                 SPI_CLOCK_FIRST_EDGE

#define SPI_CHANNELLIST_0                 Spi_ChannelList0

#define SPI_JOB_0_END_NOTIFICATION        NULL_PTR

#define SPI_WAITING_TIME_0                0u

#define SPI_JOB_0_CS_NO                   0u


#define SPI_JOB_ID1_MESSAGE               0x01u

#define SPI_NUMBER_OF_CHANNELS_1          0x01u

#define SPI_HW_UNIT_1                     SPI_1
#define SPI_BAUDRATE_1                    SPI_CSIG_BAUD
#define SPI_DIRECTION_MODE_1              MSB_FIRST

#define SPI_CLOCK_IDLE_1                  SPI_CLOCK_IDLE_LOW

#define SPI_EDGE_TIMING_1                 SPI_CLOCK_FIRST_EDGE

#define SPI_CHANNELLIST_1                 Spi_ChannelList1

#define SPI_JOB_1_END_NOTIFICATION        NULL_PTR

#define SPI_WAITING_TIME_1                0u

#define SPI_JOB_1_CS_NO                   1u




#define SPI_SEQ_ID0_MESSAGE               0x00u

#define SPI_NUMBER_OF_JOBS_0              0x01u

#define SPI_JOBLIST_0                     Spi_JobList0

#define SPI_SEQ_0_END_NOTIFICATION        NULL_PTR



#define SPI_SEQ_ID1_MESSAGE               0x01u

#define SPI_NUMBER_OF_JOBS_1              0x01u

#define SPI_JOBLIST_1                     Spi_JobList1

#define SPI_SEQ_1_END_NOTIFICATION        NULL_PTR





void Spi_WaitXus(
   uint32_t waitingTime);

void Spi_EnableCsSpi0( uint8_t numChipSelect );

void Spi_DisableCsSpi0( uint8_t numChipSelect );

void Spi_EnableCsSpi1( uint8_t numChipSelect );

void Spi_DisableCsSpi1( uint8_t numChipSelect );

void Spi_EnableCsSpi2( uint8_t numChipSelect );

void Spi_DisableCsSpi2( uint8_t numChipSelect );

void Spi_EnableCsSpi3( uint8_t numChipSelect );

void Spi_DisableCsSpi3( uint8_t numChipSelect );

void Spi_EnableCsSpi4( uint8_t numChipSelect );

void Spi_DisableCsSpi4( uint8_t numChipSelect );



#else
#error Multiple include of "Spi_Cfg.h"
#endif 


