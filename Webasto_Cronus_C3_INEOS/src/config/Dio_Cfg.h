
#ifndef DIO_CFG_H
#define DIO_CFG_H



#define DIO_CFG_SW_MAJOR_VERSION      1u

#define DIO_CFG_SW_MINOR_VERSION      0u

#define DIO_CFG_SW_PATCH_VERSION      0u



typedef enum
{
    DO_CAN1_EN = 0x00u,
    DO_CAN2_EN,
    SCI_PIGGY_TXD,
    SCI_PIGGY_RXD,
    SCI_LIN1_RXD,
    SCI_LIN1_TXD,
    AI_PWM_IN2,
    DO_IO3_PU,
    DO_IO4_PU,
    INT_PIGGY_INH,
    DO_RST_PEAK_DETEKTOR,
    I2C_SDA,
    SPI_MISO,
    SPI_MOSI,
    SPI_CLK,

    SCI_LIN3_RXD,
    SCI_LIN3_TXD,
    CAN3_RXD,
    CAN3_TXD,
    SCI_BT_RXD,
    SCI_BT_TXD,
    SCI_LIN5_RXD,
    SCI_LIN5_TXD,
    SCI_LIN4_RXD,
    SCI_LIN4_TXD,
    CS_SPI2_PRESSURE,
    DO_CAN1_TERMINATION,
    open_P1_12,
    open_P1_13,
    open_P1_14,
    open_P1_15,

    INT_PWM,
    SCI_BT_RTS,
    DO_WBUS3_TER,
    INT_IO3,
    INT_PWM_IN,
    INT_PWM_IN2,
    DO_LIN1_EN,
    DO_LIN2_EN,
    DO_LIN3_EN,
    DO_AO_RANGE,
    CS_SPI2_PIGGY,
    DI_CAN2_EER,
    DO_PWM_ANA,

    DI_CAN1_EER,
    PWM_LS,
    CS_SPI_PMIC,
    SCI_BT_CTS,
    DO_EN_PWM,
    DO_LIN2_TER,
    DO_EN_VCC7,

    PWM_HS,
    open_P10_1,
    open_P10_2,
    I2C_SCL,
    DO_CAN2_WAKE,
    PWM_IO3_HS,
    CAN1_RXD,
    CAN1_TXD,
    JTAG_FLMD1,
    PWM_IO3_LS,
    DO_RESET_BT,
    DO_FAULT_RST,
    DO_CAN1_WAKE,
    SCI_LIN2_RXD,
    SCI_LIN2_TXD,
    DO_LIN3_TER,

    DO_BEDIENTEIL,
    DO_LIN1_WAKE,
    DO_RELAIS_CAN,
    DO_LIN5_EN,
    DO_BT_WAKE,
    DO_LIN2_WAKE,
    DO_LIN3_WAKE,
    DO_SELn,
    DO_CAN2_STB,
    SPI2_MOSI,
    SPI2_CLK,
    SPI2_MISO,
    DO_SEL0,
    DO_SEL1,
    DO_CAN1_STB,
    CAN2_RXD,

    CAN2_TXD,
    AI_LIN3_2,
    DO_REL_OUT,
    open_P12_3,
    open_P12_4,
    open_P12_5,

    DO_LIN5_WAKE,
    DO_RELAIS_LIN,
    CS_SPI2_PIGGY_2,
    DO_PIGGY_ENABLE,
    open_P18_4,
    open_P18_5,
    open_P18_6,
    open_P18_7,

    open_P20_0,
    open_P20_1,
    open_P20_2,
    open_P20_3,
    DO_CAN2_TERMINATION,
    DO_CAN3_TERMINATION,

    DO_IO1_PU,
    DO_IO2_PU,
    DO_LED_green,
    DO_LED_red,

    DIO_CHANNEL_MAX
}
Dio_PinIdType;




#else
#error Multiple include of "Dio_cfg.h"
#endif 


