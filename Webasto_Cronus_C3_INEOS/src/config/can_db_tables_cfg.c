
#include "sfl_can_db_tables_data.h"
#include "can_db_tables_cfg.h"
#include "IUG_EOL_can_db_tables.h"


can_block_db_ram_typ                              can_block_db_ram_user[CAN_BLOCK_MAX + 1u];     
extern volatile const can_datenpunkt_db_const_typ can_datenpunkt_db_const[CAN_DP_MAX + 1u];      
extern volatile const can_block_db_const_typ      can_block_db_const[CAN_BLOCK_MAX + 1u];        
extern volatile const can_bus_db_const_typ        can_bus_db_const[CAN_BUS_MAX + 1u];            
extern volatile const can_gateway_db_const_typ    can_gateway_db_const[CAN_GATEWAY_DB_MAX + 1u]; 

can_block_db_ram_typ                              can_block_db_ram_eol[CAN_BLOCK_MAX_EOL + 1u];          
extern volatile const can_datenpunkt_db_const_typ can_datenpunkt_db_const_eol[CAN_DP_MAX_EOL + 1u];      
extern volatile const can_block_db_const_typ      can_block_db_const_eol[CAN_BLOCK_MAX_EOL + 1u];        
extern volatile const can_bus_db_const_typ        can_bus_db_const_eol[CAN_BUS_MAX_EOL + 1u];            
extern volatile const can_gateway_db_const_typ    can_gateway_db_const_eol[CAN_GATEWAY_DB_MAX_EOL + 1u]; 

extern volatile const can_block_db_const_typ      can_block_db_const_eol_offset[CAN_BLOCK_MAX_EOL + 1u]; 

const struct_can_db_tables_cfg ext_can_db_tables_cfg[CAN_DB_TABLES_ENUM_MAX] =
{
    {
        { &can_datenpunkt_db_const[0u]     , (uint16_t)CAN_DP_MAX             },
        { &can_block_db_const[0u]          , (uint16_t)CAN_BLOCK_MAX          },
        { &can_bus_db_const[0u]            , (uint16_t)CAN_BUS_MAX            },
        { &can_gateway_db_const[0u]        , (uint16_t)CAN_GATEWAY_DB_MAX     },
        &can_block_db_ram_user[0u],
        CAN_BUS_1
    },
    {
        { &can_datenpunkt_db_const_eol[0u] , (uint16_t)CAN_DP_MAX_EOL         },
        { &can_block_db_const_eol[0u]      , (uint16_t)CAN_BLOCK_MAX_EOL      },
        { &can_bus_db_const_eol[0u]        , (uint16_t)CAN_BUS_MAX_EOL        },
        { &can_gateway_db_const_eol[0u]    , (uint16_t)CAN_GATEWAY_DB_MAX_EOL },
        &can_block_db_ram_eol[0u],
        (can_bus_id)CAN_BUS_1_EOL
    },
    {
        { &can_datenpunkt_db_const_eol[0u]   , (uint16_t)CAN_DP_MAX_EOL         },
        { &can_block_db_const_eol_offset[0u] , (uint16_t)CAN_BLOCK_MAX_EOL      },
        { &can_bus_db_const_eol[0u]          , (uint16_t)CAN_BUS_MAX_EOL        },
        { &can_gateway_db_const_eol[0u]      , (uint16_t)CAN_GATEWAY_DB_MAX_EOL },
        &can_block_db_ram_eol[0u],
        (can_bus_id)CAN_BUS_1_EOL
    }
};

uint8_t ext_sfl_can_received_any_flag[CAN_BUS_MAX] = {0u};

uint8_t ext_sfl_can_stop_gw_unknown_ids[CAN_BUS_MAX] = {0u};

void can_db_cfg_get_rx_buf_size(uint16_t *const ptr_var)
{
    if( (NULL_PTR) != ptr_var )
    {
        *ptr_var = (CAN_RX_MSG_FULL_BUFFER_SIZE);
    }
    else
    {
    }
}


