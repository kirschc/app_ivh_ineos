#ifndef _HSD_VN7040_CFG_H_
#define _HSD_VN7040_CFG_H_

#include "hal_data_types.h"

#define HSD_VN7040_MUX_TIME_MS   100u 


typedef enum
{
    VN7040_PIN_FAULTRST   = 0u,
    VN7040_PIN_SEN            ,
    VN7040_PIN_SEL0           ,
    VN7040_PIN_SEL1           ,
    VN7040_PIN_MULTISENSE
} enum_HSD_VN7040_PINS;

typedef struct
{
    enum_HSD_VN7040_PINS pin_vn7040;
    uint8_t              pin_mcu;
} struct_hsd_vn7040_pin_cfg;

extern const struct_hsd_vn7040_pin_cfg ext_dfl_vn7040_pin_cfg[];



#endif 



