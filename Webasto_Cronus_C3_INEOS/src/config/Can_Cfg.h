
#ifndef CAN_CFG_H
#define CAN_CFG_H


#define CAN_CFG_SW_MAJOR_VERSION       1u

#define CAN_CFG_SW_MINOR_VERSION       0u

#define CAN_CFG_SW_PATCH_VERSION       0u


#define CAN_CLOCK_FREQUENCY            40000000u

#define CAN_LOOP_TIMEOUT               0xFFFFFFFFu

#define CAN_DYN_ENABLE                 0xFFFFFFFFu

#define CAN_TR_ENABLE                  0x07u

#define CAN_TR_ACTIVE                  0x01u

#define CAN_TR_DISABLE                 0x04u

#define CAN_TR_SLEEP                   0x01u

#define CAN_TR_WAKEUP                  0x01u

#define CAN_NUMBER_RR                  64u

#define CAN_HW_FILTER                  TRUE

#define CAN_BUFFER_SIZE_0              0u

#define CAN_BUFFER_SIZE_16             0x03u

#define CAN_BUFFER_SIZE_32             0x04u

#define CAN_NUMBER_OF_CHANNELS            6u

#define CAN0_RX_POLLING_MODE           FALSE
#define CAN1_RX_POLLING_MODE           FALSE
#define CAN2_RX_POLLING_MODE           FALSE
#define CAN3_RX_POLLING_MODE           FALSE
#define CAN4_RX_POLLING_MODE           FALSE
#define CAN5_RX_POLLING_MODE           FALSE

#define CAN0_ENABLE                    FALSE
#define CAN1_ENABLE                    TRUE
#define CAN2_ENABLE                    TRUE
#define CAN3_ENABLE                    TRUE
#define CAN4_ENABLE                    FALSE
#define CAN5_ENABLE                    FALSE

#define CAN0_BAUDRATE                  CAN_BAUDRATE_250
#define CAN1_BAUDRATE                  CAN_BAUDRATE_500
#define CAN2_BAUDRATE                  CAN_BAUDRATE_125
#define CAN3_BAUDRATE                  CAN_BAUDRATE_125
#define CAN4_BAUDRATE                  CAN_BAUDRATE_250
#define CAN5_BAUDRATE                  CAN_BAUDRATE_250

#define CAN0_NUMBER_TX_MESSAGES        1u
#define CAN1_NUMBER_TX_MESSAGES        2u
#define CAN2_NUMBER_TX_MESSAGES        2u
#define CAN3_NUMBER_TX_MESSAGES        2u
#define CAN4_NUMBER_TX_MESSAGES        1u
#define CAN5_NUMBER_TX_MESSAGES        1u

#define CAN0_NUMBER_TX_BUFFER          16u
#define CAN1_NUMBER_TX_BUFFER          16u
#define CAN2_NUMBER_TX_BUFFER          16u
#define CAN3_NUMBER_TX_BUFFER          16u
#define CAN4_NUMBER_TX_BUFFER          16u
#define CAN5_NUMBER_TX_BUFFER          16u


#define CAN0_NUMBER_RX_MESSAGES        1u
#define CAN1_NUMBER_RX_MESSAGES        2u
#define CAN2_NUMBER_RX_MESSAGES        2u
#define CAN3_NUMBER_RX_MESSAGES        2u
#define CAN4_NUMBER_RX_MESSAGES        1u
#define CAN5_NUMBER_RX_MESSAGES        1u

#define CAN0_NUMBER_RX_BUFFER          16u
#define CAN1_NUMBER_RX_BUFFER          16u
#define CAN2_NUMBER_RX_BUFFER          16u
#define CAN3_NUMBER_RX_BUFFER          16u
#define CAN4_NUMBER_RX_BUFFER          16u
#define CAN5_NUMBER_RX_BUFFER          16u

#define CAN0_TASK_TIME                 20u
#define CAN1_TASK_TIME                 20u
#define CAN2_TASK_TIME                 20u
#define CAN3_TASK_TIME                 20u
#define CAN4_TASK_TIME                 20u
#define CAN5_TASK_TIME                 20u

#define CAN0_RECEIVE_NOTIFICATION      NULL_PTR
#define CAN1_RECEIVE_NOTIFICATION      NULL_PTR
#define CAN2_RECEIVE_NOTIFICATION      NULL_PTR
#define CAN3_RECEIVE_NOTIFICATION      NULL_PTR
#define CAN4_RECEIVE_NOTIFICATION      NULL_PTR
#define CAN5_RECEIVE_NOTIFICATION      NULL_PTR

#define CAN0_WAKEUP_NOTIFICATION       NULL_PTR
#define CAN1_WAKEUP_NOTIFICATION       NULL_PTR
#define CAN2_WAKEUP_NOTIFICATION       NULL_PTR
#define CAN3_WAKEUP_NOTIFICATION       NULL_PTR
#define CAN4_WAKEUP_NOTIFICATION       NULL_PTR
#define CAN5_WAKEUP_NOTIFICATION       NULL_PTR

#define CAN0_BUSOFF_NOTIFICATION       NULL_PTR
#define CAN1_BUSOFF_NOTIFICATION       NULL_PTR
#define CAN2_BUSOFF_NOTIFICATION       NULL_PTR
#define CAN3_BUSOFF_NOTIFICATION       NULL_PTR
#define CAN4_BUSOFF_NOTIFICATION       NULL_PTR
#define CAN5_BUSOFF_NOTIFICATION       NULL_PTR

#define CAN0_CRC_ERROR_NOTIFICATION    NULL_PTR
#define CAN1_CRC_ERROR_NOTIFICATION    NULL_PTR
#define CAN2_CRC_ERROR_NOTIFICATION    NULL_PTR
#define CAN3_CRC_ERROR_NOTIFICATION    NULL_PTR
#define CAN4_CRC_ERROR_NOTIFICATION    NULL_PTR
#define CAN5_CRC_ERROR_NOTIFICATION    NULL_PTR

#define CAN0_ACK_ERROR_NOTIFICATION    NULL_PTR
#define CAN1_ACK_ERROR_NOTIFICATION    NULL_PTR
#define CAN2_ACK_ERROR_NOTIFICATION    NULL_PTR
#define CAN3_ACK_ERROR_NOTIFICATION    NULL_PTR
#define CAN4_ACK_ERROR_NOTIFICATION    NULL_PTR
#define CAN5_ACK_ERROR_NOTIFICATION    NULL_PTR

#define CAN0_TX_ERROR_NOTIFICATION     NULL_PTR
#define CAN1_TX_ERROR_NOTIFICATION     NULL_PTR
#define CAN2_TX_ERROR_NOTIFICATION     NULL_PTR
#define CAN3_TX_ERROR_NOTIFICATION     NULL_PTR
#define CAN4_TX_ERROR_NOTIFICATION     NULL_PTR
#define CAN5_TX_ERROR_NOTIFICATION     NULL_PTR

#define CAN0_RX_ERROR_NOTIFICATION     NULL_PTR
#define CAN1_RX_ERROR_NOTIFICATION     NULL_PTR
#define CAN2_RX_ERROR_NOTIFICATION     NULL_PTR
#define CAN3_RX_ERROR_NOTIFICATION     NULL_PTR
#define CAN4_RX_ERROR_NOTIFICATION     NULL_PTR
#define CAN5_RX_ERROR_NOTIFICATION     NULL_PTR

#define CAN0_INT_PRIORITY_ERROR        0x07u
#define CAN1_INT_PRIORITY_ERROR        0x07u
#define CAN2_INT_PRIORITY_ERROR        0x07u
#define CAN3_INT_PRIORITY_ERROR        0x07u
#define CAN4_INT_PRIORITY_ERROR        0x07u
#define CAN5_INT_PRIORITY_ERROR        0x07u

#define CAN0_INT_PRIORITY_WAKEUP       0x07u
#define CAN1_INT_PRIORITY_WAKEUP       0x07u
#define CAN2_INT_PRIORITY_WAKEUP       0x07u
#define CAN3_INT_PRIORITY_WAKEUP       0x07u
#define CAN4_INT_PRIORITY_WAKEUP       0x07u
#define CAN5_INT_PRIORITY_WAKEUP       0x07u

#define CAN0_INT_PRIORITY_RECEIVE      0x07u
#define CAN1_INT_PRIORITY_RECEIVE      0x07u
#define CAN2_INT_PRIORITY_RECEIVE      0x07u
#define CAN3_INT_PRIORITY_RECEIVE      0x07u
#define CAN4_INT_PRIORITY_RECEIVE      0x07u
#define CAN5_INT_PRIORITY_RECEIVE      0x07u

#define CAN0_INT_PRIORITY_TRANSMIT     0x07u
#define CAN1_INT_PRIORITY_TRANSMIT     0x07u
#define CAN2_INT_PRIORITY_TRANSMIT     0x07u
#define CAN3_INT_PRIORITY_TRANSMIT     0x07u
#define CAN4_INT_PRIORITY_TRANSMIT     0x07u
#define CAN5_INT_PRIORITY_TRANSMIT     0x07u

#define CAN0_MASK1_CONFIG              0x1FFFFFFFu
#define CAN1_MASK1_CONFIG              0x1FFFFFFFu
#define CAN2_MASK1_CONFIG              0x1FFFFFFFu
#define CAN3_MASK1_CONFIG              0x1FFFFFFFu
#define CAN4_MASK1_CONFIG              0x1FFFFFFFu
#define CAN5_MASK1_CONFIG              0x1FFFFFFFu

#define CAN0_MASK2_CONFIG              0x1FFFFFFFu
#define CAN1_MASK2_CONFIG              0x1FFFFFFFu
#define CAN2_MASK2_CONFIG              0x1FFFFFFFu
#define CAN3_MASK2_CONFIG              0x1FFFFFFFu
#define CAN4_MASK2_CONFIG              0x1FFFFFFFu
#define CAN5_MASK2_CONFIG              0x1FFFFFFFu

#define CAN0_MASK3_CONFIG              0x1FFFFFFFu
#define CAN1_MASK3_CONFIG              0x1FFFFFFFu
#define CAN2_MASK3_CONFIG              0x1FFFFFFFu
#define CAN3_MASK3_CONFIG              0x1FFFFFFFu
#define CAN4_MASK3_CONFIG              0x1FFFFFFFu
#define CAN5_MASK3_CONFIG              0x1FFFFFFFu

#define CAN0_MASK4_CONFIG              0x1FFFFFFFu
#define CAN1_MASK4_CONFIG              0x1FFFFFFFu
#define CAN2_MASK4_CONFIG              0x1FFFFFFFu
#define CAN3_MASK4_CONFIG              0x1FFFFFFFu
#define CAN4_MASK4_CONFIG              0x1FFFFFFFu
#define CAN5_MASK4_CONFIG              0x1FFFFFFFu




void Can_Cfg_Init(void);

void Can_Wait10us(void);



#else
#error Multiple include of "Can_Cfg.h"
#endif 


