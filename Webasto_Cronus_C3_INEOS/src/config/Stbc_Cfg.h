#ifndef STBC_CFG_H_
#define STBC_CFG_H_


#include "Stbc.h"
#include "Tau_Cfg.h"


#define SYSTICK_TIMER_IR_NO              134u
#define SYSTICK_TIMER_RTOS_IR_NO         29u


#define PROTECTED_WRITE(preg, pstatus, reg, value)    do{                               \
                                                      (preg)=WRITE_PROTECTION_CODE;     \
                                                      (reg)=(value);                    \
                                                      (reg)=~(value);                   \
                                                      (reg)=(value);                    \
                                                      }while((pstatus)==1u)





typedef enum
{
    WAKE_UP_FACTOR_TNMI                        = 0u,  
    WAKE_UP_FACTOR_WDTA0NMI                        ,  
    WAKE_UP_FACTOR_INTLVIL                         ,  
    WAKE_UP_FACTOR_INTP0                           ,
    WAKE_UP_FACTOR_INTP1                           ,
    WAKE_UP_FACTOR_INTP2                           ,
    WAKE_UP_FACTOR_INTWDTA0                        ,
    WAKE_UP_FACTOR_INTP3                           ,
    WAKE_UP_FACTOR_INTP4                           ,
    WAKE_UP_FACTOR_INTP5                           ,
    WAKE_UP_FACTOR_INTP10                          ,
    WAKE_UP_FACTOR_INTP11                          ,
    WAKE_UP_FACTOR_WUTRG1                          ,  
    WAKE_UP_FACTOR_INTTAUJ0I0_1                    ,  
    WAKE_UP_FACTOR_INTTAUJ0I1_1                    ,  
    WAKE_UP_FACTOR_INTTAUJ0I2_1                    ,  
    WAKE_UP_FACTOR_INTTAUJ0I3_1                    ,  
    WAKE_UP_FACTOR_WUTRG0                          ,  
    WAKE_UP_FACTOR_INTP6                           ,
    WAKE_UP_FACTOR_INTP7                           ,
    WAKE_UP_FACTOR_INTP8                           ,
    WAKE_UP_FACTOR_INTP12                          ,
    WAKE_UP_FACTOR_INTP9                           ,
    WAKE_UP_FACTOR_INTP13                          ,
    WAKE_UP_FACTOR_INTP14                          ,
    WAKE_UP_FACTOR_INTP15                          ,
    WAKE_UP_FACTOR_INTRTCA01S_1                    ,  
    WAKE_UP_FACTOR_INTRTCA0AL_1                    ,  
    WAKE_UP_FACTOR_INTRTCA0R_1                     ,  
    WAKE_UP_FACTOR_INTDCUTDI                       ,
    WAKE_UP_FACTOR_INTKR0                          ,
    WAKE_UP_FACTOR_INTRCANGRECC                    ,
    WAKE_UP_FACTOR_INTRCAN0REC                     ,
    WAKE_UP_FACTOR_INTRCAN1REC                     ,
    WAKE_UP_FACTOR_INTRCAN2REC                     ,
    WAKE_UP_FACTOR_INTRCAN3REC                     ,
    WAKE_UP_FACTOR_INTRCAN4REC                     ,
    WAKE_UP_FACTOR_INTRCAN5REC                     ,
    WAKE_UP_FACTOR_INTADCA0I0                      ,
    WAKE_UP_FACTOR_INTADCA0I1                      ,
    WAKE_UP_FACTOR_INTADCA0I2                      ,
    WAKE_UP_FACTOR_INTRLIN30                       ,
    WAKE_UP_FACTOR_INTTAUJ0I0_2                    ,
    WAKE_UP_FACTOR_INTTAUJ0I1_2                    ,
    WAKE_UP_FACTOR_INTTAUJ0I2_2                    ,
    WAKE_UP_FACTOR_INTTAUJ0I3_2                    ,
    WAKE_UP_FACTOR_INTRLIN31                       ,
    WAKE_UP_FACTOR_INTRLIN32                       ,
    WAKE_UP_FACTOR_INTRTCA01S_2                    ,
    WAKE_UP_FACTOR_INTRTCA0AL_2                    ,
    WAKE_UP_FACTOR_INTRTCA0R_2                     ,
    WAKE_UP_FACTOR_INTRLIN33                       ,
    WAKE_UP_FACTOR_INTRLIN34                       ,
    WAKE_UP_FACTOR_INTRLIN35                       ,
    WAKE_UP_FACTOR_MAX

}enum_WAKE_UP_FACTOR;



#endif


