

#ifndef FLS_CFG_H
#define FLS_CFG_H


#include "Std_Types.h"


#define FLS_AC_LOAD_ON_JOB_START       STD_ON
#define FLS_DEM_ERROR_DETECT           STD_OFF
#define FLS_CANCEL_API                 STD_ON
#define FLS_SET_MODE_API               STD_ON

#define FLS_VERIFY_ERASE_API           STD_ON
#define FLS_VERIFY_WRITE_API           STD_ON

#define FLS_WRITECYCLES_FOR_20YEARS    1000u
#define FLS_WRITECYCLES_FOR_15YEARS    5000u
#define FLS_WRITECYCLES_FOR_5YEARS     15000u

#define FLS_CALL_CYCLE                 20u
#define FLS_DEFAULT_MODE               MEMIF_MODE_FAST
#define FLS_JOB_END_NOTIFICATION       Fee_JobEndNotification
#define FLS_JOB_ERROR_NOTIFICATION     Fee_JobErrorNotification
#define FLS_MAX_READ_FAST_MODE         0x00000800u
#define FLS_MAX_READ_NORMAL_MODE       0x00000800u
#define FLS_MAX_WRITE_FAST_MODE        0x00000800u
#define FLS_MAX_WRITE_NORMAL_MODE      0x00000800u

#define FLS_CPU_FREQUENCY_MHZ          80u                             

#define FLS_WRITE_ADDRESS              0x4000u



Std_ReturnType Fls_WaitForData(void);


Std_ReturnType Fls_WriteData(uint16_t addr, uint16_t len, const uint8_t *data);

Std_ReturnType Fls_ReadData(uint16_t addr, uint16_t len, uint8_t *data);





#else
#error Multiple include of "Fls_Cfg.h"
#endif 


