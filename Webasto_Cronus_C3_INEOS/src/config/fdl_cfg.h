#ifdef ENABLE_QAC_TEST
    #pragma PRQA_MESSAGES_OFF 0292
#endif


#ifdef ENABLE_QAC_TEST
    #pragma PRQA_MESSAGES_ON 0292 
#endif


#ifndef FDL_CFG_H
#define FDL_CFG_H


    #define FDL_CFG_E1X_P1X_PLATFORM

    void FDL_User_CriticalSetionBegin( void );
    void FDL_User_CriticalSetionEnd( void );

    #define FDL_CRITICAL_SECTION_BEGIN     FDL_User_CriticalSetionBegin();      
    #define FDL_CRITICAL_SECTION_END       FDL_User_CriticalSetionEnd();        


#endif 


