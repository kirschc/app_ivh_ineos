#ifndef _BGM111_CFG_H_
#define _BGM111_CFG_H_

#include "gatt_db.h"
#include "bgm111_protocol_base.h"
#include "IUG_cfg.h"

#define BGM111_INIT_UNDONE          0x00u 
#define BGM111_INIT_DONE            0x01u 
#define BGM111_INIT_FAILED          0x02u 

#define BGM111_INIT_TRG_BY_NONE     0x00u 
#define BGM111_INIT_TRG_BY_CLIENT   0x10u 
#define BGM111_INIT_TRG_BY_HOST     0x20u 

#define BGM111_INIT_SET_DONE(x)     bgm111_config_set_initialized( (BGM111_INIT_DONE)  , x ); 
#define BGM111_INIT_SET_FAIL(x)     bgm111_config_set_initialized( (BGM111_INIT_FAILED), x ); 
#define BGM111_INIT_SET_TRG(x)      bgm111_config_set_initialized( (BGM111_INIT_UNDONE), x ); 

#define BGM111_GET_INIT_STATE(x)    ( x & 0x0Fu ) 
#define BGM111_GET_INIT_CALLER(x)   ( x & 0xF0u ) 

#ifdef IUG_DEV_BGM111_INIT_DEBUG_EN
    #define BGM111_DEBUG_ARY_SIZE   10u 
#endif

#define BGM111_RX_FIFO_SIZE                       500u
#define BGM111_MAX_LENGTH_PAIRING_KEY             6u       
#define BGM111_DEFAULT_PARING_KEY                 12345u   
#define BGM111_MAX_LENGTH_PRODUCT_NAME            50u      
#define BGM111_MAX_LENGTH_ADV_NAME_IPD            ( (BGM111_LIMIT_MAX_ADV_LENGTH) - (BGM111_MAX_LENGTH_ADV_RESERVED) )
#define BGM111_MAX_LENGTH_ADV_RESERVED             6u      
#define IUG_ECU_SERIAL_NUMBER_LAST_4_DIGIT_START   3u      
#define BGM111_ENABLE_PAIRING_STORAGE              1u

#define BGM111_RESET_OVER_BLE_EN


#ifdef BGM111_CAN_TEST_CMDS
    #include "sfl_can_db.h"     
    #include "app_usercode.h"   

    #define BGM111_TEST_CMDS_CAN_HANDLE CAN3    
#define BGM111_CAN_RX_ID        0x00A5A5A5      
#define BGM111_CAN_TX_RESULT    0x00A5A5A6      
#define BGM111_CAN_TX_ANSWER    0x00A5A5A7      


    void bgm111_can_cmd_test(struct_hal_can_frame msg);
#endif




#define BGM111_ENABLE_USER_CHARACTERISTICS


#define BGM111_GATT_CHARACTERISTIC_USER_RX  gattdb_client_requested_send
#define BGM111_GATT_CHARACTERISTIC_USER_TX  gattdb_server_send_notification

#define BGM111_GATT_CHARACTERISTIC_CAN_RX   gattdb_ble_to_can
#define BGM111_GATT_CHARACTERISTIC_CAN_TX   gattdb_can_to_ble

#define BGM111_GATT_CHARACTERISTIC_MRS_RX   gattdb_device_data
#define BGM111_GATT_CHARACTERISTIC_MRS_TX   gattdb_device_resp
#define BGM111_GATT_CHARACTERISTIC_OTA      gattdb_ota_control


#define BGM111_GATT_ADVERTISING_NAME	    gattdb_advertising_name
#define BGM111_GATT_MANUFACTURER_NAME       gattdb_manufacturer_name_string
#define BGM111_GATT_SERIAL_NUMBER           gattdb_serial_number_string
#define BGM111_GATT_MODEL_NUMBER            gattdb_product_name_string
#define BGM111_GATT_HW_REVISION             gattdb_hardware_revision_string
#define BGM111_GATT_SW_VERSION              gattdb_software_revision_string
#define BGM111_GATT_SEND_NOTIFICATION       gattdb_server_send_notification

#define BGM111_DEFAULT_SOFTWARE_VERSION_STRING      (SW_VERSION_NAME)
#define BGM111_DEFAULT_MANUFACTURER_NAME_STRING     "Webasto Thermo & Comfort SE"
#define BGM111_DEFAULT_DATAPOINT_VALUE              "NO READ ALLOWED"
#define BGM111_LIMIT_CUSTOM_ADV_INTERVAL_FAST       160u    
#define BGM111_LIMIT_CUSTOM_ADV_INTERVAL_SLOW       6400u   

#define BGM111_CUSTOM_CHECK_WAKE_PERIOD_MS          100u                                  
#define BGM111_WAKE_UP_DELAY_TIME_MS                2000u                                 
#define BGM111_INIT_ERR_RETRY_MAX                   10u                                   
#define BGM111_INIT_ERR_TIMEOUT_MS                  (BGM111_WAKE_UP_DELAY_TIME_MS) + 500u 
#define BGM111_INIT_ERR_STEP_TIMEOUT_MS             (BGM111_INIT_ERR_TIMEOUT_MS) + 5u     

#define BGM111_FLASH_STATE_NONE                     0x00u 
#define BGM111_FLASH_STATE_APPL                     0x01u 
#define BGM111_FLASH_STATE_GECKO                    0x02u 




typedef enum
{
    BGM111_INIT_STEPS_PREPARE              = 0u, 
    BGM111_INIT_STEPS_CHECK_WAKE               , 
    BGM111_INIT_STEPS_SET_HW_SETUP             , 
    BGM111_INIT_STEPS_SET_PASSKEY              , 
    BGM111_INIT_STEPS_SET_PRODUCT_NAME         , 
    BGM111_INIT_STEPS_SET_SERIAL_NUMBER        , 
    BGM111_INIT_STEPS_SET_ADV_NAME             , 
    BGM111_INIT_STEPS_SET_SW_VERSION           , 
    BGM111_INIT_STEPS_SET_HW_REVISION          , 
    BGM111_INIT_STEPS_SET_MANUF_NAME           , 
    BGM111_INIT_STEPS_SET_USER_VALUE           , 
    BGM111_INIT_STEPS_SET_CAN_VALUE            , 
    BGM111_INIT_STEPS_SET_SECURITY_CONFIG      , 
    BGM111_INIT_STEPS_SET_ADV_PARAMS           , 
    BGM111_INIT_STEPS_ADVERTISE                , 
    BGM111_INIT_STEPS_SEND_STARTUP_MSG         , 
    BGM111_INIT_STEPS_DONE                       
} enum_BGM111_INIT_STEPS_T;

typedef enum
{
    BGM111_INIT_ERR_NONE       = 0u, 
    BGM111_INIT_ERR_DO_RESET       , 
    BGM111_INIT_ERR_UNDO_RESET     , 
    BGM111_INIT_ERR_ACTIVE           
} enum_BGM111_INIT_ERR_STATES;

#ifdef IUG_DEV_BGM111_INIT_DEBUG_EN
typedef struct
{
    uint32_t deb_init_flg[ (BGM111_DEBUG_ARY_SIZE) ];
} struct_ble_debug_data;
#endif

typedef struct
{
    uint8_t                        init_flg;
    const enum_BGM111_INIT_STEPS_T *ptr_init_setup;
    enum_BGM111_INIT_STEPS_T       init_state;
    uint8_t                        init_state_lock;
    uint32_t                       init_retry_timer;
    enum_BGM111_INIT_ERR_STATES    init_err_state;
    uint8_t                        init_err_count_live;
    uint8_t                        init_err_count_last;
    uint32_t                       init_timer;
} struct_ble_init_data;

typedef struct
{
    uint8_t ary_ecu_serial_number_digits[ ( (IUG_ECU_MAX_CHARS_SERIAL_NUMBER) + 1u ) ];
    uint8_t ary_para_ecu_trade_name[ (BGM111_MAX_LENGTH_ADV_NAME_IPD) ];
    uint8_t ary_cust_adv_name[ (BGM111_LIMIT_MAX_ADV_LENGTH) ];
    uint8_t size_of_ptr_ary_para_ecu_trade_name;
    uint8_t size_of_ptr_ary_para_ecu_trade_name_temp;
} struct_ble_cust_data;

typedef struct
{
    const uint8_t *ptr_cust_ble_pairing_key_error;
    const uint8_t *ptr_cust_ble_status_request_err_cnt;
    const uint8_t *ptr_ary_cust_ble_pairing_key;
} struct_ble_shared_data;

typedef struct
{
#ifdef IUG_DEV_BGM111_INIT_DEBUG_EN
    struct_ble_debug_data  debug_data;
#endif
    struct_ble_init_data   init_data;
    struct_ble_cust_data   cust_data;
    struct_ble_shared_data shared_data;
} struct_ble_data;

extern uint8_t ext_cust_ble_pairing_key[ (BGM111_MAX_LENGTH_PAIRING_KEY) ];
extern uint8_t ext_cust_ble_pairing_key_error;
extern uint8_t ext_cust_ble_status_request_err_cnt;


uint8_t bgm111_config_get_initialized(void);

void bgm111_config_set_initialized(const uint8_t init_val, const uint8_t init_source);

void bgm111_init_cyclic(void);

static void bgm111_init_setup(const uint8_t trg_source);

static void bgm111_handle_init_error(void);

static uint32_t bgm111_create_cust_pairing_key(const uint8_t *const ptr_ary_key);

enum_BGM111_INIT_ERR_STATES bgm111_get_init_err_state(void);

void bgm111_monitor_advertising_available_time_cyclic(void);

void bgm111_enable_ble_adv_avail_trg(void);

void bgm111_disable_ble_adv_avail_trg(void);


#if( ( (BGM111_CUSTOM_CHECK_WAKE_PERIOD_MS)  <  50) || \
     ( (BGM111_CUSTOM_CHECK_WAKE_PERIOD_MS)  > 200)     )
     #error "Define 'BGM111_CUSTOM_CHECK_WAKE_PERIOD_MS' to a proper value!"
#endif


#endif 



