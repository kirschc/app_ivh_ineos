#ifndef _HAL_MEM_CFG_H_
#define _HAL_MEM_CFG_H_

#include "hal_data_types.h"


boolean Mem_Cfg_Is_RAM_address( uint8_t * const mem_addr );
boolean Mem_Cfg_Is_ROM_address( uint8_t * const mem_addr );


#endif 

