
#ifndef ADC_CFG_H
#define ADC_CFG_H



#define ADC_CFG_SW_MAJOR_VERSION             1u

#define ADC_CFG_SW_MINOR_VERSION             0u

#define ADC_CFG_SW_PATCH_VERSION             0u


#define ADC_NOT_IMPLEMENTED                  0u

#define ADC_CONFIGURED_GROUPS                6u

#define ADC_GRP0_HW_UNIT                     ADC_HW_UNIT_0

#define ADC_GRP0_START                       0u

#define ADC_GRP0_END                         13u

#define ADC_GRP0_NOTIFICATION                NULL_PTR

#define ADC_GRP0_TRIGGER_SOURCE              ADC_TRIGGER_SRC_SW

#define ADC_GRP00_SELECT_TRIGGER             (Adc_HWTriggerSourceType)ADC_TRIGGER_DEFAULT

#define ADC_GRP0_CONVERSION_MODE             ADC_CONV_MODE_ONESHOT

#define ADC_GRP0_RESOLUTION                  ADC_RESOLUTION_12BIT

#define ADC_GRP0_UPPER_LIMIT                 (uint32_t)  0x0FFFu

#define ADC_GRP0_LOWER_LIMIT                 (uint16_t)  0x0000u


#define ADC_GRP1_HW_UNIT                     ADC_HW_UNIT_0

#define ADC_GRP1_START                       14u

#define ADC_GRP1_END                         14u

#define ADC_GRP1_NOTIFICATION                NULL_PTR

#define ADC_GRP1_TRIGGER_SOURCE              ADC_TRIGGER_SRC_HW

#define ADC_GRP01_SELECT_TRIGGER             (Adc_HWTriggerSourceType)ADC_TRIGGER_002 

#define ADC_GRP1_CONVERSION_MODE             ADC_CONV_MODE_ONESHOT

#define ADC_GRP1_RESOLUTION                  ADC_RESOLUTION_12BIT

#define ADC_GRP1_UPPER_LIMIT                 (uint32_t)0xFFFu

#define ADC_GRP1_LOWER_LIMIT                 (uint16_t)0x000u


#define ADC_GRP2_HW_UNIT                     ADC_HW_UNIT_0

#define ADC_GRP2_START                       15u

#define ADC_GRP2_END                         15u

#define ADC_GRP2_NOTIFICATION                NULL_PTR

#define ADC_GRP2_TRIGGER_SOURCE              ADC_TRIGGER_SRC_HW

#define ADC_GRP02_SELECT_TRIGGER             (Adc_HWTriggerSourceType)ADC_TRIGGER_003 

#define ADC_GRP2_CONVERSION_MODE             ADC_CONV_MODE_ONESHOT

#define ADC_GRP2_RESOLUTION                  ADC_RESOLUTION_12BIT

#define ADC_GRP2_UPPER_LIMIT                 (uint32_t)0xFFFu

#define ADC_GRP2_LOWER_LIMIT                 (uint16_t)0x000u

#define ADC_GRP3_HW_UNIT                     ADC_HW_UNIT_1

#define ADC_GRP3_START                       0u

#define ADC_GRP3_END                         15u

#define ADC_GRP3_NOTIFICATION                NULL_PTR

#define ADC_GRP3_TRIGGER_SOURCE              ADC_TRIGGER_SRC_SW

#define ADC_GRP03_SELECT_TRIGGER             (Adc_HWTriggerSourceType) ADC_TRIGGER_DEFAULT

#define ADC_GRP3_CONVERSION_MODE             ADC_CONV_MODE_ONESHOT

#define ADC_GRP3_RESOLUTION                  ADC_RESOLUTION_12BIT

#define ADC_GRP3_UPPER_LIMIT                 (uint32_t)0xFFFu

#define ADC_GRP3_LOWER_LIMIT                 (uint16_t)0x000u


#define ADC_GRP4_HW_UNIT                     ADC_HW_UNIT_1

#define ADC_GRP4_START                       0u

#define ADC_GRP4_END                         5u

#define ADC_GRP4_NOTIFICATION                NULL_PTR

#define ADC_GRP4_TRIGGER_SOURCE              ADC_TRIGGER_SRC_SW

#define ADC_GRP04_SELECT_TRIGGER             (Adc_HWTriggerSourceType) ADC_TRIGGER_DEFAULT

#define ADC_GRP4_CONVERSION_MODE             ADC_CONV_MODE_ONESHOT

#define ADC_GRP4_RESOLUTION                  ADC_RESOLUTION_12BIT

#define ADC_GRP4_UPPER_LIMIT                 (uint32_t)0xFFFu

#define ADC_GRP4_LOWER_LIMIT                 (uint16_t)0x000u


#define ADC_GRP5_HW_UNIT                     ADC_HW_UNIT_1

#define ADC_GRP5_START                       0u

#define ADC_GRP5_END                         5u

#define ADC_GRP5_NOTIFICATION                NULL_PTR

#define ADC_GRP5_TRIGGER_SOURCE              ADC_TRIGGER_SRC_SW

#define ADC_GRP05_SELECT_TRIGGER             (Adc_HWTriggerSourceType)ADC_TRIGGER_DEFAULT

#define ADC_GRP5_CONVERSION_MODE             ADC_CONV_MODE_ONESHOT

#define ADC_GRP5_RESOLUTION                  ADC_RESOLUTION_12BIT

#define ADC_GRP5_UPPER_LIMIT                 (uint32_t)0xFFFu

#define ADC_GRP5_LOWER_LIMIT                 (uint16_t)0x000u


#define ADC_REGISTER_CONFIG_ADCA0ADCR        ((uint32_t)0x0000u)

#define ADC_REGISTER_CONFIG_ADCA0SMPCR       ((uint32_t)0x0000u)

#define ADC_REGISTER_CONFIG_ADCA0ULLMTBR0    ((uint32_t)0xfff00000u)

#define ADC_REGISTER_CONFIG_ADCA0ULLMTBR1    ((uint32_t)0xfff00000u)

#define ADC_REGISTER_CONFIG_ADCA0ULLMTBR2    ((uint32_t)0xfff00000u)

#define ADC_REGISTER_CONFIG_ADCA0SGCR1       ((uint32_t)0x00000000u)

#define ADC_REGISTER_CONFIG_ADCA0SGCR2       ((uint32_t)0x00000000u)

#define ADC_REGISTER_CONFIG_ADCA0SGCR3       ((uint32_t)0x00000000u)

#define ADC_REGISTER_CONFIG_ADCA0SGVCSP1     ((uint32_t)0x00000000u)

#define ADC_REGISTER_CONFIG_ADCA0SGVCSP2     ((uint32_t)0x00000000u)

#define ADC_REGISTER_CONFIG_ADCA0SGVCSP3     ((uint32_t)0x00000000u)

#define ADC_REGISTER_CONFIG_ADCA0SGVCEP1     ((uint32_t)0x00000000u)

#define ADC_REGISTER_CONFIG_ADCA0SGVCEP2     ((uint32_t)0x00000000u)

#define ADC_REGISTER_CONFIG_ADCA0SGVCEP3     ((uint32_t)0x00000000u)

#define ADC_REGISTER_CONFIG_ADCA0SGSTCR1     ((uint32_t)0x00000000u)

#define ADC_REGISTER_CONFIG_ADCA0SGSTCR2     ((uint32_t)0x00000000u)

#define ADC_REGISTER_CONFIG_ADCA0SGSTCR3     ((uint32_t)0x00000000u)


#define ADC_REGISTER_CONFIG_ADCA1ADCR        ((uint32_t)0x0000u)

#define ADC_REGISTER_CONFIG_ADCA1SMPCR       ((uint32_t)0x0000u)

#define ADC_REGISTER_CONFIG_ADCA1ULLMTBR0    ((uint32_t)0xfff00000u)

#define ADC_REGISTER_CONFIG_ADCA1ULLMTBR1    ((uint32_t)0xfff00000u)

#define ADC_REGISTER_CONFIG_ADCA1ULLMTBR2    ((uint32_t)0xfff00000u)

#define ADC_REGISTER_CONFIG_ADCA1SGCR1       ((uint32_t)0x00000000u)

#define ADC_REGISTER_CONFIG_ADCA1SGCR2       ((uint32_t)0x00000000u)

#define ADC_REGISTER_CONFIG_ADCA1SGCR3       ((uint32_t)0x00000000u)

#define ADC_REGISTER_CONFIG_ADCA1SGVCSP1     ((uint32_t)0x00000000u)

#define ADC_REGISTER_CONFIG_ADCA1SGVCSP2     ((uint32_t)0x00000000u)

#define ADC_REGISTER_CONFIG_ADCA1SGVCSP3     ((uint32_t)0x00000000u)

#define ADC_REGISTER_CONFIG_ADCA1SGVCEP1     ((uint32_t)0x00000000u)

#define ADC_REGISTER_CONFIG_ADCA1SGVCEP2     ((uint32_t)0x00000000u)

#define ADC_REGISTER_CONFIG_ADCA1SGVCEP3     ((uint32_t)0x00000000u)

#define ADC_REGISTER_CONFIG_ADCA1SGSTCR1     ((uint32_t)0x00000000u)

#define ADC_REGISTER_CONFIG_ADCA1SGSTCR2     ((uint32_t)0x00000000u)

#define ADC_REGISTER_CONFIG_ADCA1SGSTCR3     ((uint32_t)0x00000000u)


typedef enum
{
    AI_KL15 		    		= 0u,
    AI_KL30	    					,
    ADCA0I2					    	,
    AI_ANA						    ,
    AI_12V_OUT					    ,
    AI_PWM_PEAK_DETECT  		    ,
    AI_SENSE_PANEL				    ,
    AI_PORT_1				        ,
    AI_PUSH_BUTTON 				    ,
    AI_PWM_IN					    ,
	AI_PORT_2						,
	AI_PORT_3						,
	AI_PORT_4						,
	AI_LIN3 						,
	AI_INT_PWM 						,
	AI_INT_IO3  					,
	ADCA1I00						,
	ADCA1I01						,
	AI_PIGGY						,
	ADCA1I03						,
	ADCA1I04						,
	ADCA1I05						,
	ADCA1I06						,
	ADCA1I07						,
	ADCA1I08						,
	ADCA1I09						,
	ADCA1I10						,
	ADCA1I11						,
	ADCA1I12						,
	ADCA1I13						,
	ADCA1I14						,
	ADCA1I15						,
	ADCA1I16						,
	ADCA1I17						,
	ADCA1I18						,
	ADCA1I19						,
	ADCA1I20						,
	ADCA1I21						,
	ADCA1I22						,
	ADCA1I23						,
	ADCA1I24						,
	ADCA1I25						,
	ADCA1I26						,
	ADCA1I27						,
	ADC_CHANNEL_MAX

} AdcPinTypes;


typedef enum
{
   ADC_CGROUP_00 = 0x00u,
   ADC_CGROUP_01 = 0x01u,
   ADC_CGROUP_02 = 0x02u,
   ADC_CGROUP_03 = 0x03u,
   ADC_CGROUP_04 = 0x04u,
   ADC_CGROUP_05 = 0x05u,
}
Adc_DefinedGroupsType;




#else
#error Multiple include of "Adc_Cfg.h"
#endif 


