

#ifndef SCI_CFG_H
#define SCI_CFG_H

#include "hal_sci.h"
#include "WBusAdjust.h"
#include "WBusConstant.h"


#define SCI_CFG_SW_MAJOR_VERSION             1u

#define SCI_CFG_SW_MINOR_VERSION             0u

#define SCI_CFG_SW_PATCH_VERSION             0u




#define SCI_NUMBER_OF_CHANNELS               6u 

#define SCI_NUMBER_OF_HW_CHANNELS           (SCI_NUMBER_OF_CHANNELS)
typedef enum
{
    SCI_HW_CHANNEL_0 = 0,       
    SCI_HW_CHANNEL_1,           
    SCI_HW_CHANNEL_2,           
    SCI_HW_CHANNEL_3,           
    SCI_HW_CHANNEL_4,           
    SCI_HW_CHANNEL_5,           
    SCI_HW_CHANNEL_ENUM_MAX     
} enum_SCI_HW_CHANNEL_INDEX_t;


#if 0
#define SCI_HW_CHANNEL_0                     0u
#define SCI_HW_CHANNEL_1                     1u
#define SCI_HW_CHANNEL_2                     2u
#define SCI_HW_CHANNEL_3                     3u
#define SCI_HW_CHANNEL_4                     4u
#define SCI_HW_CHANNEL_5                     5u
#endif

#ifdef WBUS_DEV_RLIN30UART_EN
    #define SCI_SW_CHANNEL_FOR_HW_CHANNEL0       255u   
#else
    #define SCI_SW_CHANNEL_FOR_HW_CHANNEL0       3u
#endif

#ifdef WBUS_DEV_RLIN31UART_EN
    #define SCI_SW_CHANNEL_FOR_HW_CHANNEL1       0u
#else
    #define SCI_SW_CHANNEL_FOR_HW_CHANNEL1       4u
#endif

#ifdef WBUS_DEV_RLIN32UART_EN
    #define SCI_SW_CHANNEL_FOR_HW_CHANNEL2      255u   
#else
    #define SCI_SW_CHANNEL_FOR_HW_CHANNEL2      5u
#endif    

#ifdef WBUS_DEV_RLIN33UART_EN
    #define SCI_SW_CHANNEL_FOR_HW_CHANNEL3      1u 
#else
    #define SCI_SW_CHANNEL_FOR_HW_CHANNEL3      255u   
#endif    


#ifdef WBUS_DEV_RLIN34UART_EN
    #define SCI_SW_CHANNEL_FOR_HW_CHANNEL4      0u 
#else
    #define SCI_SW_CHANNEL_FOR_HW_CHANNEL4      255u   
#endif    

#ifdef WBUS_DEV_RLIN35UART_EN
    #define SCI_SW_CHANNEL_FOR_HW_CHANNEL5      255u   
#else
    #define SCI_SW_CHANNEL_FOR_HW_CHANNEL5      2u 
#endif 


#define SCI_UART_CLOCK_FREQUENCY             40000000u

#define SCI_RECEIVE_NOTIFICATION_WBUS        WBusDriver_ReceiveISRhandler      
#define SCI_RECEIVE_NOTIFICATION_BLUETOOTH   (NULL_PTR) 
#define SCI_RECEIVE_NOTIFICATION_PIGGY       (NULL_PTR) 
#define SCI_RECEIVE_NOTIFICATION_DEBUG_4     (NULL_PTR) 
#define SCI_RECEIVE_NOTIFICATION_DEBUG_5     (NULL_PTR) 

#define SCI_TRANSMIT_NOTIFICATION_WBUS       WBusDriver_TransmissionISRhandler 
#define SCI_TRANSMIT_NOTIFICATION_BLUETOOTH  (NULL_PTR)  
#define SCI_TRANSMIT_NOTIFICATION_PIGGY      (NULL_PTR)  
#define SCI_TRANSMIT_NOTIFICATION_DEBUG_4    (NULL_PTR)  
#define SCI_TRANSMIT_NOTIFICATION_DEBUG_5    (NULL_PTR)  

#define SCI_ERROR_NOTIFICATION_WBUS          WBusDriver_StatusISRhandler       
#define SCI_ERROR_NOTIFICATION_BLUETOOTH     (NULL_PTR)  
#define SCI_ERROR_NOTIFICATION_PIGGY         (NULL_PTR)  
#define SCI_ERROR_NOTIFICATION_DEBUG_4       (NULL_PTR)  
#define SCI_ERROR_NOTIFICATION_DEBUG_5       (NULL_PTR)  


#define SCI_LOOP_TIMEOUT                   1000u



typedef enum
{
    #ifdef WBUS_DEV_RLIN30UART_EN
    WBUS_DEV_PRP_REG_SET_RLIN30   = (SCI_HW_CHANNEL_0), 
    #else
    WBUS_DEV_PRP_REG_DO_NOT_USE_0 = (SCI_HW_CHANNEL_0), 
    #endif
    #ifdef WBUS_DEV_RLIN31UART_EN
    WBUS_DEV_PRP_REG_SET_RLIN31   = (SCI_HW_CHANNEL_1),
    #else
    WBUS_DEV_PRP_REG_DO_NOT_USE_1 = (SCI_HW_CHANNEL_1),
    #endif
    #ifdef WBUS_DEV_RLIN32UART_EN
    WBUS_DEV_PRP_REG_SET_RLIN32   = (SCI_HW_CHANNEL_2),
    #else
    WBUS_DEV_PRP_REG_DO_NOT_USE_2 = (SCI_HW_CHANNEL_2),
    #endif
    #ifdef WBUS_DEV_RLIN33UART_EN
    WBUS_DEV_PRP_REG_SET_RLIN33   = (SCI_HW_CHANNEL_3),
    #else
    WBUS_DEV_PRP_REG_DO_NOT_USE_3 = (SCI_HW_CHANNEL_3),
    #endif
    #ifdef WBUS_DEV_RLIN34UART_EN
    WBUS_DEV_PRP_REG_SET_RLIN34   = (SCI_HW_CHANNEL_4),
    #else
    WBUS_DEV_PRP_REG_DO_NOT_USE_4 = (SCI_HW_CHANNEL_4),
    #endif
    #ifdef WBUS_DEV_RLIN35UART_EN
    WBUS_DEV_PRP_REG_SET_RLIN35   = (SCI_HW_CHANNEL_5),
    #else
    WBUS_DEV_PRP_REG_DO_NOT_USE_5 = (SCI_HW_CHANNEL_5),
    #endif
    WBUS_DEV_PRP_REG_SET_ENUM_MAX   
} enum_WBUS_DEV_PRP_REGISTER_SET_t;


typedef enum
{
    WBUS_DEV_BSW_SW_CHANNEL_0 = 0,  
    WBUS_DEV_BSW_SW_CHANNEL_1,      
    WBUS_DEV_BSW_SW_CHANNEL_2,      
    WBUS_DEV_BSW_SW_CHANNEL_3,      
    WBUS_DEV_BSW_SW_CHANNEL_4,      
    WBUS_DEV_BSW_SW_CHANNEL_5,      
    WBUS_DEV_BSW_SW_CHANNEL_ENUM_MAX   
} enum_WBUS_DEV_BSW_SW_CHANNEL_INDEX_t;

_Static_assert((SCI_NUMBER_OF_CHANNELS)==(WBUS_DEV_BSW_SW_CHANNEL_ENUM_MAX),"Analyze: WBUS_DEV_BSW_SW_CHANNEL_ENUM_MAX");



void Sci_ReceiveNotification(
   uint8_t channel,
   uint8_t value);

void Sci_TransmitNotification(
   uint8_t channel);





#else
    #error Multiple include of "Sci_Cfg.h"
#endif 


