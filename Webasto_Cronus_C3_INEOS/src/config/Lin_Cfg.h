#ifndef LIN_CFG_H_
#define LIN_CFG_H_


#include "hal_data_types.h"

#define LIN_CFG_HW_BUS_UNUSED   0xFFu 


typedef enum
{
    LIN30   = 0u, 
    LIN31       , 
    LIN32       , 
    LIN33       , 
    LIN34       , 
    LIN35       , 
    LIN25       , 
    LIN_MAX
} enum_LIN_UNIT;

typedef struct
{
    enum_LIN_UNIT sw_ch; 
    uint8_t       hw_ch; 
} struct_cfg_lin_bus_map_table;

extern const struct_cfg_lin_bus_map_table ext_cfg_lin_bus_map_table[LIN_MAX];



#endif


