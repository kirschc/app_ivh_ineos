
#ifndef PWM_CFG_H
#define PWM_CFG_H



#define PWM_CFG_SW_MAJOR_VERSION       1u

#define PWM_CFG_SW_MINOR_VERSION       0u

#define PWM_CFG_SW_PATCH_VERSION       0u

#define PWM_EDGE_INTERRUPT_PRIORITY    0x0Fu



typedef enum
{
   PWM_CHANNEL_0 = 0u,
   PWM_CHANNEL_1,
   PWM_CHANNEL_2,
   PWM_CHANNEL_3,
   PWM_CHANNEL_21,
   PWM_CHANNEL_48,
   PWM_CHANNEL_51,
   PWM_CHANNEL_56,
}
Pwm_ChannelType;


void Pwm_Wait10us(void);



#else
#error Multiple include of "Pwm_Cfg.h"
#endif 


