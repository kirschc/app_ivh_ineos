#ifndef TAU_CFG_H_
#define TAU_CFG_H_


#include "Tau.h"



typedef enum
{
    TAUB0CH0                = 0u, 
#ifdef RTOS_ENABLE
    TAUB0CH1                    , 
#endif
    TAUJ1CH2                    ,
    PWM_ANA                     ,
    TAU_CH_MAX

}enum_TAU_CHANNEL;




#endif


