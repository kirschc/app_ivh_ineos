#ifndef DIAGNOSTIC_TROUBLE_CODE_CFG_H
#define DIAGNOSTIC_TROUBLE_CODE_CFG_H



#define DTC_UNUSED               (enum_DTC_TABLE_IDX)0xFFu

#define DTC_NVM_DTC_TABLE_ADDR   ((FEE_USED_DATA_LENGTH_0)+\
                                  (FEE_USED_DATA_LENGTH_1)+\
                                  (FEE_USED_DATA_LENGTH_2)+\
                                  (FEE_USED_DATA_LENGTH_3)+\
                                  (FEE_USED_DATA_LENGTH_4)+\
                                  (FEE_USED_DATA_LENGTH_5)+\
                                  (FEE_USED_DATA_LENGTH_6)+\
                                  sizeof(PCM_NVM_FEE_DAT_BLK_7_START)+\
                                  sizeof(mgl_IUG_NVM_DIAG_STORAGE) )



typedef enum
{
     DTC_ERR_VCC_7V = 0u,           
     DTC_1 ,                        
     DTC_ERR_KL30_ADC_OV,           
     DTC_ERR_KL30_ADC_UV,           
     DTC_4,                         
     DTC_IUG_DEV_HINT,              
     DTC_IUG_SW_ERROR,              
     DTC_IUG_NVM_ERROR,             

     DTC_ERR_VDD12_BE_OV,           
     DTC_ERR_VDD12_BE_UV,           
     DTC_ERR_VDD12_BE_OC,           
     DTC_ERR_PMIC_POWER_STATUS,     
     DTC_ERR_IO1_LED_OC,            
     DTC_ERR_IO2_OC,                
     DTC_ERR_IO3_OC,                
     DTC_ERR_IO4_OC,                

     DTC_ERR_REL1_OC,               
     DTC_ERR_REL2_OC,               
     DTC_ERR_OUT_OC,                
     DTC_ERR_PUSHBUTTON,            
     DTC_ERR_PWM_IN_SIGNAL_CHECK,   
     DTC_ERR_PWM_OUT_LIN2_SHORT,    
     DTC_ERR_PWM_OUT_IO3_SHORT,     
     DTC_ERR_PRESSURE_COM,          

     DTC_ERR_HG1_HGV,               
     DTC_ERR_HG1_STFL,              
     DTC_ERR_HG2_HGV,               
     DTC_ERR_HG2_STFL,              
     DTC_ERR_LIN1_COM,              
     DTC_ERR_HG1_UEHFL,             
     DTC_ERR_LIN2_COM,              
     DTC_ERR_HG2_UEHFL,             

     DTC_ERR_LIN3_WBUS_INTERRUPTION,
     DTC_ERR_LIN3_WBUS_MD,          
     DTC_ERR_LIN4_WBUS_MD,          
     DTC_ERR_LIN5_COM,              
     DTC_ERR_CAN1_COM,              
     DTC_ERR_CAN2_COM,              
     DTC_ERR_CAN3_COM,              
     DTC_ERR_IO1_LED_OPL,           

     DTC_ERR_IO1_LED_STBAT,         
     DTC_ERR_TEMP_WBUS_SENS_INT_DEF,
     DTC_ERR_TEMP_WBUS_SENS_INT_OOR,
     DTC_ERR_TEMP_WBUS_SENS_EXT_DEF,
     DTC_ERR_TEMP_WBUS_SENS_EXT_OOR,
     DTC_ERR_TEMP_BUTTON_STG,       
     DTC_ERR_TEMP_BUTTON_STBAT,     
     DTC_ERR_BLE_PAIRINGCODE,       

     DTC_ERR_BLE_COM,                       
     DTC_ERR_LIN3_WBUS_WRONG_HG1_DETECTED,  
     DTC_ERR_LIN3_WBUS_WRONG_HG2_DETECTED,  
     DTC_ERR_OBD_CONFIG,                    
     DTC_ERR_OBD_REQ,                       
     DTC_LIN3_WBUS_SAME_HG_ADDR_DETECTED,   
     DTC_ERR_NO_HG1_DETECTED,               
     DTC_ERR_NO_HG2_DETECTED,               

     DTC_ERR_IBN_NO_HANDHELD_TAUGHT,        
     DTC_ERR_IBN_NO_CAR_STATE_CHANGED,      
     DTC_ERR_IBN_OBD_WRONG_BAUDRATE,        
     DTC_ERR_IBN_CAR_STATE_ACTIVE,          
     DTC_ERR_IBN_CAR_STATE_INACTIVE,        
     DTC_ERR_IBN_CANCELED_BY_HEATER,        
     DTC_ERR_IBN_CANCELED_BY_USER,          
     DTC_ERR_IBN_HEATER_NOT_IN_WAIT_STATE,  

     DTC_ERR_IBN_HEATER_CTRL_STATE_PAUSE,   
     DTC_ERR_IBN_CANCELED_BY_SUBNET,        
     DTC_ERR_IBN_PUSHBUTTON_NOT_PRESSED,    
	 DTC_ERR_IBN_NOT_ALL_CONFIG_SRC_DET,    
	 DTC_ERR_IBN_T_LIN_R_MISMATCH,          
     DTC_69,                                
     DTC_70,                                
     DTC_71,                                

     DTC_ERR_LIN3_WBUS_COM_HG1,             
     DTC_ERR_LIN3_WBUS_COM_HG2,             
     DTC_ERR_GHI_HGX_COM,                   
     DTC_ERR_GHI_HGX_STFL,                  
     DTC_ERR_LIN4_WBUS_COM_MUL_CTL_3TO4,    
     DTC_ERR_LIN4_WBUS_COM_MUL_CTL_3TO5,    
     DTC_ERR_LIN4_WBUS_COM_REMOTE_CTL,      
     DTC_ERR_LIN4_WBUS_COM_THERMO_CALL,     

     DTC_ERR_LIN4_WBUS_COM_RES3,            
     DTC_ERR_LIN4_WBUS_COM_THERMO_CON,      
     DTC_ERR_LIN4_WBUS_COM_T_LIN_R,         
     DTC_ERR_LIN4_WBUS_COM_RES2,            
     DTC_ERR_BE_NO_COMMAND_CONTINUE_TO_HG1, 
     DTC_ERR_BE_NO_COMMAND_CONTINUE_TO_HG2, 
     DTC_ERR_HG1_NO_HEARTBEAT_RESPOND,      
     DTC_ERR_HG2_NO_HEARTBEAT_RESPOND,      

     DTC_ERR_HG1_ACTION_CANCELLED,          
     DTC_ERR_HG2_ACTION_CANCELLED,          
     DTC_ERR_GHI_ACTION_CANCELLED_HGX,      
     DTC_ERR_GHI_NO_HEARTBEAT_RESPOND_HGX,  
     DTC_ERR_GHI_BE_NO_CMD_CONTINUE_TO_HGX, 
     DTC_ERR_AUXILIARY_HEATING_FUNCTION_OFF,
     DTC_ERR_INTERLOCK_MISUSE_TIMEOUT,      
     DTC_95,                                

     DTC_USER_STOP_HG1,                     
     DTC_USER_STOP_HG2,                     
     DTC_USER_STOP_HG3,                     
     DTC_USER_STOP_HG4,                     
     DTC_USER_STOP_HG5,                     
     DTC_USER_STOP_HG6,                     
     DTC_USER_STOP_HG7,                     
     DTC_USER_STOP_HG8,                     

     DTC_ERR_GHI_HGX_HGV,                   
     DTC_ERR_GHI_HGX_UEHFL,                 
     DTC_106,                               
     DTC_107,                               
     DTC_108,                               
     DTC_ERR_USR_DEV_ACTION_CANCELLED,      
     DTC_ERR_USR_DEV_NO_HEARTBEAT_RESPOND,  
     DTC_ERR_USR_DEV_BE_NO_CMD_CONTINUE,    

     DTC_USR_API_112 = 112u, 
     DTC_USR_API_113 = 113u, 
     DTC_USR_API_114 = 114u, 
     DTC_USR_API_115 = 115u, 
     DTC_USR_API_116 = 116u, 
     DTC_USR_API_117 = 117u, 
     DTC_USR_API_118 = 118u, 
     DTC_USR_API_119 = 119u, 

     DTC_USR_API_120 = 120u, 
     DTC_USR_API_121 = 121u, 
     DTC_USR_API_122 = 122u, 
     DTC_USR_API_123 = 123u, 
     DTC_USR_API_124 = 124u, 
     DTC_USR_API_125 = 125u, 
     DTC_USR_API_126 = 126u, 
     DTC_USR_API_127 = 127u, 
     DTC_ENUM_MAX,           
     DTC_ENUM_FORCE_TYPE = 0x7F 
} enum_DTC_TABLE_IDX;

typedef enum_DTC_TABLE_IDX  DTC_TABLE_IDX_t;
#define DTC_MAX     DTC_ENUM_MAX

_Static_assert((DTC_ENUM_MAX)<=(128 ), "Change DTC_ENUM_FORCE_TYPE to 'DTC_ENUM_FORCE_TYPE = 0x7FFF'!!!");




uint8_t sfl_cfg_dtc_check_usr_access_in_non_usr_area(const enum_DTC_TABLE_IDX dtc_idx);


#endif 


