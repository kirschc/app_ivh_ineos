#ifndef _L99PM72_CFG_H_
#define _L99PM72_CFG_H_

#include "l99pm72_includes.h"
#include "l99pm72_base.h"
#include "sfl_diagnostic_trouble_code.h"

#ifdef IUG_NO_SOFTWARE_DELIVERY_RELEASE
    #define L99PM72_SW_DEBUG_MODE
#endif


#define L99PM72_SR_DTC_MAPPING_ACT

#ifdef L99PM72_SR_DTC_MAPPING_ACT
    #define L99PM72_STAT_REG_1_ERR_MASK    (uint16_t)0xFFFF
    #define L99PM72_STAT_REG_2_ERR_MASK    (uint16_t)0x007F
    #define L99PM72_STAT_REG_3_ERR_MASK    (uint16_t)0xCFFC
    #define L99PM72_STAT_REG_4_ERR_MASK    (uint16_t)0x0041
    #define L99PM72_STAT_REG_5_ERR_MASK    (uint16_t)0x1F80

    #define L99PM72_STAT_REG_NO_ERR        (uint16_t)0x0000u
    #define L99PM72_STAT_REG_IDX_MAX       16u
#endif

#define L99PM72_STAT_REG_REQ_TIME_MS       5000u 
#define L99PM72_STAT_REG_REQ_TIME_IBN_MS   300u  


typedef enum
{
    L99PM72_STAT_REG_1      = 0u,
    L99PM72_STAT_REG_2          ,
    L99PM72_STAT_REG_3          ,
    L99PM72_STAT_REG_4          ,
    L99PM72_STAT_REG_5          ,
    L99PM72_STAT_REG_ENUM_MAX
} enum_L99PM72_STAT_REG;

typedef enum
{
    L99PM72_PREP_LOCAL_DTCS = 0u,
    L99PM72_CHECK_DTC_CFG       ,
    L99PM72_SET_LOCAL_DTCS      ,
    L99PM72_EVAL_LOCAL_DTCS
} enum_L99PM72_DTC_HANDLING;

typedef enum_L99PM72_ERR_CODE_TYPE (*funct_ptr_access_sr)(void);

typedef struct
{
    enum_L99PM72_STAT_REG l99pm72_stat_reg;
    funct_ptr_access_sr   funct_ptr_read_sr;
    funct_ptr_access_sr   funct_ptr_read_clear_sr;
    uint8_t*              sr_err_flg;
} struct_l99pm72_err_access_sr;

typedef struct
{
    enum_DTC_TABLE_IDX dtc_index;
    uint8_t*           l99pm72_error;
} struct_l99pm72_dtc_mapping;


enum_L99PM72_ERR_CODE_TYPE l99pm72_pre_init(struct_L99PM72_TX_REG_TYPE *const ptr_defaults);

enum_L99PM72_ERR_CODE_TYPE l99pm72_previous_settings(void);

void l99pm72_update_sr(void);

enum_L99PM72_ERR_CODE_TYPE l99pm72_check_dtc_cfg(void);

static enum_L99PM72_ERR_CODE_TYPE l99pm72_dtc_handler(const enum_L99PM72_STAT_REG status_reg, const enum_L99PM72_DTC_HANDLING dtc_action);

void l99pm72_critical_error_callback(const struct_L99PM72_GLOBAL_STATUS_REGISTER_TYPE *const gsr_error);



#endif 


