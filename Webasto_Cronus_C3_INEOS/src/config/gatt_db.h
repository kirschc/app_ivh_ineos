

#ifndef GATT_DB_H
#define GATT_DB_H

#define gattdb_service_changed_char             3
#define gattdb_advertising_name                 7
#define gattdb_ota_control                     11
#define gattdb_manufacturer_name_string         15
#define gattdb_product_name_string             18
#define gattdb_serial_number_string            21
#define gattdb_hardware_revision_string         24
#define gattdb_firmware_revision_string         27
#define gattdb_software_revision_string         30
#define gattdb_device_data                     34
#define gattdb_device_resp                     37
#define gattdb_dev_state                       41
#define gattdb_client_requested_send           46
#define gattdb_server_send_notification         49
#define gattdb_ble_to_can                      53
#define gattdb_can_to_ble                      56

#endif


