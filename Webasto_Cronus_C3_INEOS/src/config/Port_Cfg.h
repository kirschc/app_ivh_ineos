
#ifndef PORT_CFG_H
#define PORT_CFG_H



#define PORT_CFG_SW_MAJOR_VERSION      1u

#define PORT_CFG_SW_MINOR_VERSION      1u

#define PORT_CFG_SW_PATCH_VERSION      0u


#define PORT_DIRECTION_CHANGEABLE_PORT_0     0xFFFFu

#define PORT_LEVEL_VALUE_PORT_0              0x0000u

#define PORT_PULLUP_PORT_0                   0x0000u

#define PORT_PULLDOWN_PORT_0                 0x0000u

#define PORT_MODE_CONTRL_PORT_0             ( 0x0000u \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0004u  \
                                            | 0x0008u  \
                                            | 0x0010u  \
                                            | 0x0020u  \
                                            | 0x0040u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x1000u  \
                                            | 0x2000u  \
                                            | 0x4000u  \
                                            | 0x0000u  \
                                            )
#define PORT_MODE_PORT_0                    ( 0xFFFFu  \
                                            & ~(0x0001u)  \
                                            & ~(0x0002u)  \
                                            & ~(0x0004u)  \
                                            & ~(0x0000u)  \
                                            & ~(0x0000u)  \
                                            & ~(0x0020u)  \
                                            & ~(0x0000u)  \
                                            & ~(0x0080u)  \
                                            & ~(0x0100u)  \
                                            & ~(0x0000u)  \
                                            & ~(0x0400u)  \
                                            & ~(0x0000u)  \
                                            & ~(0x0000u)  \
                                            & ~(0x2000u)  \
                                            & ~(0x4000u)  \
                                            & ~(0x8000u)  \
                                            )

#define PORT_FUNC_PORT_0                   ( 0x0000u \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0004u  \
                                           | 0x0008u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x1000u  \
                                           | 0x2000u  \
                                           | 0x4000u  \
                                           | 0x0000u  \
                                           )

#define PORT_FUNC_CONTRL_EXT_PORT_0        ( 0x0000u \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x1000u  \
                                           | 0x2000u  \
                                           | 0x4000u  \
                                           | 0x0000u  \
                                           )

#define PORT_FUNC_CTRL_ADD_EXP_0           ( 0x0000u \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0008u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           )

#define PORT_INPUT_BUFFER_CONTROL_0        ( 0xFFFFu \
                                           & ~(0x2000u)  \
                                           & ~(0x4000u)  \
                                           )

#define PORT_INP_CTRL_0                    0x0000u

#define PORT_BIDIRECTION_CONTROL_0         0x0000u


#define PORT_DIRECTION_CHANGEABLE_PORT_1     0xFFFFu

#define PORT_LEVEL_VALUE_PORT_1              0x0000u

#define PORT_PULLUP_PORT_1                   0x0000u

#define PORT_MODE_CONTRL_PORT_1             ( 0x0000u \
                                            | 0x0001u  \
                                            | 0x0002u  \
                                            | 0x0004u \
                                            | 0x0008u  \
                                            | 0x0010u  \
                                            | 0x0020u  \
                                            | 0x0040u  \
                                            | 0x0080u  \
                                            | 0x0100u  \
                                            | 0x0200u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            )
#define PORT_MODE_PORT_1                    ( 0xFFFFu  \
                                            & ~(0x0000u)  \
                                            & ~(0x0002u)  \
                                            & ~(0x0000u)  \
                                            & ~(0x0008u)  \
                                            & ~(0x0000u)  \
                                            & ~(0x0020u)  \
                                            & ~(0x0000u)  \
                                            & ~(0x0080u)  \
                                            & ~(0x0000u)  \
                                            & ~(0x0200u)  \
                                            & ~(0x0400u)  \
                                            & ~(0x0800u)  \
                                            & ~(0x1000u)  \
                                            & ~(0x2000u)  \
                                            & ~(0x4000u)  \
                                            & ~(0x8000u)  \
                                            )

#define PORT_FUNC_PORT_1                   ( 0x0000u \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           )

#define PORT_INPUT_BUFFER_CONTROL_1          0xFFFFu

#define PORT_BIDIRECTION_CONTROL_1           0x0000u


#define PORT_DIRECTION_CHANGEABLE_PORT_2     0xFFFFu

#define PORT_LEVEL_VALUE_PORT_2              0x0000u

#define PORT_MODE_CONTRL_PORT_2              0x0000u

#define PORT_MODE_PORT_2                     0xFFFFu

#define PORT_FUNC_CONTRL_EXT_PORT_2          0x0000u

#define PORT_INPUT_BUFFER_CONTROL_2          0xFFFFu

#define PORT_BIDIRECTION_CONTROL_2           0x0000u


#define PORT_DIRECTION_CHANGEABLE_PORT_8     0xFFFFu

#define PORT_LEVEL_VALUE_PORT_8              0x0000u

#define PORT_PULLUP_PORT_8                   0x0000u

#define PORT_PULLDOWN_PORT_8                 0x0000u

#define PORT_MODE_CONTRL_PORT_8             ( 0x0000u \
                                            | 0x0001u  \
                                            | 0x0000u  \
                                            | 0x0000u \
                                            | 0x0008u  \
                                            | 0x0010u  \
                                            | 0x0020u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x1000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            )

#define PORT_MODE_PORT_8                    ( 0xFFFFu  \
                                            & ~(0x0000u)  \
                                            & ~(0x0002u)  \
                                            & ~(0x0004u)  \
                                            & ~(0x0000u)  \
                                            & ~(0x0000u)  \
                                            & ~(0x0000u)  \
                                            & ~(0x0040u)  \
                                            & ~(0x0080u)  \
                                            & ~(0x0100u)  \
                                            & ~(0x0200u)  \
                                            & ~(0x0400u)  \
                                            & ~(0x0000u)  \
                                            & ~(0x1000u)  \
                                            & ~(0x2000u)  \
                                            & ~(0x4000u)  \
                                            & ~(0x8000u)  \
                                            )
#define PORT_FUNC_PORT_8                   ( 0x0000u \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           )

#define PORT_FUNC_CONTRL_EXT_PORT_8         ( 0x0000u \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            )
#define PORT_FUNC_CTRL_ADD_EXP_8            0x0000u

#define PORT_INP_CTRL_8                      0x0000u

#define PORT_INPUT_BUFFER_CONTROL_8          0xFFFFu

#define PORT_BIDIRECTION_CONTROL_8           0x0000u


#define PORT_DIRECTION_CHANGEABLE_PORT_9     0xFFFFu

#define PORT_LEVEL_VALUE_PORT_9              0x0004u

#define PORT_PULLUP_PORT_9                   0x0000u

#define PORT_PULLDOWN_PORT_9                 0x0000u

#define PORT_MODE_CONTRL_PORT_9             ( 0x0000u \
                                            | 0x0000u  \
                                            | 0x0002u  \
                                            | 0x0000u \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            )

#define PORT_MODE_PORT_9                    ( 0xFFFFu  \
                                            & ~(0x0000u)  \
                                            & ~(0x0002u)  \
                                            & ~(0x0004u)  \
                                            & ~(0x0008u)  \
                                            & ~(0x0010u)  \
                                            & ~(0x0020u)  \
                                            & ~(0x0040u)  \
                                            & ~(0x0080u)  \
                                            & ~(0x0100u)  \
                                            & ~(0x0200u)  \
                                            & ~(0x0400u)  \
                                            & ~(0x0800u)  \
                                            & ~(0x1000u)  \
                                            & ~(0x2000u)  \
                                            & ~(0x4000u)  \
                                            & ~(0x8000u)  \
                                            )

#define PORT_FUNC_PORT_9                   ( 0x0000u \
                                           | 0x0000u  \
                                           | 0x0002u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           )

#define PORT_FUNC_CONTRL_EXT_PORT_9         ( 0x0000u \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            )
#define PORT_FUNC_CTRL_ADD_EXP_9            0x0000u

#define PORT_INP_CTRL_9                      0x0000u

#define PORT_INPUT_BUFFER_CONTROL_9          0xFFFFu

#define PORT_BIDIRECTION_CONTROL_9           0x0000u


#define PORT_DIRECTION_CHANGEABLE_PORT_10    0xFFFFu

#define PORT_LEVEL_VALUE_PORT_10             0x0000u

#define PORT_PULLUP_PORT_10                  0x0000u

#define PORT_PULLDOWN_PORT_10                0x0000u

#define PORT_MODE_CONTRL_PORT_10            ( 0x0000u \
                                            | 0x0001u  \
                                            | 0x0000u  \
                                            | 0x0000u \
                                            | 0x0000u  \
                                            | 0x0000u \
                                            | 0x0020u \
                                            | 0x0040u  \
                                            | 0x0080u  \
                                            | 0x0000u  \
                                            | 0x0200u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x2000u  \
                                            | 0x4000u  \
                                            | 0x0000u  \
                                            )

#define PORT_MODE_PORT_10                     ( 0xFFFFu  \
                                            & ~(0x0001u)  \
                                            & ~(0x0002u)  \
                                            & ~(0x0004u)  \
                                            & ~(0x0000u)  \
                                            & ~(0x0010u)  \
                                            & ~(0x0020u)  \
                                            & ~(0x0000u)  \
                                            & ~(0x0080u)  \
                                            & ~(0x0100u)  \
                                            & ~(0x0200u)  \
                                            & ~(0x0400u)  \
                                            & ~(0x0800u)  \
                                            & ~(0x1000u)  \
                                            & ~(0x0000u)  \
                                            & ~(0x4000u)  \
                                            & ~(0x8000u)  \
                                            )
#define PORT_FUNC_PORT_10                  ( 0x0000u \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0008u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0040u  \
                                           | 0x0080u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x2000u  \
                                           | 0x4000u  \
                                           | 0x0000u  \
                                           )

#define PORT_FUNC_CONTRL_EXT_PORT_10        ( 0x0000u \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0040u  \
                                            | 0x0080u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            )
#define PORT_FUNC_CTRL_ADD_EXP_10            0x0000u

#define PORT_INPUT_BUFFER_CONTROL_10         0xFFFFu

#define PORT_INP_CTRL_10                     0x0000u

#define PORT_BIDIRECTION_CONTROL_10          0x0000u


#define PORT_DIRECTION_CHANGEABLE_PORT_11    0xFFFFu

#define PORT_LEVEL_VALUE_PORT_11             0x0000u

#define PORT_PULLUP_PORT_11                  0x0000u

#define PORT_PULLDOWN_PORT_11                0x0000u

#define PORT_MODE_CONTRL_PORT_11            ( 0x0000u \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0200u  \
                                            | 0x0400u  \
                                            | 0x0800u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x8000u  \
                                            )

#define PORT_MODE_PORT_11                     ( 0xFFFFu  \
                                            & ~(0x0001u)  \
                                            & ~(0x0002u)  \
                                            & ~(0x0004u)  \
                                            & ~(0x0008u)  \
                                            & ~(0x0010u)  \
                                            & ~(0x0020u)  \
                                            & ~(0x0040u)  \
                                            & ~(0x0080u)  \
                                            & ~(0x0100u)  \
                                            & ~(0x0200u)  \
                                            & ~(0x0400u)  \
                                            & ~(0x0000u)  \
                                            & ~(0x1000u)  \
                                            & ~(0x2000u)  \
                                            & ~(0x4000u)  \
                                            & ~(0x0000u)  \
                                            )

#define PORT_FUNC_PORT_11                  ( 0x0000u \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           )

#define PORT_FUNC_CONTRL_EXT_PORT_11        ( 0x0000u \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            )

#define PORT_FUNC_CTRL_ADD_EXP_11          ( 0x0000u \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           )

#define PORT_INPUT_BUFFER_CONTROL_11        ( 0xFFFFu \
                                            & ~( 0x0200u )  \
                                            & ~( 0x0400u )  \
                                            )

#define PORT_INP_CTRL_11                     0x0000u

#define PORT_BIDIRECTION_CONTROL_11          0x0000u


#define PORT_DIRECTION_CHANGEABLE_PORT_12    0xFFFFu

#define PORT_LEVEL_VALUE_PORT_12             0x0000u

#define PORT_PULLUP_PORT_12                  0x0000u

#define PORT_PULLDOWN_PORT_12                0x0000u

#define PORT_MODE_CONTRL_PORT_12            ( 0x0000u \
                                            | 0x0001u  \
                                            | 0x0000u  \
                                            | 0x0000u \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            )

#define PORT_MODE_PORT_12                     ( 0xFFFFu  \
                                            & ~(0x0001u)  \
                                            & ~(0x0000u)  \
                                            & ~(0x0004u)  \
                                            & ~(0x0008u)  \
                                            & ~(0x0010u)  \
                                            & ~(0x0020u)  \
                                            & ~(0x0040u)  \
                                            & ~(0x0080u)  \
                                            & ~(0x0100u)  \
                                            & ~(0x0200u)  \
                                            & ~(0x0400u)  \
                                            & ~(0x0800u)  \
                                            & ~(0x1000u)  \
                                            & ~(0x2000u)  \
                                            & ~(0x4000u)  \
                                            & ~(0x8000u)  \
                                            )

#define PORT_FUNC_PORT_12                  ( 0x0000u \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           )

#define PORT_FUNC_CONTRL_EXT_PORT_12        ( 0x0000u \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            )
#define PORT_FUNC_CTRL_ADD_EXP_12            0x0000u

#define PORT_INP_CTRL_12                     0x0000u

#define PORT_INPUT_BUFFER_CONTROL_12         0xFFFFu

#define PORT_BIDIRECTION_CONTROL_12          0x0000u


#define PORT_DIRECTION_CHANGEABLE_PORT_18    0xFFFFu

#define PORT_LEVEL_VALUE_PORT_18             0x0000u

#define PORT_MODE_CONTRL_PORT_18            ( 0x0000u \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            )

#define PORT_MODE_PORT_18                     ( 0xFFFFu  \
                                            & ~(0x0001u)  \
                                            & ~(0x0002u)  \
                                            & ~(0x0004u)  \
                                            & ~(0x0008u)  \
                                            & ~(0x0010u)  \
                                            & ~(0x0020u)  \
                                            & ~(0x0040u)  \
                                            & ~(0x0080u)  \
                                            & ~(0x0100u)  \
                                            & ~(0x0200u)  \
                                            & ~(0x0400u)  \
                                            & ~(0x0800u)  \
                                            & ~(0x1000u)  \
                                            & ~(0x2000u)  \
                                            & ~(0x4000u)  \
                                            & ~(0x8000u)  \
                                            )

#define PORT_FUNC_PORT_18                  ( 0x0000u \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           )

#define PORT_INPUT_BUFFER_CONTROL_18         0xFFFFu

#define PORT_BIDIRECTION_CONTROL_18          0x0000u


#define PORT_DIRECTION_CHANGEABLE_PORT_20    0xFFFFu

#define PORT_LEVEL_VALUE_PORT_20             0x0000u

#define PORT_MODE_CONTRL_PORT_20            ( 0x0000u \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            | 0x0000u  \
                                            )

#define PORT_MODE_PORT_20                     ( 0xFFFFu  \
                                            & ~(0x0001u)  \
                                            & ~(0x0002u)  \
                                            & ~(0x0004u)  \
                                            & ~(0x0008u)  \
                                            & ~(0x0010u)  \
                                            & ~(0x0020u)  \
                                            & ~(0x0040u)  \
                                            & ~(0x0080u)  \
                                            & ~(0x0100u)  \
                                            & ~(0x0200u)  \
                                            & ~(0x0400u)  \
                                            & ~(0x0800u)  \
                                            & ~(0x1000u)  \
                                            & ~(0x2000u)  \
                                            & ~(0x4000u)  \
                                            & ~(0x8000u)  \
                                            )

#define PORT_FUNC_PORT_20                  ( 0x0000u \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           | 0x0000u  \
                                           )

#define PORT_INPUT_BUFFER_CONTROL_20         0xFFFFu

#define PORT_BIDIRECTION_CONTROL_20          0x0000u


#define PORT_DIRECTION_CHANGEABLE_PORT_JP    0x00u

#define PORT_LEVEL_VALUE_PORT_JP             0x00u

#define PORT_MODE_CONTRL_PORT_JP             0x00u

#define PORT_MODE_PORT_JP                    0x00u

#define PORT_FUNC_PORT_JP                    0x00u

#define PORT_INPUT_BUFFER_CONTROL_JP         0x00u

#define PORT_BIDIRECTION_CONTROL_JP          0x0000u


#define PORT_DIRECTION_CHANGEABLE_PORT_AP0   0xFFFFu

#define PORT_LEVEL_VALUE_PORT_AP0            0x0000u

#define PORT_MODE_CONTRL_PORT_AP0            0x0000u

#define PORT_MODE_PORT_AP0                    ( 0xFFFFu  \
                                            & ~(0x0000u)  \
                                            & ~(0x0000u)  \
                                            & ~(0x0000u)  \
                                            & ~(0x0000u)  \
                                            & ~(0x0000u)  \
                                            & ~(0x0000u)  \
                                            & ~(0x0000u)  \
                                            & ~(0x0000u)  \
                                            & ~(0x0000u)  \
                                            & ~(0x0000u)  \
                                            & ~(0x0000u)  \
                                            & ~(0x0000u)  \
                                            & ~(0x0000u)  \
                                            & ~(0x0000u)  \
                                            & ~(0x0000u)  \
                                            & ~(0x0000u)  \
                                            )

#define PORT_INPUT_BUFFER_CONTROL_AP0        0xFFFFu

#define PORT_BIDIRECTION_CONTROL_AP0         0x0000u


#define PORT_DIRECTION_CHANGEABLE_PORT_AP1   0xFFFFu

#define PORT_LEVEL_VALUE_PORT_AP1            0x0080u

#define PORT_MODE_CONTRL_PORT_AP1            0x0000u

#define PORT_MODE_PORT_AP1                   ( 0xFFFFu  \
                                           & ~(0x0001u)  \
                                           & ~(0x0002u)  \
                                           & ~(0x0000u)  \
                                           & ~(0x0008u)  \
                                           & ~(0x0010u)  \
                                           & ~(0x0020u)  \
                                           & ~(0x0040u)  \
                                           & ~(0x0080u)  \
                                           )

#define PORT_INPUT_BUFFER_CONTROL_AP1        0xFFFFu

#define PORT_BIDIRECTION_CONTROL_AP1         0x0000u


#define PORT_DIRECTION_CHANGEABLE_PORT_IP    0xFFFFu

#define PORT_MODE_CONTRL_PORT_IP0            0x0000u

#define PORT_MODE_PORT_IP0                   0xFFFFu

#define PORT_INPUT_BUFFER_CONTROL_IP0        0x0000u


#define PORT_NMI                             (uint8_t)0x00

#define PORT_INTP0                           (uint8_t)0x00

#define PORT_INTP1                           (uint8_t)0x00
#define PORT_INTP2                           (uint8_t)0x00
#define PORT_INTP3                           (uint8_t)0x00

#define PORT_INTP4                           (uint8_t)0x00
#define PORT_INTP5                           (uint8_t)0x00

#define PORT_INTP6                           (uint8_t)0x00

#define PORT_INTP7                           (uint8_t)0x00
#define PORT_INTP8                           (uint8_t)0x00

#define PORT_INTP9                           (uint8_t)0x00
#define PORT_INTP10                          (uint8_t)0x02
#define PORT_INTP11                          (uint8_t)0x00
#define PORT_INTP12                          (uint8_t)0x00

#define PORT_INTP13                          (uint8_t)0x00

#define PORT_INTP14                          (uint8_t)0x00

#define PORT_INTP15                          (uint8_t)0x00

#define PORT_INT_CONF_PORT0                  (uint16_t)0x0087

#define PORT_INT_CONF_PORT1                  (uint16_t)0x0087

#define PORT_INT_CONF_PORT2                  (uint16_t)0x0087

#define PORT_INT_CONF_PORT3                  (uint16_t)0x0087

#define PORT_INT_CONF_PORT4                  (uint16_t)0x0087

#define PORT_INT_CONF_PORT5                  (uint16_t)0x0087

#define PORT_INT_CONF_PORT6                  (uint16_t)0x0087

#define PORT_INT_CONF_PORT7                  (uint16_t)0x0087

#define PORT_INT_CONF_PORT8                  (uint16_t)0x0087

#define PORT_INT_CONF_PORT9                  (uint16_t)0x0087

#define PORT_INT_CONF_PORT10                 (uint16_t)0x00C7

#define PORT_INT_CONF_PORT11                 (uint16_t)0x0087

#define PORT_INT_CONF_PORT12                 (uint16_t)0x0087

#define PORT_INT_CONF_PORT13                 (uint16_t)0x0087

#define PORT_INT_CONF_PORT14                 (uint16_t)0x0087

#define PORT_INT_CONF_PORT15                 (uint16_t)0x0087






#else
#error Multiple include of "Port_Cfg.h"
#endif 


