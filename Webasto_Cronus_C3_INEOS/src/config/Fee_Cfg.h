
#ifndef FEE_CFG_H
#define FEE_CFG_H


#include "Std_Types.h"

#define FEE_CFG_SW_MAJOR_VERSION           2u

#define FEE_CFG_SW_MINOR_VERSION           0u

#define FEE_CFG_SW_PATCH_VERSION           0u


#define FEE_SET_MODE_SUPPORTED               STD_ON

#define FEE_ERASE_UNUSED_BLOCKS              STD_OFF

#define FEE_COMPARE_BLANK_CHECK              STD_OFF

#define FEE_VIRTUAL_PAGE_SIZE                4u
#define FEE_MAIN_FUNCTION_PERIOD             20u
#define FEE_JOB_END_NOTIFICATION             NULL_PTR
#define FEE_JOB_ERROR_NOTIFICATION           NULL_PTR

#define FEE_NUMBER_OF_BLOCKS                 9u

#define FEE_MAX_WRITE_DATA                   2048u 

#define FEE_BLOCK_MIN_SIZE				     64u 

#define FEE_BLOCK_HEADER_SIZE				 20u

#define FEE_BLOCK_NUMBER_0                   1u
#define FEE_BLOCK_SIZE_0                     (1u << 8u)
#define FEE_IMMEDIATE_DATA_0                 FALSE
#define FEE_USED_HW_BLOCKS_0                 8u
#define FEE_NUMBER_OF_WRITE_CYCLES_0         FLS_WRITECYCLES_FOR_20YEARS
#define FEE_SIGNATURE_0                      0xAA55AA55u
#define FEE_USED_DATA_LENGTH_0               236u
#define FEE_ERASE_BLOCKS_SIZE_0              1u

#define FEE_BLOCK_NUMBER_1                   2u
#define FEE_BLOCK_SIZE_1                    (1u << 8u)
#define FEE_IMMEDIATE_DATA_1                 FALSE
#define FEE_USED_HW_BLOCKS_1                 8u
#define FEE_NUMBER_OF_WRITE_CYCLES_1         FLS_WRITECYCLES_FOR_20YEARS
#define FEE_SIGNATURE_1                      0xAA55AA55u
#define FEE_USED_DATA_LENGTH_1               236u
#define FEE_ERASE_BLOCKS_SIZE_1              1u

#define FEE_BLOCK_NUMBER_2                   3u
#define FEE_BLOCK_SIZE_2                    (1u << 8u)
#define FEE_IMMEDIATE_DATA_2                 FALSE
#define FEE_USED_HW_BLOCKS_2                 ( 2u * ((FEE_BLOCK_SIZE_2) / (FEE_BLOCK_MIN_SIZE)) ) 
#define FEE_NUMBER_OF_WRITE_CYCLES_2         FLS_WRITECYCLES_FOR_20YEARS
#define FEE_SIGNATURE_2                      0xAA55AA55u
#define FEE_USED_DATA_LENGTH_2               ((FEE_BLOCK_SIZE_2) - (FEE_BLOCK_HEADER_SIZE)) 
#define FEE_ERASE_BLOCKS_SIZE_2              1u

#define FEE_BLOCK_NUMBER_3                   4u
#define FEE_BLOCK_SIZE_3                    (1u << 10u)
#define FEE_IMMEDIATE_DATA_3                 FALSE
#define FEE_USED_HW_BLOCKS_3                 ( 2u * ((FEE_BLOCK_SIZE_3) / (FEE_BLOCK_MIN_SIZE)) ) 
#define FEE_NUMBER_OF_WRITE_CYCLES_3         FLS_WRITECYCLES_FOR_20YEARS
#define FEE_SIGNATURE_3                      0xAA55AA55u
#define FEE_USED_DATA_LENGTH_3               ((FEE_BLOCK_SIZE_3) - (FEE_BLOCK_HEADER_SIZE)) 
#define FEE_ERASE_BLOCKS_SIZE_3              1u

#define FEE_BLOCK_NUMBER_4                   5u
#define FEE_BLOCK_SIZE_4                    (1u << 10u)
#define FEE_IMMEDIATE_DATA_4                 FALSE
#define FEE_USED_HW_BLOCKS_4                 ( 2u * ((FEE_BLOCK_SIZE_4) / (FEE_BLOCK_MIN_SIZE)) ) 
#define FEE_NUMBER_OF_WRITE_CYCLES_4         FLS_WRITECYCLES_FOR_20YEARS
#define FEE_SIGNATURE_4                      0xAA55AA55u
#define FEE_USED_DATA_LENGTH_4               ((FEE_BLOCK_SIZE_4) - (FEE_BLOCK_HEADER_SIZE)) 
#define FEE_ERASE_BLOCKS_SIZE_4              1u

#define FEE_BLOCK_NUMBER_5                   6u
#define FEE_BLOCK_SIZE_5                     (1u << 11u) 

#define FEE_IMMEDIATE_DATA_5                 FALSE
#define FEE_USED_HW_BLOCKS_5                 ( 2u * ((FEE_BLOCK_SIZE_5) / (FEE_BLOCK_MIN_SIZE)) ) 
#define FEE_NUMBER_OF_WRITE_CYCLES_5         FLS_WRITECYCLES_FOR_20YEARS
#define FEE_SIGNATURE_5                      0xAA55AA55u
#define FEE_USED_DATA_LENGTH_5               ((FEE_BLOCK_SIZE_5) - (FEE_BLOCK_HEADER_SIZE))
#define FEE_ERASE_BLOCKS_SIZE_5              1u

#define FEE_BLOCK_NUMBER_6                   7u
#define FEE_BLOCK_SIZE_6                     (1u << 11u) 

#define FEE_IMMEDIATE_DATA_6                 FALSE
#define FEE_USED_HW_BLOCKS_6                 ( 2u * ((FEE_BLOCK_SIZE_6) / (FEE_BLOCK_MIN_SIZE)) ) 
#define FEE_NUMBER_OF_WRITE_CYCLES_6         FLS_WRITECYCLES_FOR_20YEARS
#define FEE_SIGNATURE_6                      0xAA55AA55u
#define FEE_USED_DATA_LENGTH_6               ((FEE_BLOCK_SIZE_6) - (FEE_BLOCK_HEADER_SIZE)) 
#define FEE_ERASE_BLOCKS_SIZE_6              1u

#define FEE_BLOCK_NUMBER_7                   8u
#define FEE_BLOCK_SIZE_7                     (1u << 11u)
#if 0
#define FEE_BLOCK_SIZE_7                     ((FEE_BLOCK_MIN_SIZE) * \
                                                 ( \
                                                    (  (FEE_BLOCK_HEADER_SIZE)  + \
                                                       (sizeof(PCM_NVM_FEE_DAT_BLK_7_START) ) + \
                                                       ((DTC_MAX)                          * sizeof(struct_DTC_TABLE_ENTRY)) + \
                                                       ((IUG_DTC_FREEZE_FRAME_MAX)* sizeof(struct_DTC_FREEZE_FRAME_ENTRY_t)) + \
                                                       (sizeof(PCM_NVM_FEE_DAT_BLK_7_END) ) + \
                                                       (FEE_BLOCK_MIN_SIZE)  + \
                                                       (- 1u)  \
                                                    ) / (FEE_BLOCK_MIN_SIZE)  \
                                                 ) \
                                              )
#endif
#define FEE_IMMEDIATE_DATA_7                 FALSE
#define FEE_USED_HW_BLOCKS_7                 ( 2u * ((FEE_BLOCK_SIZE_7) / (FEE_BLOCK_MIN_SIZE)) ) 
#define FEE_NUMBER_OF_WRITE_CYCLES_7         FLS_WRITECYCLES_FOR_20YEARS
#define FEE_SIGNATURE_7                      0xAA55AA55u
#define FEE_USED_DATA_LENGTH_7               ((FEE_BLOCK_SIZE_7) - (FEE_BLOCK_HEADER_SIZE)) 
#define FEE_ERASE_BLOCKS_SIZE_7              1u

#define FEE_BLOCK_NUMBER_8                   9u
#define FEE_BLOCK_SIZE_8                     0x100u 
#if 0
#define FEE_BLOCK_SIZE_8                     ((FEE_BLOCK_MIN_SIZE) * \
                                                 ( \
                                                    (  (FEE_BLOCK_HEADER_SIZE)  + \
                                                       (sizeof(PCM_NVM_FEE_DAT_BLK_8_START) ) + \
                                                       (sizeof(IUG_NVM_FREQUENT_DATA)) + \
                                                       (sizeof(IUG_UNIT_BLUETOOTH_TIMER_CTL) ) + \
                                                       (sizeof(PCM_NVM_FEE_DAT_BLK_8_END) ) + \
                                                       (FEE_BLOCK_MIN_SIZE)  + \
                                                       (- 1u)  \
                                                    ) / (FEE_BLOCK_MIN_SIZE)  \
                                                 ) \
                                              )
#endif                                              
#define FEE_IMMEDIATE_DATA_8                 FALSE
#define FEE_USED_HW_BLOCKS_8                 ( 2u * ((FEE_BLOCK_SIZE_8) / (FEE_BLOCK_MIN_SIZE)) ) 
#define FEE_NUMBER_OF_WRITE_CYCLES_8         FLS_WRITECYCLES_FOR_20YEARS
#define FEE_SIGNATURE_8                      0xAA55AA55u
#define FEE_USED_DATA_LENGTH_8               ((FEE_BLOCK_SIZE_8) - (FEE_BLOCK_HEADER_SIZE)) 
#define FEE_ERASE_BLOCKS_SIZE_8              1u





#if( ((FEE_NUMBER_OF_BLOCKS) != 9) ||\
     ((FEE_MAX_WRITE_DATA)   < (FEE_BLOCK_SIZE_0))||\
     ((FEE_MAX_WRITE_DATA)   < (FEE_BLOCK_SIZE_1))||\
     ((FEE_MAX_WRITE_DATA)   < (FEE_BLOCK_SIZE_2))||\
     ((FEE_MAX_WRITE_DATA)   < (FEE_BLOCK_SIZE_3))||\
     ((FEE_MAX_WRITE_DATA)   < (FEE_BLOCK_SIZE_4))||\
     ((FEE_MAX_WRITE_DATA)   < (FEE_BLOCK_SIZE_5))||\
     ((FEE_MAX_WRITE_DATA)   < (FEE_BLOCK_SIZE_6))||\
     (0)||\
     (0) )
     #error "Analyze the FEE configuration!!!"
#endif



#else
#error Multiple include of "Fee_Cfg.h"
#endif 


