
#ifndef MCU_CFG_H
#define MCU_CFG_H



#define MCU_CFG_SW_MAJOR_VERSION       1u

#define MCU_CFG_SW_MINOR_VERSION       0u

#define MCU_CFG_SW_PATCH_VERSION       0u

#define MCU_RESET_SETTING              MCU_SW_RESET


#define RH850F1L_DERIVATIVE_48PIN      1u
#define RH850F1L_DERIVATIVE_64PIN      2u
#define RH850F1L_DERIVATIVE_80PIN      3u
#define RH850F1L_DERIVATIVE_100PIN     4u
#define RH850F1L_DERIVATIVE_144PIN     5u
#define RH850F1L_DERIVATIVE_176PIN     6u

#define RH850F1L_DERIVATIVE RH850F1L_DERIVATIVE_144PIN
#define MCU_WAKEUP_SOURCES_IMR0        0x00000000u

#define MCU_WAKEUP_SOURCES_IMR1        0x00000000u

#define MCU_WAKEUP_SOURCES_IMR2        0x00000000u

#define MCU_WAKEUP_SOURCES_IMR3        0x00000000u

#define MCU_WAKEUP_SOURCES_IMR4        0x00000000u

#define MCU_WAKEUP_SOURCES_IMR5        0x00000000u

#define MCU_WAKEUP_SOURCES_IMR6        0x00000000u

#define MCU_WAKEUP_SOURCES_IMR7        0x00000000u

#define MCU_WAKEUP_SOURCES_IMR8        0x00000000u


#define MCU_MOSCC_DATA                 0x00000003u

#define MCU_MOSCST_DATA                0x0000FFFFu


#define MCU_SOSCST_DATA                0x0000FFFFu

#define MCU_PLLC_DATA                  0x00000227u 




#define MCU_CKSC_AWDTAD_CTL            0x00000002u


#define MCU_CKSC_ATAUJS_CTL            0x00000004u

#define MCU_CKSC_ATAUJD_CTL            0x00000001u



#define MCU_CKSC_ARTCAD_STPM            0x00000001u

#define MCU_CKSC_ARTCAS_CTL            0x00000001u

#define MCU_CKSC_ARTCAD_CTL            0x00000001u


#define MCU_CKSC_AADCAS_CTL            0x00000003u

#define MCU_CKSC_AADCAD_CTL            0x00000001u


#define MCU_CKSC_AFOUTS_CTL            0x00000005u


#define MCU_CKSC_CPUCLKS_CTL           0x00000003u

#define MCU_CKSC_CPUCLKD_CTL           0x00000001u

#define MCU_CKSC_IPERI1S_CTL           0x00000001u

#define MCU_CKSC_IPERI2S_CTL           0x00000001u


#define MCU_CKSC_ILINS_CTL             0x00000001u

#define MCU_CKSC_ILIND_CTL             0x00000001u

#define MCU_CKSC_IADCAS_CTL            0x00000003u

#define MCU_CKSC_IADCAD_CTL            0x00000001u

#define MCU_CKSC_ICANS_CTL             0x00000002u


#define MCU_CKSC_ICANOSCD_CTL          0x00000000u

#define MCU_CKSC_ICSIS_CTL             0x00000001u






#else
#error Multiple include of "Mcu_Cfg.h"
#endif 


