#ifndef TP_IL_CONFIG_H
#define TP_IL_CONFIG_H




#define TP_IL_RX_IDENTIFIER         0x0701  

#define TP_IL_TX_IDENTIFIER         0x0702  

#define TP_IL_TASK_TIME_IN_MS               1u 

#define TP_IL_FILL_BYTE                     0x00u

#define TP_IL_TIMEOUT                       TRUE

#define TP_IL_FC_RX_TIMEOUT                 100u

#define TP_IL_CAN_TX_TIMEOUT                50u

#define TP_IL_CF_RX_TIMEOUT                 200u

#define TP_IL_REMOTE_FLOW_CONTROL_SEP_TIME    1u 

#define TP_IL_FLOW_CONTROL_BLOCK_SIZE         5u 

#define IO_E_FCN_SUSPENDED          16u

#define IO_E_PARAM_IGNORED          17u

#define IO_E_INVALID_CHANNEL_ID     18u

#define IO_E_INVALID_VALUE          19u

#define IO_E_INVALID_SIZE           20u

#define IO_E_INVALID_POSITION       21u

#define IO_E_INVALID_NOTIF_TYPE     22u

#define IO_E_OK                     0u

#define IO_E_BUSY                   1u

#define IO_E_UNKNOWN_MODE           2u

#define IO_E_TIMEOUT                3u

#define IO_E_NOK                    64u

#define IO_LIBRARY_SW_VERSION_NUMBER        0x01u

#define IO_LIBRARY_SW_REVISION_NUMBER       0x09u

#define IO_LIBRARY_SW_DOCUMENT_NUMBER       0x14u

#define IO_LIBRARY_SW_VENDOR_NUMBER         0x00u



#if( (CAN_WEBASTO_BRIDGE_CHANNEL_MAX) <= 1)
#define app_main_get_can_handler()          &ext_can_handle[CANWEB_BRIDGE_BridgeCfg[(CANWEBBRG_CHL_0)].can_bus]

#define app_main_tp_il_get_can_tx_id(chl)   CANWEB_BRIDGE_BridgeCfg[(chl)].can_id_tx

#endif






typedef unsigned short IO_ErrorType;

 typedef unsigned long IO_U32;


void        TP_IL_Receive_DataBufferRelease( uint8_t * const DataBuffer );
uint8_t *   TP_IL_Receive_DataBufferReserve( void );
void        TP_IL_Transmit_DataBufferRelease( uint8_t * const DataBuffer );

#define TP_IL_BUFFER_CLR_RX()               tpExtCtrl_t.bufDataRx_pui08 = (NULL_PTR);
#define TP_IL_BUFFER_CLR_TX()               tpExtCtrl_t.bufDataTx_pui08 = (NULL_PTR);


#define TP_IL_BUFFER_GET_RX()               tpExtCtrl_t.bufDataRx_pui08
#define TP_IL_BUFFER_GET_TX()               tpExtCtrl_t.bufDataTx_pui08


#define TP_IL_BUFFER_SET_RX( buffer_pui8 )  tpExtCtrl_t.bufDataRx_pui08 = (buffer_pui8);
#define TP_IL_BUFFER_SET_TX( buffer_pui8 )  tpExtCtrl_t.bufDataTx_pui08 = (buffer_pui8);


void TP_IL_ReceiveNotification(void);

void TP_IL_TransmitNotification(void);

IO_ErrorType TP_IL_CAN_Transmit(const uint8_t *data_pui08);

IO_ErrorType TP_IL_CAN_Receive(uint8_t *data_pui08);

#endif


