
#ifndef __R7F701057_IRQ_H
#define __R7F701057_IRQ_H

#include "Project_Adjust.h" 



#define IRQ_TABLE_START                 0x00000200u

#define RESET_ENABLE                    0x00000001u



#ifdef RTOS_ENABLE
	#define EITRAP0_ENABLE                  0x00000040u
#endif




























#ifndef INTTAUD0I0_ENABLE              
  #define INTTAUD0I0_ENABLE               0x00000000u
#endif
#ifndef INTCSIH2IC_1_ENABLE            
  #define INTCSIH2IC_1_ENABLE             0x00000000u
#endif



#ifndef INTCSIH3IC_1_ENABLE            
  #define INTCSIH3IC_1_ENABLE             0x00000000u
#endif
#ifndef INTTAUD0I2_ENABLE              
  #define INTTAUD0I2_ENABLE               0x00000000u
#endif






#ifndef INTCSIH3IR_1_ENABLE            
  #define INTCSIH3IR_1_ENABLE             0x00000000u
#endif
#ifndef INTTAUD0I10_ENABLE             
  #define INTTAUD0I10_ENABLE              0x00000000u
#endif



#ifndef INTCSIH3IRE_1_ENABLE           
  #define INTCSIH3IRE_1_ENABLE            0x00000000u
#endif
#ifndef INTTAUD0I12_ENABLE             
  #define INTTAUD0I12_ENABLE              0x00000000u
#endif



#ifndef INTCSIH3IJC_1_ENABLE           
  #define INTCSIH3IJC_1_ENABLE            0x00000000u
#endif
#ifndef INTTAUD0I14_ENABLE             
  #define INTTAUD0I14_ENABLE              0x00000000u
#endif



#ifndef INTTAPA0IPEK0_ENABLE           
  #define INTTAPA0IPEK0_ENABLE            0x00000000u
#endif
#ifndef INTCSIH1IC_1_ENABLE            
  #define INTCSIH1IC_1_ENABLE             0x00000000u
#endif



#ifndef INTTAPA0IVLY0_ENABLE           
  #define INTTAPA0IVLY0_ENABLE            0x00000000u
#endif
#ifndef INTCSIH1IR_1_ENABLE            
  #define INTCSIH1IR_1_ENABLE             0x00000000u
#endif

 #define INTADCA0I0_ENABLE               (IRQ_TABLE_START + 0x00000028u)

 #define INTADCA0I1_ENABLE               (IRQ_TABLE_START + 0x0000002Cu)

 #define INTADCA0I2_ENABLE               (IRQ_TABLE_START + 0x00000030u)










#ifndef INTCSIH1IRE_1_ENABLE           
  #define INTCSIH1IRE_1_ENABLE            0x00000000u
#endif
#ifndef INTCSIG0IC_ENABLE              
  #define INTCSIG0IC_ENABLE               0x00000000u
#endif



#ifndef INTCSIH1IJC_1_ENABLE           
  #define INTCSIH1IJC_1_ENABLE            0x00000000u
#endif
#ifndef INTCSIG0IR_ENABLE              
  #define INTCSIG0IR_ENABLE               0x00000000u
#endif





#ifndef INTRLIN30UR0_ENABLE
    #define INTRLIN30UR0_ENABLE             (IRQ_TABLE_START + 0x00000068u)
#endif

#ifndef INTRLIN30UR1_ENABLE
    #define INTRLIN30UR1_ENABLE             (IRQ_TABLE_START + 0x0000006Cu)
#endif

#ifndef INTRLIN30UR2_ENABLE
    #define INTRLIN30UR2_ENABLE             (IRQ_TABLE_START + 0x00000070u)
#endif




#ifndef INTP0_ENABLE                   
  #define INTP0_ENABLE                    0x00000000u
#endif
#ifndef INTCSIH2IR_1_ENABLE            
  #define INTCSIH2IR_1_ENABLE             0x00000000u
#endif



#ifndef INTP1_ENABLE                   
  #define INTP1_ENABLE                    0x00000000u
#endif
#ifndef INTCSIH2IRE_1_ENABLE           
  #define INTCSIH2IRE_1_ENABLE            0x00000000u
#endif



#ifndef INTP2_ENABLE                   
  #define INTP2_ENABLE                    0x00000000u
#endif
#ifndef INTCSIH2IJC_1_ENABLE           
  #define INTCSIH2IJC_1_ENABLE            0x00000000u
#endif















 #define INTADCA0ERR_ENABLE              (IRQ_TABLE_START + 0x000000BCu)




























#define INTOSTM0_ENABLE                 (IRQ_TABLE_START + 0x00000130u)




#ifndef INTPWGA4_ENABLE                
  #define INTPWGA4_ENABLE                 0x00000000u
#endif
#ifndef INTENCA0IOV_ENABLE             
  #define INTENCA0IOV_ENABLE              0x00000000u
#endif



#ifndef INTPWGA5_ENABLE                
  #define INTPWGA5_ENABLE                 0x00000000u
#endif
#ifndef INTENCA0IUD_ENABLE             
  #define INTENCA0IUD_ENABLE              0x00000000u
#endif



#ifndef INTPWGA6_ENABLE                
  #define INTPWGA6_ENABLE                 0x00000000u
#endif
#ifndef INTENCA0I0_ENABLE              
  #define INTENCA0I0_ENABLE               0x00000000u
#endif



#ifndef INTENCA0I1_ENABLE              
  #define INTENCA0I1_ENABLE               0x00000000u
#endif
#ifndef INTPWGA7_ENABLE                
  #define INTPWGA7_ENABLE                 0x00000000u
#endif


















 #define INTRCAN1ERR_ENABLE              (IRQ_TABLE_START + 0x000001A4u)

 #define INTRCAN1REC_ENABLE              (IRQ_TABLE_START + 0x000001A8u)

 #define INTRCAN1TRX_ENABLE              (IRQ_TABLE_START + 0x000001ACu)




#ifndef INTCSIH1IC_ENABLE              
  #define INTCSIH1IC_ENABLE               0x00000000u
#endif
#ifndef INTTAPA0IPEK0_2_ENABLE         
  #define INTTAPA0IPEK0_2_ENABLE          0x00000000u
#endif



#ifndef INTCSIH1IR_ENABLE              
  #define INTCSIH1IR_ENABLE               0x00000000u
#endif
#ifndef INTTAPA0IVLY0_2_ENABLE         
  #define INTTAPA0IVLY0_2_ENABLE          0x00000000u
#endif



#ifndef INTCSIH1IRE_ENABLE             
  #define INTCSIH1IRE_ENABLE              0x00000000u
#endif
#ifndef INTCSIG0IC_2_ENABLE            
  #define INTCSIG0IC_2_ENABLE             0x00000000u
#endif



#ifndef INTCSIH1IJC_ENABLE             
  #define INTCSIH1IJC_ENABLE              0x00000000u
#endif
#ifndef INTCSIG0IR_2_ENABLE            
  #define INTCSIG0IR_2_ENABLE             0x00000000u
#endif


#ifndef INTRLIN31UR0_ENABLE
    #define INTRLIN31UR0_ENABLE             (IRQ_TABLE_START + 0x000001C4u)
#endif

#ifndef INTRLIN31UR1_ENABLE
    #define INTRLIN31UR1_ENABLE             (IRQ_TABLE_START + 0x000001C8u)
#endif

#ifndef INTRLIN31UR2_ENABLE
    #define INTRLIN31UR2_ENABLE             (IRQ_TABLE_START + 0x000001CCu)
#endif












#ifndef INTTAUD0I0_2_ENABLE            
  #define INTTAUD0I0_2_ENABLE             0x00000000u
#endif
#ifndef INTCSIH2IC_ENABLE              
  #define INTCSIH2IC_ENABLE               0x00000000u
#endif



#ifndef INTCSIH2IR_ENABLE              
  #define INTCSIH2IR_ENABLE               0x00000000u
#endif
#ifndef INTP0_2_ENABLE                 
  #define INTP0_2_ENABLE                  0x00000000u
#endif



#ifndef INTCSIH2IRE_ENABLE             
  #define INTCSIH2IRE_ENABLE              0x00000000u
#endif
#ifndef INTP1_2_ENABLE                 
  #define INTP1_2_ENABLE                  0x00000000u
#endif



#ifndef INTCSIH2IJC_ENABLE             
  #define INTCSIH2IJC_ENABLE              0x00000000u
#endif
#ifndef INTP2_2_ENABLE                 
  #define INTP2_2_ENABLE                  0x00000000u
#endif

#ifdef RTOS_ENABLE
#endif







#ifndef INTTAUB0I3_ENABLE              
  #define INTTAUB0I3_ENABLE               0x00000000u
#endif
#ifndef INTPWGA16_ENABLE               
  #define INTPWGA16_ENABLE                0x00000000u
#endif




#ifndef INTPWGA17_ENABLE               
  #define INTPWGA17_ENABLE                0x00000000u
#endif
#ifndef INTTAUB0I5_ENABLE              
  #define INTTAUB0I5_ENABLE               0x00000000u
#endif




#ifndef INTTAUB0I7_ENABLE              
  #define INTTAUB0I7_ENABLE               0x00000000u
#endif
#ifndef INTPWGA18_ENABLE               
  #define INTPWGA18_ENABLE                0x00000000u
#endif




#ifndef INTTAUB0I9_ENABLE              
  #define INTTAUB0I9_ENABLE               0x00000000u
#endif
#ifndef INTPWGA19_ENABLE               
  #define INTPWGA19_ENABLE                0x00000000u
#endif




#ifndef INTTAUB0I11_ENABLE             
  #define INTTAUB0I11_ENABLE              0x00000000u
#endif
#ifndef INTPWGA26_ENABLE               
  #define INTPWGA26_ENABLE                0x00000000u
#endif




#ifndef INTTAUB0I13_ENABLE             
  #define INTTAUB0I13_ENABLE              0x00000000u
#endif
#ifndef INTPWGA30_ENABLE               
  #define INTPWGA30_ENABLE                0x00000000u
#endif




#ifndef INTPWGA31_ENABLE               
  #define INTPWGA31_ENABLE                0x00000000u
#endif
#ifndef INTTAUB0I15_ENABLE             
  #define INTTAUB0I15_ENABLE              0x00000000u
#endif



#ifndef INTTAUD0I2_2_ENABLE            
  #define INTTAUD0I2_2_ENABLE             0x00000000u
#endif
#ifndef INTCSIH3IC_ENABLE              
  #define INTCSIH3IC_ENABLE               0x00000000u
#endif



#ifndef INTTAUD0I10_2_ENABLE           
  #define INTTAUD0I10_2_ENABLE            0x00000000u
#endif
#ifndef INTCSIH3IR_ENABLE              
  #define INTCSIH3IR_ENABLE               0x00000000u
#endif



#ifndef INTCSIH3IRE_ENABLE             
  #define INTCSIH3IRE_ENABLE              0x00000000u
#endif
#ifndef INTTAUD0I12_2_ENABLE           
  #define INTTAUD0I12_2_ENABLE            0x00000000u
#endif



#ifndef INTCSIH3IJC_ENABLE             
  #define INTCSIH3IJC_ENABLE              0x00000000u
#endif
#ifndef INTTAUD0I14_2_ENABLE           
  #define INTTAUD0I14_2_ENABLE            0x00000000u
#endif



#ifndef INTRLIN32UR0_ENABLE
    #define INTRLIN32UR0_ENABLE             (IRQ_TABLE_START + 0x00000274u)
#endif

#ifndef INTRLIN32UR1_ENABLE
    #define INTRLIN32UR1_ENABLE             (IRQ_TABLE_START + 0x00000278u)
#endif

#ifndef INTRLIN32UR2_ENABLE
    #define INTRLIN32UR2_ENABLE             (IRQ_TABLE_START + 0x0000027Cu)
#endif































 #define INTRTCA0AL_ENABLE               (IRQ_TABLE_START + 0x00000328u)


 #define INTADCA1ERR_ENABLE              (IRQ_TABLE_START + 0x00000330u)

 #define INTADCA1I0_ENABLE               (IRQ_TABLE_START + 0x00000334u)

 #define INTADCA1I1_ENABLE               (IRQ_TABLE_START + 0x00000338u)

 #define INTADCA1I2_ENABLE               (IRQ_TABLE_START + 0x0000033Cu)

 #define INTRCAN2ERR_ENABLE              (IRQ_TABLE_START + 0x00000344u)

 #define INTRCAN2REC_ENABLE              (IRQ_TABLE_START + 0x00000348u)

 #define INTRCAN2TRX_ENABLE              (IRQ_TABLE_START + 0x0000034Cu)

 #define INTRCAN3ERR_ENABLE              (IRQ_TABLE_START + 0x00000350u)

 #define INTRCAN3REC_ENABLE              (IRQ_TABLE_START + 0x00000354u)

 #define INTRCAN3TRX_ENABLE              (IRQ_TABLE_START + 0x00000358u)


#ifndef INTCSIG1IR_ENABLE
    #define INTCSIG1IR_ENABLE               (IRQ_TABLE_START + 0x00000360u)
#endif



#ifndef INTRLIN25_ENABLE
    #define INTRLIN25_ENABLE                (IRQ_TABLE_START + 0x0000036Cu)
#endif


#ifdef WBUS_DEV_RLIN33UART_EN
    #define INTRLIN33UR0_ENABLE             (IRQ_TABLE_START + 0x00000374u)

    #define INTRLIN33UR1_ENABLE             (IRQ_TABLE_START + 0x00000378u)

    #define INTRLIN33UR2_ENABLE             (IRQ_TABLE_START + 0x0000037Cu)
#endif


#ifdef WBUS_DEV_RLIN34UART_EN
    #define INTRLIN34UR0_ENABLE             (IRQ_TABLE_START + 0x00000384u)

    #define INTRLIN34UR1_ENABLE             (IRQ_TABLE_START + 0x00000388u)

    #define INTRLIN34UR2_ENABLE             (IRQ_TABLE_START + 0x0000038Cu)
#endif


#ifndef INTRLIN35UR0_ENABLE
    #define INTRLIN35UR0_ENABLE             (IRQ_TABLE_START + 0x00000394u)
#endif

#ifndef INTRLIN35UR1_ENABLE
    #define INTRLIN35UR1_ENABLE             (IRQ_TABLE_START + 0x00000398u)
#endif

#ifndef INTRLIN35UR2_ENABLE
    #define INTRLIN35UR2_ENABLE             (IRQ_TABLE_START + 0x0000039Cu)
#endif





















































#endif 


