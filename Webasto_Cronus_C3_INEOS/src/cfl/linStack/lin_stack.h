#ifndef LIN_STACK_H
#define LIN_STACK_H
#include "hal_data_types.h"
#include "lin_db_tables.h"
#include "hal_lin.h" 
#include "Lin_Cfg.h" 


#define MRS_LIN_STACK_SW_VERSION  "V2.1.1"



typedef enum
{
    LIN_HANDLE_IDLE = 0,									
    LIN_HANDLE_HEADER_TRANSMISSION_ENABLED,					
    LIN_HANDLE_HEADER_RECEPTION,							
    LIN_HANDLE_RESPONSE_TRANSMISSION_ENABLED,				
    LIN_HANDLE_RESPONSE_RECEPTION_ENABLED,					
    LIN_HANDLE_MASTER_REQUEST_FRAME_TRANSMISSION_ENABLED	
}enum_lin_handle_state_types;

typedef enum
{
    LIN_HEADER_NO_ACK                   = 0u,               
    LIN_HEADER_ACK                      = 1u                
}enum_header_received_callback;

typedef enum
{
    LIN_GET_BUS_IDX_BY_SW_CH = 0u, 
    LIN_GET_BUS_IDX_BY_HW_CH     , 
    LIN_GET_BUS_IDX_ENUM_MAX       
} enum_LIN_GET_BUS_INDEX_TYPES;

typedef struct
{
    uint8_t                     trasmitted_header_id; 
    enum_lin_handle_state_types handle_state;         
    uint32_t                    error_timestamp;      
    struct_hal_lin_handle       lin_handle;           
} struct_lin_dyn_data;


typedef enum_header_received_callback (*lin_header_received_callback_t)(const uint8_t lin_module, const uint8_t rx_header_id, const uint8_t frame_index);

typedef void (*lin_frame_received_callback_t)(const uint8_t lin_module, const struct_lin_rx_msg_type, const uint8_t frame_index);

typedef void (*lin_schedule_complete_callback_t)(const uint8_t lin_module, const uint8_t lin_schedule_table_index);

typedef void (*lin_frame_transmission_complete_callback_t)(const uint8_t lin_module, const uint8_t tx_header_id, const uint8_t frame_index);



#define LIN_WAKE_SIGNAL     (uint8_t)0xF0    
#define LIN_SYNC_SIGNAL     (uint8_t)0x55    

typedef enum
{
    LIN_TX_FRAME_START_INTERRUPT = 0,
    LIN_TX_FRAME_START_IDLE
}enum_lin_tx_frame_start_types;


#define LIN_STATE_ERROR     (uint8_t)0x00
#define LIN_STATE_SUCCESS   (uint8_t)0x01


extern lin_header_received_callback_t 				ext_lin_stack_header_received_cb_handler;
extern lin_frame_received_callback_t 				ext_lin_stack_frame_received_cb_handler;
extern lin_schedule_complete_callback_t 			ext_lin_stack_schedule_complete_cb_handler;
extern lin_frame_transmission_complete_callback_t	ext_lin_stack_frame_transmission_complete_cb_handler;

extern struct_lin_dyn_data ext_lin_dyn_ch_data[LIN_MAX];


void lin_stack_init(void);

void lin_set_callback(  lin_header_received_callback_t              ptr_lin_header_received_callback,
                        lin_frame_received_callback_t               ptr_lin_frame_received_callback,
                        lin_schedule_complete_callback_t            ptr_lin_schedule_complete_callback,
                        lin_frame_transmission_complete_callback_t  ptr_lin_frame_transmission_complete_callback);


void lin_module_disable(uint8_t lin_module);


void lin_send_wakeup(uint8_t lin_module);


void lin_send_go_to_sleep(uint8_t lin_module);


void lin_stack_ms_timer_handling(void);


void lin_send_frame_by_index(uint8_t lin_frame_index);

void lin_stack_master_transmit_frame_raw( const uint8_t lin_module, const uint8_t lin_id, const uint8_t length, const enum_HAL_LIN_CRC_TYPE crc_type, const enum_HAL_LIN_DIR lin_dir, \
                                          const uint8_t data_byte_0, const uint8_t data_byte_1, const uint8_t data_byte_2, const uint8_t data_byte_3, \
                                          const uint8_t data_byte_4, const uint8_t data_byte_5, const uint8_t data_byte_6, const uint8_t data_byte_7 );

uint32_t lin_db_get_value(uint32_t datapoint_id);

#if ARCHITECTURE_64BIT
uint64_t lin_db_get_value_64(uint32_t datapoint_id);
#endif 


void lin_db_set_value(uint32_t datapoint_id, uint32_t set_value);

#if ARCHITECTURE_64BIT
void lin_db_set_value_64(uint32_t datapoint_id, uint64_t set_value);
#endif 

void lin_schedule_handling(uint8_t lin_schedule_table_index, uint8_t lin_st_active);


void lin_schedule_handling_reset(uint8_t lin_schedule_table_index);


void lin_stack_queue_in_process(void);


void lin_stack_gateway_handling( uint8_t lin_module_in, uint8_t frame_id );


void lin_gateway_req_data_process(uint8_t frame_dest_index, uint8_t frame_src_index);


void lin_gateway_resp_data_process(uint8_t frame_dest_index, uint8_t frame_src_index);


uint8_t lin_check_for_received_response_frame(uint8_t frame_index);


uint8_t lin_check_for_received_request_frame(uint8_t frame_index);


uint8_t lin_check_for_received_frame(uint8_t lin_module, uint8_t frame_index, uint8_t reset);


void lin_header_received_ack(uint8_t frame_index);


void lin_clear_request_frame_flag(uint8_t frame_index);


uint8_t lin_ma_check_frame_resp_timeout(uint8_t frame_index);

uint16_t lin_get_lin_frame_index(uint8_t lin_module, uint8_t lin_id);

#endif






