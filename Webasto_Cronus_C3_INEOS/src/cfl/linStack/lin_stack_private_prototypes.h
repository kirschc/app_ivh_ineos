#ifndef LIN_STACK_PRIVATE_PROTOTYPES_H
#define LIN_STACK_PRIVATE_PROTOTYPES_H
#include "hal_data_types.h"
#include "lin_db_tables.h"
#include "lin_stack.h"

static void private_lin_stack_header_transmission_complete_callback(uint8_t module);

static void private_lin_stack_header_reception_callback(uint8_t module);

static void private_lin_stack_response_transmission_complete_callback(uint8_t module);

static void private_lin_stack_response_reception_complete_callback(uint8_t module);

static void private_lin_stack_rx_1st_data_byte_callback(uint8_t module);

static void private_lin_stack_error_callback(uint8_t module, uint8_t status);

static uint8_t private_lin_stack_calc_protected_lin_id( uint8_t lin_id );

static void private_lin_stack_id_crc_calculate_and_save(void);


void lin_stack_tx_frame_send_start(uint8_t lin_frame_index);


static void private_lin_stack_int_tx_frame_send_start(uint8_t lin_frame_index);


static void private_lin_stack_transmit_frame_initiation(uint8_t lin_frame_index, uint8_t call_src);


static void private_lin_stack_module_save_rx_msg(uint8_t lin_module, struct_lin_rx_msg_type rx_msg);


static uint8_t private_lin_stack_module_get_saved_rx_msg(uint8_t lin_module, struct_lin_rx_msg_type* msg);


static void private_lin_stack_module_save_rx_header(uint8_t lin_module, uint8_t rx_header_id);

static uint8_t private_lin_stack_module_get_saved_rx_header(uint8_t lin_module, uint8_t* header_id);

static void private_lin_stack_lin_module_rx_idle_and_enable_header_reception(uint8_t lin_module, uint8_t lin_hw_module);

static uint8_t private_lin_stack_get_lin_module(const enum_LIN_GET_BUS_INDEX_TYPES idx_type, const uint8_t channel);


static struct_lin_rx_msg_type* private_get_ext_dyn_lin_rx_msg_queue(uint8_t bus, uint8_t queue_ptr_write_address);

static uint8_t* private_get_ext_dyn_lin_rx_header_queue(uint8_t bus, uint8_t queue_ptr_write_address);

#endif






