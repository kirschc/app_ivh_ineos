#ifdef ENABLE_QAC_TEST
    #pragma PRQA_MESSAGES_OFF 0292
#endif


#ifdef ENABLE_QAC_TEST
    #pragma PRQA_MESSAGES_ON 0292
#endif


#ifndef FCL_DESCRIPTOR_H
#define FCL_DESCRIPTOR_H
#include "Mcu_Cfg.h"
#define FCL_CPU_FREQUENCY_MHZ  80                             
#define FCL_AUTHENTICATION_ID {0xFFFFFFFF, \
                               0xFFFFFFFF, \
                               0xFFFFFFFF, \
                               0xFFFFFFFF}                    
#if ( (RH850F1L_DERIVATIVE > RH850F1L_DERIVATIVE_100PIN) && (RH850F1L_DERIVATIVE <= RH850F1L_DERIVATIVE_176PIN) )
    #define FCL_RAM_ADDRESS        (0xFEDE0000+4)                     
#else
    #define FCL_RAM_ADDRESS        (0xFEDF8000+4)                     
#endif


extern r_fcl_descriptor_t sampleApp_fclConfig_enu; 





#endif 


