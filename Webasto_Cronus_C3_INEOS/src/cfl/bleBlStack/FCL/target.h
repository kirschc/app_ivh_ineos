#ifdef ENABLE_QAC_TEST
    #pragma PRQA_MESSAGES_OFF 0292
#endif


#ifdef ENABLE_QAC_TEST
    #pragma PRQA_MESSAGES_ON 0292
#endif


#ifndef TARGET_H
#define TARGET_H

#if R_FCL_COMPILER == R_FCL_COMP_GHS
    #include "dr7f701057.dev.h"
#elif R_FCL_COMPILER == R_FCL_COMP_IAR
    #include "ior7f701035xafp.h"
#elif R_FCL_COMPILER == R_FCL_COMP_REC
    #include "iodefine.h"
#endif




#define FLMD0_PROTECTION_OFF    (0x01u)
#define FLMD0_PROTECTION_ON     (0x00u)

#define FCL_INIT_FLASHACCESS                                        \
            volatile uint32_t i;                                    \
                                                                    \
                                                  \
            FLMDPCMD = 0xa5;                                        \
            FLMDCNT  = FLMD0_PROTECTION_OFF;                        \
            FLMDCNT  = ~FLMD0_PROTECTION_OFF;                       \
            FLMDCNT  = FLMD0_PROTECTION_OFF;                        \
            for (i=0; i<10000; i++)                                 \
            {                                                       \
                       \
                                 \
            }                                                       

#define FCL_DISABLE_FLASHACCESS                                     \
            volatile uint32_t i;                                    \
                                                                    \
                                                  \
            FLMDPCMD = 0xa5;                                        \
            FLMDCNT  = FLMD0_PROTECTION_ON;                         \
            FLMDCNT  = ~FLMD0_PROTECTION_ON;                        \
            FLMDCNT  = FLMD0_PROTECTION_ON;                         \
            for (i=0; i<10000; i++)                                 \
            {                                                       \
                       \
                                 \
            }                                                       

#define R_FCL_DEVICE_SPECIFIC_INIT                                  \
            *(volatile uint32_t *)0xFFA08000 = 0xFFFFFFFF;          \
            *(volatile uint32_t *)0xFFA08004 = 0xFFFFFFFF;          \
            *(volatile uint32_t *)0xFFA08008 = 0xFFFFFFFF;          \
            *(volatile uint32_t *)0xFFA0800C = 0xFFFFFFFF;







#endif 


