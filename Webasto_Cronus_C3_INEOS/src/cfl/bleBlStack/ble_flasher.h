
#ifndef CFL_BLE_BL_H
#define CFL_BLE_BL_H

#define BLE_FLASHER_VERSION V1.0.0

#include "hal_data_types.h"
#include "bgm111_protocol.h"
#include "bgm111_ble_api.h"



#define BLE_FLASHER_CMD_BYTE_SIZE       4u          
#define BLE_FLASHER_CMD_TIMEOUT_MS      5000u       
#define BLE_FLASHER_MAX_DATA_LEN_READ   200u        
#define BLE_FLASHER_MAX_VERSION_LEN     246u        


typedef enum
{
    BLE_FLASHER_CMD_INVALID_CMD = 0,                            
    BLE_FLASHER_CMD_APPLICATION_STARTED,                        
    BLE_FLASHER_CMD_PREPARE_NEW_APPLICATION_TRANSFER,           
    BLE_FLASHER_CMD_PREPARE_NEW_APPLICATION_TRANSFER_ACK,       
    BLE_FLASHER_CMD_WRITE_BLOCK,                                
    BLE_FLASHER_CMD_WRITE_BLOCK_ACK,                            
    BLE_FLASHER_CMD_FINNISH_APPLICATION_TRANSFER,               
    BLE_FLASHER_CMD_FINNISH_APPLICATION_TRANSFER_ACK,           
    BLE_FLASHER_CMD_READ_FLASH,                                 
    BLE_FLASHER_CMD_READ_FLASH_DATA_ACK,                        
    BLE_FLASHER_CMD_JUMP_TO_BOOTLOADER,                         

    BLE_FLASHER_CMD_CODE_MAX
}BLE_FLASHER_PROTOCOL_COMMAND_CODES;


typedef enum
{
    BLE_FLASHER_FILE_FORMAT_S19 = 0,                    
    BLE_FLASHER_FILE_FORMAT_RAW,                        
    BLE_FLASHER_FILE_FORMAT_MAX                         
}enum_BLE_FLASHER_FILE_FORMAT_TYPE;



typedef struct
{
        uint32_t line_size;
        uint32_t format_type;
}strutc_BLE_FLASHER_PREPARE_NEW_APPLICATION_TRANSFER;


void ble_flasher_init(void);



uint32_t ble_flasher_is_flashing(void);


void ble_flasher_send_startup_msg(void);

void ble_flasher_check_flash_timeout(void);







#endif 


