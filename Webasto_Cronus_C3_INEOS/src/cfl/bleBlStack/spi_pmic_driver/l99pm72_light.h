#ifndef _L99PM72_LIGHT_H_
#define _L99PM72_LIGHT_H_
#include "l99pm72_base.h"

uint8_t l99pm72hw_light_spi_read_write(uint16_t* data_tx, uint16_t* data_rx);

enum_L99PM72_ERR_CODE_TYPE l99pm72hw_light_execute_command(enum_L99PM72_OPCODE_TYPE cmd, uint8_t address, uint16_t data_tx, uint16_t* data_rx);

enum_L99PM72_ERR_CODE_TYPE l99pm72_light_get_raw(uint8_t address, enum_L99PM72_ADDR_SPACE_TYPE is_eeprom_address);

uint16_t l99pm72_light_generate_registerset(uint8_t address);

enum_L99PM72_ERR_CODE_TYPE l99pm72_light_unpack_receivebyte(uint8_t register_address, enum_L99PM72_OPCODE_TYPE operational_code, uint16_t data);

void l99pm72_light_statusbyte_store(uint8_t status_byte);

uint16_t l99pm72_light_watchdog_mode2time( enum_L99PM72_WINDOW_WATCHDOG_TRIGGER_TIME_TYPE mode );

uint8_t l99pm72_light_trigger_watchdog_forced(void);


#endif 


