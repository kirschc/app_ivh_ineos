#ifndef _BL_FLASH_H_
#define _BL_FLASH_H_

#include "hal_data_types.h"
#include "sfl_bl_protocol.h"

#define DEBUG_UART 0


#define FLASH_BLOCKSIZE             0x8000u
#define FLASH_PAGESIZE              0x100u   

#define FLASH_BLOCK_OFFSET          32u  
#define FLASH_BLOCK_AVAILABLE       44u 


#define FLASH_APPL_START_ALL        0xD0000u
#define FLASH_APPL_END_ALL          0x180000u 

#define FLASH_APPL_HEADER_START     0xD0000u
#define FLASH_APPL_HEADER_OFFSET    0x8u
#define FLASH_APPL_HEADER_LENGTH    0x1000u

#define FLASH_APPL_START            FLASH_APPL_HEADER_START + FLASH_APPL_HEADER_LENGTH
#define FLASH_APPL_END              0x17ffffu



typedef enum
{
    BL_RH850_FLASH_OK,                          
    BL_RH850_FCL_BUSY,                          
    BL_RH850_FCL_SUSPENDED,                     
    BL_RH850_FCL_ERR_FLMD0,                     
    BL_RH850_FCL_ERR_PARAMETER,                 
    BL_RH850_FCL_ERR_PROTECTION,                
    BL_RH850_FCL_ERR_REJECTED,                  
    BL_RH850_FCL_ERR_FLOW,                      
    BL_RH850_FCL_ERR_WRITE,                     
    BL_RH850_FCL_ERR_ERASE,                     
    BL_RH850_FCL_ERR_COMMAND,                   
    BL_RH850_FCL_ERR_SUSPEND_FAILED,            
    BL_RH850_FCL_CANCELLED,                     
    BL_RH850_FCL_ERR_INTERNAL,                  
    BL_RH850_FLASH_ERR_S19_CRC,                 
    BL_RH850_FLASH_ERR_APP_CRC,                 
    BL_RH850_FLASH_ERR_RANGE,                   
    BL_RH850_FLASH_ERR_S19_FORMAT,              
    BL_RH850_FLASH_ERR_INCOMPATIBLE_FLASH_HEADER,      
    BL_RH850_FLASH_ERR_WRONG_SIGNATURE          
} enum_FLASH_ERR_CODES_TYPE;


typedef enum
{
    FLASH_MODE_ERASE = 0u,   
    FLASH_MODE_PROG,   
} enum_FLASH_MODE;

enum E_FLASH_STATE
{
  E_FLASH_RECORD_WAIT_START_CHAR = 0   ,
  E_FLASH_RECORD_READ                  ,
  E_FLASH_RECORD_PROCESS               ,
};

typedef struct
{
        enum_FLASH_MODE flash_mode;
        uint32_t flash_block;
        uint32_t flash_addr;
        uint8_t buffer[500];
        uint8_t header_buffer[500];
        enum_FLASH_ERR_CODES_TYPE status;
}struct_flash_handler_data;



enum_FLASH_ERR_CODES_TYPE CheckHeader(void);

uint8_t sfl_flash_clear(void);
uint32_t sfl_S19_flasher(uint8_t* s19_line_data, uint32_t s19_line_length);

enum_FLASH_ERR_CODES_TYPE flash_appl_clear(uint32_t start_address, uint32_t end_address, uint16_t *error_sector );

enum_FLASH_ERR_CODES_TYPE flash_s19_record(uint16_t record_startpos);

uint8_t check_application_crc(void);

enum_FLASH_ERR_CODES_TYPE flash_handler(void);


#endif


