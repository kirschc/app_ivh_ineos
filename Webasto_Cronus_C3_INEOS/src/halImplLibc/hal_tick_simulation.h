#ifndef _HAL_TICK_SIMULATION_H_
#define _HAL_TICK_SIMULATION_H_


#include "hal_data_types.h"
#include "hal_tick.h"






enum_HAL_TICK_RETURN_VALUE hal_set_tick_precision(enum_HAL_PRECISION precision);

enum_HAL_TICK_RETURN_VALUE hal_set_timestamp(uint32_t timestamp, enum_HAL_PRECISION precision);

enum_HAL_TICK_RETURN_VALUE hal_increase_timestamp(uint32_t number, enum_HAL_PRECISION precision);

#endif 


