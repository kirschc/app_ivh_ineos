#ifndef HAL_SYS_SIMULATION_H
#define HAL_SYS_SIMULATION_H

#include "hal_data_types.h"
#include "hal_sys.h"






void hal_sys_set_app_header(struct_hal_sys_app_header header);

void hal_sys_set_app_update_header(struct_hal_sys_app_header update_header);

void hal_sys_set_bl_access_key(uint32_t key);

#endif


