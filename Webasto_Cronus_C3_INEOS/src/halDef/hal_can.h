#ifndef HAL_CAN_H
#define HAL_CAN_H

#include "hal_data_types.h"

#ifdef RTOS_ENABLE
    #include "FreeRTOS.h"
    #include "semphr.h"
#endif

#define HAL_CAN_DLC_MAX         8u


typedef enum
{
    HAL_CAN_BAUD_INVALID     = 0,
    HAL_CAN_BAUD_10             ,
    HAL_CAN_BAUD_20             ,
    HAL_CAN_BAUD_33             ,
    HAL_CAN_BAUD_50             ,
    HAL_CAN_BAUD_83             ,
    HAL_CAN_BAUD_100            ,
    HAL_CAN_BAUD_125            ,
    HAL_CAN_BAUD_250            ,
    HAL_CAN_BAUD_500            ,
    HAL_CAN_BAUD_1000           ,
    HAL_CAN_BAUD_MAX
}enum_HAL_CAN_BAUDRATE;

typedef enum
{
    HAL_CAN_OK                        = 0u, 
    HAL_CAN_ERROR_GENERAL                 , 
    HAL_CAN_ERROR_INIT_FAILED             , 
    HAL_CAN_ERROR_NO_CHANNEL_LEFT         , 
    HAL_CAN_ERROR_NO_MESSAGE              , 
    HAL_CAN_ERROR_SET_FILTER              , 
    HAL_CAN_ERROR_CHANNEL_INVALID         , 
    HAL_CAN_ERROR_WHILE_READING           , 
    HAL_CAN_ERROR_WHILE_WRITING           , 
    HAL_CAN_ERROR_BUSY                    , 
    HAL_CAN_ERROR_READ_EMPTY              , 
    HAL_CAN_ERROR_READ_RUN                , 
    HAL_CAN_ERROR_SET_CALLBACK            , 
    HAL_CAN_ERROR_BAUDRATE                , 
    HAL_CAN_ERROR_NOT_IMPLEMENTED         , 
    HAL_CAN_ERROR_DLC_INVALID             , 
    HAL_CAN_ERROR_RTOS_MUT_INVALID        , 
    HAL_CAN_ERROR_RTOS_MUT_TIMEOUT          

} enum_HAL_CAN_RETURN_VALUE;

typedef enum
{
    HAL_CAN_RX                        = 0u,   
    HAL_CAN_TX                                

} enum_HAL_CAN_DIRECTION;

typedef uint32_t hal_canid_t;


typedef struct
{
    hal_canid_t  can_id;                                    
    uint8_t      can_dlc;                                   
    uint8_t      data[8] __attribute__((aligned(8)));       
    uint8_t      *ptr_data __attribute__((aligned(4)));     
    uint16_t     can_timestamp;                             
} struct_hal_can_frame;

typedef enum_HAL_CAN_RETURN_VALUE (*hal_can_callback_function_type)(struct_hal_can_frame* ptr_can_msg);

typedef struct
{
    void*   can_handle_ptr;                             
    int32_t can_handle_number;                          
    int32_t can_handle_mb_idx;                          
    enum_HAL_CAN_BAUDRATE can_baudrate;                 
    enum_HAL_CAN_BAUDRATE can_usr_baudrate;             
    enum_HAL_CAN_DIRECTION can_handle_direction;        
    hal_can_callback_function_type cb_function;         
} struct_hal_can_handle;

typedef struct
{
    hal_canid_t  can_id;    
    hal_canid_t  can_mask;  
} struct_hal_can_filter;


#if defined(RTOS_ENABLE) && defined(configUSE_PREEMPTION)
enum_HAL_CAN_RETURN_VALUE hal_can_init(struct_hal_can_handle *const ptr_can_handle, const uint8_t bus_id, const SemaphoreHandle_t ptr_mutex);
#else
enum_HAL_CAN_RETURN_VALUE hal_can_init(struct_hal_can_handle *const ptr_can_handle, const uint8_t bus_id);
#endif

enum_HAL_CAN_RETURN_VALUE hal_can_deinit(struct_hal_can_handle* ptr_can_handle);

enum_HAL_CAN_RETURN_VALUE hal_can_send(const struct_hal_can_handle *const ptr_can_handle, const struct_hal_can_frame *const ptr_can_msg);

enum_HAL_CAN_RETURN_VALUE hal_can_receive(const struct_hal_can_handle* ptr_can_handle, struct_hal_can_frame* ptr_can_msg);

enum_HAL_CAN_RETURN_VALUE hal_can_set_filter(const struct_hal_can_handle* ptr_can_handle, const struct_hal_can_filter* ptr_can_filter);

enum_HAL_CAN_RETURN_VALUE hal_can_set_callback(struct_hal_can_handle* ptr_can_handle, hal_can_callback_function_type ptr_cb_function);

enum_HAL_CAN_RETURN_VALUE hal_can_dump(const struct_hal_can_frame* ptr_can_msg);

enum_HAL_CAN_RETURN_VALUE hal_can_set_baudrate(struct_hal_can_handle* ptr_can_handle, enum_HAL_CAN_BAUDRATE baudrate);

enum_HAL_CAN_RETURN_VALUE hal_can_get_baudrate(struct_hal_can_handle* ptr_can_handle, enum_HAL_CAN_BAUDRATE* baudrate);

#endif 



