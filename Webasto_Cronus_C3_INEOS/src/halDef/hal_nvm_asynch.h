#ifndef HAL_NVM_ASYNCH_H
#define HAL_NVM_ASYNCH_H

#include "hal_data_types.h"


#define HAL_NVM_ASYNCH_STATE_TIMEOUT_MS          500u   

#define HAL_NVM_ASYNCH_LEN_RD_WR_FULL_BLOCK     0xFFu   



enum_HAL_NVM_RETURN_VALUE   hal_nvm_eeprom_asynch_get_status_of_handler( void );
enum_HAL_NVM_RETURN_VALUE   hal_nvm_eeprom_asynch_get_status_via_nvm_addr( const uint32_t usr_addr );
enum_HAL_NVM_RETURN_VALUE   hal_nvm_eeprom_asynch_init( void );
void                        hal_nvm_eeprom_asynch_process( void );
enum_HAL_NVM_RETURN_VALUE   hal_nvm_eeprom_asynch_read( const uint32_t usr_addr, const uint32_t usr_data_len, uint8_t * const usr_data_ptr );
enum_HAL_NVM_RETURN_VALUE   hal_nvm_eeprom_asynch_write( const uint32_t usr_addr, const uint32_t usr_data_len, const uint8_t * const usr_data_ptr );


#if( ((HAL_NVM_ASYNCH_STATE_TIMEOUT_MS) <  100u) ||\
     ((HAL_NVM_ASYNCH_STATE_TIMEOUT_MS) > 1000u) )
    #error "Analyze 'HAL_NVM_ASYNCH_STATE_TIMEOUT_MS'!"
#endif

#endif 

