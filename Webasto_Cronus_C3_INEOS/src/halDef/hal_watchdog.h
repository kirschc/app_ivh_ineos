#ifndef HAL_WATCHDOG_H
#define HAL_WATCHDOG_H

#include "hal_data_types.h"



typedef struct
{
	uint8_t idx;
	uint16_t watchdog_no;
	uint8_t status;
} watchdog_tbl_t;



void hal_watchdog_reset(uint8_t idx);


void hal_watchdog_callback(void);
void hal_watchdog_check_trigger(void);
void hal_watchdog_tbl_clear(void);

#endif


