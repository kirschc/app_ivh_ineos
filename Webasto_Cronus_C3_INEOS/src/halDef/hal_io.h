#ifndef HAL_IO_H
#define HAL_IO_H

#include "hal_data_types.h"

#ifdef RTOS_ENABLE
    #include "FreeRTOS.h"
    #include "semphr.h"
#endif




typedef enum
{
    HAL_IO_OK						= 0u,   
    HAL_IO_ERROR_GENERAL				,   
    HAL_IO_ERROR_INIT_FAILED			,   
	HAL_IO_ERROR_DEINIT_FAILED			,   
	HAL_IO_ERROR_CHANNEL_INVALID		,   
	HAL_IO_ERROR_NO_CHANNEL_LEFT		,   
	HAL_IO_ERROR_NO_DO					,   
	HAL_IO_ERROR_NO_DI					,   
	HAL_IO_ERROR_PWM_MASTER             ,   
	HAL_IO_ERROR_NOT_IMPLEMENTED        ,   
	HAL_IO_ERROR_ADC_MAX_NOT_DEFINED    ,   
	HAL_IO_ERROR_RTOS_MUT_INVALID       ,   
	HAL_IO_ERROR_RTOS_MUT_TIMEOUT           

} enum_HAL_IO_RETURN_VALUE;


#if defined(RTOS_ENABLE) && defined(configUSE_PREEMPTION)
enum_HAL_IO_RETURN_VALUE hal_io_init(const SemaphoreHandle_t ptr_mutex);
#else
enum_HAL_IO_RETURN_VALUE hal_io_init(void);
#endif

enum_HAL_IO_RETURN_VALUE hal_io_deinit(void);

enum_HAL_IO_RETURN_VALUE hal_io_di_get(uint16_t pin, uint8_t* ptr_state);

enum_HAL_IO_RETURN_VALUE hal_io_do_readback(uint16_t pin, uint8_t* ptr_state);

enum_HAL_IO_RETURN_VALUE hal_io_do_set(const uint16_t pin, const uint8_t state);

enum_HAL_IO_RETURN_VALUE hal_io_do_toggle(const uint16_t pin);

enum_HAL_IO_RETURN_VALUE hal_io_ai_get_digits(uint16_t pin, uint16_t* ptr_val);

enum_HAL_IO_RETURN_VALUE hal_io_ai_get_mV(uint16_t pin, uint16_t* ptr_val);

enum_HAL_IO_RETURN_VALUE hal_io_ao_set_digits(uint16_t pin, uint16_t val);


enum_HAL_IO_RETURN_VALUE hal_io_ao_set_mV(uint16_t pin, uint16_t val);

enum_HAL_IO_RETURN_VALUE hal_io_pwm_duty_readback(uint16_t pin, uint16_t* ptr_duty);

enum_HAL_IO_RETURN_VALUE hal_io_pwm_duty_set(uint16_t pin, uint16_t duty);

enum_HAL_IO_RETURN_VALUE hal_io_pwm_freq_readback(uint16_t pin, uint32_t* ptr_freq);

enum_HAL_IO_RETURN_VALUE hal_io_pwm_freq_set(uint16_t pin, uint32_t freq);

enum_HAL_IO_RETURN_VALUE hal_io_pwm_process_cyclic(void);


#endif


