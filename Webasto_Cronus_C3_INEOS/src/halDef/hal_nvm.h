#ifndef HAL_NVM_H
#define HAL_NVM_H



typedef enum
{
    HAL_NVM_OK						= 0u,     
	HAL_NVM_ERROR_GENERAL				,     
	HAL_NVM_ERROR_INIT_FAILED			,     
	HAL_NVM_ERROR_DEINIT_FAILED			,     
	HAL_NVM_ERROR_WHILE_READING			,     
	HAL_NVM_ERROR_WHILE_WRITING			,     
	HAL_NVM_ERROR_BLOCK_NO_INVALID  	,     
	HAL_NVM_ERROR_DATA_LEN_INVALID		,	  
	HAL_NVM_ERROR_DATA_ADDR_INVALID     ,     
	HAL_NVM_ERROR_NOT_IMPLEMENTED       ,     
    HAL_NVM_ERROR_ASYNCH_BUSY           ,     
    HAL_NVM_ERROR_ASYNCH_PENDING        ,     
    HAL_NVM_ERROR_ASYNCH_DATA_DISPATCH  ,     
    HAL_NVM_ERROR_ASYNCH_DATA_GATHER          

} enum_HAL_NVM_RETURN_VALUE;




enum_HAL_NVM_RETURN_VALUE hal_nvm_init(void);

enum_HAL_NVM_RETURN_VALUE hal_nvm_deinit(void);

enum_HAL_NVM_RETURN_VALUE hal_nvm_eeprom_write(uint32_t addr, uint32_t len, const uint8_t* ptr_data);

enum_HAL_NVM_RETURN_VALUE hal_nvm_eeprom_read(uint32_t addr, uint32_t len, uint8_t* ptr_data);

#endif


