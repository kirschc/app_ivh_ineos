#ifndef HAL_SPI_H
#define HAL_SPI_H
#include "hal_data_types.h"



typedef enum
{
    HAL_SPI_OK                      = 0u,     
    HAL_SPI_ERROR_GENERAL               ,     
    HAL_SPI_ERROR_INIT_FAILED           ,     
    HAL_SPI_ERROR_DEINIT_FAILED         ,     
    HAL_SPI_ERROR_CHANNEL_INVALID       ,     
    HAL_SPI_ERROR_NO_CHANNEL_LEFT       ,     
    HAL_SPI_ERROR_NO_JOB_LEFT           ,     
    HAL_SPI_ERROR_NO_SEQ_LEFT           ,     
    HAL_SPI_ERROR_SET_CALLBACK          ,     
    HAL_SPI_ERROR_SET_BAUDRATE          ,     
    HAL_SPI_ERROR_TX_DATA               ,     
    HAL_SPI_ERROR_RX_DATA               ,     
    HAL_SPI_ERROR_WHILE_READING         ,     
    HAL_SPI_ERROR_WHILE_WRITING         ,     
    HAL_SPI_ERROR_NOT_IMPLEMENTED             

} enum_HAL_SPI_RETURN_VALUE;


typedef struct
{
    void* ptr_handle;
    uint8_t channel;
    uint8_t job;
    uint8_t seq;
    uint16_t* ptr_tx_data;
    uint16_t* ptr_rx_data;

}struct_hal_spi_handle;


enum_HAL_SPI_RETURN_VALUE hal_spi_init(struct_hal_spi_handle* ptr_spi_handle, uint8_t spi_channel, uint8_t spi_job, uint8_t spi_seq);

enum_HAL_SPI_RETURN_VALUE hal_spi_deinit(struct_hal_spi_handle* ptr_spi_handle);

enum_HAL_SPI_RETURN_VALUE hal_spi_send(struct_hal_spi_handle* ptr_spi_handle);


enum_HAL_SPI_RETURN_VALUE hal_spi_receive(struct_hal_spi_handle* ptr_spi_handle);

#endif


