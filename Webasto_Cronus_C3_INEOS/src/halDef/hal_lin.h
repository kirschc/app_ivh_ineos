#ifndef HAL_LIN_H
#define HAL_LIN_H
#include "hal_data_types.h"

#define HAL_LIN_DATA_MAX               8u
#define HAL_LIN_SET_WAKE_UP            1u
#define HAL_LINE_FRAME_SEPARATE_MODE   1u

#define LIN_COMMUNICATIONACTIVE_TIMEOUT 10000u

typedef enum
{
    HAL_LIN_OK                      = 0u,     
    HAL_LIN_ERROR_NO_CHANNEL_LEFT       ,     
    HAL_LIN_ERROR_DATA_LEN_INVALID      ,     
    HAL_LIN_ERROR_DIR_INVALID           ,     
    HAL_LIN_ERROR_CRC_INVALID           ,     
    HAL_LIN_ERROR_NODE_INVALID          ,     
    HAL_LIN_ERROR_RESPONSE_INVALID            

}enum_HAL_LIN_RETURN_VALUE;

typedef enum
{
    HAL_LIN_NODE_MASTER             = 0u,
    HAL_LIN_NODE_SLAVE                  ,
    HAL_LIN_NODE_INVALID

}enum_HAL_LIN_NODE;

typedef enum
{
    HAL_LIN_DIR_RX                  = 0u,
    HAL_LIN_DIR_TX                      ,
    HAL_LIN_DIR_INVALID

}enum_HAL_LIN_DIR;

typedef enum
{
    HAL_LIN_CRC_CLASSIC             = 0u,
    HAL_LIN_CRC_ENHANCED                ,
    HAL_LIN_CRC_INVALID

}enum_HAL_LIN_CRC_TYPE;

typedef enum
{
    HAL_LIN_RESPONSE_OFF            = 0u,
    HAL_LIN_RESPONSE_ON                 ,
    HAL_LIN_RESPONSE_INVALID

}enum_HAL_LIN_RESPONSE;


typedef void (*hal_lin_transmit_header_callback_t)(uint8_t);
typedef void (*hal_lin_receive_header_callback_t)(uint8_t);
typedef void (*hal_lin_transmit_frame_callback_t)(uint8_t);
typedef void (*hal_lin_receive_frame_callback_t)(uint8_t);
typedef void (*hal_lin_receive_1st_data_callback_t)(uint8_t);
typedef void (*hal_lin_error_callback_t)(uint8_t, uint8_t);

typedef struct
{
    hal_lin_transmit_header_callback_t hal_lin_transmit_header_callback;
    hal_lin_receive_header_callback_t hal_lin_receive_header_callback;
    hal_lin_transmit_frame_callback_t hal_lin_transmit_frame_callback;
    hal_lin_receive_frame_callback_t hal_lin_receive_frame_callback;
    hal_lin_receive_1st_data_callback_t hal_lin_receive_1st_data_callback;
    hal_lin_error_callback_t hal_lin_error_callback;

}struct_hal_lin_callback_handle;

typedef struct
{
    void* ptr_handle;
    uint8_t channel;
    uint8_t node;
    struct_hal_lin_callback_handle hal_lin_callback_handle;

}struct_hal_lin_handle;

typedef struct
{
    uint8_t                 id;             
    uint8_t                 prot_id;        
    enum_HAL_LIN_DIR        dir;            
    enum_HAL_LIN_CRC_TYPE   crc_type;       
    uint8_t                 len;            

}struct_lin_frame;

typedef struct
{
    uint8_t len;                        
    uint8_t tx_data[HAL_LIN_DATA_MAX];  

}struct_lin_tx_msg;

typedef struct
{
    uint8_t id;                        
    uint8_t len;                       
    uint8_t crc_val;                   
    uint8_t rx_data[HAL_LIN_DATA_MAX]; 

}struct_lin_rx_msg;



enum_HAL_LIN_RETURN_VALUE hal_lin_init(struct_hal_lin_handle* ptr_lin_handle, uint8_t channel, enum_HAL_LIN_NODE node);

enum_HAL_LIN_RETURN_VALUE hal_lin_set_baudrate(struct_hal_lin_handle* ptr_lin_handle, uint32_t baudrate);

enum_HAL_LIN_RETURN_VALUE hal_lin_set_callback(struct_hal_lin_handle*               ptr_lin_handle,
                                               hal_lin_transmit_header_callback_t   ptr_lin_transmit_header_callback,
                                               hal_lin_receive_header_callback_t    ptr_lin_receive_header_callback,
                                               hal_lin_transmit_frame_callback_t    ptr_lin_transmit_frame_callback,
                                               hal_lin_receive_frame_callback_t     ptr_lin_receive_frame_callback,
                                               hal_lin_receive_1st_data_callback_t  ptr_lin_receive_1st_data_callback,
                                               hal_lin_error_callback_t             ptr_lin_error_callback);

enum_HAL_LIN_RETURN_VALUE hal_lin_master_prepare_tx_header(struct_hal_lin_handle* ptr_lin_handle,
                                                           struct_lin_frame*      ptr_lin_frame);

enum_HAL_LIN_RETURN_VALUE hal_lin_master_prepare_tx_header_frame(struct_hal_lin_handle* ptr_lin_handle,
                                                                 struct_lin_frame*      ptr_lin_frame,
                                                                 struct_lin_tx_msg*     ptr_lin_tx_msg);

enum_HAL_LIN_RETURN_VALUE hal_lin_master_prepare_tx_frame(struct_hal_lin_handle* ptr_lin_handle,
                                                          struct_lin_tx_msg*     ptr_lin_tx_msg);

enum_HAL_LIN_RETURN_VALUE hal_lin_master_slave_rx_header_frame(struct_hal_lin_handle* ptr_lin_handle,
                                                               struct_lin_rx_msg*     ptr_lin_rx_msg);

enum_HAL_LIN_RETURN_VALUE hal_lin_master_prepare_tx_wake_up(struct_hal_lin_handle* ptr_lin_handle);

enum_HAL_LIN_RETURN_VALUE hal_lin_slave_prepare_rx_header(struct_hal_lin_handle* ptr_lin_handle);

enum_HAL_LIN_RETURN_VALUE hal_lin_slave_prepare_tx_frame(struct_hal_lin_handle* ptr_lin_handle,
                                                         struct_lin_frame*      ptr_lin_frame,
                                                         struct_lin_tx_msg*     ptr_lin_tx_msg);

enum_HAL_LIN_RETURN_VALUE hal_lin_slave_prepare_rx_frame(struct_hal_lin_handle* ptr_lin_handle,
                                                         struct_lin_frame*      ptr_lin_frame);

enum_HAL_LIN_RETURN_VALUE hal_lin_slave_prepare_tx_no_frame(struct_hal_lin_handle* ptr_lin_handle);

enum_HAL_LIN_RETURN_VALUE hal_lin_master_slave_get_rx_msg(struct_hal_lin_handle* ptr_lin_handle,
                                                          struct_lin_rx_msg*     ptr_lin_rx_msg);

enum_HAL_LIN_RETURN_VALUE hal_lin_master_slave_set_tx_msg(struct_hal_lin_handle* ptr_lin_handle,
                                                          struct_lin_tx_msg*     ptr_lin_tx_msg);





#endif


