#ifndef HAL_SYS_H
#define HAL_SYS_H

#include "hal_data_types.h"

#define HAL_SYS_APP_HEADER_VERSION  (1u)    

#define HAL_SYS_APP_HEADER_KEY          (0x12345678u)   

#define HAL_SYS_BOOTLOADER_ACCESS_KEY   (0x4D525321u)   
#define HAL_SYS_BOOTLOADER_APP_START_KEY   (0x07412807u)   
#define HAL_SYS_BOOTLOADER_ACCESS_KEY_UPDATE   (0x1AF7356B)   


typedef struct
{
    uint32_t header_key;            
    uint32_t header_crc;            
    uint32_t app_header_version;    
    uint32_t application_crc;       
    uint32_t application_length;    
    uint8_t sw_version[32];         
}struct_hal_sys_app_header;

extern const struct_hal_sys_app_header ext_hal_sys_app_header __attribute__((section(".APP_HEADER")));

extern uint32_t ext_hal_sys_bl_access_key __attribute__((section(".BL_ACCESS_KEY")));

typedef enum
{
    HAL_SYS_RESET_TYPE_NONE = 0,        
    HAL_SYS_RESET_TYPE_SOFTRESET_APP,   
    HAL_SYS_RESET_TYPE_SOFTRESET_BL,    
    HAL_SYS_RESET_TYPE_SOFTRESET_BL_UPDATE_FROM_COPY,    
    HAL_SYS_RESET_TYPE_HARDRESET_WD,    
    HAL_SYS_RESET_TYPE_HARDRESET_CPU,   

    HAL_SYS_RESET_TYPE_START_APP        
}enum_HAL_SYS_RESET;

typedef void (*funct_ptr_appl_act_pre_reset)(const enum_HAL_SYS_RESET);


void hal_sys_init(const funct_ptr_appl_act_pre_reset funct_ptr_appl_act_pre_reset);

void hal_sys_reset(enum_HAL_SYS_RESET reset_mode);

#endif


