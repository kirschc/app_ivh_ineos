
#ifndef OS_H
#define OS_H




#include "Std_Types.h"


#define OS_SW_MAJOR_VERSION     1u

#define OS_SW_MINOR_VERSION     15u

#define OS_SW_PATCH_VERSION     0u

#define OS_MODULE_ID   ((uint8_t)0x01)

#define OS_VENDOR_ID   ((uint16_t)0)



void OS_StartUpOS(void);

 void OS_ShutDownOS(void);

 void OS_WaitXus(
    uint32_t x);

 uint32_t OS_GetBaseTimer(void);

 void OS_GetVersionInfo(
    Std_VersionInfoType *versioninfo_pt);





#else
    #error Multiple include of "os.h"
#endif 


