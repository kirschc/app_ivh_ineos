
#ifndef PORTMACRO_H
#define PORTMACRO_H

#include "Std_Types.h"

#ifdef __cplusplus
extern "C" {
#endif


#define portCHAR        char
#define portFLOAT       float
#define portDOUBLE      double
#define portLONG        long
#define portSHORT       short
#define portSTACK_TYPE  uint32_t 
#define portBASE_TYPE   int
typedef portSTACK_TYPE StackType_t;

typedef int BaseType_t;
typedef unsigned UBaseType_t;

#if (configUSE_16_BIT_TICKS==1)
	typedef unsigned portSHORT portTickType;
	typedef uint16_t TickType_t;						
	#define portMAX_DELAY ( portTickType ) 0xffff
#else
	typedef unsigned portLONG portTickType;
	typedef uint32_t TickType_t;						
	#define portMAX_DELAY ( portTickType ) 0xffffffff
#endif

#define portDISABLE_INTERRUPTS()    asm ( "di" )
#define portENABLE_INTERRUPTS()	    asm ( "ei" )

#define portNO_CRITICAL_SECTION_NESTING		( ( unsigned portBASE_TYPE ) 0 )

#define portENTER_CRITICAL()														\
{																					\
extern volatile  portSTACK_TYPE usCriticalNesting;			\
																					\
	portDISABLE_INTERRUPTS();														\
																					\
					\
			\
									\
	usCriticalNesting++;															\
}

#define portEXIT_CRITICAL()															\
{																					\
extern volatile  portSTACK_TYPE usCriticalNesting;			\
																					\
	if( usCriticalNesting > portNO_CRITICAL_SECTION_NESTING )						\
	{																				\
				\
		usCriticalNesting--;														\
																					\
				\
																	\
		if( usCriticalNesting == portNO_CRITICAL_SECTION_NESTING )					\
		{																			\
			portENABLE_INTERRUPTS();												\
		}																			\
	}																				\
}

#define vApplicationTickHook            OS_ApplTickHook
#define vApplicationIdleHook            OS_ApplIdleHook

extern void vPortYield( void );
extern void vPortStart( void );
extern void portSAVE_CONTEXT( void );
extern void portRESTORE_CONTEXT( void );
#define portYIELD()	__asm ( "trap 0" )
#define portNOP()	__asm ( "NOP" )
extern void vTaskSwitchContext( void );
#define portYIELD_FROM_ISR( xHigherPriorityTaskWoken ) if( xHigherPriorityTaskWoken ) vTaskSwitchContext()

extern void OS_ApplTickHook(void);
extern void OS_ApplIdleHook(void);


#define portBYTE_ALIGNMENT	4
#define portSTACK_GROWTH	( -1 )
#define portTICK_RATE_MS	( ( portTickType ) 1000 / configTICK_RATE_HZ )

#define portTASK_FUNCTION_PROTO( vFunction, pvParameters ) void vFunction( void *pvParameters )
#define portTASK_FUNCTION( vFunction, pvParameters ) void vFunction( void *pvParameters )

#ifdef __cplusplus
}
#endif

#endif 



