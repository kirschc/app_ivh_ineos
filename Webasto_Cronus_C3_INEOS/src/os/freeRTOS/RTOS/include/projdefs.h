
#ifndef PROJDEFS_H
#define PROJDEFS_H

typedef void (*TaskFunction_t)( void * );

#ifndef pdMS_TO_TICKS
	#define pdMS_TO_TICKS( xTimeInMs ) ( ( TickType_t ) ( ( ( TickType_t ) ( xTimeInMs ) * ( TickType_t ) configTICK_RATE_HZ ) / ( TickType_t ) 1000 ) )
#endif

#define pdFALSE			( ( BaseType_t ) 0 )
#define pdTRUE			( ( BaseType_t ) 1 )

#define pdPASS			( pdTRUE )
#define pdFAIL			( pdFALSE )
#define errQUEUE_EMPTY	( ( BaseType_t ) 0 )
#define errQUEUE_FULL	( ( BaseType_t ) 0 )

#define errCOULD_NOT_ALLOCATE_REQUIRED_MEMORY	( -1 )
#define errQUEUE_BLOCKED						( -4 )
#define errQUEUE_YIELD							( -5 )

#ifndef configUSE_LIST_DATA_INTEGRITY_CHECK_BYTES
	#define configUSE_LIST_DATA_INTEGRITY_CHECK_BYTES 0
#endif

#if( configUSE_16_BIT_TICKS == 1 )
	#define pdINTEGRITY_CHECK_VALUE 0x5a5a
#else
	#define pdINTEGRITY_CHECK_VALUE 0x5a5a5a5aUL
#endif

#define pdFREERTOS_ERRNO_NONE			0	
#define	pdFREERTOS_ERRNO_ENOENT			2	
#define	pdFREERTOS_ERRNO_EINTR			4	
#define	pdFREERTOS_ERRNO_EIO			5	
#define	pdFREERTOS_ERRNO_ENXIO			6	
#define	pdFREERTOS_ERRNO_EBADF			9	
#define	pdFREERTOS_ERRNO_EAGAIN			11	
#define	pdFREERTOS_ERRNO_EWOULDBLOCK	11	
#define	pdFREERTOS_ERRNO_ENOMEM			12	
#define	pdFREERTOS_ERRNO_EACCES			13	
#define	pdFREERTOS_ERRNO_EFAULT			14	
#define	pdFREERTOS_ERRNO_EBUSY			16	
#define	pdFREERTOS_ERRNO_EEXIST			17	
#define	pdFREERTOS_ERRNO_EXDEV			18	
#define	pdFREERTOS_ERRNO_ENODEV			19	
#define	pdFREERTOS_ERRNO_ENOTDIR		20	
#define	pdFREERTOS_ERRNO_EISDIR			21	
#define	pdFREERTOS_ERRNO_EINVAL			22	
#define	pdFREERTOS_ERRNO_ENOSPC			28	
#define	pdFREERTOS_ERRNO_ESPIPE			29	
#define	pdFREERTOS_ERRNO_EROFS			30	
#define	pdFREERTOS_ERRNO_EUNATCH		42	
#define	pdFREERTOS_ERRNO_EBADE			50	
#define	pdFREERTOS_ERRNO_EFTYPE			79	
#define	pdFREERTOS_ERRNO_ENMFILE		89	
#define	pdFREERTOS_ERRNO_ENOTEMPTY		90	
#define	pdFREERTOS_ERRNO_ENAMETOOLONG 	91	
#define	pdFREERTOS_ERRNO_EOPNOTSUPP		95	
#define	pdFREERTOS_ERRNO_ENOBUFS		105	
#define	pdFREERTOS_ERRNO_ENOPROTOOPT	109	
#define	pdFREERTOS_ERRNO_EADDRINUSE		112	
#define	pdFREERTOS_ERRNO_ETIMEDOUT		116	
#define	pdFREERTOS_ERRNO_EINPROGRESS	119	
#define	pdFREERTOS_ERRNO_EALREADY		120	
#define	pdFREERTOS_ERRNO_EADDRNOTAVAIL 	125	
#define	pdFREERTOS_ERRNO_EISCONN		127	
#define	pdFREERTOS_ERRNO_ENOTCONN		128	
#define	pdFREERTOS_ERRNO_ENOMEDIUM		135	
#define	pdFREERTOS_ERRNO_EILSEQ			138	
#define	pdFREERTOS_ERRNO_ECANCELED		140	

#define pdFREERTOS_LITTLE_ENDIAN		0
#define pdFREERTOS_BIG_ENDIAN			1

#define pdLITTLE_ENDIAN					pdFREERTOS_LITTLE_ENDIAN
#define pdBIG_ENDIAN					pdFREERTOS_BIG_ENDIAN


#endif 





