
#ifndef OS_CONFIG_H
#define OS_CONFIG_H





#include "dr7f701057.dev.h"     




void OS_EnterStandbyMode(void);

void OS_ForceReset(void);

void OS_ErrorHook(void);

void OS_InitBaseTimer(void);

void OS_EnableBaseTimerInterrupt(void);

void OS_DisableBaseTimerInterrupt(void);

uint16_t OS_UserGetBaseTimer(void);

void OS_InitFreeRunningTimer(void);

void OS_InitRTOSTimeStatsTimer(void);

void OS_UserWaitXus(
    uint32_t X);

uint32_t Os_GetClockTime( void );

void Os_SetClockTime(
   uint32_t timestamp );

void Os_IncrementClockTime( void );





#else
    #error Multiple include of "os_config.h"
#endif 



