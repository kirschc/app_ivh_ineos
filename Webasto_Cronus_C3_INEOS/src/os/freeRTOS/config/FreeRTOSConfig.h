
#ifndef FREERTOS_CONFIG_H
#define FREERTOS_CONFIG_H

#ifndef __IO_MACROS__
    #include "can_db_tables.h"
#endif

#define RTOS_NUMBER_OF_USED_MUTEXES   6u 

#define configUSE_PREEMPTION                       1
#define configUSE_IDLE_HOOK                        1
#define configMAX_PRIORITIES                       7
#define configUSE_TICK_HOOK                        0
#define configCPU_CLOCK_HZ                         40000000
#define configTICK_RATE_HZ                         1000
#define configSTACK_DEPTH_TYPE                     uint32_t 
#define configMINIMAL_STACK_SIZE                   512 
#define configMINIMAL_STACK_SIZE_SYSTEM_1MS        1024 
#define configMINIMAL_STACK_SIZE_EOL               1024 
#define configMINIMAL_STACK_SIZE_1MS               1024 
#define configMINIMAL_STACK_SIZE_10MS              1024 
#define configMINIMAL_STACK_SIZE_20MS              1024 
#define configMINIMAL_STACK_SIZE_100MS             1024 
#define configMINIMAL_STACK_SIZE_CUSTOMER_1MS      1024 
#define configTOTAL_HEAP_SIZE                        ( RTOS_CALC_BYTE_USE_FOR_TASK(         (configMINIMAL_STACK_SIZE)                 ) ) \
                                                   + ( RTOS_CALC_BYTE_USE_FOR_TASK(         (configMINIMAL_STACK_SIZE_EOL)             ) ) \
                                                   + ( RTOS_CALC_BYTE_USE_FOR_TASK(         (configMINIMAL_STACK_SIZE_SYSTEM_1MS)      ) ) \
                                                   + ( RTOS_CALC_BYTE_USE_FOR_TASK(         (configMINIMAL_STACK_SIZE_1MS)             ) ) \
                                                   + ( RTOS_CALC_BYTE_USE_FOR_TASK(         (configMINIMAL_STACK_SIZE_10MS)            ) ) \
                                                   + ( RTOS_CALC_BYTE_USE_FOR_TASK(         (configMINIMAL_STACK_SIZE_20MS)            ) ) \
                                                   + ( RTOS_CALC_BYTE_USE_FOR_TASK(         (configMINIMAL_STACK_SIZE_100MS)           ) ) \
                                                   + ( RTOS_CALC_BYTE_USE_FOR_TASK(         (configMINIMAL_STACK_SIZE_CUSTOMER_1MS)    ) ) \
                                                   + ( RTOS_CALC_BYTE_USE_FOR_MUTEX(        (RTOS_NUMBER_OF_USED_MUTEXES)              ) ) \
                                                   + ( RTOS_CALC_BYTE_USE_FOR_CAN_RX_QUEUE( (CAN_RX_MSG_FULL_BUFFER_SIZE)              ) ) \
                                                   + ( (portBYTE_ALIGNMENT) ) 
#define RTOS_SIZE_OF_TCB_T                         88 
#define RTOS_SIZE_OF_HEAPSTRUCT_SIZE               8  
#define RTOS_SIZE_OF_QUEUE_T                       80 
#define RTOS_SIZE_OF_STRUCT_CAN_DB_RX_BUFFER       32 
#define RTOS_CALC_BYTE_USE_FOR_TASK(x)             ( ( (x * 4) + (RTOS_SIZE_OF_HEAPSTRUCT_SIZE)) + ((RTOS_SIZE_OF_TCB_T) + (RTOS_SIZE_OF_HEAPSTRUCT_SIZE) ) )
#define RTOS_CALC_BYTE_USE_FOR_MUTEX(x)            ( x * ( (RTOS_SIZE_OF_QUEUE_T) + (RTOS_SIZE_OF_HEAPSTRUCT_SIZE) ) )
#define RTOS_CALC_BYTE_USE_FOR_CAN_RX_QUEUE(x)     ( ( x * (RTOS_SIZE_OF_STRUCT_CAN_DB_RX_BUFFER) ) + (RTOS_SIZE_OF_QUEUE_T) + (RTOS_SIZE_OF_HEAPSTRUCT_SIZE) )
#define configMAX_TASK_NAME_LEN                    10 
#define configUSE_TRACE_FACILITY                   1  
#define configUSE_16_BIT_TICKS                     0
#define configIDLE_SHOULD_YIELD                    0
#define configUSE_MUTEXES                          1
#define configCHECK_FOR_STACK_OVERFLOW             2  
#define configGENERATE_RUN_TIME_STATS              0  
#define configENABLE_BACKWARD_COMPATIBILITY        0  
#define configAPPLICATION_ALLOCATED_HEAP           1  
#ifndef configAPPLICATION_ALLOCATED_HEAP
    #error 'size of ucHeap is dynamic with #configTOTAL_HEAP_SIZE and must be defined in non-pre-compiled area'
#endif

#define INCLUDE_vTaskPrioritySet                   1
#define INCLUDE_uxTaskPriorityGet                  1
#define INCLUDE_vTaskSuspend                       1
#define INCLUDE_vTaskDelayUntil                    1
#define INCLUDE_xTaskGetIdleTaskHandle             1  

#if (configGENERATE_RUN_TIME_STATS == 1)
    #define portCONFIGURE_TIMER_FOR_RUN_TIME_STATS() (OS_RTOSTimeStatsCounter = 0ul)

    #define portGET_RUN_TIME_COUNTER_VALUE()         (OS_RTOSTimeStatsCounter)
#endif

#if defined( _DEBUG )
	extern void vAssertCalled( void );
	#define configASSERT( x ) if( ( x ) == 0 ) vAssertCalled()
#endif


#endif 


