#ifndef __WBUS_TYPES_H_
#define __WBUS_TYPES_H_


typedef void WBUS_DRV_CALLBACK_t ( const uint16_t  com_med, const WBUS_INSTANCE_t wbus_ins, const WBUS_DRIVER_ERROR_t wbus_err );


typedef struct
{
    enum_WBUS_MEMBER_t ID;   
    uint32_t  wait_time;        

    uint8_t   retry_err_max;    
    uint16_t  retry_err_time;   

    uint8_t   retry_req_max;    
    uint16_t  retry_req_time;   

} struct_WBUS_DRIVER_UNIT_CFG_t;


typedef struct
{
    enum_WBUS_DRIVER_BAUDRATE_t ID;   
    uint32_t  baudrate;               
    uint32_t  byte_timeout;           
    uint32_t  resp_timeout_min;       
    uint32_t  resp_timeout_max;       
    uint32_t  msg_time_min;           
    uint32_t  msg_time_max;           
} struct_WBUS_DRIVER_BAUDRATE_CFG_t;


typedef struct
{
    WBUS_DEVICE_t                           wbus_dev;       
    enum_WBUS_DEVICE_PROPERTY_TYP_t         wbus_dev_typ;   

    enum_SCI_HW_CHANNEL_INDEX_t             wbus_dev_sci_hw_chl; 
    enum_WBUS_DEV_BSW_SW_CHANNEL_INDEX_t    wbus_dev_bsw_sw_chl; 

} struct_WBUS_DEVICE_CONFIGURATION_t;


typedef struct
{
    WBUS_DEVICE_t                       wbus_dev;           
    enum_WBUS_DEVICE_PROPERTY_TYP_t     wbus_dev_typ;       
    WBUS_DEVICE_STATE_t                 wbus_dev_state;     

    WBUS_INSTANCE_t                         bound_to_wbus_ins;   
    enum_WBUS_DEV_BSW_SW_CHANNEL_INDEX_t    bound_to_bsw_sw_chl; 

    volatile enum_WBUS_DRIVER_ERROR_t   wbus_dev_error;     
    volatile uint8_t                    wbus_dev_err_raw;   

    volatile WBUS_DEVICE_COM_STATE_t    wbus_dev_com_state; 

    volatile uint8_t                    *RX_p_buf;          
    volatile uint16_t                   RX_buf_free;        
    volatile uint16_t                   RX_buf_free_org;    
    volatile uint16_t                   RX_rec_number;      

    volatile uint8_t                    *TX_p_buf;          
    volatile uint16_t                   TX_buf_avail;       
    volatile uint16_t                   TX_buf_avail_org;   
    volatile uint16_t                   TX_trm_number;      

    volatile uint8_t                    InterruptsSuspended;    
    volatile uint16_t                   ICUARTUR0L_AtomicStart; 
    volatile uint16_t                   ICUARTUR1L_AtomicStart; 
    volatile uint16_t                   ICUARTUR2L_AtomicStart; 

    volatile uint32_t                   prp_byte_timeout_us;        
    volatile uint32_t                   prp_resp_timeout_us;        
    volatile uint32_t                   prp_wait_time_us;           

    uint16_t                            err_cnt_echo;       

} WBUS_DEVICE_CONTROL_t;


typedef struct
{
    enum_WBUS_DEV_BSW_SW_CHANNEL_INDEX_t    bsw_sw_chl;         
    Sci_RegisterSetType                     const *p_reg_set;   
    WBUS_DEVICE_t                           wbus_dev;           
} struct_WBUS_DEVICE_RELATED_BSW_SW_CHL_t;


typedef struct
{
    WBUS_INSTANCE_t         wbus_ins;		    

    WBUS_DEVICE_t           wbus_dev;           

} struct_WBUS_INSTANCE_CONFIGURATION_t;


typedef struct
{
    WBUS_INSTANCE_t         wbus_ins;		    
    WBUS_INSTANCE_STATE_t   wbus_ins_state;     

    WBUS_DEVICE_t           bound_to_wbus_dev;  

    WBUS_DEVICE_STATE_t		wbus_dev_state;     

    uint8_t                 wbus_mbr_cnt;       


    struct_WBUS_DRIVER_BAUDRATE_CFG_t   wbus_ins_baudrate;  

    enum_WBUS_DRIVER_ERROR_t            wbus_ins_error;     

    const WBUS_DRV_CALLBACK_t           *RX_callback;       
    const WBUS_DRV_CALLBACK_t           *TX_callback;       
    uint16_t           RX_com_med;         
    uint16_t           TX_com_med;         


} WBUS_INSTANCE_CONTROL_t;


#endif 


