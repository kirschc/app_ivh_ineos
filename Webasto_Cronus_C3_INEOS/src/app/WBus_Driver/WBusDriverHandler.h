
#ifndef WBUS_DRIVER_HANDLER_H
#define WBUS_DRIVER_HANDLER_H

void WBusDriver_WaitTimeExpired( WBUS_DEVICE_t wbus_dev );
void WBusDriver_InterbyteTimeoutExpired( WBUS_DEVICE_t wbus_dev );
void WBusDriver_ReceiveISRhandler( const uint8_t bsw_sw_chl, const uint8_t value);
void WBusDriver_StatusISRhandler( const uint8_t bsw_sw_chl );
void WBusDriver_TransmissionISRhandler( const uint8_t bsw_sw_chl );



#endif 


