
#ifndef WBUS_DRIVER_H
#define WBUS_DRIVER_H

extern uint8_t  ext_iug_entries_WBUS_INSTANCE;

extern uint32_t ext_wbus_dev_const_prep;
extern uint32_t ext_wbus_ins_const_prep;

extern const struct_WBUS_DEVICE_CONFIGURATION_t         WBUS_DEVICE_CFG[];
extern       WBUS_DEVICE_CONTROL_t                      WBUS_DEVICE[(WBUS_DEV_ENUM_MAX)];
extern       struct_WBUS_DEVICE_RELATED_BSW_SW_CHL_t    WBUS_DEVICE_BSW_SW_CHL[(WBUS_DEV_BSW_SW_CHANNEL_ENUM_MAX)];

extern const enum_WBUS_MEMBER_t                         WBUS_MEMBER_CFG[][(WBUS_INS_MEMBER_MAX)];
extern       enum_WBUS_MEMBER_t                         WBUS_MEMBER_NVM[(WBUS_INS_ENUM_MAX)][(WBUS_INS_MEMBER_MAX)];

extern const struct_WBUS_INSTANCE_CONFIGURATION_t       WBUS_INSTANCE_CFG[];
extern       WBUS_INSTANCE_CONTROL_t                    WBUS_INSTANCE[];

extern const struct_WBUS_DRIVER_UNIT_CFG_t              WBUS_DRV_UNIT_CFG[(WBUS_MBR_ENUM_MAX)];

extern const struct_WBUS_DRIVER_BAUDRATE_CFG_t          WBUS_DRV_BAUDRATE_CFG[(WBUS_DRV_BAUDRATE_ENUM_MAX)];



#define WBUS_DRIVER_BIT_WEIGHT( carrier )   IUG_Uty_BitWeight( (carrier) )


boolean WBusDriver_ChecksumCalculate( const uint8_t * const wbusMsg, const uint16_t bufLen, uint8_t * const checksum, const uint8_t mode );
boolean WBusDriver_ChecksumVerify( const uint8_t * const wbusMsg, const uint16_t bufLen );
void WBusDriver_EventReceivedCallback( void); 
void WBusDriver_EventSentCallback( void ); 


struct_WBUS_DRIVER_BAUDRATE_CFG_t * WBusDriver_GetBaudrateDataPtr( const WBUS_INSTANCE_t wbus_ins );
uint16_t                    WBusDriver_GetNumberReceived( const WBUS_INSTANCE_t wbus_ins );
uint16_t                    WBusDriver_GetNumberTransmitted( const WBUS_INSTANCE_t wbus_ins );
WBUS_INSTANCE_t        WBusDriver_Init(const WBUS_INSTANCE_t wbus_ins);
boolean                     WBusDriver_IsWaitTimeActive( const WBUS_INSTANCE_t wbus_ins );
boolean                     WBusDriver_IsReceptionActive( const WBUS_INSTANCE_t wbus_ins );
boolean                     WBusDriver_IsTransmissionActive( const WBUS_INSTANCE_t wbus_ins );
WBUS_INSTANCE_t        WBusDriver_SetBaudrate( const WBUS_INSTANCE_t wbus_ins, const enum_WBUS_DRIVER_BAUDRATE_t baud_rate_id );
WBUS_INSTANCE_t        WBusDriver_ResetCommunicationState( const WBUS_INSTANCE_t wbus_ins );
WBUS_INSTANCE_t        WBusDriver_SetOutputPolarity( const WBUS_INSTANCE_t wbus_ins, enum_WBUS_DEVIVE_INOUT_POLARITY_t wbus_pol_out );
enum_WBUS_DRIVER_BAUDRATE_t WBusDriver_TestBaudrate( const enum_WBUS_DRIVER_BAUDRATE_t baud_rate_id );
WBUS_DRIVER_ERROR_t         WBusDriver_ReadFrameAsync(  const WBUS_INSTANCE_t wbus_ins,
                                                              uint8_t * const addressOfDatabuffer,
                                                        const uint16_t sizeOfDatabuffer,
                                                              WBUS_DRV_CALLBACK_t * const Callback,
                                                        const uint16_t com_med );
WBUS_DRIVER_ERROR_t         WBusDriver_WriteFrameAsync( const WBUS_INSTANCE_t wbus_ins,
                                                              uint8_t * const ptrToFrameToSend,
                                                        const uint16_t sizeOfFrameToSend,
                                                              WBUS_DRV_CALLBACK_t * const Callback,
                                                        const uint16_t com_med );


#endif 


