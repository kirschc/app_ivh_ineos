
#ifndef WBUS_TIMER_H
#define WBUS_TIMER_H


#ifdef WBus_RTOS_Timer_Simulation
extern volatile uint8_t     WBusDriver_InterbyteTimeoutTrg[(WBUS_DEV_ENUM_MAX)]; 
extern          uint16_t    WBusDriver_InterbyteTimeoutCnt[(WBUS_DEV_ENUM_MAX)];
void WBusDriver_Timeout_Simulation(void);
#endif

void WBusDriver_WaitTimePrepare( WBUS_DEVICE_t wbus_dev );
void WBusDriver_WaitTimeStart( WBUS_DEVICE_t wbus_dev );
void WBusDriver_InterbyteTimeoutPrepare( WBUS_DEVICE_t wbus_dev );
void WBusDriver_InterbyteTimeoutStart( WBUS_DEVICE_t wbus_dev );
uint16_t WBusDriver_ResponseTimeoutGet_ms( const WBUS_INSTANCE_t wbus_ins, const GHIBUS_COM_HEATER_t idx_ghi_htr  );



#endif 


