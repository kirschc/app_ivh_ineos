#ifndef WBUS_DEVICE_H
#define WBUS_DEVICE_H


Sci_RegisterSetType const * Sci_GetRegisterSetAddress(uint8_t channel);

WBUS_DEVICE_t  WBusDevice_ActivateLINResetMode( const WBUS_DEVICE_t wbus_dev );

WBUS_DEVICE_t  WBusDevice_ActivateUARTMode( const WBUS_DEVICE_t wbus_dev );

WBUS_DEVICE_t  WBusDevice_AtomicEnd( const WBUS_DEVICE_t wbus_dev );

WBUS_DEVICE_t  WBusDevice_AtomicStart( const WBUS_DEVICE_t wbus_dev );

WBUS_DEVICE_t  WBusDevice_ClearErrorStatus ( const WBUS_DEVICE_t wbus_dev );

WBUS_DEVICE_t  WBusDevice_Construct(const struct_WBUS_DEVICE_CONFIGURATION_t *wbus_dev_cfg);

WBUS_DEVICE_t  WBusDevice_DisableReception( const WBUS_DEVICE_t wbus_dev );

WBUS_DEVICE_t  WBusDevice_DisableAllInterrupts( const WBUS_DEVICE_t wbus_dev );


#ifdef WBUS_DEV_CTL_INT_INDIVIDUAL
WBUS_DEVICE_t  WBusDevice_DisableRXInterrupts( const WBUS_DEVICE_t wbus_dev );
#endif

WBUS_DEVICE_t  WBusDevice_DisableTransmission( const WBUS_DEVICE_t wbus_dev );

#ifdef WBUS_DEV_CTL_INT_INDIVIDUAL
WBUS_DEVICE_t  WBusDevice_DisableTXInterrupts( const WBUS_DEVICE_t wbus_dev );
#endif

WBUS_DEVICE_t  WBusDevice_EnableReception( const WBUS_DEVICE_t wbus_dev );

WBUS_DEVICE_t  WBusDevice_EnableAllInterrupts( const WBUS_DEVICE_t wbus_dev );

#ifdef WBUS_DEV_CTL_INT_INDIVIDUAL
WBUS_DEVICE_t  WBusDevice_EnableRXInterrupts( const WBUS_DEVICE_t wbus_dev );
#endif

WBUS_DEVICE_t  WBusDevice_EnableTransmission( const WBUS_DEVICE_t wbus_dev );

#ifdef WBUS_DEV_CTL_INT_INDIVIDUAL
WBUS_DEVICE_t  WBusDevice_EnableTXInterrupts( const WBUS_DEVICE_t wbus_dev );
#endif


WBUS_DEVICE_t  WBusDevice_Init(const WBUS_DEVICE_t wbus_dev);

void                WBusDevice_InitBSW_Related(void);

uint8_t               WBusDevice_ReadErrorStatus( const WBUS_DEVICE_t wbus_dev );

uint8_t               WBusDevice_ReadByte( const WBUS_DEVICE_t wbus_dev );

WBUS_DEVICE_t WBusDevice_ResetCommunicationState( const WBUS_DEVICE_t wbus_dev );

WBUS_DEVICE_t  WBusDevice_SetBaudrate( const WBUS_DEVICE_t wbus_dev, const uint32_t baud_rate );

WBUS_DEVICE_t  WBusDevice_SetOutputPolarity( const WBUS_DEVICE_t wbus_dev, enum_WBUS_DEVIVE_INOUT_POLARITY_t wbus_pol_out );

WBUS_DEVICE_t  WBusDevice_WriteByte( const WBUS_DEVICE_t wbus_dev, const uint8_t data );


#endif 


