
#ifndef WBUS_INSTANCE_H
#define WBUS_INSTANCE_H

typedef enum
{
    MSKWBUSINS__                = 0x00u,    
    MSKWBUSINS_1                = 0x01u,    
    MSKWBUSINS_2                = 0x02u,    
    MSKWBUSINS__ENUM_FORCE_TYPE = 0x7F      

} IUG_WBUS_INS_MSK_t;


#define IUG_WBUS_INS_MSK_CREATE( ins )      ((MSKWBUSINS_1) << (ins) )


WBUS_INSTANCE_t WBusInstance_Bind(const WBUS_INSTANCE_t wbus_ins, const WBUS_DEVICE_t wbus_dev );
WBUS_INSTANCE_t WBusInstance_Construct(const struct_WBUS_INSTANCE_CONFIGURATION_t *wbus_ins_cfg, const enum_WBUS_MEMBER_t *wbus_mbr);

WBUS_INSTANCE_t WBusInstance_Init(const WBUS_INSTANCE_t wbus_ins);
WBUS_DEVICE_t   WBusInstance_GetBoundDevice( const WBUS_INSTANCE_t wbus_ins );
WBUS_INSTANCE_t WBusInstance_MemberAdd( const WBUS_INSTANCE_t wbus_ins, const enum_WBUS_MEMBER_t wbus_mbr );
WBUS_INSTANCE_t WBusInstance_MemberDel( const WBUS_INSTANCE_t wbus_ins, const enum_WBUS_MEMBER_t wbus_mbr );
uint8_t              WBusInstance_MemberPosition( const WBUS_INSTANCE_t wbus_ins, const enum_WBUS_MEMBER_t wbus_mbr );
WBUS_INSTANCE_t WBusInstance_MemberTest( const WBUS_INSTANCE_t wbus_ins, const enum_WBUS_MEMBER_t wbus_mbr );


#endif 


