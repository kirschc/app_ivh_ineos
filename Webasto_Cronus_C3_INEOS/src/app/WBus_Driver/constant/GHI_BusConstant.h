#ifndef _GHIBUS_CONSTANT_H_
#define _GHIBUS_CONSTANT_H_


#define GHIBUS_DRV_RESPONSE_TIMEOUT_MIN_MS       100u   
#define GHIBUS_DRV_RESPONSE_TIMEOUT_MAX_MS      5000u   
#define GHIBUS_DRV_RESPONSE_TIMEOUT_DEF_MS      3000u   



typedef struct
{
    uint16_t  resp_timeout_min;       
    uint16_t  resp_timeout_max;       

} IUG_GHI_HEATER_GHI_BUS_COMM_CFG_t;



#endif 



