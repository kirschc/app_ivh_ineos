#ifndef _WBUS_CONSTANT_HEATER_H_
#define _WBUS_CONSTANT_HEATER_H_



typedef enum
{
    HTRST_00_ABR_Ausbrennen             = 0x00u,    
    HTRST_01_ABSCH_Abschaltung          = 0x01u,    
    HTRST_02_ABT_AusbrennenTRS          = 0x02u,    
    HTRST_03_AKUE_AusbrennRampe         = 0x03u,    
    HTRST_04_AUS_State                  = 0x04u,    
    HTRST_05_BBTL_BrennerTeillast       = 0x05u,    
    HTRST_06_BBVL_BrennerVollast        = 0x06u,    
    HTRST_07_BFOE_BrennstoffFoerd       = 0x07u,    
    HTRST_08_BMS_BrennermotorLosr       = 0x08u,    
    HTRST_0F_FLA_Flammwaechterabfrg     = 0x0Fu,    
    HTRST_13_GA_Geblaeseanlauf          = 0x13u,    
    HTRST_14_GSR_GluehstiftRampe        = 0x14u,    
    HTRST_16_INIT_State                 = 0x16u,    
    HTRST_1A_KUEG_Kuehlung              = 0x1Au,    
    HTRST_1B_LTV_Wechsel_TEIL2VOLL      = 0x1Bu,    
    HTRST_1C_LUE_Lueften                = 0x1Cu,    
    HTRST_1D_LVT_Wechsel_VOLL2TEIL      = 0x1Du,    
    HTRST_1E_NEW_INIT_State             = 0x1Eu,    
    HTRST_1F_RB_Regelbetrieb            = 0x1Fu,    
    HTRST_20_RP_Regelpause              = 0x20u,    
    HTRST_28_STV_Stoerver               = 0x28u,    
    HTRST_29_STV_TRS_Stoerver_TRS       = 0x29u,    
    HTRST_2D_VFOE_Vorfoerdern           = 0x2Du,    
    HTRST_2E_VGL_Vorgluehen             = 0x2Eu,    
    HTRST_2F_VGLP_VorgluehenReg         = 0x2Fu,    
    HTRST_32_ZGL_Zugluehen              = 0x32u,    
    HTRST_35_ZWGL_Zwischengluehen       = 0x35u,    
    HTRST_41_BBS_BrennerStandheizen     = 0x41u,    
    HTRST_42_BBZ_BrennerZuheizen        = 0x42u,    
    HTRST_46_RPN_Regelpause_Nachlauf    = 0x46u,    
    HTRST_4A_RPS_Regelpause_Standheizen = 0x4Au,    
    HTRST_52_BOOST_Boostbetrieb         = 0x52u,    
    HTRST_53_ABK_Abkuehlen              = 0x53u,    
    HTRST_54_HGVP_HG_VerriegelPerm      = 0x54u,    
    HTRST_55_GBU_Geblaeseunterbrech     = 0x55u,    
    HTRST_56_LOS_BrennMotLosreissen     = 0x56u,    
    HTRST_57_TA_Temperaturabfrage       = 0x57u,    
    HTRST_58_VUS_VorlaufUnterspng       = 0x58u,    
    HTRST_5D_SV_Startversuch            = 0x5Du,    
    HTRST_5E_VLV_Vorlaufverlaeng        = 0x5Eu,    
    HTRST_5F_BB_BrennerBetrieb          = 0x5Fu,    
    HTRST_63_TRSK_TempSensKuehlung      = 0x63u,    
    HTRST_66_BEREIT                     = 0x66u,    
    HTRST_67_BR_BrennerRegeneration     = 0x67u,    
    HTRST_68_INIT_P                     = 0x68u,    
    HTRST_69_SLEEP                      = 0x69u,    
    HTRST_6A_FBFOE_FortsBrennstFoerd    = 0x6Au,    
    HTRST_6B_FSTABK_FortsStabKaltst     = 0x6Bu,    
    HTRST_6C_FSTABK_FortsStabWarmst     = 0x6Cu,    
    HTRST_6D_NLR_Nachlauframpe          = 0x6Du,    
    HTRST_6E_RWN_RestwaermeNutzung      = 0x6Eu,    
    HTRST_6F_STABK_StabKaltstart        = 0x6Fu,    
    HTRST_70_CPU_OFF                    = 0x70u,    
    HTRST_71_WST_WaitState              = 0x71u,    
    HTRST_72_FSTAB_FortsStabilis        = 0x72u,    
    HTRST_73_MC_MotorCheck              = 0x73u,    
    HTRST_74_FMCL_FlameMonCooling       = 0x74u,    
    HTRST_75_PGL1_Vorgluehen            = 0x75u,    
    HTRST_76_PGL2_Vorgluehen            = 0x76u,    
    HTRST_77_PGL2_PLS_PartLoadStart     = 0x77u,    
    HTRST_78_FMC_S_FlameMonMeasStart    = 0x78u,    
    HTRST_79_S1_START1                  = 0x79u,    
    HTRST_7A_S2_START2                  = 0x7Au,    
    HTRST_7B_S3_START3                  = 0x7Bu,    
    HTRST_7C_S4_START4                  = 0x7Cu,    
    HTRST_7D_S5_START5                  = 0x7Du,    
    HTRST_7E_S6_START6                  = 0x7Eu,    
    HTRST_7F_GPR_GluestiftRampe         = 0x7Fu,    
    HTRST_80_FMM1_FlameMonMeasurem1     = 0x80u,    
    HTRST_81_FMM2_FlameMonMeasurem2     = 0x81u,    
    HTRST_82_S1_PLS_START1              = 0x82u,    
    HTRST_83_S2_PLS_START2              = 0x83u,    
    HTRST_84_S3_PLS_START3              = 0x84u,    
    HTRST_85_S4_PLS_START4              = 0x85u,    
    HTRST_86_S5_PLS_START5              = 0x86u,    
    HTRST_87_S6_PLS_START6              = 0x87u,    
    HTRST_88_GPR_PLS_GluestiftRampe     = 0x88u,    
    HTRST_8B_FL_FullLoad                = 0x8Bu,    
    HTRST_8C_RDN_RampDown               = 0x8Cu,    
    HTRST_8D_PL_PartialLoad             = 0x8Du,    
    HTRST_8E_RUP_RampUp                 = 0x8Eu,    
    HTRST_8F_PAUSE                      = 0x8Fu,    
    HTRST_90_BO_FL_BurnOut_FullLoad     = 0x90u,    
    HTRST_91_BO_PL_BurnOut_PartLoad     = 0x91u,    
    HTRST_92_BO_S_BurnOut_Start         = 0x92u,    
    HTRST_93_BO_PLS_BurnOut_PartStrt    = 0x93u,    
    HTRST_94_BO_OVH_BurnOut_Overheat    = 0x94u,    
    HTRST_95_CL1_Cooling1               = 0x95u,    
    HTRST_96_FMC_FlameMonitorCalibr     = 0x96u,    
    HTRST_97_CL2_Cooling2               = 0x97u,    
    HTRST_98_BST_BOOST                  = 0x98u,    
    HTRST_99_CCTC_ContCoolTempCtl       = 0x99u,    
    HTRST_9A_CCTC_H_ContCoolTempCtl     = 0x9Au,    
    HTRST_A0_STABW_StabWarmstart        = 0xA0u,    
    HTRST_A1_STRK_StartrKaltstart       = 0xA1u,    
    HTRST_A2_STRK_StartrWarmstart       = 0xA2u,    
    HTRST_A3_TRWN_TestRestwNutzung      = 0xA3u,    
    HTRST_ENUM_MAX,                  
    HTRST_ENUM_FORCE_TYPE = 0x7F     
} IUG_HEATER_STATE_NUMBER_t;

#define HTRST_71_WST_WaitState_29       ((HTRST_71_WST_WaitState) << 8u)



#define IUG_UNIT_WBUS_VERSION_NONE                  0x00u   

#define IUG_UNIT_WBUS_VERSION_INVALID               0x00u     


#define IUG_UNIT_HEATER_ECU_CODING_NONE             0x00u   
#define IUG_UNIT_HEATER_ECU_CODING_WATER            0x01u   
#define IUG_UNIT_HEATER_ECU_CODING_02               0x02u   
#define IUG_UNIT_HEATER_ECU_CODING_04               0x04u   
#define IUG_UNIT_HEATER_ECU_CODING_CMD_20_ON_REC    0x08u   
#define IUG_UNIT_HEATER_ECU_CODING_CMD_21_ON_DOM    0x10u   
#define IUG_UNIT_HEATER_ECU_CODING_CMD_23_ON_AUX    0x20u   
#define IUG_UNIT_HEATER_ECU_CODING_CMD_22_ON_VENT   0x40u   
#define IUG_UNIT_HEATER_ECU_CODING_CMD_25_ON_BOOST  0x80u   


#define IUG_UNIT_HEATER_TYPE_INFO_NONE              0x00u   
#define IUG_UNIT_HEATER_TYPE_INFO_WATER             0x01u   
#define IUG_UNIT_HEATER_TYPE_INFO_HEATING_PARK      0x02u   
#define IUG_UNIT_HEATER_TYPE_INFO_CP_ACTIVE_AT_AH   0x04u   
#define IUG_UNIT_HEATER_TYPE_INFO_08                0x08u   
#define IUG_UNIT_HEATER_TYPE_INFO_10                0x10u   
#define IUG_UNIT_HEATER_TYPE_INFO_20                0x20u   
#define IUG_UNIT_HEATER_TYPE_INFO_40                0x40u   
#define IUG_UNIT_HEATER_TYPE_INFO_50                0x80u   



typedef enum
{
    HTRCAP_NONE             = 0x0000u,   

    HTRCAP_0001             = 0x0001u,   
    HTRCAP_0002             = 0x0002u,   
    HTRCAP_0004             = 0x0004u,   
    HTRCAP_0008             = 0x0008u,   
    HTRCAP_0010             = 0x0010u,   
    HTRCAP_0020             = 0x0020u,   
    HTRCAP_0040             = 0x0040u,   
    HTRCAP_0080             = 0x0080u,   
    HTRCAP_HT_REC           = 0x0100u,   
    HTRCAP_HT_DOM           = 0x0200u,   
    HTRCAP_VENTIL           = 0x0400u,   
    HTRCAP_HT_AUX           = 0x0800u,   
    HTRCAP_HT_UPF           = 0x1000u,   
    HTRCAP_HT_BOO           = 0x2000u,   
    HTRCAP_HT_ECO           = 0x4000u,   
    HTRCAP_HT__2A           = 0x8000u,   
    HTRCAP_ENUM_FORCE_TYPE  = 0x7FFFFFFF     

} IUG_HEATER_CAPABILITY_t;


typedef enum
{
    HTR_MF_NONE             =   0x0000u,   

    HTR_MF_AIRCOR           =   0x0001u,   
    HTR_MF_ETHCOR           =   0x0002u,   
    HTR_MF_0004             =   0x0004u,   
    HTR_MF_0008             =   0x0008u,   
    HTR_MF_0010             =   0x0010u,   
    HTR_MF_0020             =   0x0020u,   
    HTR_MF_0040             =   0x0040u,   
    HTR_MF_0080             =   0x0080u,   

    HTR_MF_HT_PER           =   0x0100u,   
    HTR_MF_HTCCTC           =   0x0200u,   
    HTR_MF_HT_ADR           =   0x0400u,   
    HTR_MF_PMPAUX           =   0x0800u,   
    HTR_MF_FLWBUS           =   0x1000u,   
    HTR_MF_FL_LIN           =   0x2000u,   
    HTR_MF_FL_CAN           =   0x4000u,   
    HTR_MF_SLFTST           =   0x8000u,   

    HTRMODFUNC_ENUM_FORCE_TYPE = 0x7FFFFFFF 

} IUG_HEATER_MODE_FUNC_t;


#define IUG_HTRCAP_HEATER_AIR_DEF           ( (HTRCAP_NONE)|\
                                              (HTRCAP_HT_DOM)|\
                                              (HTRCAP_VENTIL)|\
                                              (HTRCAP_HT_BOO)|\
                                              (HTRCAP_HT_ECO)|\
                                              (HTRCAP_HT__2A)    )

#define IUG_HTRCAP_HEATER_WAT_DEF           ( (HTRCAP_NONE)|\
                                              (HTRCAP_HT_DOM)|\
                                              (HTRCAP_VENTIL)|\
                                              (HTRCAP_HT_AUX)   )


#define IUG_UNIT_HEATER_STATUS_OUTPORT_NONE             0x00u   
#define IUG_UNIT_HEATER_STATUS_OUTPORT_BURNER_FAN       0x01u   
#define IUG_UNIT_HEATER_STATUS_OUTPORT_GLOW_PLUG        0x02u   
#define IUG_UNIT_HEATER_STATUS_OUTPORT_FUEL_PUMP        0x04u   
#define IUG_UNIT_HEATER_STATUS_OUTPORT_COOLANT_PUMP     0x08u   
#define IUG_UNIT_HEATER_STATUS_OUTPORT_VEHICLE_FAN      0x10u   
#define IUG_UNIT_HEATER_STATUS_OUTPORT_INJECTOR_HEAT    0x20u   
#define IUG_UNIT_HEATER_STATUS_OUTPORT_FLAME_IND        0x40u   
#define IUG_UNIT_HEATER_STATUS_OUTPORT_COOLANT_VALVE    0x80u   



#define IUG_HEATER_EEPTOP_NONE                          0x00u   
#define IUG_HEATER_EEPTOP_INFO_METAMODE_2A              0x01u   
#define IUG_HEATER_EEPTOP_BARO_INFO_AVAILABLE           0xA1u   
#define IUG_HEATER_EEPTOP_CP_1_AVAILABLE                0xA5u   
#define IUG_HEATER_EEPTOP_CP_1_ACTIVE_AT_AH             0xA6u   

#define IUG_HEATER_EEPSIG_BARO_INFO_AVAILABLE_ST2000_EN


#define IUG_HEATER_EEPADR_INVALID                                   0xFFFFu   

#define IUG_HEATER_EEPADR_EOL_0107                                  0x0107u  

#define IUG_HEATER_EEPADR_CONFIG_BYTE_1                                           0x0261u   
#define IUG_HEATER_EEPADR_CONFIG_BYTE_2             ((IUG_HEATER_EEPADR_CONFIG_BYTE_1)+1u)  
#define IUG_HEATER_EEPADR_CONFIG_BYTE_3             ((IUG_HEATER_EEPADR_CONFIG_BYTE_1)+2u)  
#define IUG_HEATER_EEPADR_CONFIG_BYTE_4             ((IUG_HEATER_EEPADR_CONFIG_BYTE_1)+3u)  

#define IUG_HEATER_EEPADR_CONFIG_BYTE_1_CUR                                       0x02C2u   
#define IUG_HEATER_EEPADR_CONFIG_BYTE_2_CUR     ((IUG_HEATER_EEPADR_CONFIG_BYTE_1_CUR)+1u)  


#define IUG_HEATER_EEPADR_CONFIG_CP_1_AVAILABLE_TT_EVO_GEN1         0x02C2u   
#define IUG_HEATER_EEPADR_CONFIG_CP_1_ACTIVE_AT_AH_TT_EVO_GEN1      0x02C2u   


#define IUG_HEATER_EEPADR_CONFIG_CP_1_AVAILABLE_TT_EVO_GEN4         0x1034u   
#define IUG_HEATER_EEPADR_CONFIG_CP_1_ACTIVE_AT_AH_TT_EVO_GEN4      0x1039u   



#define IUG_HEATER_EEPADR_EEP_CONFIG4_AT_44_55                      0x0103u  
#ifdef IUG_HEATER_EEPSIG_BARO_INFO_AVAILABLE_ST2000_EN
#define IUG_HEATER_EEPADR_EEP_CONFIG4_ST2000                        0x0103u  
#endif


#define IUG_HEATER_EEPADR_EEP_CONFIG_BARO_INFO_TP_50_WAT            
#define IUG_HEATER_EEPADR_EEP_CONFIG_BARO_INFO_TP_90_WAT            0x00FFu  
#define IUG_HEATER_EEPADR_EEP_CONFIG_BARO_INFO_TP_120_WAT           0x0064u  
#define IUG_HEATER_EEPADR_EEP_CONFIG_BARO_INFO_TT_EVO_GEN4          0x2018u  


#define IUG_HEATER_EEPSIG_EVAL_SPECIAL                              0xA5u   

#define IUG_HEATER_EEPSIG_NO_META_MODE_2A                           0x01u   

#define IUG_HEATER_EEPSIG_CONFIG_CP_AVAILABLE                       0x01u   
#define IUG_HEATER_EEPSIG_CONFIG_CP_CONTROL_ACTIVE                  0x02u   
#define IUG_HEATER_EEPSIG_CONFIG_CV_AVAILABLE                       0x04u   
#define IUG_HEATER_EEPSIG_CONFIG_CV_PULSING_ACTIVE                  0x08u   
#define IUG_HEATER_EEPSIG_CONFIG_VF_AVAILABLE                       0x10u   
#define IUG_HEATER_EEPSIG_CONFIG_CP_ACTIVE_AT_AH                    0x20u   
#define IUG_HEATER_EEPSIG_CONFIG_INTERLOCK_RESET_ALLOWED            0x40u   
#define IUG_HEATER_EEPSIG_CONFIG_MODE_20_TO_0E_APPL                 0x80u   

#ifdef IUG_HEATER_EEPSIG_BARO_INFO_AVAILABLE_ST2000_EN
#define IUG_HEATER_EEPSIG_CONFIG_BARO_INFO_AVAILABLE_ST2000         0x08u   
#endif
#define IUG_HEATER_EEPSIG_CONFIG_BARO_INFO_AVAILABLE_AT_40_55       0x40u   
#define IUG_HEATER_EEPSIG_CONFIG_BARO_INFO_AVAILABLE_TT_EVO_GEN4    (IUG_HEATER_EEPSIG_EVAL_SPECIAL) 
#define IUG_HEATER_EEPSIG_CONFIG_BARO_INFO_AVAILABLE_TP_90_WAT      (IUG_HEATER_EEPSIG_EVAL_SPECIAL) 
#define IUG_HEATER_EEPSIG_CONFIG_BARO_INFO_AVAILABLE_TP_120_WAT     0x02u   

#define IUG_HEATER_EEPSIG_CONFIG_CP_1_AVAILABLE_TT_EVO_GEN1         0x01u   
#define IUG_HEATER_EEPSIG_CONFIG_CP_1_ACTIVE_AT_AH_TT_EVO_GEN1      0x20u   
#define IUG_HEATER_EEPSIG_CONFIG_CP_1_AVAILABLE_TT_EVO_GEN4         (IUG_HEATER_EEPSIG_EVAL_SPECIAL) 
#define IUG_HEATER_EEPSIG_CONFIG_CP_1_ACTIVE_AT_AH_TT_EVO_GEN4      (IUG_HEATER_EEPSIG_EVAL_SPECIAL) 


#define IUG_HEATER_EEPSIG_CONFIG_BARO_INFO_AVAILABLE_5HPA           (IUG_AIR_PRESSURE_NORMAL_5HPA)


#endif 


