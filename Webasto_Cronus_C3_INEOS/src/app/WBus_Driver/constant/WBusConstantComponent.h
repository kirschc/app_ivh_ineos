#ifndef _WBUS_CONSTANT_COMPONENT_H_
#define _WBUS_CONSTANT_COMPONENT_H_



#define WBUS_COMP_PROPERTY_CONTROL_MAX       4u 

#define WBUS_COMP_PROPERTY_HEATER_MAX       16u 




typedef enum
{
    WBUS_COMP_00_ALL_ACTIVE            = 0x00u,    
    WBUS_COMP_01_BURNER_MOTOR          = 0x01u,    
    WBUS_COMP_02_FUEL_PUMP             = 0x02u,    
    WBUS_COMP_03_GLOW_PLUG             = 0x03u,    
    WBUS_COMP_04_COOLANT_PUMP          = 0x04u,    
    WBUS_COMP_05_VEHICLE_FAN           = 0x05u,    
    WBUS_COMP_06_YELLOW_LED            = 0x06u,    
    WBUS_COMP_07_GREEN_LED             = 0x07u,    
    WBUS_COMP_08_SPARK_DEVICE          = 0x08u,    
    WBUS_COMP_09_COOLANT_VALVE         = 0x09u,    
    WBUS_COMP_0A_OPERATION_DISPLAY     = 0x0Au,    
    WBUS_COMP_0B_IGNITION_QUARTZ       = 0x0Bu,    
    WBUS_COMP_0C_LCD_DISPLAY_BUTTON    = 0x0Cu,    
    WBUS_COMP_0D_INJECTION_TUBE_HEATER = 0x0Du,    
    WBUS_COMP_0E_FLAME_INDICATOR       = 0x0Eu,    
    WBUS_COMP_0F_FUEL_WARMER           = 0x0Fu,    
    WBUS_COMP_10_HEATER_FAN            = 0x10u,    
    WBUS_COMP_19_COOLANT_PUMP2         = 0x19u,    
    WBUS_COMP_20_ALL_SENSORS           = 0x20u,    
    WBUS_COMP_21_COOLANT_SENSOR        = 0x21u,    
    WBUS_COMP_22_OVERHEAT_SENSOR       = 0x22u,    
    WBUS_COMP_23_EXHAUST_SENSOR        = 0x23u,    
    WBUS_COMP_24_EXTERIOR_TEMP_SENSOR  = 0x24u,    
    WBUS_COMP_25_INTERIOR_TEMP_SENSOR  = 0x25u,    
    WBUS_COMP_26_FLAME_SENSOR          = 0x26u,    
    WBUS_COMP_27_BAT_VOLTAGE_SENSOR    = 0x27u,    
    WBUS_COMP_28_PCB_TEMP_SENSOR       = 0x28u,    
    WBUS_COMP_29_BLOW_OUT_TEMP_SENSOR  = 0x29u,    
    WBUS_COMP_2A_AIR_PRESSURE_SENSOR   = 0x2Au,    
    WBUS_COMP_2B_FUEL_SENSOR           = 0x2Bu,    
    WBUS_COMP_2C_ETHANOL_SENSOR        = 0x2Cu,    
    WBUS_COMP_31_PHYSICAL_VALUE_QUERY  = 0x31u,    
    WBUS_COMP_ENUM_MAX,                  
    WBUS_COMP_ENUM_FORCE_TYPE = 0x7F     

} enum_WBUS_COMPONENT_t;
typedef enum_WBUS_COMPONENT_t   WBUS_COMPONENT_t;



typedef PAR_LST_ETY_U_8_t     IUG_COMP_PROP_ENTRY_t;


#pragma pack(1) 
typedef struct
{
    WBUS_COMPONENT_t            comp_id;            
    WBUS_PHYS_VAL_M45_t         phys_val_m45_id;    

} structure_IUG_UNIT_COMPONENT_PROPERTY_M45_NVM_ENTRY_t;
#pragma pack() 
typedef structure_IUG_UNIT_COMPONENT_PROPERTY_M45_NVM_ENTRY_t     IUG_UNIT_COMP_PROP_M45_NVM_ENTRY_t;


uint8_t IUG_utility_comp_m45_get_entries( IUG_UNIT_COMP_PROP_M45_NVM_ENTRY_t * const p_comp_pty_m45, 
                                          const uint8_t comp_pty_m45_num_maximum );
uint8_t IUG_utility_comp_m45_get_entry( IUG_UNIT_COMP_PROP_M45_NVM_ENTRY_t * const p_comp_pty_m45, 
                                        const uint8_t comp_pty_m45_num_maximum,
                                        const uint8_t comp_pty_m45_entry,
                                        IUG_UNIT_COMP_PROP_M45_NVM_ENTRY_t * const comp_pty_m45_data );
uint8_t IUG_utility_comp_m45_set_entry( IUG_UNIT_COMP_PROP_M45_NVM_ENTRY_t * const p_comp_pty_m45, 
                                        const uint8_t comp_pty_m45_num_maximum,
                                        const uint8_t comp_pty_m45_entry,
                                        const IUG_UNIT_COMP_PROP_M45_NVM_ENTRY_t comp_pty_m45_data );


#endif 


