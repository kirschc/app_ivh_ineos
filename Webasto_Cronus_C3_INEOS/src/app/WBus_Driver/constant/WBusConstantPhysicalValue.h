#ifndef _WBUS_CONSTANT_PHYSICAL_VALUE_H_
#define _WBUS_CONSTANT_PHYSICAL_VALUE_H_


typedef enum
{
    WBUS_PHYSVAL_TIME_S                 =  0u,  
    WBUS_PHYSVAL_TIME_M                 =  1u,  
    WBUS_PHYSVAL_TIME_H                 =  2u,  
    WBUS_PHYSVAL_VOLTAGE_MV             =  3u,  
    WBUS_PHYSVAL_RPM_1MIN               =  4u,  
    WBUS_PHYSVAL_RESISTANCE_MOHM        =  5u,  
    WBUS_PHYSVAL_CURRENT_100MA          =  6u,  
    WBUS_PHYSVAL_FREQUENCY_50MHZ        =  7u,  
    WBUS_PHYSVAL_TEMP1_1DEG             =  8u,  
    WBUS_PHYSVAL_TEMP2_05DEG            =  9u,  
    WBUS_PHYSVAL_TEMP3_01DEG            = 10u,  
    WBUS_PHYSVAL_PERCENT_05PCT          = 11u,  
    WBUS_PHYSVAL_POWER_W                = 12u,  
    WBUS_PHYSVAL_DATE_DAY               = 13u,  
    WBUS_PHYSVAL_DATE_WEEK              = 14u,  
    WBUS_PHYSVAL_DATE_MONTH             = 15u,  
    WBUS_PHYSVAL_DATE_YEAR              = 16u,  
    WBUS_PHYSVAL_VOLUME1_L              = 17u,  
    WBUS_PHYSVAL_VOLUME2_ML             = 18u,  
    WBUS_PHYSVAL_PRESSURE_HPA           = 19u,  
    WBUS_PHYSVAL_GRAD_RESISTANCE_MOHM   = 20u,  
    WBUS_PHYSVAL_GRAD_POWER_W           = 21u,  
    WBUS_PHYSVAL_LENGTH_10KM            = 22u,  
    WBUS_PHYSVAL_DIGITAL_VALUE          = 23u,  
    WBUS_PHYSVAL_FLOW_VOLUME_MLH        = 24u,  
    WBUS_PHYSVAL_GRAD_TEMP_01DEGS       = 25u,  
    WBUS_PHYSVAL_ENUM_MAX,                      
    WBUS_PHYSVAL_ENUM_FORCE_TYPE = 0x7F         

} enum_WBUS_PHYSICAL_VALUE_t;
typedef enum_WBUS_PHYSICAL_VALUE_t   WBUS_PHYS_VAL_t;


typedef enum
{
    WBUS_PHYSVALM45_01_VOLTAGE_MV          = 0x01u,    
    WBUS_PHYSVALM45_02_RPM_1MIN            = 0x02u,    
    WBUS_PHYSVALM45_03_RESISTANCE_MOHM     = 0x03u,    
    WBUS_PHYSVALM45_04_CURRENT_100MA       = 0x04u,    
    WBUS_PHYSVALM45_05_FREQUENCY_50MHZ     = 0x05u,    
    WBUS_PHYSVALM45_06_TEMP1_1DEG          = 0x06u,    
    WBUS_PHYSVALM45_07_PERCENT_05PCT       = 0x07u,    
    WBUS_PHYSVALM45_08_POWER_W             = 0x08u,    
    WBUS_PHYSVALM45_09_VOLUME1_L           = 0x09u,    
    WBUS_PHYSVALM45_0A_DIGITAL_VALUE       = 0x0Au,    
    WBUS_PHYSVALM45_ENUM_MAX,                       
    WBUS_PHYSVALM45_ENUM_FORCE_TYPE     = 0x7F      

} enum_WBUS_PHYSICAL_VALUE_MODE45_t;
typedef enum_WBUS_PHYSICAL_VALUE_MODE45_t   WBUS_PHYS_VAL_M45_t;



#endif 


