#ifndef _WBUS_CONSTANT_FRAME_H_
#define _WBUS_CONSTANT_FRAME_H_




#define WBUS_PACKET_POS_START               0U

#define WBUS_PACKET_POS_ADDR                (WBUS_PACKET_POS_START + 0U)

#define WBUS_PACKET_POS_LEN                 (WBUS_PACKET_POS_START + 1U)

#define WBUS_PACKET_POS_MODE                (WBUS_PACKET_POS_START + 2U)

#define WBUS_PACKET_POS_MODE_NAK            (WBUS_PACKET_POS_START + 3U)

#define WBUS_PACKET_POS_SERVICE             (WBUS_PACKET_POS_START + 3U)

#define WBUS_PACKET_POS_SERVICE_SUB         (WBUS_PACKET_POS_START + 4U)

#define WBUS_PACKET_POS_MODE_NAK_CODE       (WBUS_PACKET_POS_START + 4U)

#define WBUS_PACKET_POS_DATA                (WBUS_PACKET_POS_START + 3u)        

#define WBUS_PACKET_POS_META_MODE           (WBUS_PACKET_POS_START + 3u)
#define WBUS_PACKET_POS_META_SERVICE        (WBUS_PACKET_POS_START + 4u)
#define WBUS_PACKET_POS_META_SERVICE_SUB    (WBUS_PACKET_POS_START + 5u)
#define WBUS_PACKET_POS_META_DATA           (WBUS_PACKET_POS_META_SERVICE_SUB)  

#define WBUS_PACKET_POS_CHECKSUM( msg )     ( ((msg)[WBUS_PACKET_POS_LEN]) + 1 )


#define WBUS_ADDR_OF_MSG( msg )                 ((msg)[WBUS_PACKET_POS_ADDR])

#define WBUS_LENGTHBYTE_OF_MSG( msg )       ((msg)[WBUS_PACKET_POS_LEN])

#define WBUS_MODE_OF_MSG( msg )             ((msg)[WBUS_PACKET_POS_MODE])

#define WBUS_MODE_OF_MSG_NAK( msg )         ((msg)[WBUS_PACKET_POS_MODE_NAK])

#define WBUS_SERVICE_OF_MSG( msg )          ((msg)[WBUS_PACKET_POS_SERVICE])

#define WBUS_SERVICE_SUB_OF_MSG( msg )      ((msg)[WBUS_PACKET_POS_SERVICE_SUB])

#define WBUS_DATABYTES_IN_MSG(msg) (((msg)[WBUS_PACKET_POS_LEN]) - 2)

#define WBUS_TOTAL_LENGTH_OF_MSG(msg)       (((uint16_t)(WBUS_LENGTHBYTE_OF_MSG(msg))) + 2)

#define WBUS_HEADER_LENGTH_OF_MSG           (1u + 1u)


#define WBUS_CHECKSUMBYTE_OF_MSG(msg)       ((msg)[WBUS_LENGTHBYTE_OF_MSG((msg))+1])

#define WBUS_NTH_DATABYTE_OF_MSG(msg,n)     ((msg)[(n)+((WBUS_PACKET_POS_DATA)-1)])


#define WBUS_IDX_BYTE_OF_MSG(msg,i)         ((msg)[(i)+(WBUS_PACKET_POS_START)])


#define WBUS_PACKET_ASCII_DATA_MAX_SIZE                25u  

#define WBUS_PACKET_SERIAL_NUMBER_DATA_SIZE             5u  

#define WBUS_PACKET_HEATER_SIGN_DATA_SIZE               8u  


#endif 


