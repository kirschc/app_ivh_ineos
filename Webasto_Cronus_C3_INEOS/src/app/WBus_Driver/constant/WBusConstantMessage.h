#ifndef _WBUS_CONSTANT_MESSAGE_H_
#define _WBUS_CONSTANT_MESSAGE_H_




#define WBUS_MSGMOD_80_POSACK_BIT           0x80u 
#define WBUS_MSGMOD_7F_NEGACK               0x7Fu 
#define WBUS_MSGMOD_7F_BTM_NEGACK           0x7Fu 


#define WBUS_MSGMOD_00_NONE                 0x00u 
#define WBUS_MSGMOD_10_OFF                  0x10u 
#define WBUS_MSGMOD_20_ON_REC               0x20u 
#define WBUS_MSGMOD_21_ON_DOM               0x21u 
#define WBUS_MSGMOD_22_ON_VENT              0x22u 
#define WBUS_MSGMOD_23_ON_AUX               0x23u 
#define WBUS_MSGMOD_24_ON_UP                0x24u 
#define WBUS_MSGMOD_25_ON_BOOST             0x25u 
#define WBUS_MSGMOD_26_ON_ECO               0x26u 
#define WBUS_MSGMOD_27_ON_TEMP              0x27u 
#define WBUS_MSGMOD_28_ON_TEST              0x28u 
#define WBUS_MSGMOD_29_ON_EMV               0x29u 
#define WBUS_MSGMOD_2A_ON_TEMP_MOD          0x2Au 
#define WBUS_MSGMOD_2A_META_MODE_MSG        (WBUS_MSGMOD_2A_ON_TEMP_MOD)    

#define WBUS_MSGMOD_30_RAM_RD               0x30u 
#define WBUS_MSGMOD_31_RAM_WR               0x31u 
#define WBUS_MSGMOD_32_EEPROM_RD            0x32u 
#define WBUS_MSGMOD_33_EEPROM_WR            0x33u 
#define WBUS_MSGMOD_34_BTM_BLK_TRANSF_MOD   0x34u 
#define WBUS_MSGMOD_35_TELESTART_EEP_RD     0x35u 
#define WBUS_MSGMOD_36_TELESTART_EEP_WR     0x36u 
#define WBUS_MSGMOD_37_ADC_RD               0x37u 
#define WBUS_MSGMOD_38_EOL_CHKSUM_PARAM     0x38u 
#define WBUS_MSGMOD_39_LOCK_TRANSPORT       0x39u 

#define WBUS_MSGMOD_40_TEACHIN_CUR_ON_OFF   0x40u 
#define WBUS_MSGMOD_41_TEACHIN_CLOSE        0x41u 
#define WBUS_MSGMOD_42_ROUTINE              0x42u 
#define WBUS_MSGMOD_43_ROUTINE_PENDING      0x43u 
#define WBUS_MSGMOD_44_COMMAND_PENDING      0x44u 
#define WBUS_MSGMOD_45_COMPONENT_TEST       0x45u 
#define WBUS_MSGMOD_46_TIMER_SET_ACT        0x46u 
#define WBUS_MSGMOD_47_SAM_SEC_ACC_MOD      0x47u 
#define WBUS_MSGMOD_48_MAB_MEM_ACC_BLKMOD   0x48u 
#define WBUS_MSGMOD_49_UNIT_SPEC_DATA_RW    0x49u 

#define WBUS_MSGMOD_50_STATUS_REQUEST       0x50u 
#define WBUS_MSGMOD_51_ID_REQUEST           0x51u 
#define WBUS_MSGMOD_52_PROD_WR              0x52u 
#define WBUS_MSGMOD_53_APP_DAT_RD           0x53u 
#define WBUS_MSGMOD_54_APP_DAT_WR           0x54u 
#define WBUS_MSGMOD_56_ERROR_MODE           0x56u 
#define WBUS_MSGMOD_57_CO2                  0x57u 

#define WBUS_MSGMOD_60_DEV_MOD              0x60u 

#define WBUS_MSGMOD_90_STAT_MSG_OFF         0x90u 
#define WBUS_MSGMOD_A0_STAT_MSG_ON          0xA0u 
#define WBUS_MSGMOD_A1_STAT_MSG_ON_HEAT     0xA1u 
#define WBUS_MSGMOD_A2_STAT_MSG_ON_VENT     0xA2u 
#define WBUS_MSGMOD_A3_STAT_MSG_ON_AUX      0xA3u 
#define WBUS_MSGMOD_A4_STAT_MSG_ON_UP       0xA4u 
#define WBUS_MSGMOD_A5_STAT_MSG_ON_BOOST    0xA5u 
#define WBUS_MSGMOD_A6_STAT_MSG_ON_ECON     0xA6u 
#define WBUS_MSGMOD_A7_STAT_MSG_ON_TEMP     0xA7u 
#define WBUS_MSGMOD_A8_STAT_MSG_ON_TEST     0xA8u 
#define WBUS_MSGMOD_A9_STAT_MSG_ON_EMV      0xA9u 
#define WBUS_MSGMOD_AA_STAT_MSG_ON_TEMP_MOD 0xAAu 
#define WBUS_MSGMOD_AF_STAT_MSG_SERVICE     0xAFu 
#define WBUS_MSGMOD_C4_STAT_MSG_ERROR       0xC4u 
#define WBUS_MSGMOD_C6_STAT_MSG_PRES_TIM    0xC6u 
#define WBUS_MSGMOD_C6_STAT_MSG_TIMER       (WBUS_MSGMOD_C6_STAT_MSG_PRES_TIM) 
#define WBUS_MSGMOD_D0_STAT_MSG_DATA        0xD0u 

#define WBUS_MSGMOD_C2_TEST_MSG_REM_CTL     0xC2u 

#define WBUS_MSGMOD_FF_INVALID              0xFFu 



#define WBUS_MSGSRV_00_NONE                             0x00u   
#define WBUS_MSGSRV_FF_ALL                              0xFFu   

#define WBUS_MSGSRV_FF_BTM_ALL                          0xFFu   
#define WBUS_MSGSRV_01_BTM_ENTER                        0x01u   
#define WBUS_MSGSRV_03_BTM_EEPROM_READ                  0x03u   
#define WBUS_MSGSRV_04_BTM_EEPROM_WRITE                 0x04u   
#define WBUS_MSGSRV_05_BTM_BANK_CHANGE                  0x05u   
#define WBUS_MSGSRV_09_BTM_EXIT                         0x09u   
#define WBUS_MSGSRV_10_BTM_ID_CHECK                     0x10u   
#define WBUS_MSGSRV_11_BTM_BLANK_CHECK                  0x11u   
#define WBUS_MSGSRV_12_BTM_BLOCK_READ                   0x12u   
#define WBUS_MSGSRV_13_BTM_BLOCK_WRITE                  0x13u   
#define WBUS_MSGSRV_14_BTM_FLASH_ERASE                  0x14u   
#define WBUS_MSGSRV_20_BTM_BANK_SELECT                  0x20u   


#define WBUS_MSGSRV_FF_LOCK_ALL                         0xFFu   
#define WBUS_MSGSRV_00_LOCK_RELEASE                     0x00u   
#define WBUS_MSGSRV_01_LOCK_ENTER                       0x01u   
#define WBUS_MSGSRV_02_LOCK_QUERY                       0x02u   
#define WBUS_MSGSRV_03_TRANSP_RELEASE                   0x03u   
#define WBUS_MSGSRV_04_TRANSP_ENTER                     0x04u   
#define WBUS_MSGSRV_05_TRANSP_QUERY                     0x05u   

#define WBUS_MSGACK_00_LOCK_RELEASED                    0x00u   
#define WBUS_MSGACK_01_LOCK_LOCKED                      0x01u   
#define WBUS_MSGACK_03_TRANSP_RELEASED                  0x03u   
#define WBUS_MSGACK_04_TRANSP_LOCKED                    0x04u   
#define WBUS_MSGACK_7F_UNCLEAR                          0x7Fu   

#define WBUS_MSGSRV_FF_ROUTINE_ALL                      0xFFu   
#define WBUS_MSGSVR_06_ROUTINE_HG_TEST                  0x06u   
#define WBUS_MSGSVR_0A_ROUTINE_RESET                    0x0Au   
#define WBUS_MSGSVR_0B_ROUTINE_FBL                      0x0Bu   
#define WBUS_MSGSVR_20_ROUTINE_ID_READ                  0x20u   
#define WBUS_MSGSVR_21_ROUTINE_ID_START                 0x21u   
#define WBUS_MSGSVR_22_ROUTINE_RESULT                   0x22u   


#define WBUS_MSGSRV_02_COMP_TEST_FUEL_PUMP              0x02u   
#define WBUS_MSGSRV_04_COMP_TEST_COOLANT_PUMP           0x04u   
#define WBUS_MSGSRV_31_COMP_TEST_GET_IMPLEMENTED        0x31u   
#define WBUS_MSGSRV_FF_COMP_TEST_SERVICE_ALL            0xFFu   


#define WBUS_MSGSRV_FF_SAM_ALL                          0xFFu   
#define WBUS_MSGSRV_01_SAM_STATUS                       0x01u   
#define WBUS_MSGSRV_02_SAM_DEACTIVATE                   0x02u   
#define WBUS_MSGSRV_03_SAM_ACTIVATE                     0x03u   
#define WBUS_MSGSRV_31_SAM_QUERY                        0x31u   


#define WBUS_MSGSRV_FF_MAB_ALL                          0xFFu   
#define WBUS_MSGSRV_01_MAB_ENTER                        0x01u   
#define WBUS_MSGSRV_02_MAB_REQ_COMPLETE                 0x02u   
#define WBUS_MSGSRV_03_MAB_EOL_READ                     0x03u   
#define WBUS_MSGSRV_04_MAB_EOL_WRITE                    0x04u   
#define WBUS_MSGSRV_09_MAB_EXIT                         0x09u   


#define WBUS_MSGSRV_FF_UNIT_SPEC_DATA_ALL                   0xFFu   
#define WBUS_MSGSRV_30_UNIT_SPEC_DATA_READ                  0x30u   
#define WBUS_MSGSRV_31_UNIT_SPEC_DATA_RD_SUPP_RO            0x31u   
#define WBUS_MSGSRV_32_UNIT_SPEC_DATA_WRITE                 0x32u   
#define WBUS_MSGSRV_33_UNIT_SPEC_DATA_RD_SUPP_RW            0x33u   

#define WBUS_MSGSRV_3001_UNTSPCDAT_RD_TLINR_TEMP_VAL        0x01u   
#define WBUS_MSGSRV_3005_UNTSPCDAT_RD_TLINR_RELAIS_STATE    0x05u   
#define WBUS_MSGSRV_3006_UNTSPCDAT_RD_TLINR_120_kOHM_STATE  0x06u   
#define WBUS_MSGSRV_3205_UNTSPCDAT_WR_TLINR_RELAIS_STATE    0x05u   
#define WBUS_MSGSRV_3206_UNTSPCDAT_WR_TLINR_120_kOHM_STATE  0x06u   
#define WBUS_MSGSRV_322E_UNTSPCDAT_WRITE_NO_ERR_REQUEST     0x2Eu   
#define WBUS_MSGSRV_3238_UNTSPCDAT_WRITE_HEATER_ADDR        0x38u   
#define WBUS_MSGSRV_3240_UNTSPCDAT_WRITE_MONITORING         0x40u   

#define WBUS_MSGSRV_FF_STA_ALL                              0xFFu   
#define WBUS_MSGSRV_05_STA_GET_MEASURES                     0x05u   
#define WBUS_MSGSRV_07_STA_GET_STATUS_FUNCSTATUS            0x07u   
#define WBUS_MSGSRV_08_STA_GET_EDUCATED_TRANS               0x08u   
#define WBUS_MSGSRV_15_STA_GET_TEMP_SENSOR                  0x15u   
#define WBUS_MSGSRV_30_STA_GET_STATUS                       0x30u   
#define WBUS_MSGSRV_31_STA_GET_IMPLEMENTED                  0x31u   

#define WBUS_MSGSRV_300A_STA_GET_STATUS_OUTPORT_HEATER      0x0Au   
#define WBUS_MSGSRV_300C_STA_GET_STATUS_COOL_MEDIUM_TEMP1   0x0Cu   
#define WBUS_MSGSRV_3028_STA_GET_STATUS_STATE_NUM_1BYTE     0x28u   
#define WBUS_MSGSRV_3029_STA_GET_STATUS_STATE_NUM_2BYTE     0x29u   
#define WBUS_MSGSRV_302A_STA_GET_STATUS_STATE_STFL          0x2Au   
#define WBUS_MSGSRV_302C_STA_GET_STATUS_STATE_HGV           0x2Cu   
#define WBUS_MSGSRV_3039_STA_GET_STATUS_COOL_MEDIUM_TEMP2   0x39u   
#define WBUS_MSGSRV_303A_STA_GET_AIR_PRESSURE               0x3Au   


#define WBUS_MSGSRV_FF_ID_ALL                               0xFFu   
#define WBUS_MSGSRV_02_HW_SW_VERSION                        0x02u   
#define WBUS_MSGSRV_09_GET_SERIAL_NUMBER                    0x09u   
#define WBUS_MSGSRV_0A_ID_WBUS_VERSION                      0x0Au   
#define WBUS_MSGSRV_0B_ID_ECU_ID                            0x0Bu   
#define WBUS_MSGSRV_0C_ID_ECU_CODING                        0x0Cu   
#define WBUS_MSGSRV_30_ID_GET_STATUS                        0x30u   
#define WBUS_MSGSRV_31_ID_GET_IMPLEMENTED                   0x31u   

#define WBUS_MSGSRV_300B_ID_GET_SERIAL_NUMBER               0x0Bu   
#define WBUS_MSGSRV_300C_ID_GET_STATUS_TEST_BENCH_ID        0x0Cu   
#define WBUS_MSGSRV_300D_ID_WBUS_VERSION                    0x0Du   
#define WBUS_MSGSRV_300E_ID_GET_HEATER_SIGN                 0x0Eu   
#define WBUS_MSGSRV_3012_ID_GET_MAX_HEATING_TIME            0x12u   
#define WBUS_MSGSRV_301D_ID_GET_HEATER_TYPE                 0x1Du   
#define WBUS_MSGSRV_301F_ID_GET_HEATER_NAME                 0x1Fu   
#define WBUS_MSGSRV_3020_ID_GET_HEATER_CAPABLTY             0x20u   
#define WBUS_MSGSRV_3022_ID_GET_HEATER_MODE_FUNC            0x22u   

#define WBUS_MSGSRV_FF_ERRMOD_AL                            0xFFu   
#define WBUS_MSGSRV_01_ERRMOD_ERR_READ                      0x01u   
#define WBUS_MSGSRV_03_ERRMOD_ERR_CLEAR                     0x03u   
#define WBUS_MSGSRV_06_ERRMOD_UNLOCK_HEATER                 0x06u   
#define WBUS_MSGSRV_07_ERRMOD_HIST_TAB_CLEAR                0x07u   
#define WBUS_MSGSRV_20_ERRMOD_ERR_ENV_READ                  0x20u   
#ifdef CAN_WEBASTO_BRIDGE_IMPLEMENT
#define WBUS_MSGSRV_03_ERRMOD_ERR_CLEAR_CANWEB_BRDG         (0x70u |(WBUS_MSGSRV_03_ERRMOD_ERR_CLEAR))      
#define WBUS_MSGSRV_07_ERRMOD_HIST_TAB_CLEAR_CANWEB_BRDG    (0x70u |(WBUS_MSGSRV_07_ERRMOD_HIST_TAB_CLEAR)) 
#endif


#define WBUS_MSGSRV_FF_ID_STAT_MSG_ALL                      0xFFu   
#define WBUS_MSGSRV_0F_ID_STAT_MSG_TIMER                    0x0Fu   
#define WBUS_MSGSRV_D03A_STAT_ID_AIR_PRESSURE               0x3Au   




#define WBUS_MSG_OPFLG_NONE              0x00u
#define WBUS_MSG_OPFLG_ANALYZE           0x01u  
#define WBUS_MSG_OPFLG_GATEWAY           0x02u  
#define WBUS_MSG_OPFLG_REFUSE            0x08u  
#define WBUS_MSG_OPFLG_BROADCAST         0x10u  
#define WBUS_MSG_OPFLG_20                0x20u  
#define WBUS_MSG_OPFLG_40                0x40u  
#define WBUS_MSG_OPFLG_80                0x80u  


#define WBUS_MSG_FLG_NONE                0x00u
#define WBUS_MSG_FLG_IS_ACK_POS          0x01u  
#define WBUS_MSG_FLG_IS_ACK_NEG          0x02u  
#define WBUS_MSG_FLG_IS_BROADCAST        0x04u  
#define WBUS_MSG_FLG_08                  0x08u  
#define WBUS_MSG_FLG_10                  0x10u  
#define WBUS_MSG_FLG_20                  0x20u  
#define WBUS_MSG_FLG_40                  0x40u  
#define WBUS_MSG_FLG_80                  0x80u  
#define WBUS_MSG_FLG_EVALUATION_MASK     ((WBUS_MSG_FLG_IS_ACK_POS)|(WBUS_MSG_FLG_IS_ACK_NEG)|(WBUS_MSG_FLG_IS_BROADCAST))


#endif 


