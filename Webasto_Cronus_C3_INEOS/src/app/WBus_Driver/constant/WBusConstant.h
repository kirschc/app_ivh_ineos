#ifndef _WBUS_CONSTANT_H_
#define _WBUS_CONSTANT_H_


typedef enum
{
    WBUS_MBR_NULL                 = 0u,     
	WBUS_MBR_1                    = 1u,     
    WBUS_MBR_REMOTE_CTL_TELESTART = 2u,     
    WBUS_MBR_CTL_MULTI            = 3u,     
    WBUS_MBR_HEATER_UNIT_1        = 4u,     
    WBUS_MBR_HEATER_UNIT_2        = 5u,     
    WBUS_MBR_TEMP_SENSOR          = 6u,     
    WBUS_MBR_THERMO_CONNECT       = 7u,     
    WBUS_MBR_THERMO_CALL          = 8u,     
    WBUS_MBR_IUG                  = 9u,     
    WBUS_MBR_PIGGYBACK            = 10u,    
    WBUS_MBR_ROOM_THERMOSTAT      = 11u,    
    WBUS_MBR_CTL_HEATER_BOOST     = 12u,    
    WBUS_MBR_13                   = 13u,    
    WBUS_MBR_TEMP_SENSOR_T_LIN_R  = 14u,    
    WBUS_MBR_TEST_UNIT_WTT        = 15u,    
    WBUS_MBR_ENUM_MAX,                      
    WBUS_MBR_ENUM_FORCE_TYPE      = 0x7F    

} enum_WBUS_MEMBER_t;
typedef enum_WBUS_MEMBER_t  WBUS_MEMBER_t;

#define WBUS_MBR_Se_MSK                     0xF0u
#define WBUS_MBR_sE_MSK                     0x0Fu

#define WBUS_MBR_Se_WTT_REQ                 ( ((WBUS_MBR_TEST_UNIT_WTT) << 4u)                            )
#define WBUS_MBR_sE_WTT_ACK                 (                                    (WBUS_MBR_TEST_UNIT_WTT) )

#define WBUS_MBR_SE_WTT_IUG_REQ             ( ((WBUS_MBR_TEST_UNIT_WTT) << 4u) | (WBUS_MBR_IUG)           )
#define WBUS_MBR_SE_WTT_IUG_ACK             ( ((WBUS_MBR_IUG)           << 4u) | (WBUS_MBR_TEST_UNIT_WTT) )

#define WBUS_MBR_SE_WTT_HG1_REQ             ( ((WBUS_MBR_TEST_UNIT_WTT) << 4u) | (WBUS_MBR_HEATER_UNIT_1) )
#define WBUS_MBR_SE_WTT_HG1_ACK             ( ((WBUS_MBR_HEATER_UNIT_1) << 4u) | (WBUS_MBR_TEST_UNIT_WTT) )

#define WBUS_MBR_SE_WTT_HG2_REQ             ( ((WBUS_MBR_TEST_UNIT_WTT) << 4u) | (WBUS_MBR_HEATER_UNIT_2) )
#define WBUS_MBR_SE_WTT_HG2_ACK             ( ((WBUS_MBR_HEATER_UNIT_2) << 4u) | (WBUS_MBR_TEST_UNIT_WTT) )

#define WBUS_MBR_SE_GHI_HEATER_REQ          ( ((WBUS_MBR_IUG)           << 4u) | (WBUS_MBR_HEATER_UNIT_1) )
#define WBUS_MBR_SE_GHI_HEATER_ACK          ( ((WBUS_MBR_HEATER_UNIT_1) << 4u) | (WBUS_MBR_IUG)           )

#define WBUS_MBR_HEATER_GHI                 (WBUS_MBR_HEATER_UNIT_1)
#define WBUS_INS_HEATER_GHI                 (WBUS_INS_HG)

#define WBUS_MBR_SE_BROADCAST_IUG           ( ((WBUS_MBR_IUG) << 4u)           | (WBUS_MBR_IUG)           )
#define WBUS_MBR_SE_BROADCAST_HG1           ( ((WBUS_MBR_HEATER_UNIT_1) << 4u) | (WBUS_MBR_HEATER_UNIT_1) )
#define WBUS_MBR_SE_BROADCAST_HG2           ( ((WBUS_MBR_HEATER_UNIT_2) << 4u) | (WBUS_MBR_HEATER_UNIT_2) )


#define WBUS_DRV_REPEAT_MAX_BROADCAST        3u  
#define WBUS_DRV_REPEAT_TIME_BROADCAST_MS  350u  


#define WBUS_DRV_RESPONSE_TIMEOUT_GENERAL_MAX_MS          500u  

#define WBUS_DRV_RESPONSE_TIMEOUT_WTT_LARGE_MSG_MS  (500u * 3u) 


#define WBUS_DRV_WAIT_TIME_UNIT_0_US        0u  
#define WBUS_DRV_WAIT_TIME_UNIT_1_US        0u  
#define WBUS_DRV_WAIT_TIME_UNIT_2_US     9000u  
#define WBUS_DRV_WAIT_TIME_UNIT_3_US    11000u  
#define WBUS_DRV_WAIT_TIME_UNIT_4_US    13000u  
#define WBUS_DRV_WAIT_TIME_UNIT_5_US    16000u  
#define WBUS_DRV_WAIT_TIME_UNIT_6_US    21000u  
#define WBUS_DRV_WAIT_TIME_UNIT_7_US    24000u  
#define WBUS_DRV_WAIT_TIME_UNIT_8_US    27000u  
#define WBUS_DRV_WAIT_TIME_UNIT_9_US    12000u  
#define WBUS_DRV_WAIT_TIME_UNIT_10_US       0u  
#define WBUS_DRV_WAIT_TIME_UNIT_11_US       0u  
#define WBUS_DRV_WAIT_TIME_UNIT_12_US   15000u  
#define WBUS_DRV_WAIT_TIME_UNIT_13_US       0u  
#define WBUS_DRV_WAIT_TIME_UNIT_14_US   29000u  
#define WBUS_DRV_WAIT_TIME_UNIT_15_US   41000u  


#define WBUS_DRV_RETRY_TIME_ERR_UNIT_0_MS        0u  
#define WBUS_DRV_RETRY_TIME_ERR_UNIT_1_MS        0u  
#define WBUS_DRV_RETRY_TIME_ERR_UNIT_2_MS        9u  
#define WBUS_DRV_RETRY_TIME_ERR_UNIT_3_MS       11u  
#define WBUS_DRV_RETRY_TIME_ERR_UNIT_4_MS       13u  
#define WBUS_DRV_RETRY_TIME_ERR_UNIT_5_MS       16u  
#define WBUS_DRV_RETRY_TIME_ERR_UNIT_6_MS       21u  
#define WBUS_DRV_RETRY_TIME_ERR_UNIT_7_MS       24u  
#define WBUS_DRV_RETRY_TIME_ERR_UNIT_8_MS       27u  
#define WBUS_DRV_RETRY_TIME_ERR_UNIT_9_MS       12u  
#define WBUS_DRV_RETRY_TIME_ERR_UNIT_10_MS       0u  
#define WBUS_DRV_RETRY_TIME_ERR_UNIT_11_MS       0u  
#define WBUS_DRV_RETRY_TIME_ERR_UNIT_12_MS      15u  
#define WBUS_DRV_RETRY_TIME_ERR_UNIT_13_MS       0u  
#define WBUS_DRV_RETRY_TIME_ERR_UNIT_14_MS      29u  
#define WBUS_DRV_RETRY_TIME_ERR_UNIT_15_MS      41u  


#define WBUS_DRV_RETRY_MAX_ERR_UNIT_0        0u  
#define WBUS_DRV_RETRY_MAX_ERR_UNIT_1        0u  
#define WBUS_DRV_RETRY_MAX_ERR_UNIT_2        4u  
#define WBUS_DRV_RETRY_MAX_ERR_UNIT_3        4u  
#define WBUS_DRV_RETRY_MAX_ERR_UNIT_4        4u  
#define WBUS_DRV_RETRY_MAX_ERR_UNIT_5        4u  
#define WBUS_DRV_RETRY_MAX_ERR_UNIT_6        4u  
#define WBUS_DRV_RETRY_MAX_ERR_UNIT_7        4u  
#define WBUS_DRV_RETRY_MAX_ERR_UNIT_8        4u  
#define WBUS_DRV_RETRY_MAX_ERR_UNIT_9        4u  
#define WBUS_DRV_RETRY_MAX_ERR_UNIT_10       0u  
#define WBUS_DRV_RETRY_MAX_ERR_UNIT_11       0u  
#define WBUS_DRV_RETRY_MAX_ERR_UNIT_12       4u  
#define WBUS_DRV_RETRY_MAX_ERR_UNIT_13       0u  
#define WBUS_DRV_RETRY_MAX_ERR_UNIT_14       4u  
#define WBUS_DRV_RETRY_MAX_ERR_UNIT_15       4u  


#define WBUS_DRV_RETRY_TIME_REQ_UNIT_0_MS         0u  
#define WBUS_DRV_RETRY_TIME_REQ_UNIT_1_MS         0u  
#define WBUS_DRV_RETRY_TIME_REQ_UNIT_2_MS       250u  
#define WBUS_DRV_RETRY_TIME_REQ_UNIT_3_MS       220u  
#define WBUS_DRV_RETRY_TIME_REQ_UNIT_4_MS       150u  
#define WBUS_DRV_RETRY_TIME_REQ_UNIT_5_MS       150u  
#define WBUS_DRV_RETRY_TIME_REQ_UNIT_6_MS         0u  
#define WBUS_DRV_RETRY_TIME_REQ_UNIT_7_MS       300u  
#define WBUS_DRV_RETRY_TIME_REQ_UNIT_8_MS       300u  
#define WBUS_DRV_RETRY_TIME_REQ_UNIT_9_MS       290u  
#define WBUS_DRV_RETRY_TIME_REQ_UNIT_10_MS        0u  
#define WBUS_DRV_RETRY_TIME_REQ_UNIT_11_MS        0u  
#define WBUS_DRV_RETRY_TIME_REQ_UNIT_12_MS      140u  
#define WBUS_DRV_RETRY_TIME_REQ_UNIT_13_MS        0u  
#define WBUS_DRV_RETRY_TIME_REQ_UNIT_14_MS        0u  
#define WBUS_DRV_RETRY_TIME_REQ_UNIT_15_MS      500u  

#define WBUS_DRV_RETRY_TIME_REQ_UNIT_X_SLEEP_MS  70u  


#define WBUS_DRV_RETRY_MAX_REQ_UNIT_0        0u  
#define WBUS_DRV_RETRY_MAX_REQ_UNIT_1        0u  
#define WBUS_DRV_RETRY_MAX_REQ_UNIT_2        4u  
#define WBUS_DRV_RETRY_MAX_REQ_UNIT_3        4u  
#define WBUS_DRV_RETRY_MAX_REQ_UNIT_4        4u  
#define WBUS_DRV_RETRY_MAX_REQ_UNIT_5        4u  
#define WBUS_DRV_RETRY_MAX_REQ_UNIT_6        4u  
#define WBUS_DRV_RETRY_MAX_REQ_UNIT_7        4u  
#define WBUS_DRV_RETRY_MAX_REQ_UNIT_8        4u  
#define WBUS_DRV_RETRY_MAX_REQ_UNIT_9        4u  
#define WBUS_DRV_RETRY_MAX_REQ_UNIT_10       0u  
#define WBUS_DRV_RETRY_MAX_REQ_UNIT_11       0u  
#define WBUS_DRV_RETRY_MAX_REQ_UNIT_12       4u  
#define WBUS_DRV_RETRY_MAX_REQ_UNIT_13       0u  
#define WBUS_DRV_RETRY_MAX_REQ_UNIT_14       4u  
#define WBUS_DRV_RETRY_MAX_REQ_UNIT_15       4u  


typedef enum
{
    WBUS_DRVERR_OK                      = 0u,
    WBUS_DRVERR_NO_ERROR                = (WBUS_DRVERR_OK),

    WBUS_DRVERR_BUSY                    = 1u,
    WBUS_DRVERR_IDLE_TIME               = 2u,
    WBUS_DRVERR_OVERFLOW                = 3u,
    WBUS_DRVERR_UNKNOWN_MODE            = 4u,

    WBUS_DRVERR_INIT_NOT_CALLED         = 5u,
    WBUS_DRVERR_INIT_ALREADY_CALLED     = 6u,
    WBUS_DRVERR_DEINIT_ALREADY_CALLED   = 7u,

    WBUS_DRVERR_INVALID_INSTANCE        = 10u,
    WBUS_DRVERR_INVALID_DEVICE          = 11u,

    WBUS_DRVERR_INVALID_ADDRESS         = 50u,
    WBUS_DRVERR_INVALID_VALUE           = 52u,
    WBUS_DRVERR_INVALID_SIZE            = 53u,
    WBUS_DRVERR_INTERBYTE_TIMEOUT       = 54u, 
    WBUS_DRVERR_TRANSMISSION_TIMEOUT    = 55u, 
    WBUS_DRVERR_RESPONSE_TIMEOUT        = 59u,

    WBUS_DRVERR_PARITY_ERROR            = 60u,
    WBUS_DRVERR_FRAMING_ERROR           = 61u,
    WBUS_DRVERR_OVERRUN_ERROR           = 62u,
    WBUS_DRVERR_BIT_ERROR               = 63u,
    WBUS_DRVERR_UNEXPECTED_RX           = 65u,
    WBUSDRV_DRVERR_COM_META_ERROR       = 69u,
    WBUS_DRVERR_ECHO                    = 70u,
    WBUS_DRVERR_ENUM_MAX,                  
    WBUS_DRVERR_ENUM_FORCE_TYPE = 0x7F     
} enum_WBUS_DRIVER_ERROR_t;
typedef enum_WBUS_DRIVER_ERROR_t    WBUS_DRIVER_ERROR_t;
_Static_assert((sizeof(WBUS_DRIVER_ERROR_t)==sizeof(uint8_t)),"Analyze size for 'WBUS_DRIVER_ERROR_t'!");


#define WBUS_DRV_COMERR_NONE        0x00u  
#define WBUS_DRV_COMERR_BITERROR    0x01u  
#define WBUS_DRV_COMERR_RES02       0x02u  
#define WBUS_DRV_COMERR_OVERRUN     0x04u  
#define WBUS_DRV_COMERR_FRAMING     0x08u  
#define WBUS_DRV_COMERR_EXPBIT      0x10u  
#define WBUS_DRV_COMERR_IDMATCH     0x20u  
#define WBUS_DRV_COMERR_PARITY      0x40u  
#define WBUS_DRV_COMERR_RES80       0x80u  

#define WBUS_DRV_COMERR_MASK    ( 0u \
                                | (WBUS_DRV_COMERR_BITERROR)\
                                | (WBUS_DRV_COMERR_OVERRUN)\
                                | (WBUS_DRV_COMERR_FRAMING)\
                                | (WBUS_DRV_COMERR_PARITY)\
                                )


#define WBUS_INS_STATE_NONE         0x00u 
#define WBUS_INS_STATE_CONSTRUCTED  0x01u 
#define WBUS_INS_STATE_INITIALIZED  0x02u 
#define WBUS_INS_STATE_04           0x04u 
#define WBUS_INS_STATE_08           0x08u 
#define WBUS_INS_STATE_10           0x10u 
#define WBUS_INS_STATE_20           0x20u 
#define WBUS_INS_STATE_40           0x40u 
#define WBUS_INS_STATE_80           0x80u 


#define WBUS_DEV_STATE_NONE         0x00u 
#define WBUS_DEV_STATE_CONSTRUCTED  0x01u 
#define WBUS_DEV_STATE_INITIALIZED  0x02u 
#define WBUS_DEV_STATE_BOUND        0x04u 
#define WBUS_DEV_STATE_08           0x08u 
#define WBUS_DEV_STATE_10           0x10u 
#define WBUS_DEV_STATE_20           0x20u 
#define WBUS_DEV_STATE_40           0x40u 
#define WBUS_DEV_STATE_80           0x80u 


#define WBUS_DEV_COM_STATE_NONE          0x00u  
#define WBUS_DEV_COM_STATE_INIT          0x01u  
#define WBUS_DEV_COM_STATE_RX_ACTIVE     0x02u  
#define WBUS_DEV_COM_STATE_RX_PROGRESS   0x04u  
#define WBUS_DEV_COM_STATE_RX_CALLBACK   0x08u  
#define WBUS_DEV_COM_STATE_TX_ACTIVE     0x10u  
#define WBUS_DEV_COM_STATE_TX_CALLBACK   0x20u  
#define WBUS_DEV_COM_STATE_40            0x40u  
#define WBUS_DEV_COM_STATE_WAIT_TIME     0x80u  


#define WBUS_DEVCOMSTA_RESET( wbus_dev )  \
    (WBUS_DEVICE[(wbus_dev)].wbus_dev_com_state = (WBUS_DEV_COM_STATE_INIT) )


#define WBUS_DEVCOMSTA_GET_STATUS( wbus_dev )  \
    (WBUS_DEVICE[(wbus_dev)].wbus_dev_com_state )


#define WBUS_DEVCOMSTA_SET_INITIALIZED( wbus_dev )  \
    (WBUS_DEVICE[(wbus_dev)].wbus_dev_com_state &= ~((WBUS_DEV_COM_STATE_RX_ACTIVE)   | \
                                                     (WBUS_DEV_COM_STATE_RX_PROGRESS) | \
                                                     (WBUS_DEV_COM_STATE_RX_CALLBACK) | \
                                                     (WBUS_DEV_COM_STATE_TX_ACTIVE)   | \
                                                     (WBUS_DEV_COM_STATE_TX_CALLBACK) | \
                                                     (WBUS_DEV_COM_STATE_40)          )); \
    (WBUS_DEVICE[(wbus_dev)].wbus_dev_com_state |= (WBUS_DEV_COM_STATE_INIT));


#define WBUS_DEVCOMSTA_IS_NOT_INITIALIZED( wbus_dev )  \
    ( 0u == ( WBUS_DEVICE[(wbus_dev)].wbus_dev_com_state & (WBUS_DEV_COM_STATE_INIT)) )


#define WBUS_DEVCOMSTA_IS_IDLE( wbus_dev )  \
    ( (WBUS_DEV_COM_STATE_INIT) == ( WBUS_DEVICE[(wbus_dev)].wbus_dev_com_state & ((WBUS_DEV_COM_STATE_INIT)        | \
                                                                                   (WBUS_DEV_COM_STATE_RX_ACTIVE)   | \
                                                                                   (WBUS_DEV_COM_STATE_RX_PROGRESS) | \
                                                                                   (WBUS_DEV_COM_STATE_RX_CALLBACK) | \
                                                                                   (WBUS_DEV_COM_STATE_TX_ACTIVE)   | \
                                                                                   (WBUS_DEV_COM_STATE_TX_CALLBACK) ) ) )


#define WBUS_DEVCOMSTA_IS_READY_FOR_TX( wbus_dev )  \
    ( (WBUS_DEV_COM_STATE_INIT) == ( WBUS_DEVICE[(wbus_dev)].wbus_dev_com_state & ((WBUS_DEV_COM_STATE_INIT)        | \
                                                                                  \
                                                                                   (WBUS_DEV_COM_STATE_RX_PROGRESS) | \
                                                                                   (WBUS_DEV_COM_STATE_RX_CALLBACK) | \
                                                                                   (WBUS_DEV_COM_STATE_TX_ACTIVE)   | \
                                                                                   (WBUS_DEV_COM_STATE_TX_CALLBACK) ) ) )


#define WBUS_DEVCOMSTA_SET_WAIT_TIME( wbus_dev )  \
    WBUS_DEVICE[(wbus_dev)].wbus_dev_com_state |= (WBUS_DEV_COM_STATE_WAIT_TIME)
#define WBUS_DEVCOMSTA_CLR_WAIT_TIME( wbus_dev )  \
    WBUS_DEVICE[(wbus_dev)].wbus_dev_com_state &= ~(WBUS_DEV_COM_STATE_WAIT_TIME)
#define WBUS_DEVCOMSTA_IS_WAIT_TIME( wbus_dev )  \
    ( 0u != ( WBUS_DEVICE[(wbus_dev)].wbus_dev_com_state & ((WBUS_DEV_COM_STATE_WAIT_TIME)) ) )


#define WBUS_DEVCOMSTA_SET_RX_ACTIVE( wbus_dev )  \
    WBUS_DEVICE[(wbus_dev)].wbus_dev_com_state |= (WBUS_DEV_COM_STATE_RX_ACTIVE)
#define WBUS_DEVCOMSTA_CLR_RX_ACTIVE( wbus_dev )\
    WBUS_DEVICE[(wbus_dev)].wbus_dev_com_state &= ~((WBUS_DEV_COM_STATE_RX_ACTIVE)|(WBUS_DEV_COM_STATE_RX_PROGRESS))
#define WBUS_DEVCOMSTA_IS_RX_ACTIVE( wbus_dev ) \
    ( 0u != ( WBUS_DEVICE[(wbus_dev)].wbus_dev_com_state & ((WBUS_DEV_COM_STATE_RX_ACTIVE)|(WBUS_DEV_COM_STATE_RX_PROGRESS)) ) )

#define WBUS_DEVCOMSTA_SET_RX_PROGRESS( wbus_dev ) \
    WBUS_DEVICE[(wbus_dev)].wbus_dev_com_state |= (WBUS_DEV_COM_STATE_RX_PROGRESS)
#define WBUS_DEVCOMSTA_CLR_RX_PROGRESS( wbus_dev ) \
    WBUS_DEVICE[(wbus_dev)].wbus_dev_com_state &= ~(WBUS_DEV_COM_STATE_RX_PROGRESS)
#define WBUS_DEVCOMSTA_IS_RX_PROGRESS( wbus_dev ) \
    ( 0u != ( WBUS_DEVICE[(wbus_dev)].wbus_dev_com_state & ((WBUS_DEV_COM_STATE_RX_PROGRESS)|(WBUS_DEV_COM_STATE_RX_CALLBACK)) ) )

#define WBUS_DEVCOMSTA_SET_RX_CALLBACK( wbus_dev ) \
    WBUS_DEVICE[(wbus_dev)].wbus_dev_com_state |= (WBUS_DEV_COM_STATE_RX_CALLBACK)
#define WBUS_DEVCOMSTA_CLR_RX_CALLBACK( wbus_dev ) \
    WBUS_DEVICE[(wbus_dev)].wbus_dev_com_state &= ~(WBUS_DEV_COM_STATE_RX_CALLBACK)
#define WBUS_DEVCOMSTA_IS_RX_CALLBACK( wbus_dev ) \
    ( 0u != ( WBUS_DEVICE[(wbus_dev)].wbus_dev_com_state & (WBUS_DEV_COM_STATE_RX_CALLBACK) ) )

#define WBUS_DEVCOMSTA_SET_TX_ACTIVE( wbus_dev ) \
    WBUS_DEVICE[(wbus_dev)].wbus_dev_com_state |= (WBUS_DEV_COM_STATE_TX_ACTIVE)
#define WBUS_DEVCOMSTA_CLR_TX_ACTIVE( wbus_dev ) \
    WBUS_DEVICE[(wbus_dev)].wbus_dev_com_state &= ~(WBUS_DEV_COM_STATE_TX_ACTIVE)
#define WBUS_DEVCOMSTA_IS_TX_ACTIVE( wbus_dev ) \
    ( 0u != ( WBUS_DEVICE[(wbus_dev)].wbus_dev_com_state & (WBUS_DEV_COM_STATE_TX_ACTIVE) ) )

#define WBUS_DEVCOMSTA_SET_TX_CALLBACK( wbus_dev ) \
    WBUS_DEVICE[(wbus_dev)].wbus_dev_com_state |= (WBUS_DEV_COM_STATE_TX_CALLBACK)
#define WBUS_DEVCOMSTA_CLR_TX_CALLBACK( wbus_dev ) \
    WBUS_DEVICE[(wbus_dev)].wbus_dev_com_state &= ~(WBUS_DEV_COM_STATE_TX_CALLBACK)
#define WBUS_DEVCOMSTA_IS_TX_CALLBACK( wbus_dev ) \
    ( 0u != ( WBUS_DEVICE[(wbus_dev)].wbus_dev_com_state & (WBUS_DEV_COM_STATE_TX_CALLBACK) ) )


typedef enum
{
    WBUS_DRV_BAUDRATE_300    =  0u, 
    WBUS_DRV_BAUDRATE_600    =  1u, 
    WBUS_DRV_BAUDRATE_1200   =  2u, 
    WBUS_DRV_BAUDRATE_2400   =  3u, 
    WBUS_DRV_BAUDRATE_4800   =  4u, 
    WBUS_DRV_BAUDRATE_9600   =  5u, 
    WBUS_DRV_BAUDRATE_19200  =  6u, 
    WBUS_DRV_BAUDRATE_38400  =  7u, 
    WBUS_DRV_BAUDRATE_76800  =  8u, 
    WBUS_DRV_BAUDRATE_115000 =  9u, 
    WBUS_DRV_BAUDRATE_ENUM_MAX,              
    WBUS_DRV_BAUDRATE_ENUM_FORCE_TYPE = 0x7F 
} enum_WBUS_DRIVER_BAUDRATE_t;


#define WBUS_DRV_BAUDRATE_300_BD           300u  
#define WBUS_DRV_BAUDRATE_600_BD           600u  
#define WBUS_DRV_BAUDRATE_1200_BD         1200u  
#define WBUS_DRV_BAUDRATE_2400_BD         2400u  
#define WBUS_DRV_BAUDRATE_4800_BD         4800u  
#define WBUS_DRV_BAUDRATE_9600_BD         9600u  
#define WBUS_DRV_BAUDRATE_19200_BD       19200u  
#define WBUS_DRV_BAUDRATE_38400_BD       38400u  
#define WBUS_DRV_BAUDRATE_76800_BD       76800u  
#define WBUS_DRV_BAUDRATE_115000_BD     115000u  


#define WBUS_DRV_INT_BYTE_TIMEOUT_300_US        29800u  
#define WBUS_DRV_INT_BYTE_TIMEOUT_600_US        15400u  
#define WBUS_DRV_INT_BYTE_TIMEOUT_1200_US        8200u  
#define WBUS_DRV_INT_BYTE_TIMEOUT_2400_US        4600u  
#define WBUS_DRV_INT_BYTE_TIMEOUT_4800_US        2800u  
#define WBUS_DRV_INT_BYTE_TIMEOUT_9600_US        1900u  
#define WBUS_DRV_INT_BYTE_TIMEOUT_19200_US       1450u  
#define WBUS_DRV_INT_BYTE_TIMEOUT_38400_US       1230u  
#define WBUS_DRV_INT_BYTE_TIMEOUT_76800_US       1100u  
#define WBUS_DRV_INT_BYTE_TIMEOUT_115000_US      1080u  

#define WBUS_DRV_INT_BYTE_TIMEOUT_MIN_US          900u  
#define WBUS_DRV_INT_BYTE_TIMEOUT_MAX_US        33000u  
#define WBUS_DRV_INT_BYTE_TIMEOUT_DEF_US        (WBUS_DRV_INT_BYTE_TIMEOUT_MAX_US)  


#define WBUS_DRV_RESPTOUT_FCTR 1u 

#define WBUS_DRV_RESPONSE_TIMEOUT_MIN_300_US      800000u  
#define WBUS_DRV_RESPONSE_TIMEOUT_MAX_300_US     4000000u  
#define WBUS_DRV_RESPONSE_TIMEOUT_MIN_600_US      400000u  
#define WBUS_DRV_RESPONSE_TIMEOUT_MAX_600_US     2000000u  
#define WBUS_DRV_RESPONSE_TIMEOUT_MIN_1200_US     200000u  
#define WBUS_DRV_RESPONSE_TIMEOUT_MAX_1200_US    1000000u  
#define WBUS_DRV_RESPONSE_TIMEOUT_MIN_2400_US     100000u  
#define WBUS_DRV_RESPONSE_TIMEOUT_MAX_2400_US     500000u  
#define WBUS_DRV_RESPONSE_TIMEOUT_MIN_4800_US      50000u  
#define WBUS_DRV_RESPONSE_TIMEOUT_MAX_4800_US     250000u  
#define WBUS_DRV_RESPONSE_TIMEOUT_MIN_9600_US      25000u  
#define WBUS_DRV_RESPONSE_TIMEOUT_MAX_9600_US     125000u  
#define WBUS_DRV_RESPONSE_TIMEOUT_MIN_19200_US     12500u  
#define WBUS_DRV_RESPONSE_TIMEOUT_MAX_19200_US     62500u  
#define WBUS_DRV_RESPONSE_TIMEOUT_MIN_38400_US      6250u  
#define WBUS_DRV_RESPONSE_TIMEOUT_MAX_38400_US     31250u  
#define WBUS_DRV_RESPONSE_TIMEOUT_MIN_76800_US      3126u  
#define WBUS_DRV_RESPONSE_TIMEOUT_MAX_76800_US     15630u  
#define WBUS_DRV_RESPONSE_TIMEOUT_MIN_115000_US     2087u  
#define WBUS_DRV_RESPONSE_TIMEOUT_MAX_115000_US    10435u  

#define WBUS_DRV_RESPONSE_TIMEOUT_MIN_US            1800u  
#define WBUS_DRV_RESPONSE_TIMEOUT_MAX_US         4100000u  
#define WBUS_DRV_RESPONSE_TIMEOUT_DEF_US        (WBUS_DRV_RESPONSE_TIMEOUT_MAX_US)   


#define WBUS_DRV_INT_MESSAGE_TIME_MIN_300_US        37800u  
#define WBUS_DRV_INT_MESSAGE_TIME_MAX_300_US        90000u  
#define WBUS_DRV_INT_MESSAGE_TIME_MIN_600_US        19400u  
#define WBUS_DRV_INT_MESSAGE_TIME_MAX_600_US        46000u  
#define WBUS_DRV_INT_MESSAGE_TIME_MIN_1200_US       10200u  
#define WBUS_DRV_INT_MESSAGE_TIME_MAX_1200_US       24000u  
#define WBUS_DRV_INT_MESSAGE_TIME_MIN_2400_US        5600u  
#define WBUS_DRV_INT_MESSAGE_TIME_MAX_2400_US       13000u  
#define WBUS_DRV_INT_MESSAGE_TIME_MIN_4800_US        3300u  
#define WBUS_DRV_INT_MESSAGE_TIME_MAX_4800_US        7500u  
#define WBUS_DRV_INT_MESSAGE_TIME_MIN_9600_US        2150u  
#define WBUS_DRV_INT_MESSAGE_TIME_MAX_9600_US        4750u  
#define WBUS_DRV_INT_MESSAGE_TIME_MIN_19200_US       1580u  
#define WBUS_DRV_INT_MESSAGE_TIME_MAX_19200_US       3380u  
#define WBUS_DRV_INT_MESSAGE_TIME_MIN_38400_US       1290u  
#define WBUS_DRV_INT_MESSAGE_TIME_MAX_38400_US       2690u  
#define WBUS_DRV_INT_MESSAGE_TIME_MIN_76800_US       1140u  
#define WBUS_DRV_INT_MESSAGE_TIME_MAX_76800_US       2340u  
#define WBUS_DRV_INT_MESSAGE_TIME_MIN_115000_US      1100u  
#define WBUS_DRV_INT_MESSAGE_TIME_MAX_115000_US      2230u  


#define WBUS_INS_MEMBER_MAX	          8u 

#define WBUS_INS_CONSTR_PREP_PATTERN  (('I' << 24u) | ('N' << 16u) | ('S' << 8u) | ('P') ) 

#define WBUS_DEV_CONSTR_PREP_PATTERN  (('D' << 24u) | ('E' << 16u) | ('V' << 8u) | ('P') ) 


typedef uint8_t   WBUS_INSTANCE_STATE_t;      
typedef uint8_t   WBUS_DEVICE_STATE_t;        
typedef uint8_t   WBUS_DEVICE_COM_STATE_t;    


typedef enum    
{
    WBUS_DEV_PRP_INOUT_POL_NORMAL = 0,      
    WBUS_DEV_PRP_INOUT_POL_INVERTED,        
    WBUS_DEV_PRP_INOUT_POL_ENUM_MAX,              
    WBUS_DEV_PRP_INOUT_POL_ENUM_FORCE_TYPE = 0x7F 
} enum_WBUS_DEVIVE_INOUT_POLARITY_t;


typedef enum
{
    #ifdef WBUS_DEV_RLIN30UART_EN
    WBUS_DEV_PRP_TYP_RLIN30UART,    
    #endif
    #ifdef WBUS_DEV_RLIN31UART_EN
    WBUS_DEV_PRP_TYP_RLIN31UART,    
    #endif
    #ifdef WBUS_DEV_RLIN32UART_EN
    WBUS_DEV_PRP_TYP_RLIN32UART,    
    #endif
    #ifdef WBUS_DEV_RLIN33UART_EN
    WBUS_DEV_PRP_TYP_RLIN33UART,    
    #endif
    #ifdef WBUS_DEV_RLIN34UART_EN
    WBUS_DEV_PRP_TYP_RLIN34UART,    
    #endif
    #ifdef WBUS_DEV_RLIN35UART_EN
    WBUS_DEV_PRP_TYP_RLIN35UART,    
    #endif
    #ifdef WBUS_DEV_COM0_EN
    WBUS_DEV_PRP_TYP_COM0,          
    #endif
    WBUS_DEV_PRP_TYP_ENUM_MAX,              
    WBUS_DEV_PRP_TYP_ENUM_FORCE_TYPE = 0x7F 
} enum_WBUS_DEVICE_PROPERTY_TYP_t;



#endif 


