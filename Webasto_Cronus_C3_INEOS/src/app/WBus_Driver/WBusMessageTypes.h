#ifndef __WBUS_MESSAGE_TYPES_H_
#define __WBUS_MESSAGE_TYPES_H_


typedef struct
{
    uint8_t     trm_rcp_SE;     
    uint8_t     length;         
    uint8_t     mode;           
} struct_WBUS_MSG_HEADER_GENERAL_t;


typedef struct
{
    uint8_t     data[1u];   
} struct_WBUS_MSG_DATA_1BYTE_t;


typedef struct
{
    uint8_t     data[2u];   
} struct_WBUS_MSG_DATA_2BYTE_t;


typedef struct
{
    uint8_t     data[3u];   
} struct_WBUS_MSG_DATA_3BYTE_t;


typedef struct
{
    uint8_t     data[1u];   
} struct_WBUS_MSG_ACK_DATA_1BYTE_t;


typedef struct
{
    uint8_t     data[2u];   
} struct_WBUS_MSG_ACK_DATA_2BYTE_t;


typedef struct
{
    uint8_t     data[3u];   
} struct_WBUS_MSG_ACK_DATA_3BYTE_t;


typedef struct
{
    uint8_t     mode;           
    uint8_t     time;           
} struct_WBUS_MSG_DATA_CMD_MAIN_t;


typedef struct
{
    uint8_t     mode_meta;      
    uint8_t     mode;           
    uint8_t     time;           
    uint8_t     temp;           

} struct_WBUS_MSG_DATA_CMD_META_t;



typedef struct
{
    uint8_t     mode;       
    uint8_t     code;       
} struct_WBUS_MSG_NAK_DATA_t;


typedef struct
{
    uint8_t     trm_rcp_SE;     
    uint8_t     length;         
    uint8_t     mode_ack;       
    struct_WBUS_MSG_ACK_DATA_1BYTE_t    data;   
} struct_WBUS_MSG_HEADER_ACK_1BYTE_t;


typedef struct
{
    uint8_t     trm_rcp_SE;     
    uint8_t     length;         
    uint8_t     mode_ack;       
    struct_WBUS_MSG_ACK_DATA_2BYTE_t    data;   
} struct_WBUS_MSG_HEADER_ACK_2BYTE_t;


typedef struct
{
    uint8_t     trm_rcp_SE;     
    uint8_t     length;         
    uint8_t     mode_ack;       
    struct_WBUS_MSG_ACK_DATA_3BYTE_t    data;   
} struct_WBUS_MSG_HEADER_ACK_3BYTE_t;


typedef struct
{
    uint8_t     trm_rcp_SE;     
    uint8_t     length;         
    uint8_t     mode_nak;       
    struct_WBUS_MSG_NAK_DATA_t      data;   
} struct_WBUS_MSG_HEADER_NAK_t;


typedef struct
{
    uint8_t     trm_rcp_SE;     
    uint8_t     length;         
    struct_WBUS_MSG_DATA_CMD_MAIN_t data;   
} struct_WBUS_MSG_HEADER_CMD_MAIN_t;


typedef struct
{
    uint8_t     trm_rcp_SE;     
    uint8_t     length;         
    struct_WBUS_MSG_DATA_CMD_META_t data;   
} struct_WBUS_MSG_HEADER_CMD_META_t;


typedef struct
{
    uint8_t     cmd_mode;       
    uint8_t     cmd_ctl;        
} struct_WBUS_MSG_CMD_PENDING_DATA_t;


typedef struct
{
    uint8_t     trm_rcp_SE;     
    uint8_t     length;         
    uint8_t     mode;           
    struct_WBUS_MSG_CMD_PENDING_DATA_t  data;   
} struct_WBUS_MSG_HEADER_CMD_PENDING_t;






#endif 


