#ifndef IUG_OS_TASK_MANAGEMENT_H
#define IUG_OS_TASK_MANAGEMENT_H

#include "semphr.h"

#define OS_TASK_MGT_UNUSED_VAR   0xFFu 

typedef enum
{
    OS_TASK_MGT_TASK_SYSTEM_1MS           = 0u, 
    OS_TASK_MGT_TASK_APP_INIT_AND_EOL_1MS     , 
    OS_TASK_MGT_TASK_1MS                      , 
    OS_TASK_MGT_TASK_10MS                     , 
    OS_TASK_MGT_TASK_20MS                     , 
    OS_TASK_MGT_TASK_100MS                    , 
    OS_TASK_MGT_TASK_CUSTOMER                 , 
    OS_TASK_MGT_TASK_IDLE                     , 
    OS_TASK_MGT_TASK_CPU                      , 
    OS_TASK_MGT_TASK_ENUM_MAX                   
} enum_OS_TASK_MGT_TASKS;

typedef enum
{
    OS_TASK_MGT_MUTEX_IO       = 0u, 
    OS_TASK_MGT_MUTEX_PMIC         , 
    OS_TASK_MGT_MUTEX_CAN1         , 
    OS_TASK_MGT_MUTEX_CAN2         , 
    OS_TASK_MGT_MUTEX_CAN3         , 
    OS_TASK_MGT_MUTEX_RTC          , 
    OS_TASK_MGT_MUTEX_ENUM_MAX       
} enum_OS_TASK_MGT_MUTEX;
_Static_assert( (RTOS_NUMBER_OF_USED_MUTEXES) == OS_TASK_MGT_MUTEX_ENUM_MAX ,"Analyze: OS_TASK_MGT_MUTEX_ENUM_MAX and RTOS_NUMBER_OF_USED_MUTEXES");

typedef struct
{
    enum_OS_TASK_MGT_TASKS task;             
    TaskHandle_t           *ptr_task_handle; 
    uint8_t                prio_def;         
    uint8_t                *ptr_prio_run;    
    boolean                prio_var;         
} struct_os_task_mgt_task_prio_cfg;

typedef struct
{
    enum_OS_TASK_MGT_TASKS task;          
    uint16_t               cycle_def_ms;  
    uint16_t               *ptr_cycle_ms; 
    boolean                cycle_var;     
} struct_os_task_mgt_task_cycle_cfg;

typedef struct
{
    enum_OS_TASK_MGT_TASKS task;     
    uint8_t                cycle_ms; 
    uint8_t                prio;     
    boolean                used_flg; 
} struct_os_task_mgt_arrange_prio;


uint8_t os_task_mgt_init(void);

static uint8_t os_task_mgt_create_sema_mutex(void);

uint8_t os_task_mgt_create_startup_tasks(void);

uint8_t os_task_mgt_create_runtime_tasks(void);

void os_task_mgt_cyclic(void);

static void os_task_mgt_task_system_1ms(void *param);

static void os_task_mgt_task_app_init_and_eol_1ms(void *param);

static void os_task_mgt_task_1ms(void *param);

static void os_task_mgt_task_10ms(void *param);

static void os_task_mgt_task_20ms(void *param);

static void os_task_mgt_task_100ms(void * param);

static void os_task_mgt_task_customer(void * param);

static uint8_t os_task_mgt_set_cycle_time_customer_task(void);

static void os_task_mgt_arrange_task_prio(const boolean set_prio_flg);

extern inline void os_task_mgt_task_app_init_and_eol_1ms_update_last_wake_time(void);

void os_task_mgt_bl_nvm_access_update_last_wake_time(const enum_OS_TASK_MGT_TASKS task);

extern inline void os_task_mgt_set_bl_nvm_access(void);

extern inline uint8_t os_task_mgt_get_bl_nvm_access(void);

extern inline void os_task_mgt_reset_bl_nvm_access(void);

extern TaskHandle_t ext_os_task_handle_system_1ms;
extern TaskHandle_t ext_os_task_handle_1ms;
extern TaskHandle_t ext_os_task_handle_10ms;
extern TaskHandle_t ext_os_task_handle_20ms;
extern TaskHandle_t ext_os_task_handle_100ms;
extern TaskHandle_t ext_os_task_handle_customer;
extern TaskHandle_t ext_os_task_handle_app_init_and_eol;

extern SemaphoreHandle_t ext_ptr_peripheral_access_rtos_mutex[OS_TASK_MGT_MUTEX_ENUM_MAX];


#endif 


