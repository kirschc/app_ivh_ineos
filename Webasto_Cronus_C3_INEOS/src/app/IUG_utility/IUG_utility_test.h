#ifndef __IUG_UTILITY_TEST_H_
#define __IUG_UTILITY_TEST_H_

#include "WBusInclude.h"

#define IUG_UTIL_MASK_WBUS_PROT_MAJOR_CHAR(x)   ( ((uint8_t)x >> 4u) & 0x0Fu ) 
#define IUG_UTIL_MASK_WBUS_PROT_MINOR_CHAR(x)   (  (uint8_t)x        & 0x0Fu ) 

typedef enum
{
    UTILITY_SER_NUM_ASCII = 0u, 
    UTILITY_SER_NUM_DEC         
} enum_IUG_UTILITY_SER_NUM_CONVERT;


boolean                 IUG_utility_del_comm_const_mtx_binding( const enum_IUG_UNIT_ID_t unit_id, const uint8_t del_htr_binding, const uint8_t del_udv_binding );

uint8_t                 IUG_utility_get_comm_const_mtx_entry( const uint8_t idx_com_cst, 
                                                              IUG_UNIT_COMM_MATRIX_NVM_t * const p_com_cst );
IUG_UNIT_HEATER_CONTROL_t * IUG_utility_get_htr_ctl_ptr_from_rd_id_pointer( IUG_UNIT_READ_ID_t * const p_rd_id );
IUG_HEATER_UNIT_ENUM_t  IUG_utility_get_htr_idx_from_htr_binding( const IUG_HTR_MSK_ID_t htr_binding );
boolean                 IUG_utility_get_pointer_unit_ctl_data_via_uint_idx(  const uint8_t idx_unit_tree, 
                                                                             IUG_UNIT_CONTROL_HEADER_t ** const p_ctl );
uint8_t IUG_utility_get_pointer_unit_ctl_data_via_wbus_mbr( const enum_WBUS_MEMBER_t wbus_mbr,  const WBUS_INSTANCE_t wbus_ins,
                                                            IUG_UNIT_CONTROL_HEADER_t ** const p_ctl );
API_USER_DEVICE_ID_t    IUG_utility_get_udv_idx_from_udv_binding( const IUG_USRDEV_MSK_ID_t udv_binding );
IUG_WEBPAR_TIME_COPY_t  IUG_utility_RTC_time_copy( const IUG_WEBPAR_TIME_COPY_t cmd, struct_rtc_time_t * const rtc_time, IUG_WEBPAR_TIME_t * const rtc_time_iug );

enum_WBUS_DRIVER_BAUDRATE_t IUG_utility_test_wbus_baudrate( const enum_WBUS_DRIVER_BAUDRATE_t baud_rate );


boolean IUG_utility_test_wtt_connected_heater_off_condition( void );
boolean IUG_utility_test_wtt_connected_iug_active( void );

uint8_t IUG_utility_test_avail_htr_unit_type(const IUG_HG_AVAILABLE_t check_heating_unit);



boolean                 IUG_utility_test_comm_const_mtx_binding_fast( IUG_UNIT_COMM_MTX_BINDING_t * const p_commtx_binding );
IUG_HTR_MSK_ID_t        IUG_utility_test_comm_const_mtx_binding_htr( const IUG_UNIT_ID_t  unit_id );
IUG_USRDEV_MSK_ID_t     IUG_utility_test_comm_const_mtx_binding_udv( const IUG_UNIT_ID_t unit_id );
IUG_COMMAND_t           IUG_utility_test_cmd_category( const IUG_COMMAND_t cmd_test );
boolean                 IUG_utility_test_exit_to_fbl_condition( void );
WBUS_MEMBER_t           IUG_utility_test_wbus_control_unit( const WBUS_MEMBER_t wbus_mbr );
uint8_t                 IUG_utility_test_wbus_mode_cmd_category( const uint8_t mode_test );
boolean                 IUG_utility_test_wbus_mode_cmd_participants( const uint8_t wbus_mbr_SE );

uint8_t IUG_utility_test_reset_condition( void );

uint8_t             IUG_utility_test_state_against_cmd( const structure_IUG_UNIT_HEATER_CONTROL_t * const p_htr_ctl,
                                                        const structure_IUG_UNIT_CONTROL_HEADER_t * const p_cmd_ctl );
uint8_t             IUG_utility_translate_iug_cmd_to_wbus_mode( const IUG_COMMAND_t iug_cmd );
IUG_COMMAND_t       IUG_utility_translate_iug_cmd_to_iug_cmd_air( const IUG_COMMAND_t iug_cmd );
IUG_HG_STATE_AIR_t  IUG_utility_translate_tgt_htr_state_to_iug_hg_state_air( const TARGET_HTR_STATE_t tgt_htr_state );
IUG_HG_STATE_WAT_t  IUG_utility_translate_tgt_htr_state_to_iug_hg_state_wat( const TARGET_HTR_STATE_t tgt_htr_state );
IUG_STATE_AIR_t     IUG_utility_translate_tgt_state_to_iug_state_air( const IUG_TARGET_STATE_t tgt_state );
IUG_STATE_WATER_t   IUG_utility_translate_tgt_state_to_iug_state_wat( const IUG_TARGET_STATE_t tgt_state );
IUG_COMMAND_t       IUG_utility_translate_wbus_mode_to_iug_cmd( const uint8_t wbus_mode );


uint16_t IUG_utility_get_AI_value_mv(const uint16_t pin);

uint8_t IUG_utility_get_DI_level(const uint16_t pin);

void IUG_utility_set_dig_out_by_can_id(const uint32_t can_id, const uint16_t pin);

void IUG_utility_get_ecu_serial_number(uint8_t* ary_serial_number, enum_IUG_UTILITY_SER_NUM_CONVERT converter);

void IUG_utility_set_bl_can_baudrate(void);

boolean IUG_utility_test_active_iug_state( void );
#endif 


