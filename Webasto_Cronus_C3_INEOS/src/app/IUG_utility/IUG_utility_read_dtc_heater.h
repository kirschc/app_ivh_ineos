#ifndef __IUG_UTILITY_READ_DTC_HEATER_H_
#define __IUG_UTILITY_READ_DTC_HEATER_H_


#define IUG_UNIT_READ_DTC_UPDATE_CYCLE_MS   ( 2 * 60 * 1000) 


extern const IUG_DTC_UNIT_READ_DTC_ID_CFG_t IUG_DTC_UNIT_READ_DTC_ID_CFG[];
extern const uint8_t                        ext_iug_entries_IUG_DTC_UNIT_READ_DTC_ID_CFG;

extern const IUG_UNIT_READ_DTC_ID_CFG_t     IUG_UNIT_READ_DTC_ID_HEATER_AIR_CFG[];
extern const uint8_t                        ext_iug_entries_READ_DTC_ID_HEATER_AIR_CFG;

extern const IUG_UNIT_READ_DTC_ID_CFG_t     IUG_UNIT_READ_DTC_ID_HEATER_WAT_CFG[];
extern const uint8_t                        ext_iug_entries_READ_DTC_ID_HEATER_WAT_CFG;


extern const IUG_UNIT_READ_DTC_CFG_t        IUG_UNIT_READ_DTC_CFG[(IUG_HEATER_UNIT_ENUM_MAX)];



void        IUG_unit_read_dtc_cyclic( IUG_UNIT_READ_DTC_CTL_t * const p_rd_dtc, const IUG_HEATER_UNIT_ENUM_t idx_htr );
void        IUG_unit_read_dtc_acknowlege( IUG_UNIT_READ_DTC_CTL_t * const p_rd_dtc, const IUG_HEATER_UNIT_ENUM_t idx_htr );
uint16_t    IUG_unit_read_dtc_acknowlege_set_data( IUG_UNIT_READ_DTC_CTL_t * const p_rd_dtc, IUG_QUEUE_ENTRY_DATA_t * const que_dat,
                                                   const IUG_DTC_REPSRC_t dtc_rep_src );
boolean     IUG_unit_read_dtc_consider_iug_dtc( IUG_UNIT_READ_DTC_CTL_t * const p_rd_dtc, const IUG_HEATER_UNIT_ENUM_t idx_htr );
DTC_TABLE_IDX_t IUG_unit_read_dtc_consider_iug_dtc_decide( const uint8_t idx_iug_dtc_cfg, const uint8_t idx_htr );
void        IUG_unit_read_dtc_init( void );
uint16_t    IUG_unit_read_dtc_list_response_dispatch( uint8_t * const p_rd_dtc_response,   
                                                      const uint8_t rd_dtc_size_response,  
                                                      structure_IUG_UNIT_CONTROL_HEADER_t * const p_unt_ctl_S 
                                                    );
IUG_UNIT_READ_DTC_CTL_t     *IUG_unit_read_dtc_set_ctl_pointer_from_unit_ctl_pointer( const IUG_UNIT_CONTROL_HEADER_t * const p_unt_ctl_S );



#endif 

