#ifndef __IUG_UTILITY_ID_H_
#define __IUG_UTILITY_ID_H_



#define IUG_UNIT_READ_ID_NONE                        0x0000u
#define IUG_UNIT_READ_ID_0A_WBUS_VERSION             0x0001u 
#define IUG_UNIT_READ_ID_0D_WBUS_VERSION             0x0002u 
#define IUG_UNIT_READ_ID_HW_SW_VERSION               0x0004u 
#define IUG_UNIT_READ_ID_READ_ECU_CODING             0x0008u 
#define IUG_UNIT_READ_ID_READ_HEATER_SIGN            0x0010u 
#define IUG_UNIT_READ_ID_READ_HEATER_TYPE            0x0020u 
#define IUG_UNIT_READ_ID_READ_HEATER_CAPABLTY        0x0040u 
#define IUG_UNIT_READ_ID_READ_HEATER_MODE_FUNC       0x0080u 
#define IUG_UNIT_READ_ID_READ_HEATER_NAME            0x0100u 
#define IUG_UNIT_READ_ID_READ_MAX_HEATING_TIME       0x0200u 
#define IUG_UNIT_READ_ID_SERIAL_NUMBER               0x0400u 
#define IUG_UNIT_READ_ID_SERIAL_NUMBER_5109          0x0800u 
#define IUG_UNIT_READ_ID_1000                        0x1000u
#define IUG_UNIT_READ_ID_2000                        0x2000u
#define IUG_UNIT_READ_ID_4000                        0x4000u
#define IUG_UNIT_READ_ID_RDWR_VIA_BUFFER             (IUG_UNIT_RD_WR_INTERFACE_RDWR_VIA_BUFFER) 


#define IUG_UNIT_READ_ID_HG_RELATED         ((IUG_UNIT_READ_ID_0A_WBUS_VERSION)|\
                                             (IUG_UNIT_READ_ID_0D_WBUS_VERSION)|\
                                             (IUG_UNIT_READ_ID_READ_ECU_CODING)|\
                                             (IUG_UNIT_READ_ID_READ_HEATER_SIGN)|\
                                             (IUG_UNIT_READ_ID_READ_HEATER_TYPE)|\
                                             (IUG_UNIT_READ_ID_READ_HEATER_CAPABLTY)|\
                                             (IUG_UNIT_READ_ID_READ_HEATER_MODE_FUNC)|\
                                             (IUG_UNIT_READ_ID_READ_HEATER_NAME)|\
                                             (IUG_UNIT_READ_ID_READ_MAX_HEATING_TIME)|\
                                             (IUG_UNIT_READ_ID_SERIAL_NUMBER)|\
                                             (IUG_UNIT_READ_ID_SERIAL_NUMBER_5109)\
                                             )


#define IUG_UNIT_READ_ID_BE_RELATED         ((IUG_UNIT_READ_ID_0A_WBUS_VERSION)\
                                             )



#define IUG_UNIT_READ_ID_SET_ENABLED( car, id )         IUG_UNIT_RD_WR_INTERFACE_SET_ENABLED((car),(id))
#define IUG_UNIT_READ_ID_CLR_ENABLED( car, id )         IUG_UNIT_RD_WR_INTERFACE_CLR_ENABLED((car),(id))

#define IUG_UNIT_READ_ID_SET_REQUEST( car, id )         IUG_UNIT_RD_WR_INTERFACE_SET_REQUEST((car),(id))
#define IUG_UNIT_READ_ID_IS_REQUEST( car, id )          IUG_UNIT_RD_WR_INTERFACE_IS_REQUEST((car),(id))

#define IUG_UNIT_READ_ID_IS_CONFIRM( car, id )          IUG_UNIT_RD_WR_INTERFACE_IS_CONFIRM((car),(id))

#define IUG_UNIT_READ_ID_SET_RESPONSE( car, id )        IUG_UNIT_RD_WR_INTERFACE_SET_RESPONSE((car),(id))
#define IUG_UNIT_READ_ID_CFM_RESPONSE( car, id )        IUG_UNIT_RD_WR_INTERFACE_CFM_RESPONSE((car),(id))
#define IUG_UNIT_READ_ID_IS_RESPONSE( car, id )         IUG_UNIT_RD_WR_INTERFACE_IS_RESPONSE((car),(id))

#define IUG_UNIT_READ_ID_SET_ERROR( car, id )           IUG_UNIT_RD_WR_INTERFACE_SET_ERROR((car),(id))
#define IUG_UNIT_READ_ID_SET_ERROR_CODE( car, err )     IUG_UNIT_RD_WR_INTERFACE_SET_ERROR_CODE((car),(err))

#define IUG_UNIT_READ_ID_IS_ERROR( car, id )            IUG_UNIT_RD_WR_INTERFACE_IS_ERROR((car),(id))

#define IUG_UNIT_READ_ID_SET_RESPONSE_ERROR( car, id )  IUG_UNIT_RD_WR_INTERFACE_SET_RESPONSE_ERROR((car),(id))



typedef IUG_UNIT_RD_WR_INTERFACE_CFG_t          IUG_UNIT_READ_ID_CFG_t;


typedef struct
{
    uint8_t     res_service;        
    uint8_t     res_service_sub;    

    uint8_t     id_type;            

    void        *p_id_data;         

} struct_IUG_READ_ID_DATA_ADDRESS_CFG_t;
typedef struct_IUG_READ_ID_DATA_ADDRESS_CFG_t   IUG_READ_ID_DATA_ADDRESS_CFG_t;



extern const IUG_UNIT_READ_ID_CFG_t     IUG_UNIT_READ_ID_CFG[];
extern const uint8_t                    ext_iug_entries_IUG_UNIT_READ_ID_CFG;



#endif 

