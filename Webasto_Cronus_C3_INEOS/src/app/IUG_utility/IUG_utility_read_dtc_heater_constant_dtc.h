#ifndef __IUG_UTILITY_READ_DTC_HEATER_CONSTANT_DTC_H__
#define __IUG_UTILITY_READ_DTC_HEATER_CONSTANT_DTC_H__



typedef enum
{
    WBUS_UNIT_DTC_NONE                          = 0x00u,    
    WBUS_UNIT_DTC_ECU_DEFECTIVE                 = 0x01u,    
    WBUS_UNIT_DTC_FLAME_OFF_PERM                = 0x03u,    
    WBUS_UNIT_DTC_KL30_OV                       = 0x04u,    
    WBUS_UNIT_DTC_UEHFL                         = 0x06u,    
    WBUS_UNIT_DTC_STFL                          = 0x07u,    
    WBUS_UNIT_DTC_W_BUS_COMM                    = 0x12u,    
    WBUS_UNIT_DTC_FUEL_MIN                      = 0x24u,    
    WBUS_UNIT_DTC_FLAME_OFF_RETRY               = 0x2Fu,    
    WBUS_UNIT_DTC_START_FAILED_FIRST            = 0x38u,    
    WBUS_UNIT_DTC_START_FAILED_FIRST_NO_START   = 0x39u,    
    WBUS_UNIT_DTC_FLAME_OFF_FAZ                 = 0x83u,    
    WBUS_UNIT_DTC_KL30_UV                       = 0x84u,    
    WBUS_UNIT_DTC_HGVP                          = 0x87u,    
    WBUS_UNIT_DTC_DOSING_PUMP                   = 0x88u,    
    WBUS_UNIT_DTC_COOLANT_PUMP                  = 0x8Bu,    
    WBUS_UNIT_DTC_PENDING_ERR                   = 0x92u,    
    #ifdef IUG_WBUS_UNIT_READ_DTC_ADVANCED_DISPLAY_EN
    WBUS_UNIT_DTC_CMD_REFUSED                   = 0xE0u,    
    WBUS_UNIT_DTC_CMD_DOUBLED                   = 0xE1u,    
    WBUS_UNIT_DTC_HEATER_SWITCH_OFF             = 0xE2u,    
    WBUS_UNIT_DTC_NO_COND_HEATING               = 0xE3u,    
    WBUS_UNIT_DTC_NO_COND_VENTILATING           = 0xE4u,    
    WBUS_UNIT_DTC_E5                            = 0xE5u,    
    WBUS_UNIT_DTC_E6                            = 0xE6u,    
    WBUS_UNIT_DTC_E7                            = 0xE7u,    
    WBUS_UNIT_DTC_E8                            = 0xE8u,    
    WBUS_UNIT_DTC_E9                            = 0xE9u,    
    WBUS_UNIT_DTC_EA                            = 0xEAu,    
    WBUS_UNIT_DTC_EB                            = 0xEBu,    
    WBUS_UNIT_DTC_EC                            = 0xECu,    
    WBUS_UNIT_DTC_ED                            = 0xEDu,    
    WBUS_UNIT_DTC_EE                            = 0xEEu,    
    WBUS_UNIT_DTC_EF                            = 0xEFu,    
    #endif
    WBUS_UNIT_DTC_UNKNOWN                       = 0xFFu,    
    WBUS_UNIT_DTC_ENUM_FORCE_TYPE = 0x7F        
} enum_WBUS_UNIT_DTC_ID_t;

typedef enum_WBUS_UNIT_DTC_ID_t     WBUS_UNIT_DTC_ID_t;


#define WBUS_UNIT_DTC_ST_NONE       0x00u   
#define WBUS_UNIT_DTC_ST_STORED     0x01u   
#define WBUS_UNIT_DTC_ST_ACTIVE     0x02u   
#define WBUS_UNIT_DTC_ST_CAUSAL     0x04u   


typedef struct
{
    WBUS_UNIT_DTC_ID_t      unit_dtc_id;    
    uint8_t                 dtc_status;     
    uint8_t                 dtc_cnt;        

} structure_WBUS_UNIT_DTC_t;
typedef structure_WBUS_UNIT_DTC_t   WBUS_UNIT_DTC_t;


#endif 

