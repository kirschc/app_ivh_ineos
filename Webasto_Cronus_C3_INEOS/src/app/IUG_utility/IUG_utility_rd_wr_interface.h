#ifndef _IUG_UTILITY_RD_WR_INTERFACE_H_
#define _IUG_UTILITY_RD_WR_INTERFACE_H_


typedef struct
{
    uint8_t             *buf_req;           
    uint16_t            buf_req_sze;        

    uint8_t             *buf_rsp;           
    uint16_t            buf_rsp_sze;        
    uint16_t            buf_rsp_max;        

} IUG_UNIT_RD_WR_INTERFACE_BUFFER_PARAM_t;


typedef struct
{
    IUG_UNIT_RD_WR_INTERFACE_BUFFER_PARAM_t param;              

    IUG_UNIT_RD_WR_INTERFACE_CARRIER_t      rdwr_sel_req;       
    IUG_UNIT_RD_WR_INTERFACE_CARRIER_t      rdwr_sel_cfm;       
    IUG_UNIT_RD_WR_INTERFACE_CARRIER_t      rdwr_sel_rsp;       
    IUG_UNIT_RD_WR_INTERFACE_CARRIER_t      rdwr_sel_err;       

} IUG_UNIT_RD_WR_INTERFACE_BUFFER_t;


typedef struct
{
    IUG_UNIT_RD_WR_INTERFACE_CARRIER_t  selector;           
    uint16_t                            update_cyc;         
    uint8_t     req_service;        
    uint8_t     req_service_sub;    

} IUG_UNIT_RD_WR_INTERFACE_CFG_t;


typedef enum
{
    RDWR_IF_ERRCOD_NONE             = 0x00u,    
    RDWR_IF_ERRCOD_TOUT             = 0x01u,    
    RDWR_IF_ERRCOD_NAK              = 0x02u,    
    RDWR_IF_ERRCOD_DAT_NONE         = 0x03u,    
    RDWR_IF_ERRCOD_DAT_LESS         = 0x04u,    
    RDWR_IF_ERRCOD_DAT_INVALID      = 0x05u,    
    RDWR_IF_ERRCOD_DAT_NO_PTR       = 0x06u,    
    RDWR_IF_ERRCOD_UNKNOWN          = 0x7Eu,    
    RDWR_IF_ERRCOD_ENUM_MAX         = 0x7Fu     

} IUG_UNIT_RD_WR_INTERFACE_ERROR_CODE_t;


typedef struct
{
    IUG_UNIT_RD_WR_INTERFACE_CARRIER_t      ena;                
    IUG_UNIT_RD_WR_INTERFACE_CARRIER_t      req;                
    IUG_UNIT_RD_WR_INTERFACE_CARRIER_t      cfm;                
    IUG_UNIT_RD_WR_INTERFACE_CARRIER_t      rsp;                
    IUG_UNIT_RD_WR_INTERFACE_CARRIER_t      err;                
    IUG_UNIT_RD_WR_INTERFACE_CARRIER_t      mon_flg;            
    IUG_UNIT_RD_WR_INTERFACE_CARRIER_t      last_id_flg;        
    IUG_UNIT_RD_WR_INTERFACE_CARRIER_t      id_permitted;       
    uint16_t                                mon_cnt;            
    IUG_UNIT_RD_WR_INTERFACE_ERROR_CODE_t   err_code_last;      
    uint16_t    cyc_cnt[(IUG_UNIT_RD_WR_INTERFACE_NUM_ID_MAX)]; 

    IUG_COMM_STATE_BAG_t                    com_state;          

    IUG_UNIT_RD_WR_INTERFACE_CFG_t          const * p_rdwr_cfg;
    uint8_t                                 rdwr_cfg_entries;
    uint8_t                                 rdwr_wbus_msg_mode; 

    IUG_UNIT_RD_WR_INTERFACE_BUFFER_t       rdwr_buffer;

} IUG_UNIT_RD_WR_INTERFACE_OBJECT_t;


typedef struct
{
    IUG_UNIT_RD_WR_INTERFACE_OBJECT_t   *p_rdwr_obj;            
    IUG_UNIT_RD_WR_INTERFACE_CFG_t      const *p_rdwr_cfg;      
    uint8_t                             rdwr_cfg_entries;       
    uint8_t                             rdwr_wbus_msg_mode;     

    IUG_COMM_STATE_BAG_t                *p_com_state;           
    uint16_t                            com_state_mon_cnt;      

    WBUS_MEMBER_t                       wbus_mbr;
    WBUS_MEMBER_t                       wbus_mbr_cur;
    WBUS_INSTANCE_t                     wbus_ins;
    WBUS_INSTANCE_t                     wbus_ins_cur;

    IUG_UNIT_RD_WR_INTERFACE_CARRIER_t  rdwr_id_cfm_now;        
    IUG_UNIT_RD_WR_INTERFACE_CARRIER_t  rdwr_id_err_now;        
    uint8_t                             wbus_ins_free;          
    IUG_UNIT_RD_WR_INTERFACE_CFG_t      const * p_cfg_fnd;      

} IUG_UNIT_RD_WR_INTERFACE_PARAM_t;


#define IUG_UNIT_RD_WR_INTERFACE_SET_ENABLED( car, id ) \
    (*(IUG_UNIT_RD_WR_INTERFACE_OBJECT_t *)(car)).ena |=  (id);

#define IUG_UNIT_RD_WR_INTERFACE_IS_ENABLED( car, id ) \
    ( (id) == ((*(IUG_UNIT_RD_WR_INTERFACE_OBJECT_t *)(car)).ena & (id)) )


#define IUG_UNIT_RD_WR_INTERFACE_CLR_ENABLED( car, id ) \
    (*(IUG_UNIT_RD_WR_INTERFACE_OBJECT_t *)(car)).ena &= ~(id);


#define IUG_UNIT_RD_WR_INTERFACE_SET_REQUEST( car, id ) \
    (*(IUG_UNIT_RD_WR_INTERFACE_OBJECT_t *)(car)).cfm &= ~(id); \
    (*(IUG_UNIT_RD_WR_INTERFACE_OBJECT_t *)(car)).rsp &= ~(id); \
    (*(IUG_UNIT_RD_WR_INTERFACE_OBJECT_t *)(car)).err &= ~(id); \
    (*(IUG_UNIT_RD_WR_INTERFACE_OBJECT_t *)(car)).req |=  (id);


#define IUG_UNIT_RD_WR_INTERFACE_IS_REQUEST( car, id ) \
    ( (id) == ((*(IUG_UNIT_RD_WR_INTERFACE_OBJECT_t *)(car)).req & (id)) )

#define IUG_UNIT_RD_WR_INTERFACE_IS_CONFIRM( car, id ) \
    ( (id) == ((*(IUG_UNIT_RD_WR_INTERFACE_OBJECT_t *)(car)).cfm & (id)) )

#define IUG_UNIT_RD_WR_INTERFACE_SET_RESPONSE( car, id ) \
    (*(IUG_UNIT_RD_WR_INTERFACE_OBJECT_t *)(car)).rsp |=  (id);

#define IUG_UNIT_RD_WR_INTERFACE_IS_RESPONSE( car, id ) \
    ( (id) == ((*(IUG_UNIT_RD_WR_INTERFACE_OBJECT_t *)(car)).rsp & (id)) )

#define IUG_UNIT_RD_WR_INTERFACE_CFM_RESPONSE( car, id ) \
     \
    (*(IUG_UNIT_RD_WR_INTERFACE_OBJECT_t *)(car)).req     &= ~(id); \
    (*(IUG_UNIT_RD_WR_INTERFACE_OBJECT_t *)(car)).cfm     &= ~(id); \
    (*(IUG_UNIT_RD_WR_INTERFACE_OBJECT_t *)(car)).rsp     &= ~(id); \
    (*(IUG_UNIT_RD_WR_INTERFACE_OBJECT_t *)(car)).err     &= ~(id); \
    (*(IUG_UNIT_RD_WR_INTERFACE_OBJECT_t *)(car)).mon_flg &= ~(id);     


#define IUG_UNIT_RD_WR_INTERFACE_SET_ERROR( car, id ) \
    (*(IUG_UNIT_RD_WR_INTERFACE_OBJECT_t *)(car)).err |=  (id);

#define IUG_UNIT_RD_WR_INTERFACE_SET_ERROR_CODE( car, err ) \
    (*(IUG_UNIT_RD_WR_INTERFACE_OBJECT_t *)(car)).err_code_last = (err);

#define IUG_UNIT_RD_WR_INTERFACE_IS_ERROR( car, id ) \
    ( (id) == ((*(IUG_UNIT_RD_WR_INTERFACE_OBJECT_t *)(car)).err & (id)) )

#define IUG_UNIT_RD_WR_INTERFACE_SET_RESPONSE_ERROR( car, id ) \
    (*(IUG_UNIT_RD_WR_INTERFACE_OBJECT_t *)(car)).err |=  (id); \
    (*(IUG_UNIT_RD_WR_INTERFACE_OBJECT_t *)(car)).rsp |=  (id);


uint8_t IUG_unit_rd_wr_interface_buffer_trigger( IUG_UNIT_RD_WR_INTERFACE_OBJECT_t * const rdwr_obj, 
                                                 IUG_UNIT_RD_WR_INTERFACE_BUFFER_PARAM_t * const rdwr_buffer_par );
IUG_UNIT_RD_WR_INTERFACE_CARRIER_t  IUG_unit_rd_wr_interface_cyclic_evaluating( IUG_UNIT_RD_WR_INTERFACE_PARAM_t * const rdwr_par );
void    IUG_unit_rd_wr_interface_cyclic_counters( IUG_UNIT_RD_WR_INTERFACE_PARAM_t * const rdwr_par );
void    IUG_unit_rd_wr_interface_cyclic_triggering( IUG_UNIT_RD_WR_INTERFACE_PARAM_t * const rdwr_par );
void    IUG_unit_rd_wr_interface_cyclic_monitoring( IUG_UNIT_RD_WR_INTERFACE_OBJECT_t * const rdwr_obj );
void    IUG_unit_rd_wr_interface_cyclic_processing_prolog( IUG_UNIT_RD_WR_INTERFACE_PARAM_t * const rdwr_par );
void    IUG_unit_rd_wr_interface_par_fill( IUG_UNIT_RD_WR_INTERFACE_PARAM_t    * const rdwr_par,
                                           IUG_UNIT_RD_WR_INTERFACE_OBJECT_t   * const rdwr_obj,
                                           IUG_UNIT_RD_WR_INTERFACE_CFG_t      const * rdwr_cfg,
                                           const uint8_t                       rdwr_cfg_entries,
                                           const uint8_t                       rdwr_wbus_msg_mode,
                                           IUG_UNIT_ID_t                       unit_id,
                                           const WBUS_MEMBER_t                 wbus_mbr );
void    IUG_unit_rd_wr_interface_request_confirm( IUG_UNIT_RD_WR_INTERFACE_PARAM_t * const rdwr_par, 
                                                  const IUG_UNIT_RD_WR_INTERFACE_CARRIER_t req_cfm,
                                                  const uint16_t mon_cnt );
void    IUG_unit_rd_wr_interface_request_erroneous( IUG_UNIT_RD_WR_INTERFACE_PARAM_t * const rdwr_par,
                                                    const IUG_UNIT_RD_WR_INTERFACE_CARRIER_t req_err );


#endif 


