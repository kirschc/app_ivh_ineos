#ifndef _IUG_UTILITY_UNIT_DATA_SUPPORT_H_
#define _IUG_UTILITY_UNIT_DATA_SUPPORT_H_




extern const API_UNITS_DATA_SUPPORT_CFG_t       API_UNITS_DATA_SUPPORT_CFG[];
extern const uint16_t                           ext_entries_API_UNITS_DATA_SUPPORT_CFG;

extern uint8_t     ext_API_UNITS_DATA_SUPPORT_TX_BUF[(IUG_QUEUE_DATBUF_SIZE_RDWR_VIA_BUFFER_TX)];

uint16_t    API_units_data_support_get_cfg_by_unit_data_id( const API_UNITS_DATA_SUPPORT_ID_t unit_data_id );
void        API_units_data_support_cyclic( IUG_UNIT_CONTROL_HEADER_t * const p_unt_ctl );


#endif 


