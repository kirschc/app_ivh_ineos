#ifndef __IUG_UTILITY_COMM_ABSTRACTION_H__
#define __IUG_UTILITY_COMM_ABSTRACTION_H__



#ifdef IUG_COMM_DBG_STATE_HISTORY_EN
    #define IUG_COMM_STATE_HISTORY_FILTER_NUM               10u  
    #define IUG_COMM_STATE_HISTORY_FILTER_SUGGESTION_NUM    30u  
    #define IUG_COMM_STATE_HISTORY_INITIALIZED_PATTERN      0xA0B1E8F9u
#endif


#define IUG_COMM_STATE_GET_INLINE



typedef enum
{
    IUG_COMST_____NO_COMM       = 0x00u,    
    IUG_COMST_REQ__AWAITE       = 0x10u,    
    IUG_COMST_REQ_TIMEOUT       = 0x11u,    
    IUG_COMST_REQ___EVENT       = 0x12u,    
    IUG_COMST_REQ_PROCESS       = 0x15u,    
    IUG_COMST_REQ_DISPTCH       = 0x16u,    
    IUG_COMST_ACK__AWAITE       = 0x20u,    
    IUG_COMST_ACK_TIMEOUT       = 0x21u,    
    IUG_COMST_ACK___EVENT       = 0x22u,    
    IUG_COMST_ACK_PROCESS       = 0x25u,    
    IUG_COMST_ACK_DISPTCH       = 0x26u,    
    IUG_COMST_NAK___EVENT       = 0x2Au,    
    IUG_COMST_NAK_PROCESS       = 0x2Bu,    
    IUG_COMST_NAK_DISPTCH       = 0x2Cu,    
    IUG_COMST_ENUM_MAX,                 
    IUG_COMST_ENUM_FORCE_TYPE   = 0x7F  

} IUG_COMM_STATE_t;


typedef enum
{
    IUG_COMSTID______NONE       = 0x0000u,    
    IUG_COMSTID_H1_EXTOUT       = 0x0101u,    
    IUG_COMSTID_H2_EXTOUT       = 0x0102u,    
    IUG_COMSTID_H3_EXTOUT       = 0x0103u,    
    IUG_COMSTID_H4_EXTOUT       = 0x0104u,    
    IUG_COMSTID_H5_EXTOUT       = 0x0105u,    
    IUG_COMSTID_H6_EXTOUT       = 0x0106u,    
    IUG_COMSTID_H7_EXTOUT       = 0x0107u,    
    IUG_COMSTID_H8_EXTOUT       = 0x0108u,    

    IUG_COMSTID_H1_PNDOUT       = 0x0201u,    
    IUG_COMSTID_H2_PNDOUT       = 0x0202u,    
    IUG_COMSTID_H3_PNDOUT       = 0x0203u,    
    IUG_COMSTID_H4_PNDOUT       = 0x0204u,    
    IUG_COMSTID_H5_PNDOUT       = 0x0205u,    
    IUG_COMSTID_H6_PNDOUT       = 0x0206u,    
    IUG_COMSTID_H7_PNDOUT       = 0x0207u,    
    IUG_COMSTID_H8_PNDOUT       = 0x0208u,    

    IUG_COMSTID_H1_RD_STA       = 0x0301u,    
    IUG_COMSTID_H2_RD_STA       = 0x0302u,    
    IUG_COMSTID_H3_RD_STA       = 0x0303u,    
    IUG_COMSTID_H4_RD_STA       = 0x0304u,    
    IUG_COMSTID_H5_RD_STA       = 0x0305u,    
    IUG_COMSTID_H6_RD_STA       = 0x0306u,    
    IUG_COMSTID_H7_RD_STA       = 0x0307u,    
    IUG_COMSTID_H8_RD_STA       = 0x0308u,    

    IUG_COMSTID_H1_CMPTST       = 0x0401u,    
    IUG_COMSTID_H2_CMPTST       = 0x0402u,    
    IUG_COMSTID_H3_CMPTST       = 0x0403u,    
    IUG_COMSTID_H4_CMPTST       = 0x0404u,    
    IUG_COMSTID_H5_CMPTST       = 0x0405u,    
    IUG_COMSTID_H6_CMPTST       = 0x0406u,    
    IUG_COMSTID_H7_CMPTST       = 0x0407u,    
    IUG_COMSTID_H8_CMPTST       = 0x0408u,    

    IUG_COMSTID_H1_RD_DTC       = 0x0501u,    
    IUG_COMSTID_H2_RD_DTC       = 0x0502u,    
    IUG_COMSTID_H3_RD_DTC       = 0x0503u,    
    IUG_COMSTID_H4_RD_DTC       = 0x0504u,    
    IUG_COMSTID_H5_RD_DTC       = 0x0505u,    
    IUG_COMSTID_H6_RD_DTC       = 0x0506u,    
    IUG_COMSTID_H7_RD_DTC       = 0x0507u,    
    IUG_COMSTID_H8_RD_DTC       = 0x0508u,    

    IUG_COMSTID_H1_EEP_RD       = 0x0601u,    
    IUG_COMSTID_H2_EEP_RD       = 0x0602u,    

    IUG_COMSTID_MULEXTINP       = 0x010Au,    
    IUG_COMSTID_TCOEXTINP       = 0x010Bu,    
    IUG_COMSTID_TCAEXTINP       = 0x010Cu,    
    IUG_COMSTID_TSTEXTINP       = 0x010Du,    

    IUG_COMSTID_ENUM_FORCE_TYPE   = 0x7FFF  

} IUG_COMM_STATE_ID_t;



#ifdef IUG_COMM_DBG_STATE_HISTORY_EN
typedef struct
{
    IUG_COMM_STATE_ID_t             id;
    IUG_COMM_STATE_t                st;
    uint8_t                         d1;

} structure_IUG_COMM_STATE_HISTORY_DATA_t;
typedef structure_IUG_COMM_STATE_HISTORY_DATA_t       IUG_COMM_STATE_HISTORY_DATA_t;
#endif


#ifdef IUG_COMM_DBG_STATE_HISTORY_EN
typedef struct
{
    IUG_COMM_STATE_ID_t             id;
    uint8_t                         d1;

} IUG_COMM_STATE_HISTORY_FILTER_t;
#endif


#ifdef IUG_COMM_DBG_STATE_HISTORY_EN
typedef struct
{
    uint8_t                         idx;
    IUG_COMM_STATE_ID_t             id;
    uint8_t                         d1;

} IUG_COMM_STATE_HISTORY_FILTER_SUGGESTION_t;
#endif


#ifdef IUG_COMM_DBG_STATE_HISTORY_EN
typedef struct
{
    IUG_COMM_STATE_HISTORY_FILTER_t             filter[(IUG_COMM_STATE_HISTORY_FILTER_NUM)];
    uint8_t                                     use_idx_filter_suggestion_as_filter;
    IUG_COMM_STATE_HISTORY_FILTER_SUGGESTION_t  filter_suggestion[(IUG_COMM_STATE_HISTORY_FILTER_SUGGESTION_NUM)];

    IUG_COMM_STATE_HISTORY_DATA_t               history[(IUG_COMM_DBG_STATE_HISTORY_EN)];
    uint32_t                                    initialized;
    IUG_COMM_STATE_t                            com_state_old[(IUG_COMM_DBG_STATE_HISTORY_EN)];

} IUG_COMM_STATE_HISTORY_t;
#endif


typedef struct
{
    IUG_COMM_STATE_t                com_state;

    uint16_t                        com_state_mon_cnt;

    #ifdef IUG_COMM_DBG_STATE_HISTORY_EN
    IUG_COMM_STATE_ID_t             com_state_id;
    #endif

} IUG_COMM_STATE_BAG_t;



typedef struct
{
    IUG_COMM_STATE_BAG_t    com_state;

    void                    *comm_csc_req;
    void                    *comm_csc_ack;

    uint16_t                req_ack_timeout_cnt;    
    void                    *p_req_dat;             

} structure_IUG_COMM_CASCADED_CTL_t;
typedef structure_IUG_COMM_CASCADED_CTL_t       IUG_COMM_CASC_CTL_t;




#ifndef IUG_COMM_STATE_GET_INLINE
IUG_COMM_STATE_t    IUG_comm_state_get( IUG_COMM_STATE_BAG_t * const com_bag );
#else
inline IUG_COMM_STATE_t    IUG_comm_state_get( IUG_COMM_STATE_BAG_t * const com_bag )
{
    if( 0u != com_bag )
    {
        return com_bag->com_state;
    }
    else
    {
        return (IUG_COMST_ENUM_MAX);
    }
}
#endif
void                IUG_comm_state_init( IUG_COMM_STATE_BAG_t * const com_bag );
IUG_COMM_STATE_t    IUG_comm_state_monitoring( IUG_COMM_STATE_BAG_t * const com_bag,
                                               const uint16_t timeout, const uint8_t dat1 );
IUG_COMM_STATE_t    IUG_comm_state_set( const IUG_COMM_STATE_t com_st, IUG_COMM_STATE_BAG_t * const com_bag, const uint8_t dat1 );


void                    IUG_commcasc_cyclic( IUG_COMM_CASC_CTL_t * const com_casc );
boolean                 IUG_commcasc_link_set( IUG_COMM_CASC_CTL_t * const comcsc_req, IUG_COMM_CASC_CTL_t * const comcsc_ack,
                                               const uint16_t req_timeout, const uint16_t ack_timeout );
IUG_COMM_CASC_CTL_t   * IUG_commcasc_link_test_ack( IUG_COMM_CASC_CTL_t * const comcsc_ack );
void                    IUG_commcasc_state_init( IUG_COMM_CASC_CTL_t * const com_casc );
IUG_COMM_STATE_t        IUG_commcasc_state_get( IUG_COMM_CASC_CTL_t * const com_casc );
IUG_COMM_STATE_t        IUG_commcasc_state_set( const IUG_COMM_STATE_t com_st, IUG_COMM_CASC_CTL_t * const com_casc, const uint8_t dat1 );


#endif 

