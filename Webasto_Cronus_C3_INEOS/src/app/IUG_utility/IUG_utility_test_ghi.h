#ifndef _IUG_UTILITY_TEST_GHI_H_
#define _IUG_UTILITY_TEST_GHI_H_




IUG_HEATER_UNIT_ENUM_t  IUG_utility_get_htr_idx_via_ghi_com_dev( const IUG_COMM_DEVICE_ID_t ghi_com_dev );
IUG_COMM_DEVICE_ID_t    IUG_utility_get_ghi_com_dev_via_htr_idx( const IUG_HEATER_UNIT_ENUM_t htr_idx );
uint8_t                 IUG_utility_test_ghi_htr_available( void );


#endif 


