#ifndef __IUG_UTILITY_EEPROM_RD_WR_H_
#define __IUG_UTILITY_EEPROM_RD_WR_H_


void    IUG_unit_eeprom_rd_wr_cyclic( IUG_UNIT_EEROM_RD_WR_t * const p_eep_ctl, const IUG_HEATER_UNIT_ENUM_t idx_htr );
boolean IUG_unit_eeprom_rd_wr_trigger_transmission( IUG_UNIT_EEROM_RD_WR_t * const eeprom_rdwr, const uint8_t msg_mode );




#endif 

