#ifndef _IUG_UTILITY_COMPONENT_H__
#define _IUG_UTILITY_COMPONENT_H__




#if( 8u >= (IUG_UNIT_COMP_TEST_NUM_ID_MAX))
    typedef  uint8_t    IUG_UNIT_COMP_TEST_CARRIER_t;
#elif ( 16u >= (IUG_UNIT_COMP_TEST_NUM_ID_MAX))
    typedef  uint16_t    IUG_UNIT_COMP_TEST_CARRIER_t;
#else
    #error "Analyze value for 'IUG_UNIT_COMP_TEST_NUM_ID_MAX'!"
#endif


#define IUG_UNIT_COMP_TEST_NONE                      0x0000u
#define IUG_UNIT_COMP_TEST_START                     0x0001u    
#define IUG_UNIT_COMP_TEST_0002                      0x0002u
#define IUG_UNIT_COMP_TEST_0004                      0x0004u
#define IUG_UNIT_COMP_TEST_0008                      0x0008u
#define IUG_UNIT_COMP_TEST_0010                      0x0010u
#define IUG_UNIT_COMP_TEST_0020                      0x0020u
#define IUG_UNIT_COMP_TEST_0040                      0x0040u
#define IUG_UNIT_COMP_TEST_31_GET_IMPLEMENTED        0x0080u    



#define IUG_UNIT_COMP_TEST_SET_ENABLED( car, comp ) \
    (*(IUG_UNIT_COMP_TEST_t *)(car)).ena |=  (comp);

#define IUG_UNIT_COMP_TEST_CLR_ENABLED( car, comp ) \
    (*(IUG_UNIT_COMP_TEST_t *)(car)).ena &= ~(comp);


#define IUG_UNIT_COMP_TEST_SET_REQUEST( car, comp ) \
    (*(IUG_UNIT_COMP_TEST_t *)(car)).cfm &= ~(comp); \
    (*(IUG_UNIT_COMP_TEST_t *)(car)).rsp &= ~(comp); \
    (*(IUG_UNIT_COMP_TEST_t *)(car)).err &= ~(comp); \
    (*(IUG_UNIT_COMP_TEST_t *)(car)).req |=  (comp);

#define IUG_UNIT_COMP_TEST_IS_REQUEST( car, comp ) \
    ( (comp) == ((*(IUG_UNIT_COMP_TEST_t *)(car)).req & (comp)) )


#define IUG_UNIT_COMP_TEST_IS_CONFIRM( car, comp ) \
    ( (comp) == ((*(IUG_UNIT_COMP_TEST_t *)(car)).cfm & (comp)) )


#define IUG_UNIT_COMP_TEST_SET_RESPONSE( car, comp ) \
    (*(IUG_UNIT_COMP_TEST_t *)(car)).rsp |=  (comp);

#define IUG_UNIT_COMP_TEST_CFM_RESPONSE( car, comp ) \
    (*(IUG_UNIT_COMP_TEST_t *)(car)).req &= ~(comp); \
    (*(IUG_UNIT_COMP_TEST_t *)(car)).cfm &= ~(comp); \
    (*(IUG_UNIT_COMP_TEST_t *)(car)).rsp &= ~(comp); \
    (*(IUG_UNIT_COMP_TEST_t *)(car)).err &= ~(comp);


#define IUG_UNIT_COMP_TEST_IS_RESPONSE( car, comp ) \
    ( (comp) == ((*(IUG_UNIT_COMP_TEST_t *)(car)).rsp & (comp)) )


#define IUG_UNIT_COMP_TEST_SET_ERROR( car, comp ) \
    (*(IUG_UNIT_COMP_TEST_t *)(car)).err |=  (comp);

#define IUG_UNIT_COMP_TEST_IS_ERROR( car, comp ) \
    ( (comp) == ((*(IUG_UNIT_COMP_TEST_t *)(car)).err & (comp)) )



typedef struct
{
    IUG_UNIT_COMP_TEST_CARRIER_t    selector;   

    uint8_t     req_service;        
    uint8_t     req_service_sub;    

} struct_IUG_UNIT_COMP_TEST_CFG_t;
typedef struct_IUG_UNIT_COMP_TEST_CFG_t   IUG_UNIT_COMP_TEST_CFG_t;


typedef struct
{
    WBUS_COMPONENT_t                comp_id;        

    uint8_t                         req_duration;   

    uint16_t                        req_setpoint;   

    WBUS_PHYS_VAL_M45_t             phys_val_id;    

    uint8_t                         tgt_wbus_prot;  

} struct_IUG_UNIT_COMP_TEST_REQ_IF_t;
typedef struct_IUG_UNIT_COMP_TEST_REQ_IF_t   IUG_UNIT_COMP_TEST_REQ_IF_t;


typedef struct
{
    WBUS_COMPONENT_t                comp_id;        

    uint8_t                         nak_code;       

} struct_IUG_UNIT_COMP_TEST_RSP_IF_t;
typedef struct_IUG_UNIT_COMP_TEST_RSP_IF_t   IUG_UNIT_COMP_TEST_RSP_IF_t;


typedef struct
{
    uint16_t                        req_active; 

    IUG_UNIT_COMP_TEST_REQ_IF_t     req;        
    IUG_UNIT_COMP_TEST_RSP_IF_t     rsp;        

} struct_IUG_UNIT_COMP_TEST_IF_t;
typedef struct_IUG_UNIT_COMP_TEST_IF_t   IUG_UNIT_COMP_TEST_IF_t;



extern const IUG_UNIT_COMP_TEST_CFG_t   IUG_UNIT_COMP_TEST_CFG[];
extern const uint8_t                    ext_iug_entries_IUG_UNIT_COMP_TEST_CFG;

extern const PAR_LST_CFG_t              mgl_IUG_COMP_PROPERTY_cfg;
extern const PAR_LST_CFG_t              mgl_IUG_COMP_PROPERTY_M45_cfg;




#endif 


