#ifndef _IUG_UTILITY_ACCESS_H_
#define _IUG_UTILITY_ACCESS_H_



#define IUG_UTIL_SWAP_16(x) (((x) >>  8) & 0xFFu) | (((x) << 8) & 0xFF00u)
#define IUG_UTIL_SWAP_32(x) (((x) >> 24) & 0xFFu) | (((x) << 8) & 0xFF0000u) | (((x) >> 8) & 0xFF00u) | (((x) << 24) & 0xFF000000u)


#define IUG_UTIL_READ_DAT16_FROM_BUFFER( p_dat16, p_buf ) \
                    \
                    *(uint16_t *)(p_dat16) = ( (*(uint8_t *)((uint8_t *)(p_buf) + 0u)) << 8u) + \
                                               (*(uint8_t *)((uint8_t *)(p_buf) + 1u))


#define IUG_UTIL_READ_DAT32_FROM_BUFFER( p_dat32, p_buf ) \
                    \
                    *(uint32_t *)(p_dat32) = ( (*(uint8_t *)((uint8_t *)(p_buf) + 0u)) << 24u) + \
                                             ( (*(uint8_t *)((uint8_t *)(p_buf) + 1u)) << 16u) + \
                                             ( (*(uint8_t *)((uint8_t *)(p_buf) + 2u)) <<  8u) + \
                                               (*(uint8_t *)((uint8_t *)(p_buf) + 3u))



#define IUG_UTIL_WRITE_DAT16_TO_BUFFER( p_dat16, p_buf ) \
                    \
                    (*(uint8_t *)((uint8_t *)(p_buf) + 0u)) = (*(uint16_t *)(p_dat16)) >> 8u; \
                    (*(uint8_t *)((uint8_t *)(p_buf) + 1u)) = (*(uint16_t *)(p_dat16));


#define IUG_UTIL_WRITE_DAT32_TO_BUFFER( p_dat32, p_buf ) \
                    \
                    (*(uint8_t *)((uint8_t *)(p_buf) + 0u)) = (*(uint32_t *)(p_dat32)) >> 24u; \
                    (*(uint8_t *)((uint8_t *)(p_buf) + 1u)) = (*(uint32_t *)(p_dat32)) >> 16u; \
                    (*(uint8_t *)((uint8_t *)(p_buf) + 2u)) = (*(uint32_t *)(p_dat32)) >>  8u; \
                    (*(uint8_t *)((uint8_t *)(p_buf) + 3u)) = (*(uint32_t *)(p_dat32));



#endif 


