#ifndef __IUG_UTILITY_ID_INTERFACE_H_
#define __IUG_UTILITY_ID_INTERFACE_H_





void        IUG_unit_read_id_cyclic( IUG_UNIT_READ_ID_t   * const p_rd_id, 
                                     const enum_WBUS_MEMBER_t     wbus_mbr,
                                     const IUG_HEATER_UNIT_ENUM_t idx_ghi_heater );
uint8_t     IUG_unit_read_id_request_process( IUG_QUEUE_ENTRY_DATA_t     * const que_dat, 
                                              WBUS_MSG_DESCRIPTION_CTL_t * const wbus_msg_ctl,
                                              IUG_UNIT_CONTROL_HEADER_t  * const p_unt_ctl_S );
uint8_t     IUG_unit_read_id_response_process( IUG_QUEUE_ENTRY_DATA_t * const que_dat, 
                                               WBUS_MSG_DESCRIPTION_CTL_t * const wbus_msg_ctl,
                                               structure_IUG_UNIT_CONTROL_HEADER_t     * const p_unt_ctl_S );
uint16_t    IUG_unit_read_id_single_response_dispatch( IUG_QUEUE_ENTRY_DATA_t * const que_dat, 
                                                       WBUS_MSG_DESCRIPTION_CTL_t * const wbus_msg_ctl,
                                                       IUG_UNIT_READ_ID_CFG_t const * const p_rid_cfg_cur,
                                                       IUG_UNIT_READ_ID_t * const p_ctl_rd_id ); 



#endif 

