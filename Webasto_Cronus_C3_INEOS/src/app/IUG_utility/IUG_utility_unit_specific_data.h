#ifndef __IUG_UTILITY_UNIT_SPECIFIC_DATA_H_
#define __IUG_UTILITY_UNIT_SPECIFIC_DATA_H_




extern const IUG_UNIT_RD_WR_INTERFACE_CFG_t     IUG_UNIT_SPEC_DATA_CFG[];
extern const uint8_t                            ext_iug_entries_IUG_UNIT_SPEC_DATA_CFG;


void        IUG_unit_specific_data_cyclic( IUG_UNIT_SPEC_DATA_t * const p_spc_dat, const enum_WBUS_MEMBER_t wbus_mbr );
uint8_t     IUG_unit_specific_data_get_usr_data_by_selector( const IUG_UNIT_SPEC_DATA_CARRIER_t selector,
                                                             const WBUS_MEMBER_t wbus_mbr,
                                                             uint8_t * const usr_data );
uint8_t     IUG_unit_specific_data_response_process_post( IUG_QUEUE_ENTRY_DATA_t     * const que_dat, 
                                                          WBUS_MSG_DESCRIPTION_CTL_t * const wbus_msg_ctl,
                                                          IUG_UNIT_CONTROL_HEADER_t  * const p_unt_ctl_S,
                                                          const IUG_UNIT_SPEC_DATA_CARRIER_t    spec_dat_sel );
uint8_t     IUG_unit_specific_data_response_process_pre( IUG_QUEUE_ENTRY_DATA_t       * const que_dat, 
                                                         WBUS_MSG_DESCRIPTION_CTL_t   * const wbus_msg_ctl,
                                                         IUG_UNIT_CONTROL_HEADER_t    * const p_unt_ctl_S,
                                                         IUG_UNIT_SPEC_DATA_CARRIER_t * const spec_dat_sel );


#endif 

