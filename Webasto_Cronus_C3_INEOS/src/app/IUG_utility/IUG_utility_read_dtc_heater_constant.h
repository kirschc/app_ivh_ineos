#ifndef __IUG_UTILITY_READ_DTC_HEATER_CONSTANT_H__
#define __IUG_UTILITY_READ_DTC_HEATER_CONSTANT_H__


#ifdef IUG_DEVELOPMENT_ACTIVE
    #ifdef IUG_WBUS_UNIT_READ_DTC_DEVELOPMENT_EN
        #warning "Enable 'IUG_WBUS_UNIT_READ_DTC_DEVELOPMENT_EN' for development/debugging only!!!"
    #endif
#endif


#define IUG_WBUS_UNIT_READ_DTC_TABLE_SIZE    5u 


typedef enum
{
    IUG_DTC_REPLVL_NONE = 0u,       
    IUG_DTC_REPLVL_REDUCED,         
    IUG_DTC_REPLVL_HEATER,          
    IUG_DTC_REPLVL_ENUM_MAX         
} enum_IUG_DTC_REPLVL_t;
typedef enum_IUG_DTC_REPLVL_t   IUG_DTC_REPLVL_t;


typedef enum
{
    IUG_DTC_REPSRC_DTC_TABLE = 0u,  
    IUG_DTC_REPSRC_WBUS_MSG,        
    IUG_DTC_REPSRC_ENUM_MAX         
} enum_IUG_DTC_REPSRC_t;
typedef enum_IUG_DTC_REPSRC_t   IUG_DTC_REPSRC_t;


typedef struct
{
    uint16_t    update_cyc_cnt;      

} structure_IUG_UNIT_READ_DTC_CFG_t;
typedef structure_IUG_UNIT_READ_DTC_CFG_t       IUG_UNIT_READ_DTC_CFG_t;


typedef struct
{
    WBUS_UNIT_DTC_ID_t      unit_dtc_id;    

} structure_IUG_UNIT_READ_DTC_ID_CFG_t;
typedef structure_IUG_UNIT_READ_DTC_ID_CFG_t    IUG_UNIT_READ_DTC_ID_CFG_t;


typedef struct
{
    DTC_TABLE_IDX_t         iug_dtc_id;     
    WBUS_UNIT_DTC_ID_t      unit_dtc_id;    

} structure_IUG_DTC_UNIT_READ_DTC_ID_CFG_t;
typedef structure_IUG_DTC_UNIT_READ_DTC_ID_CFG_t    IUG_DTC_UNIT_READ_DTC_ID_CFG_t;


typedef struct
{
    uint8_t     ena;            
    uint8_t     req;            
    uint8_t     cfm;            
    uint8_t     rsp;            
    uint16_t    update_cyc_cnt; 

    IUG_UNIT_READ_DTC_ID_CFG_t  *p_dtc_id;  
    uint8_t                     num_dtc_id; 

    WBUS_UNIT_DTC_t             dtc_table[(IUG_WBUS_UNIT_READ_DTC_TABLE_SIZE)];  
    uint8_t                     entries_dtc_table;  

    IUG_DTC_REPLVL_t            dtc_rep_lvl;    
    IUG_DTC_REPSRC_t            dtc_rep_src;    

    IUG_COMM_STATE_BAG_t            dtc_rep_com_state;

    uint8_t     dtc_rep_req_trigger;        
    uint8_t     dtc_rep_req_service;        
    uint8_t     dtc_rep_req_trm_rcp_SE;     
    uint8_t     dtc_rep_ack_timeout_cnt;    
    uint8_t     dtc_rep_ack_released;       
    void        *p_dtc_rep_ack_que_dat;     

    uint8_t     dtc_rep_off_by_heater;      
    #ifdef IUG_WBUS_UNIT_READ_DTC_DEVELOPMENT_EN
    uint8_t     dtc_rep_off_by_heater_force;    
    #endif

} structure_IUG_UNIT_READ_DTC_CTL_t;
typedef structure_IUG_UNIT_READ_DTC_CTL_t  IUG_UNIT_READ_DTC_CTL_t;


#endif 

