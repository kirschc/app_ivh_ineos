#ifndef _IUG_UTILITY_COMPONENT_INTERFACE_H_
#define _IUG_UTILITY_COMPONENT_INTERFACE_H_





uint16_t    IUG_utility_comp_implemented_list_response_dispatch( 
                            uint8_t * const p_rdcomp_response,   
                            const uint8_t rdcomp_size_response,  
                            IUG_UNIT_CONTROL_HEADER_t * const p_unt_ctl_S 
                                                    );
uint8_t     IUG_utility_comp_init( void );
uint16_t    IUG_utility_comp_list_store_data_m45( const IUG_UNIT_COMP_PROP_M45_NVM_ENTRY_t comp_pty_m45_data, 
                                                  IUG_UNIT_CONTROL_HEADER_t * const p_unt_ctl_S );
void        IUG_utility_comp_test_cyclic( IUG_UNIT_COMP_TEST_t * const p_comp_id, const enum_WBUS_MEMBER_t wbus_mbr );
WBUS_COMPONENT_t    IUG_utility_comp_test_start_poll_ack( IUG_UNIT_COMP_TEST_t * const p_comp_test, const WBUS_COMPONENT_t req_comp_id,
                                                          IUG_UNIT_COMP_TEST_RSP_IF_t * const rsp_dat );
WBUS_COMPONENT_t    IUG_utility_comp_test_start_trigger(  IUG_UNIT_COMP_TEST_t * const p_comp_test, const IUG_UNIT_COMP_TEST_REQ_IF_t req_dat );


#endif 

