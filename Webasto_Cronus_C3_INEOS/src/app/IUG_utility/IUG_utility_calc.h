#ifndef _IUG_UTILITY_CALC_H_
#define _IUG_UTILITY_CALC_H_



uint8_t         IUG_Uty_BitWeight( const uint32_t bit_carrier );
uint8_t         IUG_Uty_RunningNumber_LSB_Bit( const uint32_t bit_carrier );


#endif 


