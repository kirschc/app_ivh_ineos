#ifndef __IUG_UTILITY_SIGNAL_FILTER_H_
#define __IUG_UTILITY_SIGNAL_FILTER_H_



typedef enum
{
    SIG_HG1_CATEGORY_OFF = 0,           
    SIG_HG2_CATEGORY_OFF,               
    #if( (IUG_UNT_HEATER_HG_LAST) == (IUG_UNT_HEATER_HG8_DEF) )
    SIG_HG3_CATEGORY_OFF,               
    SIG_HG4_CATEGORY_OFF,               
    SIG_HG5_CATEGORY_OFF,               
    SIG_HG6_CATEGORY_OFF,               
    SIG_HG7_CATEGORY_OFF,               
    SIG_HG8_CATEGORY_OFF,               
    #endif
    SIG_CLAMP_30,                       
    UTIL_SIGNAL_FILTER_SIG_MAX          
}enum_UTIL_SIGNAL_FILTER_SIGNAL_t;

_Static_assert((SIG_HG2_CATEGORY_OFF) ==((SIG_HG1_CATEGORY_OFF)+1u),"Analyze: 'SIG_HG2_CATEGORY_OFF'!");
#if( (IUG_UNT_HEATER_HG_LAST) == (IUG_UNT_HEATER_HG8_DEF) )
_Static_assert((SIG_HG3_CATEGORY_OFF) ==((SIG_HG2_CATEGORY_OFF)+1u),"Analyze: 'SIG_HG3_CATEGORY_OFF'!");
_Static_assert((SIG_HG4_CATEGORY_OFF) ==((SIG_HG3_CATEGORY_OFF)+1u),"Analyze: 'SIG_HG4_CATEGORY_OFF'!");
_Static_assert((SIG_HG5_CATEGORY_OFF) ==((SIG_HG4_CATEGORY_OFF)+1u),"Analyze: 'SIG_HG5_CATEGORY_OFF'!");
_Static_assert((SIG_HG6_CATEGORY_OFF) ==((SIG_HG5_CATEGORY_OFF)+1u),"Analyze: 'SIG_HG6_CATEGORY_OFF'!");
_Static_assert((SIG_HG7_CATEGORY_OFF) ==((SIG_HG6_CATEGORY_OFF)+1u),"Analyze: 'SIG_HG7_CATEGORY_OFF'!");
_Static_assert((SIG_HG8_CATEGORY_OFF) ==((SIG_HG7_CATEGORY_OFF)+1u),"Analyze: 'SIG_HG8_CATEGORY_OFF'!");
#endif


#define IUG_SIGNAL_FILTER_MAX  (UTIL_SIGNAL_FILTER_SIG_MAX)


#define UTIL_SIG_FLT_MOD_LOW_PASS            0u 
#define UTIL_SIG_FLT_MOD_DELAY_FOR_ZERO      1u 
#define UTIL_SIG_FLT_MOD_DELAY_FOR_NOT_ZERO  2u 


#define UTIL_SIG_FLT_FLG_NONE            0x00u 
#define UTIL_SIG_FLT_FLG_UNUSED          0x00u 


typedef struct
{
    uint8_t  flt_mod; 
    uint16_t flt_cnt; 
    uint8_t  flt_flg; 

}struct_UTIL_SIGNAL_FILTER_CFG_t;


typedef struct
{
	uint16_t  sig_flt;  
    uint16_t  sig_old;
    uint16_t cnt;       

}struct_UTIL_SIGNAL_FILTER_WRK_t;



void    IUG_util_signal_filter_cyclic( uint16_t ms_gone );
uint8_t IUG_util_signal_filter_set( enum_UTIL_SIGNAL_FILTER_SIGNAL_t sig_idx, uint16_t sig_val );
uint8_t IUG_util_signal_filter_get( enum_UTIL_SIGNAL_FILTER_SIGNAL_t sig_idx, uint8_t *ret_err );


#endif 

