#ifndef _IUG_UTILITY_UNIT_DATA_RD_WR_SUPPORT_H_
#define _IUG_UTILITY_UNIT_DATA_RD_WR_SUPPORT_H_





extern const API_UNITS_DATA_RDWRSUP_ID_DATA_CFG_ENTRY_t         API_UNITS_DATA_RDWRSUP_ID_DATA_CFG[];
extern const API_UNITS_DATA_RDWRSUP_ST_DATA_CFG_ENTRY_t         API_UNITS_DATA_RDWRSUP_ST_DATA_CFG[];
extern const API_UNITS_DATA_RDWRSUP_DATA_CFG_BAG_t              API_UNITS_DATA_RDWRSUP_DATA_CFG_BAG[];


extern const uint16_t   ext_entries_API_UNITS_DATA_RDWRSUP_ID_DATA_CFG;
extern const uint16_t   ext_entries_API_UNITS_DATA_RDWRSUP_ST_DATA_CFG;
extern const uint8_t    ext_entries_API_UNITS_DATA_RDWRSUP_DATA_CFG_BAG;


uint16_t    IUG_rdwr_support_data_get_cfg__via_data_id( API_UNITS_DATA_RDWRSUP_GET_DESCRIPTION_t * const p_get_des, API_UNITS_DATA_RDWRSUP_ABSTRACT_DATA_CFG_ENTRY_t const * * const p_ret_cfg );
uint16_t    IUG_rdwr_support_data_id_check_cfg( void );
uint16_t    IUG_rdwr_support_data_st_check_cfg( void );



#endif 



