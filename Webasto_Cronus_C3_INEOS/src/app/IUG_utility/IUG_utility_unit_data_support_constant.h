#ifndef _IUG_UTILITY_UNIT_DATA_SUPPORT_CONSTANT_H_
#define _IUG_UTILITY_UNIT_DATA_SUPPORT_CONSTANT_H_






typedef struct
{
    API_UNITS_DATA_SUPPORT_ID_t         unit_data_id;       
    IUG_UNIT_RD_WR_INTERFACE_CARRIER_t  rdwr_selector;      
    uint8_t                             wbus_mode;          
    uint8_t                             wbus_service;       
    uint8_t                             data_size;          

} API_UNITS_DATA_SUPPORT_CFG_t;


typedef struct
{
    IUG_UNIT_API_UNIT_ID_t                  unit_id_api;        
    API_UNITS_DATA_SUPPORT_ID_t             unit_data_id;       
    API_UNITS_DATA_SUPPORT_STORAGE_t        *p_unit_data_storage;

    IUG_UNIT_API_RETVAL_t                   api_ret;            

    IUG_UNIT_ID_t                           unit_id;            
    IUG_UNIT_RD_WR_INTERFACE_OBJECT_t       *p_rdwr_obj_host;   
    IUG_UNIT_RD_WR_INTERFACE_CARRIER_t      rdwr_sel_req;       
    IUG_UNIT_RD_WR_INTERFACE_CARRIER_t      rdwr_sel_cfm;       
    IUG_UNIT_RD_WR_INTERFACE_CARRIER_t      rdwr_sel_rsp;       
    IUG_UNIT_RD_WR_INTERFACE_CARRIER_t      rdwr_sel_err;       
    IUG_UNIT_RD_WR_INTERFACE_ERROR_CODE_t   rdwr_err_code_last; 
    uint16_t                                rdwr_timeout_cnt;   

} IUG_UNT_USER_API_UNIT_DATA_CONTROL_t;




#endif 


