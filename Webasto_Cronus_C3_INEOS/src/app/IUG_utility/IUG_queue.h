#ifndef __IUG_QUEUE_H_
#define __IUG_QUEUE_H_



#undef IUG_QUEUE_IMPL_FILLING_STATUS_EN


#define IUG_QUEUE_DATA_BUFFER_SEGMENT_GRANULARITY       16u 


#define IUG_QUEUE_ALLOC_LIST_MAX                    (IUG_QUEUE_ENTRY_MAX)




#if 1
    #define IUG_QUEUE_DATA_BUFFER_MNG_POOL_SIZE \
        ( \
            \
            ( ((WBUS_INS_ENUM_MAX) * 2u) * (IUG_QUEUE_DATA_BUFFER_SIZE_MAX) ) +\
            \
            ( 2u * (IUG_QUEUE_DATA_BUFFER_SIZE_MAX) ) +\
            \
            ( ((IUG_QUEUE_ENTRY_MAX) * 32u) )\
        )
#else
    #error "Use 'IUG-Queue data buffer pool' with max. size for development/debugging only!!!"
    #define IUG_QUEUE_DATA_BUFFER_MNG_POOL_SIZE \
        ( (IUG_QUEUE_DATA_BUFFER_SIZE_MAX) * ((IUG_QUEUE_ENTRY_MAX) +1u) )
#endif



#define IUG_QUE_ETYREQ_NONE             0x00u  
#define IUG_QUE_ETYREQ_EVALUATE         0x01u  
#define IUG_QUE_ETYREQ_ANALYZE          0x02u  
#define IUG_QUE_ETYREQ_04               0x04u  
#define IUG_QUE_ETYREQ_08               0x08u  
#define IUG_QUE_ETYREQ_10               0x10u  
#define IUG_QUE_ETYREQ_20               0x20u  
#define IUG_QUE_ETYREQ_KEEP_ALIVE       0x40u  
#define IUG_QUE_ETYREQ_PROCESS          0x80u  

#define IUG_QUE_ETYREQ_NO_REQ_MSK      ((IUG_QUE_ETYREQ_KEEP_ALIVE)|\
                                        (IUG_QUE_ETYREQ_PROCESS))



#define IUG_QUE_INSREQ_NONE             0x0000u  
#define IUG_QUE_INSREQ_RECEIVE          0x0001u  
#define IUG_QUE_INSREQ_TRANSMIT         0x0002u  
#define IUG_QUE_INSREQ_0004             0x0004u  
#define IUG_QUE_INSREQ_0008             0x0008u  
#define IUG_QUE_INSREQ_0010             0x0010u  
#define IUG_QUE_INSREQ_0020             0x0020u  
#define IUG_QUE_INSREQ_IGNORE           0x0040u  
#define IUG_QUE_INSREQ_0080             0x0080u  
#define IUG_QUE_INSREQ_DO_ACK_POS       0x0100u  
#define IUG_QUE_INSREQ_DO_ACK_NEG       0x0200u  
#define IUG_QUE_INSREQ_CHANGE_BAUDRATE  0x0400u  
#define IUG_QUE_INSREQ_0800             0x0800u  
#define IUG_QUE_INSREQ_1000             0x1000u  
#define IUG_QUE_INSREQ_2000             0x2000u  
#define IUG_QUE_INSREQ_DELAY_ANALYZED   0x4000u  
#define IUG_QUE_INSREQ_PROCESS          0x8000u  


#define IUG_QUE_INSREQ_NO_REQ_MSK     ((IUG_QUE_INSREQ_DELAY_ANALYZED)|\
                                       (IUG_QUE_INSREQ_PROCESS) )


#define IUG_QUE_INSREQ_CHANGE_BAUDRATE_DELAY_MSK    ((IUG_QUE_INSREQ_RECEIVE)|\
                                                     (IUG_QUE_INSREQ_TRANSMIT)|\
                                                     (IUG_QUE_INSREQ_DO_ACK_POS)|\
                                                     (IUG_QUE_INSREQ_DO_ACK_NEG)|\
                                                     (IUG_QUE_INSREQ_DELAY_ANALYZED) )



#define IUG_QUEUE_CLR_CONFIRMATION(que_dat, wbus_ins) \
     \
    (que_dat)->ety_act_cfm = (IUG_QUE_ETYREQ_NONE); \
    (que_dat)->ins_act_cfm[(wbus_ins)] = (IUG_QUE_ETYREQ_NONE);


#define IUG_QUEUE_TRG_ANALYZE(que_dat, wbus_ins) \
     \
    (que_dat)->ety_act_req |= (IUG_QUE_ETYREQ_ANALYZE); \
     \
    (que_dat)->ety_act_req |= (IUG_QUE_ETYREQ_PROCESS);


#define IUG_QUEUE_TRG_EVALUATE(que_dat, wbus_ins) \
     \
    (que_dat)->ety_act_req |= (IUG_QUE_ETYREQ_EVALUATE); \
     \
    (que_dat)->ety_act_req |= (IUG_QUE_ETYREQ_PROCESS);

#define IUG_QUEUE_CFM_EVALUATE(que_dat, wbus_ins) \
     \
    (que_dat)->ety_act_cfm |= (IUG_QUE_ETYREQ_EVALUATE);


#define IUG_QUEUE_TRG_RECEPTION(que_dat, wbus_ins) \
     \
    (que_dat)->ins_act_req[(wbus_ins)] |= ((IUG_QUE_INSREQ_PROCESS)|(IUG_QUE_INSREQ_RECEIVE)); \
     \
    (que_dat)->ety_act_req             |= (IUG_QUE_ETYREQ_PROCESS);

#define IUG_QUEUE_CFM_RECEPTION(que_dat, wbus_ins) \
     \
    (que_dat)->ins_act_cfm[(wbus_ins)] |= (IUG_QUE_INSREQ_RECEIVE); \
    (que_dat)->ins_com_sta[wbus_ins]    = (IUG_COMSTA_IDLE);


#define IUG_QUEUE_TRG_TRANSMISSION(que_dat, wbus_ins) \
     \
    (que_dat)->ins_act_req[(wbus_ins)] |=  ((IUG_QUE_INSREQ_PROCESS)|(IUG_QUE_INSREQ_TRANSMIT)); \
    (que_dat)->ins_act_cfm[(wbus_ins)] &= ~(IUG_QUE_INSREQ_TRANSMIT); \
     \
    (que_dat)->ety_act_req             |= (IUG_QUE_ETYREQ_PROCESS);

#define IUG_QUEUE_CLR_TRANSMISSION(que_dat, wbus_ins) \
     \
    (que_dat)->ins_act_req[(wbus_ins)] &= ~(IUG_QUE_INSREQ_TRANSMIT);

#define IUG_QUEUE_CFM_TRANSMISSION(que_dat, wbus_ins) \
     \
    (que_dat)->ins_act_cfm[(wbus_ins)] |= (IUG_QUE_INSREQ_TRANSMIT); \
    (que_dat)->ins_com_sta[wbus_ins]    = (IUG_COMSTA_IDLE);

#define IUG_QUEUE_TRG_ACK(que_dat, wbus_ins) \
     \
    (que_dat)->ins_act_req[(wbus_ins)] |=  (IUG_QUE_INSREQ_DO_ACK_POS); \
    (que_dat)->ins_act_req[(wbus_ins)] |=  (IUG_QUE_INSREQ_PROCESS); \
    (que_dat)->ins_act_cfm[(wbus_ins)] &= ~(IUG_QUE_INSREQ_DO_ACK_POS); \
     \
    (que_dat)->ety_act_req             |= (IUG_QUE_ETYREQ_PROCESS);

#define IUG_QUEUE_CFM_ACK(que_dat, wbus_ins) \
     \
    (que_dat)->ins_act_cfm[(wbus_ins)] |= (IUG_QUE_INSREQ_DO_ACK_POS); \
    (que_dat)->ins_com_sta[wbus_ins]    = (IUG_COMSTA_IDLE);


#define IUG_QUEUE_TRG_NAK(que_dat, wbus_ins) \
     \
    (que_dat)->ins_act_req[(wbus_ins)] |= (IUG_QUE_INSREQ_DO_ACK_NEG); \
    (que_dat)->ins_act_req[(wbus_ins)] |= (IUG_QUE_INSREQ_PROCESS); \
     \
    (que_dat)->ety_act_req             |= (IUG_QUE_ETYREQ_PROCESS);

#define IUG_QUEUE_CFM_NAK(que_dat, wbus_ins) \
     \
    (que_dat)->ins_act_cfm[(wbus_ins)] |= (IUG_QUE_INSREQ_DO_ACK_NEG); \
    (que_dat)->ins_com_sta[wbus_ins]    = (IUG_COMSTA_IDLE);


#define IUG_QUEUE_TRG_CHANGE_BAUDRATE(que_dat, wbus_ins) \
     \
    (que_dat)->ins_act_req[(wbus_ins)] |=  (IUG_QUE_INSREQ_CHANGE_BAUDRATE); \
    (que_dat)->ins_act_req[(wbus_ins)] |=  (IUG_QUE_INSREQ_PROCESS); \
    (que_dat)->ins_act_cfm[(wbus_ins)] &= ~(IUG_QUE_INSREQ_CHANGE_BAUDRATE); \
     \
    (que_dat)->ety_act_req             |= (IUG_QUE_ETYREQ_PROCESS);

#define IUG_QUEUE_CFM_CHANGE_BAUDRATE(que_dat, wbus_ins) \
     \
    (que_dat)->ins_act_cfm[(wbus_ins)] |= (IUG_QUE_INSREQ_CHANGE_BAUDRATE);


#define IUG_QUEUE_TRG_ALIVE_TIME(que_dat) \
     \
    (que_dat)->alvtrg = 1u;


#define IUG_QUEUE_SET_KEEP_ALIVE(que_dat) \
    (que_dat)->ety_act_req |=  (IUG_QUE_ETYREQ_KEEP_ALIVE);


#define IUG_QUEUE_CLR_KEEP_ALIVE(que_dat) \
    (que_dat)->ety_act_req &=  ~(IUG_QUE_ETYREQ_KEEP_ALIVE);


#undef IUG_QUEUE_EVALUATE_FOR_OPEN_REQUEST_EN


#ifdef IUG_QUEUE_DBG_DISPLAY_BUFFER_EN
    #define IUG_QUEUE_DBG_DISPLAY_BUFFER_UPDATE( que_key )  IUG_QueueUpdateDisplayBuffer( (que_key) )
#else
    #define IUG_QUEUE_DBG_DISPLAY_BUFFER_UPDATE( que_key )
#endif

#ifdef IUG_QUEUE_DBG_DISPLAY_BUFFER_QUICK_EN
    #define IUG_QUEUE_DBG_DISPLAY_BUFFER_UPDATE_QUICK( que_ety )  IUG_QueueUpdateDisplayBufferQuick( (que_ety) )
#else
    #define IUG_QUEUE_DBG_DISPLAY_BUFFER_UPDATE_QUICK( que_ety )
#endif


typedef enum
{
    IUG_QUEPRIO_GRP_MSK         =  0xF0u,                           
    IUG_QUEPRIO_GRP_HIGH        = (0x10u & (IUG_QUEPRIO_GRP_MSK)),  
    IUG_QUEPRIO_GRP_WBUS_RXTX   = (0x20u & (IUG_QUEPRIO_GRP_MSK)),  
    IUG_QUEPRIO_GRP_GHI_RXTX    = (0x40u & (IUG_QUEPRIO_GRP_MSK)),  
    IUG_QUEPRIO_GRP_LOW         = (0xF0u & (IUG_QUEPRIO_GRP_MSK)),  


    IUG_QUEPRIO_NONE            = 0x00u,    
    IUG_QUEPRIO_HIGH            = ((IUG_QUEPRIO_GRP_HIGH)     |0x00u),   
    IUG_QUEPRIO_WBUS_HIGH       = ((IUG_QUEPRIO_GRP_WBUS_RXTX)|0x00u),  
    IUG_QUEPRIO_WBUS_REC        = ((IUG_QUEPRIO_GRP_WBUS_RXTX)|0x02u),  
    IUG_QUEPRIO_WBUS_CMD        = ((IUG_QUEPRIO_GRP_WBUS_RXTX)|0x03u),  
    IUG_QUEPRIO_WBUS_WTT        = ((IUG_QUEPRIO_GRP_WBUS_RXTX)|0x05u),  
    IUG_QUEPRIO_WBUS_RDID       = ((IUG_QUEPRIO_GRP_WBUS_RXTX)|0x0Au),  
    IUG_QUEPRIO_WBUS_RDST       = ((IUG_QUEPRIO_GRP_WBUS_RXTX)|0x0Bu),  
    IUG_QUEPRIO_WBUS_LOW        = ((IUG_QUEPRIO_GRP_WBUS_RXTX)|0x0Fu),  
    IUG_QUEPRIO_GHI_HIGH        = ((IUG_QUEPRIO_GRP_GHI_RXTX)|0x00u),   
    IUG_QUEPRIO_GHI_REC         = ((IUG_QUEPRIO_GRP_GHI_RXTX)|0x02u),   
    IUG_QUEPRIO_GHI_CMD         = ((IUG_QUEPRIO_GRP_GHI_RXTX)|0x05u),   
    IUG_QUEPRIO_GHI_RDID        = ((IUG_QUEPRIO_GRP_GHI_RXTX)|0x0Au),   
    IUG_QUEPRIO_GHI_RDST        = ((IUG_QUEPRIO_GRP_GHI_RXTX)|0x0Bu),   
    IUG_QUEPRIO_GHI_LOW         = ((IUG_QUEPRIO_GRP_GHI_RXTX)|0x0Fu),   
    IUG_QUEPRIO_LOW             = ((IUG_QUEPRIO_GRP_LOW)     |0x0Fu),   
    IUG_QUEPRI_ENUM_FORCE_TYPE  = 0x7F        

} IUG_QUEUE_PRIORITY_t;


typedef struct
{
    #ifdef IUG_QUEUE_DBG_DISPLAY_BUFFER_EN
    uint8_t     *p;     
    uint8_t     d[(sizeof(uint32_t)*2u)];  
    #endif

    uint16_t                    alive;      

    uint8_t                     alvtrg;     

    IUG_QUEUE_TIMESTAMP_t       stamp;     

} struct_IUG_QUEUE_ENTRY_CTL_t;
typedef struct_IUG_QUEUE_ENTRY_CTL_t    IUG_QUEUE_ENTRY_CTL_t;


typedef struct
{
    IUG_QUEUE_KEY_t             key_ptr_data;   
    uint16_t                    key_sze_data;   
    IUG_QUEUE_PRIORITY_t        que_priority;

    IUG_COMM_MEDIUM_ID_t        addr_ins_com_med[(IUG_WBUS_INSTANCES_MAX)];      
    IUG_COMM_DEVICE_ID_t        addr_ins_com_dev;

    WBUS_INSTANCE_t             addr_ins_rec;        
    enum_WBUS_MEMBER_t          addr_trg_iug;        

    #if( 0 < (IUG_QUEUE_ENTRY_EXTRA_DATA_MAX) )
    uint8_t                     extra_data[(IUG_QUEUE_ENTRY_EXTRA_DATA_MAX)];
    #endif

    IUG_QUEUE_FLAGS_ENTRY_t     ety_act_req;    
    IUG_QUEUE_FLAGS_ENTRY_t     ety_act_cfm;    

    IUG_QUEUE_FLAGS_INSTANCE_t  ins_act_req[(IUG_WBUS_INSTANCES_MAX)];      
    IUG_QUEUE_FLAGS_INSTANCE_t  ins_act_cfm[(IUG_WBUS_INSTANCES_MAX)];      

    uint16_t                    ins_time_next[(IUG_WBUS_INSTANCES_MAX)];    
    IUG_COMMM_STATUS_t          ins_com_sta[(IUG_WBUS_INSTANCES_MAX)];      


    uint8_t             retry_ins_err_max[(IUG_WBUS_INSTANCES_MAX)];        
    uint8_t             retry_ins_err_cur[(IUG_WBUS_INSTANCES_MAX)];        
    uint16_t            retry_ins_err_tim[(IUG_WBUS_INSTANCES_MAX)];        

    uint8_t             retry_ins_req_max[(IUG_WBUS_INSTANCES_MAX)];        
    uint8_t             retry_ins_req_cur[(IUG_WBUS_INSTANCES_MAX)];        
    uint16_t            retry_ins_req_tim[(IUG_WBUS_INSTANCES_MAX)];        

    IUG_QUEUE_TIMEOUT_t retry_ins_res_timeout[(IUG_WBUS_INSTANCES_MAX)];    

    uint16_t            ins_data_size[(IUG_WBUS_INSTANCES_MAX)];            
    uint8_t             ins_ack_nak_rec[(IUG_WBUS_INSTANCES_MAX)];          

    uint8_t             ins_be_com_incognito[(IUG_WBUS_INSTANCES_MAX)];     
    uint8_t             ins_be_com_expected[(IUG_WBUS_INSTANCES_MAX)];      

} struct_IUG_QUEUE_ENTRY_DATA_t;
typedef struct_IUG_QUEUE_ENTRY_DATA_t   IUG_QUEUE_ENTRY_DATA_t;


typedef struct
{
    IUG_QUEUE_ENTRY_CTL_t           ctl;

    IUG_QUEUE_ENTRY_DATA_t          data;

} IUG_QUEUE_ENTRY_t;


typedef enum
{
    ETYACT_EVALUATE         = (IUG_QUE_ETYREQ_EVALUATE),
    ETYACT_ANALYZE          = (IUG_QUE_ETYREQ_ANALYZE),
    ETYACT_KEEP_ALIVE       = (IUG_QUE_ETYREQ_KEEP_ALIVE),
    ETYACT_PROCESS          = (IUG_QUE_ETYREQ_PROCESS),
    ETYACT_FORCE_TYPE       = 0x7FFF    

} IUG_QUEUE_DISPLAY_ETYACT_t;

typedef enum
{
    INSACT_RECEIVE          = (IUG_QUE_INSREQ_RECEIVE),
    INSACT_TRANSMIT         = (IUG_QUE_INSREQ_TRANSMIT),
    INSACT_IGNORE           = (IUG_QUE_INSREQ_IGNORE),
    INSACT_DO_ACK_POS       = (IUG_QUE_INSREQ_DO_ACK_POS),
    INSACT_DO_ACK_NEG       = (IUG_QUE_INSREQ_DO_ACK_NEG),
    INSACT_CHANGE_BAUDRATE  = (IUG_QUE_INSREQ_CHANGE_BAUDRATE),
    INSACT_DELAY_ANALYZED   = (IUG_QUE_INSREQ_DELAY_ANALYZED),
    INSACT_PROCESS          = (IUG_QUE_INSREQ_PROCESS),
    INSACT_FORCE_TYPE       = 0x7FFFFFFF    

} IUG_QUEUE_DISPLAY_INSACT_t;


typedef struct
{
    uint8_t     seg_frst;
    uint8_t     seg_last;

} IUG_QUEUE_BUF_ALLOC_LIST_ENTRY_t;


typedef struct
{
    uint8_t                             dat_buf_alloc_used_entries;                     
    uint8_t                             dat_buf_alloc_free_entries;                     
    uint8_t                             dat_buf_alloc_free_seg_idx;                     
    IUG_QUEUE_BUF_ALLOC_LIST_ENTRY_t    dat_buf_alloc_used[(IUG_QUEUE_ALLOC_LIST_MAX)]; 
    IUG_QUEUE_BUF_ALLOC_LIST_ENTRY_t    dat_buf_alloc_free[(IUG_QUEUE_ALLOC_LIST_MAX)]; 

} IUG_QUEUE_BUF_ALLOC_LIST_t;


typedef struct
{
    IUG_QUEUE_TIMESTAMP_t           time_stamp;                 
    uint8_t                         idx_find_que_free;          

    #ifdef IUG_QUEUE_IMPL_FILLING_STATUS_EN
    uint8_t                         empty;                      
    uint8_t                         full;                       
    #endif

    uint8_t                         entry_num;                  
    uint8_t                         entry_most;                 
    uint8_t                         entry_max;                  

    uint8_t                             dat_buf_alloc_used_seg_next;                    

    uint16_t                            info_dat_buf_size__byte;                        
    uint8_t                             info_dat_buf_usage_perc;                        

    uint8_t                             *info_dat_buf_alloc_used_byte_first;            
    uint8_t                             *info_dat_buf_alloc_used_byte__last;            
    uint8_t                             *info_dat_buf_alloc_cnfg_byte__last;            


    #ifdef IUG_QUEUE_BUF_ALLOC_DBG_SUPPORT_EN
    uint8_t                             dat_buf_alloc_used_entries;                     
    uint8_t                             dat_buf_alloc_free_entries;                     
    uint8_t                             dat_buf_alloc_free_seg_idx;                     
    uint8_t                             dat_buf_alloc_used_seg_perc;                    
    IUG_QUEUE_BUF_ALLOC_LIST_ENTRY_t    dat_buf_alloc_used[(IUG_QUEUE_ALLOC_LIST_MAX)]; 
    IUG_QUEUE_BUF_ALLOC_LIST_ENTRY_t    dat_buf_alloc_free[(IUG_QUEUE_ALLOC_LIST_MAX)]; 
    #endif

    IUG_QUEUE_DISPLAY_ETYACT_t      info_display_symbol___ety_act_req;
    IUG_QUEUE_DISPLAY_INSACT_t      info_display_symbol___ins_act_req;

} IUG_QUEUE_CONTROL_t;


typedef struct
{
    IUG_QUEUE_FLAGS_ENTRY_t     ety_act_req;                            
    IUG_QUEUE_FLAGS_INSTANCE_t  ins_act_req[(IUG_WBUS_INSTANCES_MAX)];  

    #ifdef IUG_QUEUE_EVALUATE_FOR_OPEN_REQUEST_EN
    uint8_t         ety_req_open;       
    uint8_t         ins_req_open;       
    #endif

    uint8_t         ety_req_closed;     
    uint8_t         ins_req_closed;     

} struct_IUG_EVALUATION_RESULT_t;
typedef struct_IUG_EVALUATION_RESULT_t      IUG_EVALUATION_RESULT_t;


#ifdef IUG_QUEUE_DBG_DISPLAY_BUF_MNG_ALLOC_EN
typedef struct
{
    IUG_QUEUE_KEY_t                 key_ptr_data;   

} struct_IUG_QUEUE_BUFFER_MNG_POINTER_t;
typedef struct_IUG_QUEUE_BUFFER_MNG_POINTER_t   IUG_QUEUE_BUF_MNG_POINTER_t;


typedef struct
{
    uint16_t                        key_sze_data;   

} struct_IUG_QUEUE_BUFFER_MNG_SIZE_t;
typedef struct_IUG_QUEUE_BUFFER_MNG_SIZE_t      IUG_QUEUE_BUF_MNG_SIZE_t;


typedef struct
{
    IUG_QUEUE_BUF_MNG_POINTER_t     buf_ptr;
    IUG_QUEUE_BUF_MNG_SIZE_t        buf_sze;

} struct_IUG_QUEUE_BUFFER_MNG_ALLOCATION_t;
typedef struct_IUG_QUEUE_BUFFER_MNG_ALLOCATION_t      IUG_QUEUE_BUF_MNG_ALLOC_t;
#endif



#ifdef IUG_QUEUE_DBG_DISPLAY_HISTORY_NUM
typedef struct
{
    IUG_QUEUE_TIMESTAMP_t       s;          
    uint8_t                     *p;         
    uint8_t                     d[(sizeof(uint32_t)*3u)]; 

} IUG_QUEUE_HIST_ENTRY_t;

typedef struct
{
    IUG_QUEUE_HIST_ENTRY_t  HISTORY[(IUG_QUEUE_DBG_DISPLAY_HISTORY_NUM)];

} IUG_QUEUE_HIST_t;
#endif


extern IUG_QUEUE_CONTROL_t              IUG_QUEUE_CTL;

void                    IUG_QueueCyclic( const uint32_t delta_ms );
void                    IUG_QueueCyclic_Fast( void );

IUG_QUEUE_ENTRY_CTL_t   * IUG_QueueEntryGetCtlPtr( const IUG_QUEUE_KEY_t que_key, IUG_QUEUE_ENTRY_t * * const que_ety );
IUG_QUEUE_ENTRY_DATA_t  * IUG_QueueEntryGetDataPtr( const IUG_QUEUE_KEY_t que_key );
int16_t                 IUG_QueueEntryGetActiveNext( const int16_t etry_now, IUG_QUEUE_ENTRY_t * * const que_ety, IUG_QUEUE_ENTRY_DATA_t * * const que_dat );
IUG_QUEUE_KEY_t         IUG_QueueEntryGetOldestNext( const IUG_QUEUE_PRIORITY_t que_priority_get,
                                                     const IUG_QUEUE_TIMESTAMP_t time_stamp_younger_than,
                                                     IUG_QUEUE_ENTRY_CTL_t  * * const que_ctl,
                                                     IUG_QUEUE_ENTRY_DATA_t * * const que_dat );
IUG_QUEUE_KEY_t         IUG_QueueEntryRelease( const IUG_QUEUE_KEY_t key );
IUG_QUEUE_ENTRY_t *     IUG_QueueEntryReserve( const uint16_t dat_buf_size );
IUG_QUEUE_ENTRY_t *     IUG_QueueEntrySearchMatching( uint8_t * const p_msg, const IUG_COMM_DEVICE_ID_t com_dev,
                                                      const uint16_t entry_start, const int8_t search_dir, int16_t *entry_found );
IUG_QUEUE_KEY_t         IUG_QueueEntrySetTimestamp( IUG_QUEUE_ENTRY_t * const p_etyque );
struct_IUG_EVALUATION_RESULT_t IUG_QueueEvaluate( const IUG_QUEUE_KEY_t que_key );
inline uint16_t         IUG_QueueGetEntriesMax( void ) { return (IUG_QUEUE_ENTRY_MAX); };
int8_t                  IUG_QueueInit( void );
uint8_t                 IUG_QueuePriority_GetGroupList( IUG_QUEUE_PRIORITY_t * const prio_grp_list );
IUG_QUEUE_KEY_t         IUG_QueueTestData_WBus_Compare( uint8_t * const p_que_key_ref, const uint8_t offset_ref,
                                                        const uint8_t offset_cmp, const uint8_t size_cmp );
int16_t                 IUG_QueueTestDataSelective_WBus( const int16_t entry, uint8_t * const p_WBus_Data, const IUG_COMM_DEVICE_ID_t com_dev );
int                     IUG_QueueTestKey( const IUG_QUEUE_KEY_t key );
int16_t                 IUG_QueueTestKeyUsed( const IUG_QUEUE_KEY_t key );
IUG_QUEUE_KEY_t         IUG_QueueTestKeyValid( const IUG_QUEUE_KEY_t key );
IUG_QUEUE_PRIORITY_t    IUG_QueueTestQueue_AnyPriorityLower( const IUG_QUEUE_PRIORITY_t que_priority );
int16_t                 IUG_QueueTestQueue_AnyPriorityHigher( const IUG_QUEUE_PRIORITY_t que_priority );
inline void                    IUG_QueueTimestampCyclic( const uint8_t delta_ms ) { IUG_QUEUE_CTL.time_stamp += delta_ms; };
inline IUG_QUEUE_TIMESTAMP_t   IUG_QueueTimestampGet( void ) { return IUG_QUEUE_CTL.time_stamp; };

#ifdef IUG_QUEUE_DBG_DISPLAY_BUFFER_EN
void                    IUG_QueueUpdateDisplayBuffer( const IUG_QUEUE_KEY_t key );
#endif

#ifdef IUG_QUEUE_DBG_DISPLAY_BUFFER_QUICK_EN
void                    IUG_QueueUpdateDisplayBufferQuick( IUG_QUEUE_ENTRY_t * const que_ety );
#endif

#ifdef IUG_QUEUE_DBG_DISPLAY_HISTORY_NUM
void                    IUG_QueueHistoryEntry( IUG_QUEUE_ENTRY_t * const que_ety );
void                    IUG_QueueHistoryUpdate( const IUG_QUEUE_KEY_t key, const IUG_QUEUE_KEY_t key_released  );
#endif



#endif 


