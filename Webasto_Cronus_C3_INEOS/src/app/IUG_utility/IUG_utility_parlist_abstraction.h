#ifndef _IUG_UTILITY_PARLIST_ABSTRACTION_H__
#define _IUG_UTILITY_PARLIST_ABSTRACTION_H__


typedef enum
{
    PARLST_DATTYP_SI8   = 0xF0u,    
    PARLST_DATTYP_UI8   = 0xF1u,    
    PARLST_DATTYP_SI16  = 0xF2u,    
    PARLST_DATTYP_UI16  = 0xF3u,    
    PARLST_DATTYP_SI32  = 0xF4u,    
    PARLST_DATTYP_UI32  = 0xF5u,    
    PARLST_DATTYP_SI64  = 0xF6u,    
    PARLST_DATTYP_UI64  = 0xF7u,    
    PARLST_DATTYP_INV   = 0xFFu     

} enum_PAR_LIST_DATA_TYPE_t;
typedef enum_PAR_LIST_DATA_TYPE_t  PAR_LST_DAT_TYP_t;


typedef struct
{
    uint8_t                 par_id;             

    PAR_LST_DAT_TYP_t       par_typ;            

} structure_PAR_LIST_ENTRY_ID_UI8_t;
typedef structure_PAR_LIST_ENTRY_ID_UI8_t   PAR_LST_ETY_U_8_t;


typedef struct
{
    uint16_t                par_id;             

    PAR_LST_DAT_TYP_t       par_typ;            

} structure_PAR_LIST_ENTRY_ID_UI16_t;
typedef structure_PAR_LIST_ENTRY_ID_UI16_t   PAR_LST_ETY_U16_t;


typedef struct
{
    PAR_LST_DAT_TYP_t       par_id_typ;         

    void                    *par_id_list;       
    uint16_t                 par_id_num;        

} structure_PAR_LIST_CONFIGURATION_t;
typedef structure_PAR_LIST_CONFIGURATION_t       PAR_LST_CFG_t;


typedef union
{
    PAR_LST_ETY_U_8_t       *p_ety__8;
    PAR_LST_ETY_U16_t       *p_ety_16;

} union_PAR_LIST_CFG_ENTRY_POINTER_t;
typedef union_PAR_LIST_CFG_ENTRY_POINTER_t  PAR_LST_CFG_ENTRY_PTR_t;




uint16_t    IUG_parlst_check_id( PAR_LST_CFG_t * const p_parlst_cfg );
uint16_t    IUG_parlst_get_next_idx_par_id( PAR_LST_CFG_t * const p_parlst_cfg, const uint16_t idx_now, 
                                            const uint16_t par_id, PAR_LST_DAT_TYP_t * const p_par_typ );



#endif 

