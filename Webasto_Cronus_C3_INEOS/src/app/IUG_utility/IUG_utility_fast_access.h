#ifndef __IUG_UTILITY_FAST_ACCESS_H__
#define __IUG_UTILITY_FAST_ACCESS_H__

IUG_HEATER_UNIT_ENUM_t IUG_FstAcc_get_idx_heater_property_via_uint_tree( const uint8_t idx_unit_tree );


#define IUG_FstAcc_heater_available( heater_id )            ext_iug_heater_property[(heater_id)].unit_available


#define IUG_FstAcc_idx_heater_available( heater_id )        ext_iug_heater_property_idx_hgx_ava[(heater_id)]


#define IUG_FstAcc_idx_heater_air_available( heater_id )    ext_iug_heater_property_idx_air_ava[(heater_id)]


#define IUG_FstAcc_idx_heater_wat_available( heater_id )    ext_iug_heater_property_idx_wat_ava[(heater_id)]


#define IUG_FstAcc_idx_heater_ghi_available( heater_id )    ext_iug_heater_property_idx_ghi_ava[(heater_id)]


#define IUG_FstAcc_unit_available( unit_id )                ext_iug_unit_property[(unit_id)].unit_available


#define IUG_FstAcc_unit_bound_to_heater( unit_id )     ext_iug_unit_property[(unit_id)].msk_htr_bnd_ava


#define IUG_FstAcc_unit_bound_to_user_device( unit_id )     ext_iug_unit_property[(unit_id)].msk_udv_bnd_ava


#define IUG_FstAcc_wbus_mbr_available( wbus_mbr )           ext_iug_wbus_member_property[(wbus_mbr)].mbr_available



#endif 

