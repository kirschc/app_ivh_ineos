#ifndef __IUG_UTILITY_READ_STATUS_LIST_H_
#define __IUG_UTILITY_READ_STATUS_LIST_H_



typedef struct
{
    uint16_t    selector;           

    uint8_t     *p_id_list;         
    uint8_t     size_id_list;       

} struct_IUG_READ_STATUS_LIST_CFG_t;

typedef struct_IUG_READ_STATUS_LIST_CFG_t   IUG_READ_STATUS_LIST_CFG_t;


extern const IUG_READ_STATUS_LIST_CFG_t     IUG_READ_STATUS_LIST_CFG[];
extern const uint8_t                        ext_iug_entries_IUG_READ_STATUS_LIST_CFG;
extern const IUG_READ_STATUS_LIST_CFG_t     IUG_GHI_READ_STATUS_LIST_CFG[];
extern const uint8_t                        ext_iug_entries_IUG_GHI_READ_STATUS_LIST_CFG;
extern const uint8_t                        ext_iug_entries_IUG_READ_STATUS_LIST_DATA_CFG;


uint16_t IUG_unit_read_status_list_request_create( uint8_t * const p_rdst_request,    
                                                   const uint8_t * const p_rdst_list, 
                                                   const uint8_t rdst_list_number     
                                                 );
uint16_t IUG_unit_read_status_list_response_dispatch( uint8_t * const p_rdst_response,   
                                                      const uint8_t rdst_size_response,  
                                                      structure_IUG_UNIT_CONTROL_HEADER_t * const p_unt_ctl_S 
                                                    );
uint8_t IUG_unit_read_status_list_get_idx_st_id( const uint16_t status_id );
uint8_t IUG_unit_read_status_list_get_idx_st_list( IUG_READ_STATUS_LIST_CFG_t const * p_lst_cfg, const uint8_t lst_cfg_entries, const uint16_t status_id );                                                 
boolean IUG_unit_read_status_list_init( void );


#endif 

