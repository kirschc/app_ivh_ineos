#ifndef _IUG_UTILITY_UNIT_DATA_RD_WR_SUPPORT_CONSTANT_H_
#define _IUG_UTILITY_UNIT_DATA_RD_WR_SUPPORT_CONSTANT_H_





typedef struct
{
    API_UNITS_DATA_RDWRSUP_TOPIC_ID_t                  topic_id;
    API_UNITS_DATA_RDWRSUP_DATA_CFG_NUMBER_t           dat_cfg_num;

    union 
    {
        API_UNITS_DATA_RDWRSUP_ID_DATA_CFG_ENTRY_t         const *rdwr_id_cfg;
        API_UNITS_DATA_RDWRSUP_ST_DATA_CFG_ENTRY_t         const *rdwr_st_cfg;
        API_UNITS_DATA_RDWRSUP_ABSTRACT_DATA_CFG_ENTRY_t   const *rdwr_abstr_cfg;
    }   p_dat_cfg;

} API_UNITS_DATA_RDWRSUP_DATA_CFG_BAG_t;




#endif 


