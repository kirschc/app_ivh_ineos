#ifndef __IUG_UTILITY_READ_STATUS_H_
#define __IUG_UTILITY_READ_STATUS_H_



#define IUG_RDST_ID_NONE                            0x00u   
#define IUG_RDST_ID_0A_STATUS_OUTPORT_HEATER        0x0Au   
#define IUG_RDST_ID_0C_STATUS_COOL_MEDIUM_TEMP1     0x0Cu   
#define IUG_RDST_ID_0E_STATUS_SUPPLY_VOLTAGE        0x0Eu   
#define IUG_RDST_ID_28_STATUS_STATE_NUM_1BYTE       0x28u   
#define IUG_RDST_ID_29_STATUS_STATE_NUM_2BYTE       0x29u   
#define IUG_RDST_ID_2A_STATUS_STATE_STFL_FAULTY     0x2Au   
#define IUG_RDST_ID_2B_STATUS_STATE_UEHFL_OVERHEAT  0x2Bu   
#define IUG_RDST_ID_2C_STATUS_STATE_HGV_LOCKED      0x2Cu   
#define IUG_RDST_ID_39_STATUS_COOL_MEDIUM_TEMP2     0x39u   
#define IUG_RDST_ID_3A_STATUS_AIR_PRESSURE          0x3Au   


#define IUG_RDST_SIZE_NONE                             0u   
#define IUG_RDST_SIZE_1                                1u   
#define IUG_RDST_SIZE_2                                2u   
#define IUG_RDST_SIZE_3                                3u   
#define IUG_RDST_SIZE_4                                4u   
#define IUG_RDST_SIZE_5                                5u   
#define IUG_RDST_SIZE_10                              10u   
#define IUG_RDST_SIZE_12                              12u   
#define IUG_RDST_SIZE_24                              24u   
#define IUG_RDST_SIZE_U8                            0xFAu   
#define IUG_RDST_SIZE_S8                            0xFBu   
#define IUG_RDST_SIZE_U16                           0xFCu   
#define IUG_RDST_SIZE_S16                           0xFDu   
#define IUG_RDST_SIZE_U32                           0xFEu   
#define IUG_RDST_SIZE_S32                           0xFFu   

_Static_assert((IUG_RDST_SIZE_U8) ==(0xFAu),"Analyze: 'IUG_RDST_SIZE_xxx'");
_Static_assert((IUG_RDST_SIZE_S8) ==(0xFBu),"Analyze: 'IUG_RDST_SIZE_xxx'");
_Static_assert((IUG_RDST_SIZE_U16)==(0xFCu),"Analyze: 'IUG_RDST_SIZE_xxx'");
_Static_assert((IUG_RDST_SIZE_S16)==(0xFDu),"Analyze: 'IUG_RDST_SIZE_xxx'");
_Static_assert((IUG_RDST_SIZE_U32)==(0xFEu),"Analyze: 'IUG_RDST_SIZE_xxx'");
_Static_assert((IUG_RDST_SIZE_S32)==(0xFFu),"Analyze: 'IUG_RDST_SIZE_xxx'");


typedef struct
{
    uint8_t     id;                 

    uint8_t     size;               

} struct_IUG_READ_STATUS_LIST_DATA_CFG_t;
typedef struct_IUG_READ_STATUS_LIST_DATA_CFG_t   IUG_READ_STATUS_LIST_DATA_CFG_t;


typedef IUG_UNIT_RD_WR_INTERFACE_CFG_t          IUG_UNIT_READ_STATUS_CFG_t;


#ifdef IUG_UNIT_READ_STATUS_AVAILABLE_DECIDE_EN
typedef struct
{
    uint8_t                                     status_id;  
    IUG_UNIT_READ_STATUS_CARRIER_t              available;  

} struct_IUG_UNIT_READ_STATUS_AVAILABLE_SELECTION_t;
#endif


extern const IUG_READ_STATUS_LIST_DATA_CFG_t    IUG_READ_STATUS_LIST_DATA_CFG[];
extern const IUG_UNIT_READ_STATUS_CFG_t         IUG_UNIT_READ_STATUS_CFG[];
extern const uint8_t                            ext_iug_entries_IUG_UNIT_READ_STATUS_CFG;


#ifdef IUG_UNIT_READ_STATUS_AVAILABLE_DECIDE_EN
extern const struct_IUG_UNIT_READ_STATUS_AVAILABLE_SELECTION_t  IUG_UNIT_READ_STATUS_AVAILABLE[];
extern const uint8_t ext_iug_entries_IUG_UNIT_READ_STATUS_AVAILABLE;
#endif


#define IUG_UNIT_READ_STATUS_NONE                           0x0000u
#define IUG_UNIT_READ_STATUS_LIST_HEATER_HIGH               0x0001u   
#define IUG_UNIT_READ_STATUS_LIST_HEATER_MEDIUM             0x0002u   
#define IUG_UNIT_READ_STATUS_LIST_HEATER_LOW                0x0004u   
#define IUG_UNIT_READ_STATUS_0008                           0x0008u

#define IUG_UNIT_READ_STATUS_WBUS_TEMP_SENSOR_INT           0x0010u   
#define IUG_UNIT_READ_STATUS_WBUS_TEMP_SENSOR_EXT           0x0020u   
#define IUG_UNIT_READ_STATUS_0040                           0x0040u
#define IUG_UNIT_READ_STATUS_IMPLEMENTED_LIST               0x0080u   

#define IUG_UNIT_READ_STATUS_FUNCSTATUS                     0x0100u   
#define IUG_UNIT_READ_STATUS_MEASURES                       0x0200u   
#define IUG_UNIT_READ_STATUS_0400                           0x0400u
#define IUG_UNIT_READ_STATUS_0800                           0x0800u
#define IUG_UNIT_READ_STATUS_WBUS_TEMP_SENSOR_T_LIN_R_INT   0x1000u   
#define IUG_UNIT_READ_STATUS_WBUS_TEMP_SENSOR_T_LIN_R_EXT   0x2000u   
#define IUG_UNIT_READ_STATUS_4000                           0x4000u
#define IUG_UNIT_READ_STATUS_8000                           0x8000u


#define IUG_UNIT_READ_STATUS_HEATER_STATE_NUM       ((IUG_UNIT_READ_STATUS_LIST_HEATER_HIGH)|\
                                                     (IUG_UNIT_READ_STATUS_FUNCSTATUS)      )


#define IUG_UNIT_READ_STATUS_HG_RELATED             ((IUG_UNIT_READ_STATUS_WBUS_TEMP_SENSOR_EXT)|\
                                                     (IUG_UNIT_READ_STATUS_LIST_HEATER_HIGH)    |\
                                                     (IUG_UNIT_READ_STATUS_LIST_HEATER_MEDIUM)  |\
                                                     (IUG_UNIT_READ_STATUS_LIST_HEATER_LOW)     |\
                                                     (IUG_UNIT_READ_STATUS_FUNCSTATUS)          |\
                                                     (IUG_UNIT_READ_STATUS_MEASURES)            |\
                                                     (IUG_UNIT_READ_STATUS_IMPLEMENTED_LIST)     \
                                                    )



#define IUG_UNIT_READ_STATUS_SET_ENABLED( car, st )         IUG_UNIT_RD_WR_INTERFACE_SET_ENABLED((car),(st))
#define IUG_UNIT_READ_STATUS_IS_ENABLED( car, st )          IUG_UNIT_RD_WR_INTERFACE_IS_ENABLED((car),(st))
#define IUG_UNIT_READ_STATUS_CLR_ENABLED( car, st )         IUG_UNIT_RD_WR_INTERFACE_CLR_ENABLED((car),(st))

#define IUG_UNIT_READ_STATUS_SET_REQUEST( car, st )         IUG_UNIT_RD_WR_INTERFACE_SET_REQUEST((car),(st))
#define IUG_UNIT_READ_STATUS_IS_REQUEST( car, st )          IUG_UNIT_RD_WR_INTERFACE_IS_REQUEST((car),(st))

#define IUG_UNIT_READ_STATUS_IS_CONFIRM( car, st )          IUG_UNIT_RD_WR_INTERFACE_IS_CONFIRM((car),(st))

#define IUG_UNIT_READ_STATUS_SET_RESPONSE( car, st )        IUG_UNIT_RD_WR_INTERFACE_SET_RESPONSE((car),(st))
#define IUG_UNIT_READ_STATUS_CFM_RESPONSE( car, st )        IUG_UNIT_RD_WR_INTERFACE_CFM_RESPONSE((car),(st))
#define IUG_UNIT_READ_STATUS_IS_RESPONSE( car, st )         IUG_UNIT_RD_WR_INTERFACE_IS_RESPONSE((car),(st))

#define IUG_UNIT_READ_STATUS_SET_ERROR( car, st )           IUG_UNIT_RD_WR_INTERFACE_SET_ERROR((car),(st))
#define IUG_UNIT_READ_STATUS_SET_ERROR_CODE( car, err )     IUG_UNIT_RD_WR_INTERFACE_SET_ERROR_CODE((car),(err))

#define IUG_UNIT_READ_STATUS_IS_ERROR( car, st )            IUG_UNIT_RD_WR_INTERFACE_IS_ERROR((car),(st))

#define IUG_UNIT_READ_STATUS_SET_RESPONSE_ERROR( car, st )  IUG_UNIT_RD_WR_INTERFACE_SET_RESPONSE_ERROR((car),(st))


#ifdef IUG_UNIT_READ_STATUS_AVAILABLE_DECIDE_EN
#define IUG_UNIT_READ_STATUS_AVAILABLE_CFG \
{ \
    \
    \
    { (0x28u)   ,(IUG_UNIT_READ_STATUS_STATE_NUM_1BYTE)    },\
    { (0x29u)   ,(IUG_UNIT_READ_STATUS_STATE_NUM_2BYTE)    },\
    { (0x2Au)   ,(IUG_UNIT_READ_STATUS_STATE_STFL)         },\
    { (0x2Bu)   ,(IUG_UNIT_READ_STATUS_STATE_UEHFL)        },\
    { (0x2Cu)   ,(IUG_UNIT_READ_STATUS_STATE_HGVP)         },\
    { (0x0Au)   ,(IUG_UNIT_READ_STATUS_OUTPORT_HEATER)     },\
    { (0x0Cu)   ,(IUG_UNIT_READ_STATUS_COOL_MEDIUM_TEMP_1) },\
}
#endif


void        IUG_unit_read_status_adjust_period( IUG_UNIT_READ_STATUS_t    * const p_rd_st,
                                                IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl );
void        IUG_unit_read_status_cyclic( struct_IUG_UNIT_READ_STATUS_t * const p_rd_st, 
                                         const enum_WBUS_MEMBER_t wbus_mbr,
                                         const IUG_HEATER_UNIT_ENUM_t idx_ghi_heater );
IUG_UNIT_READ_STATUS_CARRIER_t  IUG_unit_read_status_available_decide( uint8_t * const p_st_id_list );
uint8_t     IUG_unit_read_status_calc_idx( const IUG_UNIT_READ_STATUS_CARRIER_t status_id );
uint16_t    IUG_unit_read_status_get_cycle_max( const IUG_UNIT_READ_STATUS_CARRIER_t status_id );
uint8_t     IUG_unit_read_status_request_process( IUG_COMM_RX_PARAM_t * const p_rx_par );
uint8_t     IUG_unit_read_status_response_process( IUG_QUEUE_ENTRY_DATA_t     * const que_dat, 
                                                   WBUS_MSG_DESCRIPTION_CTL_t * const wbus_msg_ctl,
                                                   IUG_UNIT_CONTROL_HEADER_t  * const p_unt_ctl_S,
                                                   const uint8_t  unt_ctl_S_is_ghi_htr );
boolean     IUG_unit_read_status_single_response_dispatch( IUG_QUEUE_ENTRY_DATA_t     * const que_dat, 
                                                           WBUS_MSG_DESCRIPTION_CTL_t * const wbus_msg_ctl,
                                                           const IUG_UNIT_READ_STATUS_CFG_t * const p_sta_cfg,
                                                           IUG_UNIT_CONTROL_HEADER_t  * const p_unt_ctl_S );
uint8_t     IUG_unit_read_status_list_store_data( const uint8_t idx_st_id, 
                                                  void * const p_data,
                                                  IUG_UNIT_CONTROL_HEADER_t * const p_unt_ctl_S );
boolean     IUG_unit_read_status_response_func_status( IUG_QUEUE_ENTRY_DATA_t     * const que_dat, 
                                                       WBUS_MSG_DESCRIPTION_CTL_t * const wbus_msg_ctl,
                                                       const IUG_UNIT_READ_STATUS_CFG_t * const p_sta_cfg,
                                                       IUG_UNIT_CONTROL_HEADER_t  * const p_unt_ctl_S );
boolean     IUG_unit_read_status_response_measures( IUG_QUEUE_ENTRY_DATA_t     * const que_dat, 
                                                    WBUS_MSG_DESCRIPTION_CTL_t * const wbus_msg_ctl,
                                                    const IUG_UNIT_READ_STATUS_CFG_t * const p_sta_cfg,
                                                    IUG_UNIT_CONTROL_HEADER_t  * const p_unt_ctl_S );
boolean     IUG_unit_read_status_response_temp_sensor( IUG_QUEUE_ENTRY_DATA_t     * const que_dat, 
                                                       WBUS_MSG_DESCRIPTION_CTL_t * const wbus_msg_ctl,
                                                       const IUG_UNIT_READ_STATUS_CFG_t * const p_sta_cfg );
IUG_UNIT_READ_STATUS_t * IUG_utility_read_status_set_ctl_pointer_from_unit_ctl_pointer( const IUG_UNIT_CONTROL_HEADER_t * const p_unt_ctl_S );


#endif 


