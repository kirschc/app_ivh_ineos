#ifndef __IUG_USER_API_UNITS_H_
#define __IUG_USER_API_UNITS_H_






IUG_UNIT_API_COMMAND_t USRAPI_heater_get_cmd_confirmation( const IUG_UNIT_API_UNIT_ID_t unt_api_id,
                                                           const IUG_UNIT_API_COMMAND_t unt_api_cmd );


IUG_UNIT_API_RETVAL_t API_units_get_heater_property( const IUG_UNIT_API_UNIT_ID_t unit_id_api, IUG_UNIT_API_HEATER_PROPERTY_t * const heater_property_api );


IUG_UNIT_API_RETVAL_t API_units_get_unit_state( const IUG_UNIT_API_UNIT_ID_t unit_id_api, IUG_UNIT_API_STATE_t *const unit_state_api );

IUG_UNIT_API_RETVAL_t API_units_set_unit_cmd( const IUG_UNIT_API_UNIT_ID_t unit_id_api, const structure_IUG_UNIT_API_UNIT_CTL_CMD_t unit_cmd );

#endif 


