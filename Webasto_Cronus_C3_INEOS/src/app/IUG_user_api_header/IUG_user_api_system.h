#ifndef __IUG_USER_API_SYSTEM_H_
#define __IUG_USER_API_SYSTEM_H_



typedef enum
{
    API_SYSTEM_TRIGGER_CAN_ERR_NONE      = 0u, 
    API_SYSTEM_TRIGGER_CAN_ERR_INVAL_BUS = 1u, 
    API_SYSTEM_TRIGGER_CAN_ERR_NOT_ACT   = 2u  
} enum_API_SYSTEM_TRIGGER_CAN_ERROR_STATES;

typedef enum
{
    API_SHARED_CONST_CAN_BUS_1 = 1u, 
    API_SHARED_CONST_CAN_BUS_2 = 2u, 
    API_SHARED_CONST_CAN_BUS_3 = 3u  
} enum_API_SYSTEM_CAN_BUSES;


typedef enum
{
    API_TSKMON_TASK_CUSTOM_1MS      = 0u,   
    API_TSKMON_ENUM_TASK_MAX                

} enum_API_TASK_MONITORING_TASK_ID_t;


typedef struct
{
    uint16_t    duty_min_us;            
    uint16_t    duty_max_us;            
    uint16_t    duty_avg_us;            
    uint8_t     duty_avg_percent;       

} struct_API_TASK_MONITORING_METRICS_t;



void API_system_wu_enable_X1_3_LIN1_PWM_IN( void );

void API_system_enable_sleep_without_ecu_state( void );

void API_system_lock_sleep_mode( void );


boolean API_system_set_time( const uint8_t second, const uint8_t minute, const uint8_t hour,
                             const uint8_t day,    const uint8_t month,  const uint8_t year );


boolean API_task_monitoring_get_metrics( const enum_API_TASK_MONITORING_TASK_ID_t api_task_id, 
                                         struct_API_TASK_MONITORING_METRICS_t * const api_tsk_mon_metrics,
                                         const uint8_t api_duty_clear );


enum_API_SYSTEM_TRIGGER_CAN_ERROR_STATES API_system_trigger_car_state_source_CAN( const enum_API_SYSTEM_CAN_BUSES can_bus );


#endif 


