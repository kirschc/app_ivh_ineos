#ifndef IUG_USER_API_OBD_H
#define IUG_USER_API_OBD_H



typedef enum
{
    API_OBD_ERR_NONE    = 0u, 
    API_OBD_ERR_UNAVAIL = 1u  
} enum_API_OBD_ERROR_STATES;


enum_API_OBD_ERROR_STATES API_obd_ctrl_request( const boolean ctrl_flg );


#endif 


