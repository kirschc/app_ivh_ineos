#ifndef __IUG_USER_API_USER_DEVICE_H_
#define __IUG_USER_API_USER_DEVICE_H_






API_USER_DEVICE_CMD_t API_user_device_get_cmd( const API_USER_DEVICE_ID_t user_device_id );


API_USER_DEVICE_CMD_t API_user_device_get_cmd_param( const API_USER_DEVICE_ID_t         user_device_id,
                                                    API_USER_DEVICE_CMD_DATA_t * const user_device_cmd_data );


API_USER_DEVICE_CMD_CFM_t API_user_device_get_cmd_confirmation( const API_USER_DEVICE_ID_t     user_device_id,
                                                                const API_USER_DEVICE_CMD_t    user_device_cmd,
                                                                const API_USER_DEVIC_CMD_EVT_t user_device_cmd_event,
                                                                API_USER_DEVICE_CMD_DATA_t     * const user_device_cmd_data );


#ifdef IUG_USER_API_USER_DEVICE_ENHANCED_DEMO_EN
void USRAPI_user_device_cyclic_20ms_demo( void );
#endif


#endif 


