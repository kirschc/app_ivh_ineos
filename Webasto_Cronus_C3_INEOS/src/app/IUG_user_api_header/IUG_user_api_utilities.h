#ifndef __IUG_USER_API_UTILITIES_H_
#define __IUG_USER_API_UTILITIES_H_



typedef enum
{
    API_UTILITIES_CAN_ERR_NONE       = 0u, 
    API_UTILITIES_CAN_ERR_INVAL_DP   = 1u, 
    API_UTILITIES_CAN_ERR_INVAL_BUS  = 2u, 
    API_UTILITIES_CAN_ERR_DRIVER     = 3u, 
    API_UTILITIES_CAN_ERR_INVAL_PARA = 4u  
} enum_API_UTILITIES_CAN_ERROR_STATES;

typedef enum
{
    API_UTILITIES_TIMER_ERR_NONE       = 0u, 
    API_UTILITIES_TIMER_ERR_INVAL_PREC = 1u, 
    API_UTILITIES_TIMER_ERR_DRIVER     = 2u,  
    API_UTILITIES_TIMER_ERR_INVAL_PARA = 3u  
} enum_API_UTILITIES_TIMER_ERROR_STATES;

typedef enum
{
    API_UTILITIES_TIMER_PRECISION_1MS   = 0u, 
    API_UTILITIES_TIMER_PRECISION_10MS  = 1u, 
    API_UTILITIES_TIMER_PRECISION_100MS = 2u, 
    API_UTILITIES_TIMER_PRECISION_1S    = 3u  
} enum_API_UTILITIES_TIMER_PRECISIONS;

typedef struct
{
    uint8_t  extended_id; 
    uint32_t can_id;      
    uint8_t  can_dlc;     
    uint8_t  data[8u];    
} struct_api_utilities_can_frame;


enum_API_UTILITIES_CAN_ERROR_STATES API_utilities_can_send_message( const enum_API_SYSTEM_CAN_BUSES can_bus, const uint32_t can_id, const uint8_t extended_id, const uint8_t can_dlc, \
                                                                    const uint8_t data_byte_0, const uint8_t data_byte_1, const uint8_t data_byte_2, const uint8_t data_byte_3, \
                                                                    const uint8_t data_byte_4, const uint8_t data_byte_5, const uint8_t data_byte_6, const uint8_t data_byte_7 );

enum_API_UTILITIES_CAN_ERROR_STATES API_utilities_can_db_get_value( const uint16_t can_datapoint_id, uint32_t *const can_datapoint_value );

enum_API_UTILITIES_CAN_ERROR_STATES API_utilities_can_db_set_value( const uint16_t can_datapoint_id, const uint32_t can_datapoint_value );

enum_API_UTILITIES_CAN_ERROR_STATES API_utilities_can_db_test_dp_value( const uint16_t can_datapoint_id, uint8_t *const can_datapoint_result );

enum_API_UTILITIES_CAN_ERROR_STATES API_utilities_can_db_set_transmit_flag( const uint16_t can_datapoint_id );

enum_API_UTILITIES_CAN_ERROR_STATES API_utilities_can_db_stop_transmission( const enum_API_SYSTEM_CAN_BUSES can_bus, const boolean status );

enum_API_UTILITIES_CAN_ERROR_STATES API_utilities_can_db_stop_gateway_for_known_ids( const enum_API_SYSTEM_CAN_BUSES can_bus, const boolean status );

enum_API_UTILITIES_CAN_ERROR_STATES API_utilities_can_db_stop_gateway_for_unknown_ids( const enum_API_SYSTEM_CAN_BUSES can_bus, const boolean status );

enum_API_UTILITIES_TIMER_ERROR_STATES API_utilities_timer_set_timestamp( uint32_t *const timestamp, const enum_API_UTILITIES_TIMER_PRECISIONS precision );

enum_API_UTILITIES_TIMER_ERROR_STATES API_utilities_get_time_elapsed(int32_t* time_elapsed, uint32_t timestamp, const enum_API_UTILITIES_TIMER_PRECISIONS precision );

enum_API_UTILITIES_TIMER_ERROR_STATES API_utilities_timer_check_timestamp( uint8_t *const elapsed_flg, const uint32_t timestamp, \
                                                                           const uint32_t compare_time, const enum_API_UTILITIES_TIMER_PRECISIONS precision );


#endif 


