#ifndef __IUG_USER_API_CMD_BUTTON_H_
#define __IUG_USER_API_CMD_BUTTON_H_





API_CMDBTN_LED_PATTERN_t IUG_cmd_button_usrapi_del_led_pattern_cfg( const API_CMDBTN_LED_PATTERN_t pattern_id );


API_CMDBTN_LED_PATTERN_t IUG_cmd_button_usrapi_set_led_pattern_cfg( const API_CMDBTN_LED_PATTERN_t  pattern_id,
                                                                    const uint16_t                  pattern_period_ms,
                                                                    const API_CMDBTN_PATTERN_MODE_t pattern_cfg_mode,
                                                                    const uint8_t                   pattern_cfg_points,
                                                                    int16_t                       * pattern_cfg_x_progress,
                                                                    int16_t                       * pattern_cfg_y_brightness
                                                               );


#ifdef IUG_USER_API_CMD_BUTTON_DEMO_EN
void USRAPI_cmd_button_cyclic_20ms_demo( void );
#endif


#endif 


