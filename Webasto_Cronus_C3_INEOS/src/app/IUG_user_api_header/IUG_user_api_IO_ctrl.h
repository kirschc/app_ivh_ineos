#ifndef __IUG_USER_API_IO_CTRL_H_
#define __IUG_USER_API_IO_CTRL_H_



typedef enum
{
    API_IO_CTRL_STATE_STATIC_OFF     = 0u, 
    API_IO_CTRL_STATE_STATIC_ON      = 1u, 
    API_IO_CTRL_STATE_PMIC_PWM_CH1   = 2u, 
    API_IO_CTRL_STATE_PMIC_PWM_CH2   = 3u, 
    API_IO_CTRL_STATE_PMIC_PWM_CH3   = 4u, 
    API_IO_CTRL_STATE_PMIC_PWM_CH4   = 5u, 
    API_IO_CTRL_STATE_PMIC_TIMER_CH1 = 6u, 
    API_IO_CTRL_STATE_PMIC_TIMER_CH2 = 7u  
} enum_API_IO_CTRL_STATES;

typedef enum
{
    API_IO_CTRL_ERR_NONE              = 0u, 
    API_IO_CTRL_ERR_DRIVER            = 1u, 
    API_IO_CTRL_ERR_INVAL_STATE       = 2u, 
    API_IO_CTRL_ERR_PIN_UNAVAIL       = 3u, 
    API_IO_CTRL_ERR_INVAL_PARA        = 4u, 
    API_IO_CTRL_ERR_IN_USE            = 5u, 
    API_IO_CTRL_ERR_DRIVER_AND_IN_USE = 6u  
} enum_API_IO_CTRL_ERROR_STATES;

typedef enum
{
    API_IO_CTRL_DO_CAN1_TERMINATION = DO_CAN1_TERMINATION, 
    API_IO_CTRL_DO_CAN2_TERMINATION = DO_CAN2_TERMINATION, 
    API_IO_CTRL_DO_CAN3_TERMINATION = DO_CAN3_TERMINATION, 
    API_IO_CTRL_DO_IO1_PU           = DO_IO1_PU          , 
    API_IO_CTRL_DO_IO2_PU           = DO_IO2_PU          , 
    API_IO_CTRL_DO_IO3_PU           = DO_IO3_PU          , 
    API_IO_CTRL_DO_IO4_PU           = DO_IO4_PU          , 
    API_IO_CTRL_DO_LIN2_TER         = DO_LIN2_TER        , 
    API_IO_CTRL_DO_LIN3_TER         = DO_LIN3_TER        , 
    API_IO_CTRL_DO_PIGGY_ENABLE     = DO_PIGGY_ENABLE    , 
    API_IO_CTRL_DO_RELAIS_OUT       = DO_REL_OUT         , 
    API_IO_CTRL_DO_RELAIS_CAN       = DO_RELAIS_CAN      , 
    API_IO_CTRL_DO_RELAIS_LIN       = DO_RELAIS_LIN      , 
    API_IO_CTRL_DO_WBUS3_TER        = DO_WBUS3_TER         
} enum_API_IO_CTRL_DIG_OUTS;

typedef enum
{
    API_IO_CTRL_DI_EXAMPLE = 0u
} enum_API_IO_CTRL_DIG_INS;

typedef enum
{
    API_IO_CTRL_AI_12V_OUT         = AI_12V_OUT        , 
    API_IO_CTRL_AI_ANA             = AI_ANA            , 
    API_IO_CTRL_AI_KL15            = AI_KL15           , 
    API_IO_CTRL_AI_KL30            = AI_KL30           , 
    API_IO_CTRL_AI_PORT_1          = AI_PORT_1         , 
    API_IO_CTRL_AI_PORT_2          = AI_PORT_2         , 
    API_IO_CTRL_AI_PORT_3          = AI_PORT_3         , 
    API_IO_CTRL_AI_PORT_4          = AI_PORT_4         , 
    API_IO_CTRL_AI_PUSH_BUTTON     = AI_PUSH_BUTTON    , 
    API_IO_CTRL_AI_PWM_IN          = AI_PWM_IN         , 
    API_IO_CTRL_AI_PWM_PEAK_DETECT = AI_PWM_PEAK_DETECT, 
    API_IO_CTRL_AI_LIN3            = AI_LIN3           , 
    API_IO_CTRL_AI_INT_PWM         = AI_INT_PWM        , 
    API_IO_CTRL_AI_INT_IO3         = AI_INT_IO3        , 
    API_IO_CTRL_AI_PIGGY           = AI_PIGGY            
} enum_API_IO_CTRL_ANA_INS;

typedef enum
{
    API_IO_CTRL_ERR_CTRL_BTN_NONE       = 0u, 
    API_IO_CTRL_ERR_CTRL_BTN_NOT_AVAIL  = 1u, 
    API_IO_CTRL_ERR_CTRL_BTN_INVAL_PARA = 2u, 
} enum_API_IO_CTRL_ERROR_STATES_CTRL_BTN;

typedef enum
{
    API_IO_CTRL_OUT_PARA_PMIC_PWM_FREQ_128HZ = 0u, 
    API_IO_CTRL_OUT_PARA_PMIC_PWM_FREQ_256HZ = 1u  
} enum_API_IO_CTRL_OUT_PARA_PMIC_PWM_FREQ;

typedef enum
{
    API_IO_CTRL_OUT_PARA_PMIC_TIMER_CH1_T_ON_10MS  = 0u, 
    API_IO_CTRL_OUT_PARA_PMIC_TIMER_CH1_T_ON_20MS  = 1u, 
    API_IO_CTRL_OUT_PARA_PMIC_TIMER_CH2_T_ON_100US = 2u, 
    API_IO_CTRL_OUT_PARA_PMIC_TIMER_CH2_T_ON_1MS   = 3u  
} enum_API_IO_CTRL_OUT_PARA_PMIC_TIMER_T_ON;

typedef enum
{
    API_IO_CTRL_OUT_PARA_PMIC_TIMER_CH1_T_PER_1S    = 0u, 
    API_IO_CTRL_OUT_PARA_PMIC_TIMER_CH1_T_PER_2S    = 1u, 
    API_IO_CTRL_OUT_PARA_PMIC_TIMER_CH1_T_PER_3S    = 2u, 
    API_IO_CTRL_OUT_PARA_PMIC_TIMER_CH1_T_PER_4S    = 3u, 
    API_IO_CTRL_OUT_PARA_PMIC_TIMER_CH2_T_PER_10MS  = 4u, 
    API_IO_CTRL_OUT_PARA_PMIC_TIMER_CH2_T_PER_20MS  = 5u, 
    API_IO_CTRL_OUT_PARA_PMIC_TIMER_CH2_T_PER_50MS  = 6u, 
    API_IO_CTRL_OUT_PARA_PMIC_TIMER_CH2_T_PER_200MS = 7u  
} enum_API_IO_CTRL_OUT_PARA_PMIC_TIMER_T_PERIOD;

typedef struct
{
    uint8_t rd_button_pushed_state; 
} struct_API_IO_CTRL_CTRL_PUSH_BTN;

typedef struct
{
    enum_API_IO_CTRL_OUT_PARA_PMIC_PWM_FREQ freq;       
    uint8_t                                 duty_cycle; 
} struct_API_IO_CTRL_OUT_PARA_PMIC_PWM;

typedef struct
{
    enum_API_IO_CTRL_OUT_PARA_PMIC_TIMER_T_ON     on_time;     
    enum_API_IO_CTRL_OUT_PARA_PMIC_TIMER_T_PERIOD period_time; 
} struct_API_IO_CTRL_OUT_PARA_PMIC_TIMER;


enum_API_IO_CTRL_ERROR_STATES API_IO_ctrl_get_ana_input( const enum_API_IO_CTRL_ANA_INS ana_in, uint16_t *const ana_in_value );

enum_API_IO_CTRL_ERROR_STATES API_IO_ctrl_get_dig_input( const enum_API_IO_CTRL_DIG_INS dig_in, enum_API_IO_CTRL_STATES *const act_state );

enum_API_IO_CTRL_ERROR_STATES API_IO_ctrl_set_dig_output( const enum_API_IO_CTRL_DIG_OUTS dig_out, const enum_API_IO_CTRL_STATES act_state );

enum_API_IO_CTRL_ERROR_STATES API_IO_ctrl_get_dig_output( const enum_API_IO_CTRL_DIG_OUTS dig_out, enum_API_IO_CTRL_STATES *const ptr_act_state );

enum_API_IO_CTRL_ERROR_STATES API_IO_ctrl_set_para_for_output_state_pmic_pwm( const enum_API_IO_CTRL_STATES pwm_ch, const struct_API_IO_CTRL_OUT_PARA_PMIC_PWM pwm_para );

enum_API_IO_CTRL_ERROR_STATES API_IO_ctrl_set_para_for_output_state_pmic_timer( const enum_API_IO_CTRL_STATES timer_ch, const struct_API_IO_CTRL_OUT_PARA_PMIC_TIMER timer_para );

enum_API_IO_CTRL_ERROR_STATES API_IO_ctrl_set_X1_6_LIN4_WBUS_BE_ter( const enum_API_IO_CTRL_STATES output_state );

enum_API_IO_CTRL_ERROR_STATES API_IO_ctrl_set_X1_11_IO4_TEMP_IN_output( const enum_API_IO_CTRL_STATES output_state );

enum_API_IO_CTRL_ERROR_STATES API_IO_ctrl_set_X1_12_OUT_1_output( const enum_API_IO_CTRL_STATES output_state );

enum_API_IO_CTRL_ERROR_STATES API_IO_ctrl_set_X1_14_REL_1_output( const enum_API_IO_CTRL_STATES output_state );

enum_API_IO_CTRL_ERROR_STATES API_IO_ctrl_set_X1_15_IO1_LED_output( const enum_API_IO_CTRL_STATES output_state );

enum_API_IO_CTRL_ERROR_STATES API_IO_ctrl_set_X2_5_IO2_output( const enum_API_IO_CTRL_STATES output_state );

enum_API_IO_CTRL_ERROR_STATES API_IO_ctrl_set_X2_6_IO3_PWM_OUT_output( const enum_API_IO_CTRL_STATES output_state );

enum_API_IO_CTRL_ERROR_STATES API_IO_ctrl_set_X2_12_REL_2_output( const enum_API_IO_CTRL_STATES output_state );

enum_API_IO_CTRL_ERROR_STATES_CTRL_BTN API_IO_ctrl_push_btn_ctrl(struct_API_IO_CTRL_CTRL_PUSH_BTN *const ptr_ctrl_push_btn);


#endif 


