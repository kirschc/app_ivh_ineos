#ifndef _API_UNITS_DATA_RD_WR_SUPPORT_STRUCT_H_
#define _API_UNITS_DATA_RD_WR_SUPPORT_STRUCT_H_






typedef struct
{
    API_UNITS_DATA_RDWRSUP_ABSTRACT_DATA_ID_t  data_id;
    API_UNITS_DATA_RDWRSUP_DATA_TYPE_t         size_type;

} API_UNITS_DATA_RDWRSUP_ABSTRACT_DATA_CFG_ENTRY_t;


typedef struct
{
    API_UNITS_DATA_RDWRSUP_IDENT_ID_t      data_id;
    API_UNITS_DATA_RDWRSUP_DATA_TYPE_t     size_type;

} API_UNITS_DATA_RDWRSUP_ID_DATA_CFG_ENTRY_t;


typedef struct
{
    API_UNITS_DATA_RDWRSUP_STATUS_ID_t     data_id;
    API_UNITS_DATA_RDWRSUP_DATA_TYPE_t     size_type;

} API_UNITS_DATA_RDWRSUP_ST_DATA_CFG_ENTRY_t;


typedef struct
{
    API_UNITS_DATA_RDWRSUP_TOPIC_ID_t          topic_id;

    union
    {
        API_UNITS_DATA_RDWRSUP_IDENT_ID_t          ident_id;
        API_UNITS_DATA_RDWRSUP_STATUS_ID_t         status_id;
        API_UNITS_DATA_RDWRSUP_ABSTRACT_DATA_ID_t  id;
    } data_id;

} API_UNITS_DATA_RDWRSUP_GET_DESCRIPTION_t;







#endif 


