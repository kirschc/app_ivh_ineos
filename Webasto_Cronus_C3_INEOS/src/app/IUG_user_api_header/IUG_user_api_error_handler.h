#ifndef IUG_USER_API_ERROR_HANDLER_H
#define IUG_USER_API_ERROR_HANDLER_H



typedef enum
{
    API_ERROR_HANDLER_DTC_ERR_NONE       = 0u, 
    API_ERROR_HANDLER_DTC_ERR_NO_USR_DTC = 1u, 
    API_ERROR_HANDLER_DTC_ERR_INVAL_DTC  = 2u, 
    API_ERROR_HANDLER_DTC_ERR_INVAL_PARA = 3u  
} enum_API_ERROR_HANDLER_DTC_ERROR_STATES;


enum_API_ERROR_HANDLER_DTC_ERROR_STATES API_error_handler_set_dtc( const enum_DTC_TABLE_IDX dtc_index );

enum_API_ERROR_HANDLER_DTC_ERROR_STATES API_error_handler_reset_dtc( const enum_DTC_TABLE_IDX dtc_index );

enum_API_ERROR_HANDLER_DTC_ERROR_STATES API_error_handler_get_dtc( const enum_DTC_TABLE_IDX dtc_index,
                                                                   structure_API_ERROR_HANDLER_DTC_ERROR_INFO_t * const dtc_info );


#endif 


