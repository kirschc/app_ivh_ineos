#ifndef __IUG_USER_API_CALLBACK_H_
#define __IUG_USER_API_CALLBACK_H_


#define IUG_API_CB_CFG_CAR_DET_KL15_LVL_MV        5000u 
#define IUG_API_CB_CFG_CAR_DET_LIN1_LVL_LOW_MV    800u  
#define IUG_API_CB_CFG_CAR_DET_LIN1_LVL_HIGH_MV   1000u 



uint8_t API_callback_03_FahrzeugZustandserkennung_X1_9_KL15( void );

uint8_t API_callback_03_FahrzeugZustandserkennung_X1_3_LIN1_PWM_IN( void );

IUG_UNIT_API_COMMAND_t API_callback_27_AutomatischesHeizenLueften_Decide( const IUG_UNIT_API_PHYS_TEMP_1_1DEG_T cfg_level_auto_heat_vent, \
                                                                          const IUG_UNIT_API_PHYS_TEMP_1_1DEG_T room_temp );

void API_callback_31_MV_Ansteuerung( void );

void API_callback_32_UP_Ansteuerung( void );


#endif 


