#ifndef __IUG_USER_API_USER_DEVICE_CONSTANT_H_
#define __IUG_USER_API_USER_DEVICE_CONSTANT_H_




#define IUG_USER_DEVICE_MAX      3u    


typedef enum
{
    API_USRDVC_ID_1        = 0u, 
    API_USRDVC_ID_2        = 1u, 
    API_USRDVC_ID_3        = 2u, 
    API_USRDVC_ID_ENUM_MAX = 3u  
} enum_API_USER_DEVICE_ID_t;
typedef enum_API_USER_DEVICE_ID_t                   API_USER_DEVICE_ID_t; 


typedef enum
{
    API_USRDVC_CMD_NONE          = 0u, 
    API_USRDVC_CMD_OFF           = 1u, 
    API_USRDVC_CMD_HEATING_REC   = 2u, 
    API_USRDVC_CMD_HEATING_DOM   = 3u, 
    API_USRDVC_CMD_HEATING_AUX   = 4u, 
    API_USRDVC_CMD_HEATING_BOOST = 5u, 
    API_USRDVC_CMD_HEATING_ECO   = 6u, 
    API_USRDVC_CMD_VENTILATING   = 7u, 
    API_USRDVC_CMD_ENUM_MAX      = 8u, 
} enum_API_USER_DEVIC_CMD_t;
typedef enum_API_USER_DEVIC_CMD_t                   API_USER_DEVICE_CMD_t; 


typedef enum
{
    API_USRDVC_CMDCFM_DEACTIVATE     = 0u, 
    API_USRDVC_CMDCFM_ACTIVATE_ALIVE = 1u, 
    API_USRDVC_CMDCFM_ENUM_MAX       = 2u, 
} enum_API_USER_DEVIC_CMD_CONFIRMATION_t;
typedef enum_API_USER_DEVIC_CMD_CONFIRMATION_t      API_USER_DEVICE_CMD_CFM_t; 


typedef enum
{
    API_USRDVC_CMDEVT_ACTIVATION   = 0u, 
    API_USRDVC_CMDEVT_DEACTIVATION = 1u, 
    API_USRDVC_CMDEVT_CYCLIC       = 2u, 
    API_USRDVC_CMDEVT_ENUM_MAX     = 3u, 
} enum_API_USER_DEVIC_CMD_EVENT_t;
typedef enum_API_USER_DEVIC_CMD_EVENT_t             API_USER_DEVIC_CMD_EVT_t; 


typedef struct
{
    API_USER_DEVICE_CMD_t   cmd;                
    uint8_t                 ACTIVATION_PERIOD;  
    uint8_t                 SETPOINT;           
}  structure_API_USER_DEVICE_COMMAND_DATA_t;
typedef structure_API_USER_DEVICE_COMMAND_DATA_t    API_USER_DEVICE_CMD_DATA_t; 




#endif 


