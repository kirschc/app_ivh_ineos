#ifndef __IUG_USER_API_UNITS_CONSTANT_H_
#define __IUG_USER_API_UNITS_CONSTANT_H_


#define IUG_UNIT_API_HEATER_UNIT_NUM_MAX         8u 

#define IUG_UNIT_API_HEATER_UNIT_HG1_IDX         0u 
#define IUG_UNIT_API_HEATER_UNIT_HG2_IDX         1u 
#define IUG_UNIT_API_HEATER_UNIT_HG3_IDX         2u 
#define IUG_UNIT_API_HEATER_UNIT_HG4_IDX         3u 
#define IUG_UNIT_API_HEATER_UNIT_HG5_IDX         4u 
#define IUG_UNIT_API_HEATER_UNIT_HG6_IDX         5u 
#define IUG_UNIT_API_HEATER_UNIT_HG7_IDX         6u 
#define IUG_UNIT_API_HEATER_UNIT_HG8_IDX         7u 


typedef uint8_t     IUG_UNIT_API_PHYS_TEMP_1_1DEG_T;    

typedef enum
{
    IUG_UNIT_API_UNIT_HEATER_HG1 = (IUG_UNIT_API_HEATER_UNIT_HG1_IDX), 
    IUG_UNIT_API_UNIT_HEATER_HG2 = (IUG_UNIT_API_HEATER_UNIT_HG2_IDX), 
    IUG_UNIT_API_UNIT_HEATER_HG3 = (IUG_UNIT_API_HEATER_UNIT_HG3_IDX), 
    IUG_UNIT_API_UNIT_HEATER_HG4 = (IUG_UNIT_API_HEATER_UNIT_HG4_IDX), 
    IUG_UNIT_API_UNIT_HEATER_HG5 = (IUG_UNIT_API_HEATER_UNIT_HG5_IDX), 
    IUG_UNIT_API_UNIT_HEATER_HG6 = (IUG_UNIT_API_HEATER_UNIT_HG6_IDX), 
    IUG_UNIT_API_UNIT_HEATER_HG7 = (IUG_UNIT_API_HEATER_UNIT_HG7_IDX), 
    IUG_UNIT_API_UNIT_HEATER_HG8 = (IUG_UNIT_API_HEATER_UNIT_HG8_IDX), 
    IUG_UNIT_API_UNIT_IUG        =  8u, 
    IUG_UNIT_API_UNIT_TLINR_INT  =  9u, 
    IUG_UNIT_API_UNIT_TLINR_EXT  = 10u, 
    IUG_UNIT_API_UNIT_ENUM_MAX          
} enum_IUG_UNIT_API_UNIT_ID_t;

typedef enum_IUG_UNIT_API_UNIT_ID_t     IUG_UNIT_API_UNIT_ID_t; 


typedef enum
{
    IUG_UNIT_API_HEATER_TYPE_AIR      = 0u, 
    IUG_UNIT_API_HEATER_TYPE_WAT      = 1u, 
    IUG_UNIT_API_HEATER_TYPE_ENUM_MAX = 2u 
} enum_IUG_UNIT_API_HEATER_TYPE_t;

typedef enum_IUG_UNIT_API_HEATER_TYPE_t     IUG_UNIT_API_HEATER_TYPE_t; 


typedef enum
{
    IUG_UNIT_API_CMD_NONE        = 0u, 
    IUG_UNIT_API_CMD_OFF         = 1u, 
    IUG_UNIT_API_CMD_HEATING     = 2u, 
    IUG_UNIT_API_CMD_BOOST       = 3u, 
    IUG_UNIT_API_CMD_ECO         = 4u, 
    IUG_UNIT_API_CMD_VENTILATION = 5u, 
    IUG_UNIT_API_CMD_ENUM_MAX    = 6u  
} enum_IUG_UNIT_API_COMMAND_t;

typedef enum_IUG_UNIT_API_COMMAND_t     IUG_UNIT_API_COMMAND_t;

typedef enum
{
    IUG_UNIT_API_STATE_WAIT_STATE    = 0u,
    IUG_UNIT_API_STATE_SWITCH_OFF    = 1u,
    IUG_UNIT_API_STATE_BOOST         = 2u,
    IUG_UNIT_API_STATE_ECO           = 3u,
    IUG_UNIT_API_STATE_VENTILATION   = 4u,
    IUG_UNIT_API_STATE_HEATING       = 5u,
    IUG_UNIT_API_STATE_AUX_HEATING   = 6u,
    IUG_UNIT_API_STATE_DRIVE_HEATING = 7u,
    IUG_UNIT_API_STATE_FAULT         = 8u,
    IUG_UNIT_API_STATE_UNKNOWN       = 9u,
    IUG_UNIT_API_STATE_ENUM_MAX      = 10u 
} enum_IUG_UNIT_API_STATE_t;

typedef enum_IUG_UNIT_API_STATE_t       IUG_UNIT_API_STATE_t;

typedef enum
{

    IUG_UNIT_API_RETVAL_OK                  =  0x00u, 
    IUG_UNIT_API_RETVAL_AVAILABLE           =  0x01u, 
    IUG_UNIT_API_RETVAL_DATA_OK             =  0x02u, 
    IUG_UNIT_API_RETVAL_DATA_AVAILABLE      =  0x03u, 

    IUG_UNIT_API_RETVAL_BUSY                =  0x10u, 
    IUG_UNIT_API_RETVAL_TIMEOUT             =  0x11u, 
    IUG_UNIT_API_RETVAL_DATA_NO_POINTER     =  0x12u, 
    IUG_UNIT_API_RETVAL_DATA_NOT_AVAILABLE  =  0x13u, 
    IUG_UNIT_API_RETVAL_DATA_INVALID        =  0x14u, 
    IUG_UNIT_API_RETVAL_DATA_NOT_RELEASED   =  0x15u, 

    IUG_UNIT_API_RETVAL_ERR                 = 0x20u, 
    IUG_UNIT_API_RETVAL_ERR_UNIT_ID         = 0x21u, 
    IUG_UNIT_API_RETVAL_ERR_COMMAND         = 0x22u, 
    IUG_UNIT_API_RETVAL_ERR_PARAMETER       = 0x23u, 
    IUG_UNIT_API_RETVAL_ERR_BUSY            = 0x24u, 
    IUG_UNIT_API_RETVAL_ERR_CMD_UNEXPECTED  = 0x25u, 

    IUG_UNIT_API_RETVAL_REQ_PENDING         = 0x40u, 
    IUG_UNIT_API_RETVAL_RSP_PENDING         = 0x41u, 
    IUG_UNIT_API_RETVAL_RSP_NEGATIVE        = 0x42u, 
    IUG_UNIT_API_RETVAL_RSP_TIMEOUT         = 0x43u, 

    IUG_UNIT_API_RETVAL_INFO_CMD_MULTIPLE   = 0x60u, 

    IUG_UNIT_API_RETVAL_ENUM_MAX            = 0x7Fu  

} enum_IUG_UNIT_API_RETVAL_t;

typedef enum_IUG_UNIT_API_RETVAL_t  IUG_UNIT_API_RETVAL_t;

typedef enum
{
    IUG_UNIT_API_MSKHTR__                = 0x0000u,    

    IUG_UNIT_API_MSKHTR_1                = 0x0001u,    
    IUG_UNIT_API_MSKHTR_2                = 0x0002u,    
    IUG_UNIT_API_MSKHTR_3                = 0x0004u,    
    IUG_UNIT_API_MSKHTR_4                = 0x0008u,    
    IUG_UNIT_API_MSKHTR_5                = 0x0010u,    
    IUG_UNIT_API_MSKHTR_6                = 0x0020u,    
    IUG_UNIT_API_MSKHTR_7                = 0x0040u,    
    IUG_UNIT_API_MSKHTR_8                = 0x0080u,    

    IUG_UNIT_API_MSKHTR_9                = 0x0100u,    
    IUG_UNIT_API_MSKHTR10                = 0x0200u,    
    IUG_UNIT_API_MSKHTR11                = 0x0400u,    
    IUG_UNIT_API_MSKHTR12                = 0x0800u,    
    IUG_UNIT_API_MSKHTR13                = 0x1000u,    
    IUG_UNIT_API_MSKHTR14                = 0x2000u,    
    IUG_UNIT_API_MSKHTR15                = 0x4000u,    
    IUG_UNIT_API_MSKHTR16                = 0x8000u,    
    IUG_UNIT_API_MSKHTR__ENUM_FORCE_TYPE = 0x7FFF      

} enum_IUG_UNIT_API_HTR_MSK_ID_t;


typedef enum
{
    IUG_UNIT_API_MSKUDV__                 = 0x00u,    
    IUG_UNIT_API_MSKUDV_1                 = 0x01u,    
    IUG_UNIT_API_MSKUDV_2                 = 0x02u,    
    IUG_UNIT_API_MSKUDV_3                 = 0x04u,    
    IUG_UNIT_API_MSKUDV_4                 = 0x08u,    
    IUG_UNIT_API_MSKUDV_5                 = 0x10u,    
    IUG_UNIT_API_MSKUDV_6                 = 0x20u,    
    IUG_UNIT_API_MSKUDV_7                 = 0x40u,    
    IUG_UNIT_API_MSKUDV_8                 = 0x80u,    
    IUG_UNIT_API_MSKUDV__ENUM_FORCE_TYPE  = 0x7F      

} enum_IUG_USER_API_USRDEV_MSK_ID_t;

typedef enum
{
    IUG_UNIT_API_HTRCAP_NONE             = 0x0000u,     
    IUG_UNIT_API_HTRCAP_HT_REC           = 0x0100u,     
    IUG_UNIT_API_HTRCAP_HT_DOM           = 0x0200u,     
    IUG_UNIT_API_HTRCAP_VENTIL           = 0x0400u,     
    IUG_UNIT_API_HTRCAP_HT_AUX           = 0x0800u,     
    IUG_UNIT_API_HTRCAP_HT_UPF           = 0x1000u,     
    IUG_UNIT_API_HTRCAP_HT_BOO           = 0x2000u,     
    IUG_UNIT_API_HTRCAP_HT_ECO           = 0x4000u,     
    IUG_UNIT_API_HTRCAP_HT__2A           = 0x8000u,     
    IUG_UNIT_API_HTRCAP_ENUM_FORCE_TYPE  = 0x7FFFFFFF   
} enum_IUG_UNIT_API_HEATER_CAPABILITY_t;

typedef enum_IUG_UNIT_API_HEATER_CAPABILITY_t  IUG_UNIT_API_HEATER_CAPABILITY_t;

typedef enum
{
    IUG_UNIT_API_UNTFLG_NONE                 = 0x0000u, 
    IUG_UNIT_API_UNTFLG_TYP_HTR              = 0x0001u, 
    IUG_UNIT_API_UNTFLG_TYP_CTL              = 0x0002u, 
    IUG_UNIT_API_UNTFLG_TYP_GENCTL           = 0x0004u, 
    IUG_UNIT_API_UNTFLG_TYP_TEMPSEN          = 0x0010u, 
    IUG_UNIT_API_UNTFLG_TYP_HTRGHI           = 0x0080u, 
    IUG_UNIT_API_UNTFLG_COOL_PUMP_AVAIL      = 0x0100u, 
    IUG_UNIT_API_UNTFLG_COOL_PUMP_ACT_AT_AH  = 0x0200u, 
    IUG_UNIT_API_UNTFLG_SERNUM_5109          = 0x0400u, 
    IUG_UNIT_API_UNTFLG_AIR_PRESS_CORR_AVAIL = 0x0800u, 
    IUG_UNIT_API_UNTFLG_SWAP_ID_STATUS_DATA  = 0x1000u, 
    IUG_UNIT_API_UNTFLG_ENUM_FORCE_TYPE      = 0x7FFF   
} enum_IUG_UNIT_API_UNIT_FLAG_t;

typedef enum_IUG_UNIT_API_UNIT_FLAG_t  IUG_UNIT_API_UNIT_FLAG_t;


typedef struct
{
    IUG_UNIT_API_COMMAND_t  cmd;                
    uint8_t                 ACTIVATION_PERIOD;  
    uint8_t                 SETPOINT;           
}  structure_IUG_UNIT_API_UNIT_CTL_CMD_t;

typedef structure_IUG_UNIT_API_UNIT_CTL_CMD_t   IUG_UNIT_API_UNIT_CTL_CMD_t;


typedef struct
{
    IUG_UNIT_API_UNIT_ID_t           unit_id;                
    uint8_t                          unit_available;         
    IUG_SITE_TARGET_ID_t             unit_target_id;         
    IUG_UNIT_API_HEATER_TYPE_t       unit_htr_typ;           
    IUG_UNIT_API_HEATER_CAPABILITY_t unit_htr_capablty;      
    IUG_UNIT_API_UNIT_FLAG_t         unit_flg_ctl;           

} structure_IUG_UNIT_API_HEATER_PROPERTY_t;

typedef structure_IUG_UNIT_API_HEATER_PROPERTY_t    IUG_UNIT_API_HEATER_PROPERTY_t;


typedef struct
{
    enum_DTC_TABLE_IDX request__dtc_index;   
    uint8_t            response_dtc_active;  
    uint16_t           response_dtc_symptom; 
}  structure_API_ERROR_HANDLER_DTC_ERROR_INFO_t;




#endif 


