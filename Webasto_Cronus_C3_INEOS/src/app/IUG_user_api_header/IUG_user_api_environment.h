#ifndef __IUG_USER_API_ENVIRONMENT_H_
#define __IUG_USER_API_ENVIRONMENT_H_



typedef enum
{
    API_ENV_TEMP_SENS_INTERNAL = 0u, 
    API_ENV_TEMP_SENS_EXTERNAL = 1u  
} enum_API_ENV_TEMP_SENS_LOCATION;

typedef enum
{
    API_ENV_TEMP_SENS_INT_USER   = 0u, 
    API_ENV_TEMP_SENS_INT_WBUS   = 1u, 
    API_ENV_TEMP_SENS_INT_BUTTON = 2u, 
    API_ENV_TEMP_SENS_INT_BMP    = 3u, 
    API_ENV_TEMP_SENS_EXT_USER   = 4u, 
    API_ENV_TEMP_SENS_EXT_WBUS   = 5u, 
} enum_API_ENV_TEMP_SENS;

typedef enum
{
    API_ENV_ERR_TEMP_SENS_NONE       = 0u, 
    API_ENV_ERR_TEMP_SENS_NOT_AVAL   = 1u, 
    API_ENV_ERR_TEMP_SENS_EVAL       = 2u, 
    API_ENV_ERR_TEMP_SENS_RANGE      = 3u, 
    API_ENV_ERR_TEMP_SENS_SHORT_VBAT = 4u, 
    API_ENV_ERR_TEMP_SENS_SHORT_GND  = 5u, 
    API_ENV_ERR_TEMP_SENS_OPEN_LOAD  = 6u, 
    API_ENV_ERR_TEMP_SENS_GEN_FAULT  = 7u, 
    API_ENV_ERR_TEMP_SENS_INVAL_PARA = 8u  
} enum_API_ENV_ERR_TEMP_SENS;

typedef enum
{
    API_ENV_ERR_OBD_NONE               = 0x00u, 
    API_ENV_ERR_OBD_PROT_NOT_SET       = 0x01u, 
    API_ENV_ERR_OBD_PROT_NOT_FOUND     = 0x02u, 
    API_ENV_ERR_OBD_PID_PENDING        = 0x04u, 
    API_ENV_ERR_OBD_PID_UNKNOWN        = 0x08u, 
    API_ENV_ERR_OBD_PID_TIMEOUT        = 0x10u, 
    API_ENV_ERR_OBD_INACTIVE_CAR_STATE = 0x20u, 
    API_ENV_ERR_OBD_INVAL_CFG          = 0x40u, 
    API_ENV_ERR_OBD_INVAL_PARA         = 0x80u  
} enum_API_ENV_ERR_OBD;


enum_API_ENV_ERR_OBD API_environment_obd_set_pid_req( const enum_PIDS_DESCRIPTION obd_pid );

enum_API_ENV_ERR_OBD API_environment_obd_get_pid_val( const enum_PIDS_DESCRIPTION obd_pid, int32_t *const obd_pid_value );

enum_API_ENV_ERR_TEMP_SENS API_environment_get_temperature_priority( const enum_API_ENV_TEMP_SENS_LOCATION temp_sens_location, int16_t *const temp_value );

enum_API_ENV_ERR_TEMP_SENS API_environment_get_temperature_explicit( const enum_API_ENV_TEMP_SENS temp_sens, int16_t *const temp_value );

enum_API_ENV_ERR_TEMP_SENS API_environment_set_temp_sens_user( const enum_API_ENV_TEMP_SENS temp_sens, const int16_t temp_value );

uint8_t API_environment_get_status_MV_ctrl( void );

uint8_t API_environment_get_status_UP_ctrl( void );


#endif 


