#ifndef _IUG_USER_API_INCLUDE_H_
#define _IUG_USER_API_INCLUDE_H_

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

#include "hal_data_types.h"

#include "lin_db_tables.h"
#include "can_db_tables.h"

#include "lin_stack.h"
#include "hal_io.h"
#include "sfl_can_db.h"


#ifndef ADC_CFG_H
    #include "Adc_Cfg.h"
#endif
#ifndef DIO_CFG_H
    #include "Dio_Cfg.h"
#endif


#include "webasto_parameter.h"
#include "IUG_webasto_parameter_if.h"
#include "obd_diagnosis_cfg.h"
#include "sfl_diagnostic_trouble_code_cfg.h"
#include "IUG_site__con_target.h"

#include "IUG_user_api_IO_ctrl_deprecated.h"
#include "IUG_user_api_units_constant_deprecated.h"


#include "IUG_user_api_cmd_button_cfg.h"
#include "IUG_user_api_cmd_button_constant.h"
#include "IUG_user_api_units_constant.h"
#include "IUG_user_api_units_data_support_constant.h"
#include "IUG_user_api_user_device_cfg.h"
#include "IUG_user_api_user_device_constant.h"
#include "IUG_user_api_initial_operation_cfg.h"


#include "IUG_user_api_port_mode.h"
#include "IUG_user_api_callback.h"
#include "IUG_user_api_car_climate_ctrl.h"
#include "IUG_user_api_cmd_button.h"
#include "IUG_user_api_environment.h"
#include "IUG_user_api_error_handler.h"
#include "IUG_user_api_IO_ctrl.h"
#include "IUG_user_api_ghi_heater_interface.h"
#include "IUG_user_api_initial_operation.h"
#include "IUG_user_api_ipd.h"
#include "IUG_user_api_obd.h"
#include "IUG_user_api_port_mode.h"
#include "IUG_user_api_system.h"
#include "IUG_user_api_units.h"
#include "IUG_user_api_units_data_support.h"
#include "IUG_user_api_user_device.h"
#include "IUG_user_api_utilities.h"

#include "IUG_unit_gen_heater_type.h"
#include "IUG_user_api_control_heater_demo_cfg.h"
#include "IUG_user_api_units_data_support_demo_cfg.h"
#include "IUG_user_api_ghi_heater_demo_cfg.h"
#include "IUG_user_api_units_demo.h"






#endif 


