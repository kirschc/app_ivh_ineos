#ifndef _API_UNITS_DATA_RD_WR_SUPPORT_H_
#define _API_UNITS_DATA_RD_WR_SUPPORT_H_







IUG_UNIT_API_RETVAL_t API_units_data_rdwr_support_wtt_diag_response_create( 
                                const IUG_UNIT_API_UNIT_ID_t        unit_id_api,
                                API_UNITS_DATA_RDWRSUP_TOPIC_ID_t   topic_id,
                                uint8_t  * const request_header_data,
                                uint16_t         request_size,
                                uint8_t  * const response_header_data,
                                uint16_t * const response_size,
                                uint16_t         response_size_max );


#endif 



