#ifndef _IUG_USER_API_CAR_CLIMATE_CTRL_H_
#define _IUG_USER_API_CAR_CLIMATE_CTRL_H_



typedef enum
{
    API_CAR_CLIMATE_CTRL_MODE_NOT_CONFIGURED  = 0u, 
    API_CAR_CLIMATE_CTRL_MODE_ANALOG_OUT      = 1u, 
    API_CAR_CLIMATE_CTRL_MODE_PWM_OUT_ONLY    = 2u, 
    API_CAR_CLIMATE_CTRL_MODE_PWM_READ_OUT    = 3u, 
    API_CAR_CLIMATE_CTRL_MODE_LIN_BLOWER_CTRL = 4u, 
    API_CAR_CLIMATE_CTRL_MODE_LIN_GATEWAY     = 5u, 
    API_CAR_CLIMATE_CTRL_MODE_CAN_MEMBER      = 6u, 
    API_CAR_CLIMATE_CTRL_MODE_CAN_GATEWAY     = 7u, 
    API_CAR_CLIMATE_CTRL_MODE_MIXED_MODE      = 8u  
} enum_API_CAR_CLIMATE_CTRL_MODES;

typedef enum
{
    API_CAR_CLIMATE_CTRL_PWM_OUT_PIN_X1_LIN2 = 0u, 
    API_CAR_CLIMATE_CTRL_PWM_OUT_PIN_X2_IO3  = 1u, 
    API_CAR_CLIMATE_CTRL_PWM_OUT_ENUM_MAX    = 2u  
} enum_API_CAR_CLIMATE_CTRL_PWM_OUT_PORTS;

typedef enum
{
    API_CAR_CLIMATE_CTRL_PWM_OUT_MODE_PUSHPULL = 0u, 
    API_CAR_CLIMATE_CTRL_PWM_OUT_MODE_LOWSIDE  = 1u, 
    API_CAR_CLIMATE_CTRL_PWM_OUT_MODE_HIGHSIDE = 2u, 
    API_CAR_CLIMATE_CTRL_PWM_OUT_MODE_ENUM_MAX = 3u  
} enum_API_CAR_CLIMATE_CTRL_PWM_OUT_MODES;

typedef enum
{
    API_CAR_CLIMATE_CTRL_PWM_OUT_ERR_NONE             = 0u, 
    API_CAR_CLIMATE_CTRL_PWM_OUT_ERR_INVAL_DUTY       = 1u, 
    API_CAR_CLIMATE_CTRL_PWM_OUT_ERR_INVAL_FREQ       = 2u, 
    API_CAR_CLIMATE_CTRL_PWM_OUT_ERR_INVAL_AMPL       = 3u, 
    API_CAR_CLIMATE_CTRL_PWM_OUT_ERR_INVAL_PWM_MODULE = 4u, 
    API_CAR_CLIMATE_CTRL_PWM_OUT_ERR_INVAL_PWM_MODE   = 5u, 
    API_CAR_CLIMATE_CTRL_PWM_OUT_ERR_INVAL_PARA       = 6u, 
    API_CAR_CLIMATE_CTRL_PWM_OUT_ERR_INVAL_PORT_MODE  = 7u
} enum_API_CAR_CLIMATE_CTRL_PWM_OUT_ERROR_STATES;

typedef enum
{
    API_CAR_CLIMATE_CTRL_PWM_IN_ERR_NONE            = 0u, 
    API_CAR_CLIMATE_CTRL_PWM_IN_ERR_NOT_ACT         = 1u, 
    API_CAR_CLIMATE_CTRL_PWM_IN_ERR_INVAL_PORT_MODE = 2u, 
    API_CAR_CLIMATE_CTRL_PWM_IN_ERR_INVAL_PARA      = 3u  
} enum_API_CAR_CLIMATE_CTRL_PWM_IN_ERROR_STATES;

typedef enum
{
    API_CAR_CLIMATE_CTRL_CAR_SOURCE_KL15 = 0u, 
    API_CAR_CLIMATE_CTRL_CAR_SOURCE_LIN1 = 1u, 
    API_CAR_CLIMATE_CTRL_CAR_SOURCE_CAN1 = 2u, 
    API_CAR_CLIMATE_CTRL_CAR_SOURCE_CAN3 = 3u  
} enum_API_CAR_CLIMATE_CTRL_CAR_SOURCES;

typedef struct
{
    uint32_t amplitude;  
    uint32_t frequency;  
    uint32_t duty_cycle; 
} struct_api_car_climate_ctrl_pwm_values;

typedef struct
{
    enum_API_CAR_CLIMATE_CTRL_PWM_OUT_PORTS module; 
    enum_API_CAR_CLIMATE_CTRL_PWM_OUT_MODES mode;   
    struct_api_car_climate_ctrl_pwm_values  values; 
} struct_api_car_climate_ctrl_pwm_out_data;


enum_API_CAR_CLIMATE_CTRL_PWM_OUT_ERROR_STATES API_car_climate_ctrl_set_pwm_out_para( const struct_api_car_climate_ctrl_pwm_out_data *const pwm_out_values );

enum_API_CAR_CLIMATE_CTRL_PWM_OUT_ERROR_STATES API_car_climate_ctrl_enable_pwm_out( const enum_API_CAR_CLIMATE_CTRL_PWM_OUT_PORTS pwm_module );

enum_API_CAR_CLIMATE_CTRL_PWM_OUT_ERROR_STATES API_car_climate_ctrl_disable_pwm_out( const enum_API_CAR_CLIMATE_CTRL_PWM_OUT_PORTS pwm_module );

enum_API_CAR_CLIMATE_CTRL_PWM_IN_ERROR_STATES API_car_climate_ctrl_get_pwm_in_para( struct_api_car_climate_ctrl_pwm_values *const pwm_read_values );

void API_car_climate_ctrl_enable_pwm_in( void );

void API_car_climate_ctrl_disable_pwm_in( void );

uint8_t API_car_climate_ctrl_get_status_climate_ctrl( void );

uint8_t API_car_climate_ctrl_get_car_state( void );

uint8_t API_car_climate_ctrl_set_car_state_detection_source( const enum_API_CAR_CLIMATE_CTRL_CAR_SOURCES car_state_source, const uint8_t act_state );


#endif 


