#ifndef IUG_USER_API_UNITS_DATA_SUPPORT_H
#define IUG_USER_API_UNITS_DATA_SUPPORT_H

#include "IUG_user_api_units_constant.h"






IUG_UNIT_API_RETVAL_t API_units_data_support_trigger( const IUG_UNIT_API_UNIT_ID_t unit_id_api, API_UNITS_DATA_SUPPORT_PARAMETER_t * const unit_data_param );

IUG_UNIT_API_RETVAL_t API_units_data_support_status( const IUG_UNIT_API_UNIT_ID_t unit_id_api, const API_UNITS_DATA_SUPPORT_ID_t unit_data_id );



#endif 


