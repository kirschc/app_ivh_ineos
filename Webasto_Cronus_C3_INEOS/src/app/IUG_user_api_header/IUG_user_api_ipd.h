#ifndef IUG_USER_API_IPD_H
#define IUG_USER_API_IPD_H




typedef enum
{
    IUG_API_IPD_RETVAL_OK                               = 0u,   
    IUG_API_IPD_RETVAL_ERR_NULL_POINTER                 = 1u,   
    IUG_API_IPD_RETVAL_ERR_INVAL_PARAM_ID               = 2u,   
    IUG_API_IPD_RETVAL_ERR_INVAL_PARAM_VAL              = 3u,   
} enum_IUG_API_IPD_RETVAL_t;


enum_IUG_API_IPD_RETVAL_t API_ipd_get_para_by_id( const enum_parameter_id ipd_para_id, int32_t * const ipd_out_para_val );

enum_IUG_API_IPD_RETVAL_t API_ipd_set_para_by_id( const enum_parameter_id ipd_para_id, const int32_t ipd_para_val );

enum_IUG_API_IPD_RETVAL_t API_ipd_get_para_ary_by_id( const enum_parameter_id ipd_para_id, uint8_t const * *const ipd_out_para_array_ptr );

enum_IUG_API_IPD_RETVAL_t API_ipd_set_para_ary_by_id( const enum_parameter_id ipd_para_id, uint8_t * const ipd_para_array_ptr);

#endif 


