#ifndef __IUG_USER_API_GHI_HEATER_INTERFACE_H_
#define __IUG_USER_API_GHI_HEATER_INTERFACE_H_


#define API_GHI_HTR_IF_HTR_ID_RESPONSE_BUF_MAX      25u 


typedef struct
{
    uint8_t wbus_meta_mode;    
    uint8_t wbus_cmd_req;      
    uint8_t wbus_cmd_act_time; 
    uint8_t wbus_cmd_setpoint; 
} struct_api_ghi_heater_interface_htr_cmd_req;


typedef struct
{
    struct_api_ghi_heater_interface_htr_cmd_req     htr_cmd_ack;
    uint8_t                                         htr_nak_code;   

} struct_api_ghi_heater_interface_htr_cmd_ack;


typedef struct
{
    uint16_t htr_status_29;     
    uint8_t  htr_err_STFL;      
    uint8_t  htr_err_HGV;       
    uint8_t  htr_err_UEHFL;     
    uint8_t  htr_outport;       
    uint8_t  htr_temp;          
    uint16_t supply_voltage;    

    #ifdef joki_STATUS
    uint8_t     ghi_com_dev;    

    uint16_t    req_sze;        
    uint8_t     *req_buf;       

    uint16_t    rsp_sze;        
    uint16_t    rsp_sze_max;    
    uint8_t     *rsp_buf;       
    #endif

} struct_api_ghi_heater_interface_htr_status;

typedef struct
{
    uint16_t    comm_timeout_ms;    

} struct_api_ghi_heater_interface_htr_property;


typedef struct
{
    uint8_t     ghi_com_dev;    
    #ifndef joki_IDENT
    uint8_t     req_id;         
    #endif
    #ifdef joki_IDENT
    uint16_t    req_sze;        
    uint8_t     *req_buf;       
    #endif

    #ifndef joki_IDENT
    uint8_t     rsp_id;         
    uint8_t     rsp_sze;        
    uint8_t     rsp_buf[API_GHI_HTR_IF_HTR_ID_RESPONSE_BUF_MAX];   
    #endif
    #ifdef joki_IDENT
    uint16_t    rsp_sze;        
    uint16_t    rsp_sze_max;    
    uint8_t     *rsp_buf;       
    #endif

} struct_api_ghi_heater_interface_htr_id;


typedef void (*funct_ptr_ghi_heater_preset_cmd)(const IUG_UNIT_API_UNIT_ID_t, const struct_api_ghi_heater_interface_htr_cmd_req);

typedef void (*funct_ptr_ghi_heater_provide_cmd_ack)(const IUG_UNIT_API_UNIT_ID_t, struct_api_ghi_heater_interface_htr_cmd_ack * const );

typedef void (*funct_ptr_ghi_heater_provide_id)(const IUG_UNIT_API_UNIT_ID_t, struct_api_ghi_heater_interface_htr_id * const);

typedef void (*funct_ptr_ghi_heater_provide_status)(const IUG_UNIT_API_UNIT_ID_t, struct_api_ghi_heater_interface_htr_status * const);

typedef void (*funct_ptr_ghi_heater_provide_keep_alive)(const IUG_UNIT_API_UNIT_ID_t, uint8_t *const);

typedef void (*funct_ptr_ghi_heater_provide_property)(const IUG_UNIT_API_UNIT_ID_t, struct_api_ghi_heater_interface_htr_property * const);


void API_ghi_heater_interface_init(const funct_ptr_ghi_heater_provide_property      cb_provide_htr_property, \
                                   const funct_ptr_ghi_heater_preset_cmd            cb_preset_htr_cmd, \
                                   const funct_ptr_ghi_heater_provide_cmd_ack       cb_provide_htr_cmd_ack, \
                                   const funct_ptr_ghi_heater_provide_id            cb_provide_htr_id, \
                                   const funct_ptr_ghi_heater_provide_status        cb_provide_htr_status, \
                                   const funct_ptr_ghi_heater_provide_keep_alive    cb_provide_htr_keep_alive_info );


#endif 


