#ifndef __IUG_USER_API_INITIAL_OPERATION_H_
#define __IUG_USER_API_INITIAL_OPERATION_H_



typedef enum
{
    API_INIT_OP_ERR_NONE        = 0u, 
    API_INIT_OP_ERR_INVAL_ROUT  = 1u, 
    API_INIT_OP_ERR_SKIP_REJECT = 2u, 
    API_INIT_OP_ERR_INVAL_PARA  = 3u, 
    API_INIT_OP_ERR_MANU_MODE   = 4u  
} enum_API_INIT_OP_ERROR_STATES;

typedef enum
{
    API_INIT_OP_ROUT_SCAN_WBUS_HTR      = 0u,  
    API_INIT_OP_ROUT_SCAN_GHI_HTR       = 1u,  
    API_INIT_OP_ROUT_BE                 = 2u,  
    API_INIT_OP_ROUT_BE_ASSIGN          = 3u,  
    API_INIT_OP_ROUT_T_LIN_R            = 4u,  
    API_INIT_OP_ROUT_DOSING_PUMP_HG1    = 5u,  
    API_INIT_OP_ROUT_TEMP_SENS          = 6u,  
    API_INIT_OP_ROUT_CAR_STATE          = 7u,  
    API_INIT_OP_ROUT_OBD                = 8u,  
    API_INIT_OP_ROUT_CAR_CLIMATE_DETECT = 9u,  
    API_INIT_OP_ROUT_CAR_CLIMATE_CHECK  = 10u, 
    API_INIT_OP_ROUT_DISPL_STATE        = 11u, 
    API_INIT_OP_ROUT_TEST_RUN_HG1       = 12u, 
    API_INIT_OP_ROUT_INACT_CAR_STATE    = 13u, 
    API_INIT_OP_ROUT_TEST_RUN_HG2       = 14u, 
    API_INIT_OP_ROUT_DOSING_PUMP_HG2    = 15u, 
    API_INIT_OP_ROUT_TEACH_IN_TELESTART = 16u, 
    API_INIT_OP_ROUT_COOLANT_PUMP_HG1   = 17u, 
    API_INIT_OP_ROUT_COOLANT_PUMP_HG2   = 18u  
} enum_API_INIT_OP_ROUTINES;

typedef enum
{
    API_INIT_OP_ROUT_STATE_INIT              = 1u,  
    API_INIT_OP_ROUT_STATE_PENDING           = 2u,  
    API_INIT_OP_ROUT_STATE_SUCCESSFUL        = 3u,  
    API_INIT_OP_ROUT_STATE_UNSUCCESSFUL      = 4u,  
    API_INIT_OP_ROUT_STATE_PSEUDO_SUCCESSFUL = 5u,  
    API_INIT_OP_ROUT_STATE_SKIP_REQ          = 6u,  
    API_INIT_OP_ROUT_STATE_SKIP_CFM          = 7u,  
    API_INIT_OP_ROUT_STATE_CLOSED_REQ        = 8u,  
    API_INIT_OP_ROUT_STATE_CLOSED_CFM        = 9u,  
    API_INIT_OP_ROUT_STATE_ABORTED_REQ       = 10u, 
    API_INIT_OP_ROUT_STATE_ABORTED_CFM       = 11u, 
    API_INIT_OP_ROUT_STATE_TIMEOUT_REQ       = 12u, 
    API_INIT_OP_ROUT_STATE_TIMEOUT_CFM       = 13u, 
    API_INIT_OP_ROUT_STATE_STOP_DTC_REQ      = 14u, 
    API_INIT_OP_ROUT_STATE_STOP_DTC_CFM      = 15u  
} enum_API_INIT_OP_ROUTINE_STATES;

typedef enum_API_INIT_OP_ROUTINE_STATES (*fct_ptr_api_init_op_climate_detect)(const enum_API_INIT_OP_ROUTINE_STATES);


enum_API_INIT_OP_ERROR_STATES API_initial_operation_set_rout_skip( const enum_API_INIT_OP_ROUTINES init_op_rout );

enum_API_INIT_OP_ERROR_STATES API_initial_operation_set_cb_climate_detect( const fct_ptr_api_init_op_climate_detect cb_climate_detect );

enum_API_INIT_OP_ERROR_STATES API_initial_operation_set_to_auto_mode(void);


#endif 


