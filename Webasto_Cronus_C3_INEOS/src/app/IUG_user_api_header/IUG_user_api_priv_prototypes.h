#ifndef __IUG_USER_API_PRIV_PROTOTYPES_H_
#define __IUG_USER_API_PRIV_PROTOTYPES_H_



typedef enum
{
    API_PORT_MODE_X1_CAN3_NOT_USED = 0u, 
    API_PORT_MODE_X1_CAN3_CAN      = 1u, 
    API_PORT_MODE_X1_CAN3_OBD      = 2u, 
    API_PORT_MODE_X1_CAN3_CAN_OBD  = 3u  
} enum_API_PORT_MODE_X1_CAN3_MODES;

typedef enum
{
    API_PORT_MODE_X1_9_KL15_UNUSED = 0u, 
    API_PORT_MODE_X1_9_KL15_INPUT  = 1u  
} enum_API_PORT_MODE_X1_9_KL15_MODES;

typedef enum
{
    API_PORT_MODE_X1_LIN_PWM_REL_UNUSED = 0u, 
    API_PORT_MODE_X1_LIN_PWM_REL_USED   = 1u  
} enum_API_PORT_MODE_X1_LIN_PWM_REL_MODES;

typedef enum
{
    API_PORT_MODE_X1_5_LIN3_WBUS_HG_UNUSED = 0u, 
    API_PORT_MODE_X1_5_LIN3_WBUS_HG_LIN    = 1u, 
    API_PORT_MODE_X1_5_LIN3_WBUS_HG_WBUS   = 2u  
} enum_API_PORT_MODE_X1_5_LIN3_WBUS_HG_MODES;

typedef enum
{
    API_PORT_MODE_X1_6_LIN4_WBUS_BE_UNUSED = 0u, 
    API_PORT_MODE_X1_6_LIN4_WBUS_BE_LIN    = 1u, 
    API_PORT_MODE_X1_6_LIN4_WBUS_BE_WBUS   = 2u  
} enum_API_PORT_MODE_X1_6_LIN4_WBUS_BE_MODES;

typedef enum
{
    API_PORT_MODE_X1_7_LIN5_UNUSED = 0u, 
    API_PORT_MODE_X1_7_LIN5_LIN    = 1u  
} enum_API_PORT_MODE_X1_7_LIN5_MODES;

typedef enum
{
    API_PORT_MODE_X1_12_OUT1_UNUSED = 0u, 
    API_PORT_MODE_X1_12_OUT1_OUTPUT = 1u 
} enum_API_PORT_MODE_X1_12_OUT1_MODES;

typedef enum
{
    API_PORT_MODE_X1_10_PUSH_BUTTON_UNUSED      = 0u, 
    API_PORT_MODE_X1_10_PUSH_BUTTON_PUSH_BUTTON = 1u  
} enum_API_PORT_MODE_X1_10_PUSH_BUTTON_MODES;

typedef enum
{
    API_PORT_MODE_PIN_X1_14_REL1_UNUSED         = 0u, 
    API_PORT_MODE_PIN_X1_14_REL1_LOWSIDE_RELAIS = 1u  
} enum_API_PORT_MODE_X1_14_REL1_MODES;

typedef enum
{
    API_PORT_MODE_X1_13_VDD12_BE_UNUSED = 0u, 
    API_PORT_MODE_X1_13_VDD12_BE_OUTPUT = 1u  
} enum_API_PORT_MODE_X1_13_VDD12_BE_MODES;

typedef enum
{
    API_PORT_MODE_X2_CAN_REL_UNUSED = 0u, 
    API_PORT_MODE_X2_CAN_REL_USED   = 1u  
} enum_API_PORT_MODE_X2_CAN_REL_MODES;

typedef enum
{
    API_PORT_MODE_X2_CAN1_UNUSED = 0u, 
    API_PORT_MODE_X2_CAN1_CAN    = 1u  
} enum_API_PORT_MODE_X2_CAN1_MODES;

typedef enum
{
    API_PORT_MODE_X2_CAN2_UNUSED = 0u, 
    API_PORT_MODE_X2_CAN2_CAN    = 1u  
} enum_API_PORT_MODE_X2_CAN2_MODES;

typedef enum
{
    API_PORT_MODE_X2_12_REL2_UNUSED         = 0u, 
    API_PORT_MODE_X2_12_REL2_LOWSIDE_RELAIS = 1u  
} enum_API_PORT_MODE_X2_12_REL2_MODES;

typedef enum
{
    API_PORT_MODE_X2_RELAIS_UNUSED = 0u, 
    API_PORT_MODE_X2_RELAIS_USED   = 1u  
} enum_API_PORT_MODE_X2_RELAIS_MODES;


void API_priv_initial_operation_get_ary_rout_skip( const uint8_t **const ptr_ary_ibn_skip );

int8_t API_priv_ghi_heater_interface_fetch_htr_property( const IUG_UNIT_API_UNIT_ID_t htr_idx, struct_api_ghi_heater_interface_htr_property * const htr_property );

int8_t API_priv_ghi_heater_interface_fetch_htr_cmd_ack( const IUG_UNIT_API_UNIT_ID_t api_unit_htr_idx, struct_api_ghi_heater_interface_htr_cmd_ack *const htr_cmd_ack );

int8_t API_priv_ghi_heater_interface_fetch_htr_id( const IUG_UNIT_API_UNIT_ID_t api_unit_htr_idx, struct_api_ghi_heater_interface_htr_id *const htr_id );

int8_t API_priv_ghi_heater_interface_fetch_htr_status( const IUG_UNIT_API_UNIT_ID_t api_unit_htr_idx, struct_api_ghi_heater_interface_htr_status *const htr_status );

int8_t API_priv_ghi_heater_interface_fetch_htr_keep_alive_info( const IUG_UNIT_API_UNIT_ID_t api_unit_htr_idx, uint8_t *const ptr_keep_alive_status );

int8_t API_priv_ghi_heater_interface_preset_htr_cmd( const IUG_UNIT_API_UNIT_ID_t api_unit_htr_idx, const struct_api_ghi_heater_interface_htr_cmd_req cmd_req );

void API_priv_port_mode_init( void );

void API_priv_port_mode_handler_cyclic( void );

IUG_UNIT_API_UNIT_ID_t API_units_translt_htr_idx_2_api_unit( const uint8_t htr_idx );



#endif 


