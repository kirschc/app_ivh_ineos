#ifndef _IUG_SITE__STR_TARGET_H_
#define _IUG_SITE__STR_TARGET_H_






typedef struct struct_IUG_SITE_CONTROL_CTL
{
    IUG_SITE_CONTROL_ID_t       ctl_id;     

    IUG_UNIT_ID_t               unit_id;    
    WBUS_MEMBER_t               wbus_mbr;   

} struct_IUG_SITE_CONTROL_CTL_t;
typedef struct_IUG_SITE_CONTROL_CTL_t    IUG_SITE_CONTROL_CTL_t;


typedef struct struct_IUG_SITE_TARGET_CFG
{
    IUG_SITE_TARGET_ID_t        tgt_id;     
    IUG_SITE_TARGET_CAPA_t      tgt_capa;   

} struct_IUG_SITE_TARGET_CFG_t;
typedef struct_IUG_SITE_TARGET_CFG_t    IUG_SITE_TARGET_CFG_t;


typedef struct struct_IUG_SITE_TARGET_CTL
{
    IUG_SITE_TARGET_ID_t        tgt_id;     
    IUG_SITE_TARGET_CAPA_t      tgt_capa;   

    IUG_UNIT_ID_t               unit_id;    
    WBUS_MEMBER_t               wbus_mbr;   

    IUG_HEATER_UNIT_ENUM_t      htr_idx;    
    API_USER_DEVICE_ID_t        udv_idx;    

} struct_IUG_SITE_TARGET_CTL_t;
typedef struct_IUG_SITE_TARGET_CTL_t    IUG_SITE_TARGET_CTL_t;







#endif 

