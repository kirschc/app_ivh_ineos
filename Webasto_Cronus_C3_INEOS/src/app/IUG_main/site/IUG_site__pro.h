#ifndef _IUG_SITE__PRO_H_
#define _IUG_SITE__PRO_H_






uint16_t                IUG_site_check_tgt_id_cfg( void );
uint16_t                IUG_site_check_tgt_id_list( void );
void                    IUG_site_cyclic( void );
IUG_SITE_TARGET_ID_t    IUG_site_decide_tgt_id_for_gen_heater( IUG_UNIT_HEATER_PROPERTY_t * const p_htr_pty );
IUG_SITE_TARGET_CFG_t const * IUG_site_get_tgt_cfg__via_tgt_id( const IUG_SITE_TARGET_ID_t tgt_id );
IUG_SITE_TARGET_ID_t    IUG_site_get_tgt_id__for_first_address( const IUG_SITE_TARGET_ID_t tgt_id );
IUG_SITE_TARGET_IDX_t   IUG_site_get_tgt_idx__unused( void );
IUG_SITE_TARGET_IDX_t   IUG_site_get_tgt_idx__via_htr_idx( const IUG_HEATER_UNIT_ENUM_t idx_htr );
IUG_SITE_TARGET_IDX_t   IUG_site_get_tgt_idx__via_tgt_id( const IUG_SITE_TARGET_ID_t tgt_id );
IUG_SITE_TARGET_IDX_t   IUG_site_get_tgt_idx__via_udv_idx( const API_USER_DEVICE_ID_t idx_udv );
void                    IUG_site_init( void );


#endif 


