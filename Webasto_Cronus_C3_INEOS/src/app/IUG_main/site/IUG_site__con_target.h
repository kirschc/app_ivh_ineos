#ifndef _IUG_SITE__CON_TARGET_H_
#define _IUG_SITE__CON_TARGET_H_








typedef enum enum_IUG_SITE_TARGET_GROUP_ID
{
    TGT_COM_WBUS_ID             = 0x00u,    
    TGT_COM_GHI_ID              = 0x80u,    

    TGT_MSK_COM                 = (TGT_COM_GHI_ID),

    TGT_MSK_COM_MISC            = (0x80u|0x40u),



    TGT_WBUS_DEF_AIR            = (0x01u << 1u),    
    TGT_WBUS_DEF_WAT            = (0x02u << 1u),    

    TGT_WBUS_AT_3500_AIR        = (0x03u << 1u),    
    TGT_WBUS_AT_40_55_AIR       = (0x04u << 1u),    
    TGT_WBUS_AT_EVO_2000_AIR    = (0x05u << 1u),    
    TGT_WBUS_AT_2000_STC_AIR    = (0x06u << 1u),    

    TGT_WBUS_TT_VEVO_WAT        = (0x07u << 1u),    
    TGT_WBUS_TT_Z_C_WAT         = (0x08u << 1u),    
    TGT_WBUS_TT_EVO_GEN1_WAT    = (0x09u << 1u),    
    TGT_WBUS_TT_EVO_GEN4_WAT    = (0x0Au << 1u),    

    TGT_WBUS_TP_50_WAT          = (0x0Bu << 1u),    
    TGT_WBUS_TP_90_WAT          = (0x0Cu << 1u),    
    TGT_WBUS_TP_120_WAT         = (0x0Du << 1u),    


    TGT_WBUS_ADDR_4_ID          = 0x00u,
    TGT_WBUS_ADDR_5_ID          = 0x01u,

    TGT_MSK_WBUS_ADDR_ID        = (TGT_WBUS_ADDR_5_ID),



    TGT_GHI_DEF_AIR             = (0x00u << 3u),    
    TGT_GHI_DEF_WAT             = (0x01u << 3u),    

    TGT_GHI_HVH_AIR             = (0x02u << 3u),    
    TGT_GHI_HVH_WAT             = (0x03u << 3u),    

    TGT_GHI_GEN4_WAT            = (0x04u << 3u),    


    TGT_GHI_ADDR_1_ID           = 0x00u,
    TGT_GHI_ADDR_2_ID           = 0x01u,
    TGT_GHI_ADDR_3_ID           = 0x02u,
    TGT_GHI_ADDR_4_ID           = 0x03u,
    TGT_GHI_ADDR_5_ID           = 0x04u,
    TGT_GHI_ADDR_6_ID           = 0x05u,
    TGT_GHI_ADDR_7_ID           = 0x06u,
    TGT_GHI_ADDR_8_ID           = 0x07u,

    TGT_MSK_GHI_ADDR_ID         = (TGT_GHI_ADDR_8_ID),




    TGT_USR_DEV                 = 0x60u,    


    TGT_MISC_UDV_ADDR_1_ID      = 0x00u,
    TGT_MISC_UDV_ADDR_2_ID      = 0x01u,
    TGT_MISC_UDV_ADDR_3_ID      = 0x02u,
    TGT_MISC_UDV_ADDR_4_ID      = 0x00u,    

    TGT_MSK_MISC_UDV_ADDR_ID    = 0x03u,    

} IUG_SITE_TARGET_GROUP_ID_t;


typedef enum enum_IUG_SITE_TARGET_ID
{
    TGT_NONE                    = 0x00u,     



    TGT_WBUS_DEFAULT_AIR_4      = (TGT_COM_WBUS_ID)|(TGT_WBUS_DEF_AIR)          |(TGT_WBUS_ADDR_4_ID),
    TGT_WBUS_DEFAULT_AIR_5      = (TGT_COM_WBUS_ID)|(TGT_WBUS_DEF_AIR)          |(TGT_WBUS_ADDR_5_ID),
    TGT_WBUS_DEFAULT_WAT_4      = (TGT_COM_WBUS_ID)|(TGT_WBUS_DEF_WAT)          |(TGT_WBUS_ADDR_4_ID),
    TGT_WBUS_DEFAULT_WAT_5      = (TGT_COM_WBUS_ID)|(TGT_WBUS_DEF_WAT)          |(TGT_WBUS_ADDR_5_ID),

    TGT_AT_3500_AIR_4           = (TGT_COM_WBUS_ID)|(TGT_WBUS_AT_3500_AIR)      |(TGT_WBUS_ADDR_4_ID),
    TGT_AT_40_55_AIR_4          = (TGT_COM_WBUS_ID)|(TGT_WBUS_AT_40_55_AIR)     |(TGT_WBUS_ADDR_4_ID),
    TGT_AT_40_55_AIR_5          = (TGT_COM_WBUS_ID)|(TGT_WBUS_AT_40_55_AIR)     |(TGT_WBUS_ADDR_5_ID),
    TGT_AT_EVO_2000_AIR_4       = (TGT_COM_WBUS_ID)|(TGT_WBUS_AT_EVO_2000_AIR)  |(TGT_WBUS_ADDR_4_ID),
    TGT_AT_2000_STC_AIR_4       = (TGT_COM_WBUS_ID)|(TGT_WBUS_AT_2000_STC_AIR)  |(TGT_WBUS_ADDR_4_ID),
    TGT_AT_2000_STC_AIR_5       = (TGT_COM_WBUS_ID)|(TGT_WBUS_AT_2000_STC_AIR)  |(TGT_WBUS_ADDR_5_ID),

    TGT_TT_VEVO_WAT_4           = (TGT_COM_WBUS_ID)|(TGT_WBUS_TT_VEVO_WAT)      |(TGT_WBUS_ADDR_4_ID),
    TGT_TT_Z_C_WAT_4            = (TGT_COM_WBUS_ID)|(TGT_WBUS_TT_Z_C_WAT)       |(TGT_WBUS_ADDR_4_ID),
    TGT_TT_EVO_GEN_1_WAT_4      = (TGT_COM_WBUS_ID)|(TGT_WBUS_TT_EVO_GEN1_WAT)  |(TGT_WBUS_ADDR_4_ID),
    TGT_TT_EVO_GEN_1_WAT_5      = (TGT_COM_WBUS_ID)|(TGT_WBUS_TT_EVO_GEN1_WAT)  |(TGT_WBUS_ADDR_5_ID),
    TGT_TT_EVO_GEN_4_WAT_4      = (TGT_COM_WBUS_ID)|(TGT_WBUS_TT_EVO_GEN4_WAT)  |(TGT_WBUS_ADDR_4_ID),
    TGT_TT_EVO_GEN_4_WAT_5      = (TGT_COM_WBUS_ID)|(TGT_WBUS_TT_EVO_GEN4_WAT)  |(TGT_WBUS_ADDR_5_ID),

    TGT_TP_50_WAT_4             = (TGT_COM_WBUS_ID)|(TGT_WBUS_TP_50_WAT)        |(TGT_WBUS_ADDR_4_ID),
    TGT_TP_90_WAT_4             = (TGT_COM_WBUS_ID)|(TGT_WBUS_TP_90_WAT)        |(TGT_WBUS_ADDR_4_ID),
    TGT_TP_120_WAT_4            = (TGT_COM_WBUS_ID)|(TGT_WBUS_TP_120_WAT)       |(TGT_WBUS_ADDR_4_ID),




    TGT_GHI_DEFAULT_AIR_1       = (TGT_COM_GHI_ID)|(TGT_GHI_DEF_AIR)|(TGT_GHI_ADDR_1_ID),
    TGT_GHI_DEFAULT_AIR_2       = (TGT_COM_GHI_ID)|(TGT_GHI_DEF_AIR)|(TGT_GHI_ADDR_2_ID),
    TGT_GHI_DEFAULT_AIR_3       = (TGT_COM_GHI_ID)|(TGT_GHI_DEF_AIR)|(TGT_GHI_ADDR_3_ID),
    TGT_GHI_DEFAULT_AIR_4       = (TGT_COM_GHI_ID)|(TGT_GHI_DEF_AIR)|(TGT_GHI_ADDR_4_ID),
    TGT_GHI_DEFAULT_AIR_5       = (TGT_COM_GHI_ID)|(TGT_GHI_DEF_AIR)|(TGT_GHI_ADDR_5_ID),
    TGT_GHI_DEFAULT_AIR_6       = (TGT_COM_GHI_ID)|(TGT_GHI_DEF_AIR)|(TGT_GHI_ADDR_6_ID),
    TGT_GHI_DEFAULT_AIR_7       = (TGT_COM_GHI_ID)|(TGT_GHI_DEF_AIR)|(TGT_GHI_ADDR_7_ID),
    TGT_GHI_DEFAULT_AIR_8       = (TGT_COM_GHI_ID)|(TGT_GHI_DEF_AIR)|(TGT_GHI_ADDR_8_ID),

    TGT_GHI_DEFAULT_WAT_1       = (TGT_COM_GHI_ID)|(TGT_GHI_DEF_WAT)|(TGT_GHI_ADDR_1_ID),
    TGT_GHI_DEFAULT_WAT_2       = (TGT_COM_GHI_ID)|(TGT_GHI_DEF_WAT)|(TGT_GHI_ADDR_2_ID),
    TGT_GHI_DEFAULT_WAT_3       = (TGT_COM_GHI_ID)|(TGT_GHI_DEF_WAT)|(TGT_GHI_ADDR_3_ID),
    TGT_GHI_DEFAULT_WAT_4       = (TGT_COM_GHI_ID)|(TGT_GHI_DEF_WAT)|(TGT_GHI_ADDR_4_ID),
    TGT_GHI_DEFAULT_WAT_5       = (TGT_COM_GHI_ID)|(TGT_GHI_DEF_WAT)|(TGT_GHI_ADDR_5_ID),
    TGT_GHI_DEFAULT_WAT_6       = (TGT_COM_GHI_ID)|(TGT_GHI_DEF_WAT)|(TGT_GHI_ADDR_6_ID),
    TGT_GHI_DEFAULT_WAT_7       = (TGT_COM_GHI_ID)|(TGT_GHI_DEF_WAT)|(TGT_GHI_ADDR_7_ID),
    TGT_GHI_DEFAULT_WAT_8       = (TGT_COM_GHI_ID)|(TGT_GHI_DEF_WAT)|(TGT_GHI_ADDR_8_ID),

    TGT_GHI_HVH_AIR_1           = (TGT_COM_GHI_ID)|(TGT_GHI_HVH_AIR)|(TGT_GHI_ADDR_1_ID), 
    TGT_GHI_HVH_AIR_2           = (TGT_COM_GHI_ID)|(TGT_GHI_HVH_AIR)|(TGT_GHI_ADDR_2_ID), 
    TGT_GHI_HVH_AIR_3           = (TGT_COM_GHI_ID)|(TGT_GHI_HVH_AIR)|(TGT_GHI_ADDR_3_ID), 
    TGT_GHI_HVH_AIR_4           = (TGT_COM_GHI_ID)|(TGT_GHI_HVH_AIR)|(TGT_GHI_ADDR_4_ID), 
    TGT_GHI_HVH_AIR_5           = (TGT_COM_GHI_ID)|(TGT_GHI_HVH_AIR)|(TGT_GHI_ADDR_5_ID), 
    TGT_GHI_HVH_AIR_6           = (TGT_COM_GHI_ID)|(TGT_GHI_HVH_AIR)|(TGT_GHI_ADDR_6_ID), 
    TGT_GHI_HVH_AIR_7           = (TGT_COM_GHI_ID)|(TGT_GHI_HVH_AIR)|(TGT_GHI_ADDR_7_ID), 
    TGT_GHI_HVH_AIR_8           = (TGT_COM_GHI_ID)|(TGT_GHI_HVH_AIR)|(TGT_GHI_ADDR_8_ID), 

    TGT_GHI_HVH_WAT_1           = (TGT_COM_GHI_ID)|(TGT_GHI_HVH_WAT)|(TGT_GHI_ADDR_1_ID),
    TGT_GHI_HVH_WAT_2           = (TGT_COM_GHI_ID)|(TGT_GHI_HVH_WAT)|(TGT_GHI_ADDR_2_ID),
    TGT_GHI_HVH_WAT_3           = (TGT_COM_GHI_ID)|(TGT_GHI_HVH_WAT)|(TGT_GHI_ADDR_3_ID),
    TGT_GHI_HVH_WAT_4           = (TGT_COM_GHI_ID)|(TGT_GHI_HVH_WAT)|(TGT_GHI_ADDR_4_ID),
    TGT_GHI_HVH_WAT_5           = (TGT_COM_GHI_ID)|(TGT_GHI_HVH_WAT)|(TGT_GHI_ADDR_5_ID),
    TGT_GHI_HVH_WAT_6           = (TGT_COM_GHI_ID)|(TGT_GHI_HVH_WAT)|(TGT_GHI_ADDR_6_ID),
    TGT_GHI_HVH_WAT_7           = (TGT_COM_GHI_ID)|(TGT_GHI_HVH_WAT)|(TGT_GHI_ADDR_7_ID),
    TGT_GHI_HVH_WAT_8           = (TGT_COM_GHI_ID)|(TGT_GHI_HVH_WAT)|(TGT_GHI_ADDR_8_ID),

    TGT_GHI_GEN4_WAT_1          = (TGT_COM_GHI_ID)|(TGT_GHI_GEN4_WAT)|(TGT_GHI_ADDR_1_ID),
    TGT_GHI_GEN4_WAT_2          = (TGT_COM_GHI_ID)|(TGT_GHI_GEN4_WAT)|(TGT_GHI_ADDR_2_ID),
    TGT_GHI_GEN4_WAT_3          = (TGT_COM_GHI_ID)|(TGT_GHI_GEN4_WAT)|(TGT_GHI_ADDR_3_ID),
    TGT_GHI_GEN4_WAT_4          = (TGT_COM_GHI_ID)|(TGT_GHI_GEN4_WAT)|(TGT_GHI_ADDR_4_ID),
    TGT_GHI_GEN4_WAT_5          = (TGT_COM_GHI_ID)|(TGT_GHI_GEN4_WAT)|(TGT_GHI_ADDR_5_ID),
    TGT_GHI_GEN4_WAT_6          = (TGT_COM_GHI_ID)|(TGT_GHI_GEN4_WAT)|(TGT_GHI_ADDR_6_ID),
    TGT_GHI_GEN4_WAT_7          = (TGT_COM_GHI_ID)|(TGT_GHI_GEN4_WAT)|(TGT_GHI_ADDR_7_ID),
    TGT_GHI_GEN4_WAT_8          = (TGT_COM_GHI_ID)|(TGT_GHI_GEN4_WAT)|(TGT_GHI_ADDR_8_ID),




    TGT_MISC_USR_DEV_1          = (TGT_COM_WBUS_ID)|(TGT_USR_DEV)|(TGT_MISC_UDV_ADDR_1_ID),
    TGT_MISC_USR_DEV_2          = (TGT_COM_WBUS_ID)|(TGT_USR_DEV)|(TGT_MISC_UDV_ADDR_2_ID),
    TGT_MISC_USR_DEV_3          = (TGT_COM_WBUS_ID)|(TGT_USR_DEV)|(TGT_MISC_UDV_ADDR_3_ID),

} IUG_SITE_TARGET_ID_t;


typedef enum enum_IUG_SITE_TARGET_CAPABILITY
{
    TGT_CAPA_NONE               = 0x0000u,  

    TGTCAPCOM_UNDEF             = 0x0000u,
    TGTCAPCOM_WBUS              = 0x1000u,  
    TGTCAPCOM_GHI               = 0x2000u,  
    TGTCAPCOM_CAN               = 0x3000u,  
    TGTCAPCOM_USER_DEVICE       = 0x4000u,  
    TGTCAPCOM_BLUETOOTH         = 0x5000u,  
    TGTCAPCOM_6                 = 0x6000u,
    TGTCAPCOM_7                 = 0x7000u,
    TGTCAPCOM_8                 = 0x8000u,
    TGTCAPCOM_9                 = 0x9000u,
    TGTCAPCOM_A                 = 0xA000u,
    TGTCAPCOM_B                 = 0xB000u,
    TGTCAPCOM_C                 = 0xC000u,
    TGTCAPCOM_D                 = 0xD000u,
    TGTCAPCOM_E                 = 0xE000u,
    TGTCAPCOM_F                 = 0xF000u,
    TGTCAPCOM_MSK               = 0xF000u,  

    TGTCAPTYP_UNDEF             = 0x0000u,
    TGTCAPTYP_HEATER_AIR        = 0x0100u,  
    TGTCAPTYP_HEATER_WAT        = 0x0200u,  
    TGTCAPTYP_3                 = 0x0300u,
    TGTCAPTYP_4                 = 0x0400u,
    TGTCAPTYP_COOLER_AIR        = 0x0500u,  
    TGTCAPTYP_COOLER_WAT        = 0x0600u,  
    TGTCAPTYP_7                 = 0x0700u,
    TGTCAPTYP_8                 = 0x0800u,
    TGTCAPTYP_9                 = 0x0900u,
    TGTCAPTYP_TEMP_SENSOR       = 0x0A00u,  
    TGTCAPTYP_B                 = 0x0B00u,
    TGTCAPTYP_C                 = 0x0C00u,
    TGTCAPTYP_D                 = 0x0D00u,
    TGTCAPTYP_USER_DEVICE       = 0x0E00u,  
    TGTCAPTYP_F                 = 0x0F00u,
    TGTCAPTYP_MSK               = 0x0F00u,  

    TGTCAPFLG_NONE              = 0x0000u,  
    TGTCAPFLG_0001              = 0x0001u,
    TGTCAPFLG_0002              = 0x0002u,
    TGTCAPFLG_0004              = 0x0004u,
    TGTCAPFLG_0008              = 0x0008u,
    TGTCAPFLG_0010              = 0x0010u,
    TGTCAPFLG_TRANSIT_CMD_HT    = 0x0020u,  
    TGTCAPFLG_SERNUM_5109       = 0x0040u,  
    TGTCAPFLG_CONVERT_CMD       = 0x0080u   

} IUG_SITE_TARGET_CAPA_t;


typedef enum enum_IUG_SITE_TARGET_IDX
{
    TGT_IDX__1 = 0u,              
    TGT_IDX__2 = 1u,
    TGT_IDX__3 = 2u,
    TGT_IDX__4 = 3u,
    TGT_IDX__5 = 4u,
    TGT_IDX__6 = 5u,
    TGT_IDX__7 = 6u,
    TGT_IDX__8 = 7u,
    TGT_IDX__9 = 8u,
    TGT_IDX_10 = 9u,
    TGT_IDX_11 = 10u,
    TGT_IDX_12 = 11u,
    TGT_IDX_13 = 12u,
    TGT_IDX_14 = 13u,
    TGT_IDX_15 = 14u,
    TGT_IDX_16 = 15u,
    TGT_IDX_ENUM_MAX            

} enum_IUG_SITE_TARGET_IDX_t;
typedef enum_IUG_SITE_TARGET_IDX_t  IUG_SITE_TARGET_IDX_t;


typedef enum enum_IUG_SITE_TARGET_STATE
{
    TGT_STATE_WAITE_STATE       = 0u,
    TGT_STATE_HEATING           = 1u,
    TGT_STATE_VENTILATING       = 2u,
    TGT_STATE_HEATING_BOOST     = 3u,
    TGT_STATE_HEATING_ECO       = 4u,
    TGT_STATE_AUX_HEATING       = 5u,
    TGT_STATE_AUT_AUX_HEATING   = 6u,
    TGT_STATE_DRIVE_HEATING     = 7u,
    TGT_STATE_SWITCH_OFF        = 8u,
    TGT_STATE_FAULT             = 9u,
    TGT_STATE_ENUM_MAX              

} enum_IUG_SITE_TARGET_STATE_t;
typedef enum_IUG_SITE_TARGET_STATE_t    IUG_SITE_TARGET_STATE_t;







_Static_assert((TGT_WBUS_ADDR_4_ID) == (0x00u), "TGT_WBUS_ADDR_4_ID must be 0x00!");
_Static_assert((TGT_WBUS_ADDR_5_ID) == (0x01u), "TGT_WBUS_ADDR_5_ID must be 0x01!");


#endif 

