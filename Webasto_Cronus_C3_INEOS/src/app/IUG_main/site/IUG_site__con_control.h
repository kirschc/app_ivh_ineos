#ifndef _IUG_SITE__CON_CONTROL_H_
#define _IUG_SITE__CON_CONTROL_H_






typedef enum enum_IUG_SITE_CONTROL_ID
{
    CTL_NONE                    =  0u,  
    CTL_MULTI_CTL_3TO4          =  1u,  
    CTL_TELE_START              =  2u,  
    CTL_THERMO_CALL             =  3u,  
    CTL_THERMO_CONNECT          =  4u,  
    CTL_UNUSED_5                =  5u,  
    CTL_CMD_BUTTON              =  6u,  
    CTL_BLUETOOTH_CTL_AIR       =  7u,  
    CTL_BLUETOOTH_CTL_WAT       =  8u,  
    CTL_UNUSED_9                =  9u,  
    CTL_TEMP_SENSOR_INT         = 10u,  
    CTL_TEMP_SENSOR_EXT         = 11u,  
    CTL_TEMP_SENSOR_T_LIN_R_INT = 12u,  
    CTL_TEMP_SENSOR_T_LIN_R_EXT = 13u,  
    CTL_ENUM_MAX                

} enum_IUG_SITE_CONTROL_ID_t;
typedef enum_IUG_SITE_CONTROL_ID_t   IUG_SITE_CONTROL_ID_t;


typedef enum enum_IUG_SITE_CONTROL_IDX
{
    CTL_IDX_1 = 0u,
    CTL_IDX_2 = 1u,
    CTL_IDX_3 = 2u,
    CTL_IDX_4 = 3u,
    CTL_IDX_5 = 4u,
    CTL_IDX_6 = 5u,
    CTL_IDX_7 = 6u,
    CTL_IDX_8 = 7u,     
    CTL_IDX_ENUM_MAX    

} enum_IUG_SITE_CONTROL_IDX_t;
typedef enum_IUG_SITE_CONTROL_IDX_t  IUG_SITE_CONTROL_IDX_t;

#define CTL_IDX_CMD_BUTTON  (IUG_SITE_CONTROL_IDX_t)((CTL_IDX_ENUM_MAX) -1u)


typedef enum enum_IUG_SITE_CONTROL_STATE
{
    CTL_CMD_NONE            = 0,
    CTL_CMD_OFF             = 1,
    CTL_CMD_HEATING         = 2,
    CTL_CMD_HEATING_ECO     = 3,
    CTL_CMD_HEATING_BOOST   = 4,
    CTL_CMD_VENTILATING     = 5,
    CTL_CMD_BTN_RELEASED    = 6,
    CTL_CMD_BTN_PRESSED     = 7,
    CTL_CMD_BTN_CLICK       = 8,
    CTL_CMD_ENUM_MAX         

} enum_IUG_SITE_CONTROL_STATE_t;
typedef enum_IUG_SITE_CONTROL_STATE_t    IUG_SITE_CONTROL_STATE_t;







#endif 

