#ifndef _IUG_SITE_MAIN_H_
#define _IUG_SITE_MAIN_H_


#define IUG_TARGET_ASSIGN_TO_LAST_WEBPAR_CFG                ((IUG_HEATER_UNIT_ENUM_MAX) - 1u)
_Static_assert((IUG_TARGET_ASSIGN_TO_LAST_WEBPAR_CFG)==((IUG_HEATER_UNIT_ENUM_MAX) - 1u),"Analyze: 'IUG_TARGET_ASSIGN_TO_LAST_WEBPAR_CFG'");


#define IUG_TARGET_WEBPAR_CFG_ASSIGN_TO_LAST_WBUS_HTR       1u 
_Static_assert((IUG_TARGET_WEBPAR_CFG_ASSIGN_TO_LAST_WBUS_HTR)==(1u),"Analyze: 'IUG_TARGET_WEBPAR_CFG_ASSIGN_TO_LAST_WBUS_HTR'");


#define IUG_TARGET_WEBPAR_CFG_ASSIGN_TO_FIRST_USER_DEVICE   3u 
#define IUG_TARGET_WEBPAR_CFG_ASSIGN_TO_LAST_USER_DEVICE    5u 
_Static_assert((IUG_TARGET_WEBPAR_CFG_ASSIGN_TO_FIRST_USER_DEVICE)==(3u),"Analyze: 'IUG_TARGET_WEBPAR_CFG_ASSIGN_TO_FIRST_USER_DEVICE'");
_Static_assert((IUG_TARGET_WEBPAR_CFG_ASSIGN_TO_LAST_USER_DEVICE) ==(5u),"Analyze: 'IUG_TARGET_WEBPAR_CFG_ASSIGN_TO_LAST_USER_DEVICE'");


#define IUG_TARGET_WEBPAR_CFG_ASSIGN_TO_FIRST_LIN_HTR       0x10u   
#define IUG_TARGET_WEBPAR_CFG_ASSIGN_TO_LAST_LIN_HTR        ((IUG_TARGET_WEBPAR_CFG_ASSIGN_TO_FIRST_LIN_HTR) + ((IUG_HEATER_UNIT_ENUM_MAX)-1u)) 
_Static_assert((IUG_TARGET_WEBPAR_CFG_ASSIGN_TO_FIRST_LIN_HTR)==(0x10u),"Analyze: 'IUG_TARGET_WEBPAR_CFG_ASSIGN_TO_FIRST_LIN_HTR'");
_Static_assert((IUG_TARGET_WEBPAR_CFG_ASSIGN_TO_LAST_LIN_HTR) ==(0x17u),"Analyze: 'IUG_TARGET_WEBPAR_CFG_ASSIGN_TO_LAST_LIN_HTR'");




extern IUG_SITE_CONTROL_CTL_t      ext_IUG_SITE_CONTROL_CTL[(CTL_IDX_ENUM_MAX)];
extern IUG_SITE_TARGET_CTL_t       ext_IUG_SITE_TARGET_CTL[(TGT_IDX_ENUM_MAX)];




#endif 


