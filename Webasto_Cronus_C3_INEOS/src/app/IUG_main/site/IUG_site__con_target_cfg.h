#ifndef _IUG_SITE__CON_TARGET_CFG_H_
#define _IUG_SITE__CON_TARGET_CFG_H_




#define IUG_SITE_TARGET_CFG_DEF \
{\
\
    \
    \
    \
    {(TGT_NONE)                 ,(TGT_CAPA_NONE)                                                                                                       },\
    \
    \
    \
    {(TGT_WBUS_DEFAULT_AIR_4)   ,(IUG_SITE_TARGET_CAPA_t)((TGTCAPCOM_WBUS)|(TGTCAPTYP_HEATER_AIR)|(TGTCAPFLG_CONVERT_CMD)|(TGTCAPFLG_TRANSIT_CMD_HT))  },\
    \
    {(TGT_WBUS_DEFAULT_WAT_4)   ,(IUG_SITE_TARGET_CAPA_t)((TGTCAPCOM_WBUS)|(TGTCAPTYP_HEATER_WAT)|(TGTCAPFLG_CONVERT_CMD)                           )  },\
    \
    \
    {(TGT_AT_3500_AIR_4)        ,(IUG_SITE_TARGET_CAPA_t)((TGTCAPCOM_WBUS)|(TGTCAPTYP_HEATER_AIR)                        |(TGTCAPFLG_TRANSIT_CMD_HT))  },\
    \
    \
    {(TGT_AT_40_55_AIR_4)       ,(IUG_SITE_TARGET_CAPA_t)((TGTCAPCOM_WBUS)|(TGTCAPTYP_HEATER_AIR)                        |(TGTCAPFLG_TRANSIT_CMD_HT))  },\
    \
    \
    {(TGT_AT_EVO_2000_AIR_4)    ,(IUG_SITE_TARGET_CAPA_t)((TGTCAPCOM_WBUS)|(TGTCAPTYP_HEATER_AIR)|(TGTCAPFLG_CONVERT_CMD)|(TGTCAPFLG_TRANSIT_CMD_HT))  },\
    \
    {(TGT_AT_2000_STC_AIR_4)    ,(IUG_SITE_TARGET_CAPA_t)((TGTCAPCOM_WBUS)|(TGTCAPTYP_HEATER_AIR)|(TGTCAPFLG_CONVERT_CMD)|(TGTCAPFLG_TRANSIT_CMD_HT))  },\
    \
    \
    {(TGT_TT_VEVO_WAT_4)        ,(IUG_SITE_TARGET_CAPA_t)((TGTCAPCOM_WBUS)|(TGTCAPTYP_HEATER_WAT)|(TGTCAPFLG_CONVERT_CMD)                           )  },\
    \
    {(TGT_TT_Z_C_WAT_4)         ,(IUG_SITE_TARGET_CAPA_t)((TGTCAPCOM_WBUS)|(TGTCAPTYP_HEATER_WAT)|(TGTCAPFLG_CONVERT_CMD)                           )  },\
    \
    \
    {(TGT_TT_EVO_GEN_1_WAT_4)   ,(IUG_SITE_TARGET_CAPA_t)((TGTCAPCOM_WBUS)|(TGTCAPTYP_HEATER_WAT)|(TGTCAPFLG_CONVERT_CMD)                           )  },\
    \
    \
    {(TGT_TT_EVO_GEN_4_WAT_4)   ,(IUG_SITE_TARGET_CAPA_t)((TGTCAPCOM_WBUS)|(TGTCAPTYP_HEATER_WAT)|(TGTCAPFLG_CONVERT_CMD)                           )  },\
    \
    \
    {(TGT_TP_50_WAT_4)          ,(IUG_SITE_TARGET_CAPA_t)((TGTCAPCOM_WBUS)|(TGTCAPTYP_HEATER_WAT)|(TGTCAPFLG_CONVERT_CMD)|(TGTCAPFLG_TRANSIT_CMD_HT))  },\
    \
    {(TGT_TP_90_WAT_4)          ,(IUG_SITE_TARGET_CAPA_t)((TGTCAPCOM_WBUS)|(TGTCAPTYP_HEATER_WAT)|(TGTCAPFLG_CONVERT_CMD)|(TGTCAPFLG_TRANSIT_CMD_HT))  },\
    \
    {(TGT_TP_120_WAT_4)         ,(IUG_SITE_TARGET_CAPA_t)((TGTCAPCOM_WBUS)|(TGTCAPTYP_HEATER_WAT)|(TGTCAPFLG_CONVERT_CMD)|(TGTCAPFLG_TRANSIT_CMD_HT))  },\
    \
    \
    \
    \
    {(TGT_MISC_USR_DEV_1)       ,(IUG_SITE_TARGET_CAPA_t)((TGTCAPCOM_USER_DEVICE)|(TGTCAPTYP_USER_DEVICE))                                             },\
    {(TGT_MISC_USR_DEV_2)       ,(IUG_SITE_TARGET_CAPA_t)((TGTCAPCOM_USER_DEVICE)|(TGTCAPTYP_USER_DEVICE))                                             },\
    {(TGT_MISC_USR_DEV_3)       ,(IUG_SITE_TARGET_CAPA_t)((TGTCAPCOM_USER_DEVICE)|(TGTCAPTYP_USER_DEVICE))                                             },\
    \
    \
    \
    {(TGT_GHI_DEFAULT_AIR_1)    ,(IUG_SITE_TARGET_CAPA_t)((TGTCAPCOM_GHI)|(TGTCAPTYP_HEATER_AIR)|(TGTCAPFLG_CONVERT_CMD)                           )   },\
    \
    \
    {(TGT_GHI_DEFAULT_WAT_1)    ,(IUG_SITE_TARGET_CAPA_t)((TGTCAPCOM_GHI)|(TGTCAPTYP_HEATER_WAT)|(TGTCAPFLG_CONVERT_CMD)                           )   },\
    \
    \
    {(TGT_GHI_HVH_AIR_1)        ,(IUG_SITE_TARGET_CAPA_t)((TGTCAPCOM_GHI)|(TGTCAPTYP_HEATER_AIR)|(TGTCAPFLG_CONVERT_CMD)                           )   },\
    \
    \
    {(TGT_GHI_HVH_WAT_1)        ,(IUG_SITE_TARGET_CAPA_t)((TGTCAPCOM_GHI)|(TGTCAPTYP_HEATER_WAT)|(TGTCAPFLG_CONVERT_CMD)                           )   },\
    \
    \
    {(TGT_GHI_GEN4_WAT_1)       ,(IUG_SITE_TARGET_CAPA_t)((TGTCAPCOM_GHI)|(TGTCAPTYP_HEATER_WAT)                                                   )   },\
    \
}


#define IUG_SITE_TARGET_ID_LIST_DEF \
{\
\
    \
    \
    \
    (TGT_NONE),\
    \
    \
    \
    \
    (TGT_WBUS_DEFAULT_AIR_4),\
    \
    (TGT_WBUS_DEFAULT_WAT_5),\
    \
    \
    (TGT_AT_3500_AIR_4),\
    \
    (TGT_AT_40_55_AIR_4),\
    \
    (TGT_AT_EVO_2000_AIR_4),\
    \
    (TGT_AT_2000_STC_AIR_4),\
    \
    \
    (TGT_TT_VEVO_WAT_4),\
    \
    (TGT_TT_Z_C_WAT_4),\
    \
    (TGT_TT_EVO_GEN_1_WAT_4),\
    \
    (TGT_TT_EVO_GEN_4_WAT_4),\
    \
    \
    (TGT_TP_50_WAT_4),\
    \
    (TGT_TP_90_WAT_4),\
    \
    (TGT_TP_120_WAT_4),\
    \
    \
    \
    \
    \
    \
    \
    \
    \
    (TGT_MISC_USR_DEV_1),\
    (TGT_MISC_USR_DEV_2),\
    (TGT_MISC_USR_DEV_3),\
    \
    \
    \
    \
    (TGT_GHI_DEFAULT_AIR_1),\
    \
    (TGT_GHI_DEFAULT_WAT_1),\
    \
    \
    (TGT_GHI_HVH_AIR_1),\
    \
    (TGT_GHI_HVH_WAT_1),\
    \
    \
    (TGT_GHI_GEN4_WAT_1),\
    \
    \
    \
    \
    \
    \
    \
    \
    \
    \
    \
    \
    \
    \
}








#endif 

