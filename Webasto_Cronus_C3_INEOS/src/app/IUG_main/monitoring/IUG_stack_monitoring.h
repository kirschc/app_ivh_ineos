#ifndef __STK_MON_H__
#define __STK_MON_H__








typedef struct
{
    STKMON_STACK_ID_t            stack_mon_id;       

} struct_STKMON_STACK_CFG_t;
typedef struct_STKMON_STACK_CFG_t         STKMON_STACK_CFG_t;


typedef struct
{
    STKMON_STACK_ID_t           stack_mon_id;           

    uint8_t                     test_size;              

    STKMON_STACK_OBJECT_TYPE_t  *stack_begin;           
    STKMON_STACK_OBJECT_TYPE_t  *stack_end;             
    STKMON_STACK_OBJECT_TYPE_t  stack_test_pattern;     

    STKMON_STACK_OBJECT_TYPE_t  *stack_border_begin;    
    STKMON_STACK_OBJECT_TYPE_t  *stack_border_end;      

} struct_STKMON_STACK_DYN_CFG_t;
typedef struct_STKMON_STACK_DYN_CFG_t     STKMON_STACK_DYN_CFG_t;


typedef struct
{
    STKMON_STACK_OBJECT_TYPE_t  *stack_cur;             

} struct_STKMON_MONITORING_IMAGE_t;
typedef struct_STKMON_MONITORING_IMAGE_t             STKMON_MONITORING_IMAGE_t;


typedef struct
{
    uint32_t                    size_bytes;             
    uint32_t                    used_bytes;             
    uint8_t                     used_percent;           
    uint8_t                     overflow_flag;          
    uint8_t                     underflow_flag;         

    uint8_t                     tested_once;            

}  struct_STKMON_METRICS_t;
typedef struct_STKMON_METRICS_t           STKMON_METRICS_t;


typedef struct
{
    STKMON_STACK_ID_t           stack_mon_id;           

    STKMON_METRICS_t            stack_metrics;          
    STKMON_STACK_OBJECT_TYPE_t  *stack_max;             


} struct_STKMON_DISPLAY_t;
typedef struct_STKMON_DISPLAY_t           STKMON_DISPLAY_t;


typedef struct
{
    STKMON_STACK_DYN_CFG_t      stack_dyn_cfg[(STKMON_ENUM_MAX)];   
    STKMON_MONITORING_IMAGE_t   stack_image[(STKMON_ENUM_MAX)];     
    STKMON_DISPLAY_t            stack_display[(STKMON_ENUM_MAX)];   

} struct_STKMON_MONITORING_BAG_t;
typedef struct_STKMON_MONITORING_BAG_t    STKMON_MONITORING_BAG_t;




extern const STKMON_STACK_CFG_t             IUG_STACK_MON_STACK_CFG[];
extern const STKMON_STACK_CFG_t             IUG_STACK_MON_TASK_CFG[(STKMON_ENUM_MAX)];
extern const uint8_t                        ext_entries_STKMON_STACK_CFG;
extern STKMON_MONITORING_BAG_t              IUG_STACK_MONITORING;



void                    STKMON_monitoring_cyclic( void );
STKMON_STACK_ID_t       STKMON_monitoring_entry_deinit( const STKMON_STACK_ID_t        stack_id, 
                                                        STKMON_STACK_DYN_CFG_t * const p_entry_cfg );

STKMON_STACK_ID_t       STKMON_monitoring_entry_get_metrics_ptr( const STKMON_STACK_ID_t    stack_id, 
                                                                 STKMON_METRICS_t * * const p_stack_metrics );

STKMON_STACK_ID_t       STKMON_monitoring_entry_init( const STKMON_STACK_ID_t        stack_id, 
                                                      STKMON_STACK_DYN_CFG_t * const p_entry_cfg );

void                    STKMON_monitoring_init( void );



#endif 


