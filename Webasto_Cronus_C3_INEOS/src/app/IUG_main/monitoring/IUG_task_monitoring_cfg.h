#ifndef __TSKMON_CFG_H__
#define __TSKMON_CFG_H__












#define TSKMON_IDLE_DETAILED_EN

#ifdef TSKMON_IDLE_DETAILED_EN
    #define TSKMON_IDLE_DETAILED_IDLE_TASK_ID       (TSKMON_TASK_IDLE)


    #define TSKMON_IDLE_DETAILED_INTERVAL_TASK_ID   (TSKMON_TASK_1MS)

    #define TSKMON_IDLE_DETAILED_INTERVAL_WINDOW_MS   100u  


#endif 




#define TSKMON_CPU_CLOCK_HZ                         40000000uL                      

#define TSKMON_CPU_CLOCK_DIVIDER                    512uL

#if( (TSKMON_CPU_CLOCK_DIVIDER) == 4096 )
#define TSKMON_CPU_CLOCK_PRE_DIVIDER                100uL     
#elif( (TSKMON_CPU_CLOCK_DIVIDER) == 512 )
#define TSKMON_CPU_CLOCK_PRE_DIVIDER                 10uL     
#elif( (TSKMON_CPU_CLOCK_DIVIDER) == 16 )
#define TSKMON_CPU_CLOCK_PRE_DIVIDER                  1uL     
#else
#error "Set a ceratin 'TSKMON_CPU_CLOCK_PRE_DIVIDER' matching for the TSKMON_CPU_CLOCK_DIVIDER!"
#endif

#define TSKMON_RESOLUTION_HZ                        ((TSKMON_CPU_CLOCK_HZ)/(TSKMON_CPU_CLOCK_PRE_DIVIDER)) / (TSKMON_CPU_CLOCK_DIVIDER) 
#define TSKMON_RESOLUTION_FACTOR                   ( (1000L / (TSKMON_CPU_CLOCK_PRE_DIVIDER)) * 1000L )


#define TSKMON_CYLIC_PERIOD            20u 


#define TSKMON_OS_FREEZE()               vTaskSuspendAll();


#define TSKMON_OS_RELEASE()              xTaskResumeAll();



typedef uint16_t    TSKMON_TSK_DUTY_IMAGE_t;


typedef enum
{
    TSKMON_TASK_SYSTEM_1MS = 0u,    
    TSKMON_TASK_1MS     ,           
    TSKMON_TASK_10MS,               
    TSKMON_TASK_20MS,               
    TSKMON_TASK_100MS,              
    TSKMON_TASK_CUSTOM_xMS,         
    TSKMON_TASK_IDLE,               
    TSKMON_ENUM_MAX,                
    TSKMON_ENUM_FORCE_TYPE = 0x7F   

} enum_TSKMON_TASK_ID_t;
typedef enum_TSKMON_TASK_ID_t  TSKMON_TASK_ID_t;



inline uint16_t TSKMON_init_display_suppression_cnt( void ) { return (3000u / (TSKMON_CYLIC_PERIOD)); };


boolean    TSKMON_measurement_time_base_init( void );

void TSKMON_set_task_period(const TSKMON_TASK_ID_t task, const uint16_t period_ms);


inline TSKMON_TSK_DUTY_IMAGE_t IUG_task_monitoring_ticks( void ) { return (TAUB0CNT1); };



#if( ((TSKMON_CPU_CLOCK_HZ)          != 40000000 ) ||\
     ((TSKMON_CPU_CLOCK_DIVIDER)     !=      512 ) ||\
     ((TSKMON_CPU_CLOCK_PRE_DIVIDER) !=       10 ) ||\
     ((TSKMON_RESOLUTION_FACTOR)     !=   100000 ) )
    #error "Analyze with care the configuration for 'TSKMON_xxx'!"
#endif


#endif 


