#ifndef __TSK_MON_H__
#define __TSK_MON_H__



#ifdef TSKMON_DEV_SPOTPOINT_EN
#define TSKMON_DEV_SPOTPOINT___WAIT        0x0000u 
#define TSKMON_DEV_SPOTPOINT_CALVAL        0xFFFFu 
#endif





typedef uint8_t     TSKMON_TSK_ACTIVE_t;
typedef uint32_t    TSKMON_TSK_DUTY_IMAGE_CUM_t;
typedef uint16_t    TSKMON_TSK_INTERRUPTED_t;
typedef uint16_t    TSKMON_TSK_DUTY_DISPLY_us_t;

#ifdef TSKMON_TASK_LOAD_SIMULATION_EN
typedef uint32_t    TSKMON_TASK_LOAD_SIM_CNT_t;     
#endif


#define TSKMON_TSK_DUTY_DISPLY_us_MAX         0xFFFFu


typedef struct
{
    TSKMON_TASK_ID_t                task_mon_id;            
    uint16_t                        task_reference;         
    uint16_t                        *ptr_task_period_ms;    

} struct_TSKMON_TASK_CFG_t;
typedef struct_TSKMON_TASK_CFG_t    TSKMON_TASK_CFG_t;


typedef struct
{
    #ifdef TSKMON_DEV_SUPPORT_EN
    TSKMON_TASK_ID_t                task_mon_id;            
    #endif

    TSKMON_TSK_DUTY_IMAGE_t         task_duty_raw_entry;    
    TSKMON_TSK_DUTY_IMAGE_t         task_duty_raw;          

    TSKMON_TSK_DUTY_IMAGE_t         task_duty_raw_cal;      
    TSKMON_TSK_DUTY_IMAGE_t         task_duty_raw_min;      
    TSKMON_TSK_DUTY_IMAGE_t         task_duty_raw_max;      

    TSKMON_TSK_ACTIVE_t             task_active;                    
    TSKMON_TSK_INTERRUPTED_t        task_interrupted;               
    TSKMON_TSK_DUTY_IMAGE_t         task_duty_interruption_first;   
    TSKMON_TSK_DUTY_IMAGE_t         task_duty_interruption_raw;     

    uint8_t                         task_duty_interruption_first_flg;

    #ifdef TSKMON_DEV_SPOTPOINT_EN
    uint16_t                        task_duty_interruption_event;   
    #endif

} struct_TSKMON_MEASUREMENT_IMAGE_t;
typedef struct_TSKMON_MEASUREMENT_IMAGE_t             TSKMON_MEASUREMENT_IMAGE_t;


typedef struct
{
    TSKMON_TSK_DUTY_DISPLY_us_t     duty_min_us;            
    TSKMON_TSK_DUTY_DISPLY_us_t     duty_max_us;            
    TSKMON_TSK_DUTY_DISPLY_us_t     duty_avg_us;            
    uint8_t                         duty_avg_percent;       

    uint8_t                         tested_once;            

}  struct_TSKMON_METRICS_t;
typedef struct_TSKMON_METRICS_t            TSKMON_METRICS_t;


#ifdef TSKMON_IDLE_DETAILED_EN
typedef struct
{
    TSKMON_TSK_DUTY_IMAGE_CUM_t interval_raw_cum;           
    TSKMON_TSK_DUTY_IMAGE_CUM_t interval_raw_min;           
    TSKMON_TSK_DUTY_IMAGE_CUM_t interval_raw_max;           

    TSKMON_TSK_DUTY_IMAGE_CUM_t task_duty_interuption_raw_cal;

    uint8_t                     interval_wnd_cnt;
    uint8_t                     interval_raising_prgr;              
    TSKMON_TSK_DUTY_IMAGE_t     interval_raising_raw;               
    TSKMON_TSK_DUTY_IMAGE_t     interval_elapsing_correcture_raw;   

}  struct_TSKMON_IDLE_IMAGE_t;
typedef struct_TSKMON_IDLE_IMAGE_t         TSKMON_IDLE_IMAGE_t;


typedef struct
{
    uint32_t                        interval_window_cfg_us;                 
    uint32_t                        interval_duty_idle_min_us;              
    uint32_t                        interval_duty_idle_max_us;              
    uint32_t                        interval_duty_idle_avg_us;              
    uint8_t                         interval_duty_idle_avg_percent;         

    uint8_t                         interval_idle_task_duty_avg_percent;    

}  struct_TSKMON_IDLE_METRICS_t;
typedef struct_TSKMON_IDLE_METRICS_t         TSKMON_IDLE_METRICS_t;


typedef struct
{
    TSKMON_TASK_ID_t          task_mon_id;        

    TSKMON_IDLE_METRICS_t     task_metrics;       

} struct_TSKMON_IDLE_DISPLAY_t;
typedef struct_TSKMON_IDLE_DISPLAY_t           TSKMON_IDLE_DISPLAY_t;
#endif


typedef struct
{
    TSKMON_TASK_ID_t          task_mon_id;        

    TSKMON_METRICS_t   task_metrics;       

} struct_TSKMON_DISPLAY_t;
typedef struct_TSKMON_DISPLAY_t            TSKMON_DISPLAY_t;


typedef struct
{
    TSKMON_MEASUREMENT_IMAGE_t  task_image[(TSKMON_ENUM_MAX)];      
    TSKMON_DISPLAY_t            task_display[(TSKMON_ENUM_MAX)];    

    #ifdef TSKMON_IDLE_DETAILED_EN
    TSKMON_IDLE_IMAGE_t         idle_task_image;                    
    TSKMON_IDLE_DISPLAY_t       idle_task_display;                  
    #endif

    uint8_t                     task_total_duty_min_percent;                
    uint8_t                     task_total_duty_max_percent;                
    uint8_t                     task_total_duty_without_idle_avg_percent;   

    #ifdef TSKMON_IDLE_DETAILED_EN
    uint8_t                     task_total_duty_with_idle_avg_percent;      
    #endif

    uint8_t                     display_suppression_cnt;            

    uint8_t                     info_1_image_raw_tic_is_this_us;    

} struct_TSKMON_MONITORING_BAG_t;
typedef struct_TSKMON_MONITORING_BAG_t                TSKMON_MONITORING_BAG_t;



#ifdef TSKMON_PARTIAL_TASK_DUTY_MONITORING_EN
typedef struct
{
    struct
    { TSKMON_TSK_DUTY_IMAGE_t       raw_ety;            
      TSKMON_TSK_DUTY_IMAGE_t       raw_exi;            
      uint8_t                       calculate;          
    } raw_img;

    TSKMON_TSK_DUTY_DISPLY_us_t     duty_cur_us;        
    TSKMON_TSK_DUTY_DISPLY_us_t     duty_max_us;        

} struct_TSKMON_PARTIAL_TASK_DUTY_MONITORING_t;
typedef struct_TSKMON_PARTIAL_TASK_DUTY_MONITORING_t    TSKMON_PARTIAL_DUTY_MONITORING_t;
#endif



#ifdef TSKMON_TASK_LOAD_SIMULATION_EN
typedef struct
{
    TSKMON_TASK_ID_t                task_mon_id;        

    TSKMON_TASK_LOAD_SIM_CNT_t      task_load_sim_cnt_cfg;

    TSKMON_TASK_LOAD_SIM_CNT_t      task_load_sim_cnt;

} struct_TSKMON_TASK_LOAD_SIMULATION_t;
typedef struct_TSKMON_TASK_LOAD_SIMULATION_t    TSKMON_TASK_LOAD_SIM_t;
#endif


#ifdef TSKMON_DEV_SPOTPOINT_EN
typedef struct
{
    TSKMON_MEASUREMENT_IMAGE_t      task_image;                 
    TSKMON_DISPLAY_t                task_display;               

    #ifdef TSKMON_IDLE_DETAILED_EN
    TSKMON_IDLE_IMAGE_t             idle_task_image;            
    TSKMON_IDLE_DISPLAY_t           idle_task_display;          
    #endif

    TSKMON_TSK_DUTY_IMAGE_t         task_duty_cur_raw;          
    TSKMON_TSK_DUTY_DISPLY_us_t     task_duty_cur_us;           

    uint32_t                        cfg_TSKMON_RESOLUTION_____HZ;
    uint32_t                        cfg_TSKMON_RESOLUTION_FACTOR;

    TSKMON_TASK_ID_t                trigger_task_mon_id;        
    TSKMON_TSK_DUTY_IMAGE_t         trigger_task_duty_raw;      

} struct_TSKMON_DEV_SPOTPOINT_t;
typedef struct_TSKMON_DEV_SPOTPOINT_t                TSKMON_DEV_SPOTPOINT_t;
#endif



extern const TSKMON_TASK_CFG_t              IUG_TASK_MON_TASK_CFG[(TSKMON_ENUM_MAX)];
extern TSKMON_MONITORING_BAG_t              IUG_TASK_MONITORING;

#ifdef TSKMON_TASK_LOAD_SIMULATION_EN
extern TSKMON_TASK_LOAD_SIM_t               TSKMON_TASK_LOAD_SIM[(TSKMON_ENUM_MAX)];
#endif

#ifdef TSKMON_DEV_SPOTPOINT_EN
extern TSKMON_DEV_SPOTPOINT_t               TSKMON_DEV_SPOTPOINT;
#endif



void                TSKMON_monitoring_cyclic( void );
TSKMON_TASK_ID_t    TSKMON_entry_get_metrics_ptr( const TSKMON_TASK_ID_t     task_id, 
                                                  TSKMON_METRICS_t * * const p_task_metrics );

void                TSKMON_task_image_on_entry( const TSKMON_TASK_ID_t task_mon_id );
void                TSKMON_task_image_on_exit( const TSKMON_TASK_ID_t task_mon_id );
void                TSKMON_monitoring_init( void );
#ifdef TSKMON_PARTIAL_TASK_DUTY_MONITORING_EN
void                TSKMON_partial_duty_calculate( TSKMON_PARTIAL_DUTY_MONITORING_t * const part_duty_obj );
#endif


#define TASK_MONITORING_ENTRY( task_id )                    TSKMON_task_image_on_entry( (task_id) )


#define TASK_MONITORING_EXIT( task_id )                     TSKMON_task_image_on_exit( (task_id) )


#ifdef TSKMON_PARTIAL_TASK_DUTY_MONITORING_EN
    #define TASKMON_PARTIAL_DUTY_ENTRY( part_duty_obj )         { (part_duty_obj)->raw_img.raw_ety = IUG_task_monitoring_ticks(); }

    #define TASKMON_PARTIAL_DUTY_EXIT( part_duty_obj )          { (part_duty_obj)->raw_img.raw_exi = IUG_task_monitoring_ticks(); }

    #define TASKMON_PARTIAL_DUTY_CALC_TRG( part_duty_obj )      { (part_duty_obj)->raw_img.calculate = 1u; }

    #define TASKMON_PARTIAL_DUTY_CALCULATE( part_duty_obj )     TSKMON_partial_duty_calculate( (part_duty_obj) )

    #define TASKMON_PARTIAL_DUTY_CLEAN( part_duty_obj )         { (part_duty_obj)->duty_max_us = 0u; }

    #define TASKMON_PARTIAL_DUTY_START( part_duty_obj )         { (part_duty_obj)->raw_img.raw_ety = IUG_task_monitoring_ticks(); \
                                                                  (part_duty_obj)->raw_img.calculate = 1u; \
                                                                }

    #define TASKMON_PARTIAL_DUTY_STOP( part_duty_obj )          { (part_duty_obj)->raw_img.raw_exi = IUG_task_monitoring_ticks(); \
                                                                  TSKMON_partial_duty_calculate( (part_duty_obj) ); \
                                                                }

#endif


#if defined( TSKMON_IDLE_DETAILED_INTERVAL_WINDOW_START )
    #define TSKMON_IDLE_DETAILED_INTERVAL_WND_START()       TSKMON_IDLE_DETAILED_INTERVAL_WINDOW_START()
#else
    #define TSKMON_IDLE_DETAILED_INTERVAL_WND_START()
#endif
#if defined( TSKMON_IDLE_DETAILED_INTERVAL_WINDOW_STOP )
    #define TSKMON_IDLE_DETAILED_INTERVAL_WND_STOP()        TSKMON_IDLE_DETAILED_INTERVAL_WINDOW_STOP()
#else    
    #define TSKMON_IDLE_DETAILED_INTERVAL_WND_STOP()
#endif


#ifdef TSKMON_TASK_LOAD_SIMULATION_EN
#define TSKMON_TASK_LOAD_SIMULATION( task_id )  {   TSKMON_TASK_LOAD_SIM_t *ptr; \
                                                    ptr = &TSKMON_TASK_LOAD_SIM[(task_id)]; \
                                                    ptr->task_mon_id = (task_id); \
                                                    ptr->task_load_sim_cnt = ptr->task_load_sim_cnt_cfg; \
                                                    while( 0u != ptr->task_load_sim_cnt ) \
                                                    { \
                                                        ptr->task_load_sim_cnt--; \
                                                    } \
                                                }
#else
#define TSKMON_TASK_LOAD_SIMULATION( task_id )
#endif


inline void TSKMON_monitoring_get_metrics_all( uint8_t * const p_total_duty_min_perc,
                                               uint8_t * const p_total_duty_max_perc,
                                               uint8_t * const p_total_duty_avg_perc )
{
    *p_total_duty_min_perc = IUG_TASK_MONITORING.task_total_duty_min_percent;
    *p_total_duty_max_perc = IUG_TASK_MONITORING.task_total_duty_max_percent;
    *p_total_duty_avg_perc = IUG_TASK_MONITORING.task_total_duty_without_idle_avg_percent;
}


#ifdef TSKMON_TASK_LOAD_SIMULATION_EN
    #warning "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    #warning "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    #warning "Enable 'TSKMON_TASK_LOAD_SIMULATION_EN' for development/debugging only!!!"
    #warning "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    #warning "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
#endif

#if( (TSKMON_CYLIC_PERIOD) != 20u )
    #error "Value for 'TSKMON_CYLIC_PERIOD' must be 20!"
#endif

#ifdef TSKMON_IDLE_DETAILED_EN
    #if( ((TSKMON_IDLE_DETAILED_INTERVAL_WINDOW_MS)  <   20 ) ||\
         ((TSKMON_IDLE_DETAILED_INTERVAL_WINDOW_MS)  >  200 ) )
        #error "Analyze value of 'TSKMON_IDLE_DETAILED_INTERVAL_WINDOW_MS'!"
    #endif

    #if( defined( TSKMON_IDLE_DETAILED_INTERVAL_WINDOW_START ) ||\
         defined( TSKMON_IDLE_DETAILED_INTERVAL_WINDOW_STOP  ) )
        #warning "Enable instrumentation 'TSKMON_IDLE_DETAILED_INTERVAL_WINDOW_START/STOP' for development/debugging only!!!"
    #endif    
#endif


#endif 


