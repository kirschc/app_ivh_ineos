#ifndef __STKMON_CFG_H__
#define __STKMON_CFG_H__


#include "IUG_task_monitoring_include.h"


#define STKMON_STACK_OVER_UNDER_FLOW_RANGE          4u 



typedef uint32_t STKMON_STACK_OBJECT_TYPE_t;


typedef enum
{
    STKMON_STACK_SYSTEM_1MS           = 0u, 
    STKMON_STACK_APP_INIT_AND_EOL_1MS     , 
    STKMON_STACK_1MS,                       
    STKMON_STACK_10MS,                      
    STKMON_STACK_20MS,                      
    STKMON_STACK_100MS,                     
    STKMON_STACK_SHARED_PRIO_1MS,           
    STKMON_STACK_CUSTOM_1MS,                
    STKMON_STACK_IDLE,                      
    STKMON_STACK_CPU,                       
    STKMON_ENUM_MAX,                        
    STKMON_ENUM_FORCE_TYPE = 0x7F           

} enum_STKMON_STACK_ID_t;
typedef enum_STKMON_STACK_ID_t  STKMON_STACK_ID_t;




STKMON_STACK_ID_t STKMON_monitoring_os_task_entry_deinit( const STKMON_STACK_ID_t stack_id,
                                                          const uint16_t          cfg_min_stack_size,
                                                          const TaskHandle_t      TaskHandle );

STKMON_STACK_ID_t STKMON_monitoring_os_task_entry_init( const STKMON_STACK_ID_t stack_id,
                                                        const uint16_t          cfg_min_stack_size,
                                                        const TaskHandle_t      TaskHandle );




#endif 


