#ifndef __TSKMON_INCLUDE_H__
#define __TSKMON_INCLUDE_H__


#include <string.h>

#include "FreeRTOS.h" 
#include "task.h"

#include "target.h"            
#include "Tau.h"
#include "sfl_filter.h"

#include "IUG_task_monitoring_cfg.h"
#include "IUG_task_monitoring.h"


#if( defined( TSKMON_IDLE_DETAILED_INTERVAL_WINDOW_START ) ||\
     defined( TSKMON_IDLE_DETAILED_INTERVAL_WINDOW_STOP  ) )
    #include "IUG_include.h"
#endif


#endif 


