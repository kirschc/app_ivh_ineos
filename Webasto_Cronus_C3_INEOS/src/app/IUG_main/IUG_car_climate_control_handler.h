#ifndef _IUG_CAR_CLIMATE_CONTROL_HANDLER_H_
#define _IUG_CAR_CLIMATE_CONTROL_HANDLER_H_



typedef enum
{
    CAR_CLIMATE_CONTROL_NOT_CONFIGURED  = 0u,
    CAR_CLIMATE_CONTROL_ANALOG_OUT      = 1u,
    CAR_CLIMATE_CONTROL_PWM_OUT_ONLY    = 2u,
    CAR_CLIMATE_CONTROL_PWM_READ_OUT    = 3u,
    CAR_CLIMATE_CONTROL_LIN_BLOWER_CTRL = 4u,
    CAR_CLIMATE_CONTROL_LIN_GATEWAY     = 5u,
    CAR_CLIMATE_CONTROL_CAN_MEMBER      = 6u,
    CAR_CLIMATE_CONTROL_CAN_GATEWAY     = 7u,
    CAR_CLIMATE_CONTROL_MIXED_MODE      = 8u
} enum_CAR_CLIMATE_CONTROL_MODES;
_Static_assert(0u==(CAR_CLIMATE_CONTROL_NOT_CONFIGURED)  ,"Change encoding for webasto parameter #ID_CLIMATE_CONTROL too!!!");
_Static_assert(1u==(CAR_CLIMATE_CONTROL_ANALOG_OUT)      ,"Change encoding for webasto parameter #ID_CLIMATE_CONTROL too!!!");
_Static_assert(2u==(CAR_CLIMATE_CONTROL_PWM_OUT_ONLY)    ,"Change encoding for webasto parameter #ID_CLIMATE_CONTROL too!!!");
_Static_assert(3u==(CAR_CLIMATE_CONTROL_PWM_READ_OUT)    ,"Change encoding for webasto parameter #ID_CLIMATE_CONTROL too!!!");
_Static_assert(4u==(CAR_CLIMATE_CONTROL_LIN_BLOWER_CTRL) ,"Change encoding for webasto parameter #ID_CLIMATE_CONTROL too!!!");
_Static_assert(5u==(CAR_CLIMATE_CONTROL_LIN_GATEWAY)     ,"Change encoding for webasto parameter #ID_CLIMATE_CONTROL too!!!");
_Static_assert(6u==(CAR_CLIMATE_CONTROL_CAN_MEMBER)      ,"Change encoding for webasto parameter #ID_CLIMATE_CONTROL too!!!");
_Static_assert(7u==(CAR_CLIMATE_CONTROL_CAN_GATEWAY)     ,"Change encoding for webasto parameter #ID_CLIMATE_CONTROL too!!!");
_Static_assert(8u==(CAR_CLIMATE_CONTROL_MIXED_MODE)      ,"Change encoding for webasto parameter #ID_CLIMATE_CONTROL too!!!");

typedef enum
{
    CAR_CLIMATE_CONTROL_PWM_NOT_SET           = 0u,
    CAR_CLIMATE_CONTROL_PWM_HIGHSIDE_CHECK    = 1u,
    CAR_CLIMATE_CONTROL_PWM_LOWSIDE_CHECK     = 2u,
    CAR_CLIMATE_CONTROL_PWM_HIGHSIDE_DETECTED = 3u,
    CAR_CLIMATE_CONTROL_PWM_LOWSIDE_DETECTED  = 4u,
    CAR_CLIMATE_CONTROL_PWM_NOT_FOUND         = 5u
} enum_CAR_CLIMATE_CONTROL_PWM_MODE;
_Static_assert(0u==(CAR_CLIMATE_CONTROL_PWM_NOT_SET)           ,"Change encoding for webasto parameter #ID_CLIMATE_CTRL_DETECTED_PWM_MODE too!!!");
_Static_assert(1u==(CAR_CLIMATE_CONTROL_PWM_HIGHSIDE_CHECK)    ,"Change encoding for webasto parameter #ID_CLIMATE_CTRL_DETECTED_PWM_MODE too!!!");
_Static_assert(2u==(CAR_CLIMATE_CONTROL_PWM_LOWSIDE_CHECK)     ,"Change encoding for webasto parameter #ID_CLIMATE_CTRL_DETECTED_PWM_MODE too!!!");
_Static_assert(3u==(CAR_CLIMATE_CONTROL_PWM_HIGHSIDE_DETECTED) ,"Change encoding for webasto parameter #ID_CLIMATE_CTRL_DETECTED_PWM_MODE too!!!");
_Static_assert(4u==(CAR_CLIMATE_CONTROL_PWM_LOWSIDE_DETECTED)  ,"Change encoding for webasto parameter #ID_CLIMATE_CTRL_DETECTED_PWM_MODE too!!!");
_Static_assert(5u==(CAR_CLIMATE_CONTROL_PWM_NOT_FOUND)         ,"Change encoding for webasto parameter #ID_CLIMATE_CTRL_DETECTED_PWM_MODE too!!!");

typedef struct
{
    enum_API_PORT_MODE_X1_3_LIN1_PWM_IN_MODES  pin_x1_lin1_pwm_in_mode;
    enum_API_PORT_MODE_X1_4_LIN2_PWM_OUT_MODES pin_x1_lin2_pwm_out_mode;
    enum_API_PORT_MODE_X2_6_IO3_PWM_OUT_MODES  pin_x2_io3_pwm_out_mode;
} struct_car_climate_ports_config;

typedef struct
{
    const struct_car_climate_ports_config *ptr_ports_cfg;
    const struct_pwm_out_values           *ptr_pwm_out_values;
    const struct_pwm_values               *ptr_pwm_in_values;
    uint8_t                               status_car_climate;
    enum_CAR_CLIMATE_CONTROL_PWM_MODE     pwm_mode;
} struct_car_climate_control_handle;


void car_climate_control_handler_init(void);

void car_climate_control_handler_cyclic(void);

void car_climate_control_ctrl_car_climate(const uint8_t car_climate_ctrl_flg);

uint8_t car_climate_control_get_status_car_climate(void);

uint8_t car_climate_control_set_pwm_mode( const enum_CAR_CLIMATE_CONTROL_PWM_MODE pwm_mode );

static void car_climate_control_eval_ports_cfg(void);

void car_climate_control_disable_LIN1_transceiver(void);

static void car_climate_control_diag_webpar(void);


#endif


