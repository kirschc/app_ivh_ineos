#ifndef _IUG_DIAGNOSIS_DTC_FILTER_H_
#define _IUG_DIAGNOSIS_DTC_FILTER_H_







typedef enum
{
    DTCFLT_ST_NONE         = 0x00,
    DTCFLT_ST_ACTIVE       = 0x01,  
    DTCFLT_ST_02           = 0x02,
    DTCFLT_ST_04           = 0x04,
    DTCFLT_ST_08           = 0x08,
    DTCFLT_ST_10           = 0x10,
    DTCFLT_ST_20           = 0x20,
    DTCFLT_ST_40           = 0x40,
    DTCFLT_ST_80           = 0x80u,
    DTCFLT_ST_ENUM_MAX,               
    DTCFLT_ST_ENUM_FORCE_TYPE = 0x7F  

} enum_IUG_DIAG_DTC_FILTER_STATUS_t;
typedef enum_IUG_DIAG_DTC_FILTER_STATUS_t   IUG_DIAG_DTC_FILTER_STATUS_t;



typedef enum
{
    DTCFLT_FLG_NONE                    = 0x00u,
    DTCFLT_FLG_FILTER_ACTIVE           = 0x01u, 
    DTCFLT_FLG_FILTER_INACTIVE         = 0x02u, 
    DTCFLT_FLG_04                      = 0x04u,
    DTCFLT_FLG_08                      = 0x08u,
    DTCFLT_FLG_10                      = 0x10u,
    DTCFLT_FLG_20                      = 0x20u,
    DTCFLT_FLG_UPDATE_WHEN_FILTERED    = 0x40u, 
    DTCFLT_FLG_80                      = 0x80u,
    DTCFLT_FLG_ENUM_MAX,                   
    DTCFLT_FLG_ENUM_FORCE_TYPE     = 0x7F  

} enum_IUG_DIAG_DTC_FILTER_FLAGS_t;
typedef enum_IUG_DIAG_DTC_FILTER_FLAGS_t    IUG_DIAG_DTC_FILTER_FLAGS_t;


typedef uint16_t        IUG_DIAG_DTC_FILTER_CNT_t;


typedef struct
{
    DTC_TABLE_IDX_t                 dtc_index;

    IUG_DIAG_DTC_FILTER_FLAGS_t     flags;

    IUG_DIAG_DTC_FILTER_CNT_t       filter_cnt_active;
    IUG_DIAG_DTC_FILTER_CNT_t       filter_cnt_inactive;

} IUG_DIAG_DTC_FILTER_CFG_t;


typedef struct
{
    DTC_TABLE_IDX_t                 dtc_index;

    IUG_DIAG_DTC_FILTER_STATUS_t    dtc_status;
    IUG_DIAG_DTC_FILTER_CNT_t       filter_cnt_act_pas;     

} IUG_DIAG_DTC_FILTER_CTL_t;


boolean IUG_sfl_dtc_filter_init_dtc_related( const enum_DTC_TABLE_IDX dtc_index, const enum_DTC_STATUS dtc_sta );


#endif 


