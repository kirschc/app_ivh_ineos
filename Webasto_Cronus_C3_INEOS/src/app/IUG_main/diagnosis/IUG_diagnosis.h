#ifndef __IUG_DIAGNOSIS_H_
#define __IUG_DIAGNOSIS_H_

#ifndef DIAGNOSTIC_TROUBLE_CODE_CFG_H
#include "sfl_diagnostic_trouble_code_cfg.h"
#endif
#ifndef SFL_DIAGNOSTIC_TROUBLE_CODE_H
#include "sfl_diagnostic_trouble_code.h"
#endif
#ifndef _IUG_ERROR_HANDLER_SYMPTOM_CONSTANT_H_
#include "IUG_error_handler_symptom__constant.h"
#endif


#ifdef IUG_DEVELOPMENT_ACTIVE
#endif


#define IUG_DIAG_CAN_SEND_CYCLE_MS                                            500u   

#define IUG_DIAG_STIM_VAL_1_CAN_ID                                            0x60Au 

#define IUG_DIAG_STIM_SIM_VAL_1_ENV_ACT(data_byte_0)                                   ( ( (data_byte_0) & 0x02u ) >> 1u )      
#define IUG_DIAG_STIM_SIM_VAL_1_CFG_CP_ACT_AT_AH(data_byte_1)                          ( ( (data_byte_1) ) )                    
#define IUG_DIAG_STIM_SIM_VAL_1_EXT_TEMP(data_byte_4, data_byte_5, data_byte_6)     (  ( ( (data_byte_4) & 0xFCu ) >> 2u  ) \
                                                                                     | ( ( (data_byte_5)         ) << 6u  ) \
                                                                                     | ( ( (data_byte_6) & 0x03u ) << 14u )   ) 

#define IUG_DIAG_STIM_VAL_2_CAN_ID                                            0x60Bu 

#define IUG_DIAG_STIM_SIM_VAL_2_BMP_ACT(data_byte_0)                                     ( (data_byte_0) & 0x01u )              
#define IUG_DIAG_STIM_SIM_VAL_2_BMP_AIR_PRESSURE(data_byte_1, data_byte_2)          (  ( ( (data_byte_1)        )        ) \
                                                                                     |   ( (data_byte_2) & 0x07u )  << 8u     ) 
#define IUG_DIAG_STIM_SIM_VAL_2_BMP_TEMP(data_byte_2, data_byte_3)                  (  ( ( (data_byte_2) & 0xF8u )  >> 3u     ) \
                                                                                     |   ( (data_byte_3) & 0x07u )  << 5u     ) 

#define IUG_DIAGMTX0_IMPLEMENTED
#define IUG_DIAGMTX1_IMPLEMENTED
#define IUG_DIAGMTX2_IMPLEMENTED
#define IUG_DIAGMTX3_IMPLEMENTED
#define IUG_DIAGMTX4_IMPLEMENTED
#define IUG_DIAGMTX5_IMPLEMENTED
#define IUG_DIAGMTX6_IMPLEMENTED


#ifdef IUG_DIAGMTX0_IMPLEMENTED

    #define IUG_DIAGMTX0_NUM_ENTRIES                 8u 

    #define IUG_DIAGMTX0_ENTRY_IUG_STATUS            0u
    #define IUG_DIAGMTX0_ENTRY_HG1_STATUS            1u 
    #define IUG_DIAGMTX0_ENTRY_HG2_STATUS            2u 
    #define IUG_DIAGMTX0_ENTRY_HG1_STATE             3u 
    #define IUG_DIAGMTX0_ENTRY_HG2_STATE             4u 
    #define IUG_DIAGMTX0_ENTRY_HG1_COOL_TEMP         5u 
    #define IUG_DIAGMTX0_ENTRY_HG2_COOL_TEMP         6u 
    #define IUG_DIAGMTX0_ENTRY_DEV                   7u 


    #define IUG_DIAGMTX0_STA_IUG_NONE                0x00u
    #define IUG_DIAGMTX0_STA_IUG_FAILURE             0x01u      
    #define IUG_DIAGMTX0_STA_IUG_NO_WBUS_MEMBERS     0x02u      
    #define IUG_DIAGMTX0_STA_IUG_04                  0x04u      
    #define IUG_DIAGMTX0_STA_IUG_08                  0x08u      
    #define IUG_DIAGMTX0_STA_IUG_10                  0x10u      
    #define IUG_DIAGMTX0_STA_IUG_20                  0x20u      
    #define IUG_DIAGMTX0_STA_IUG_40                  0x40u      
    #define IUG_DIAGMTX0_STA_IUG_CLAMP_15_ON         0x80u      


    #define IUG_DIAGMTX0_STA_HGn_NONE                0x00u
    #define IUG_DIAGMTX0_STA_HGn_FAILURE             0x01u
    #define IUG_DIAGMTX0_STA_HGn_WBUS_COM_ERR        0x02u
    #define IUG_DIAGMTX0_STA_HGn_STFL                0x04u
    #define IUG_DIAGMTX0_STA_HGn_HGVP                0x08u
    #define IUG_DIAGMTX0_STA_HGn_UEHFL               0x10u
    #define IUG_DIAGMTX0_STA_HGn_20                  0x20u
    #define IUG_DIAGMTX0_STA_HGn_40                  0x40u
    #define IUG_DIAGMTX0_STA_HGn_80                  0x80u



    extern uint8_t IUG_DIAG_MATRIX0[(IUG_DIAGMTX0_NUM_ENTRIES)];

#endif 


#ifdef IUG_DIAGMTX1_IMPLEMENTED

    #define IUG_DIAGMTX1_NUM_ENTRIES                 8u 

    #define IUG_DIAGMTX1_ENTRY_STATUS_FLAG           0u
    #define IUG_DIAGMTX1_ENTRY_STATE_IUG             1u
    #define IUG_DIAGMTX1_ENTRY_STATE_AIR             2u
    #define IUG_DIAGMTX1_ENTRY_STATE_WAT             3u
    #define IUG_DIAGMTX1_ENTRY_DURATION_AIR          4u
    #define IUG_DIAGMTX1_ENTRY_DURATION_WAT          5u
    #define IUG_DIAGMTX1_ENTRY_6                     6u
    #define IUG_DIAGMTX1_ENTRY_7                     7u


    #define IUG_DIAGMTX1_STAFLG_NONE                 0x00u
    #define IUG_DIAGMTX1_STAFLG_HG1_AVAILABLE        0x0Fu      
    #define IUG_DIAGMTX1_STAFLG_HG2_AVAILABLE        0xF0u      


    #define IUG_DIAGMTX1_STAIUG_NONE                 0x00u
    #define IUG_DIAGMTX1_STAIUG_IUG_STATE            0x0Fu      
    #define IUG_DIAGMTX1_STAFLG_FZG_STATE            0x10u      
    #define IUG_DIAGMTX1_STAFLG_CAR_CLIMATE          0x20u      
    #define IUG_DIAGMTX1_STAIUG_40                   0x40u      
    #define IUG_DIAGMTX1_STAIUG_80                   0x80u      


    #define IUG_DIAGMTX1_STAAIR_NONE                 0x00u
    #define IUG_DIAGMTX1_STAAIR_IUG_STATE_AIR        0x0Fu      
    #define IUG_DIAGMTX1_STAAIR_HG_STATE_AIR         0xF0u      


    #define IUG_DIAGMTX1_STAWAT_NONE                 0x00u
    #define IUG_DIAGMTX1_STAWAT_IUG_STATE_WAT        0x0Fu      
    #define IUG_DIAGMTX1_STAWAT_HG_STATE_WAT         0xF0u      


    #define IUG_DIAGMTX1_STAAIR_DURATION_AIR         0xFFu      


    #define IUG_DIAGMTX1_STAWAT_DURATION_WAT         0xFFu      

    extern uint8_t IUG_DIAG_MATRIX1[(IUG_DIAGMTX1_NUM_ENTRIES)];

#endif 


#ifdef IUG_DIAGMTX2_IMPLEMENTED

    #define IUG_DIAGMTX2_NUM_ENTRIES                   8u 

    #define IUG_DIAGMTX2_ENTRY_HG_AIR_TERM_FLAG_BYTE1  0u
    #define IUG_DIAGMTX2_ENTRY_HG_AIR_TERM_FLAG_BYTE2  1u
    #define IUG_DIAGMTX2_ENTRY_HG_AIR_TERM_FLAG_BYTE3  2u
    #define IUG_DIAGMTX2_ENTRY_HG_AIR_TERM_FLAG_BYTE4  3u
    #define IUG_DIAGMTX2_ENTRY_HG_WAT_TERM_FLAG_BYTE1  4u
    #define IUG_DIAGMTX2_ENTRY_HG_WAT_TERM_FLAG_BYTE2  5u
    #define IUG_DIAGMTX2_ENTRY_HG_WAT_TERM_FLAG_BYTE3  6u
    #define IUG_DIAGMTX2_ENTRY_HG_WAT_TERM_FLAG_BYTE4  7u

    extern uint8_t IUG_DIAG_MATRIX2[(IUG_DIAGMTX2_NUM_ENTRIES)];

#endif 


#ifdef IUG_DIAGMTX3_IMPLEMENTED

    #define IUG_DIAGMTX3_NUM_ENTRIES               8u 

    #define IUG_DIAGMTX3_ENTRY_IUG_STAT_RTC_DATA1  0u
    #define IUG_DIAGMTX3_ENTRY_IUG_STAT_RTC_DATA2  1u
    #define IUG_DIAGMTX3_ENTRY_IUG_STAT_RTC_DATA3  2u
    #define IUG_DIAGMTX3_ENTRY_IUG_STAT_RTC_DATA4  3u
    #define IUG_DIAGMTX3_ENTRY_IUG_STAT_RTC_DATA5  4u  
    #define IUG_DIAGMTX3_ENTRY_IUG_STAT_PERIPHERAL 5u
    #define IUG_DIAGMTX3_ENTRY_IUG_CONFIG_FLAG     6u
    #define IUG_DIAGMTX3_ENTRY_IUG_CONFIG_LEVEL    7u


    #define IUG_DIAGMTX3_IUG_STAT_NONE                                               0x00u
    #define IUG_DIAGMTX3_IUG_STAT_RTC_SEC_MIN(sec_val, min_val)                      ( (sec_val   & 0x3Fu)      ) | \
                                                                                     ( (min_val   & 0x3Fu) << 6u)       
    #define IUG_DIAGMTX3_IUG_STAT_RTC_MIN_HOUR(min_val, hour_val)                    ( (min_val   & 0x3Fu) >> 2u) | \
                                                                                     ( (hour_val  & 0x1Fu) << 4u)       
    #define IUG_DIAGMTX3_IUG_STAT_RTC_HOUR_DAY_MONTH(hour_val, day_val, month_val)   ( (hour_val  & 0x1Fu) >> 4u) | \
                                                                                     ( (day_val   & 0x1Fu) << 1u) | \
                                                                                     ( (month_val & 0x0Fu) << 6u)       
    #define IUG_DIAGMTX3_IUG_STAT_RTC_MONTH_YEAR(month_val, year_val)                ( (month_val & 0x0Fu) >> 2u) | \
                                                                                     ( (year_val  & 0x7Fu) << 2u)       
    #define IUG_DIAGMTX3_IUG_STAT_RTC_YEAR(year_val)                                 ( (year_val  & 0x7Fu) >> 5u)       

    #define IUG_DIAGMTX3_IUG_STAT_PERIPHERAL_OBD_PROTOCOL                            0xF0u                              


    #define IUG_DIAGMTX3_IUG_STAT_PERIPHERAL_TEMP_SENS_WBUS_BE                       0x01u        
    #define IUG_DIAGMTX3_IUG_STAT_PERIPHERAL_TEMP_SENS_WBUS_HG                       0x02u        
    #define IUG_DIAGMTX3_IUG_STAT_PERIPHERAL_TEMP_SENS_BMP                           0x04u        
    #define IUG_DIAGMTX3_IUG_STAT_PERIPHERAL_TEMP_SENS_CMD_BUT                       0x08u        
    #define IUG_DIAGMTX3_IUG_STAT_PERIPHERAL_TEMP_SENS_10                            0x10u        


    #define IUG_DIAGMTX3_CFGFLG_NONE                       0x00u
    #define IUG_DIAGMTX3_CFGFLG_CONFIG_DRV_HEATING         0x01u                                  
    #define IUG_DIAGMTX3_CFGFLG_SWITCH_DRV_HEATING         0x02u                                  
    #define IUG_DIAGMTX3_CFGFLG_UI_AUTOMATIC_AUX_HEATING   0x04u                                  
    #define IUG_DIAGMTX3_CFGFLG_CFG_HEAT_VENT_BTS          0x08u                                  
    #define IUG_DIAGMTX3_CFGFLG_CONFIG_BUTTON_BTS          0xF0u                                  


    #define IUG_DIAGMTX3_CFGLVL_NONE                 0x00u
    #define IUG_DIAGMTX3_CFGLVL_LEVEL_BTS_AIR        0x0Fu                                  
    #define IUG_DIAGMTX3_CFGLVL_MODE_BTS_AIR         0xF0u                                  

    extern uint8_t IUG_DIAG_MATRIX3[(IUG_DIAGMTX3_NUM_ENTRIES)];

#endif 


#ifdef IUG_DIAGMTX4_IMPLEMENTED

    #define IUG_DIAGMTX4_NUM_ENTRIES                                8u 

    #define IUG_DIAGMTX4_IUG_STAT_PERIPHERAL_TEMP_SENS_WBUS_BE_VAL  0u
    #define IUG_DIAGMTX4_IUG_STAT_PERIPHERAL_TEMP_SENS_WBUS_HG_VAL  1u
    #define IUG_DIAGMTX4_IUG_STAT_PERIPHERAL_TEMP_SENS_BMP_VAL      2u
    #define IUG_DIAGMTX4_IUG_STAT_PERIPHERAL_TEMP_SENS_CMD_BUT_VAL  3u
    #define IUG_DIAGMTX4_IUG_STAT_PERIPHERAL_TEMP_SENS_4            4u
    #define IUG_DIAGMTX4_IUG_STAT_PERIPHERAL_AIR_PRESSURE_VAL       5u
    #define IUG_DIAGMTX4_IUG_STAT_PERIPHERAL_AIR_PRESSURE_VAL2      6u
    #define IUG_DIAGMTX4_ENTRY_7                                    7u


    extern uint8_t IUG_DIAG_MATRIX4[(IUG_DIAGMTX4_NUM_ENTRIES)];

#endif 


#ifdef IUG_DIAGMTX5_IMPLEMENTED

    #define IUG_DIAGMTX5_NUM_ENTRIES                     8u 

    #define IUG_DIAGMTX5_IUG_STAT_BASE_SW_VERSION_MAJOR  0u
    #define IUG_DIAGMTX5_IUG_STAT_BASE_SW_VERSION_MINOR  1u
    #define IUG_DIAGMTX5_IUG_STAT_BASE_SW_VERSION_BUILD  2u
    #define IUG_DIAGMTX5_IUG_STAT_HW_VERSION             3u
    #define IUG_DIAGMTX5_IUG_STAT_HW_REVISION            4u
    #define IUG_DIAGMTX5_ENTRY_5                         5u
    #define IUG_DIAGMTX5_ENTRY_6                         6u
    #define IUG_DIAGMTX5_ENTRY_7                         7u


    extern uint8_t IUG_DIAG_MATRIX5[(IUG_DIAGMTX5_NUM_ENTRIES)];

#endif 


#ifdef IUG_DIAGMTX6_IMPLEMENTED

    #define IUG_DIAGMTX6_NUM_ENTRIES            8u 

    #define IUG_DIAGMTX6_ENTRY_HG_AIR_SETPOINT  0u
    #define IUG_DIAGMTX6_ENTRY_HG_WAT_SETPOINT  1u
    #define IUG_DIAGMTX6_ENTRY_HG_AIR_NAK_CODE  2u
    #define IUG_DIAGMTX6_ENTRY_HG_WAT_NAK_CODE  3u
    #define IUG_DIAGMTX6_ENTRY_4                4u
    #define IUG_DIAGMTX6_ENTRY_5                5u
    #define IUG_DIAGMTX6_ENTRY_6                6u
    #define IUG_DIAGMTX6_ENTRY_7                7u


    extern uint8_t IUG_DIAG_MATRIX6[(IUG_DIAGMTX6_NUM_ENTRIES)];

#endif 


#define IUG_DIAG_FRZ_FRM_DESCR_CFG \
{ \
    { (IUG_DTC_FRZ_FRM_ID_FZG_STATE)        },\
    { (IUG_DTC_FRZ_FRM_ID_IUG_STATE_HG1)    },\
    { (IUG_DTC_FRZ_FRM_ID_IUG_STATE_HG2)    },\
    { (IUG_DTC_FRZ_FRM_ID_HG_STATE_HG1)     },\
    { (IUG_DTC_FRZ_FRM_ID_HG_STATE_HG2)     },\
    { (IUG_DTC_FRZ_FRM_ID_AIR_PRESSURE_VALUE)},\
    { (IUG_DTC_FRZ_FRM_ID_VAL_TIME_STAMP)   },\
    { (IUG_DTC_FRZ_FRM_ID_VAL_IO2)          },\
    { (IUG_DTC_FRZ_FRM_ID_VAL_IO3)          },\
    { (IUG_DTC_FRZ_FRM_ID_DTC_SYMPTOM)      },\
    { (IUG_DTC_FRZ_FRM_ID_VAL_KL15)         },\
    { (IUG_DTC_FRZ_FRM_ID_VAL_KL30)         },\
    { (IUG_DTC_FRZ_FRM_ID_VAL_BE_VOLTAGE)   },\
    { (IUG_DTC_FRZ_FRM_ID_VAL_BE_CURRENT)   } \
}


#define IUG_DTC_FRZ_FRM_DESCR_CFG \
{ \
    { (IUG_DTC_FRZ_FRM_ID_FZG_STATE),           (sizeof(uint8_t)) },\
    { (IUG_DTC_FRZ_FRM_ID_IUG_STATE_HG1),       (sizeof(uint8_t)) },\
    { (IUG_DTC_FRZ_FRM_ID_IUG_STATE_HG2),       (sizeof(uint8_t)) },\
    { (IUG_DTC_FRZ_FRM_ID_HG_STATE_HG1),        (sizeof(uint8_t)) },\
    { (IUG_DTC_FRZ_FRM_ID_HG_STATE_HG2),        (sizeof(uint8_t)) },\
    { (IUG_DTC_FRZ_FRM_ID_AIR_PRESSURE_VALUE),  (sizeof(uint16_t)) },\
    { (IUG_DTC_FRZ_FRM_ID_VAL_TIME_STAMP),      (6u) },\
    { (IUG_DTC_FRZ_FRM_ID_VAL_IO2),             (sizeof(uint16_t)) },\
    { (IUG_DTC_FRZ_FRM_ID_VAL_IO3),             (sizeof(uint16_t)) },\
    { (IUG_DTC_FRZ_FRM_ID_DTC_SYMPTOM),         (sizeof(uint16_t)) },\
    { (IUG_DTC_FRZ_FRM_ID_VAL_KL15),            (sizeof(uint16_t)) },\
    { (IUG_DTC_FRZ_FRM_ID_VAL_KL30),            (sizeof(uint16_t)) },\
    { (IUG_DTC_FRZ_FRM_ID_VAL_BE_VOLTAGE),      (sizeof(uint16_t)) },\
    { (IUG_DTC_FRZ_FRM_ID_VAL_BE_CURRENT),      (sizeof(uint16_t)) } \
}


typedef struct
{
    uint8_t     frz_frm_id;     

} struct_IUG_DIAG_FREEZE_FRAME_DESCRIPTION_t;

typedef struct
{
    uint8_t     sim_env_act;
    uint8_t     sim_cp_act_at_ah;
    int16_t     sim_ext_temp;
} struct_DIAG_SIM_DATA_VAL_1;

typedef struct
{
    uint8_t     sim_bmp_act;
    uint16_t    sim_bmp_air_pressure;
    uint8_t     sim_bmp_temp;
} struct_DIAG_SIM_DATA_VAL_2;

typedef struct
{
    struct_DIAG_SIM_DATA_VAL_1 sim_data_val_1;
    struct_DIAG_SIM_DATA_VAL_2 sim_data_val_2;
} struct_DIAG_SIM_DATA;

#ifdef IUG_DIAG_DTC_DEV_SUPPORT_EN
typedef struct
{
    DTC_TABLE_IDX_t          dtc_index_trg;      
    DTC_TABLE_IDX_t          dtc_his[(IUG_DIAG_DTC_DEV_SUPPORT_EN)];  

} struct_IUG_DIAG_DTC_DEV_SUPPORT_t;
#endif

extern struct_IUG_DIAG_FREEZE_FRAME_DESCRIPTION_t  mgl_IUG_DIAG_FRZ_FRM_DESCR[];
extern const uint8_t                        ext_iug_entries_IUG_DIAG_FRZ_FRM_DESCR;
extern struct_DIAG_SIM_DATA                 ext_diag_sim_data;
#ifdef IUG_DIAG_DTC_DEV_SUPPORT_EN
extern struct_IUG_DIAG_DTC_DEV_SUPPORT_t    IUG_DIAG_DTC_DEV_SUPPORT;
#endif

void                    IUG_diag_cyclic(void);

void IUG_get_diag_stim_data(const struct_hal_can_frame *const ptr_can_msg);

void                    IUG_sfl_dtc_check_cfg(void);
boolean                 IUG_sfl_dtc_filter_cyclic( void );
enum_DTC_STATUS         IUG_sfl_dtc_filter_event( const DTC_TABLE_IDX_t dtc_index, const enum_DTC_STATUS dtc_sta );
uint16_t                IUG_sfl_dtc_filter_get_dtc_entry_idx( const DTC_TABLE_IDX_t dtc_index );
boolean                 IUG_sfl_dtc_filter_init( void );
uint8_t                 IUG_sfl_dtc_filter_inhibition_service( const DTC_TABLE_IDX_t dtc_index, const uint8_t mode );
uint8_t                 IUG_sfl_dtc_get_err_freq_cnt_from_table( const DTC_TABLE_IDX_t dtc_index );
enum_DTC_RETURN_VALUE   IUG_sfl_dtc_set_dtc_and_freeze_frame( const DTC_TABLE_IDX_t dtc_index, const IUG_err_hdl_dtc_sypmtom_carrier_t dtc_sym_value );
enum_DTC_RETURN_VALUE   IUG_sfl_dtc_reset_dtc_and_freeze_frame( const DTC_TABLE_IDX_t dtc_index, const IUG_err_hdl_dtc_sypmtom_carrier_t dtc_sym_value );
enum_DTC_RETURN_VALUE IUG_sfl_dtc_reset_dtc_and_freeze_frame_immediately( const DTC_TABLE_IDX_t dtc_index, const IUG_err_hdl_dtc_sypmtom_carrier_t dtc_sym_value );


enum_DTC_RETURN_VALUE IUG_sfl_dtc_reset_dtc_and_freeze_frame_by_ary( const DTC_TABLE_IDX_t *const ptr_ary_dtcs );

uint8_t IUG_sfl_dtc_is_dtc_active_by_ary( const DTC_TABLE_IDX_t *const ptr_ary_dtcs );


#endif 


