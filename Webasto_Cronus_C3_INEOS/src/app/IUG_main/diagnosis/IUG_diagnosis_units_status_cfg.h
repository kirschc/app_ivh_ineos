#ifndef _IUG_DIAGNOSIS_UNITS_STATUS_CFG_H_
#define _IUG_DIAGNOSIS_UNITS_STATUS_CFG_H_




#define IUG_UNITS_STA_SEL_MUX_20MS_CFG \
{\
    (IUG_UNITS_STASELMUX_CYCLIC_20ms) \
}


#define IUG_UNITS_STA_SEL_MUX_100MS_CFG \
{\
    (IUG_UNITS_STASELMUX_CYCLIC_100ms)              ,\
    (IUG_UNITS_STASELMUX_IUG_STATUS)                ,\
    (IUG_UNITS_STASELMUX_IUG_MISC_VALUE)             \
}

#define IUG_UNITS_STA_SEL_MUX_1S_CFG \
{\
    (IUG_UNITS_STASELMUX_CYCLIC_1s)                 ,\
    (IUG_UNITS_STASELMUX_SW_HW_VERSION)             ,\
    (IUG_UNITS_STASELMUX_IUG_RTC_STATUS)            ,\
    (IUG_UNITS_STASELMUX_IUG_MISC_CFG_STAT)         ,\
    (IUG_UNITS_STASELMUX_CTL_1_7_BOUND_TO_TGT_X)    ,\
    (IUG_UNITS_STASELMUX_CTL_8_14_BOUND_TO_TGT_X)   ,\
    (IUG_UNITS_STASELMUX_CTL_15_21_BOUND_TO_TGT_X)   \
}


#define IUG_UNITS_STA_SEL_MUX_CTL_FAST_CFG \
{\
    (IUG_UNITS_STASELMUX_CONTROL_1_1)       ,\
    (IUG_UNITS_STASELMUX_CONTROL_2_1)       ,\
    (IUG_UNITS_STASELMUX_CONTROL_3_1)       ,\
    (IUG_UNITS_STASELMUX_CONTROL_4_1)       ,\
    (IUG_UNITS_STASELMUX_CONTROL_5_1)       ,\
    (IUG_UNITS_STASELMUX_CONTROL_6_1)       ,\
    (IUG_UNITS_STASELMUX_CONTROL_7_1)       ,\
    (IUG_UNITS_STASELMUX_CONTROL_8_1)       ,\
    (IUG_UNITS_STASELMUX_CONTROL_9_1)       ,\
    (IUG_UNITS_STASELMUX_CONTROL_10_1)      ,\
    (IUG_UNITS_STASELMUX_CONTROL_11_1)      ,\
    (IUG_UNITS_STASELMUX_CONTROL_12_1)      ,\
    (IUG_UNITS_STASELMUX_CONTROL_13_1)      ,\
    (IUG_UNITS_STASELMUX_CONTROL_14_1)      ,\
    (IUG_UNITS_STASELMUX_CONTROL_15_1)      ,\
    (IUG_UNITS_STASELMUX_CONTROL_16_1)       \
}


#define IUG_UNITS_STA_SEL_MUX_CTL_SLOW_CFG \
{\
    (IUG_UNITS_STASELMUX_CONTROL_1_2)       ,\
    (IUG_UNITS_STASELMUX_CONTROL_2_2)       ,\
    (IUG_UNITS_STASELMUX_CONTROL_3_2)       ,\
    (IUG_UNITS_STASELMUX_CONTROL_4_2)       ,\
    (IUG_UNITS_STASELMUX_CONTROL_5_2)       ,\
    (IUG_UNITS_STASELMUX_CONTROL_6_2)       ,\
    (IUG_UNITS_STASELMUX_CONTROL_7_2)       ,\
    (IUG_UNITS_STASELMUX_CONTROL_8_2)       ,\
    (IUG_UNITS_STASELMUX_CONTROL_9_2)       ,\
    (IUG_UNITS_STASELMUX_CONTROL_10_2)      ,\
    (IUG_UNITS_STASELMUX_CONTROL_11_2)      ,\
    (IUG_UNITS_STASELMUX_CONTROL_12_2)      ,\
    (IUG_UNITS_STASELMUX_CONTROL_13_2)      ,\
    (IUG_UNITS_STASELMUX_CONTROL_14_2)      ,\
    (IUG_UNITS_STASELMUX_CONTROL_15_2)      ,\
    (IUG_UNITS_STASELMUX_CONTROL_16_2)       \
}


#define IUG_UNITS_STA_SEL_MUX_HTR_FAST_CFG \
{\
    (IUG_UNITS_STASELMUX_HEATER_1_1)        ,\
    (IUG_UNITS_STASELMUX_HEATER_2_1)        ,\
    (IUG_UNITS_STASELMUX_HEATER_3_1)        ,\
    (IUG_UNITS_STASELMUX_HEATER_4_1)        ,\
    (IUG_UNITS_STASELMUX_HEATER_5_1)        ,\
    (IUG_UNITS_STASELMUX_HEATER_6_1)        ,\
    (IUG_UNITS_STASELMUX_HEATER_7_1)        ,\
    (IUG_UNITS_STASELMUX_HEATER_8_1)         \
}


#define IUG_UNITS_STA_SEL_MUX_HTR_SLOW_CFG \
{\
    (IUG_UNITS_STASELMUX_HEATER_1_2)        ,\
    (IUG_UNITS_STASELMUX_HEATER_1_3)        ,\
    (IUG_UNITS_STASELMUX_HEATER_1_4)        ,\
    (IUG_UNITS_STASELMUX_HEATER_2_2)        ,\
    (IUG_UNITS_STASELMUX_HEATER_2_3)        ,\
    (IUG_UNITS_STASELMUX_HEATER_2_4)        ,\
    (IUG_UNITS_STASELMUX_HEATER_3_2)        ,\
    (IUG_UNITS_STASELMUX_HEATER_3_3)        ,\
    (IUG_UNITS_STASELMUX_HEATER_3_4)        ,\
    (IUG_UNITS_STASELMUX_HEATER_4_2)        ,\
    (IUG_UNITS_STASELMUX_HEATER_4_3)        ,\
    (IUG_UNITS_STASELMUX_HEATER_4_4)        ,\
    (IUG_UNITS_STASELMUX_HEATER_5_2)        ,\
    (IUG_UNITS_STASELMUX_HEATER_5_3)        ,\
    (IUG_UNITS_STASELMUX_HEATER_5_4)        ,\
    (IUG_UNITS_STASELMUX_HEATER_6_2)        ,\
    (IUG_UNITS_STASELMUX_HEATER_6_3)        ,\
    (IUG_UNITS_STASELMUX_HEATER_6_4)        ,\
    (IUG_UNITS_STASELMUX_HEATER_7_2)        ,\
    (IUG_UNITS_STASELMUX_HEATER_7_3)        ,\
    (IUG_UNITS_STASELMUX_HEATER_7_4)        ,\
    (IUG_UNITS_STASELMUX_HEATER_8_2)        ,\
    (IUG_UNITS_STASELMUX_HEATER_8_3)        ,\
    (IUG_UNITS_STASELMUX_HEATER_8_4)         \
}

#define IUG_UNITS_STA_SEL_EXT_MUX_500MS_CFG \
{\
    (IUG_UNITS_STASELEXTMUX_KEEP_ALIVE)     ,\
    (IUG_UNITS_STASELEXTMUX_RESET_REASON)    \
}

#define IUG_UNITS_STA_SEL_EXT_MUX_1000MS_CFG \
{\
    (IUG_UNITS_STASELEXTMUX_HISTORY_1_1)    ,\
    (IUG_UNITS_STASELEXTMUX_HISTORY_2_1)    ,\
    (IUG_UNITS_STASELEXTMUX_HISTORY_3_1)    ,\
    (IUG_UNITS_STASELEXTMUX_HISTORY_4_1)    ,\
    (IUG_UNITS_STASELEXTMUX_HISTORY_5_1)    ,\
    (IUG_UNITS_STASELEXTMUX_HISTORY_6_1)    ,\
    (IUG_UNITS_STASELEXTMUX_HISTORY_7_1)    ,\
    (IUG_UNITS_STASELEXTMUX_HISTORY_8_1)    ,\
    (IUG_UNITS_STASELEXTMUX_HISTORY_9_1)    ,\
    (IUG_UNITS_STASELEXTMUX_HISTORY_10_1)    \
}










#endif 


