#ifndef _IUG_DIAGNOSIS_DTC_SHARE_CFG_H_
#define _IUG_DIAGNOSIS_DTC_SHARE_CFG_H_



#ifdef DTC_ERR_GHI_ACTION_CANCELLED_HGX_SHARED
    #define DTC_ERR_SHARE_GHI_ACTION_CANCELLED_HGX \
        {\
            (DTC_ERR_GHI_ACTION_CANCELLED_HGX_SHARED),                              \
            (25000u / (IUG_UNIT_CTL_CYLIC_PERIOD))      \
        },
#else
    #define DTC_ERR_SHARE_GHI_ACTION_CANCELLED_HGX
#endif


#ifdef DTC_ERR_GHI_NO_HEARTBEAT_RESP_HGX_SHARED
    #define DTC_ERR_SHARE_GHI_NO_HEARTBEAT_RESPOND_HGX \
        {\
            (DTC_ERR_GHI_NO_HEARTBEAT_RESP_HGX_SHARED),                             \
            (25000u / (IUG_UNIT_CTL_CYLIC_PERIOD))      \
        },
#else
    #define DTC_ERR_SHARE_GHI_NO_HEARTBEAT_RESPOND_HGX
#endif


#ifdef DTC_ERR_GHI_BE_NO_CMD_CONTINUE_HGX_SHARED
    #define DTC_ERR_SHARE_GHI_BE_NO_CMD_CONTINUE_HGX \
        {\
            (DTC_ERR_GHI_BE_NO_CMD_CONTINUE_TO_HGX),                                \
            (25000u / (IUG_UNIT_CTL_CYLIC_PERIOD))      \
        },
#else
    #define DTC_ERR_SHARE_GHI_BE_NO_CMD_CONTINUE_HGX
#endif




#define IUG_DIAG_DTC_SHARE_CONFIGURATION \
{ \
    \
    \
    \
    \
    \
    \
    \
    \
    \
      \
    DTC_ERR_SHARE_GHI_ACTION_CANCELLED_HGX \
      \
    DTC_ERR_SHARE_GHI_NO_HEARTBEAT_RESPOND_HGX \
      \
    DTC_ERR_SHARE_GHI_BE_NO_CMD_CONTINUE_HGX \
    { \
      \
      \
      \
    (DTC_ENUM_MAX),                                                    \
    (0u / (IUG_UNIT_CTL_CYLIC_PERIOD)),    \
    },\
}



#endif 


