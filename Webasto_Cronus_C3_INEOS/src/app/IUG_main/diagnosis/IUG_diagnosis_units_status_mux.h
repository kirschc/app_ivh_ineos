#ifndef _IUG_DIAGNOSIS_UNITS_STATUS_MUX_H_
#define _IUG_DIAGNOSIS_UNITS_STATUS_MUX_H_





#define IUG_UNITS_STATUS_MESSAGE_CAN_ID             0x60Fu  

#define IUG_UNITS_STATUS_MESSAGE_EACH_TARGET        4u      

#define IUG_UNITS_STATUS_MESSAGE_EACH_CONTROL       2u      


#define IUG_UNITS_STASELMUX_SW_HW_VERSION_BASE_SW_VERSION_MAJOR     0u
#define IUG_UNITS_STASELMUX_SW_HW_VERSION_BASE_SW_VERSION_MINOR     1u
#define IUG_UNITS_STASELMUX_SW_HW_VERSION_BASE_SW_VERSION_BUILD     2u
#define IUG_UNITS_STASELMUX_SW_HW_VERSION_HW_VERSION                3u
#define IUG_UNITS_STASELMUX_SW_HW_VERSION_HW_REVISION               4u
#define IUG_UNITS_STASELMUX_SW_HW_VERSION_ENTRY_5                   5u
#define IUG_UNITS_STASELMUX_SW_HW_VERSION_ENTRY_6                   6u

#define IUG_UNITS_STASELMUX_RTC_SECOND                      0u
#define IUG_UNITS_STASELMUX_RTC_MINUTE                      1u
#define IUG_UNITS_STASELMUX_RTC_HOUR                        2u
#define IUG_UNITS_STASELMUX_RTC_DAY                         3u
#define IUG_UNITS_STASELMUX_RTC_MONTH                       4u
#define IUG_UNITS_STASELMUX_RTC_YEAR                        5u
#define IUG_UNITS_STASELMUX_RTC_6                           6u

#define IUG_UNITS_STASELMUX_MISC_OBD_PROTOCOL               0u
#define IUG_UNITS_STASELMUX_MISC_TEMP_SENS                  1u
#define IUG_UNITS_STASELMUX_MISC_CFG_HEATING                2u
#define IUG_UNITS_STASELMUX_MISC_CFG_BUTTON                 3u
#define IUG_UNITS_STASELMUX_MISC_4                          4u
#define IUG_UNITS_STASELMUX_MISC_CFG_LEVEL_BTS_AIR          5u
#define IUG_UNITS_STASELMUX_MISC_CFG_MODE_BTS_AIR           6u

#define IUG_UNITS_STASELMUX_MISC_TEMP_SENS_WBUS_BE          0x01u
#define IUG_UNITS_STASELMUX_MISC_TEMP_SENS_WBUS_HG          0x02u
#define IUG_UNITS_STASELMUX_MISC_TEMP_SENS_BMP              0x04u
#define IUG_UNITS_STASELMUX_MISC_TEMP_SENS_CMD_BUT          0x08u
#define IUG_UNITS_STASELMUX_MISC_TEMP_10                    0x10u
#define IUG_UNITS_STASELMUX_MISC_TEMP_20                    0x20u
#define IUG_UNITS_STASELMUX_MISC_TEMP_40                    0x40u
#define IUG_UNITS_STASELMUX_MISC_TEMP_80                    0x80u


#define IUG_UNITS_STASELMUX_MISC_CONFIG_DRV_HEATING         0x01u   
#define IUG_UNITS_STASELMUX_MISC_SWITCH_DRV_HEATING         0x02u   
#define IUG_UNITS_STASELMUX_MISC_UI_AUTOMATIC_AUX_HEATING   0x04u   
#define IUG_UNITS_STASELMUX_MISC_CFG_HEAT_VENT_BTS          0x08u   
#define IUG_UNITS_STASELMUX_MISC_10                         0x10u 
#define IUG_UNITS_STASELMUX_MISC_20                         0x20u 
#define IUG_UNITS_STASELMUX_MISC_40                         0x40u 
#define IUG_UNITS_STASELMUX_MISC_80                         0x80u 


#define IUG_UNITS_STASELMUX_MISC_TEMP_SENS_WBUS_BE_VAL      0u
#define IUG_UNITS_STASELMUX_MISC_TEMP_SENS_WBUS_HG_VAL      1u
#define IUG_UNITS_STASELMUX_MISC_TEMP_SENS_BMP_VAL          2u
#define IUG_UNITS_STASELMUX_MISC_TEMP_SENS_CMD_BUT_VAL      3u
#define IUG_UNITS_STASELMUX_MISC_TEMP_SENS_4                4u
#define IUG_UNITS_STASELMUX_MISC_AIR_PRESSURE_VAL           5u
#define IUG_UNITS_STASELMUX_MISC_AIR_PRESSURE_VAL2          6u


#define IUG_UNITS_STASELMUX_IUG_STATUS_FLG                  0u  
#define IUG_UNITS_STASELMUX_IUG_STATE                       1u  
#define IUG_UNITS_STASELMUX_IUG_HGX_AVAILABLE               2u  
#define IUG_UNITS_STASELMUX_IUG_MISC_STATUS                 3u  
#define IUG_UNITS_STASELMUX_IUG_4                           4u
#define IUG_UNITS_STASELMUX_IUG_5                           5u
#define IUG_UNITS_STASELMUX_IUG_ENTRY_DEV                   6u 

#define IUG_UNITS_STASELMUX_IUG_FLG_FAILURE              0x01u  
#define IUG_UNITS_STASELMUX_IUG_FLG__NO_WBUS_MEMBERS     0x02u  
#define IUG_UNITS_STASELMUX_IUG_FLG_04                   0x04u  
#define IUG_UNITS_STASELMUX_IUG_FLG_08                   0x08u  
#define IUG_UNITS_STASELMUX_IUG_FLG_10                   0x10u  
#define IUG_UNITS_STASELMUX_IUG_FLG_20                   0x20u  
#define IUG_UNITS_STASELMUX_IUG_FLG_40                   0x40u  
#define IUG_UNITS_STASELMUX_IUG_CLAMP_15_ON              0x80u  

#define IUG_UNITS_STASELMUX_IUG_MISC_FZG_STATE           0x01u  
#define IUG_UNITS_STASELMUX_IUG_MISC_CAR_CLIMATE         0x02u  
#define IUG_UNITS_STASELMUX_IUG_MISC_04                  0x04u
#define IUG_UNITS_STASELMUX_IUG_MISC_08                  0x08u
#define IUG_UNITS_STASELMUX_IUG_MISC_10                  0x10u
#define IUG_UNITS_STASELMUX_IUG_MISC_20                  0x20u
#define IUG_UNITS_STASELMUX_IUG_MISC_40                  0x40u
#define IUG_UNITS_STASELMUX_IUG_MISC_80                  0x80u


typedef enum
{

    IUG_UNITS_STASELMUX_0x00                        = 0x00u,    
    IUG_UNITS_STASELMUX_0x09                        = 0x09u,    

    IUG_UNITS_STASELMUX_SW_HW_VERSION               = 0x0Au,    
    IUG_UNITS_STASELMUX_IUG_RTC_STATUS              = 0x0Bu,    
    IUG_UNITS_STASELMUX_IUG_MISC_CFG_STAT           = 0x0Cu,    
    IUG_UNITS_STASELMUX_IUG_MISC_VALUE              = 0x0Du,    
    IUG_UNITS_STASELMUX_0x0E                        = 0x0Eu,    
    IUG_UNITS_STASELMUX_0x0F                        = 0x0Fu,    

    IUG_UNITS_STASELMUX_IUG_STATUS                  = 0x10u,   

    IUG_UNITS_STASELMUX_IUG_11                      = 0x11u,   
    IUG_UNITS_STASELMUX_IUG_79                      = 0x79u,   

    IUG_UNITS_STASELMUX_CTL_1_7_BOUND_TO_TGT_X      = 0x7Au,   
    IUG_UNITS_STASELMUX_CTL_8_14_BOUND_TO_TGT_X     = 0x7Bu,   
    IUG_UNITS_STASELMUX_CTL_15_21_BOUND_TO_TGT_X    = 0x7Cu,   


    IUG_UNITS_STASELMUX_CYCLIC_20ms                 = 0x7Du,   
    IUG_UNITS_STASELMUX_CYCLIC_100ms                = 0x7Eu,   
    IUG_UNITS_STASELMUX_CYCLIC_1s                   = 0x7Fu,   


    IUG_UNITS_STASELMUX_HEATER_1_1                  = 0x80u,   
    IUG_UNITS_STASELMUX_HEATER_1_2                  = 0x81u,   
    IUG_UNITS_STASELMUX_HEATER_1_3                  = 0x82u,   
    IUG_UNITS_STASELMUX_HEATER_1_4                  = 0x83u,   

    IUG_UNITS_STASELMUX_HEATER_2_1                  = 0x84u,   
    IUG_UNITS_STASELMUX_HEATER_2_2                  = 0x85u,   
    IUG_UNITS_STASELMUX_HEATER_2_3                  = 0x86u,   
    IUG_UNITS_STASELMUX_HEATER_2_4                  = 0x87u,   

    IUG_UNITS_STASELMUX_HEATER_3_1                  = 0x88u,   
    IUG_UNITS_STASELMUX_HEATER_3_2                  = 0x89u,   
    IUG_UNITS_STASELMUX_HEATER_3_3                  = 0x8Au,   
    IUG_UNITS_STASELMUX_HEATER_3_4                  = 0x8Bu,   

    IUG_UNITS_STASELMUX_HEATER_4_1                  = 0x8Cu,   
    IUG_UNITS_STASELMUX_HEATER_4_2                  = 0x8Du,   
    IUG_UNITS_STASELMUX_HEATER_4_3                  = 0x8Eu,   
    IUG_UNITS_STASELMUX_HEATER_4_4                  = 0x8Fu,   

    IUG_UNITS_STASELMUX_HEATER_5_1                  = 0x90u,   
    IUG_UNITS_STASELMUX_HEATER_5_2                  = 0x91u,   
    IUG_UNITS_STASELMUX_HEATER_5_3                  = 0x92u,   
    IUG_UNITS_STASELMUX_HEATER_5_4                  = 0x93u,   

    IUG_UNITS_STASELMUX_HEATER_6_1                  = 0x94u,   
    IUG_UNITS_STASELMUX_HEATER_6_2                  = 0x95u,   
    IUG_UNITS_STASELMUX_HEATER_6_3                  = 0x96u,   
    IUG_UNITS_STASELMUX_HEATER_6_4                  = 0x97u,   

    IUG_UNITS_STASELMUX_HEATER_7_1                  = 0x98u,   
    IUG_UNITS_STASELMUX_HEATER_7_2                  = 0x99u,   
    IUG_UNITS_STASELMUX_HEATER_7_3                  = 0x9Au,   
    IUG_UNITS_STASELMUX_HEATER_7_4                  = 0x9Bu,   

    IUG_UNITS_STASELMUX_HEATER_8_1                  = 0x9Cu,   
    IUG_UNITS_STASELMUX_HEATER_8_2                  = 0x9Du,   
    IUG_UNITS_STASELMUX_HEATER_8_3                  = 0x9Eu,   
    IUG_UNITS_STASELMUX_HEATER_8_4                  = 0x9Fu,   


    IUG_UNITS_STASELMUX_CONTROL_1_1                 = 0xC0u,   
    IUG_UNITS_STASELMUX_CONTROL_1_2                 = 0xC1u,   

    IUG_UNITS_STASELMUX_CONTROL_2_1                 = 0xC2u,   
    IUG_UNITS_STASELMUX_CONTROL_2_2                 = 0xC3u,   

    IUG_UNITS_STASELMUX_CONTROL_3_1                 = 0xC4u,   
    IUG_UNITS_STASELMUX_CONTROL_3_2                 = 0xC5u,   

    IUG_UNITS_STASELMUX_CONTROL_4_1                 = 0xC6u,   
    IUG_UNITS_STASELMUX_CONTROL_4_2                 = 0xC7u,   

    IUG_UNITS_STASELMUX_CONTROL_5_1                 = 0xC8u,   
    IUG_UNITS_STASELMUX_CONTROL_5_2                 = 0xC9u,   

    IUG_UNITS_STASELMUX_CONTROL_6_1                 = 0xCAu,   
    IUG_UNITS_STASELMUX_CONTROL_6_2                 = 0xCBu,   

    IUG_UNITS_STASELMUX_CONTROL_7_1                 = 0xCCu,   
    IUG_UNITS_STASELMUX_CONTROL_7_2                 = 0xCDu,   

    IUG_UNITS_STASELMUX_CONTROL_8_1                 = 0xCEu,   
    IUG_UNITS_STASELMUX_CONTROL_8_2                 = 0xCFu,   

    IUG_UNITS_STASELMUX_CONTROL_9_1                 = 0xD0u,   
    IUG_UNITS_STASELMUX_CONTROL_9_2                 = 0xD1u,   

    IUG_UNITS_STASELMUX_CONTROL_10_1                = 0xD2u,   
    IUG_UNITS_STASELMUX_CONTROL_10_2                = 0xD3u,   

    IUG_UNITS_STASELMUX_CONTROL_11_1                = 0xD4u,   
    IUG_UNITS_STASELMUX_CONTROL_11_2                = 0xD5u,   

    IUG_UNITS_STASELMUX_CONTROL_12_1                = 0xD6u,   
    IUG_UNITS_STASELMUX_CONTROL_12_2                = 0xD7u,   

    IUG_UNITS_STASELMUX_CONTROL_13_1                = 0xD8u,   
    IUG_UNITS_STASELMUX_CONTROL_13_2                = 0xD9u,   

    IUG_UNITS_STASELMUX_CONTROL_14_1                = 0xDAu,   
    IUG_UNITS_STASELMUX_CONTROL_14_2                = 0xDBu,   

    IUG_UNITS_STASELMUX_CONTROL_15_1                = 0xDCu,   
    IUG_UNITS_STASELMUX_CONTROL_15_2                = 0xDDu,   

    IUG_UNITS_STASELMUX_CONTROL_16_1                = 0xDEu,   
    IUG_UNITS_STASELMUX_CONTROL_16_2                = 0xDFu,   


    IUG_UNITS_STASELMUX_ENUM_FF                     = 0xFFu,   
    IUG_UNITS_STASELMUX_ENUM_FORCE_TYPE = 0x7F  

} enum_IUG_UNITS_STATUS_SELECTOR_MUX_t;
typedef enum_IUG_UNITS_STATUS_SELECTOR_MUX_t    IUG_UNITS_STA_SEL_MUX_t;



typedef enum
{
    IUG_UNITS_STAGRPMUX_FAST =  0u, 
    IUG_UNITS_STAGRPMUX_MEDI,       
    IUG_UNITS_STAGRPMUX_SLOW,       
    IUG_UNITS_STAGRPMUX_CTL_FAST,   
    IUG_UNITS_STAGRPMUX_CTL_SLOW,   
    IUG_UNITS_STAGRPMUX_HTR_FAST,   
    IUG_UNITS_STAGRPMUX_HTR_SLOW,   
    IUG_UNITS_STAGRPMUX_ENUM_MAX,               
    IUG_UNITS_STAGRPMUX_ENUM_FORCE_TYPE = 0x7F  

} IUG_UNITS_STATUS_GROUP_MUX_t;


typedef enum
{
    IUG_UNITS_STASELMUX_IDX_CTL1_TELE_START_BOUND_TGTx      = 0u,
    IUG_UNITS_STASELMUX_IDX_CTL2_MULTI_CTL_BOUND_TGTx       = 1u,
    IUG_UNITS_STASELMUX_IDX_CTL3_THERMO_CONNECT_BOUND_TGTx  = 2u,
    IUG_UNITS_STASELMUX_IDX_CTL4_THERMO_CALL_BOUND_TGTx     = 3u,
    IUG_UNITS_STASELMUX_IDX_CTL5_UNDEFINED_BOUND_TGTx       = 4u,
    IUG_UNITS_STASELMUX_IDX_CTL6_UNDEFINED_BOUND_TGTx       = 5u,
    IUG_UNITS_STASELMUX_IDX_CTL7_UNDEFINED_BOUND_TGTx       = 6u,

    IUG_UNITS_STASELMUX_IDX_CTL8_CMD_BUTTON_BOUND_TGTx      = 7u,
    IUG_UNITS_STASELMUX_IDX_CTL9_UNDEFINED_BOUND_TGTx       = 8u,
    IUG_UNITS_STASELMUX_IDX_CTL10_BLUETOOTH_AIR_BOUND_TGTx  = 9u,
    IUG_UNITS_STASELMUX_IDX_CTL11_BLUETOOTH_WAT_BOUND_TGTx  = 10u,
    IUG_UNITS_STASELMUX_IDX_CTL12_UNDEFINED_BOUND_TGTx      = 11u,
    IUG_UNITS_STASELMUX_IDX_CTL13_UNDEFINED_BOUND_TGTx      = 12u,
    IUG_UNITS_STASELMUX_IDX_CTL14_UNDEFINED_BOUND_TGTx      = 13u,
    IUG_UNITS_STASELMUX_IDX_CTL15_UNDEFINED_BOUND_TGTx      = 14u,
    IUG_UNITS_STASELMUX_IDX_CTL16_UNDEFINED_BOUND_TGTx      = 15u,
    IUG_UNITS_STASELMUX_IDX_CTL_ENUM_MAX,               
    IUG_UNITS_STASELMUX_IDX_CTL_ENUM_FORCE_TYPE = 0x7F  

} enum_IUG_UNITS_STASELMUX_IDX_CTLx_BOUND_TGTx_t;
typedef enum_IUG_UNITS_STASELMUX_IDX_CTLx_BOUND_TGTx_t  IUG_UNITS_STASELMUX_IDX_CTL_BND_TGT_t;


typedef enum
{
    IUG_UNITS_STASELMUX_HEATER_FLAGS_NONE       = 0x00u,    
    IUG_UNITS_STASELMUX_HEATER_FLAGS_FAILURE    = 0x01u,    
    IUG_UNITS_STASELMUX_HEATER_FLAGS_STFL       = 0x02u,    
    IUG_UNITS_STASELMUX_HEATER_FLAGS_HGVP       = 0x04u,    
    IUG_UNITS_STASELMUX_HEATER_FLAGS_UEHFL      = 0x08u,    
    IUG_UNITS_STASELMUX_HEATER_FLAGS_10         = 0x10u,    
    IUG_UNITS_STASELMUX_HEATER_FLAGS_20         = 0x20u,    
    IUG_UNITS_STASELMUX_HEATER_FLAGS_40         = 0x40u,    
    IUG_UNITS_STASELMUX_HEATER_FLAGS_COMM_ERR   = 0x80u     

} enum_IUG_UNITS_STASELMUX_HEATER_FLAGS_t;
typedef enum_IUG_UNITS_STASELMUX_HEATER_FLAGS_t     IUG_UNITS_STASELMUX_HEATER_FLAGS_t;







_Static_assert(16u==(IUG_UNITS_STASELMUX_IDX_CTL_ENUM_MAX)     ,"Review encoding of 'IUG_UNITS_STASELMUX_IDX_CTL_ENUM_MAX'!!!");



#endif 


