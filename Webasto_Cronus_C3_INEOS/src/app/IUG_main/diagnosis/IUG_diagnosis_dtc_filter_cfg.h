#ifndef _IUG_DIAGNOSIS_DTC_FILTER_CFG_H_
#define _IUG_DIAGNOSIS_DTC_FILTER_CFG_H_

#include "IUG_error_handler_dtc_cfg.h"




#define DTCFLT_FLG_VCC_7V                  ((DTCFLT_FLG_NONE)|\
                                            (DTCFLT_FLG_FILTER_ACTIVE)|\
                                            (DTCFLT_FLG_UPDATE_WHEN_FILTERED))

#define DTCFLT_CNT_ACT_VCC_7V              ((5000u)/(IUG_UNIT_CTL_CYLIC_PERIOD))
#define DTCFLT_CNT_PAS_VCC_7V              (DTCFLT_CNT_ACT_VCC_7V)

#define DTCFLT_FLG_PRESSURE_COM            ((DTCFLT_FLG_NONE)|\
                                            (DTCFLT_FLG_FILTER_ACTIVE)|\
                                            (DTCFLT_FLG_UPDATE_WHEN_FILTERED))

#define DTCFLT_CNT_ACT_PRESSURE_COM        (((ENV_SENSOR_BMP280_ACCESS_TIME)+5000u)/(IUG_UNIT_CTL_CYLIC_PERIOD))
#define DTCFLT_CNT_PAS_PRESSURE_COM        (DTCFLT_CNT_ACT_PRESSURE_COM)

#define DTCFLT_FLG_TEMP_BUTTON_STG         ((DTCFLT_FLG_NONE)|\
                                            (DTCFLT_FLG_FILTER_ACTIVE)|\
                                            (DTCFLT_FLG_UPDATE_WHEN_FILTERED))

#define DTCFLT_CNT_ACT_TEMP_BUTTON_STG     ((5000u)/(IUG_UNIT_CTL_CYLIC_PERIOD))
#define DTCFLT_CNT_PAS_TEMP_BUTTON_STG     (DTCFLT_CNT_ACT_TEMP_BUTTON_STG)

#define DTCFLT_FLG_TEMP_BUTTON_STBAT       ((DTCFLT_FLG_NONE)|\
                                            (DTCFLT_FLG_FILTER_ACTIVE)|\
                                            (DTCFLT_FLG_UPDATE_WHEN_FILTERED))

#define DTCFLT_CNT_ACT_TEMP_BUTTON_STBAT   ((5000u)/(IUG_UNIT_CTL_CYLIC_PERIOD))
#define DTCFLT_CNT_PAS_TEMP_BUTTON_STBAT   (DTCFLT_CNT_ACT_TEMP_BUTTON_STBAT)

#define DTCFLT_FLG_OBD_REQ                 ((DTCFLT_FLG_NONE)|\
                                            (DTCFLT_FLG_FILTER_ACTIVE)|\
                                            (DTCFLT_FLG_UPDATE_WHEN_FILTERED))

#define DTCFLT_CNT_ACT_OBD_REQ             ((5000u)/(IUG_UNIT_CTL_CYLIC_PERIOD))
#define DTCFLT_CNT_PAS_OBD_REQ             (DTCFLT_CNT_ACT_OBD_REQ)

#define DTCFLT_FLG_HG1_HGV             ((DTCFLT_FLG_NONE)|\
                                        (DTCFLT_FLG_FILTER_ACTIVE)|\
                                        (DTCFLT_FLG_UPDATE_WHEN_FILTERED))

#define DTCFLT_CNT_ACT_HG1_HGV         (((IUG_UNIT_READ_STATUS_HEATER_HIGH_CYCLE)+3000u)/(IUG_UNIT_CTL_CYLIC_PERIOD))
#define DTCFLT_CNT_PAS_HG1_HGV         (DTCFLT_CNT_ACT_HG1_HGV)

#define DTCFLT_FLG_HG2_HGV             (DTCFLT_FLG_HG1_HGV)

#define DTCFLT_CNT_ACT_HG2_HGV         (DTCFLT_CNT_ACT_HG1_HGV)
#define DTCFLT_CNT_PAS_HG2_HGV         (DTCFLT_CNT_ACT_HG2_HGV)

#define DTCFLT_FLG_HG1_STFL            ((DTCFLT_FLG_NONE)|\
                                        (DTCFLT_FLG_FILTER_ACTIVE)|\
                                        (DTCFLT_FLG_UPDATE_WHEN_FILTERED))

#define DTCFLT_CNT_ACT_HG1_STFL        (((IUG_UNIT_READ_STATUS_HEATER_HIGH_CYCLE)+3000u)/(IUG_UNIT_CTL_CYLIC_PERIOD))
#define DTCFLT_CNT_PAS_HG1_STFL        (DTCFLT_CNT_ACT_HG1_STFL)

#define DTCFLT_FLG_HG2_STFL            (DTCFLT_FLG_HG1_STFL)

#define DTCFLT_CNT_ACT_HG2_STFL        (DTCFLT_CNT_ACT_HG1_STFL)
#define DTCFLT_CNT_PAS_HG2_STFL        (DTCFLT_CNT_ACT_HG2_STFL)

#define DTCFLT_FLG_HG1_UEHFL           ((DTCFLT_FLG_NONE)|\
                                        (DTCFLT_FLG_FILTER_ACTIVE)|\
                                        (DTCFLT_FLG_UPDATE_WHEN_FILTERED))

#define DTCFLT_CNT_ACT_HG1_UEHFL       (((IUG_UNIT_READ_STATUS_HEATER_HIGH_CYCLE)+3000u)/(IUG_UNIT_CTL_CYLIC_PERIOD))
#define DTCFLT_CNT_PAS_HG1_UEHFL        (DTCFLT_CNT_ACT_HG1_UEHFL)

#define DTCFLT_FLG_HG2_UEHFL           (DTCFLT_FLG_HG1_UEHFL)

#define DTCFLT_CNT_ACT_HG2_UEHFL        (DTCFLT_CNT_ACT_HG1_UEHFL)
#define DTCFLT_CNT_PAS_HG2_UEHFL        (DTCFLT_CNT_ACT_HG2_UEHFL)


#define DTCFLT_FLG_WBUS_COM_HG1        ((DTCFLT_FLG_NONE)|\
                                        (DTCFLT_FLG_FILTER_ACTIVE)|\
                                        (DTCFLT_FLG_FILTER_INACTIVE) )

#define DTCFLT_CNT_ACT_WBUS_COM_HG1    (((IUG_UNIT_READ_STATUS_HEATER_HIGH_CYCLE)+3000u)/(IUG_UNIT_CTL_CYLIC_PERIOD))
#define DTCFLT_CNT_PAS_WBUS_COM_HG1    (DTCFLT_CNT_ACT_WBUS_COM_HG1)

#define DTCFLT_FLG_WBUS_COM_HG2        (DTCFLT_FLG_WBUS_COM_HG1)

#define DTCFLT_CNT_ACT_WBUS_COM_HG2    (DTCFLT_CNT_ACT_WBUS_COM_HG1)
#define DTCFLT_CNT_PAS_WBUS_COM_HG2    (DTCFLT_CNT_ACT_WBUS_COM_HG2)


#define DTCFLT_FLG_WBUS_COM_MUL_CTL_3TO4    ((DTCFLT_FLG_NONE)|\
                                             (DTCFLT_FLG_FILTER_ACTIVE)|\
                                              0|\
                                             (DTCFLT_FLG_UPDATE_WHEN_FILTERED))

#define DTCFLT_CNT_ACT_WBUS_COM_MUL_CTL_3TO4  (((IUG_UNIT_CTL_READ_ID_WBUS_VERSION_PERIOD_MS)+3000u)/(IUG_UNIT_CTL_CYLIC_PERIOD))
#define DTCFLT_CNT_PAS_WBUS_COM_MUL_CTL_3TO4  (DTCFLT_CNT_ACT_WBUS_COM_MUL_CTL_3TO4)


#define DTCFLT_FLG_WBUS_COM_REMOTE_CTL      ((DTCFLT_FLG_NONE)|\
                                             (DTCFLT_FLG_FILTER_ACTIVE)|\
                                              0|\
                                             (DTCFLT_FLG_UPDATE_WHEN_FILTERED))

#define DTCFLT_CNT_ACT_WBUS_COM_REMOTE_CTL  (((IUG_UNIT_CTL_READ_ID_WBUS_VERSION_PERIOD_MS)+3000u)/(IUG_UNIT_CTL_CYLIC_PERIOD))
#define DTCFLT_CNT_PAS_WBUS_COM_REMOTE_CTL  (DTCFLT_CNT_ACT_WBUS_COM_REMOTE_CTL)


#define DTCFLT_FLG_WBUS_COM_THERMO_CALL     ((DTCFLT_FLG_NONE)|\
                                             (DTCFLT_FLG_FILTER_ACTIVE)|\
                                              0|\
                                             (DTCFLT_FLG_UPDATE_WHEN_FILTERED))

#define DTCFLT_CNT_ACT_WBUS_COM_THERMO_CALL (((IUG_UNIT_CTL_READ_ID_WBUS_VERSION_PERIOD_MS)+3000u)/(IUG_UNIT_CTL_CYLIC_PERIOD))
#define DTCFLT_CNT_PAS_WBUS_COM_THERMO_CALL (DTCFLT_CNT_ACT_WBUS_COM_THERMO_CALL)


#define DTCFLT_FLG_WBUS_COM_THERMO_CONN     ((DTCFLT_FLG_NONE)|\
                                             (DTCFLT_FLG_FILTER_ACTIVE)|\
                                              0|\
                                             (DTCFLT_FLG_UPDATE_WHEN_FILTERED))

#define DTCFLT_CNT_ACT_WBUS_COM_THERMO_CONN (((IUG_UNIT_CTL_READ_ID_WBUS_VERSION_PERIOD_MS)+3000u)/(IUG_UNIT_CTL_CYLIC_PERIOD))
#define DTCFLT_CNT_PAS_WBUS_COM_THERMO_CONN (DTCFLT_CNT_ACT_WBUS_COM_THERMO_CONN)


#define DTCFLT_FLG_WBUS_COM_T_LIN_R_INT_EXT ((DTCFLT_FLG_NONE)|\
                                             0|\
                                             (DTCFLT_FLG_FILTER_INACTIVE)|\
                                             (DTCFLT_FLG_UPDATE_WHEN_FILTERED))

#define DTCFLT_CNT_ACT_WBUS_COM_T_LIN_R_INT_EXT  (((IUG_UNIT_CTL_READ_ID_WBUS_VERSION_PERIOD_MS)+3000u)/(IUG_UNIT_CTL_CYLIC_PERIOD))
#define DTCFLT_CNT_PAS_WBUS_COM_T_LIN_R_INT_EXT  (DTCFLT_CNT_ACT_WBUS_COM_T_LIN_R_INT_EXT)



#ifdef DTC_ERR_GHI_COM_HGX_SHARED
    #define IUG_DIAG_DTC_FLT_CFG_GHI_DTC_COM_HGX \
        {\
            (DTC_ERR_GHI_COM_HGX_SHARED),                               \
            (IUG_DIAG_DTC_FILTER_FLAGS_t)(DTCFLT_FLG_FILTER_INACTIVE),  \
            (DTCFLT_CNT_ACT_WBUS_COM_HG1),                              \
            (DTCFLT_CNT_PAS_WBUS_COM_HG1)                               \
        },
#else
    #define IUG_DIAG_DTC_FLT_CFG_GHI_DTC_COM_HGX
#endif

#ifdef DTC_ERR_GHI_STFL_HGX_SHARED
    #define IUG_DIAG_DTC_FLT_CFG_GHI_DTC_STFL_HGX \
        {\
            (DTC_ERR_GHI_STFL_HGX_SHARED),                              \
            (IUG_DIAG_DTC_FILTER_FLAGS_t)(DTCFLT_FLG_FILTER_INACTIVE),  \
            (DTCFLT_CNT_ACT_HG1_STFL),                                  \
            (DTCFLT_CNT_PAS_HG1_STFL)                                   \
        },
#else
    #define IUG_DIAG_DTC_FLT_CFG_GHI_DTC_STFL_HGX
#endif

#ifdef DTC_ERR_GHI_HGV_HGX_SHARED
    #define IUG_DIAG_DTC_FLT_CFG_GHI_DTC_HGV_HGX \
        {\
            (DTC_ERR_GHI_HGV_HGX_SHARED),                               \
            (IUG_DIAG_DTC_FILTER_FLAGS_t)(DTCFLT_FLG_FILTER_INACTIVE),  \
            (DTCFLT_CNT_ACT_HG1_HGV),                                   \
            (DTCFLT_CNT_PAS_HG1_HGV)                                    \
        },
#else
    #define IUG_DIAG_DTC_FLT_CFG_GHI_DTC_HGV_HGX
#endif

#ifdef DTC_ERR_GHI_UEHFL_HGX_SHARED
    #define IUG_DIAG_DTC_FLT_CFG_GHI_DTC_UEHFL_HGX \
        {\
            (DTC_ERR_GHI_UEHFL_HGX_SHARED),                             \
            (IUG_DIAG_DTC_FILTER_FLAGS_t)(DTCFLT_FLG_FILTER_INACTIVE),  \
            (DTCFLT_CNT_ACT_HG1_UEHFL),                                 \
            (DTCFLT_CNT_PAS_HG1_UEHFL)                                  \
        },
#else
    #define IUG_DIAG_DTC_FLT_CFG_GHI_DTC_UEHFL_HGX
#endif

#ifdef DTC_ERR_T_LIN_R_COM_INT_EXT_SHARED
    #define IUG_DIAG_DTC_FLT_CFG_T_LIN_R_DTC_COM_INT_EXT \
        {\
            (DTC_ERR_T_LIN_R_COM_INT_EXT_SHARED),                               \
            (IUG_DIAG_DTC_FILTER_FLAGS_t)(DTCFLT_FLG_WBUS_COM_T_LIN_R_INT_EXT), \
            (DTCFLT_CNT_ACT_WBUS_COM_T_LIN_R_INT_EXT),                          \
            (DTCFLT_CNT_PAS_WBUS_COM_T_LIN_R_INT_EXT)                           \
        },
#else
    #define IUG_DIAG_DTC_FLT_CFG_T_LIN_R_DTC_COM_INT_EXT
#endif

#ifdef DTC_ERR_T_LIN_R_MISMATCH_INT_EXT_SHARED
#else
    #define IUG_DIAG_DTC_FLT_CFG_T_LIN_R_DTC_MISMATCH_INT_EXT
#endif


#define IUG_DIAG_DTC_FILTER_CONFIGURATION \
{ \
        { \
        (DTC_ERR_VCC_7V),                                        \
        (IUG_DIAG_DTC_FILTER_FLAGS_t)(DTCFLT_FLG_VCC_7V),        \
        (DTCFLT_CNT_ACT_VCC_7V),                                 \
        (DTCFLT_CNT_PAS_VCC_7V)                                  \
    },\
    { \
        (DTC_ERR_PRESSURE_COM),                                  \
        (IUG_DIAG_DTC_FILTER_FLAGS_t)(DTCFLT_FLG_PRESSURE_COM),  \
        (DTCFLT_CNT_ACT_PRESSURE_COM),                           \
        (DTCFLT_CNT_PAS_PRESSURE_COM),                           \
    },\
    { \
        (DTC_ERR_HG1_HGV),                                      \
        (IUG_DIAG_DTC_FILTER_FLAGS_t)(DTCFLT_FLG_HG1_HGV),      \
        (DTCFLT_CNT_ACT_HG1_HGV),                               \
        (DTCFLT_CNT_PAS_HG1_HGV),                               \
    },\
    { \
        (DTC_ERR_HG1_STFL),                                     \
        (IUG_DIAG_DTC_FILTER_FLAGS_t)(DTCFLT_FLG_HG1_STFL),     \
        (DTCFLT_CNT_ACT_HG1_STFL),                              \
        (DTCFLT_CNT_PAS_HG1_STFL),                              \
    },\
    { \
        (DTC_ERR_HG2_HGV),                                      \
        (IUG_DIAG_DTC_FILTER_FLAGS_t)(DTCFLT_FLG_HG2_HGV),      \
        (DTCFLT_CNT_ACT_HG2_HGV),                               \
        (DTCFLT_CNT_PAS_HG2_HGV),                               \
    },\
    { \
        (DTC_ERR_HG2_STFL),                                     \
        (IUG_DIAG_DTC_FILTER_FLAGS_t)(DTCFLT_FLG_HG2_STFL),     \
        (DTCFLT_CNT_ACT_HG2_STFL),                              \
        (DTCFLT_CNT_PAS_HG2_STFL),                              \
    },\
    { \
        (DTC_ERR_HG1_UEHFL),                                    \
        (IUG_DIAG_DTC_FILTER_FLAGS_t)(DTCFLT_FLG_HG1_UEHFL),    \
        (DTCFLT_CNT_ACT_HG1_UEHFL),                             \
        (DTCFLT_CNT_PAS_HG1_UEHFL),                             \
    },\
    { \
        (DTC_ERR_HG2_UEHFL),                                    \
        (IUG_DIAG_DTC_FILTER_FLAGS_t)(DTCFLT_FLG_HG2_UEHFL),    \
        (DTCFLT_CNT_ACT_HG2_UEHFL),                             \
        (DTCFLT_CNT_PAS_HG2_UEHFL),                             \
    },\
    { \
        (DTC_ERR_TEMP_BUTTON_STG),                                          \
        (IUG_DIAG_DTC_FILTER_FLAGS_t)(DTCFLT_FLG_TEMP_BUTTON_STG),          \
        (DTCFLT_CNT_ACT_TEMP_BUTTON_STG),                                   \
        (DTCFLT_CNT_PAS_TEMP_BUTTON_STG),                                   \
    },\
    { \
        (DTC_ERR_TEMP_BUTTON_STBAT),                                        \
        (IUG_DIAG_DTC_FILTER_FLAGS_t)(DTCFLT_FLG_TEMP_BUTTON_STBAT),        \
        (DTCFLT_CNT_ACT_TEMP_BUTTON_STBAT),                                 \
        (DTCFLT_CNT_PAS_TEMP_BUTTON_STBAT),                                 \
    },\
    { \
        (DTC_ERR_OBD_REQ),                                                  \
        (IUG_DIAG_DTC_FILTER_FLAGS_t)(DTCFLT_FLG_OBD_REQ),                  \
        (DTCFLT_CNT_ACT_OBD_REQ),                                           \
        (DTCFLT_CNT_PAS_OBD_REQ),                                           \
    },\
    { \
        (DTC_ERR_LIN3_WBUS_COM_HG1),                                        \
        (IUG_DIAG_DTC_FILTER_FLAGS_t)(DTCFLT_FLG_WBUS_COM_HG1),             \
        (DTCFLT_CNT_ACT_WBUS_COM_HG1),                                      \
        (DTCFLT_CNT_PAS_WBUS_COM_HG1)                                       \
    },\
    { \
        (DTC_ERR_LIN3_WBUS_COM_HG2),                                        \
        (IUG_DIAG_DTC_FILTER_FLAGS_t)(DTCFLT_FLG_WBUS_COM_HG2),             \
        (DTCFLT_CNT_ACT_WBUS_COM_HG2),                                      \
        (DTCFLT_CNT_PAS_WBUS_COM_HG2)                                       \
    },\
      \
    IUG_DIAG_DTC_FLT_CFG_GHI_DTC_COM_HGX \
      \
    IUG_DIAG_DTC_FLT_CFG_GHI_DTC_STFL_HGX \
    { \
        (DTC_ERR_LIN4_WBUS_COM_MUL_CTL_3TO4),                               \
        (IUG_DIAG_DTC_FILTER_FLAGS_t)(DTCFLT_FLG_WBUS_COM_MUL_CTL_3TO4),    \
        (DTCFLT_CNT_ACT_WBUS_COM_MUL_CTL_3TO4),                             \
        (DTCFLT_CNT_PAS_WBUS_COM_MUL_CTL_3TO4),                             \
    },\
    { \
        (DTC_ERR_LIN4_WBUS_COM_REMOTE_CTL),                                 \
        (IUG_DIAG_DTC_FILTER_FLAGS_t)(DTCFLT_FLG_WBUS_COM_REMOTE_CTL),      \
        (DTCFLT_CNT_ACT_WBUS_COM_REMOTE_CTL),                               \
        (DTCFLT_CNT_PAS_WBUS_COM_REMOTE_CTL),                               \
    },\
    { \
        (DTC_ERR_LIN4_WBUS_COM_THERMO_CALL),                                \
        (IUG_DIAG_DTC_FILTER_FLAGS_t)(DTCFLT_FLG_WBUS_COM_THERMO_CALL),     \
        (DTCFLT_CNT_ACT_WBUS_COM_THERMO_CALL),                              \
        (DTCFLT_CNT_PAS_WBUS_COM_THERMO_CALL),                              \
    },\
    { \
        (DTC_ERR_LIN4_WBUS_COM_THERMO_CON),                                 \
        (IUG_DIAG_DTC_FILTER_FLAGS_t)(DTCFLT_FLG_WBUS_COM_THERMO_CONN),     \
        (DTCFLT_CNT_ACT_WBUS_COM_THERMO_CONN),                              \
        (DTCFLT_CNT_PAS_WBUS_COM_THERMO_CONN),                              \
    },\
      \
    IUG_DIAG_DTC_FLT_CFG_T_LIN_R_DTC_COM_INT_EXT \
      \
    IUG_DIAG_DTC_FLT_CFG_GHI_DTC_HGV_HGX \
      \
    IUG_DIAG_DTC_FLT_CFG_GHI_DTC_UEHFL_HGX \
};




#endif 


