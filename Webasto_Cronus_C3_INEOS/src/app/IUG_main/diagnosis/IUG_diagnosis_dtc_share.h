#ifndef _IUG_DIAGNOSIS_DTC_SHARE_H_
#define _IUG_DIAGNOSIS_DTC_SHARE_H_






typedef uint16_t        IUG_DIAG_DTC_SHARE_CNT_t;


typedef struct
{
    DTC_TABLE_IDX_t                 dtc_index;

    IUG_DIAG_DTC_SHARE_CNT_t        auto_reset_cnt_cfg;     

} IUG_DIAG_DTC_SHARE_CFG_t;


typedef struct
{
    IUG_DIAG_DTC_SHARE_CNT_t        auto_reset_cnt;         

} IUG_DIAG_DTC_SHARE_CTL_t;


extern uint8_t     IUG_DIAG_DTC_SHARE_set_reset_via__share_cyclic;


void        IUG_sfl_dtc_share_cyclic( void );
uint16_t    IUG_sfl_dtc_share_get_dtc_entry_idx( const DTC_TABLE_IDX_t dtc_index );
boolean     IUG_sfl_dtc_share_init( void );
uint8_t     IUG_sfl_dtc_share_is_reset_cnt_active( const uint16_t index_entry );
uint16_t    IUG_sfl_dtc_share_reset_cnt_clear( const uint16_t index_entry );
uint16_t    IUG_sfl_dtc_share_reset_cnt_set( const uint16_t index_entry );

inline uint8_t      IUG_DIAG_DTC_SHARE_is_reset_via__share_cyclic( void )\
                            { if( 0x5Au == IUG_DIAG_DTC_SHARE_set_reset_via__share_cyclic ) { return 1u; } else { return 0u; } };


#endif 


