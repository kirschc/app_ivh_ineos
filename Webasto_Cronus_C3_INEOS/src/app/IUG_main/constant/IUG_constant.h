#ifndef __IUG_CONSTANT_H_
#define __IUG_CONSTANT_H_



#define ENABLE_FOR_DEBUGGING_ONLY       0u



#define IUG_UNIT_READ_ID_SERIAL_NUMBER_SIZE           (WBUS_PACKET_SERIAL_NUMBER_DATA_SIZE) 


#define IUG_CONST_CNT_TIME_1_MINUTE_BY_SECONDS         60u 
_Static_assert(60u==(IUG_CONST_CNT_TIME_1_MINUTE_BY_SECONDS),"Value for 'IUG_CONST_CNT_TIME_1_MINUTE_BY_SECONDS' must be 60!");


#define IUG_CONST_CNT_MSG_KEEP_ALIVE_TIME            ((15u * 1000u) / (IUG_UNIT_CTL_CYLIC_PERIOD))


#define IUG_CONST_CNT_MSG_KEEP_ALIVE_TIMEOUT         ((21u * 1000u) / (IUG_UNIT_CTL_CYLIC_PERIOD))


#define IUG_AIR_PRESSURE_5HPA_RESOLUTION           (    5u) 

#define IUG_AIR_PRESSURE_NORMAL_HPA                 (990u)     
#define IUG_AIR_PRESSURE_NORMAL_5HPA               ((IUG_AIR_PRESSURE_NORMAL_HPA) / (IUG_AIR_PRESSURE_5HPA_RESOLUTION)); 

#define IUG_AIR_PRESSURE_NORMAL_UPDATE_LIMIT_HPA    (100u)     

#define IUG_AIR_PRESSURE_AT2000_HIGH_LIMIT_HPA      (890u)     

#define IUG_AIR_PRESSURE_AT2000_VALUE_LOW_HPA       (890u)     
#define IUG_AIR_PRESSURE_AT2000_VALUE_HIGH_HPA     (1013u)     


#define IUG_DTC_FRZ_FRM_ID_FZG_STATE                    1u
#define IUG_DTC_FRZ_FRM_ID_IUG_STATE_HG1                2u
#define IUG_DTC_FRZ_FRM_ID_IUG_STATE_HG2                3u
#define IUG_DTC_FRZ_FRM_ID_HG_STATE_HG1                 4u
#define IUG_DTC_FRZ_FRM_ID_HG_STATE_HG2                 5u
#define IUG_DTC_FRZ_FRM_ID_AIR_PRESSURE_VALUE           6u
#define IUG_DTC_FRZ_FRM_ID_VAL_TIME_STAMP               7u
#define IUG_DTC_FRZ_FRM_ID_VAL_IO2                      8u
#define IUG_DTC_FRZ_FRM_ID_VAL_IO3                      9u
#define IUG_DTC_FRZ_FRM_ID_DTC_SYMPTOM                 10u  
#define IUG_DTC_FRZ_FRM_ID_VAL_KL15                    11u
#define IUG_DTC_FRZ_FRM_ID_VAL_KL30                    12u
#define IUG_DTC_FRZ_FRM_ID_VAL_BE_VOLTAGE              13u
#define IUG_DTC_FRZ_FRM_ID_VAL_BE_CURRENT              14u


typedef enum
{
    IUG_CMD_UNIT_ASSIGN_WAT      = 0u, 
    IUG_CMD_UNIT_ASSIGN_AIR      = 1u, 
    IUG_CMD_UNIT_ASSIGN_NONE     = 2u, 
    IUG_CMD_UNIT_ASSIGN_USRDVC1  = 3u, 
    IUG_CMD_UNIT_ASSIGN_USRDVC2  = 4u, 
    IUG_CMD_UNIT_ASSIGN_USRDVC3  = 5u, 
    IUG_CMD_UNIT_ASSIGN_ENUM_FORCE_TYPE = 0x7F  

} enum_IUG_CMD_UNIT_ASSIGNMENT_t;
typedef enum_IUG_CMD_UNIT_ASSIGNMENT_t      IUG_CMD_UNIT_ASSIGNMENT_t;

_Static_assert(0u==(IUG_CMD_UNIT_ASSIGN_WAT)     ,"Review encoding of e.g. webasto parameter 'CONFIG_BUTTON_BTS'!!!");
_Static_assert(1u==(IUG_CMD_UNIT_ASSIGN_AIR)     ,"Review encoding of e.g. webasto parameter 'CONFIG_BUTTON_BTS'!!!");
_Static_assert(2u==(IUG_CMD_UNIT_ASSIGN_NONE)    ,"Review encoding of e.g. webasto parameter 'CONFIG_BUTTON_BTS'!!!");
_Static_assert(3u==(IUG_CMD_UNIT_ASSIGN_USRDVC1) ,"Review encoding of e.g. webasto parameter 'CONFIG_BUTTON_BTS'!!!");
_Static_assert(4u==(IUG_CMD_UNIT_ASSIGN_USRDVC2) ,"Review encoding of e.g. webasto parameter 'CONFIG_BUTTON_BTS'!!!");
_Static_assert(5u==(IUG_CMD_UNIT_ASSIGN_USRDVC3) ,"Review encoding of e.g. webasto parameter 'CONFIG_BUTTON_BTS'!!!");






#endif 

