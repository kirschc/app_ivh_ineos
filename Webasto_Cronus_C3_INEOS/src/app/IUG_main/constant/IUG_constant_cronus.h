#ifndef _IUG_CONSTANT_CRONUS_H_
#define _IUG_CONSTANT_CRONUS_H_





typedef enum
{
    IUG_WAIT_STATE      = 0u,
    IUG_BOOST           = 1u,
    IUG_ECO             = 2u,
    IUG_VENTILATION     = 3u,
    IUG_HEATING         = 4u,
    IUG_AUX_HEATING     = 5u,
    IUG_AUT_AUX_HEATING = 6u,
    IUG_DRIVE_HEATING   = 7u,
    IUG_SWITCH_OFF      = 8u,
    IUG_FAULT           = 9u,
    IUG_INITIAL_OPERATION     = 10,     
    IUG_INITIAL_WAIT_STATE    = 11,     
    IUG_STATE_ENUM_MAX,                 
    IUG_STATE_ENUM_FORCE_TYPE = 0x7F    

} IUG_STATE_t;

_Static_assert( (IUG_WAIT_STATE)         ==  0u,"Analyze: IUG_WAIT_STATE");
_Static_assert( (IUG_BOOST)              ==  1u,"Analyze: IUG_BOOST");
_Static_assert( (IUG_ECO)                ==  2u,"Analyze: IUG_ECO");
_Static_assert( (IUG_VENTILATION)        ==  3u,"Analyze: IUG_VENTILATION");
_Static_assert( (IUG_HEATING)            ==  4u,"Analyze: IUG_HEATING");
_Static_assert( (IUG_AUX_HEATING)        ==  5u,"Analyze: IUG_AUX_HEATING");
_Static_assert( (IUG_AUT_AUX_HEATING)    ==  6u,"Analyze: IUG_AUT_AUX_HEATING");
_Static_assert( (IUG_DRIVE_HEATING)      ==  7u,"Analyze: IUG_DRIVE_HEATING");
_Static_assert( (IUG_SWITCH_OFF)         ==  8u,"Analyze: IUG_SWITCH_OFF");
_Static_assert( (IUG_FAULT)              ==  9u,"Analyze: IUG_FAULT");
_Static_assert( (IUG_INITIAL_OPERATION)  == 10u,"Analyze: IUG_INITIAL_OPERATION");
_Static_assert( (IUG_INITIAL_WAIT_STATE) == 11u,"Analyze: IUG_INITIAL_WAIT_STATE");
_Static_assert( (IUG_STATE_ENUM_MAX)     == 12u,"Analyze: IUG_STATE_ENUM_MAX");


typedef enum
{
    IUG_WAIT_STATE_WAT      = 0u,
    IUG_AUX_HEATING_WAT     = 1u,
    IUG_AUT_AUX_HEATING_WAT = 2u,
    IUG_DRIVE_HEATING_WAT   = 3u,
    IUG_VENTILATION_WAT     = 4u,
    IUG_HEATING_WAT         = 5u,
    IUG_SWITCH_OFF_WAT      = 6u,
    IUG_FAULT_WAT           = 7u,
    IUG_STATE_WATER_ENUM_MAX,               
    IUG_STATE_WATER_ENUM_FORCE_TYPE = 0x7F  

} IUG_STATE_WATER_t;

_Static_assert( (IUG_WAIT_STATE_WAT)       == 0u,"Analyze: IUG_WAIT_STATE_WAT");
_Static_assert( (IUG_AUX_HEATING_WAT)      == 1u,"Analyze: IUG_AUX_HEATING_WAT");
_Static_assert( (IUG_AUT_AUX_HEATING_WAT)  == 2u,"Analyze: IUG_AUT_AUX_HEATING_WAT");
_Static_assert( (IUG_DRIVE_HEATING_WAT)    == 3u,"Analyze: IUG_DRIVE_HEATING_WAT");
_Static_assert( (IUG_VENTILATION_WAT)      == 4u,"Analyze: IUG_VENTILATION_WAT");
_Static_assert( (IUG_HEATING_WAT)          == 5u,"Analyze: IUG_HEATING_WAT");
_Static_assert( (IUG_SWITCH_OFF_WAT)       == 6u,"Analyze: IUG_SWITCH_OFF_WAT");
_Static_assert( (IUG_FAULT_WAT)            == 7u,"Analyze: IUG_FAULT_WAT");
_Static_assert( (IUG_STATE_WATER_ENUM_MAX) == 8u,"Analyze: IUG_STATE_WATER_ENUM_MAX");
_Static_assert((IUG_FAULT_WAT)             ==((IUG_STATE_WATER_ENUM_MAX)-1),"Analyze: IUG_FAULT_WAT");


typedef enum
{
    IUG_WAIT_STATE_AIR  = 0u,
    IUG_BOOST_AIR       = 1u,
    IUG_ECO_AIR         = 2u,
    IUG_VENTILATION_AIR = 3u,
    IUG_HEATING_AIR     = 4u,
    IUG_SWITCH_OFF_AIR  = 5u,
    IUG_FAULT_AIR       = 6u,
    IUG_STATE_AIR_ENUM_MAX,                 
    IUG_STATE_AIR_ENUM_FORCE_TYPE = 0x7F    

} IUG_STATE_AIR_t;

_Static_assert( (IUG_WAIT_STATE_AIR)     == 0u,"Analyze: IUG_WAIT_STATE_AIR");
_Static_assert( (IUG_BOOST_AIR)          == 1u,"Analyze: IUG_BOOST_AIR");
_Static_assert( (IUG_ECO_AIR)            == 2u,"Analyze: IUG_ECO_AIR");
_Static_assert( (IUG_VENTILATION_AIR)    == 3u,"Analyze: IUG_VENTILATION_AIR");
_Static_assert( (IUG_HEATING_AIR)        == 4u,"Analyze: IUG_HEATING_AIR");
_Static_assert( (IUG_SWITCH_OFF_AIR)     == 5u,"Analyze: IUG_SWITCH_OFF_AIR");
_Static_assert( (IUG_FAULT_AIR)          == 6u,"Analyze: IUG_FAULT_AIR");
_Static_assert( (IUG_STATE_AIR_ENUM_MAX) == 7u,"Analyze: IUG_STATE_AIR_ENUM_MAX");
_Static_assert( (IUG_FAULT_AIR)          ==((IUG_STATE_AIR_ENUM_MAX)-1),"Analyze: IUG_FAULT_AIR");


typedef enum
{
    HG_WAIT_STATE      = 0u,
    HG_AUX_HEATING     = 1u,
    HG_VENTILATION     = 3u,
    HG_HEATING         = 4u,
    HG_SWITCH_OFF      = 5u,
    HG_FAULT           = 6u,
    HG_STATE_ENUM_MAX,              
    HG_STATE_ENUM_FORCE_TYPE = 0x7F 

} IUG_HG_STATE_WAT_t;

_Static_assert( (HG_WAIT_STATE)     == 0u,"Analyze: HG_WAIT_STATE");
_Static_assert( (HG_AUX_HEATING)    == 1u,"Analyze: HG_AUX_HEATING");
_Static_assert( (HG_VENTILATION)    == 3u,"Analyze: HG_VENTILATION");
_Static_assert( (HG_HEATING)        == 4u,"Analyze: HG_HEATING");
_Static_assert( (HG_SWITCH_OFF)     == 5u,"Analyze: HG_SWITCH_OFF");
_Static_assert( (HG_FAULT)          == 6u,"Analyze: HG_FAULT");
_Static_assert( (HG_STATE_ENUM_MAX) == 7u,"Analyze: HG_STATE_ENUM_MAX");
_Static_assert( (HG_FAULT)          ==((HG_STATE_ENUM_MAX)-1),"Analyze: HG_FAULT");


typedef enum
{
    HG_WAIT_STATE_AIR  = 0u,
    HG_BOOST_AIR       = 1u,
    HG_ECO_AIR         = 2u,
    HG_VENTILATION_AIR = 3u,
    HG_HEATING_AIR     = 4u,
    HG_SWITCH_OFF_AIR  = 5u,
    HG_FAULT_AIR       = 6u,
    HG_STATE_AIR_ENUM_MAX,              
    HG_STATE_AIR_ENUM_FORCE_TYPE = 0x7F 
} IUG_HG_STATE_AIR_t;

_Static_assert( (HG_WAIT_STATE_AIR)     == 0u,"Analyze: HG_WAIT_STATE_AIR");
_Static_assert( (HG_BOOST_AIR)          == 1u,"Analyze: HG_BOOST_AIR");
_Static_assert( (HG_ECO_AIR)            == 2u,"Analyze: HG_ECO_AIR");
_Static_assert( (HG_VENTILATION_AIR)    == 3u,"Analyze: HG_VENTILATION_AIR");
_Static_assert( (HG_HEATING_AIR)        == 4u,"Analyze: HG_HEATING_AIR");
_Static_assert( (HG_SWITCH_OFF_AIR)     == 5u,"Analyze: HG_SWITCH_OFF_AIR");
_Static_assert( (HG_FAULT_AIR)          == 6u,"Analyze: HG_FAULT_AIR");
_Static_assert( (HG_STATE_AIR_ENUM_MAX) == 7u,"Analyze: HG_STATE_AIR_ENUM_MAX");
_Static_assert((HG_FAULT_AIR)==((HG_STATE_AIR_ENUM_MAX)-1),"Analyze: HG_FAULT_AIR");



#endif 


