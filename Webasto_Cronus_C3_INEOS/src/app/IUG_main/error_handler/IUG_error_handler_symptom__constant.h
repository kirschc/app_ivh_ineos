#ifndef _IUG_ERROR_HANDLER_SYMPTOM_CONSTANT_H_
#define _IUG_ERROR_HANDLER_SYMPTOM_CONSTANT_H_




#define IUG_DTC_SYMPTOM_MAX               50u




typedef enum
{
    DTC_SYM_ID_NONE = 0u,           
    DTC_SYM_ID_DO_NOT_CHANGE,       
    DTC_SYM_ID_SHORTED_OPEN,        
    DTC_SYM_ID_TARGET_NUMBER_1_TO_8_begin,  
    DTC_SYM_ID_TARGET_NUMBER_1_TO_8,        
    DTC_SYM_ID_TARGET_NUMBER_1_TO_8_end,    

    DTC_SYM_ID_HGX_ENUM_MAX,               
    DTC_SYM_ID_HGX_ENUM_FORCE_TYPE = 0x7F  

} IUG_err_hdl_dtc_sypmtom_id_t;


typedef enum
{
    DTC_SYM_NONE            = 0x00u,
    DTC_SYM_SHORT2GND       = 0x01u, 
    DTC_SYM_SHORT2BAT       = 0x02u, 
    DTC_SYM_OPEN_LOAD       = 0x04u, 
    DTC_SYM_WBUS_BE         = 0x01u, 
    DTC_SYM_WBUS_HG         = 0x02u, 
    DTC_SYM_ENUM_MAX,                
    DTC_SYM_ENUM_FORCE_TYPE = 0x7FFF 

} IUG_err_hdl_dtc_sypmtom_t;



typedef uint16_t        IUG_err_hdl_dtc_sypmtom_carrier_t;

typedef struct
{
    DTC_TABLE_IDX_t                     dtc_id;         
    IUG_err_hdl_dtc_sypmtom_id_t        dtc_sym_id;     

    IUG_err_hdl_dtc_topic_t             dtc_topic;      

} IUG_err_hdl_dtc_sypmtom_cfg_entry_t;


#pragma  pack(SFL_DTC_PRAGMA_PACK_NVM_DATA) 
typedef struct
{
    DTC_TABLE_IDX_t                     dtc_id;         

    IUG_err_hdl_dtc_sypmtom_id_t        dtc_sym_id;     
    IUG_err_hdl_dtc_sypmtom_carrier_t   dtc_symptom;    

} IUG_err_hdl_dtc_sypmtom_entry_t;
#pragma pack() 


#pragma  pack(SFL_DTC_PRAGMA_PACK_NVM_DATA) 
typedef struct
{
    uint8_t                             sym_htr_msk;        
    uint8_t                             sym_htr_msk_hst;    

} IUG_err_hdl_dtc_sypmtom_entry_htr_mask_t;
_Static_assert((sizeof(IUG_err_hdl_dtc_sypmtom_entry_htr_mask_t) == sizeof(IUG_err_hdl_dtc_sypmtom_carrier_t)),"Analyze: 'IUG_err_hdl_dtc_sypmtom_entry_htr_mask_t'");
#pragma pack() 






#endif 


