#ifndef _IUG_ERROR_HANDLER_NVM_DIAGNOSIS_STORAGE_H_
#define _IUG_ERROR_HANDLER_NVM_DIAGNOSIS_STORAGE_H_



#define IUG_NVM_DIAG_STORAGE_POOL_SIZE          0x40u  

#define IUG_NVM_DIAG_DTC_SPECIFIC_DATA_NUM      0x08u  








#pragma  pack(SFL_DTC_PRAGMA_PACK_NVM_DATA) 
typedef struct
{
    uint16_t    value[(IUG_NVM_DIAG_DTC_SPECIFIC_DATA_NUM)];

} IUG_NVM_DIAG_DTC_PMIC_SPECIFIC_DATA_t;
#pragma pack() 


#pragma  pack(SFL_DTC_PRAGMA_PACK_NVM_DATA) 
typedef struct
{
    uint8_t     pool[(IUG_NVM_DIAG_STORAGE_POOL_SIZE)];

} IUG_NVM_DIAG_STORAGE_POOL_t;

_Static_assert(((IUG_NVM_DIAG_STORAGE_POOL_SIZE) == sizeof(IUG_NVM_DIAG_STORAGE_POOL_t)),"Analyze: 'IUG_NVM_DIAG_STORAGE_POOL_t'");
#pragma pack() 


#pragma  pack(SFL_DTC_PRAGMA_PACK_NVM_DATA) 
typedef union
{
    struct
    {
        struct_DTC_FREEZE_FRAME_ENTRY_t         dtc_frz_frm;
        IUG_NVM_DIAG_DTC_PMIC_SPECIFIC_DATA_t   dtc_specific;
    }   uni_dtc;

    IUG_NVM_DIAG_STORAGE_POOL_t                 uni_pool;

} IUG_NVM_DIAG_STORAGE_ENTRY_t;

_Static_assert(((IUG_NVM_DIAG_STORAGE_POOL_SIZE) == sizeof(IUG_NVM_DIAG_STORAGE_ENTRY_t)),"Analyze: 'IUG_NVM_DIAG_STORAGE_ENTRY_t'");
#pragma pack() 



extern IUG_NVM_DIAG_STORAGE_ENTRY_t         mgl_IUG_NVM_DIAG_STORAGE;


void    IUG_sfl_err_hdl_nvm_diag__cyclic( void );
uint8_t IUG_sfl_err_hdl_nvm_diag__test_unused( void );
uint8_t IUG_sfl_err_hdl_nvm_diag__write_to_nvm( uint8_t write );


#endif 


