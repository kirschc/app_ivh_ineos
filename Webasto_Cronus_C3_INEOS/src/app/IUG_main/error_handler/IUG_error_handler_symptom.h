#ifndef _IUG_ERROR_HANDLER_SYMPTOM_H_
#define _IUG_ERROR_HANDLER_SYMPTOM_H_









extern const IUG_err_hdl_dtc_sypmtom_cfg_entry_t    mgl_DTC_SYMPTOM_CFG[];
extern const uint16_t                               mgl_DTC_SYMPTOM_CFG_entries;
extern IUG_err_hdl_dtc_sypmtom_entry_t              mgl_DTC_SYMPTOM[(IUG_DTC_SYMPTOM_MAX)];


uint16_t    IUG_sfl_err_hdl_dtc_symptom_cfg_check( void );
uint16_t    IUG_sfl_err_hdl_dtc_symptom_cfg_entry_get( const DTC_TABLE_IDX_t dtc_id, IUG_err_hdl_dtc_sypmtom_cfg_entry_t const * * const p_cfg_ety );
enum_DTC_RETURN_VALUE   IUG_sfl_err_hdl_dtc_symptom_clear_table( const uint8_t test_clear );
uint16_t    IUG_sfl_err_hdl_dtc_symptom_entry_clr( const DTC_TABLE_IDX_t dtc_id );
void        IUG_sfl_err_hdl_dtc_symptom_cyclic( void );
uint16_t    IUG_sfl_err_hdl_dtc_symptom_entry_get( const DTC_TABLE_IDX_t dtc_id, IUG_err_hdl_dtc_sypmtom_entry_t * * const p_entry );
uint16_t    IUG_sfl_err_hdl_dtc_symptom_test_htr( const DTC_TABLE_IDX_t dtc_id, const IUG_HEATER_UNIT_ENUM_t idx_htr_pty );
uint16_t    IUG_sfl_err_hdl_dtc_symptom_update( const DTC_TABLE_IDX_t dtc_id, const uint8_t dtc_force_entry,
                                                const IUG_err_hdl_dtc_sypmtom_id_t dtc_sym_id, 
                                                const IUG_err_hdl_dtc_sypmtom_carrier_t dtc_sym_value );


#endif 


