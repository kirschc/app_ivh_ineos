#ifndef _IUG_ERROR_HANDLER_H_
#define _IUG_ERROR_HANDLER_H_




#define IUG_ERRHDL_INIT_DTC_MASK_CYCLIC     0x5Au

#define IUG_ERRHDL_DTC_ERR_BLE_PAIRINGCODE_ACTIVE_CNT   (2000u/(IUG_UNIT_CTL_CYLIC_PERIOD))


#define IUG_ERRHDL_DTC_SIM_ARY_UNLOCKED     0xF3E5B6A9u
#define IUG_ERRHDL_DTC_SIM_ARY_MAX          10u





typedef struct
{
    IUG_UNIT_ID_t                       unit_id;                

    IUG_err_hdl_dtc_list_bag_entry_t    const * p_dtc_lst_bag;  
    uint8_t                             dtc_lst_bag_entries;    

} IUG_err_hdl_dtc_cfg_description_t;


typedef struct
{
    IUG_SITE_TARGET_ID_t                tgt_id;                         
    uint8_t                             tgt_addr_msk;                   

    IUG_err_hdl_dtc_list_bag_entry_t    const * p_dtc_lst_bag;  
    uint8_t                             dtc_lst_bag_entries;    

    IUG_dtc_reaction_configuration_t    const * p_DTC_DRC_cfg;          
    uint16_t                            const   DTC_DRC_cfg_entries;    

} IUG_err_hdl_dtc_cfg_target_assignment_t;


extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_wbus_def_air_DTC_list_COM_HGX              [(IUG_HEATER_UNIT_ENUM_MAX)];
extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_wbus_def_air_DTC_list_STFL_HGX             [(IUG_HEATER_UNIT_ENUM_MAX)];
extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_wbus_def_air_DTC_list_HGV_HGX              [(IUG_HEATER_UNIT_ENUM_MAX)];
extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_wbus_def_air_DTC_list_UEHFL_HGX            [(IUG_HEATER_UNIT_ENUM_MAX)];
extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_wbus_def_air_DTC_list_NO_CMD_CONT_HGX      [(IUG_HEATER_UNIT_ENUM_MAX)];
extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_wbus_def_air_DTC_list_PND_TIMEOUT_HGX      [(IUG_HEATER_UNIT_ENUM_MAX)];
extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_wbus_def_air_DTC_list_PND_ABORT_HGX        [(IUG_HEATER_UNIT_ENUM_MAX)];
extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_wbus_def_air_DTC_list_SERIAL_NUMBER_HGX    [(IUG_HEATER_UNIT_ENUM_MAX)];

extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_wbus_def_wat_DTC_list_COM_HGX              [(IUG_HEATER_UNIT_ENUM_MAX)];
extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_wbus_def_wat_DTC_list_STFL_HGX             [(IUG_HEATER_UNIT_ENUM_MAX)];
extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_wbus_def_wat_DTC_list_HGV_HGX              [(IUG_HEATER_UNIT_ENUM_MAX)];
extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_wbus_def_wat_DTC_list_UEHFL_HGX            [(IUG_HEATER_UNIT_ENUM_MAX)];
extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_wbus_def_wat_DTC_list_NO_CMD_CONT_HGX      [(IUG_HEATER_UNIT_ENUM_MAX)];
extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_wbus_def_wat_DTC_list_PND_TIMEOUT_HGX      [(IUG_HEATER_UNIT_ENUM_MAX)];
extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_wbus_def_wat_DTC_list_PND_ABORT_HGX        [(IUG_HEATER_UNIT_ENUM_MAX)];
extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_wbus_def_wat_DTC_list_SERIAL_NUMBER_HGX    [(IUG_HEATER_UNIT_ENUM_MAX)];

extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_user_device_DTC_list_NO_CMD_CONT           [(IUG_USER_DEVICE_MAX)];
extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_user_device_DTC_list_PND_TIMEOUT           [(IUG_USER_DEVICE_MAX)];
extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_user_device_DTC_list_PND_ABORT             [(IUG_USER_DEVICE_MAX)];


extern struct_DTC_TABLE_ENTRY                   mgl_dtc_table[(DTC_ENUM_MAX)];
extern const struct_DTC_CONFIG_TABLE_ENTRY      dtc_config_table[(DTC_ENUM_MAX)];

extern uint8_t          ext_iug_errhdl_init_dtc_mask_cyclic_en;

extern uint8_t          ext_iug_errhdl_DTC_sim_ary[(IUG_ERRHDL_DTC_SIM_ARY_MAX)];

extern uint32_t         ext_iug_errhdl_DTC_sim_ary_unlocked;
extern uint8_t          ext_iug_errhdl_DTC_sim_ary[(IUG_ERRHDL_DTC_SIM_ARY_MAX)];



void    IUG_error_handler_cyclic( void );
IUG_err_hdl_dtc_cfg_target_assignment_t const * IUG_error_handler_get_ptr_tgt_assign___via_tgt_id( const IUG_SITE_TARGET_ID_t unit_tgt_id );
void    IUG_error_handler_err_due_DTC_add( IUG_UNIT_ERROR_HANDLER_t * const p_err_hdl, const enum_DTC_TABLE_IDX dtc_index );
void    IUG_error_handler_err_due_DTC_add_from_unit( IUG_UNIT_ERROR_HANDLER_t * const p_err_hdl,
                                                     IUG_UNIT_ERROR_HANDLER_t * const p_err_hdl_unit );
void    IUG_error_handler_err_due_DTC_clr( IUG_UNIT_ERROR_HANDLER_t * const p_err_hdl );
enum_DTC_TABLE_IDX  IUG_err_hdl_dtc_list_bag_get_dtc_via_topic( IUG_err_hdl_dtc_list_bag_entry_t const * const dtc_list_bag, const uint8_t dtc_list_bag_entries,
                                                                const IUG_err_hdl_dtc_topic_t dtc_topic, const uint8_t idx_dtc_indexed );
enum_DTC_TABLE_IDX  IUG_err_hdl_dtc_list_bag_test_dtc( IUG_err_hdl_dtc_list_bag_entry_t const * const dtc_list_bag, const uint8_t dtc_list_bag_entries,
                                                       const enum_DTC_TABLE_IDX dtc_index, const uint8_t idx_dtc_indexed );
boolean IUG_error_handler_init( void );
boolean IUG_error_handler_is_dtc_active( const enum_DTC_TABLE_IDX dtc_index );
uint8_t IUG_error_handler_test_error_for_unit( const IUG_UNIT_ID_t unit_id, const IUG_HEATER_UNIT_ENUM_t idx_htr, const API_USER_DEVICE_ID_t idx_udv,
                                               uint8_t * const drc_err, uint8_t * const drc_inh, uint8_t * const drc_lmp );
uint8_t IUG_error_handler_test_error_for_state( const IUG_UNIT_ID_t unit_id, const enum_DTC_TABLE_IDX dtc_id, const IUG_SITE_TARGET_ID_t unit_tgt_id, const IUG_HEATER_UNIT_ENUM_t idx_htr,
                                                uint8_t * const drcerr_fnd, uint8_t * const drcinh_fnd, uint8_t * const drclmp_fnd );
uint8_t IUG_error_handler_test_error_of_units( const IUG_UNIT_ID_t unit_id_tst, const IUG_HEATER_UNIT_ENUM_t idx_htr_tst, const API_USER_DEVICE_ID_t idx_udv_tst );


uint8_t IUG_error_handler_is_unit_faulty( const IUG_UNIT_ID_t unit_req );


#endif 


