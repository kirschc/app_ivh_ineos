#ifndef _IUG_ERROR_HANDLER_CFG_H_
#define _IUG_ERROR_HANDLER_CFG_H_




typedef struct
{
    IUG_UNIT_ID_t           unit_id;                
    uint16_t                test_cnt;               

} struct_IUG_error_handler_dtc_wbus_comm_configuration_description_t;
typedef struct_IUG_error_handler_dtc_wbus_comm_configuration_description_t      IUG_err_hdl_dtc_wbus_comm_cfg_description_t;


extern const enum_DTC_TABLE_IDX     mgl_iug_errhdl_DTC_list_IUG                        [];
extern const enum_DTC_TABLE_IDX     mgl_iug_errhdl_wbus_def_air_DTC_list_GENERAL       [];
extern const enum_DTC_TABLE_IDX     mgl_iug_errhdl_wbus_def_wat_DTC_list_GENERAL       [];
extern const enum_DTC_TABLE_IDX     mgl_iug_errhdl_ghi_def_air_DTC_list_GENERAL        [];
extern const enum_DTC_TABLE_IDX     mgl_iug_errhdl_ghi_def_wat_DTC_list_GENERAL        [];
extern const enum_DTC_TABLE_IDX     mgl_iug_errhdl_ghi_hvh_air_DTC_list_GENERAL        [];
extern const enum_DTC_TABLE_IDX     mgl_iug_errhdl_ghi_hvh_wat_DTC_list_GENERAL        [];
extern const enum_DTC_TABLE_IDX     mgl_iug_errhdl_ghi_tt_evo_gen4_wat_DTC_list_GENERAL[];
extern const enum_DTC_TABLE_IDX     mgl_iug_errhdl_user_device_DTC_list_GENERAL        [];

extern const IUG_err_hdl_dtc_cfg_description_t              IUG_err_hdl_dtc_cfg_dscrp[];

extern const IUG_err_hdl_dtc_wbus_comm_cfg_description_t    IUG_err_hdl_dtc_wbus_comm_cfg_dscrp[];
extern const uint8_t                                        IUG_err_hdl_entries_dtc_wbus_comm_cfg_dscrp;
extern const uint8_t                                        IUG_err_hdl_entries_dtc_cfg_tgt_assign;

extern const IUG_err_hdl_dtc_cfg_target_assignment_t        IUG_err_hdl_dtc_cfg_tgt_assign_IUG            [];
extern const IUG_err_hdl_dtc_cfg_target_assignment_t        IUG_err_hdl_dtc_cfg_tgt_assign                [];

extern const IUG_err_hdl_dtc_list_bag_entry_t               IUG_err_hdl_dtc_list_bag_IUG[];



#endif 

