#ifndef _IUG_ERROR_HANDLER_GHI_CFG_H_
#define _IUG_ERROR_HANDLER_GHI_CFG_H_








extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_ghi_def_air_DTC_list_COM_HGX[(IUG_HEATER_UNIT_ENUM_MAX)];
extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_ghi_def_wat_DTC_list_COM_HGX[(IUG_HEATER_UNIT_ENUM_MAX)];
extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_ghi_hvh_air_DTC_list_COM_HGX[(IUG_HEATER_UNIT_ENUM_MAX)];
extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_ghi_hvh_wat_DTC_list_COM_HGX[(IUG_HEATER_UNIT_ENUM_MAX)];
extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_ghi_tt_evo_gen4_wat_DTC_list_COM_HGX[(IUG_HEATER_UNIT_ENUM_MAX)];

extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_ghi_def_air_DTC_list_STFL_HGX[(IUG_HEATER_UNIT_ENUM_MAX)];
extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_ghi_def_wat_DTC_list_STFL_HGX[(IUG_HEATER_UNIT_ENUM_MAX)];
extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_ghi_hvh_air_DTC_list_STFL_HGX[(IUG_HEATER_UNIT_ENUM_MAX)];
extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_ghi_hvh_wat_DTC_list_STFL_HGX[(IUG_HEATER_UNIT_ENUM_MAX)];
extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_ghi_tt_evo_gen4_wat_DTC_list_STFL_HGX[(IUG_HEATER_UNIT_ENUM_MAX)];

extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_ghi_def_air_DTC_list_HGV_HGX [(IUG_HEATER_UNIT_ENUM_MAX)];
extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_ghi_def_wat_DTC_list_HGV_HGX [(IUG_HEATER_UNIT_ENUM_MAX)];
extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_ghi_hvh_air_DTC_list_HGV_HGX [(IUG_HEATER_UNIT_ENUM_MAX)];
extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_ghi_hvh_wat_DTC_list_HGV_HGX [(IUG_HEATER_UNIT_ENUM_MAX)];
extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_ghi_tt_evo_gen4_wat_DTC_list_HGV_HGX [(IUG_HEATER_UNIT_ENUM_MAX)];

extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_ghi_def_air_DTC_list_UEHFL_HGX[(IUG_HEATER_UNIT_ENUM_MAX)];
extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_ghi_def_wat_DTC_list_UEHFL_HGX[(IUG_HEATER_UNIT_ENUM_MAX)];
extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_ghi_hvh_air_DTC_list_UEHFL_HGX[(IUG_HEATER_UNIT_ENUM_MAX)];
extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_ghi_hvh_wat_DTC_list_UEHFL_HGX[(IUG_HEATER_UNIT_ENUM_MAX)];
extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_ghi_tt_evo_gen4_wat_DTC_list_UEHFL_HGX[(IUG_HEATER_UNIT_ENUM_MAX)];

extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_ghi_def_air_DTC_list_NO_CMD_CONT_HGX[(IUG_HEATER_UNIT_ENUM_MAX)];
extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_ghi_def_wat_DTC_list_NO_CMD_CONT_HGX[(IUG_HEATER_UNIT_ENUM_MAX)];
extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_ghi_hvh_air_DTC_list_NO_CMD_CONT_HGX[(IUG_HEATER_UNIT_ENUM_MAX)];
extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_ghi_hvh_wat_DTC_list_NO_CMD_CONT_HGX[(IUG_HEATER_UNIT_ENUM_MAX)];
extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_ghi_tt_evo_gen4_wat_DTC_list_NO_CMD_CONT_HGX[(IUG_HEATER_UNIT_ENUM_MAX)];

extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_ghi_def_air_DTC_list_PND_TIMEOUT_HGX[(IUG_HEATER_UNIT_ENUM_MAX)];
extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_ghi_def_wat_DTC_list_PND_TIMEOUT_HGX[(IUG_HEATER_UNIT_ENUM_MAX)];
extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_ghi_hvh_air_DTC_list_PND_TIMEOUT_HGX[(IUG_HEATER_UNIT_ENUM_MAX)];
extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_ghi_hvh_wat_DTC_list_PND_TIMEOUT_HGX[(IUG_HEATER_UNIT_ENUM_MAX)];
extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_ghi_tt_evo_gen4_wat_DTC_list_PND_TIMEOUT_HGX[(IUG_HEATER_UNIT_ENUM_MAX)];

extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_ghi_def_air_DTC_list_PND_ABORT_HGX[(IUG_HEATER_UNIT_ENUM_MAX)];
extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_ghi_def_wat_DTC_list_PND_ABORT_HGX[(IUG_HEATER_UNIT_ENUM_MAX)];
extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_ghi_hvh_air_DTC_list_PND_ABORT_HGX[(IUG_HEATER_UNIT_ENUM_MAX)];
extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_ghi_hvh_wat_DTC_list_PND_ABORT_HGX[(IUG_HEATER_UNIT_ENUM_MAX)];
extern const enum_DTC_TABLE_IDX    ext_iug_errhdl_ghi_tt_evo_gen4_wat_DTC_list_PND_ABORT_HGX[(IUG_HEATER_UNIT_ENUM_MAX)];



#endif 


