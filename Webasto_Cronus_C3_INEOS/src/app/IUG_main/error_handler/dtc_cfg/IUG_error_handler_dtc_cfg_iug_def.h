#ifndef _IUG_ERR_HDL_DTC_CFG_IUG_DEF_H_
#define _IUG_ERR_HDL_DTC_CFG_IUG_DEF_H_

#include "IUG_include.h"



#define IUG_ERRHDL_DTC_list_IUG \
{ \
    (DTC_ERR_VCC_7V),\
    (DTC_IUG_DEV_HINT),\
    (DTC_IUG_SW_ERROR),\
    (DTC_IUG_NVM_ERROR),\
    (DTC_ERR_PMIC_POWER_STATUS),\
}


#endif 

