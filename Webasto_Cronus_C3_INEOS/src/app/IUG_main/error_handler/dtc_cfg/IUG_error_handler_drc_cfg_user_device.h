#ifndef _IUG_ERR_HDL_DRC_CFG_USER_DEVICE_H_
#define _IUG_ERR_HDL_DRC_CFG_USER_DEVICE_H_

#include "IUG_include.h"


#define IUG_ERRHDL_DRC_CFG_USER_DEVICE \
{\
\
\
\
\
\
{{DTC_ERR_VCC_7V}                        ,{DRC___, DRCLPH, DRCLPH, DRCLPH, DRCLPH, DRCLPH, DRCLPH, DRCLPH,  DRCLPH, DRCLPH, DRCLPH, DRCLPH, DRCLPH, DRC___,DRC___,DRC___}},\
\
{{DTC_ERR_KL30_ADC_OV}                   ,{DRC___, DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRCINH, DRCERR,  DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
{{DTC_ERR_KL30_ADC_UV}                   ,{DRC___, DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRCINH, DRCERR,  DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
\
{{DTC_IUG_DEV_HINT}                      ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_IUG_SW_ERROR}                      ,{DRC___, DRCLPH, DRCLPH, DRCLPH, DRCLPH, DRCLPH, DRCLPH, DRCLPH,  DRCLPH, DRCLPH, DRCLPH, DRCLPH, DRCLPH, DRC___,DRC___,DRC___}},\
{{DTC_IUG_NVM_ERROR}                     ,{DRC___, DRCLPH, DRCLPH, DRCLPH, DRCLPH, DRCLPH, DRCLPH, DRCLPH,  DRCLPH, DRCLPH, DRCLPH, DRCLPH, DRCLPH, DRC___,DRC___,DRC___}},\
\
\
\
\
{{DTC_ERR_VDD12_BE_OV}                   ,{DRC___, DRCINH, DRCERR, DRCERR, DRCERR, DRC___, DRC___, DRC___,  DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
{{DTC_ERR_VDD12_BE_UV}                   ,{DRC___, DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRCINH, DRC___,  DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
{{DTC_ERR_VDD12_BE_OC}                   ,{DRC___, DRCINH, DRCERR, DRCERR, DRCERR, DRC___, DRCINH, DRC___,  DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
{{DTC_ERR_PMIC_POWER_STATUS}             ,{DRC___, DRCLPH, DRCLPH, DRCLPH, DRCLPH, DRCLPH, DRCLPH, DRCLPH,  DRCLPH, DRCLPH, DRCLPH, DRCLPH, DRCLPH, DRC___,DRC___,DRC___}},\
{{DTC_ERR_IO1_LED_OC}                    ,{DRC___, DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRCINH, DRCERR,  DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
{{DTC_ERR_IO2_OC}                        ,{DRC___, DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR,  DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
{{DTC_ERR_IO3_OC}                        ,{DRC___, DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR,  DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
{{DTC_ERR_IO4_OC}                        ,{DRC___, DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR,  DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
\
\
\
\
{{DTC_ERR_REL1_OC}                       ,{DRC___, DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR,  DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
{{DTC_ERR_REL2_OC}                       ,{DRC___, DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR,  DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
{{DTC_ERR_OUT_OC}                        ,{DRC___, DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR,  DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
{{DTC_ERR_PUSHBUTTON}                    ,{DRC___, DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR,  DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
{{DTC_ERR_PWM_IN_SIGNAL_CHECK}           ,{DRC___, DRC___, DRC___, DRC___, DRCERR, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_PWM_OUT_LIN2_SHORT}            ,{DRC___, DRC___, DRC___, DRC___, DRCERR, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_PWM_OUT_IO3_SHORT}             ,{DRC___, DRC___, DRC___, DRC___, DRCERR, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_PRESSURE_COM}                  ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
\
\
\
\
\
\
\
\
{{DTC_ERR_LIN1_COM}                      ,{DRC___, DRC___, DRC___, DRC___, DRCERR, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
\
{{DTC_ERR_LIN2_COM}                      ,{DRC___, DRCERR, DRC___, DRC___, DRCERR, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
\
\
\
\
\
\
\
\
{{DTC_ERR_LIN5_COM}                      ,{DRC___, DRCINH, DRC___, DRC___, DRCERR, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_CAN1_COM}                      ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_CAN2_COM}                      ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_CAN3_COM}                      ,{DRC___, DRCINH, DRCERR, DRCERR, DRC___, DRC___, DRCINH, DRCERR,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_IO1_LED_OPL}                   ,{DRC___, DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR,  DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
\
\
\
\
{{DTC_ERR_IO1_LED_STBAT}                 ,{DRC___, DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR,  DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
{{DTC_ERR_TEMP_WBUS_SENS_INT_DEF}        ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_TEMP_WBUS_SENS_INT_OOR}        ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_TEMP_WBUS_SENS_EXT_DEF}        ,{DRC___, DRCINH, DRC___, DRC___, DRC___, DRC___, DRCINH, DRCERR,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_TEMP_WBUS_SENS_EXT_OOR}        ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_TEMP_BUTTON_STG}               ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_TEMP_BUTTON_STBAT}             ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_BLE_PAIRINGCODE}               ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
\
\
\
\
{{DTC_ERR_BLE_COM}                       ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
\
\
{{DTC_ERR_OBD_CONFIG}                    ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_OBD_REQ}                       ,{DRC___, DRCINH, DRCERR, DRCERR, DRC___, DRC___, DRCINH, DRCERR,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
{{DTC_ERR_LIN4_WBUS_COM_MUL_CTL_3TO4}    ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRCERR,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_LIN4_WBUS_COM_MUL_CTL_3TO5}    ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_LIN4_WBUS_COM_REMOTE_CTL}      ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_LIN4_WBUS_COM_THERMO_CALL}     ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
\
{{DTC_ERR_USR_DEV_ACTION_CANCELLED}      ,{DRC___, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR,  DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
{{DTC_ERR_USR_DEV_NO_HEARTBEAT_RESPOND}  ,{DRC___, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR,  DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
{{DTC_ERR_USR_DEV_BE_NO_CMD_CONTINUE}    ,{DRC___, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR,  DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
\
\
\
\
\
}


#endif 


