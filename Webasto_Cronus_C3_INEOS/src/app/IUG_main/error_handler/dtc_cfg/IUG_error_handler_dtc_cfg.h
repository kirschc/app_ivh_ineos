#ifndef _IUG_ERROR_HANDLER_DTC_CFG_H_
#define _IUG_ERROR_HANDLER_DTC_CFG_H_




#define DTC_ERR_GHI_COM_HGX_SHARED                  (DTC_ERR_GHI_HGX_COM)
#define DTC_ERR_GHI_STFL_HGX_SHARED                 (DTC_ERR_GHI_HGX_STFL)
#define DTC_ERR_GHI_HGV_HGX_SHARED                  (DTC_ERR_GHI_HGX_HGV)
#define DTC_ERR_GHI_UEHFL_HGX_SHARED                (DTC_ERR_GHI_HGX_UEHFL)
#define DTC_ERR_GHI_ACTION_CANCELLED_HGX_SHARED     (DTC_ERR_GHI_ACTION_CANCELLED_HGX)
#define DTC_ERR_GHI_NO_HEARTBEAT_RESP_HGX_SHARED    (DTC_ERR_GHI_NO_HEARTBEAT_RESPOND_HGX)
#define DTC_ERR_GHI_BE_NO_CMD_CONTINUE_HGX_SHARED   (DTC_ERR_GHI_BE_NO_CMD_CONTINUE_TO_HGX)

#define DTC_ERR_T_LIN_R_COM_INT_EXT_SHARED          (DTC_ERR_LIN4_WBUS_COM_T_LIN_R)
#define DTC_ERR_T_LIN_R_MISMATCH_INT_EXT_SHARED     (DTC_ERR_IBN_T_LIN_R_MISMATCH)




#define IUG_ERROR_HANDLER_USR_API_FIRST_DTC_ID  (DTC_USR_API_112)






extern const IUG_dtc_reaction_configuration_t       IUG_dtc_err_reaction_cfg_iug_def        [];
extern const IUG_dtc_reaction_configuration_t       IUG_dtc_err_reaction_cfg_temp_sensor_int[];
extern const IUG_dtc_reaction_configuration_t       IUG_dtc_err_reaction_cfg_temp_sensor_ext[];
extern const IUG_dtc_reaction_configuration_t       IUG_dtc_err_reaction_cfg_wbus_def       [];
extern const IUG_dtc_reaction_configuration_t       IUG_dtc_err_reaction_cfg_ghi_def        [];
extern const IUG_dtc_reaction_configuration_t       IUG_dtc_err_reaction_cfg_ghi_hvh        [];

extern const IUG_htr_state_to_drc_assignment_t      mgl_iug_htr_state_air_to_idx_drc[];
extern const IUG_htr_state_to_drc_assignment_t      mgl_iug_htr_state_wat_to_idx_drc[];

extern const uint16_t   ext_iug_entries_IUG_dtc_err_reaction_air_cfg;
extern const uint16_t   ext_iug_entries_IUG_dtc_err_reaction_wat_cfg;
extern const uint16_t   ext_iug_entries_IUG_dtc_err_reaction_cfg_ghi_def;
extern const uint16_t   ext_iug_entries_IUG_dtc_err_reaction_cfg_ghi_hvh;


#endif 


