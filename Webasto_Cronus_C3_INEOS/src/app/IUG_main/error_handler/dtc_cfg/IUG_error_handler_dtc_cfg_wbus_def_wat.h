
#include "IUG_include.h"


#define IUG_ERRHDL_WBUS_DEF_WAT_DTC_list_COM_HGX_CFG \
{ \
    (DTC_ERR_LIN3_WBUS_COM_HG1),    \
    (DTC_ERR_LIN3_WBUS_COM_HG2),    \
    (DTC_ENUM_MAX),                 \
    (DTC_ENUM_MAX),                 \
    (DTC_ENUM_MAX),                 \
    (DTC_ENUM_MAX),                 \
    (DTC_ENUM_MAX),                 \
    (DTC_ENUM_MAX)                  \
}


#define IUG_ERRHDL_WBUS_DEF_WAT_DTC_list_STFL_HGX_CFG \
{ \
    (DTC_ERR_HG1_STFL),             \
    (DTC_ERR_HG2_STFL),             \
    (DTC_ENUM_MAX),                 \
    (DTC_ENUM_MAX),                 \
    (DTC_ENUM_MAX),                 \
    (DTC_ENUM_MAX),                 \
    (DTC_ENUM_MAX),                 \
    (DTC_ENUM_MAX)                  \
}


#define IUG_ERRHDL_WBUS_DEF_WAT_DTC_list_HGV_HGX_CFG \
{ \
    (DTC_ERR_HG1_HGV),              \
    (DTC_ERR_HG2_HGV),              \
    (DTC_ENUM_MAX),                 \
    (DTC_ENUM_MAX),                 \
    (DTC_ENUM_MAX),                 \
    (DTC_ENUM_MAX),                 \
    (DTC_ENUM_MAX),                 \
    (DTC_ENUM_MAX)                  \
}


#define IUG_ERRHDL_WBUS_DEF_WAT_DTC_list_UEHFL_HGX_CFG \
{ \
    (DTC_ERR_HG1_UEHFL),            \
    (DTC_ERR_HG2_UEHFL),            \
    (DTC_ENUM_MAX),                 \
    (DTC_ENUM_MAX),                 \
    (DTC_ENUM_MAX),                 \
    (DTC_ENUM_MAX),                 \
    (DTC_ENUM_MAX),                 \
    (DTC_ENUM_MAX)                  \
}


#define IUG_ERRHDL_WBUS_DEF_WAT_DTC_list_BE_NO_CMD_CONTINUE_HGX_CFG \
{ \
    (DTC_ERR_WBUSHG1_BE_NO_CMD_CONTINUE),       \
    (DTC_ERR_WBUSHG2_BE_NO_CMD_CONTINUE),       \
    (DTC_ERR_WBUSHG3_BE_NO_CMD_CONTINUE),       \
    (DTC_ERR_WBUSHG4_BE_NO_CMD_CONTINUE),       \
    (DTC_ERR_WBUSHG5_BE_NO_CMD_CONTINUE),       \
    (DTC_ERR_WBUSHG6_BE_NO_CMD_CONTINUE),       \
    (DTC_ERR_WBUSHG7_BE_NO_CMD_CONTINUE),       \
    (DTC_ERR_WBUSHG8_BE_NO_CMD_CONTINUE)        \
}


#define IUG_ERRHDL_WBUS_DEF_WAT_DTC_list_PND_TIMEOUT_HGX_CFG \
{ \
    (DTC_ERR_WBUSHG1_NO_HEARTBEAT_RESP),    \
    (DTC_ERR_WBUSHG2_NO_HEARTBEAT_RESP),    \
    (DTC_ERR_WBUSHG3_NO_HEARTBEAT_RESP),    \
    (DTC_ERR_WBUSHG4_NO_HEARTBEAT_RESP),    \
    (DTC_ERR_WBUSHG5_NO_HEARTBEAT_RESP),    \
    (DTC_ERR_WBUSHG6_NO_HEARTBEAT_RESP),    \
    (DTC_ERR_WBUSHG7_NO_HEARTBEAT_RESP),    \
    (DTC_ERR_WBUSHG8_NO_HEARTBEAT_RESP)     \
}


#define IUG_ERRHDL_WBUS_DEF_WAT_DTC_list_PND_ABORT_HGX_CFG \
{ \
    (DTC_ERR_WBUSHG1_ACTIVITY_CANCELLED),       \
    (DTC_ERR_WBUSHG2_ACTIVITY_CANCELLED),       \
    (DTC_ERR_WBUSHG3_ACTIVITY_CANCELLED),       \
    (DTC_ERR_WBUSHG4_ACTIVITY_CANCELLED),       \
    (DTC_ERR_WBUSHG5_ACTIVITY_CANCELLED),       \
    (DTC_ERR_WBUSHG6_ACTIVITY_CANCELLED),       \
    (DTC_ERR_WBUSHG7_ACTIVITY_CANCELLED),       \
    (DTC_ERR_WBUSHG8_ACTIVITY_CANCELLED)        \
}


#define IUG_ERRHDL_WBUS_DEF_WAT_DTC_list_SERIAL_NUMBER_HGX_CFG \
{ \
    (DTC_ERR_LIN3_WBUS_WRONG_HG1_DETECTED),     \
    (DTC_ERR_LIN3_WBUS_WRONG_HG2_DETECTED),     \
    (DTC_ENUM_MAX),                             \
    (DTC_ENUM_MAX),                             \
    (DTC_ENUM_MAX),                             \
    (DTC_ENUM_MAX),                             \
    (DTC_ENUM_MAX),                             \
    (DTC_ENUM_MAX)                              \
}



#define IUG_ERRHDL_WBUS_DEF_WAT_DTC_list_GENERAL \
{ \
    \
    \
    (DTC_ERR_KL30_ADC_OV),\
    (DTC_ERR_KL30_ADC_UV),\
    (DTC_ERR_IO1_LED_OC),\
    (DTC_ERR_IO2_OC),\
    (DTC_ERR_IO3_OC),\
    (DTC_ERR_IO4_OC),\
    (DTC_ERR_REL1_OC),\
    (DTC_ERR_REL2_OC),\
    (DTC_ERR_OUT_OC),\
    (DTC_ERR_PWM_OUT_LIN2_SHORT),       \
    (DTC_ERR_PWM_OUT_IO3_SHORT),        \
    (DTC_ERR_PRESSURE_COM),             \
    (DTC_ERR_LIN1_COM),\
    (DTC_ERR_LIN2_COM),\
    (DTC_ERR_LIN3_WBUS_MD),             \
    (DTC_ERR_LIN4_WBUS_MD),             \
    (DTC_ERR_LIN5_COM),\
    (DTC_ERR_CAN1_COM),\
    (DTC_ERR_CAN2_COM),\
    (DTC_ERR_CAN3_COM),\
    \
    \
    \
    \
}


