#ifndef _IUG_ERROR_HANDLER_DTC_CFG_GHI_HVH_AIR_H_
#define _IUG_ERROR_HANDLER_DTC_CFG_GHI_HVH_AIR_H_





#ifdef DTC_ERR_GHI_COM_HGX_SHARED
    #define IUG_ERRHDL_GHI_HVH_AIR_DTC_list_COM_HGX \
        (DTC_ERR_GHI_COM_HGX_SHARED),   \
        (DTC_ERR_GHI_COM_HGX_SHARED),   \
        (DTC_ERR_GHI_COM_HGX_SHARED),   \
        (DTC_ERR_GHI_COM_HGX_SHARED),   \
        (DTC_ERR_GHI_COM_HGX_SHARED),   \
        (DTC_ERR_GHI_COM_HGX_SHARED),   \
        (DTC_ERR_GHI_COM_HGX_SHARED),   \
        (DTC_ERR_GHI_COM_HGX_SHARED),   
#else
    #define IUG_ERRHDL_GHI_HVH_AIR_DTC_list_COM_HGX \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   
#endif

#define IUG_ERRHDL_GHI_HVH_AIR_DTC_list_COM_HGX_CFG \
{ \
    ##IUG_ERRHDL_GHI_HVH_AIR_DTC_list_COM_HGX       \
}


#ifdef DTC_ERR_GHI_STFL_HGX_SHARED
    #define IUG_ERRHDL_GHI_HVH_AIR_DTC_list_STFL_HGX \
        (DTC_ERR_GHI_STFL_HGX_SHARED),   \
        (DTC_ERR_GHI_STFL_HGX_SHARED),   \
        (DTC_ERR_GHI_STFL_HGX_SHARED),   \
        (DTC_ERR_GHI_STFL_HGX_SHARED),   \
        (DTC_ERR_GHI_STFL_HGX_SHARED),   \
        (DTC_ERR_GHI_STFL_HGX_SHARED),   \
        (DTC_ERR_GHI_STFL_HGX_SHARED),   \
        (DTC_ERR_GHI_STFL_HGX_SHARED),   
#else
    #define IUG_ERRHDL_GHI_HVH_AIR_DTC_list_STFL_HGX \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   
#endif

#define IUG_ERRHDL_GHI_HVH_AIR_DTC_list_STFL_HGX_CFG \
{ \
    ##IUG_ERRHDL_GHI_HVH_AIR_DTC_list_STFL_HGX       \
}


#ifdef DTC_ERR_GHI_HGV_HGX_SHARED
    #define IUG_ERRHDL_GHI_HVH_AIR_DTC_list_HGV_HGX \
        (DTC_ERR_GHI_HGV_HGX_SHARED),   \
        (DTC_ERR_GHI_HGV_HGX_SHARED),   \
        (DTC_ERR_GHI_HGV_HGX_SHARED),   \
        (DTC_ERR_GHI_HGV_HGX_SHARED),   \
        (DTC_ERR_GHI_HGV_HGX_SHARED),   \
        (DTC_ERR_GHI_HGV_HGX_SHARED),   \
        (DTC_ERR_GHI_HGV_HGX_SHARED),   \
        (DTC_ERR_GHI_HGV_HGX_SHARED),   
#else
    #define IUG_ERRHDL_GHI_HVH_AIR_DTC_list_HGV_HGX \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   
#endif

#define IUG_ERRHDL_GHI_HVH_AIR_DTC_list_HGV_HGX_CFG \
{ \
    ##IUG_ERRHDL_GHI_HVH_AIR_DTC_list_HGV_HGX       \
}


#ifdef DTC_ERR_GHI_UEHFL_HGX_SHARED
    #define IUG_ERRHDL_GHI_HVH_AIR_DTC_list_UEHFL_HGX \
        (DTC_ERR_GHI_UEHFL_HGX_SHARED),   \
        (DTC_ERR_GHI_UEHFL_HGX_SHARED),   \
        (DTC_ERR_GHI_UEHFL_HGX_SHARED),   \
        (DTC_ERR_GHI_UEHFL_HGX_SHARED),   \
        (DTC_ERR_GHI_UEHFL_HGX_SHARED),   \
        (DTC_ERR_GHI_UEHFL_HGX_SHARED),   \
        (DTC_ERR_GHI_UEHFL_HGX_SHARED),   \
        (DTC_ERR_GHI_UEHFL_HGX_SHARED),   
#else
    #define IUG_ERRHDL_GHI_HVH_AIR_DTC_list_UEHFL_HGX \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   
#endif

#define IUG_ERRHDL_GHI_HVH_AIR_DTC_list_UEHFL_HGX_CFG \
{ \
    ##IUG_ERRHDL_GHI_HVH_AIR_DTC_list_UEHFL_HGX       \
}


#ifndef DTC_ERR_GHI_BE_NO_CMD_CONTINUE_HGX_SHARED
    #define IUG_ERRHDL_GHI_HVH_AIR_DTC_list_BE_NO_CMD_CONT_HGX \
        (DTC_ERR_GHIHVHAIR1_BE_NO_CMD_CONTINUE),        \
        (DTC_ERR_GHIHVHAIR2_BE_NO_CMD_CONTINUE),        \
        (DTC_ERR_GHIHVHAIR3_BE_NO_CMD_CONTINUE),        \
        (DTC_ERR_GHIHVHAIR4_BE_NO_CMD_CONTINUE),        \
        (DTC_ERR_GHIHVHAIR5_BE_NO_CMD_CONTINUE),        \
        (DTC_ERR_GHIHVHAIR6_BE_NO_CMD_CONTINUE),        \
        (DTC_ERR_GHIHVHAIR7_BE_NO_CMD_CONTINUE),        \
        (DTC_ERR_GHIHVHAIR8_BE_NO_CMD_CONTINUE)         
#else
    #define IUG_ERRHDL_GHI_HVH_AIR_DTC_list_BE_NO_CMD_CONT_HGX \
        (DTC_ERR_GHIHVHAIR1_BE_NO_CMD_CONTINUE),        \
        (DTC_ERR_GHIHVHAIR2_BE_NO_CMD_CONTINUE),        \
        (DTC_ERR_GHI_BE_NO_CMD_CONTINUE_HGX_SHARED),    \
        (DTC_ERR_GHI_BE_NO_CMD_CONTINUE_HGX_SHARED),    \
        (DTC_ERR_GHI_BE_NO_CMD_CONTINUE_HGX_SHARED),    \
        (DTC_ERR_GHI_BE_NO_CMD_CONTINUE_HGX_SHARED),    \
        (DTC_ERR_GHI_BE_NO_CMD_CONTINUE_HGX_SHARED),    \
        (DTC_ERR_GHI_BE_NO_CMD_CONTINUE_HGX_SHARED)     
#endif

#define IUG_ERRHDL_GHI_HVH_AIR_DTC_list_BE_NO_CMD_CONT_HGX_CFG \
{ \
    ##IUG_ERRHDL_GHI_HVH_AIR_DTC_list_BE_NO_CMD_CONT_HGX       \
}


#ifndef DTC_ERR_GHI_NO_HEARTBEAT_RESP_HGX_SHARED
    #define IUG_ERRHDL_GHI_HVH_AIR_DTC_list_PND_TIMEOUT_HGX \
        (DTC_ERR_GHIHVHAIR1_NO_HEARTBEAT_RESP),         \
        (DTC_ERR_GHIHVHAIR2_NO_HEARTBEAT_RESP),         \
        (DTC_ERR_GHIHVHAIR3_NO_HEARTBEAT_RESP),         \
        (DTC_ERR_GHIHVHAIR4_NO_HEARTBEAT_RESP),         \
        (DTC_ERR_GHIHVHAIR5_NO_HEARTBEAT_RESP),         \
        (DTC_ERR_GHIHVHAIR6_NO_HEARTBEAT_RESP),         \
        (DTC_ERR_GHIHVHAIR7_NO_HEARTBEAT_RESP),         \
        (DTC_ERR_GHIHVHAIR8_NO_HEARTBEAT_RESP),         
#else
    #define IUG_ERRHDL_GHI_HVH_AIR_DTC_list_PND_TIMEOUT_HGX \
        (DTC_ERR_GHIHVHAIR1_NO_HEARTBEAT_RESP),         \
        (DTC_ERR_GHIHVHAIR2_NO_HEARTBEAT_RESP),         \
        (DTC_ERR_GHI_NO_HEARTBEAT_RESP_HGX_SHARED),     \
        (DTC_ERR_GHI_NO_HEARTBEAT_RESP_HGX_SHARED),     \
        (DTC_ERR_GHI_NO_HEARTBEAT_RESP_HGX_SHARED),     \
        (DTC_ERR_GHI_NO_HEARTBEAT_RESP_HGX_SHARED),     \
        (DTC_ERR_GHI_NO_HEARTBEAT_RESP_HGX_SHARED),     \
        (DTC_ERR_GHI_NO_HEARTBEAT_RESP_HGX_SHARED),     
#endif

#define IUG_ERRHDL_GHI_HVH_AIR_DTC_list_PND_TIMEOUT_HGX_CFG \
{ \
    ##IUG_ERRHDL_GHI_HVH_AIR_DTC_list_PND_TIMEOUT_HGX       \
}


#ifndef DTC_ERR_GHI_ACTION_CANCELLED_HGX_SHARED
    #define IUG_ERRHDL_GHI_HVH_AIR_DTC_list_PND_ABORT_HGX \
        (DTC_ERR_GHIHVHAIR1_ACTIVITY_CANCELLED),        \
        (DTC_ERR_GHIHVHAIR2_ACTIVITY_CANCELLED),        \
        (DTC_ERR_GHIHVHAIR3_ACTIVITY_CANCELLED),        \
        (DTC_ERR_GHIHVHAIR4_ACTIVITY_CANCELLED),        \
        (DTC_ERR_GHIHVHAIR5_ACTIVITY_CANCELLED),        \
        (DTC_ERR_GHIHVHAIR6_ACTIVITY_CANCELLED),        \
        (DTC_ERR_GHIHVHAIR7_ACTIVITY_CANCELLED),        \
        (DTC_ERR_GHIHVHAIR8_ACTIVITY_CANCELLED)         
#else
    #define IUG_ERRHDL_GHI_HVH_AIR_DTC_list_PND_ABORT_HGX \
        (DTC_ERR_GHIHVHAIR1_ACTIVITY_CANCELLED),        \
        (DTC_ERR_GHIHVHAIR2_ACTIVITY_CANCELLED),        \
        (DTC_ERR_GHI_ACTION_CANCELLED_HGX_SHARED),      \
        (DTC_ERR_GHI_ACTION_CANCELLED_HGX_SHARED),      \
        (DTC_ERR_GHI_ACTION_CANCELLED_HGX_SHARED),      \
        (DTC_ERR_GHI_ACTION_CANCELLED_HGX_SHARED),      \
        (DTC_ERR_GHI_ACTION_CANCELLED_HGX_SHARED),      \
        (DTC_ERR_GHI_ACTION_CANCELLED_HGX_SHARED)       
#endif

#define IUG_ERRHDL_GHI_HVH_AIR_DTC_list_PND_ABORT_HGX_CFG \
{ \
    ##IUG_ERRHDL_GHI_HVH_AIR_DTC_list_PND_ABORT_HGX       \
}


#define IUG_ERRHDL_GHI_HVH_AIR_DTC_LIST_GENERAL \
{ \
    \
    \
    (DTC_ERR_KL30_ADC_OV),\
    (DTC_ERR_KL30_ADC_UV),\
    (DTC_ERR_IO1_LED_OC),\
    (DTC_ERR_IO2_OC),\
    (DTC_ERR_IO3_OC),\
    (DTC_ERR_IO4_OC),\
    (DTC_ERR_REL1_OC),\
    (DTC_ERR_REL2_OC),\
    (DTC_ERR_OUT_OC),\
    (DTC_ERR_PWM_OUT_LIN2_SHORT),       \
    (DTC_ERR_PWM_OUT_IO3_SHORT),        \
    (DTC_ERR_PRESSURE_COM),             \
    (DTC_ERR_LIN1_COM),\
    (DTC_ERR_LIN2_COM),\
    (DTC_ERR_LIN3_WBUS_MD),             \
    (DTC_ERR_LIN4_WBUS_MD),             \
    (DTC_ERR_LIN5_COM),\
    (DTC_ERR_CAN1_COM),\
    (DTC_ERR_CAN2_COM),\
    (DTC_ERR_CAN3_COM) \
    \
    \
    \
    \
}






#endif 


