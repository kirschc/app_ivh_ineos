#ifndef _IUG_ERROR_HANDLER_DTC_CFG_GHI_TT_EVO_GEN4_WAT_H_
#define _IUG_ERROR_HANDLER_DTC_CFG_GHI_TT_EVO_GEN4_WAT_H_





#ifdef DTC_ERR_GHI_COM_HGX_SHARED
    #define IUG_ERRHDL_GHI_TT_EVO_GEN4_WAT_DTC_list_COM_HGX \
        (DTC_ERR_GHI_COM_HGX_SHARED),   \
        (DTC_ERR_GHI_COM_HGX_SHARED),   \
        (DTC_ERR_GHI_COM_HGX_SHARED),   \
        (DTC_ERR_GHI_COM_HGX_SHARED),   \
        (DTC_ERR_GHI_COM_HGX_SHARED),   \
        (DTC_ERR_GHI_COM_HGX_SHARED),   \
        (DTC_ERR_GHI_COM_HGX_SHARED),   \
        (DTC_ERR_GHI_COM_HGX_SHARED),   
#else
    #define IUG_ERRHDL_GHI_TT_EVO_GEN4_WAT_DTC_list_COM_HGX \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   
#endif

#define IUG_ERRHDL_GHI_TT_EVO_GEN4_WAT_DTC_list_COM_HGX_CFG \
{ \
    ##IUG_ERRHDL_GHI_TT_EVO_GEN4_WAT_DTC_list_COM_HGX       \
}


#ifdef DTC_ERR_GHI_STFL_HGX_SHARED
    #define IUG_ERRHDL_GHI_TT_EVO_GEN4_WAT_DTC_list_STFL_HGX \
        (DTC_ERR_GHI_STFL_HGX_SHARED),   \
        (DTC_ERR_GHI_STFL_HGX_SHARED),   \
        (DTC_ERR_GHI_STFL_HGX_SHARED),   \
        (DTC_ERR_GHI_STFL_HGX_SHARED),   \
        (DTC_ERR_GHI_STFL_HGX_SHARED),   \
        (DTC_ERR_GHI_STFL_HGX_SHARED),   \
        (DTC_ERR_GHI_STFL_HGX_SHARED),   \
        (DTC_ERR_GHI_STFL_HGX_SHARED),   
#else
    #define IUG_ERRHDL_GHI_TT_EVO_GEN4_WAT_DTC_list_STFL_HGX \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   
#endif

#define IUG_ERRHDL_GHI_TT_EVO_GEN4_WAT_DTC_list_STFL_HGX_CFG \
{ \
    ##IUG_ERRHDL_GHI_TT_EVO_GEN4_WAT_DTC_list_STFL_HGX       \
}


#ifdef DTC_ERR_GHI_HGV_HGX_SHARED
    #define IUG_ERRHDL_GHI_TT_EVO_GEN4_WAT_DTC_list_HGV_HGX \
        (DTC_ERR_GHI_HGV_HGX_SHARED),   \
        (DTC_ERR_GHI_HGV_HGX_SHARED),   \
        (DTC_ERR_GHI_HGV_HGX_SHARED),   \
        (DTC_ERR_GHI_HGV_HGX_SHARED),   \
        (DTC_ERR_GHI_HGV_HGX_SHARED),   \
        (DTC_ERR_GHI_HGV_HGX_SHARED),   \
        (DTC_ERR_GHI_HGV_HGX_SHARED),   \
        (DTC_ERR_GHI_HGV_HGX_SHARED),   
#else
    #define IUG_ERRHDL_GHI_TT_EVO_GEN4_WAT_DTC_list_HGV_HGX \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   
#endif

#define IUG_ERRHDL_GHI_TT_EVO_GEN4_WAT_DTC_list_HGV_HGX_CFG \
{ \
    ##IUG_ERRHDL_GHI_TT_EVO_GEN4_WAT_DTC_list_HGV_HGX       \
}


#ifdef DTC_ERR_GHI_UEHFL_HGX_SHARED
    #define IUG_ERRHDL_GHI_TT_EVO_GEN4_WAT_DTC_list_UEHFL_HGX \
        (DTC_ERR_GHI_UEHFL_HGX_SHARED),   \
        (DTC_ERR_GHI_UEHFL_HGX_SHARED),   \
        (DTC_ERR_GHI_UEHFL_HGX_SHARED),   \
        (DTC_ERR_GHI_UEHFL_HGX_SHARED),   \
        (DTC_ERR_GHI_UEHFL_HGX_SHARED),   \
        (DTC_ERR_GHI_UEHFL_HGX_SHARED),   \
        (DTC_ERR_GHI_UEHFL_HGX_SHARED),   \
        (DTC_ERR_GHI_UEHFL_HGX_SHARED),   
#else
    #define IUG_ERRHDL_GHI_TT_EVO_GEN4_WAT_DTC_list_UEHFL_HGX \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   \
        (DTC_ENUM_MAX),   
#endif

#define IUG_ERRHDL_GHI_TT_EVO_GEN4_WAT_DTC_list_UEHFL_HGX_CFG \
{ \
    ##IUG_ERRHDL_GHI_TT_EVO_GEN4_WAT_DTC_list_UEHFL_HGX       \
}


#ifndef DTC_ERR_GHI_BE_NO_CMD_CONTINUE_HGX_SHARED
    #define IUG_ERRHDL_GHI_TT_EVO_GEN4_WAT_DTC_list_BE_NO_CMD_CONT_HGX \
        (DTC_ERR_GHIHVHWAT1_BE_NO_CMD_CONTINUE),        \
        (DTC_ERR_GHIHVHWAT2_BE_NO_CMD_CONTINUE),        \
        (DTC_ERR_GHIHVHWAT3_BE_NO_CMD_CONTINUE),        \
        (DTC_ERR_GHIHVHWAT4_BE_NO_CMD_CONTINUE),        \
        (DTC_ERR_GHIHVHWAT5_BE_NO_CMD_CONTINUE),        \
        (DTC_ERR_GHIHVHWAT6_BE_NO_CMD_CONTINUE),        \
        (DTC_ERR_GHIHVHWAT7_BE_NO_CMD_CONTINUE),        \
        (DTC_ERR_GHIHVHWAT8_BE_NO_CMD_CONTINUE)         
#else
    #define IUG_ERRHDL_GHI_TT_EVO_GEN4_WAT_DTC_list_BE_NO_CMD_CONT_HGX \
        (DTC_ERR_GHIHVHWAT1_BE_NO_CMD_CONTINUE),        \
        (DTC_ERR_GHIHVHWAT2_BE_NO_CMD_CONTINUE),        \
        (DTC_ERR_GHI_BE_NO_CMD_CONTINUE_HGX_SHARED),    \
        (DTC_ERR_GHI_BE_NO_CMD_CONTINUE_HGX_SHARED),    \
        (DTC_ERR_GHI_BE_NO_CMD_CONTINUE_HGX_SHARED),    \
        (DTC_ERR_GHI_BE_NO_CMD_CONTINUE_HGX_SHARED),    \
        (DTC_ERR_GHI_BE_NO_CMD_CONTINUE_HGX_SHARED),    \
        (DTC_ERR_GHI_BE_NO_CMD_CONTINUE_HGX_SHARED)     
#endif

#define IUG_ERRHDL_GHI_TT_EVO_GEN4_WAT_DTC_list_BE_NO_CMD_CONT_HGX_CFG \
{ \
    ##IUG_ERRHDL_GHI_TT_EVO_GEN4_WAT_DTC_list_BE_NO_CMD_CONT_HGX       \
}


#ifndef DTC_ERR_GHI_NO_HEARTBEAT_RESP_HGX_SHARED
    #define IUG_ERRHDL_GHI_TT_EVO_GEN4_WAT_DTC_list_PND_TIMEOUT_HGX \
        (DTC_ERR_GHIHVHWAT1_NO_HEARTBEAT_RESP),         \
        (DTC_ERR_GHIHVHWAT2_NO_HEARTBEAT_RESP),         \
        (DTC_ERR_GHIHVHWAT3_NO_HEARTBEAT_RESP),         \
        (DTC_ERR_GHIHVHWAT4_NO_HEARTBEAT_RESP),         \
        (DTC_ERR_GHIHVHWAT5_NO_HEARTBEAT_RESP),         \
        (DTC_ERR_GHIHVHWAT6_NO_HEARTBEAT_RESP),         \
        (DTC_ERR_GHIHVHWAT7_NO_HEARTBEAT_RESP),         \
        (DTC_ERR_GHIHVHWAT8_NO_HEARTBEAT_RESP),         
#else
    #define IUG_ERRHDL_GHI_TT_EVO_GEN4_WAT_DTC_list_PND_TIMEOUT_HGX \
        (DTC_ERR_GHIHVHWAT1_NO_HEARTBEAT_RESP),         \
        (DTC_ERR_GHIHVHWAT2_NO_HEARTBEAT_RESP),         \
        (DTC_ERR_GHI_NO_HEARTBEAT_RESP_HGX_SHARED),     \
        (DTC_ERR_GHI_NO_HEARTBEAT_RESP_HGX_SHARED),     \
        (DTC_ERR_GHI_NO_HEARTBEAT_RESP_HGX_SHARED),     \
        (DTC_ERR_GHI_NO_HEARTBEAT_RESP_HGX_SHARED),     \
        (DTC_ERR_GHI_NO_HEARTBEAT_RESP_HGX_SHARED),     \
        (DTC_ERR_GHI_NO_HEARTBEAT_RESP_HGX_SHARED),     
#endif

#define IUG_ERRHDL_GHI_TT_EVO_GEN4_WAT_DTC_list_PND_TIMEOUT_HGX_CFG \
{ \
    ##IUG_ERRHDL_GHI_TT_EVO_GEN4_WAT_DTC_list_PND_TIMEOUT_HGX       \
}


#ifndef DTC_ERR_GHI_ACTION_CANCELLED_HGX_SHARED
    #define IUG_ERRHDL_GHI_TT_EVO_GEN4_WAT_DTC_list_PND_ABORT_HGX \
        (DTC_ERR_GHIHVHWAT1_ACTIVITY_CANCELLED),        \
        (DTC_ERR_GHIHVHWAT2_ACTIVITY_CANCELLED),        \
        (DTC_ERR_GHIHVHWAT3_ACTIVITY_CANCELLED),        \
        (DTC_ERR_GHIHVHWAT4_ACTIVITY_CANCELLED),        \
        (DTC_ERR_GHIHVHWAT5_ACTIVITY_CANCELLED),        \
        (DTC_ERR_GHIHVHWAT6_ACTIVITY_CANCELLED),        \
        (DTC_ERR_GHIHVHWAT7_ACTIVITY_CANCELLED),        \
        (DTC_ERR_GHIHVHWAT8_ACTIVITY_CANCELLED)         
#else
    #define IUG_ERRHDL_GHI_TT_EVO_GEN4_WAT_DTC_list_PND_ABORT_HGX \
        (DTC_ERR_GHIHVHWAT1_ACTIVITY_CANCELLED),        \
        (DTC_ERR_GHIHVHWAT2_ACTIVITY_CANCELLED),        \
        (DTC_ERR_GHI_ACTION_CANCELLED_HGX_SHARED),      \
        (DTC_ERR_GHI_ACTION_CANCELLED_HGX_SHARED),      \
        (DTC_ERR_GHI_ACTION_CANCELLED_HGX_SHARED),      \
        (DTC_ERR_GHI_ACTION_CANCELLED_HGX_SHARED),      \
        (DTC_ERR_GHI_ACTION_CANCELLED_HGX_SHARED),      \
        (DTC_ERR_GHI_ACTION_CANCELLED_HGX_SHARED)       
#endif

#define IUG_ERRHDL_GHI_TT_EVO_GEN4_WAT_DTC_list_PND_ABORT_HGX_CFG \
{ \
    ##IUG_ERRHDL_GHI_TT_EVO_GEN4_WAT_DTC_list_PND_ABORT_HGX       \
}


#define IUG_ERRHDL_GHI_TT_EVO_GEN4_WAT_DTC_LIST_GENERAL \
{ \
    \
    \
    (DTC_ERR_KL30_ADC_OV),\
    (DTC_ERR_KL30_ADC_UV),\
    (DTC_ERR_IO1_LED_OC),\
    (DTC_ERR_IO2_OC),\
    (DTC_ERR_IO3_OC),\
    (DTC_ERR_IO4_OC),\
    (DTC_ERR_REL1_OC),\
    (DTC_ERR_REL2_OC),\
    (DTC_ERR_OUT_OC),\
    (DTC_ERR_PWM_OUT_LIN2_SHORT),       \
    (DTC_ERR_PWM_OUT_IO3_SHORT),        \
    (DTC_ERR_PRESSURE_COM),             \
    (DTC_ERR_LIN1_COM),\
    (DTC_ERR_LIN2_COM),\
    (DTC_ERR_LIN3_WBUS_MD),             \
    (DTC_ERR_LIN4_WBUS_MD),             \
    (DTC_ERR_LIN5_COM),\
    (DTC_ERR_CAN1_COM),\
    (DTC_ERR_CAN2_COM),\
    (DTC_ERR_CAN3_COM) \
    \
    \
    \
    \
}






#endif 


