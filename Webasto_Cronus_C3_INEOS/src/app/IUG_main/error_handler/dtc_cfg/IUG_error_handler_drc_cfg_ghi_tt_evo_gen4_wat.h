#ifndef _IUG_ERROR_HANDLER_DRC_CFG_GHI_TT_EVO_GEN4_WAT_H_
#define _IUG_ERROR_HANDLER_DRC_CFG_GHI_TT_EVO_GEN4_WAT_H_




#define IUG_ERRHDL_DRC_CFG_GHI_TT_EVO_GEN4_WAT \
{\
\
\
\
\
\
{{DTC_ERR_VCC_7V}                        ,{DRC___, DRCLPH, DRCLPH, DRCLPH, DRCLPH, DRCLPH, DRCLPH, DRCLPH,  DRCLPH, DRCLPH, DRCLPH, DRCLPH, DRCLPH, DRC___,DRC___,DRC___}},\
\
{{DTC_ERR_KL30_ADC_OV}                   ,{DRC___, DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRCINH, DRCERR,  DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
{{DTC_ERR_KL30_ADC_UV}                   ,{DRC___, DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRCINH, DRCERR,  DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
\
{{DTC_IUG_DEV_HINT}                      ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_IUG_SW_ERROR}                      ,{DRC___, DRCLPH, DRCLPH, DRCLPH, DRCLPH, DRCLPH, DRCLPH, DRCLPH,  DRCLPH, DRCLPH, DRCLPH, DRCLPH, DRCLPH, DRC___,DRC___,DRC___}},\
{{DTC_IUG_NVM_ERROR}                     ,{DRC___, DRCLPH, DRCLPH, DRCLPH, DRCLPH, DRCLPH, DRCLPH, DRCLPH,  DRCLPH, DRCLPH, DRCLPH, DRCLPH, DRCLPH, DRC___,DRC___,DRC___}},\
\
\
\
\
{{DTC_ERR_VDD12_BE_OV}                   ,{DRC___, DRCINH, DRCERR, DRCERR, DRCERR, DRC___, DRC___, DRC___,  DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
{{DTC_ERR_VDD12_BE_UV}                   ,{DRC___, DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRCINH, DRC___,  DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
{{DTC_ERR_VDD12_BE_OC}                   ,{DRC___, DRCINH, DRCERR, DRCERR, DRCERR, DRC___, DRCINH, DRC___,  DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
{{DTC_ERR_PMIC_POWER_STATUS}             ,{DRC___, DRCLPH, DRCLPH, DRCLPH, DRCLPH, DRCLPH, DRCLPH, DRCLPH,  DRCLPH, DRCLPH, DRCLPH, DRCLPH, DRCLPH, DRC___,DRC___,DRC___}},\
{{DTC_ERR_IO1_LED_OC}                    ,{DRC___, DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRCINH, DRCERR,  DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
{{DTC_ERR_IO2_OC}                        ,{DRC___, DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR,  DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
{{DTC_ERR_IO3_OC}                        ,{DRC___, DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR,  DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
{{DTC_ERR_IO4_OC}                        ,{DRC___, DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR,  DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
\
\
\
\
{{DTC_ERR_REL1_OC}                       ,{DRC___, DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR,  DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
{{DTC_ERR_REL2_OC}                       ,{DRC___, DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR,  DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
{{DTC_ERR_OUT_OC}                        ,{DRC___, DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR,  DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
{{DTC_ERR_PUSHBUTTON}                    ,{DRC___, DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR,  DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
{{DTC_ERR_PWM_IN_SIGNAL_CHECK}           ,{DRC___, DRC___, DRC___, DRC___, DRCERR, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_PWM_OUT_LIN2_SHORT}            ,{DRC___, DRC___, DRC___, DRC___, DRCERR, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_PWM_OUT_IO3_SHORT}             ,{DRC___, DRC___, DRC___, DRC___, DRCERR, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_PRESSURE_COM}                  ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
\
\
\
\
\
\
\
\
{{DTC_ERR_LIN1_COM}                      ,{DRC___, DRC___, DRC___, DRC___, DRCERR, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
\
{{DTC_ERR_LIN2_COM}                      ,{DRC___, DRCERR, DRC___, DRC___, DRCERR, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
\
\
\
\
\
{{DTC_ERR_LIN3_WBUS_INTERRUPTION}        ,{DRC___, DRC___, DRCINH, DRCINH, DRC___, DRCERR, DRCINH, DRCERR,  DRC___, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
{{DTC_ERR_LIN3_WBUS_MD}                  ,{DRC___, DRCINH, DRCINH, DRCINH, DRC___, DRCERR, DRCERR, DRCERR,  DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
{{DTC_ERR_LIN4_WBUS_MD}                  ,{DRC___, DRCINH, DRCINH, DRCINH, DRC___, DRCERR, DRCERR, DRCERR,  DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
{{DTC_ERR_LIN5_COM}                      ,{DRC___, DRCINH, DRC___, DRC___, DRCERR, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_CAN1_COM}                      ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_CAN2_COM}                      ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_CAN3_COM}                      ,{DRC___, DRCINH, DRCERR, DRCERR, DRC___, DRC___, DRCINH, DRCERR,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_IO1_LED_OPL}                   ,{DRC___, DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR,  DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
\
\
\
\
{{DTC_ERR_IO1_LED_STBAT}                 ,{DRC___, DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR,  DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
{{DTC_ERR_TEMP_WBUS_SENS_INT_DEF}        ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_TEMP_WBUS_SENS_INT_OOR}        ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_TEMP_WBUS_SENS_EXT_DEF}        ,{DRC___, DRCINH, DRC___, DRC___, DRC___, DRC___, DRCINH, DRCERR,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_TEMP_WBUS_SENS_EXT_OOR}        ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_TEMP_BUTTON_STG}               ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_TEMP_BUTTON_STBAT}             ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_BLE_PAIRINGCODE}               ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
\
\
\
\
{{DTC_ERR_BLE_COM}                       ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_LIN3_WBUS_WRONG_HG1_DETECTED}  ,{DRC___, DRCINH, DRCERR, DRCERR, DRC___, DRCERR, DRCERR, DRCERR,  DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
{{DTC_ERR_LIN3_WBUS_WRONG_HG2_DETECTED}  ,{DRC___, DRCINH, DRCERR, DRCERR, DRC___, DRCERR, DRCERR, DRCERR,  DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
{{DTC_ERR_OBD_CONFIG}                    ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_OBD_REQ}                       ,{DRC___, DRCINH, DRCERR, DRCERR, DRC___, DRC___, DRCINH, DRCERR,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_LIN3_WBUS_SAME_HG_ADDR_DETECTED}   ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_NO_HG1_DETECTED}               ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_NO_HG2_DETECTED}               ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
\
\
\
\
{{DTC_ERR_IBN_NO_HANDHELD_TAUGHT}        ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_IBN_NO_CAR_STATE_CHANGED}      ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_IBN_OBD_WRONG_BAUDRATE}        ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_IBN_CAR_STATE_ACTIVE}          ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_IBN_CAR_STATE_INACTIVE}        ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_IBN_CANCELED_BY_HEATER}        ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_IBN_CANCELED_BY_USER}          ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_IBN_HEATER_NOT_IN_WAIT_STATE}  ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
\
\
\
\
{{DTC_ERR_IBN_HEATER_CTRL_STATE_PAUSE}   ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_IBN_CANCELED_BY_SUBNET}        ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_IBN_PUSHBUTTON_NOT_PRESSED}    ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_IBN_NOT_ALL_CONFIG_SRC_DET}    ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_IBN_T_LIN_R_MISMATCH}          ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
\
\
\
\
\
\
\
\
\
{{DTC_ERR_GHI_HGX_COM}                   ,{DRC___, DRCINH, DRCERR, DRCERR, DRC___, DRCERR, DRCERR, DRCERR,  DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
{{DTC_ERR_GHI_HGX_STFL}                  ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_LIN4_WBUS_COM_MUL_CTL_3TO4}    ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_LIN4_WBUS_COM_MUL_CTL_3TO5}    ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_LIN4_WBUS_COM_REMOTE_CTL}      ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_LIN4_WBUS_COM_THERMO_CALL}     ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
\
\
\
\
\
{{DTC_ERR_LIN4_WBUS_COM_THERMO_CON}             ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_LIN4_WBUS_COM_T_LIN_R}                ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
\
{{DTC_ERR_GHITTEVOGEN4WAT1_BE_NO_CMD_CONTINUE}  ,{DRC___,DRC___,DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR,  DRC___, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
{{DTC_ERR_GHITTEVOGEN4WAT2_BE_NO_CMD_CONTINUE}  ,{DRC___,DRC___,DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR,  DRC___, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
\
\
\
\
\
\
\
\
{{DTC_ERR_GHITTEVOGEN4WATX_ACTIVITY_CANCELLED}  ,{DRC___, DRC___, DRCERR, DRCERR, DRCERR, DRCERR, DRC___, DRCERR,  DRC___, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
{{DTC_ERR_GHITTEVOGEN4WATX_NO_HEARTBEAT_RESP}   ,{DRC___, DRC___, DRCERR, DRCERR, DRCERR, DRCERR, DRC___, DRCERR,  DRC___, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
{{DTC_ERR_GHITTEVOGEN4WATX_BE_NO_CMD_CONTINUE}  ,{DRC___, DRC___, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR,  DRC___, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
{{DTC_ERR_AUXILIARY_HEATING_FUNCTION_OFF}   ,{DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,  DRC___, DRC___, DRC___, DRC___, DRC___, DRC___,DRC___,DRC___}},\
{{DTC_ERR_INTERLOCK_MISUSE_TIMEOUT}         ,{DRC___, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR,  DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
\
\
\
\
\
{{DTC_USER_STOP_HG1}                     ,{DRC___, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR,  DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
{{DTC_USER_STOP_HG2}                     ,{DRC___, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR,  DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
{{DTC_USER_STOP_HG3}                     ,{DRC___, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR,  DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
{{DTC_USER_STOP_HG4}                     ,{DRC___, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR,  DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
{{DTC_USER_STOP_HG5}                     ,{DRC___, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR,  DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
{{DTC_USER_STOP_HG6}                     ,{DRC___, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR,  DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
{{DTC_USER_STOP_HG7}                     ,{DRC___, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR,  DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
{{DTC_USER_STOP_HG8}                     ,{DRC___, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRCERR,  DRCERR, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
\
\
\
\
{{DTC_ERR_GHI_HGX_HGV}                   ,{DRC___, DRCINH, DRCERR, DRCERR, DRC___, DRCERR, DRC___, DRCERR,  DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
{{DTC_ERR_GHI_HGX_UEHFL}                 ,{DRC___, DRCINH, DRCERR, DRCERR, DRC___, DRCERR, DRC___, DRCERR,  DRCINH, DRCERR, DRCERR, DRCERR, DRCERR, DRC___,DRC___,DRC___}},\
\
\
\
\
\
\
}






#endif 


