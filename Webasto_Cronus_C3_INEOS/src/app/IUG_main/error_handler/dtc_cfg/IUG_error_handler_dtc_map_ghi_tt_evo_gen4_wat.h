#ifndef _IUG_ERR_HDL_DTC_MAP_GHI_TT_EVO_GEN4_WAT_H_
#define _IUG_ERR_HDL_DTC_MAP_GHI_TT_EVO_GEN4_WAT_H_






#define DTC_ERR_GHITTEVOGEN4WATX_BE_NO_CMD_CONTINUE           (DTC_TABLE_IDX_t)(DTC_ERR_GHI_BE_NO_CMD_CONTINUE_TO_HGX)

#define DTC_ERR_GHITTEVOGEN4WAT1_BE_NO_CMD_CONTINUE           (DTC_TABLE_IDX_t)(DTC_ERR_BE_NO_COMMAND_CONTINUE_TO_HG1)
#define DTC_ERR_GHITTEVOGEN4WAT2_BE_NO_CMD_CONTINUE           (DTC_TABLE_IDX_t)(DTC_ERR_BE_NO_COMMAND_CONTINUE_TO_HG2)
#define DTC_ERR_GHITTEVOGEN4WAT3_BE_NO_CMD_CONTINUE           (DTC_TABLE_IDX_t)(DTC_ERR_GHITTEVOGEN4WATX_BE_NO_CMD_CONTINUE)
#define DTC_ERR_GHITTEVOGEN4WAT4_BE_NO_CMD_CONTINUE           (DTC_TABLE_IDX_t)(DTC_ERR_GHITTEVOGEN4WATX_BE_NO_CMD_CONTINUE)
#define DTC_ERR_GHITTEVOGEN4WAT5_BE_NO_CMD_CONTINUE           (DTC_TABLE_IDX_t)(DTC_ERR_GHITTEVOGEN4WATX_BE_NO_CMD_CONTINUE)
#define DTC_ERR_GHITTEVOGEN4WAT6_BE_NO_CMD_CONTINUE           (DTC_TABLE_IDX_t)(DTC_ERR_GHITTEVOGEN4WATX_BE_NO_CMD_CONTINUE)
#define DTC_ERR_GHITTEVOGEN4WAT7_BE_NO_CMD_CONTINUE           (DTC_TABLE_IDX_t)(DTC_ERR_GHITTEVOGEN4WATX_BE_NO_CMD_CONTINUE)
#define DTC_ERR_GHITTEVOGEN4WAT8_BE_NO_CMD_CONTINUE           (DTC_TABLE_IDX_t)(DTC_ERR_GHITTEVOGEN4WATX_BE_NO_CMD_CONTINUE)


#define DTC_ERR_GHITTEVOGEN4WATX_NO_HEARTBEAT_RESP            (DTC_TABLE_IDX_t)(DTC_ERR_GHI_NO_HEARTBEAT_RESPOND_HGX)

#define DTC_ERR_GHITTEVOGEN4WAT1_NO_HEARTBEAT_RESP            (DTC_TABLE_IDX_t)(DTC_ERR_HG1_NO_HEARTBEAT_RESPOND)
#define DTC_ERR_GHITTEVOGEN4WAT2_NO_HEARTBEAT_RESP            (DTC_TABLE_IDX_t)(DTC_ERR_HG2_NO_HEARTBEAT_RESPOND)
#define DTC_ERR_GHITTEVOGEN4WAT3_NO_HEARTBEAT_RESP            (DTC_TABLE_IDX_t)(DTC_ERR_GHITTEVOGEN4WATX_NO_HEARTBEAT_RESP)
#define DTC_ERR_GHITTEVOGEN4WAT4_NO_HEARTBEAT_RESP            (DTC_TABLE_IDX_t)(DTC_ERR_GHITTEVOGEN4WATX_NO_HEARTBEAT_RESP)
#define DTC_ERR_GHITTEVOGEN4WAT5_NO_HEARTBEAT_RESP            (DTC_TABLE_IDX_t)(DTC_ERR_GHITTEVOGEN4WATX_NO_HEARTBEAT_RESP)
#define DTC_ERR_GHITTEVOGEN4WAT6_NO_HEARTBEAT_RESP            (DTC_TABLE_IDX_t)(DTC_ERR_GHITTEVOGEN4WATX_NO_HEARTBEAT_RESP)
#define DTC_ERR_GHITTEVOGEN4WAT7_NO_HEARTBEAT_RESP            (DTC_TABLE_IDX_t)(DTC_ERR_GHITTEVOGEN4WATX_NO_HEARTBEAT_RESP)
#define DTC_ERR_GHITTEVOGEN4WAT8_NO_HEARTBEAT_RESP            (DTC_TABLE_IDX_t)(DTC_ERR_GHITTEVOGEN4WATX_NO_HEARTBEAT_RESP)


#define DTC_ERR_GHITTEVOGEN4WATX_ACTIVITY_CANCELLED           (DTC_TABLE_IDX_t)(DTC_ERR_GHI_ACTION_CANCELLED_HGX)

#define DTC_ERR_GHITTEVOGEN4WAT1_ACTIVITY_CANCELLED           (DTC_TABLE_IDX_t)(DTC_ERR_HG1_ACTION_CANCELLED)
#define DTC_ERR_GHITTEVOGEN4WAT2_ACTIVITY_CANCELLED           (DTC_TABLE_IDX_t)(DTC_ERR_HG2_ACTION_CANCELLED)
#define DTC_ERR_GHITTEVOGEN4WAT3_ACTIVITY_CANCELLED           (DTC_TABLE_IDX_t)(DTC_ERR_GHITTEVOGEN4WATX_ACTIVITY_CANCELLED)
#define DTC_ERR_GHITTEVOGEN4WAT4_ACTIVITY_CANCELLED           (DTC_TABLE_IDX_t)(DTC_ERR_GHITTEVOGEN4WATX_ACTIVITY_CANCELLED)
#define DTC_ERR_GHITTEVOGEN4WAT5_ACTIVITY_CANCELLED           (DTC_TABLE_IDX_t)(DTC_ERR_GHITTEVOGEN4WATX_ACTIVITY_CANCELLED)
#define DTC_ERR_GHITTEVOGEN4WAT6_ACTIVITY_CANCELLED           (DTC_TABLE_IDX_t)(DTC_ERR_GHITTEVOGEN4WATX_ACTIVITY_CANCELLED)
#define DTC_ERR_GHITTEVOGEN4WAT7_ACTIVITY_CANCELLED           (DTC_TABLE_IDX_t)(DTC_ERR_GHITTEVOGEN4WATX_ACTIVITY_CANCELLED)
#define DTC_ERR_GHITTEVOGEN4WAT8_ACTIVITY_CANCELLED           (DTC_TABLE_IDX_t)(DTC_ERR_GHITTEVOGEN4WATX_ACTIVITY_CANCELLED)


#endif 


