#ifndef _IUG_ERROR_HANDLER__CONSTANT_H_
#define _IUG_ERROR_HANDLER__CONSTANT_H_




#define IUG_ERRHDL_DTC_ACT_ARY_SIZE             64u 


#define IUG_ERRHDL_ERR_DUE_DTC_ARY_SIZE         4u  
_Static_assert(4u==(IUG_ERRHDL_ERR_DUE_DTC_ARY_SIZE),"Invalid size for 'IUG_ERRHDL_ERR_DUE_DTC_ARY_SIZE'!!!");


#define DRC___      0x0u    
#define DRCERR      0x1u    
#define DRCINH      0x2u    
#define DRCLPH      0x3u    

#define DRC_WIDTH   2u          
#define DRC_MASK    (DRCLPH)

typedef enum
{
    DRC_non  = (DRC___),
    DRC_ERR  = (DRCERR),
    DRC_INH  = (DRCINH),
    DRC_LPH  = (DRCLPH),
    DRC_ENUM_MAX  = ((DRCLPH) + 1u),

} IUG_err_hdl_DRC_t;


#define DRC_IDX_InitialOperation         0u
#define DRC_IDX_WatWaitState             1u
#define DRC_IDX_WatHeating               2u
#define DRC_IDX_WatDrvHeating            3u
#define DRC_IDX_WatVentilating           4u
#define DRC_IDX_WatAuxHeating            5u
#define DRC_IDX_WatAuxHtgPrep            6u
#define DRC_IDX_WatAutAuxHtg             7u
#define DRC_IDX_AirWaitState             8u
#define DRC_IDX_AirHeating               9u
#define DRC_IDX_AirBoostHtg             10u
#define DRC_IDX_AirEcoHtg               11u
#define DRC_IDX_AirVentilating          12u
#define DRC_IDX_res13                   13u
#define DRC_IDX_res14                   14u
#define DRC_IDX_res15                   15u
#define DRC_IDX_max                     16u

_Static_assert((DRC_WIDTH)  ==( 2u),"Analyze: 'DRC_WIDTH'   (shall be  2 ever!!!)!");
_Static_assert((DRC_IDX_max)==(16u),"Analyze: 'DRC_IDX_max' (shall be 16 ever!!!)!");


typedef enum
{
    DTCTOP_NONE = 0u,                   
    DTCTOP_GENERAL,                     
    DTCTOP_HGX_COM,                     
    DTCTOP_HGX_STFL,                    
    DTCTOP_HGX_HGV,                     
    DTCTOP_HGX_UEHFL,                   
    DTCTOP_HGX_PND_REQ_MISSING,         
    DTCTOP_HGX_PND_ACK_MISSING,         
    DTCTOP_HGX_PND_ACK_ABORT,           
    DTCTOP_HGX_TEMP_SEN_EXT_DEF,        
    DTCTOP_HGX_TEMP_SEN_EXT_OOR,        
    DTCTOP_HGX_TEMP_SEN_INT_DEF,        
    DTCTOP_HGX_TEMP_SEN_INT_OOR,        
    DTCTOP_UDVX_PND_REQ_MISSING,        
    DTCTOP_UDVX_PND_ACK_MISSING,        
    DTCTOP_UDVX_PND_ACK_ABORT,          
    DTCTOP_ENUM_MAX,                    
    DTCTOP_ENUM_FORCE_TYPE = 0x7F       

} IUG_err_hdl_dtc_topic_t;


typedef enum
{
    DTCLSTTYP_LISTED = 0u,              
    DTCLSTTYP_IDX_HTR,                  
    DTCLSTTYP_IDX_DTC,                  
    DTCLSTTYP_ENUM_MAX,                 
    DTCLSTTYP_ENUM_FORCE_TYPE = 0x7F    

} IUG_err_hdl_dtc_list_type_t;






typedef struct
{
    union
    {
        struct
        {
             uint32_t res15           : 2u, 
                      res14           : 2u, 
                      res13           : 2u, 
                      AirVentilating  : 2u, 

                      AirEcoHtg       : 2u, 
                      AirBoostHtg     : 2u, 
                      AirHeating      : 2u, 
                      AirWaitState    : 2u, 

                      WatAutAuxHtg    : 2u, 
                      WatAuxHtgPrep   : 2u, 
                      WatAuxHeating   : 2u, 
                      WatVentilating  : 2u, 

                      WatDrvHeating   : 2u, 
                      WatHeating      : 2u, 
                      WatWaitState    : 2u, 
                      InitialOperat   : 2u; 

        }        drc_carrier;
        uint32_t drc_uint32;
    } uni;
} struct_IUG_dtc_reaction_code_entry_t;
typedef struct_IUG_dtc_reaction_code_entry_t        IUG_dtc_reaction_code_entry_t;


typedef struct
{
    enum_DTC_TABLE_IDX                      dtc_id;     
    IUG_dtc_reaction_code_entry_t           dtc_drc;    

} IUG_dtc_reaction_configuration_t;


typedef struct
{
    IUG_TARGET_STATE_t      tgt_state;      

    uint8_t                 idx_drc;        

} IUG_htr_state_to_drc_assignment_t;





#endif 


