#ifndef _IUG_ERROR_HANDLER_DTC_LIST_H_
#define _IUG_ERROR_HANDLER_DTC_LIST_H_









typedef struct
{
    enum_DTC_TABLE_IDX              const *p_dtc_lst;   
    uint8_t                         lst_dtc_entries;    
    IUG_err_hdl_dtc_list_type_t     lst_typ;            
    IUG_err_hdl_dtc_topic_t         lst_top;            

} IUG_err_hdl_dtc_list_bag_entry_t;






#endif 


