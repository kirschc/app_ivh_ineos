#ifndef _IUG_IERROR_HANDLER__INCLUDE_H_
#define _IUG_IERROR_HANDLER__INCLUDE_H_


#include "IUG_include.h"


#include "IUG_error_handler_symptom__constant.h"


#include "IUG_error_handler_symptom.h"
#include "IUG_error_handler_dtc_list.h"
#include "IUG_error_handler.h"

#include "IUG_error_handler_drc_cfg_ghi_def_air.h"
#include "IUG_error_handler_drc_cfg_ghi_def_wat.h"
#include "IUG_error_handler_drc_cfg_ghi_hvh_air.h"
#include "IUG_error_handler_drc_cfg_ghi_hvh_wat.h"
#include "IUG_error_handler_drc_cfg_ghi_tt_evo_gen4_wat.h"
#include "IUG_error_handler_drc_cfg_iug_def.h"
#include "IUG_error_handler_drc_cfg_temp_sensor_ext.h"
#include "IUG_error_handler_drc_cfg_temp_sensor_int.h"
#include "IUG_error_handler_drc_cfg_user_device.h"
#include "IUG_error_handler_drc_cfg_wbus_def.h"

#include "IUG_error_handler_dtc_map_ghi_def_air.h"
#include "IUG_error_handler_dtc_map_ghi_def_wat.h"
#include "IUG_error_handler_dtc_map_ghi_hvh_air.h"
#include "IUG_error_handler_dtc_map_ghi_hvh_wat.h"
#include "IUG_error_handler_dtc_map_ghi_tt_evo_gen4_wat.h"
#include "IUG_error_handler_dtc_map_user_device.h"
#include "IUG_error_handler_dtc_map_wbus_def.h"

#include "IUG_error_handler_cfg.h"
#include "IUG_error_handler_dtc_cfg.h"
#include "IUG_error_handler_dtc_cfg_ghi_def_air.h"
#include "IUG_error_handler_dtc_cfg_ghi_def_wat.h"
#include "IUG_error_handler_dtc_cfg_ghi_hvh_air.h"
#include "IUG_error_handler_dtc_cfg_ghi_hvh_wat.h"
#include "IUG_error_handler_dtc_cfg_ghi_tt_evo_gen4_wat.h"
#include "IUG_error_handler_dtc_cfg_iug_def.h"
#include "IUG_error_handler_dtc_cfg_temp_sensor_ext.h"
#include "IUG_error_handler_dtc_cfg_temp_sensor_int.h"
#include "IUG_error_handler_dtc_cfg_temp_sensor_t_lin_r.h"
#include "IUG_error_handler_dtc_cfg_wbus_control.h"
#include "IUG_error_handler_dtc_cfg_wbus_def_air.h"
#include "IUG_error_handler_dtc_cfg_wbus_def_wat.h"
#include "IUG_error_handler_ghi_cfg.h"
#include "IUG_error_handler_dtc_cfg_unit_control.h"
#include "IUG_error_handler_dtc_cfg_user_device.h"


#include "IUG_error_handler_plaus_check.h"


#endif 


