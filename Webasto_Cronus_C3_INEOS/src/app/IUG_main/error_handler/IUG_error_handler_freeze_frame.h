#ifndef _IUG_ERROR_HANDLER_FREEZE_FRAME_H_
#define _IUG_ERROR_HANDLER_FREEZE_FRAME_H_



#define IUG_DTC_FREEZE_FRAME_MAX       8u  

#define IUG_FRZFRM_GETETYCMD_GET_FOR_DTC_IDX    0x00u   
#define IUG_FRZFRM_GETETYCMD_GET_OLDEST         0xF0u   
#define IUG_FRZFRM_GETETYCMD_GET_FREE           0xFFu   








typedef struct
{
    uint8_t     frz_frm_id;     
    uint8_t     frz_frm_size;   

} struct_DTC_FREEZE_FRAME_DESCRIPTION_t;


#pragma  pack(SFL_DTC_PRAGMA_PACK_NVM_DATA) 
typedef struct
{
    enum_DTC_TABLE_IDX  dtc_idx;
    struct_rtc_time_t   VAL_TIME_STAMP;     
    uint8_t             FZG_STATE;          
    uint8_t             IUG_STATE_HG1;      
    uint8_t             IUG_STATE_HG2;      
    uint8_t             HG_STATE_HG1;       
    uint8_t             HG_STATE_HG2;       
    uint16_t            AIR_PRESSURE_VALUE; 
    uint16_t            VAL_IO2;            
    uint16_t            VAL_IO3;            
    uint16_t            DTC_SYMPTOM;        
    uint16_t            VAL_KL_15;          
    uint16_t            VAL_KL_30;          
    uint16_t            VAL_BE_VOLTAGE;     
    uint16_t            VAL_BE_CURRENT;     

} struct_DTC_FREEZE_FRAME_ENTRY_t;
_Static_assert((29u == sizeof(struct_DTC_FREEZE_FRAME_ENTRY_t)),"Analyze: 'struct_DTC_FREEZE_FRAME_ENTRY_t'");
#pragma pack() 



extern const struct_DTC_FREEZE_FRAME_DESCRIPTION_t  mgl_DTC_FREEZE_FRAME_DESCR[];
extern const uint8_t                                ext_iug_entries_DTC_FREEZE_FRAME_DESCR;



extern struct_DTC_FREEZE_FRAME_ENTRY_t     mgl_DTC_FREEZE_FRAME[(IUG_DTC_FREEZE_FRAME_MAX)];


enum_DTC_RETURN_VALUE   IUG_sfl_err_hdl_dtc_freeze_frame_clear_table( void );
void                    IUG_sfl_err_hdl_dtc_freeze_frame_cyclic( void );
uint8_t                 IUG_sfl_err_hdl_dtc_freeze_frame_entry_clr( const enum_DTC_TABLE_IDX dtc_idx );
enum_DTC_RETURN_VALUE   IUG_sfl_err_hdl_dtc_freeze_frame_entry_get( const uint8_t get_cmd, 
                                                            const enum_DTC_TABLE_IDX dtc_idx,
                                                            struct_DTC_FREEZE_FRAME_ENTRY_t * * const ptr_entry );


#endif 


