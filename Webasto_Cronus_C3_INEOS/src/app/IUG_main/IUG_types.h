#ifndef __IUG_TYPES_H_
#define __IUG_TYPES_H_


#include "IUG_unit_constant_t_lin_r.h"


typedef enum_IUG_COMMAND_t  IUG_COMMAND_t;


typedef struct
{
    IUG_QUEUE_KEY_t     comm_rx_act[(WBUS_INS_ENUM_MAX)];

    IUG_QUEUE_KEY_t     comm_tx_act[(WBUS_INS_ENUM_MAX)];

    uint16_t            comm_tx_locked_cnt[(WBUS_INS_ENUM_MAX)];    
    IUG_QUEUE_KEY_t     comm_tx_locked_by_key[(WBUS_INS_ENUM_MAX)]; 

    uint16_t            member_sleep_assumption_cnt[(WBUS_INS_ENUM_MAX)];   

    uint8_t             comm_rx_rec_err[(WBUS_INS_ENUM_MAX)][(WBUS_INS_MEMBER_MAX)]; 
    uint8_t             comm_tx_trm_err[(WBUS_INS_ENUM_MAX)][(WBUS_INS_MEMBER_MAX)]; 

    uint8_t             comm_rx_hdl_install[(WBUS_INS_ENUM_MAX)];

    #ifdef IUG_COMSTA_TRM_WAIT_ACK_FOR_CMD_ACCELERATION
    IUG_HTR_MSK_ID_t        comm_ack_inj_htr_prepared;
    IUG_HTR_MSK_ID_t        comm_ack_inj_htr_injected;
    uint16_t                comm_ack_inj_htr_req_cnt[(IUG_HEATER_UNIT_ENUM_MAX)];               
    uint8_t                 comm_ack_inj_htr_req_mode[(IUG_HEATER_UNIT_ENUM_MAX)];              
    IUG_PHYS_TIME_1M_T      comm_ack_inj_htr_req_ACTIVATION_PERIOD[(IUG_HEATER_UNIT_ENUM_MAX)]; 
    IUG_PHYS_TEMP_1_1DEG_T  comm_ack_inj_htr_req_SETPOINT[(IUG_HEATER_UNIT_ENUM_MAX)];          

        #ifdef IUG_COMSTA_TRM_WAIT_ACK_FOR_CMD_ACCELERATION_DEBUG_EN
        uint8_t             dbg_comm_ack_inj_req_cmd_inhibit_cnt;                               
        IUG_PHYS_TIME_1M_T  dbg_comm_ack_inj_htr_req_ACTIVATION_PERIOD;                         
        #endif
    #endif

} IUG_WBUS_COMM_CTL_t;


typedef struct
{
    WBUS_MEMBER_t   wbus_mbr;       
    IUG_UNIT_ID_t   iug_unit_id;    

} IUG_WBUS_MEMBER_DESCRIPTION_t;


typedef struct
{
    IUG_COMMAND_t   iug_cmd;        
    uint8_t         wbus_mode;      

} IUG_CMD_TO_WBUS_MSG_TRANSLATION_t;


typedef struct
{
    IUG_COMMAND_t   iug_cmd;        
    IUG_COMMAND_t   iug_cmd_air;    

} IUG_CMD_TO_IUG_CMD_AIR_TRANSLATION_t;


typedef struct
{
    uint16_t                            cmd_RX_reserved_cnt[(WBUS_INS_ENUM_MAX)];   
    uint16_t                            cmd_TX_reserved_cnt[(WBUS_INS_ENUM_MAX)];   

    uint8_t                             last_cmd_wbus_mbr_SE;
    uint8_t                             last_cmd_mode_msg;

    uint8_t                             wbus_none_regular_comm; 

    IUG_COMM_TYPE_ID_t                  com_type_pending[(WBUS_INS_ENUM_MAX)];

    uint16_t                            wbus_WTT_connected_cnt;
    WBUS_MSG_DESCRIPTION_CTL_t          wbus_WTT_connected_msg_ctl; 
    uint8_t                             wbus_WTT_connected_msg_dat[(IUG_TEST_WTT_CONNECTED_DATA_SIZE)]; 

} IUG_WBUS_COORDINATION_DATA_t;


typedef struct
{
    WBUS_INSTANCE_t                     wbus_ins_available[(IUG_WBUS_INSTANCES_MAX)];

    enum_WBUS_DRIVER_BAUDRATE_t         wbus_baudrate_id[(IUG_WBUS_INSTANCES_MAX)];     
    enum_WBUS_DRIVER_BAUDRATE_t         wbus_baudrate_req_id[(IUG_WBUS_INSTANCES_MAX)];  

    enum_WBUS_DRIVER_BAUDRATE_t         wbus_baudrate_max_id[(IUG_WBUS_INSTANCES_MAX)];  

} IUG_WBUS_STATUS_t;


typedef struct
{
    uint8_t     btm_entered;                
    uint8_t     btm_exit;                   
    uint16_t    btm_timeout_ms;             

    enum_WBUS_DRIVER_BAUDRATE_t     btm_req_baud_id;
    enum_WBUS_MEMBER_t              btm_msg_mbrini_I;       
    enum_WBUS_MEMBER_t              btm_msg_mbrcli_C;       
    WBUS_INSTANCE_t                 btm_ins_msg_mbrini_I;   
    WBUS_INSTANCE_t                 btm_ins_msg_mbrcli_C;   
} struct_IUG_BTM_BLOCK_TRANSFER_MODE_DATA_t;
typedef struct_IUG_BTM_BLOCK_TRANSFER_MODE_DATA_t   IUG_BTM_BLOCK_TRANSFER_MODE_DATA_t;


typedef struct
{
    uint8_t             ApplicationVersionMajor;    
    uint8_t             ApplicationVersionMinor;    
    uint8_t             ApplicationVersionBuild;    

    uint8_t             ECU_power_ON_reset;
    volatile uint8_t    ECU_init_done;

    uint16_t            ECU_time_since_reset_ms;

    struct_IUG_SYSTEM_TIME_t            IUG_SYSTEM_TIME;

    enum_HAL_CAN_BAUDRATE               can1_appl_baudrate; 
    enum_HAL_CAN_BAUDRATE               can2_appl_baudrate; 
    enum_HAL_CAN_BAUDRATE               can3_appl_baudrate; 
    enum_bl_baudrate_t                  can_bl_baudrate;    

    uint8_t                             ary_webasto_appl_number_can[10u]; 
    enum_IUG_SHARED_PARA_CTRL           key_webasto_appl_number_can;      
    uint8_t                             ary_flash_file_name_can[70u];     
    enum_IUG_SHARED_PARA_CTRL           key_flash_file_name_can;          

    uint8_t                             ary_webasto_cronus_id[8u];        
    enum_IUG_VARIANTS                   variant;                          

    IUG_GHIBUS_STATUS_t                     GHIBUS_STATUS;

    IUG_WBUS_STATUS_t                       WBUS_STATUS;
    IUG_WBUS_COORDINATION_DATA_t            WBUS_COORD;

    IUG_SAM_SECURITY_ACCESS_MODE_DATA_t     SAM;

    IUG_BTM_BLOCK_TRANSFER_MODE_DATA_t      BTM;

    IUG_MAB_MEMORY_ACCESS_BLOCKMODE_DATA_t  MAB;

    uint8_t     ble_flashing_flg; 

    uint8_t     routine_fbl_released;           
    uint8_t     routine_reset_released;         
    uint16_t    routine_fbl_timeout_ms;         

    struct_port_modes_ram port_modes;

} struct_IUG_SYSTEM_STATE_t;


#endif 


