#ifndef IUG_INIT_OP_GLOBAL_CFG_CONSTANT_H
#define IUG_INIT_OP_GLOBAL_CFG_CONSTANT_H



typedef enum
{
    INIT_OP_ROUT_SCAN_WBUS_HTR      = 0u, 
    INIT_OP_ROUT_SCAN_GHI_HTR           , 
    INIT_OP_ROUT_BE                     , 
    INIT_OP_ROUT_BE_ASSIGN              , 
    INIT_OP_ROUT_SCAN_T_LIN_R           , 
    INIT_OP_ROUT_DOSING_PUMP_HG1        , 
    INIT_OP_ROUT_TEMP_SENS              , 
    INIT_OP_ROUT_CAR_STATE              , 
    INIT_OP_ROUT_OBD                    , 
    INIT_OP_ROUT_CAR_CLIMATE_DETECT     , 
    INIT_OP_ROUT_CAR_CLIMATE_CHECK      , 
    INIT_OP_ROUT_DISPL_STATE            , 
    INIT_OP_ROUT_TEST_RUN_HG1           , 
    INIT_OP_ROUT_INACT_CAR_STATE        , 
#ifdef INIT_OP_CFG_CAN_LINK_EN
    INIT_OP_ROUT_SYS_TIME               , 
#endif
    INIT_OP_ROUT_TEST_RUN_HG2           , 
    INIT_OP_ROUT_DOSING_PUMP_HG2        , 
    INIT_OP_ROUT_TEACH_IN_TELESTART     , 
    INIT_OP_ROUT_COOLANT_PUMP_HG1       , 
    INIT_OP_ROUT_COOLANT_PUMP_HG2       , 
    INIT_OP_ROUT_ENUM_MAX               , 
    INIT_OP_ROUT_WAIT                   , 

} enum_INIT_OP_ROUTINES;


#endif 


