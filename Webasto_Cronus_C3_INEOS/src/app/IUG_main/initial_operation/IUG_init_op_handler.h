#ifndef IUG_INIT_OP_HANDLER_H
#define IUG_INIT_OP_HANDLER_H


#ifdef INIT_OP_CFG_CAN_LINK_EN
    #define INIT_OP_CAN_REQ_MODE(data)   ( (data      )         ) 
    #define INIT_OP_CAN_REQ_DATA(data)   ( (data >> 0u) & 0x01u ) 
    #define INIT_OP_CAN_REQ_ACT(data)    ( (data >> 1u) & 0x01u ) 

    #define INIT_OP_CAN_RES_MODE(struct_member)          (struct_member      ) 
    #define INIT_OP_CAN_RES_DATA(struct_member)          (struct_member << 0u) 
    #define INIT_OP_CAN_RES_CTRL(struct_member)          (struct_member << 1u) 
    #define INIT_OP_CAN_RES_ACK_TX(struct_member)        (struct_member << 2u) 
    #define INIT_OP_CAN_RES_MODE_RESULT(struct_member)   (struct_member << 3u) 
#endif

typedef enum
{
    INIT_OP_STATUS_MANU_NOT_DONE = 0u, 
    INIT_OP_STATUS_MANU_DONE_IO  = 1u, 
    INIT_OP_STATUS_MANU_DONE_NIO = 2u, 
    INIT_OP_STATUS_AUTO_NOT_DONE = 3u, 
    INIT_OP_STATUS_AUTO_DONE_IO  = 4u, 
    INIT_OP_STATUS_AUTO_DONE_NIO = 5u  
} enum_INIT_OP_STATUS;
_Static_assert(0u==(INIT_OP_STATUS_MANU_NOT_DONE) ,"Change encoding for webasto parameter 'ID_INITIAL_OPERATION' too!!!");
_Static_assert(1u==(INIT_OP_STATUS_MANU_DONE_IO)  ,"Change encoding for webasto parameter 'ID_INITIAL_OPERATION' too!!!");
_Static_assert(2u==(INIT_OP_STATUS_MANU_DONE_NIO) ,"Change encoding for webasto parameter 'ID_INITIAL_OPERATION' too!!!");
_Static_assert(3u==(INIT_OP_STATUS_AUTO_NOT_DONE) ,"Change encoding for webasto parameter 'ID_INITIAL_OPERATION' too!!!");
_Static_assert(4u==(INIT_OP_STATUS_AUTO_DONE_IO)  ,"Change encoding for webasto parameter 'ID_INITIAL_OPERATION' too!!!");
_Static_assert(5u==(INIT_OP_STATUS_AUTO_DONE_NIO) ,"Change encoding for webasto parameter 'ID_INITIAL_OPERATION' too!!!");

typedef enum
{
    INIT_OP_TRG_SRC_EMPTY     = 0x00u, 
    INIT_OP_TRG_SRC_MANU_WBUS = 0x01u, 
    INIT_OP_TRG_SRC_MANU_CAN  = 0x02u, 
    INIT_OP_TRG_SRC_AUTO_ECU  = 0x04u, 
    INIT_OP_TRG_SRC_AUTO_BTN  = 0x08u  
} enum_INIT_OP_TRG_SRC;

typedef enum
{
    INIT_OP_ROUT_STATE_IDLE              = 0u, 
    INIT_OP_ROUT_STATE_INIT                  , 
    INIT_OP_ROUT_STATE_PENDING               , 
    INIT_OP_ROUT_STATE_SUCCESSFUL            , 
    INIT_OP_ROUT_STATE_UNSUCCESSFUL          , 
    INIT_OP_ROUT_STATE_PSEUDO_SUCCESSFUL     , 
    INIT_OP_ROUT_STATE_SKIP_REQ              , 
    INIT_OP_ROUT_STATE_SKIP_CFM              , 
    INIT_OP_ROUT_STATE_CLOSED_REQ            , 
    INIT_OP_ROUT_STATE_CLOSED_CFM            , 
    INIT_OP_ROUT_STATE_ABORTED_REQ           , 
    INIT_OP_ROUT_STATE_ABORTED_CFM           , 
    INIT_OP_ROUT_STATE_TIMEOUT_REQ           , 
    INIT_OP_ROUT_STATE_TIMEOUT_CFM           , 
    INIT_OP_ROUT_STATE_STOP_DTC_REQ          , 
    INIT_OP_ROUT_STATE_STOP_DTC_CFM            
} enum_INIT_OP_ROUT_STATES;

typedef enum
{
    INIT_OP_WAIT_STATE_WAT = 0u,
    INIT_OP_SWITCH_ON_WAT  = 1u,
    INIT_OP_HEATING_WAT    = 2u,
    INIT_OP_SWITCH_OFF_WAT = 3u,
    INIT_OP_FAULT_WAT      = 4u
} enum_INIT_OP_HG_STATES_WAT;

typedef enum
{
    INIT_OP_WAIT_STATE_AIR = 0u,
    INIT_OP_SWITCH_ON_AIR  = 1u,
    INIT_OP_HEATING_AIR    = 2u,
    INIT_OP_SWITCH_OFF_AIR = 3u,
    INIT_OP_FAULT_AIR      = 4u
} enum_INIT_OP_HG_STATES_AIR;

typedef enum_INIT_OP_ROUT_STATES (*funct_ptr_ibn)(enum_INIT_OP_ROUT_STATES routine_ctl, enum_parameter_diagnose_id *const ptr_trans_ids);

typedef struct
{
    enum_INIT_OP_ROUTINES    rout;       
    enum_INIT_OP_ROUT_STATES rout_state; 
    enum_INIT_OP_TRG_SRC     trg_src;    
} struct_init_op_main_data;

#ifdef INIT_OP_CFG_CAN_LINK_EN
typedef struct
{
    uint8_t can_act;
    uint8_t can_mode;
    uint8_t can_mode_result;
    uint8_t can_ctrl;
    uint8_t can_get_data;
    uint8_t can_set_data;
    uint8_t can_ack_tx;
    uint8_t can_rx_data_bytes[ (HAL_CAN_DLC_MAX) ];
    uint8_t can_tx_data_bytes[ (HAL_CAN_DLC_MAX) ];
} struct_init_op_can;
#endif

typedef struct
{
    uint8_t                     process_state; 
    uint32_t                    timeout_timer; 
#ifdef INIT_OP_CFG_CAN_LINK_EN
    volatile struct_init_op_can can_data;      
#endif
} struct_init_op_data_handle;

#pragma pack(1) 
typedef struct
{
    uint8_t  orig_routine; 
    uint8_t  step_result;  
    uint32_t data;         
    uint32_t counter;      
} struct_init_op_nvm_rout_data;
#pragma pack() 

#pragma pack(1) 
typedef struct
{
    boolean                      sys_rout_done_flg;                
    struct_init_op_nvm_rout_data rout_data[INIT_OP_ROUT_ENUM_MAX]; 
} struct_init_op_nvm_data;
#pragma pack() 

typedef struct
{
    enum_INIT_OP_ROUTINES              routine;       
    funct_ptr_ibn                      cb_rout;       
    struct_init_op_nvm_rout_data       *ptr_nvm_data; 
    const struct_init_op_cfg_rout_main *ptr_cfg;      
    boolean                            interrupt_flg; 
    boolean                            auto_mode;     
} struct_init_op_routines;

extern const struct_init_op_routines ext_ary_init_op_routines[INIT_OP_ROUT_ENUM_MAX];
extern struct_init_op_nvm_data ext_init_op_nvm_data;
extern struct_init_op_data_handle ext_init_op_data_handle;


void IUG_init_op_init(void);

void IUG_init_op_handler_cylic(void);

#ifdef INIT_OP_CFG_CAN_LINK_EN
void IUG_init_op_can_irq_receive_data(const struct_hal_can_frame *const ptr_can_msg);
#endif

#ifdef INIT_OP_CFG_CAN_LINK_EN
static void IUG_init_op_can_transmit_data(const enum_INIT_OP_ROUT_STATES step_result);
#endif

#ifdef INIT_OP_CFG_CAN_LINK_EN
static void IUG_init_op_handle_request_can(void);
#endif

static void IUG_init_op_handle_request(void);

enum_INIT_OP_ROUT_STATES IUG_init_op_check_step_skip(const enum_INIT_OP_ROUT_STATES routine_ctl, \
                                                     const enum_INIT_OP_ROUTINES routine, \
                                                     enum_parameter_diagnose_id *const ptr_trans_ids);

static void IUG_init_op_process_routine(void);

static void IUG_init_op_clear_local_dtc_data(void);

void IUG_init_op_wr_res_to_nvm(void);

void IUG_init_op_fcn_billing(void);

void IUG_init_op_set_trg_src(const enum_INIT_OP_TRG_SRC trg_src);

enum_INIT_OP_ROUTINES IUG_init_op_get_act_rout(void);

enum_INIT_OP_ROUT_STATES IUG_init_op_get_act_rout_state(void);


#endif 


