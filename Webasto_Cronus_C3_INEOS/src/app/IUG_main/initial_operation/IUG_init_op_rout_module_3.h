#ifndef IUG_INIT_OP_ROUT_MODULE_3_H
#define IUG_INIT_OP_ROUT_MODULE_3_H


#define INIT_OP_CAR_STATE_ALL_SOURCES_OFF   0x00u 
#define INIT_OP_CAR_STATE_ALL_SOURCES_ON    0x01u 

#define INIT_OP_TEACH_IN_TELE_INVAL_WBUS_DATA   0xFFu 


typedef enum
{
    INIT_OP_ROUT_DETECT_CAR_STATE_OFF = 0u,
    INIT_OP_ROUT_DETECT_CAR_STATE_ON
} enum_INIT_OP_ROUT_DETECT_CAR_STATE_STATES;

typedef enum
{
    INIT_OP_ROUT_CLIMATE_DETECT_PWM_HIGHSIDE = 0u,
    INIT_OP_ROUT_CLIMATE_DETECT_PWM_LOWSIDE
} enum_INIT_OP_CLIMATE_DETECT_PWM_MODES;

typedef enum
{
    INIT_OP_ROUT_PUSH_BTN_STEP_1       = 0u,
    INIT_OP_ROUT_PUSH_BTN_STEP_2           ,
    INIT_OP_ROUT_PUSH_BTN_STEP_3           ,
    INIT_OP_ROUT_PUSH_BTN_WAIT_FOR_DTC
} enum_INIT_OP_ROUT_PUSH_BTN_STATES;

typedef enum
{
    INIT_OP_ROUT_TELESTART_GET_EDUCATED_TRANS_FIRST   = 0u,
    INIT_OP_ROUT_TELESTART_EVAL_EDUCATED_TRANS_FIRST      ,
    INIT_OP_ROUT_TELESTART_TEACH_IN_START                 ,
    INIT_OP_ROUT_TELESTART_EVAL_START                     ,
    INIT_OP_ROUT_TELESTART_WAIT                           ,
    INIT_OP_ROUT_TELESTART_GET_EDUCATED_TRANS_SECOND      ,
    INIT_OP_ROUT_TELESTART_EVAL_EDUCATED_TRANS_SECOND
} enum_INIT_OP_ROUT_TELESTART_STATES;

typedef struct
{
    enum_ENV_TEMP_SENS                       wbus_temp_sens;
    uint16_t                                 wbus_location_idx;
    WBUS_INSTANCE_t                          wbus_bus;
    structure_IUG_UNIT_TEMP_SENSOR_CONTROL_t *ptr_wbus_temp_ctl;
} struct_init_op_cfg_wbus_temp_sens;

typedef struct
{
    uint8_t  teach_in_start_M40_res_db1;          
    uint8_t  teach_in_result_M41_res_db1;         
    uint8_t  teach_in_educated_trans_M50_S08_db1; 
    uint8_t  teach_in_educated_trans_M50_S08_db2; 
    boolean  teach_in_rsp;                        
    uint8_t  teach_in_start_time;                 
    uint8_t  teach_in_window_time_val;            
    uint32_t teach_in_window_time_timer;          
} struct_init_op_telestart_data;

typedef struct
{
    uint8_t            rty_cnt;  
    boolean            trg_lock; 
    enum_ENV_TEMP_SENS cur_sens; 
} struct_init_op_temp_sens_data;

extern struct_init_op_telestart_data ext_init_op_telestart_data;


void IUG_init_op_module_3_init(void);

void IUG_init_op_module_3_cyclic(void);

enum_INIT_OP_ROUT_STATES IUG_init_op_rout_scan_ctrl_units(enum_INIT_OP_ROUT_STATES routine_ctl, enum_parameter_diagnose_id *const ptr_trans_ids);

enum_INIT_OP_ROUT_STATES IUG_init_op_rout_assign_ctrl_units(enum_INIT_OP_ROUT_STATES routine_ctl, enum_parameter_diagnose_id *const ptr_trans_ids);

static enum_INIT_OP_ROUT_STATES IUG_init_op_addon_htr_binding(enum_INIT_OP_ROUT_STATES routine_ctl);

enum_INIT_OP_ROUT_STATES IUG_init_op_rout_t_lin_r(enum_INIT_OP_ROUT_STATES routine_ctl, enum_parameter_diagnose_id *const ptr_trans_ids);

enum_INIT_OP_ROUT_STATES IUG_init_op_rout_scan_temp_sens(enum_INIT_OP_ROUT_STATES routine_ctl, enum_parameter_diagnose_id *const ptr_trans_ids);

static enum_INIT_OP_ROUT_STATES IUG_init_op_addon_t_lin_r_compare_variants_to_set_dtc(enum_INIT_OP_ROUT_STATES routine_ctl);

static void IUG_init_op_addon_trg_wbus_temp_sens(const enum_ENV_TEMP_SENS cur_sens, const boolean del_sens);

void IUG_init_op_addon_set_next_valid_sens(void);

#ifdef IUG_IBN_AUTO_TEMP_SENS_DET_EN
static void IUG_init_op_addon_update_temp_sens(void);
#endif

static enum_INIT_OP_ROUT_STATES IUG_init_op_addon_calc_bmp_offset(enum_INIT_OP_ROUT_STATES routine_ctl);

enum_INIT_OP_ROUT_STATES IUG_init_op_rout_detect_car_state(enum_INIT_OP_ROUT_STATES routine_ctl, enum_parameter_diagnose_id *const ptr_trans_ids);

static uint8_t IUG_init_op_addon_detect_car_state_eval_all_sources(const uint8_t eval_mode);

enum_INIT_OP_ROUT_STATES IUG_init_op_rout_scan_obd(enum_INIT_OP_ROUT_STATES routine_ctl, enum_parameter_diagnose_id *const ptr_trans_ids);

enum_INIT_OP_ROUT_STATES IUG_init_op_rout_climate_detect(enum_INIT_OP_ROUT_STATES routine_ctl, enum_parameter_diagnose_id *const ptr_trans_ids);

uint8_t IUG_init_op_addon_climate_detect_set_api_cb(const fct_ptr_api_init_op_climate_detect cb_climate_detect);

enum_INIT_OP_ROUT_STATES IUG_init_op_rout_climate_check(enum_INIT_OP_ROUT_STATES routine_ctl, enum_parameter_diagnose_id *const ptr_trans_ids);

enum_INIT_OP_ROUT_STATES IUG_init_op_rout_push_button(enum_INIT_OP_ROUT_STATES routine_ctl, enum_parameter_diagnose_id *const ptr_trans_ids);

enum_INIT_OP_ROUT_STATES IUG_init_op_rout_teach_in_telestart(enum_INIT_OP_ROUT_STATES routine_ctl, enum_parameter_diagnose_id *const ptr_trans_ids);

static uint8_t IUG_init_op_addon_teach_in_telestart_limit_teach_in_time(void);

static void IUG_init_op_addon_teach_in_telestart_process_opened_window(void);


#endif 


