#ifndef IUG_INIT_OP_ROUT_MODULE_3_CFG_H
#define IUG_INIT_OP_ROUT_MODULE_3_CFG_H


#define INIT_OP_CFG_DTC_DELAY_PUSH_BTN_MS       ( (L99PM72_STAT_REG_REQ_TIME_IBN_MS) * 3u ) 
#define INIT_OP_CFG_DTC_DELAY_BE_SCAN_MS        ( (L99PM72_STAT_REG_REQ_TIME_IBN_MS) * 3u ) 
#define INIT_OP_CFG_DTC_DELAY_CLIMATE_DET_MS    ( (PWM_OUT_CFG_SH_DET_DELAY_MS)      * 3u ) 
#define INIT_OP_CFG_DTC_DELAY_OBD_MS            500u                                        

#define INIT_OP_CFG_TIMEOUT_WBUS_UNIT2_CTRL_MS  ( ( (WBUS_DRV_RESPONSE_TIMEOUT_GENERAL_MAX_MS) * (WBUS_DRV_RETRY_MAX_ERR_UNIT_2) ) * 2u ) 

#define INIT_OP_CFG_TIMEOUT_PWM_READ_MODE_S     10u 

#define INIT_OP_CFG_RESULT_DELAY_OBD_MS         2000u 

#define INIT_OP_CFG_TEMP_SENS_SCAN_RETRIES      3u 
#define INIT_OP_CFG_TEMP_SENS_SCAN_RETRY_TIME   100u   

typedef struct
{
    enum_ENV_TEMP_SENS temp_sens;
    uint16_t           retry_time_ms; 
} struct_init_op_cfg_temp_sens_retry_time;

extern const enum_parameter_diagnose_id ext_init_op_cfg_ids_scan_ctrl_units[];
extern const enum_DTC_TABLE_IDX ext_init_op_cfg_used_dtcs_scan_ctrl_units[];
extern const enum_DTC_TABLE_IDX ext_init_op_cfg_abort_dtcs_scan_ctrl_units[];
extern const uint8_t ext_init_op_cfg_used_dtcs_scan_ctrl_units_entries;
extern uint8_t ext_init_op_cfg_dtc_state_scan_ctrl_units[];

extern const enum_parameter_diagnose_id ext_init_op_cfg_ids_teach_in_telestart[];
extern const enum_DTC_TABLE_IDX ext_init_op_cfg_used_dtcs_teach_in_telestart[];
extern const enum_DTC_TABLE_IDX ext_init_op_cfg_abort_dtcs_teach_in_telestart[];
extern const uint8_t ext_init_op_cfg_used_dtcs_teach_in_telestart_entries;
extern uint8_t ext_init_op_cfg_dtc_state_teach_in_telestart[];

extern const enum_parameter_diagnose_id ext_init_op_cfg_ids_assign_ctrl_units[];
extern const enum_DTC_TABLE_IDX ext_init_op_cfg_used_dtcs_assign_ctrl_units[];
extern const enum_DTC_TABLE_IDX ext_init_op_cfg_abort_dtcs_assign_ctrl_units[];
extern const uint8_t ext_init_op_cfg_used_dtcs_assign_ctrl_units_entries;
extern uint8_t ext_init_op_cfg_dtc_state_assign_ctrl_units[];

extern const enum_parameter_diagnose_id ext_init_op_cfg_ids_t_lin_r[];
extern const enum_DTC_TABLE_IDX ext_init_op_cfg_used_dtcs_t_lin_r[];
extern const enum_DTC_TABLE_IDX ext_init_op_cfg_abort_dtcs_t_lin_r[];
extern const uint8_t ext_init_op_cfg_used_dtcs_t_lin_r_entries;
extern uint8_t ext_init_op_cfg_dtc_state_t_lin_r[];
extern IUG_err_hdl_dtc_sypmtom_t ext_init_op_cfg_dtc_sym_t_lin_r[];

extern const enum_parameter_diagnose_id ext_init_op_cfg_ids_scan_temp_sens[];
extern const enum_DTC_TABLE_IDX ext_init_op_cfg_used_dtcs_scan_temp_sens[];
extern const enum_DTC_TABLE_IDX ext_init_op_cfg_abort_dtcs_scan_temp_sens[];
extern const uint8_t ext_init_op_cfg_used_dtcs_scan_temp_sens_entries;
extern uint8_t ext_init_op_cfg_dtc_state_scan_temp_sens[];

extern const enum_parameter_diagnose_id ext_init_op_cfg_ids_detect_car_state_on[];
extern const enum_DTC_TABLE_IDX ext_init_op_cfg_used_dtcs_detect_car_state_on[];
extern const enum_DTC_TABLE_IDX ext_init_op_cfg_abort_dtcs_detect_car_state_on[];
extern const uint8_t ext_init_op_cfg_used_dtcs_detect_car_state_on_entries;
extern uint8_t ext_init_op_cfg_dtc_state_detect_car_state_on[];

extern const enum_parameter_diagnose_id ext_init_op_cfg_ids_scan_obd[];
extern const enum_DTC_TABLE_IDX ext_init_op_cfg_used_dtcs_scan_obd[];
extern const enum_DTC_TABLE_IDX ext_init_op_cfg_abort_dtcs_scan_obd[];
extern const uint8_t ext_init_op_cfg_used_dtcs_scan_obd_entries;
extern uint8_t ext_init_op_cfg_dtc_state_scan_obd[];

extern const enum_parameter_diagnose_id ext_init_op_cfg_ids_climate_detect[];
extern const enum_DTC_TABLE_IDX ext_init_op_cfg_used_dtcs_climate_detect[];
extern const enum_DTC_TABLE_IDX ext_init_op_cfg_abort_dtcs_climate_detect[];
extern const uint8_t ext_init_op_cfg_used_dtcs_climate_detect_entries;
extern uint8_t ext_init_op_cfg_dtc_state_climate_detect[];

extern const enum_parameter_diagnose_id ext_init_op_cfg_ids_climate_check[];
extern const enum_DTC_TABLE_IDX ext_init_op_cfg_used_dtcs_climate_check[];
extern const enum_DTC_TABLE_IDX ext_init_op_cfg_abort_dtcs_climate_check[];
extern const enum_DTC_TABLE_IDX ext_init_op_cfg_abort_dtcs_climate_check_pwm[];
extern const uint8_t ext_init_op_cfg_used_dtcs_climate_check_entries;
extern uint8_t ext_init_op_cfg_dtc_state_climate_check[];

extern const enum_parameter_diagnose_id ext_init_op_cfg_ids_push_button[];
extern const enum_DTC_TABLE_IDX ext_init_op_cfg_used_dtcs_push_button[];
extern const enum_DTC_TABLE_IDX ext_init_op_cfg_abort_dtcs_push_button[];
extern const uint8_t ext_init_op_cfg_used_dtcs_push_button_entries;
extern uint8_t ext_init_op_cfg_dtc_state_push_button[];

extern const enum_parameter_diagnose_id ext_init_op_cfg_ids_detect_car_state_off[];
extern const enum_DTC_TABLE_IDX ext_init_op_cfg_used_dtcs_detect_car_state_off[];
extern const enum_DTC_TABLE_IDX ext_init_op_cfg_abort_dtcs_detect_car_state_off[];
extern const uint8_t ext_init_op_cfg_used_dtcs_detect_car_state_off_entries;
extern uint8_t ext_init_op_cfg_dtc_state_detect_car_state_off[];

extern const struct_init_op_cfg_temp_sens_retry_time ext_init_op_cfg_temp_sens_retry_time[ENV_TEMP_SENS_MAX];



#endif 


