#ifndef IUG_INIT_OP_HANDLER_AUTO_H
#define IUG_INIT_OP_HANDLER_AUTO_H



typedef enum
{
    INIT_OP_AUTO_TRG_SRC_CLR = 0x00u, 
    INIT_OP_AUTO_TRG_SRC_POR = 0x01u, 
    INIT_OP_AUTO_TRG_SRC_PAR = 0x02u, 
    INIT_OP_AUTO_TRG_SRC_API = 0x04u, 
#ifdef IUG_TRIGGER_WBUS_SCANNING_DEVELOPMENT_EN
    INIT_OP_AUTO_TRG_SRC_BTN = 0x08u  
#endif
} enum_INIT_OP_AUTO_TRG_SRC;

typedef struct
{
    boolean                   por_lock;      
    boolean                   clr_nvm_data;  
    uint8_t                   rout_idx_cnt;  
    uint8_t                   *ptr_rout_seq; 
    uint32_t                  timer_delay;   
    enum_INIT_OP_AUTO_TRG_SRC trg_src;       
} struct_init_op_auto_data_handle;


void IUG_init_op_auto_init(void);

void IUG_init_op_auto_cylic(void);

static void IUG_init_op_auto_handle_request(void);

static void IUG_init_op_auto_set_trg_src(void);

static void IUG_init_op_auto_set_result(const uint8_t ary_size);

void IUG_init_op_auto_trg_by_api(void);

void IUG_init_op_auto_trg_by_btn(void);

void IUG_init_op_auto_set_next_rout(void);


#endif 


