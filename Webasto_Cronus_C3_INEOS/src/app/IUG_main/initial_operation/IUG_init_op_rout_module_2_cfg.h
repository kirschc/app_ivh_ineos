#ifndef IUG_INIT_OP_ROUT_MODULE_2_CFG_H
#define IUG_INIT_OP_ROUT_MODULE_2_CFG_H


#define INIT_OP_CFG_DTC_DELAY_HTR_CTRL_MS     ( (IUG_UNIT_READ_STATUS_HEATER_HIGH_CYCLE) * 3u ) 

#define INIT_OP_CFG_TIMEOUT_WBUS_HG_CTRL_MS   ( ( (WBUS_DRV_RESPONSE_TIMEOUT_GENERAL_MAX_MS) * (WBUS_DRV_RETRY_MAX_ERR_UNIT_4) ) * 2u )

#define INIT_OP_CFG_TIME_TEST_RUN_DET_RP_S    30u 

extern const enum_parameter_diagnose_id ext_init_op_cfg_ids_scan_wbus_htr[];
extern const enum_DTC_TABLE_IDX ext_init_op_cfg_used_dtcs_scan_wbus_htr[];
extern const enum_DTC_TABLE_IDX ext_init_op_cfg_abort_dtcs_scan_wbus_htr[];
extern const uint8_t ext_init_op_cfg_used_dtcs_scan_wbus_htr_entries;
extern uint8_t ext_init_op_cfg_dtc_state_scan_wbus_htr[];

extern const enum_parameter_diagnose_id ext_init_op_cfg_ids_scan_ghi_htr[];
extern const enum_DTC_TABLE_IDX ext_init_op_cfg_used_dtcs_scan_ghi_htr[];
extern const enum_DTC_TABLE_IDX ext_init_op_cfg_abort_dtcs_scan_ghi_htr[];
extern const uint8_t ext_init_op_cfg_used_dtcs_scan_ghi_htr_entries;
extern uint8_t ext_init_op_cfg_dtc_state_scan_ghi_htr[];

extern const enum_parameter_diagnose_id ext_init_op_cfg_ids_dosing_pump_hg1[];
extern const enum_DTC_TABLE_IDX ext_init_op_cfg_used_dtcs_dosing_pump_hg1[];
extern const enum_DTC_TABLE_IDX ext_init_op_cfg_abort_dtcs_dosing_pump_hg1[];
extern const uint8_t ext_init_op_cfg_used_dtcs_dosing_pump_hg1_entries;
extern uint8_t ext_init_op_cfg_dtc_state_dosing_pump_hg1[];

extern const enum_parameter_diagnose_id ext_init_op_cfg_ids_dosing_pump_hg2[];
extern const enum_DTC_TABLE_IDX ext_init_op_cfg_used_dtcs_dosing_pump_hg2[];
extern const enum_DTC_TABLE_IDX ext_init_op_cfg_abort_dtcs_dosing_pump_hg2[];
extern const uint8_t ext_init_op_cfg_used_dtcs_dosing_pump_hg2_entries;
extern uint8_t ext_init_op_cfg_dtc_state_dosing_pump_hg2[];

extern const enum_parameter_diagnose_id ext_init_op_cfg_ids_heater_test_run[];
extern const enum_DTC_TABLE_IDX ext_init_op_cfg_used_dtcs_heater_test_run[];
extern const enum_DTC_TABLE_IDX ext_init_op_cfg_abort_dtcs_heater_test_run_hg1[];
extern const enum_DTC_TABLE_IDX ext_init_op_cfg_abort_dtcs_heater_test_run_hg2[];
extern const uint8_t ext_init_op_cfg_used_dtcs_heater_test_run_entries;
extern uint8_t ext_init_op_cfg_dtc_state_heater_test_run[];

extern const enum_parameter_diagnose_id ext_init_op_cfg_ids_coolant_pump_hg1[];
extern const enum_DTC_TABLE_IDX ext_init_op_cfg_used_dtcs_coolant_pump_hg1[];
extern const enum_DTC_TABLE_IDX ext_init_op_cfg_abort_dtcs_coolant_pump_hg1[];
extern const uint8_t ext_init_op_cfg_used_dtcs_coolant_pump_hg1_entries;
extern uint8_t ext_init_op_cfg_dtc_state_coolant_pump_hg1[];

extern const enum_parameter_diagnose_id ext_init_op_cfg_ids_coolant_pump_hg2[];
extern const enum_DTC_TABLE_IDX ext_init_op_cfg_used_dtcs_coolant_pump_hg2[];
extern const enum_DTC_TABLE_IDX ext_init_op_cfg_abort_dtcs_coolant_pump_hg2[];
extern const uint8_t ext_init_op_cfg_used_dtcs_coolant_pump_hg2_entries;
extern uint8_t ext_init_op_cfg_dtc_state_coolant_pump_hg2[];



#endif 


