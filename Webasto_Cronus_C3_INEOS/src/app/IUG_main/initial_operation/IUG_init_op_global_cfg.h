#ifndef IUG_INIT_OP_GLOBAL_CONFIG_H
#define IUG_INIT_OP_GLOBAL_CONFIG_H


#ifdef INIT_OP_CFG_CAN_LINK_EN
    #define INIT_OP_CFG_CAN_ID_REQ   0x999u 
    #define INIT_OP_CFG_CAN_ID_RES   0x998u 
#endif


typedef struct
{
    enum_parameter_id para_id_act_time;
    enum_parameter_id para_id_default_heat_time;
} struct_init_op_cfg_para;

typedef struct
{
    enum_INIT_OP_ROUTINES     ibn_routine;
    const enum_DTC_TABLE_IDX  *ptr_def_abort_dtc;
    const enum_DTC_TABLE_IDX  *ptr_cust_abort_dtc_1;
} struct_init_op_cfg_abort_dtc_tables;

typedef struct
{
    const enum_DTC_TABLE_IDX  *ptr_used_dtcs_cfg;     
    const enum_DTC_TABLE_IDX  **ptr_abort_dtcs_cfg;   
    uint8_t                   *ptr_local_dtc_status;  
    IUG_err_hdl_dtc_sypmtom_t *ptr_local_dtc_sym;     
    const uint8_t             *ptr_used_dtcs_entries; 
} struct_init_op_cfg_dtc_data;

typedef struct
{
    enum_INIT_OP_ROUTINES             ibn_routine;
    const enum_parameter_diagnose_id  *ptr_initial_id_cfg;
    const struct_init_op_cfg_para     *ptr_wat_heat_para_id_cfg;
    const struct_init_op_cfg_para     *ptr_air_heat_para_id_cfg;
    const struct_init_op_cfg_dtc_data *ptr_dtc_cfg;
} struct_init_op_cfg_rout_main;

extern const enum_DTC_TABLE_IDX ext_init_op_cfg_global_abort_dtcs[];
extern const struct_init_op_cfg_abort_dtc_tables ext_init_op_cfg_abort_dtc_tables[INIT_OP_ROUT_ENUM_MAX];
extern const struct_init_op_cfg_rout_main ext_init_op_cfg_rout_main[INIT_OP_ROUT_ENUM_MAX];



#endif 


