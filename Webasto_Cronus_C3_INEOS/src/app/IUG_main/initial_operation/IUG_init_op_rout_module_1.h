#ifndef IUG_INIT_OP_ROUT_MODULE_1_H
#define IUG_INIT_OP_ROUT_MODULE_1_H





void IUG_init_op_module_1_init(void);

void IUG_init_op_module_1_cyclic(void);

void IUG_init_op_rout_system(void);

#ifdef INIT_OP_CFG_CAN_LINK_EN
enum_INIT_OP_ROUT_STATES IUG_init_op_rout_set_rtc(enum_INIT_OP_ROUT_STATES routine_ctl, enum_parameter_diagnose_id *const ptr_trans_ids);
#endif


#endif 


