#ifndef IUG_INIT_OP_ROUT_MODULE_2_H
#define IUG_INIT_OP_ROUT_MODULE_2_H


#define INIT_OP_MULTI_RESULT_MASK_PENDING       0xF000u
#define INIT_OP_MULTI_RESULT_POS_PENDING        12u
#define INIT_OP_MULTI_RESULT_MASK_IDLE          0x0F00u
#define INIT_OP_MULTI_RESULT_POS_IDLE           8u
#define INIT_OP_MULTI_RESULT_MASK_SUCCESS       0x00F0u
#define INIT_OP_MULTI_RESULT_POS_SUCCESS        4u
#define INIT_OP_MULTI_RESULT_MASK_UNSUCCESS     0x000Fu
#define INIT_OP_MULTI_RESULT_POS_UNSUCCESS      0u

#define INIT_OP_HTR_UNIT_CTRL_REQ_ACT           0x01u
#define INIT_OP_HTR_UNIT_CTRL_REQ_POS_ACK       0x02u

#define INIT_OP_ROUT_HEATER_TEST_RUN_SIG_BTN    0x01u 
#define INIT_OP_ROUT_HEATER_TEST_RUN_SIG_HTR    0x02u 
#define INIT_OP_ROUT_HEATER_TEST_RUN_SIG_DONE   ( (INIT_OP_ROUT_HEATER_TEST_RUN_SIG_BTN) | (INIT_OP_ROUT_HEATER_TEST_RUN_SIG_HTR) ) 

#define INIT_OP_ROUT_HEATER_TEST_RUN_LOCK_RP    0x01u 

typedef struct
{
    uint8_t                        unit_ibn_status;
    IUG_UNIT_HEATER_TYPE_ID_t       unit_type;
    uint8_t                        result_cond_state;
    uint8_t                        req_act;
    uint8_t                        act_flg;
    uint8_t                        fcn_locking;
    uint32_t                       timer;
    const struct_init_op_cfg_para  *ptr_heat_unit_cfg;
    uint8_t                        duration_time;
} struct_init_op_htr_ctrl_data;


void IUG_init_op_module_2_init(void);

void IUG_init_op_module_2_cyclic(void);

enum_INIT_OP_ROUT_STATES IUG_init_op_rout_scan_wbus_htr(enum_INIT_OP_ROUT_STATES routine_ctl, enum_parameter_diagnose_id *const ptr_trans_ids);

static void IUG_init_op_addon_scan_wbus_htr_check_result( void );

enum_INIT_OP_ROUT_STATES IUG_init_op_rout_scan_ghi_htr(enum_INIT_OP_ROUT_STATES routine_ctl, enum_parameter_diagnose_id *const ptr_trans_ids);

static void IUG_init_op_addon_scan_ghi_htr_check_result( void );

enum_INIT_OP_ROUT_STATES IUG_init_op_rout_heater_test_run(enum_INIT_OP_ROUT_STATES routine_ctl, enum_parameter_diagnose_id *const ptr_trans_ids);

static void IUG_init_op_addon_heater_test_run_check_terms(const structure_IUG_UNIT_HEATER_CONTROL_t *const p_unt_ctl, struct_init_op_htr_ctrl_data *const ptr_ibn_heat_unit_ctl_cfg);

enum_INIT_OP_ROUT_STATES IUG_init_op_rout_heater_comp(enum_INIT_OP_ROUT_STATES routine_ctl, enum_parameter_diagnose_id *const ptr_trans_ids);

static void IUG_init_op_set_single_htr_ctrl(const enum_INIT_OP_ROUTINES ibn_step, const uint8_t act_status);

static enum_INIT_OP_ROUT_STATES IUG_init_op_eval_multi_htr_ctrl_result(const struct_init_op_htr_ctrl_data *const ibn_step_cfg);


#endif 


