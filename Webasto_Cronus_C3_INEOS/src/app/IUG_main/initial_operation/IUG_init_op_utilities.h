#ifndef IUG_INIT_OP_UTILITIES_H
#define IUG_INIT_OP_UTILITIES_H

#include "IUG_init_op_handler.h"

#define INIT_OP_PARA_LIVE_TIME_TIMEOUT    0x01u 
#define INIT_OP_PARA_LIVE_TIME_FINISHED   0x02u 

#define INIT_OP_PARA_LIVE_TIME_SECONDS    0x00u 
#define INIT_OP_PARA_LIVE_TIME_MINUTES    0x01u 

#define INIT_OP_DTC_SET_LOCAL             0x00u 
#define INIT_OP_DTC_RESET_LOCAL           0x01u 
#define INIT_OP_DTC_EVAL_LOCAL            0x02u 

#define INIT_OP_DTC_ABORT_TABLE_DEFAULT   0x00u 
#define INIT_OP_DTC_ABORT_TABLE_CUST_1    0x01u 

typedef enum
{
    IUG_INIT_OP_PARA_LIVE_TIME_1 = 0u,   
    IUG_INIT_OP_PARA_LIVE_TIME_2 = 1u,   
    IUG_INIT_OP_PARA_LIVE_TIME_ENUM_MAX  
} enum_INIT_OP_PARA_LIVE_TIME_SLOTS;

typedef struct
{
    uint8_t           act_flg;        
    uint8_t           lock_flg;       
    enum_parameter_id para;           
    uint16_t          para_time_def;  
    uint16_t          para_time_live; 
    uint8_t           abort_flg;      
    uint8_t           time_base;      
} struct_init_op_para_live_time_slots;

typedef struct
{
    uint8_t  live_time_state;                                                           
    uint8_t  live_time_min;                                                             
    uint32_t duration_time_timer;                                                       
    struct_init_op_para_live_time_slots slot_data[IUG_INIT_OP_PARA_LIVE_TIME_ENUM_MAX]; 
} struct_init_op_para_live_time_data;


void IUG_init_op_utilities_init(void);

void IUG_init_op_util_create_rout_diag_ids(const enum_parameter_diagnose_id id, enum_parameter_diagnose_id *const ptr_trans_ids);

void IUG_init_op_util_live_time_para_view_init(const enum_INIT_OP_PARA_LIVE_TIME_SLOTS slot, const enum_parameter_id para, const uint8_t abort_flg, const uint8_t time_base);

enum_INIT_OP_ROUT_STATES IUG_init_op_util_live_time_para_view_process(enum_INIT_OP_ROUT_STATES routine_ctl);

void IUG_init_op_util_live_time_para_view_slot_lock(const enum_INIT_OP_PARA_LIVE_TIME_SLOTS slot);

void IUG_init_op_util_live_time_para_view_slot_unlock(const enum_INIT_OP_PARA_LIVE_TIME_SLOTS slot);

void IUG_init_op_util_live_time_para_view_slot_set_time(const enum_INIT_OP_PARA_LIVE_TIME_SLOTS slot, const uint8_t time);

#ifdef IUG_DIAGMTX3_IMPLEMENTED
void IUG_init_op_util_diag_mtx(void);
#endif


void IUG_init_op_util_diag_webpar(void);

uint8_t IUG_init_op_util_get_rout_result(const enum_ROUTINE_ID_t routine_id, uint8_t *const routine_result);

#ifdef IUG_DEV_ENVIRONMENTAL_SIMULATION_EN
void IUG_init_op_util_set_nvm_para_available( const enum_parameter_id id_nvm_para, const uint8_t nvm_para_available );
#endif

void IUG_init_op_util_handle_dtcs(const enum_DTC_TABLE_IDX dtc, const uint8_t dtc_mode, const IUG_err_hdl_dtc_sypmtom_t dtc_sym);

void IUG_init_op_util_set_abort_dtc_table(const enum_INIT_OP_ROUTINES ibn_routine, const uint8_t cust_table);


#endif 


