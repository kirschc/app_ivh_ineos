
#ifndef __DFL_BMP280_H__
#define __DFL_BMP280_H__

#include "dfl_bmp280_defs.h"

#ifdef __cplusplus
extern "C" {
#endif

int8_t bmp280_get_regs(uint8_t reg_addr, uint8_t *reg_data, uint8_t len, const struct bmp280_dev *dev);

int8_t bmp280_set_regs(uint8_t *reg_addr, const uint8_t *reg_data, uint8_t len, const struct bmp280_dev *dev);

int8_t bmp280_soft_reset(const struct bmp280_dev *dev);

int8_t bmp280_init(struct bmp280_dev *dev);

int8_t bmp280_get_config(struct bmp280_config *conf, struct bmp280_dev *dev);

int8_t bmp280_set_config(const struct bmp280_config *conf, struct bmp280_dev *dev);

int8_t bmp280_get_status(struct bmp280_status *status, const struct bmp280_dev *dev);

int8_t bmp280_get_power_mode(uint8_t *mode, const struct bmp280_dev *dev);

int8_t bmp280_set_power_mode(uint8_t mode, struct bmp280_dev *dev);

int8_t bmp280_get_uncomp_data(struct bmp280_uncomp_data *uncomp_data, const struct bmp280_dev *dev);

int8_t bmp280_get_comp_temp_32bit(int32_t *comp_temp, int32_t uncomp_temp, struct bmp280_dev *dev);

int8_t bmp280_get_comp_pres_32bit(uint32_t *comp_pres, uint32_t uncomp_pres, const struct bmp280_dev *dev);

#ifndef BMP280_DISABLE_64BIT_COMPENSATION

int8_t bmp280_get_comp_pres_64bit(uint32_t *pressure, uint32_t uncomp_pres, const struct bmp280_dev *dev);

#endif 

#ifndef BMP280_DISABLE_DOUBLE_COMPENSATION

int8_t bmp280_get_comp_temp_double(double *temperature, int32_t uncomp_temp, struct bmp280_dev *dev);

int8_t bmp280_get_comp_pres_double(double *pressure, uint32_t uncomp_pres, const struct bmp280_dev *dev);

#endif 

uint8_t bmp280_compute_meas_time(const struct bmp280_dev *dev);

int8_t null_ptr_check(const struct bmp280_dev *dev);

int8_t get_calib_param(struct bmp280_dev *dev);

int8_t st_check_boundaries(int32_t utemperature, int32_t upressure);

#ifdef __cplusplus
}
#endif 

#endif 


