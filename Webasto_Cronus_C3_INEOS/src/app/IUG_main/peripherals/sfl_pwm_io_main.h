#ifndef _SFL_PWM_IO_MAIN_H_
#define _SFL_PWM_IO_MAIN_H_


#define PWM_OUT_STAT_CLR              0x00u 
#define PWM_OUT_STAT_ACT              0x01u
#define PWM_OUT_STAT_SH_DET_DONE      0x02u 

#define PWM_OUT_STAT_SH_UBAT          0x10u 
#define PWM_OUT_STAT_SH_GND           0x20u 

#define PWM_OUT_CFG_SH_DET_DELAY_MS   250u 

#define PWM_OUT_MEAS_AMPL_TIME_MS     5u
#define PWM_OUT_MEAS_AMPL_FLT_SIZE    10u
#define PWM_IN_MEAS_AMPL_FLT_SIZE     10u

#define PWM_IO_CALIBRATION_ACTIVE_KEY 0x06D3235Cu

#define LUT_PWM_OUT_VOLTAGE_IN_SETPOINTS_MAX             33u
#define LUT_PWM_OUT_VOLTAGE_IN_SETPOINTS_COUNT_DEFAULT   4u
#define LUT_PWM_IN_PEAK_V_SETPOINTS_MAX                  33u
#define LUT_PWM_IN_PEAK_V_SETPOINTS_COUNT_DEFAULT        11u
#define LUT_PWM_OUT_VREG_SETPOINTS_MAX                   11u
#define LUT_PWM_OUT_VREG_SETPOINTS_COUNT_DEFAULT         11u
#define LUT_PWM_OUT_DUTY_OFFSET_SETPOINTS_MAX            21u
#define LUT_PWM_OUT_DUTY_OFFSET_SETPOINTS_COUNT_DEFAULT  21u

typedef enum
{
    PWM_OUT_PIN_X1_LIN2 = 0u, 
    PWM_OUT_PIN_X2_IO3  = 1u, 
    PWM_OUT_ENUM_MAX          
} enum_PWM_OUTPUT;

typedef enum
{
    PWM_OUT_PUSHPULL = 0u, 
    PWM_OUT_LOWSIDE  = 1u, 
    PWM_OUT_HIGHSIDE = 2u, 
    PWM_OUT_MODES_ENUM_MAX          
} enum_PWM_MODE;

typedef enum
{
    PWM_OUT_MODE_CTRL = 0u, 
    PWM_OUT_MODE_LUT      , 
    PWM_OUT_MODE_RAW        
}enum_PWM_OUTPUT_MODES;

typedef enum
{
    PWM_IN_PEAK_DETECTOR_STATE_INIT        = 0u, 
    PWM_IN_PEAK_DETECTOR_STATE_RESET           , 
    PWM_IN_PEAK_DETECTOR_STATE_WAIT_STABLE     , 
    PWM_IN_PEAK_DETECTOR_STATE_MEASURE         , 
    PWM_IN_PEAK_DETECTOR_STATE_IDLE            , 
} enum_PWM_IN_PEAK_DETECTOR_STATE;

typedef enum
{
    PWM_OUT_SHORT_DETECT_INIT_STEP_1 = 0u, 
    PWM_OUT_SHORT_DETECT_INIT_STEP_2     , 
    PWM_OUT_SHORT_DETECT_GND             , 
    PWM_OUT_SHORT_DETECT_UBAT            , 
    PWM_OUT_SHORT_DETECT_EVAL              
} enum_PWM_OUT_SHORT_DETECT_STATES;

typedef enum
{
    PWM_OUT_SHORT_TYPE_GND  = 0u, 
    PWM_OUT_SHORT_TYPE_UBAT = 1u, 
    PWM_OUT_SHORT_TYPE_ENUM_MAX       
} enum_PWM_OUT_SHORT_DETECT_TYPES;

typedef enum
{
    PWM_OUT_SHORT_CHECK_NOT_ACT    = 0u, 
    PWM_OUT_SHORT_CHECK_ACT_PU_OFF     , 
    PWM_OUT_SHORT_CHECK_ACT_PU_ON        
} enum_PWM_OUT_SHORT_DETECT_USAGE;

typedef enum
{
    PWM_OUT_IDLE        , 
    PWM_OUT_INIT_STEP_1 , 
    PWM_OUT_INIT_STEP_2 , 
    PWM_OUT_PROCESS     , 
    PWM_OUT_ERROR
} enum_PWM_OUT_PROCESS_STATES;

typedef struct
{
    uint8_t  act_flg;    
    uint32_t amplitude;  
    uint32_t frequency;  
    uint32_t duty_cycle; 
} struct_pwm_values;

typedef struct
{
    enum_PWM_OUT_SHORT_DETECT_STATES state;       
    uint32_t                         timer_delay; 
} struct_pwm_out_short_det;

typedef struct
{
    enum_PWM_OUT_PROCESS_STATES   process_state; 
    struct_pwm_out_short_det      short_det;     
    uint8_t                       switch_off;    
    uint32_t                      duty;          
    uint32_t                      duty_old;      
    uint32_t                      freq;          
    uint32_t                      freq_old;      
} struct_pwm_out_process_data;

typedef struct
{
    enum_PWM_OUT_SHORT_DETECT_TYPES type;         
    enum_PWM_OUT_SHORT_DETECT_USAGE used_flg;     
    Dio_PinIdType                   pull_up;      
    AdcPinTypes                     adc_feedback; 
    uint16_t                        threshold;    
} struct_pwm_out_cfg_short_det_data;

typedef struct
{
    enum_PWM_MODE mode;
    struct_pwm_out_cfg_short_det_data cfg_short_det_data[PWM_OUT_SHORT_TYPE_ENUM_MAX]; 
} struct_pwm_out_cfg_short_det_modes;

typedef struct
{
    enum_PWM_OUTPUT                    pwm_output;                                 
    struct_pwm_out_cfg_short_det_modes cfg_short_det_mode[PWM_OUT_MODES_ENUM_MAX]; 
} struct_pwm_out_config;

typedef struct
{
    const struct_pwm_out_config *ptr_pwm_cfg;     
    uint8_t                     pwm_out_status;   
    struct_pwm_values           pwm_values;       
    enum_PWM_MODE               pwm_mode;         
    struct_pwm_out_process_data pwm_process_data; 
} struct_pwm_out_values;

typedef struct
{
    struct_pwm_out_values pwm_out_ctrl[PWM_OUT_ENUM_MAX]; 
    enum_PWM_OUTPUT       pwm_out_ampl_ctrl_ch;           
    struct_pwm_values     pwm_in_ctrl;                    
} struct_pwm_ctrl;

typedef struct
{
    uint8_t               disable_outputs_only; 
    uint8_t               cal_enable;           
    uint16_t              amplitude_feedback;   
    enum_PWM_OUTPUT_MODES output_mode;          
} struct_pwm_calib_settings;

extern uint32_t ext_pwm_cal_values_active;
extern uint32_t ext_pwm_cal_values_checksum;
extern struct_pwm_calib_settings ext_pwm_calib_settings;

extern uint16_t ext_pwm_out_lut_voltage_in_setpoint_count;
extern int16_t ext_pwm_out_lut_voltage_in1_setpoint[ (LUT_PWM_OUT_VOLTAGE_IN_SETPOINTS_MAX) ];
extern int16_t ext_pwm_out_lut_voltage_in1_value[ (LUT_PWM_OUT_VOLTAGE_IN_SETPOINTS_MAX) ];
extern int16_t ext_pwm_out_lut_voltage_in2_setpoint[ (LUT_PWM_OUT_VOLTAGE_IN_SETPOINTS_MAX) ];
extern int16_t ext_pwm_out_lut_voltage_in2_value[ (LUT_PWM_OUT_VOLTAGE_IN_SETPOINTS_MAX) ];

extern const uint16_t ext_pwm_out_lut_voltage_in_setpoint_count_default;
extern const int16_t ext_pwm_out_lut_voltage_in1_setpoint_default[ (LUT_PWM_OUT_VOLTAGE_IN_SETPOINTS_MAX) ];
extern const int16_t ext_pwm_out_lut_voltage_in1_value_default[ (LUT_PWM_OUT_VOLTAGE_IN_SETPOINTS_MAX) ];
extern const int16_t ext_pwm_out_lut_voltage_in2_setpoint_default[ (LUT_PWM_OUT_VOLTAGE_IN_SETPOINTS_MAX) ];
extern const int16_t ext_pwm_out_lut_voltage_in2_value_default[ (LUT_PWM_OUT_VOLTAGE_IN_SETPOINTS_MAX) ];

extern uint16_t ext_pwm_in_lut_peak_v_setpoint_count;
extern int16_t ext_pwm_in_lut_peak_v_setpoint[ (LUT_PWM_IN_PEAK_V_SETPOINTS_MAX) ];
extern int16_t ext_pwm_in_lut_peak_v_value[ (LUT_PWM_IN_PEAK_V_SETPOINTS_MAX) ];
extern const uint16_t ext_pwm_in_lut_peak_v_setpoint_count_default;
extern const int16_t ext_pwm_in_lut_peak_v_setpoint_default[ (LUT_PWM_IN_PEAK_V_SETPOINTS_MAX) ];
extern const int16_t ext_pwm_in_lut_peak_v_value_default[ (LUT_PWM_IN_PEAK_V_SETPOINTS_MAX) ];

extern uint16_t ext_pwm_out_lut_vreg_setpoint_count;
extern int16_t ext_pwm_out_lut_vreg_setpoint[ (LUT_PWM_OUT_VREG_SETPOINTS_MAX) ];
extern int16_t ext_pwm_out_lut_vreg_range0[ (LUT_PWM_OUT_VREG_SETPOINTS_MAX) ];
extern int16_t ext_pwm_out_lut_vreg_range1[ (LUT_PWM_OUT_VREG_SETPOINTS_MAX) ];

extern const uint16_t ext_pwm_out_lut_vreg_setpoint_count_default;
extern const int16_t ext_pwm_out_lut_vreg_setpoint_default[ (LUT_PWM_OUT_VREG_SETPOINTS_MAX) ];
extern const int16_t ext_pwm_out_lut_vreg_range0_default[ (LUT_PWM_OUT_VREG_SETPOINTS_MAX) ];
extern const int16_t ext_pwm_out_lut_vreg_range1_default[ (LUT_PWM_OUT_VREG_SETPOINTS_MAX) ];

extern uint16_t ext_pwm_out_lut_duty_freq_setpoint_count;
extern int16_t ext_pwm_out_lut_duty_offset_value[ (LUT_PWM_OUT_DUTY_OFFSET_SETPOINTS_MAX) ];
extern int16_t ext_pwm_out_lut_duty_freq_setpoint[ (LUT_PWM_OUT_DUTY_OFFSET_SETPOINTS_MAX) ];

extern const uint16_t ext_pwm_out_lut_duty_freq_setpoint_count_default;
extern const int16_t ext_pwm_out_lut_duty_offset_value_default[ (LUT_PWM_OUT_DUTY_OFFSET_SETPOINTS_MAX) ];
extern const int16_t ext_pwm_out_lut_duty_freq_setpoint_default[ (LUT_PWM_OUT_DUTY_OFFSET_SETPOINTS_MAX) ];


void pwm_module_init(const struct_pwm_out_values **const ptr_pwm_out_data, const struct_pwm_values **const ptr_pwm_in_data);


uint8_t pwm_module_init_calibration_lut(void);

static void pwm_module_init_default_lut(void);

void pwm_module_handler_cyclic(void);

static void pwm_module_diag_webpar(void);

static void pwm_module_eval_err_info_for_dtc(void);

static void pwm_module_pwm_out_handler_cyclic(void);

static uint8_t pwm_module_pwm_out_utility_short_detection(const enum_PWM_OUTPUT pwm_out_ch);

static uint8_t pwm_module_pwm_out_utility_update_ampl_master_ch(void);

static void pwm_module_pwm_out_utility_volt_controller(const enum_PWM_OUTPUT pwm_out_ch);

static void pwm_module_pwm_out_utility_meas_ampl(const enum_PWM_OUTPUT pwm_out_ch);

static uint8_t pwm_module_pwm_out_utility_disable_lin_transc(const enum_PWM_OUTPUT pwm_out_ch);

static int32_t pwm_module_pwm_out_utitlity_limit_pwm_val(const int32_t value, const int32_t min, const int32_t max);

static void pwm_module_pwm_in_handler_cyclic(void);

static void pwm_module_pwm_in_utility_meas_ampl(void);

void pwm_module_pwm_out_set_para(const enum_PWM_OUTPUT pwm_out_ch, const struct_pwm_out_values *const ptr_data);

void pwm_module_pwm_out_enable(const enum_PWM_OUTPUT pwm_out_ch);

void pwm_module_pwm_out_disable(const enum_PWM_OUTPUT pwm_out_ch);

uint8_t pwm_module_pwm_out_get_status(const enum_PWM_OUTPUT pwm_out_ch);

void pwm_module_pwm_in_get_para(struct_pwm_values *const ptr_data);

void pwm_module_pwm_in_enable(void);

void pwm_module_pwm_in_disable(void);

uint8_t pwm_module_pwm_in_get_status(void);


#endif


