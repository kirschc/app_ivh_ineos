#ifndef _CFL_OBD_DIAGNOSIS_MAIN_H_
#define _CFL_OBD_DIAGNOSIS_MAIN_H_



typedef struct
{
    uint8_t  ibn_act_flg;           
    uint32_t timer_ibn_delay;       
    uint8_t  *ptr_pid_ary;          
    uint8_t  *ptr_pid_ary_size_min; 
    uint8_t  pid_ary_size;          
} struct_obd_diagnosis_ctrl_data;


void IUG_obd_main_cyclic(void);

uint8_t IUG_obd_get_car_state(void);

static void IUG_obd_queue_pid_from_ary(void);

static void IUG_obd_diag_webpar(void);

static void IUG_obd_eval_err_info_for_dtc(void);


#endif 


