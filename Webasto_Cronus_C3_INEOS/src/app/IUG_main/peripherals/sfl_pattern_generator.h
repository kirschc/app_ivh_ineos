#ifndef __SFL_PATTERN_GENERATOR_H__
#define __SFL_PATTERN_GENERATOR_H__


#include "hal_data_types.h"






typedef enum
{
    PG_CMD_STOP = 0u,               
    PG_CMD_CYCLIC,                  
    PG_CMD_SINGLE,                  
    PG_CMD_ENUM_MAX,                 
    PG_CMD_ENUM_FORCE_TYPE   = 0x7F  

} enum_PG_PATTERN_COMMAND_t;
typedef enum_PG_PATTERN_COMMAND_t   PG_PATTERN_CMD_t;


typedef enum
{
    PG_MODE_BINARY = 0u,                
    PG_MODE_RAMP,                       
    PG_MODE_ENUM_MAX,                   
    PG_MODE_ENUM_FORCE_TYPE   = 0x7F    

} enum_PG_PATTERN_MODE_t;
typedef enum_PG_PATTERN_MODE_t   PG_PATTERN_MODE_t;




typedef struct
{
    PG_PATTERN_CMD_t    cmd_old;
    uint8_t             processed;      
    uint16_t            x_progress;     

} struct_PG_PATTERN_CONTROL_t;
typedef struct_PG_PATTERN_CONTROL_t   PG_PATTERN_CTL_t;

typedef struct
{
    PG_PATTERN_MODE_t       mode;       
    uint8_t                 point_num;  

    int16_t                 *x_progr;   

    int16_t                 *y_value;   

} struct_PG_PATTERN_CONFIG_t;
typedef struct_PG_PATTERN_CONFIG_t   PG_PATTERN_CFG_t;


int16_t sfl_pattern_generator_progress( const PG_PATTERN_CMD_t pattern_cmd, PG_PATTERN_CFG_t * const pattern_cfg, PG_PATTERN_CTL_t * const pattern_ctl, const uint16_t dx_delta_progress );



inline boolean sfl_pattern_generator_is_processed( PG_PATTERN_CTL_t * const pattern_ctl )
{
    if( 0u != pattern_ctl->processed )
    {
        return (TRUE);
    }
    else
    {
        return (FALSE);
    }
}



#endif 


