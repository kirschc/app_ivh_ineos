#ifndef _DFL_HSD_VN7040_H_
#define _DFL_HSD_VN7040_H_

#include "hal_data_types.h"

#define DFL_VN7040_ERR_CFG   0xFFu 

typedef enum
{
    HSD_VN7040_VOLTAGE = 0u,
    HSD_VN7040_CURRENT
} enum_HSD_VN7040_VALUES;

typedef enum
{
    HSD_VN7040_ERR_NONE       = 0u,
    HSD_VN7040_ERR_INVAL_PARA
} enum_HSD_VN7040_ERR_CODES;

typedef struct
{
    uint32_t               mux_timer; 
    enum_HSD_VN7040_VALUES mux_state; 
} struct_hsd_vn7040_mux_data;

typedef struct
{
    const enum_HSD_VN7040_VALUES val_type;
    uint16_t                     val_raw_phys; 
} struct_hsd_vn7040_values;

void hsd_vn7040_main_cyclic(void);

static void hsd_vn7040_mux_data_cyclic(void);

enum_HSD_VN7040_ERR_CODES hsd_vn7040_get_value(const enum_HSD_VN7040_VALUES val_type, uint16_t *const val);


#endif


