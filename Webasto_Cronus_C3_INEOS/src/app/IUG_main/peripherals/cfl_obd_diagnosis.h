#ifndef _CFL_OBD_DIAGNOSIS_H_
#define _CFL_OBD_DIAGNOSIS_H_

#include "obd_diagnosis_cfg.h"
#include "hal_can.h"

#define OBD_SW_MAJOR   0 
#define OBD_SW_MINOR   3 
#define OBD_SW_BUILD   2 

#define OBD_PROT_DET_CAN_BAUD_CHANGE_DELAY_MS   500u  
#define OBD_PROT_DET_REQ_INTERVAL_MS            200u  
#define OBD_PROT_DET_RETRY_COUNT                5u    

#define OBD_PID_REQ_IDLE                        0x00u 
#define OBD_PID_REQ_ACT                         0x01u 

#define OBD_PID_STAT_REC                        0xF0u 
#define OBD_PID_STAT_ERR                        0x0Fu 

#define OBD_INIT_ERR_NONE                       0x00u 
#define OBD_INIT_ERR_ZP_GET_CAR_STATE           0x01u 
#define OBD_INIT_ERR_ZP_CAN_HANDLE              0x02u 
#define OBD_INIT_ERR_PID_CFG_FAULTY             0x04u 
#define OBD_INIT_ERR_INVALID                    0xFFu 

typedef uint8_t (*funct_ptr_get_car_state)(void);

typedef enum
{
    OBD_ERR_NONE               = 0x00u, 
    OBD_ERR_PROT_NOT_SET       = 0x01u, 
    OBD_ERR_PROT_NOT_FOUND     = 0x02u, 
    OBD_ERR_PID_PENDING        = 0x04u, 
    OBD_ERR_PID_UNKNOWN        = 0x08u, 
    OBD_ERR_PID_TIMEOUT        = 0x10u, 
    OBD_ERR_INACTIVE_CAR_STATE = 0x20u, 
    OBD_ERR_INVAL_CFG          = 0x40u, 
    OBD_ERR_INVAL_PARA         = 0x80u  
} enum_OBD_ERROR_CODES;

typedef enum
{
    OBD_PROT_NOT_SET   = 0u,
    OBD_PROT_CAN250_11     ,
    OBD_PROT_CAN250_29     ,
    OBD_PROT_CAN500_11     ,
    OBD_PROT_CAN500_29     ,
    OBD_PROT_NOT_FOUND
} enum_OBD_PROTOCOLS;

typedef enum
{
    OBD_QUEUE_REQ_NEW      = 0u,
    OBD_QUEUE_REQ_DONE_ERR     ,
    OBD_QUEUE_REQ_DONE
} enum_OBD_QUEUE_STATES;

typedef struct
{
    uint8_t  resp_pid;
    uint8_t  data_byte_1;
    uint8_t  data_byte_2;
    uint8_t  data_byte_3;
    uint8_t  data_byte_4;
    uint32_t res_can_id;
} struct_obd_res_data;

typedef struct
{
    uint32_t req_timer;  
    uint8_t  req_status; 
} struct_obd_monitoring_pid_data;

typedef struct
{
    uint8_t               retry_counter;     
    uint8_t               baud_changed_flg;
    uint32_t              baud_change_timer; 
    enum_OBD_PROTOCOLS    prot_state;        
} struct_obd_det_prot_data;

extern enum_OBD_PROTOCOLS ext_obd_prot;


void obd_init(const funct_ptr_get_car_state funct_ptr_get_car_state, struct_hal_can_handle *const ptr_can_handle);

static void obd_clear_data_buffer(void);

void obd_can_rx_callback(const uint8_t can_bus, const struct_hal_can_frame *const ptr_can_msg);

void obd_module_handler_cyclic(const uint8_t enable_ctrl);

static enum_OBD_ERROR_CODES obd_module_detect_protocol(void);

static void obd_module_trg_request(void);

static void obd_module_queue_handler(const enum_PIDS_DESCRIPTION req_pid, const enum_OBD_QUEUE_STATES queue_state);

static void obd_module_calculate_phys_pid_val(volatile const struct_obd_res_data *const ptr_res_data);

static enum_OBD_ERROR_CODES obd_module_check_pid_error(void);

enum_OBD_ERROR_CODES obd_set_pid_request(const enum_PIDS_DESCRIPTION req_pid);

enum_PIDS_DESCRIPTION obd_get_pid_name_index(const uint8_t req_pid);

enum_OBD_ERROR_CODES obd_get_phys_pid_val(const enum_PIDS_DESCRIPTION pid, int32_t *const pid_value);

enum_OBD_ERROR_CODES obd_get_error_state(void);


#endif


