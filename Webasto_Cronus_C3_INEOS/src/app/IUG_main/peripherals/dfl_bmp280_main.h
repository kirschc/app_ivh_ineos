
#ifndef _DFL_BMP280_MAIN_H_
#define _DFL_BMP280_MAIN_H_


#define BMP280_MAX_INIT_RETRIES        5u                   
#define BMP280_MAX_OPERATIONAL_RETRIES 5u                   
#define BMP280_SPI_WAIT_TIMEOUT        50u                  
#define BMP280_MEAS_TIME_TRG           1000u                
#define BMP280_ERROR_TIME              60000u               

_Static_assert((ENV_SENSOR_BMP280_ACCESS_TIME)<=(BMP280_MEAS_TIME_TRG)     ,"The measurement cycle time must be greater than main cycle time,because otherwise the measuring time cannot be guaranteed!");

#define BMP280_SUCCESSFUL              0                    
#define BMP280_COM_RSLT_ERROR          INT8_C(-1)           
#define BMP280_COM_RSLT_NOT_IMPL       INT8_C(-2)           

#define BMP280_POWER_MODE               BMP280_FORCED_MODE  
typedef enum
{
    BMP280_TEMPERATURE  = 0u,      
    BMP280_AIR_PRESSURE     ,      
    BMP280_ENUM_MAX                
} enum_BMP280_TYPES;


typedef enum
{
    STAGE_INIT          = 0u,   
    STAGE_OPERATIONAL       ,   
    STAGE_ERROR             ,   
    STAGE_ENUM_MAX              
} enum_BMP280_STAGES;

typedef enum
{
    INIT_STATE_BMP280_SOFT_RESET        = 0u,   
    INIT_STATE_BMP280_GET_CHIP_ID           ,   
    INIT_STATE_BMP280_GET_CALIB_PARAM       ,   
    INIT_STATE_BMP280_SET_CONFIG            ,   
    INIT_STATE_ENUM_MAX                         
} enum_BMP280_INIT_STATES;

typedef enum
{
    CONFIG_STEP_BMP280_GET_CONFIG       = 0u,       
    CONFIG_STEP_BMP280_SOFT_RESET           ,       
    CONFIG_STEP_BMP280_SET_CONFIG           ,       
    CONFIG_STEP_BMP280_SET_POWER_MODE       ,       
    CONFIG_STEP_ENUM_MAX                            
} enum_BMP280_CONFIG_STEPS;

typedef enum
{
    OPERATIONAL_STATE_BMP280_IDLE            = 0u,  
    OPERATIONAL_STATE_BMP280_SET_POWER_MODE      ,  
    OPERATIONAL_STATE_BMP280_GET_UNCOMP_DATA     ,  
    OPERATIONAL_STATE_BMP280_GET_COMP_DATA       ,  
    OPERATIONAL_STATE_ENUM_MAX                      
} enum_BMP280_OPERATIONAL_STATES;

typedef enum
{
    ERROR_STATE_BMP280_GENERAL_ERR    = 0u, 
    ERROR_STATE_ENUM_MAX                    
} enum_BMP280_ERROR_STATES;

typedef enum
{
    BMP280_SPI_FLOW_CTRL_COM_IDLE = 0x00u, 
    BMP280_SPI_FLOW_CTRL_TX_REQ   = 0x01u, 
    BMP280_SPI_FLOW_CTRL_RX_REQ   = 0x02u, 
    BMP280_SPI_FLOW_CTRL_ENUM_MAX          
} enum_BMP280_SPI_FLOW_CTRL;

extern struct_hal_spi_handle ext_spi_handle_bmp280; 

typedef struct
{
    enum_BMP280_TYPES val_type;                 
    uint32_t          val_raw;                  
    int16_t           val_norm;                 
    int16_t           val_final;                
} struct_bmp280_values;

typedef struct
{
    uint8_t is_bmp280_init_done_flg;            
    uint8_t active_stage;                       
    uint8_t active_init_state;                  
    uint8_t active_operational_state;           
    uint8_t active_error_state;                 
    uint8_t config_step;                        
    uint8_t init_retry_counter;                 
    uint8_t operational_retry_counter;          
} struct_bmp280_state_data;


int32_t BMP280_init(void);

int32_t BMP280_deinit(void);

void bmp280_main_cyclic(void);


static int8_t SPI_routine(void);



static int8_t BMP280_SPI_bus_read(const uint8_t dev_addr, const uint8_t reg_addr, uint8_t *const reg_data, const uint16_t cnt);

static int8_t BMP280_SPI_bus_write(const uint8_t dev_addr, const uint8_t reg_addr, uint8_t *const reg_data, const uint16_t cnt);

static void BMP280_delay_msek(uint32_t msek);

void BMP280_env_init(const int16_t __packed* * const ptr_temp_val, const enum_ENV_TEMP_SENS temp_sens, funct_ptr_ibn_temp_sens* funct_get_temp);

static void BMP280_calc_val_cyclic(void);

static enum_ENV_ERR_TEMP_SENS BMP280_get_temperature( int16_t *const temp_value, const enum_ENV_TEMP_SENS temp_sens );

enum_ENV_ERR_TEMP_SENS BMP280_get_air_pressure(uint16_t *const temp_value);

static void BMP280_diag_webpar(void);

static void BMP280_eval_err_info_for_dtc(const int32_t error_info);


#endif 


