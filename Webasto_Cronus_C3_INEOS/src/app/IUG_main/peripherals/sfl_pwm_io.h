
#ifndef _SFL_PWM_IO_H_
#define _SFL_PWM_IO_H_

#define NR_MAX_INT16    32767
#define NR_MIN_INT16   -32768

#define CPU_BUSCLK 40000000UL
#define PWM_DEADTIME 400       

typedef enum
{
    PWM_OUT_LIN_DISABLE_SEQ_IDLE    = 0u,
    PWM_OUT_LIN_DISABLE_SEQ_INIT        ,
    PWM_OUT_LIN_DISABLE_SEQ_STEP_1      ,
    PWM_OUT_LIN_DISABLE_SEQ_STEP_2      ,
    PWM_OUT_LIN_DISABLE_SEQ_STEP_3      ,
    PWM_OUT_LIN_DISABLE_SEQ_STEP_4      ,
    PWM_OUT_LIN_DISABLE_SEQ_STEP_5      ,
    PWM_OUT_LIN_DISABLE_DONE
} enum_PWM_OUT_LIN_DISABLE_SEQ;

void pwm_init_rh850_timer(void);

static void pwm_in_init_tauj0(void);

static void pwm_out_init_taud(const enum_PWM_OUTPUT module, const enum_PWM_MODE mode);

void pwm_out_enable(const enum_PWM_OUTPUT module, const enum_PWM_MODE mode, const uint8_t volt_ctrl_flg);

void pwm_out_taud_set_duty_cycle_pushpull(const enum_PWM_OUTPUT module,
                                          uint16_t pwm_promille,
                                          const uint16_t pwm_resolution,
                                          const uint16_t deadtime,
                                          const uint8_t update_reg);

uint32_t pwm_out_taud_set_freq(const enum_PWM_OUTPUT module, const uint32_t frequency);

void pwm_out_disable(const enum_PWM_OUTPUT module, const uint8_t disable_outputs_only, const uint8_t volt_ctrl_flg);


#endif 


