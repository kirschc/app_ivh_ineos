#ifndef _IUG_environment_DATA_H_
#define _IUG_environment_DATA_H_


#define ENV_SENSOR_READOUT_CYCL_POR             4u      

#define ENV_SENSOR_TEMPERATURE_INVALID          0xFEu
#define ENV_SENSOR_TEMPERATURE_OUT_OF_RANGE     0xFFu
#define ENV_SENSOR_AIR_PRESSURE_INVALID         0xFFFEu
#define ENV_SENSOR_AIR_PRESSURE_OUT_OF_RANGE    0xFFFFu

#define ENV_TEMP_SENS_ERR_UNUSED                0x00u   
#define ENV_TEMP_SENS_VAL_UNUSED                0x00u   

#define ENV_SENSOR_BMP280_ACCESS_TIME           10u    

_Static_assert( (10u <= (ENV_SENSOR_BMP280_ACCESS_TIME)), "The access time of the bmp280 sensor must be greater than 10ms, because of the minimum access time of the bosch sensor");

#define ENV_SENSOR_CMD_BUTTON_TRESHHOLD_TIME    500u    

#define ENV_SENSOR_CMD_BUTTON_THRESHOLD_OL      3500u   

#define ENV_SENSOR_CMD_BUT_THOLD_UBAT           1000u   

#define ENV_SENSOR_CMD_BUT_THOLD_GND            100u    

#define HSD_VN7040_DTC_FLT_ON                   5000u   
#define HSD_VN7040_DTC_FLT_OFF                  500u    
#define HSD_VN7040_OVERLOAD_THOLD               1800u   
#define HSD_VN7040_SHORT_GND_THOLD              100u    


typedef enum
{
    ENV_DATA_ADC_NO_ERR     = 0u,
    ENV_DATA_ADC_NOT_AVAIL      ,
    ENV_DATA_ADC_ENUM_FORCE_TYPE = 0x7Fu 
} enum_ENV_DATA_ADC_ERRORS;

typedef enum
{
    ENV_TEMP_SENS_INTERNAL  = 0u, 
    ENV_TEMP_SENS_EXTERNAL      , 
    ENV_TEMP_SENS_EXPLICIT      ,
    ENV_TEMP_SENS_LOCATION_ENUM_FORCE_TYPE = 0x7Fu 
} enum_ENV_TEMP_SENS_LOCATION;

typedef enum
{
    ENV_TEMP_SENS_INT_USER   = 0u,
    ENV_TEMP_SENS_INT_WBUS       ,
    ENV_TEMP_SENS_INT_BUTTON     ,
    ENV_TEMP_SENS_INT_BMP        ,
#ifdef IUG_DEV_ENVIRONMENTAL_SIMULATION_EN
    ENV_TEMP_SENS_EXT_SIM        ,
#endif
    ENV_TEMP_SENS_EXT_USER       ,
    ENV_TEMP_SENS_EXT_WBUS       ,
    ENV_TEMP_SENS_MAX            , 
    ENV_TEMP_SENS_ENUM_FORCE_TYPE = 0x7Fu  
} enum_ENV_TEMP_SENS;

typedef enum
{
    ENV_ERR_TEMP_SENS_NONE       = 0u, 
    ENV_ERR_TEMP_SENS_NOT_AVAL       , 
    ENV_ERR_TEMP_SENS_EVAL           , 
    ENV_ERR_TEMP_SENS_RANGE          , 
    ENV_ERR_TEMP_SENS_SHORT_VBAT     , 
    ENV_ERR_TEMP_SENS_SHORT_GND      , 
    ENV_ERR_TEMP_SENS_OPEN_LOAD      , 
    ENV_ERR_TEMP_SENS_GEN_FAULT      , 
    ENV_ERR_TEMP_ENUM_FORCE_TYPE = 0x7Fu 
} enum_ENV_ERR_TEMP_SENS;

typedef enum_ENV_ERR_TEMP_SENS (*funct_ptr_ibn_temp_sens)(int16_t *const, const enum_ENV_TEMP_SENS);
typedef void (*ptr_funct_temp_sens_init)(const int16_t __packed* *const, const enum_ENV_TEMP_SENS, funct_ptr_ibn_temp_sens*);

typedef struct
{
    const AdcPinTypes ai_port;        
    uint16_t          ai_mV_raw;      
    uint16_t          ai_mV_filtered; 
    struct_PT1_FILTER ai_pt1_filter;  
} struct_analog_inputs_values;

typedef struct
{
    enum_ENV_ERR_TEMP_SENS  temp_sens_status;
    funct_ptr_ibn_temp_sens funct_get_temp;
    const int16_t           __packed *ptr_temp_sens_val;
} struct_temp_sens_status;

typedef struct
{
    enum_ENV_TEMP_SENS          temp_sens;
    enum_ENV_TEMP_SENS_LOCATION temp_location;
    uint16_t                    temp_sens_err_ubat_mV; 
    uint16_t                    temp_sens_err_gnd_mV;  
    int16_t                     temp_range_min;
    int16_t                     temp_range_max;
    uint16_t                    air_pressure_min;
    uint16_t                    air_pressure_max;
    ptr_funct_temp_sens_init    funct_temp_sens_init;
    enum_parameter_id           id_nvm_para;
    struct_temp_sens_status     *ptr_temp_sens_status;
} struct_temp_sens_cfg;

typedef struct
{
    const enum_ENV_TEMP_SENS sensor;
    int16_t                  temp_val_raw;
    int16_t                  temp_val_final;
} struct_env_user_specific_sensor;

extern const struct_temp_sens_cfg ext_temp_sens_cfg[];


void IUG_env_data_init(void);

void IUG_env_process_data(void);

enum_ENV_DATA_ADC_ERRORS IUG_env_get_adc_value_flt(const AdcPinTypes adc_port, uint16_t *const adc_value);

enum_ENV_ERR_TEMP_SENS IUG_env_temp_request( const enum_ENV_TEMP_SENS_LOCATION temp_sens_location,
                                             const enum_ENV_TEMP_SENS temp_sens,
                                             int16_t *const temp_value );


#ifdef IUG_DEV_ENVIRONMENTAL_SIMULATION_EN
void IUG_env_ext_sim_sens_env_init(const int16_t __packed* * const ptr_temp_val, const enum_ENV_TEMP_SENS temp_sens, funct_ptr_ibn_temp_sens* funct_get_temp);
#endif

#ifdef IUG_DEV_ENVIRONMENTAL_SIMULATION_EN
static void IUG_env_ext_sim_sens_calc_temp_cyclic(void);
#endif

#ifdef IUG_DEV_ENVIRONMENTAL_SIMULATION_EN
static enum_ENV_ERR_TEMP_SENS IUG_env_ext_sim_sens_get_temp(int16_t *const temp_value, const enum_ENV_TEMP_SENS temp_sens);
#endif

void IUG_env_user_sens_env_init(const int16_t __packed* * const ptr_temp_val, const enum_ENV_TEMP_SENS temp_sens, funct_ptr_ibn_temp_sens* funct_get_temp);

enum_ENV_ERR_TEMP_SENS IUG_env_user_sens_set_temp(const enum_ENV_TEMP_SENS temp_sens, const int16_t temp_val);

static void IUG_env_user_sens_temp_cyclic(void);

static enum_ENV_ERR_TEMP_SENS IUG_env_user_sens_get_temp(int16_t *const temp_value, const enum_ENV_TEMP_SENS temp_sens );

#ifdef IUG_DEV_ENVIRONMENTAL_SIMULATION_EN
static uint8_t IUG_env_user_test_nvm_para_available( const enum_parameter_id id_nvm_para );
#endif

static void IUG_env_diag_webpar(void);

static void IUG_env_eval_err_info_for_dtc(void);



#endif


