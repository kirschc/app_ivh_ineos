#ifndef _IUG_TYPES_BASIC_H_
#define _IUG_TYPES_BASIC_H_


#include "IUG_unit_constant_t_lin_r.h"


typedef uint8_t     IUG_PHYS_TEMP_1_1DEG_T;         
#define IUG_PHYS_TEMP_1_1DEG_T_OFFSET        50u    

typedef uint8_t     IUG_PHYS_TIME_1M_T;             

typedef uint16_t    IUG_PHYS_AIR_PRESSURE_HPA_T;    
typedef uint8_t     IUG_PHYS_AIR_PRESSURE_5HPA_T;   

typedef int16_t     IUG_TEMP_CELSIUS_05DEG_T;       

typedef uint16_t    IUG_TIME_MS_T;                  

typedef uint16_t    IUG_TIME_S_T;                   

typedef uint16_t    IUG_TIME_M_T;                   

typedef uint16_t    IUG_DURATION_TIME_M_T;          


#if( 16u == (IUG_UNIT_RD_WR_INTERFACE_NUM_ID_MAX) )
typedef uint16_t   IUG_UNIT_RD_WR_INTERFACE_CARRIER_t;
typedef IUG_UNIT_RD_WR_INTERFACE_CARRIER_t      IUG_UNIT_READ_ID_CARRIER_t;
typedef IUG_UNIT_RD_WR_INTERFACE_CARRIER_t      IUG_UNIT_READ_STATUS_CARRIER_t;
#else
    #error "Analyze value for 'IUG_UNIT_RD_WR_INTERFACE_NUM_ID_MAX'!"
#endif


typedef uint8_t             IUG_UNIT_PRPTY_FLAG_t;


typedef uint32_t            IUG_HEATER_TERM_FLAG_t;


typedef uint16_t            IUG_USER_DEVICE_TERM_FLAG_t;    


typedef uint8_t *   IUG_QUEUE_KEY_t;

typedef uint8_t     IUG_QUEUE_FLAGS_ENTRY_t;        
typedef uint16_t    IUG_QUEUE_FLAGS_INSTANCE_t;     
typedef uint32_t    IUG_QUEUE_TIMESTAMP_t;          
typedef uint16_t    IUG_QUEUE_TIMEOUT_t;            

_Static_assert(4u==(sizeof(IUG_QUEUE_TIMESTAMP_t)),"Analyze: IUG_QUEUE_TIMESTAMP_t");
_Static_assert(2u==(sizeof(IUG_QUEUE_TIMEOUT_t)  ),"Analyze: IUG_QUEUE_TIMEOUT_t");


typedef uint8_t     IUG_WBUS_MSG_FLAG_t;


typedef uint16_t    IUG_BRDCST_ID_t;


typedef uint32_t    IUG_BRDCST_INDICATION_t;


typedef enum
{
    IUG_WEBPAR_TIME_HOUR = 0u,      
    IUG_WEBPAR_TIME_MINUTE,         
    IUG_WEBPAR_TIME_SECOND,         
    IUG_WEBPAR_TIME_DAY,            
    IUG_WEBPAR_TIME_MONTH,          
    IUG_WEBPAR_TIME_YEAR,           
    IUG_WEBPAR_TIME_IDX_ENUM_MAX    
} enum_IUG_WEBPAR_TIME_IDX_t;
typedef enum_IUG_WEBPAR_TIME_IDX_t      IUG_WEBPAR_TIME_IDX_t;
_Static_assert(6u==(IUG_WEBPAR_TIME_IDX_ENUM_MAX),"Analyze: IUG_WEBPAR_TIME_IDX_ENUM_MAX");


typedef enum
{
    IUG_WEBPAR_TIME_CPY_RTC_2_WEBPAR = 0u,  
    IUG_WEBPAR_TIME_CPY_WEBPAR_2_RTC,       
    IUG_WEBPAR_TIME_CPY_ENUM_MAX            
} enum_IUG_WEBPAR_TIME_COPY_t;
typedef enum_IUG_WEBPAR_TIME_COPY_t     IUG_WEBPAR_TIME_COPY_t;
_Static_assert(2u==(IUG_WEBPAR_TIME_CPY_ENUM_MAX),"Analyze: IUG_WEBPAR_TIME_CPY_ENUM_MAX");


typedef enum
{
    IUG_SHARED_PARA_CTRL_TRG__CAN = 0x01u, 
    IUG_SHARED_PARA_CTRL_TRG_WBUS = 0x02u, 
    IUG_SHARED_PARA_CTRL_TRG__BSW = 0x04u, 
    IUG_SHARED_PARA_CTRL_CFM__CAN = 0x10u, 
    IUG_SHARED_PARA_CTRL_CFM_WBUS = 0x20u, 
    IUG_SHARED_PARA_CTRL_CFM__BSW = 0x40u, 
} enum_IUG_SHARED_PARA_CTRL;

typedef enum
{
    IUG_VAR_NONE               = 0u, 
    IUG_VAR_DEV_2              = 1u, 
    IUG_VAR_DEV                = 2u, 
    IUG_VAR_SMART_24V          = 3u, 
    IUG_VAR_SMART_12V          = 4u, 
    IUG_VAR_SMART_24V_LIN3_24V = 5u, 
    IUG_VAR_SMART_24V_LIN3_12V = 6u, 
} enum_IUG_VARIANTS;
_Static_assert(0u==(IUG_VAR_NONE)               ,"Change encoding for webasto parameter 'CRONUS_VARIANTE' too!!!");
_Static_assert(1u==(IUG_VAR_DEV_2)              ,"Change encoding for webasto parameter 'CRONUS_VARIANTE' too!!!");
_Static_assert(2u==(IUG_VAR_DEV)                ,"Change encoding for webasto parameter 'CRONUS_VARIANTE' too!!!");
_Static_assert(3u==(IUG_VAR_SMART_24V)          ,"Change encoding for webasto parameter 'CRONUS_VARIANTE' too!!!");
_Static_assert(4u==(IUG_VAR_SMART_12V)          ,"Change encoding for webasto parameter 'CRONUS_VARIANTE' too!!!");
_Static_assert(5u==(IUG_VAR_SMART_24V_LIN3_24V) ,"Change encoding for webasto parameter 'CRONUS_VARIANTE' too!!!");
_Static_assert(6u==(IUG_VAR_SMART_24V_LIN3_12V) ,"Change encoding for webasto parameter 'CRONUS_VARIANTE' too!!!");

typedef struct
{
    union
    {
        struct
        {
          uint8_t hour;
          uint8_t minute;
          uint8_t second;
          uint8_t day;
          uint8_t month;
          uint8_t year;
        } ind;    

        uint8_t     ary[(IUG_WEBPAR_TIME_IDX_ENUM_MAX)];    

    } uni;
} struct_IUG_WEBPAR_TIME_t;
typedef struct_IUG_WEBPAR_TIME_t    IUG_WEBPAR_TIME_t;


#pragma pack(1) 
typedef struct
{
    uint8_t     sam_invalid_access_attempt_cnt; 
    uint16_t    sam_invalid_access_locked_s;    
} struct_IUG_SAM_REMANENT_DATA_t;
#pragma pack() 


#pragma pack(1) 
typedef struct
{
    uint8_t     securized_mode;     
    uint8_t     securized_service;  
} struct_IUG_SAM_SECURIZED_SERVICE_t;
#pragma pack() 


typedef enum
{
    WBUS_MSG_OPFLG_IDX_REQ = 0u,    
    WBUS_MSG_OPFLG_IDX_ACK,         
    WBUS_MSG_OPFLG_IDX_NAK,         
    WBUS_MSG_OPFLG_IDX_ENUM_MAX     
} enum_WBUS_MSG_DESCRIPTION_FLAG_IDX_t;
_Static_assert(3u==(WBUS_MSG_OPFLG_IDX_ENUM_MAX),"Analyze: WBUS_MSG_OPFLG_IDX_ENUM_MAX");


typedef struct
{
    uint8_t   mode;           
    uint8_t   service;        

    uint8_t   op_flag[(WBUS_MSG_OPFLG_IDX_ENUM_MAX)];       

} struct_WBUS_MSG_DESCRIPTION_CFG_t;


typedef struct
{
    uint8_t     service;    
    uint8_t     size_req;   
    uint8_t     size_ack;   
    uint8_t     size_nak;   

} struct_WBUS_MSG_BTM_DESCRIPTION_CFG_t;


typedef struct
{
    uint8_t             msg_SE;         
    uint16_t            msg_size;       
    uint8_t             msg_mode;       
    uint8_t             msg_service;    
    uint8_t             msg_service_sub;
    IUG_WBUS_MSG_FLAG_t msg_flag;       


    struct_WBUS_MSG_DESCRIPTION_CFG_t       msg_cfg;
    struct_WBUS_MSG_BTM_DESCRIPTION_CFG_t   msgbtm_cfg;

    enum_WBUS_MSG_DESCRIPTION_FLAG_IDX_t    op_flg_idx;     
    uint8_t             op_flag;       

    WBUS_MEMBER_t       msg_mbrtrm_S;           
    WBUS_MEMBER_t       msg_mbrrcp_E;           
    uint8_t             msg_in_description;     
    uint8_t             msg_is_btm_msg;         
    uint8_t             msg_is_request;         
    uint8_t             msg_is_broadcast;       
    uint8_t             msg_is_ack_nak;         
    uint8_t             msg_is_ack_pos;         
    uint8_t             msg_is_ack_neg;         
    uint8_t             msg_will_be_analyzed;   
    uint8_t             msg_wtt_to_iug;         

} struct_WBUS_MSG_DESCRIPTION_CTL_t;    
typedef struct_WBUS_MSG_DESCRIPTION_CTL_t   WBUS_MSG_DESCRIPTION_CTL_t;


typedef enum
{
    IUG_POWER_SUPPLY_VOLTAGE_NOT_SET    = 0u, 
    IUG_POWER_SUPPLY_VOLTAGE_12V            , 
    IUG_POWER_SUPPLY_VOLTAGE_24V            , 
    IUG_POWER_SUPPLY_VOLTAGE_MAX              
} enum_IUG_POWER_SUPPLY_VOLTAGE_t;


typedef struct
{
    enum_IUG_POWER_SUPPLY_VOLTAGE_t voltage_id; 
    uint16_t    vltg_low_limit_mV;      
    uint16_t    vltg_high_limit_mV;     
} structure_IUG_POWER_SUPPLY_CFG_t;


typedef struct
{
    uint16_t    K15_vltg_filtered_mV;       
    uint16_t    K30_vltg_filtered_mV;       
} structure_IUG_POWER_SUPPLY_CTL_t;


#pragma pack(1) 
typedef struct
{
    struct_IUG_SAM_REMANENT_DATA_t  SAM_rem_data;

    uint32_t                        NVM_UsrCfgDat_TrgDefault;


} struct_IUG_NVM_FREQUENT_DATA_t;
#pragma pack() 


typedef struct
{
    uint8_t                             ghi_bus_heater_available;

} IUG_GHIBUS_STATUS_t;


typedef struct
{
    uint8_t     active;                     
    uint8_t     active_timeout_trigger;     
    uint16_t    active_timeout_ms;          
    uint8_t     random_num[4u];             
    uint32_t    random_nbr;                 
} struct_IUG_SAM_SECURITY_ACCESS_MODE_DATA_t;
typedef struct_IUG_SAM_SECURITY_ACCESS_MODE_DATA_t  IUG_SAM_SECURITY_ACCESS_MODE_DATA_t;


typedef struct
{
    uint8_t     mab_entered;                
    uint16_t    mab_timeout_ms;             

} struct_IUG_MAB_MEMORY_ACCESS_BLOCKMODE_DATA_t;
typedef struct_IUG_MAB_MEMORY_ACCESS_BLOCKMODE_DATA_t   IUG_MAB_MEMORY_ACCESS_BLOCKMODE_DATA_t;


typedef struct
{
    uint8_t             rtc_time_valid;          
    struct_rtc_time_t   rtc_time;                
    uint8_t             rtc_time_set_via_webpar; 

    uint8_t             tim_sec_grid;            
    uint8_t             tim_min_grid;            
    uint16_t            tim_min_gone;            

} struct_IUG_SYSTEM_TIME_t;
typedef struct_IUG_SYSTEM_TIME_t    IUG_SYSTEM_TIME_t;

typedef struct
{
    enum_API_PORT_MODE_X1_CAN3_MODES           port_x1_can_3;    
    enum_API_PORT_MODE_X1_15_IO1_LED_MODES     port_x1_15_io1;   
    enum_API_PORT_MODE_X1_11_IO4_TEMP_IN_MODES port_x1_11_io4;   
    enum_API_PORT_MODE_X1_9_KL15_MODES         port_x1_9_kl15;   
    enum_API_PORT_MODE_X1_LIN_PWM_REL_MODES    port_x1_lin_rel;  
    enum_API_PORT_MODE_X1_3_LIN1_PWM_IN_MODES  port_x1_3_lin1;   
    enum_API_PORT_MODE_X1_4_LIN2_PWM_OUT_MODES port_x1_4_lin2;   
    enum_API_PORT_MODE_X1_5_LIN3_WBUS_HG_MODES port_x1_5_lin3;   
    enum_API_PORT_MODE_X1_6_LIN4_WBUS_BE_MODES port_x1_6_lin4;   
    enum_API_PORT_MODE_X1_7_LIN5_MODES         port_x1_7_lin5;   
    enum_API_PORT_MODE_X1_12_OUT1_MODES        port_x1_12_out1;  
    enum_API_PORT_MODE_X1_10_PUSH_BUTTON_MODES port_x1_10_btn;   
    enum_API_PORT_MODE_X1_14_REL1_MODES        port_x1_14_rel1;  
    enum_API_PORT_MODE_X1_13_VDD12_BE_MODES    port_x1_13_vdd12; 
    enum_API_PORT_MODE_X2_CAN_REL_MODES        port_x2_can_rel;  
    enum_API_PORT_MODE_X2_CAN1_MODES           port_x2_can1;     
    enum_API_PORT_MODE_X2_CAN2_MODES           port_x2_can2;     
    enum_API_PORT_MODE_X2_5_IO2_MODES          port_x2_5_io2;    
    enum_API_PORT_MODE_X2_6_IO3_PWM_OUT_MODES  port_x2_6_io3;    
    enum_API_PORT_MODE_X2_12_REL2_MODES        port_x2_12_rel2;  
    enum_API_PORT_MODE_X2_RELAIS_MODES         port_x2_rel;      
} struct_port_modes_ram;


#endif 


