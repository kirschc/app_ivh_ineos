#ifndef _PCM_PARAM_CONFIG_MANAGEMENT_H_
#define _PCM_PARAM_CONFIG_MANAGEMENT_H_



#define HAL_START_ADDR_ABSOLUT                      512u    

#define PCM_NVM_ADDR_INITITAL_PMIC_GSR              0x6BAu  

#define PCM_NVM_BLOCK_3_SIZE_DATA_END_OF_LIST_CHECK_VALUE   2u 



#define PCM_NVM_IUG_CFG_RESERVED            0x200u  


typedef enum
{
    PCM_OK                                       = 0u,
    PCM_ERROR_WRITE_NVM                              ,
    PCM_ERROR_READ_NVM                               ,
    PCM_ERROR_PCM_DATA_TYPE_INVALID                  ,
    PCM_ERROR_PCM_DATA_TYPE_NOT_IMPLEMENTED          ,
    PCM_ERROR_PCM_DATA_DIR_INVALID                   ,
    PCM_ERROR_PCM_DATA_HANDLER_SETTING_INVALID       ,
    PCM_ERROR_PCM_DATA_ENUM_INVALID                  ,
    PCM_ERROR_PCM_DATA_ADDRESS_INVALID               ,
    PCM_ERROR_PCM_DATA_ACCESS_INVALID                ,
    PCM_ERROR_PCM_COPY_DATA_SERVICE_INVALID          ,
    PCM_ERROR_TAG_TYPE_INVALID                       ,
    PCM_ERROR_BUSY                                   ,
    PCM_ERROR_INIT

}enum_PCM_RETURN_VALUE;


typedef enum
{
    PCM_DATA_TYPE_DEFAULT                         = 0u, 
    PCM_DATA_TYPE_CUSTOM                                

}enum_PCM_DATA_TYPE;


typedef enum
{
    PCM_DATA_DIR_NOT_SET = 0u,              
    PCM_DATA_DIR_READ_FROM_NVM,             
    PCM_DATA_DIR_WRITE_TO_NVM,              
    PCM_DATA_DIR_READ_FROM_NVM_FULL_BLOCK,  
    PCM_DATA_DIR_READ_FROM_NVM_INIT_ALL,    
    PCM_DATA_DIR_WRITE_TO_NVM_FULL_BLOCK,   
    PCM_DATA_DIR_WRITE_TO_NVM_INIT_ALL,     
    PCM_DATA_DIR_WRITE_TO_NVM_CUSTOM_ALL,   
    PCM_DATA_DIR_ENUM_FORCE_TYPE = 0x7F     

}enum_PCM_DATA_DIRECTION;


typedef enum
{
    PCM_DATHDLSRV_NONE = 0,
    PCM_DATHDLSRV_RD_FROM_NVM_SINGLE_TEST,
    PCM_DATHDLSRV_RD_FROM_NVM_SINGLE_DISPATCH,
    PCM_DATHDLSRV_RD_FROM_NVM_FULL_TEST,
    PCM_DATHDLSRV_RD_FROM_NVM_FULL_DISPATCH,
    PCM_DATHDLSRV_WR_TO_NVM_SINGLE_DEF_TO_CUST,
    PCM_DATHDLSRV_WR_TO_NVM_SINGLE_TEST,
    PCM_DATHDLSRV_WR_TO_NVM_SINGLE_GATHER,
    PCM_DATHDLSRV_WR_TO_NVM_FULL_DEF_TO_CUST,
    PCM_DATHDLSRV_WR_TO_NVM_FULL_TEST,
    PCM_DATHDLSRV_WR_TO_NVM_FULL_GATHER

} enum_PCM_COPY_DATA_SERVICE_t;


typedef struct
{
    enum_PCM_DATA_TYPE          DataType;
    enum_PCM_DATA_DIRECTION     DataDirection;
}struct_PCM_HANDLER;

#pragma pack(1) 
typedef struct
{
    uint32_t    PCM_Data_Version;           
    uint16_t    PCM_Data_Size;              

    uint8_t     ApplicationVersionMajor;    
    uint8_t     ApplicationVersionMinor;    
    uint8_t     ApplicationVersionBuild;    
    uint8_t     AV_Filler;

    uint8_t     HwtestVersionMajor;         
    uint8_t     HwtestVersionMinor;         
    uint8_t     HwtestVersionBuild;         
    uint8_t     HV_Filler;

    uint8_t     BootloaderVersionMajor;
    uint8_t     BootloaderVersionMinor;
    uint8_t     BootloaderVersionBuild;
    uint8_t     BV_Filler;

    uint8_t     HardwareRevision;   
    uint16_t    HardwareVersion;    
    uint8_t     HW_Revision_Webasto; 
    uint16_t    HW_Version_Webasto;  
}struct_PCM_DATA_SYSTEM;
#pragma pack() 

extern struct_PCM_DATA_SYSTEM   ext_pcm_data_system;
extern const uint16_t           ext_NVM_ADDR___NVM_USR_CFG_DATA;

extern const uint32_t  PCM_NVM_FEE_DAT_BLK_2_START;
extern const uint32_t  PCM_NVM_FEE_DAT_BLK_2_END;
extern const uint32_t  PCM_NVM_FEE_DAT_BLK_3_START;
extern const uint32_t  PCM_NVM_FEE_DAT_BLK_3_END;
extern const uint32_t  PCM_NVM_FEE_DAT_BLK_4_START;
extern const uint32_t  PCM_NVM_FEE_DAT_BLK_4_END;
extern const uint32_t  PCM_NVM_FEE_DAT_BLK_5_START;
extern const uint32_t  PCM_NVM_FEE_DAT_BLK_5_END;
extern const uint32_t  PCM_NVM_FEE_DAT_BLK_6_START;
extern const uint32_t  PCM_NVM_FEE_DAT_BLK_6_END;
extern const uint32_t  PCM_NVM_FEE_DAT_BLK_7_START;
extern const uint32_t  PCM_NVM_FEE_DAT_BLK_7_END;
extern const uint32_t  PCM_NVM_FEE_DAT_BLK_8_START;
extern const uint32_t  PCM_NVM_FEE_DAT_BLK_8_END;

#ifdef PCM_NVM_BLOCK_3_SIZE_DATA_END_OF_LIST_CHECK_VALUE
extern const uint8_t PCM_NVM_FEE_DATA_END_OF_LIST_CHECK_VALUE[(PCM_NVM_BLOCK_3_SIZE_DATA_END_OF_LIST_CHECK_VALUE)];
#endif





#define PCM_NVM_ADDR_FEE_DAT_BLK_2_START_SPACER         ((FEE_USED_DATA_LENGTH_0) + (FEE_USED_DATA_LENGTH_1))
#define PCM_NVM_SIZE_FEE_DAT_BLK_2_SPACER               ((HAL_START_ADDR_ABSOLUT) - (((FEE_USED_DATA_LENGTH_0) + (FEE_USED_DATA_LENGTH_1)))


#define PCM_NVM_ADDR_FEE_DAT_BLK_2_START                (HAL_START_ADDR_ABSOLUT)
#define PCM_NVM_ADDR_DATA_SYSTEM                        ((PCM_NVM_ADDR_FEE_DAT_BLK_2_START)                     + sizeof(PCM_NVM_FEE_DAT_BLK_2_START))
#define PCM_NVM_ADDR_ECU_EXT_SYSTEM_DATA                ((PCM_NVM_ADDR_DATA_SYSTEM)                             + sizeof(ext_pcm_data_system))
#define PCM_NVM_ADDR_FEE_DAT_BLK_2_END                  ((PCM_NVM_ADDR_ECU_EXT_SYSTEM_DATA)                     + sizeof(ext_ecu_ext_sys_data_main))
#define PCM_NVM_ADDR_FEE_DAT_BLK_2_LAST_BYTE            ((PCM_NVM_ADDR_FEE_DAT_BLK_2_END)                       + sizeof(PCM_NVM_FEE_DAT_BLK_2_END) - 1u)


#define PCM_NVM_ADDR_FEE_DAT_BLK_3_START                ((FEE_USED_DATA_LENGTH_0) + (FEE_USED_DATA_LENGTH_1) + (FEE_USED_DATA_LENGTH_2))
#define PCM_NVM_ADDR_PWM_CAL_CHECKSUM                   ((PCM_NVM_ADDR_FEE_DAT_BLK_3_START)                     + sizeof(PCM_NVM_FEE_DAT_BLK_3_START))
#define PCM_NVM_ADDR_PWM_CAL_ACTIVE                     ((PCM_NVM_ADDR_PWM_CAL_CHECKSUM)                        + sizeof(ext_pwm_cal_values_checksum))
#define PCM_NVM_ADDR_PWM_OUT_LUT_V_IN_SP_CNT            ((PCM_NVM_ADDR_PWM_CAL_ACTIVE)                          + sizeof(ext_pwm_cal_values_active))
#define PCM_NVM_ADDR_PWM_IN_LUT_V_SP_CNT                ((PCM_NVM_ADDR_PWM_OUT_LUT_V_IN_SP_CNT)                 + sizeof(ext_pwm_out_lut_voltage_in_setpoint_count))
#define PCM_NVM_ADDR_PWM_OUT_LUT_V_SP_CNT               ((PCM_NVM_ADDR_PWM_IN_LUT_V_SP_CNT)                     + sizeof(ext_pwm_in_lut_peak_v_setpoint_count))
#define PCM_NVM_ADDR_PWM_OUT_LUT_FREQ_SP_CNT            ((PCM_NVM_ADDR_PWM_OUT_LUT_V_SP_CNT)                    + sizeof(ext_pwm_out_lut_vreg_setpoint_count))
#define PCM_NVM_ADDR_PWM_OUT_LUT_V_IN1_SP               ((PCM_NVM_ADDR_PWM_OUT_LUT_FREQ_SP_CNT)                 + sizeof(ext_pwm_out_lut_duty_freq_setpoint_count))
#define PCM_NVM_ADDR_PWM_OUT_LUT_V_IN1_VAL              ((PCM_NVM_ADDR_PWM_OUT_LUT_V_IN1_SP)                    + sizeof(ext_pwm_out_lut_voltage_in1_setpoint))
#define PCM_NVM_ADDR_PWM_OUT_LUT_V_IN2_SP               ((PCM_NVM_ADDR_PWM_OUT_LUT_V_IN1_VAL)                   + sizeof(ext_pwm_out_lut_voltage_in1_value))
#define PCM_NVM_ADDR_PWM_OUT_LUT_V_IN2_VAL              ((PCM_NVM_ADDR_PWM_OUT_LUT_V_IN2_SP)                    + sizeof(ext_pwm_out_lut_voltage_in2_setpoint))
#define PCM_NVM_ADDR_PWM_IN_LUT_V_SP                    ((PCM_NVM_ADDR_PWM_OUT_LUT_V_IN2_VAL)                   + sizeof(ext_pwm_out_lut_voltage_in2_value))
#define PCM_NVM_ADDR_PWM_IN_LUT_V_VAL                   ((PCM_NVM_ADDR_PWM_IN_LUT_V_SP)                         + sizeof(ext_pwm_in_lut_peak_v_setpoint))
#define PCM_NVM_ADDR_PWM_OUT_LUT_V_SP                   ((PCM_NVM_ADDR_PWM_IN_LUT_V_VAL)                        + sizeof(ext_pwm_in_lut_peak_v_value))
#define PCM_NVM_ADDR_PWM_OUT_LUT_V_RANGE0               ((PCM_NVM_ADDR_PWM_OUT_LUT_V_SP)                        + sizeof(ext_pwm_out_lut_vreg_setpoint))
#define PCM_NVM_ADDR_PWM_OUT_LUT_V_RANGE1               ((PCM_NVM_ADDR_PWM_OUT_LUT_V_RANGE0)                    + sizeof(ext_pwm_out_lut_vreg_range0))
#define PCM_NVM_ADDR_PWM_OUT_LUT_DUTY_VAL               ((PCM_NVM_ADDR_PWM_OUT_LUT_V_RANGE1)                    + sizeof(ext_pwm_out_lut_vreg_range1))
#define PCM_NVM_ADDR_PWM_OUT_LUT_FREQ_SP                ((PCM_NVM_ADDR_PWM_OUT_LUT_DUTY_VAL)                    + sizeof(ext_pwm_out_lut_duty_offset_value))
#define PCM_NVM_ADDR_FEE_DAT_BLK_3_END                  ((PCM_NVM_ADDR_PWM_OUT_LUT_FREQ_SP)                     + sizeof(ext_pwm_out_lut_duty_freq_setpoint) \
                                                                                                                + sizeof(PCM_NVM_FEE_DATA_END_OF_LIST_CHECK_VALUE))
#define PCM_NVM_ADDR_FEE_DAT_BLK_3_LAST_BYTE            ((PCM_NVM_ADDR_FEE_DAT_BLK_3_END)                       + sizeof(PCM_NVM_FEE_DAT_BLK_3_END) - 1u)


#define PCM_NVM_ADDR_FEE_DAT_BLK_4_START                ((FEE_USED_DATA_LENGTH_0)+(FEE_USED_DATA_LENGTH_1)      + (FEE_USED_DATA_LENGTH_2) \
                                                          + (FEE_USED_DATA_LENGTH_3))
#define PCM_NVM_ADDR_FEE_BLE_PAIRING_KEY                ((PCM_NVM_ADDR_FEE_DAT_BLK_4_START)                     + sizeof(PCM_NVM_ADDR_FEE_DAT_BLK_4_START))
#define PCM_NVM_ADDR_FEE_PMIC_INITIAL_GSR               ((PCM_NVM_ADDR_FEE_BLE_PAIRING_KEY)                     + sizeof(ext_cust_ble_pairing_key))
#define PCM_NVM_ADDR_FEE_WEBASTO_APPL_NUMBER_CAN        ((PCM_NVM_ADDR_FEE_PMIC_INITIAL_GSR)                    + sizeof(ext_l99pm72_initial_statusbyte))
#define PCM_NVM_ADDR_FEE_KEY_WEBASTO_APPL_NUMBER_CAN    ((PCM_NVM_ADDR_FEE_WEBASTO_APPL_NUMBER_CAN)             + sizeof(IUG_SYSTEM_STATE.ary_webasto_appl_number_can))
#define PCM_NVM_ADDR_FEE_FLASH_FILE_NAME_CAN            ((PCM_NVM_ADDR_FEE_KEY_WEBASTO_APPL_NUMBER_CAN)         + sizeof(IUG_SYSTEM_STATE.key_webasto_appl_number_can))
#define PCM_NVM_ADDR_FEE_KEY_FLASH_FILE_NAME_CAN        ((PCM_NVM_ADDR_FEE_FLASH_FILE_NAME_CAN)                 + sizeof(IUG_SYSTEM_STATE.ary_flash_file_name_can))
#define PCM_NVM_ADDR_FEE_DAT_BLK_4_END                  ((PCM_NVM_ADDR_FEE_KEY_FLASH_FILE_NAME_CAN)             + sizeof(IUG_SYSTEM_STATE.key_flash_file_name_can))
#define PCM_NVM_ADDR_FEE_DAT_BLK_4_LAST_BYTE            ((PCM_NVM_ADDR_FEE_DAT_BLK_4_END)                       + sizeof(PCM_NVM_FEE_DAT_BLK_4_END) - 1u)


#define PCM_NVM_ADDR_FEE_DAT_BLK_5_START                ((FEE_USED_DATA_LENGTH_0) + (FEE_USED_DATA_LENGTH_1)    + (FEE_USED_DATA_LENGTH_2) \
                                                          + (FEE_USED_DATA_LENGTH_3) + (FEE_USED_DATA_LENGTH_4))
#ifdef PCM_NVM_ADDR_FEE_DAT_BLK_5_START
    #define PCM_NVM_ADDR_WEBASTO_PARAMETER               ((PCM_NVM_ADDR_FEE_DAT_BLK_5_START)                    + sizeof(PCM_NVM_FEE_DAT_BLK_5_START))
#else
    #error "Not without a 'block start marker'!"
    #define PCM_NVM_ADDR_WEBASTO_PARAMETER              ((FEE_USED_DATA_LENGTH_0) + (FEE_USED_DATA_LENGTH_1)    + (FEE_USED_DATA_LENGTH_2) \
                                                          + (FEE_USED_DATA_LENGTH_3) + (FEE_USED_DATA_LENGTH_4))
#endif
#define PCM_NVM_ADDR_FEE_DAT_BLK_5_END                  ((PCM_NVM_ADDR_WEBASTO_PARAMETER)                       + (PCM_PARID_WEBASTO_PARAMETER_POOL_SIZE))
#define PCM_NVM_ADDR_FEE_DAT_BLK_5_LAST_BYTE            ((PCM_NVM_ADDR_FEE_DAT_BLK_5_END)                       + sizeof(PCM_NVM_FEE_DAT_BLK_5_END) - 1u)


#define PCM_NVM_ADDR_FEE_DAT_BLK_6_START                ((FEE_USED_DATA_LENGTH_0) + (FEE_USED_DATA_LENGTH_1)    + (FEE_USED_DATA_LENGTH_2) \
                                                          + (FEE_USED_DATA_LENGTH_3) + (FEE_USED_DATA_LENGTH_4) + (FEE_USED_DATA_LENGTH_5))
#define PCM_NVM_ADDR_APPL_PARAM_IUG_CFG_START           ((PCM_NVM_ADDR_FEE_DAT_BLK_6_START)                     + sizeof(PCM_NVM_FEE_DAT_BLK_6_START))
#define PCM_NVM_ADDR_APPL_PARAM_TASK_STAT_DATA          ((PCM_NVM_ADDR_APPL_PARAM_IUG_CFG_START)                + (PCM_NVM_IUG_CFG_RESERVED))
#define PCM_NVM_ADDR_APPL_PARAM_WBUS_MEMBER             ((PCM_NVM_ADDR_APPL_PARAM_TASK_STAT_DATA)               + sizeof(ext_task_statistic_data))
#define PCM_NVM_ADDR_APPL_PARAM_HEATER_PROPERTY         ((PCM_NVM_ADDR_APPL_PARAM_WBUS_MEMBER)                  + sizeof(WBUS_MEMBER_NVM))
#define PCM_NVM_ADDR_APPL_PARAM_UNIT_PROPERTY           ((PCM_NVM_ADDR_APPL_PARAM_HEATER_PROPERTY)              + sizeof(IUG_HEATER_PROPERTY_NVM))
#define PCM_NVM_ADDR_APPL_PARAM_COM_MTX_CFG             ((PCM_NVM_ADDR_APPL_PARAM_UNIT_PROPERTY)                + sizeof(IUG_UNIT_PROPERTY_NVM))
#define PCM_NVM_ADDR_APPL_PARAM_IBN_PROCESS             ((PCM_NVM_ADDR_APPL_PARAM_COM_MTX_CFG)                  + sizeof(IUG_UNIT_COMMTX_NVM_DEF))
#define PCM_NVM_ADDR_APPL_PARAM_TGT1_COMP_PROPERTY      ((PCM_NVM_ADDR_APPL_PARAM_IBN_PROCESS)                  + sizeof(ext_init_op_nvm_data))
#define PCM_NVM_ADDR_APPL_PARAM_TGT2_COMP_PROPERTY      ((PCM_NVM_ADDR_APPL_PARAM_TGT1_COMP_PROPERTY)           + sizeof(IUG_UNT_HEATER_1_COMP_PROP_NVM))
#define PCM_NVM_ADDR_APPL_PARAM_TGT3_COMP_PROPERTY      ((PCM_NVM_ADDR_APPL_PARAM_TGT2_COMP_PROPERTY)           + sizeof(IUG_UNT_HEATER_2_COMP_PROP_NVM))
#define PCM_NVM_ADDR_APPL_PARAM_TGT4_COMP_PROPERTY      ((PCM_NVM_ADDR_APPL_PARAM_TGT3_COMP_PROPERTY)           + sizeof(IUG_UNT_HEATER_3_COMP_PROP_NVM))
#define PCM_NVM_ADDR_APPL_PARAM_TGT5_COMP_PROPERTY      ((PCM_NVM_ADDR_APPL_PARAM_TGT4_COMP_PROPERTY)           + sizeof(IUG_UNT_HEATER_4_COMP_PROP_NVM))
#define PCM_NVM_ADDR_APPL_PARAM_TGT6_COMP_PROPERTY      ((PCM_NVM_ADDR_APPL_PARAM_TGT5_COMP_PROPERTY)           + sizeof(IUG_UNT_HEATER_5_COMP_PROP_NVM))
#define PCM_NVM_ADDR_APPL_PARAM_TGT7_COMP_PROPERTY      ((PCM_NVM_ADDR_APPL_PARAM_TGT6_COMP_PROPERTY)           + sizeof(IUG_UNT_HEATER_6_COMP_PROP_NVM))
#define PCM_NVM_ADDR_APPL_PARAM_TGT8_COMP_PROPERTY      ((PCM_NVM_ADDR_APPL_PARAM_TGT7_COMP_PROPERTY)           + sizeof(IUG_UNT_HEATER_7_COMP_PROP_NVM))
#define PCM_NVM_ADDR_FEE_DAT_BLK_6_END                  ((PCM_NVM_ADDR_APPL_PARAM_TGT8_COMP_PROPERTY)           + sizeof(IUG_UNT_HEATER_8_COMP_PROP_NVM))
#define PCM_NVM_ADDR_FEE_DAT_BLK_6_LAST_BYTE            ((PCM_NVM_ADDR_FEE_DAT_BLK_6_END)                       + sizeof(PCM_NVM_FEE_DAT_BLK_6_END) - 1u)


#define PCM_NVM_ADDR_FEE_DAT_BLK_7_START                ((FEE_USED_DATA_LENGTH_0) + (FEE_USED_DATA_LENGTH_1)    + (FEE_USED_DATA_LENGTH_2) \
                                                       + (FEE_USED_DATA_LENGTH_3) + (FEE_USED_DATA_LENGTH_4)    + (FEE_USED_DATA_LENGTH_5) \
                                                       + (FEE_USED_DATA_LENGTH_6))
#define PCM_NVM_ADDR_NVM_DIAGNOSIS_STORAGE              ((PCM_NVM_ADDR_FEE_DAT_BLK_7_START)                     + sizeof(PCM_NVM_FEE_DAT_BLK_7_START))

#define PCM_NVM_ADDR_DTC_TABLE                          ((PCM_NVM_ADDR_NVM_DIAGNOSIS_STORAGE)                   + sizeof(mgl_IUG_NVM_DIAG_STORAGE))
#define PCM_NVM_ADDR_DTC_SYMPTOM                        ((PCM_NVM_ADDR_DTC_TABLE)                               + sizeof(mgl_dtc_table))
#define PCM_NVM_ADDR_DTC_FREEZE_FRAME                   ((PCM_NVM_ADDR_DTC_SYMPTOM)                             + sizeof(mgl_DTC_SYMPTOM))
#define PCM_NVM_ADDR_FEE_DAT_BLK_7_END                  ((PCM_NVM_ADDR_DTC_FREEZE_FRAME)                        + sizeof(mgl_DTC_FREEZE_FRAME))
#define PCM_NVM_ADDR_FEE_DAT_BLK_7_LAST_BYTE            ((PCM_NVM_ADDR_FEE_DAT_BLK_7_END)                       + sizeof(PCM_NVM_FEE_DAT_BLK_7_END) - 1u)


#define PCM_NVM_ADDR_FEE_DAT_BLK_8_START                ((FEE_USED_DATA_LENGTH_0) + (FEE_USED_DATA_LENGTH_1)    + (FEE_USED_DATA_LENGTH_2) \
                                                          + (FEE_USED_DATA_LENGTH_3) + (FEE_USED_DATA_LENGTH_4) + (FEE_USED_DATA_LENGTH_5) \
                                                          + (FEE_USED_DATA_LENGTH_6) + (FEE_USED_DATA_LENGTH_7))
#define PCM_NVM_ADDR_NVM_FREQUENT_DATA                  ((PCM_NVM_ADDR_FEE_DAT_BLK_8_START)                     + sizeof(PCM_NVM_FEE_DAT_BLK_8_START))
#define PCM_NVM_ADDR_APPL_PARAM_BLE_TIMER_CTL           ((PCM_NVM_ADDR_NVM_FREQUENT_DATA)                       + sizeof(IUG_NVM_FREQUENT_DATA))
#define PCM_NVM_ADDR_FEE_DAT_BLK_8_END                  ((PCM_NVM_ADDR_APPL_PARAM_BLE_TIMER_CTL)                + sizeof(IUG_UNIT_BLUETOOTH_TIMER_CTL))
#define PCM_NVM_ADDR_FEE_DAT_BLK_8_LAST_BYTE            ((PCM_NVM_ADDR_FEE_DAT_BLK_8_END)                       + sizeof(PCM_NVM_FEE_DAT_BLK_8_END) - 1u)


_Static_assert((DTC_NVM_DTC_TABLE_ADDR)==(PCM_NVM_ADDR_DTC_TABLE),"Analyze: 'DTC_NVM_DTC_TABLE_ADDR' and 'PCM_NVM_ADDR_DTC_TABLE'");

_Static_assert((PCM_NVM_ADDR_FEE_PMIC_INITIAL_GSR)==(PCM_NVM_ADDR_INITITAL_PMIC_GSR), "Analyze this conflict!!!");




#endif  


