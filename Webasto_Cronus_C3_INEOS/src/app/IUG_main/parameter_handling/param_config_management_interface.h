#ifndef _PCM_PARAM_CONFIG_MANAGEMENT_IF_H_
#define _PCM_PARAM_CONFIG_MANAGEMENT_IF_H_







enum_PCM_RETURN_VALUE pcm_data_handler_asynch_service( const enum_PCM_COPY_DATA_SERVICE_t data_dir, const enum_PCM_DATA_TYPE data_type,
                                                uint32_t  const nvm_blk_addr, uint8_t * const data_buffer,
                                                uint8_t * const p_rd_wr_any_data,
                                                uint8_t * const p_idx_last_pcm_blk_processed );


enum_PCM_RETURN_VALUE pcm_init( uint8_t * const err_flag );


enum_PCM_RETURN_VALUE pcm_data_handler( const struct_PCM_HANDLER * const ptrPcmHandler );


enum_PCM_RETURN_VALUE pcm_data_handler_asynch( const struct_PCM_HANDLER * const ptrPcmHandler, const uint16_t id_pcm_blk );


enum_PCM_RETURN_VALUE pcm_data_handler_asynch_process_synchronous( const struct_PCM_HANDLER * const ptrPcmHandler, const uint16_t id_pcm_blk );


static enum_HAL_NVM_RETURN_VALUE pcm_set_dev_studio_sw_module_name(void);

static boolean pcm_check_sw_version_changed( void );

enum_PCM_RETURN_VALUE pcm_rd_wr_system_data_nvm( enum_PCM_DATA_DIRECTION data_direction );

static void pcm_init_ecu_ext_sys_data(void);


uint8_t         pcm_usr_cfg_dat_cyclic_check( void );
ERR_USRCFGDAT_t pcm_usr_cfg_dat_get_para_by_id( const ID_USRCFGDAT_t param_id, uint32_t * const para_val );
ERR_USRCFGDAT_t pcm_usr_cfg_dat_get_para_can_cfg( const ID_USRCFGDAT_t param_id, usr_cfg_dat_can_cfg_t * const para_can_cfg );
uint8_t         pcm_usr_cfg_dat_init( void );


#ifdef PCM_DATA_HANDLER_DEBUG_SUPPORT_EN
void pcm_data_handler_debug_support( void );
void pcm_data_handler_debug_support_init( void );
#endif


#endif  


