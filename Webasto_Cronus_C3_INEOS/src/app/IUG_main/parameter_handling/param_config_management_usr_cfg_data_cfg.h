#ifndef _PCM_PARAM_CONFIG_MANAGEMENT__USR_CFG_DATA_CFG_H_
#define _PCM_PARAM_CONFIG_MANAGEMENT__USR_CFG_DATA_CFG_H_

#include "tp_il_config.h"

#define ID_USRCFGDAT_VERSION_DEF                    0xDEFFu     
#define ID_USRCFGDAT_GEN_OPT_1_DEF                  0x0000u
#define ID_USRCFGDAT_GEN_OPT_2_DEF                  0x0000u
#define ID_USRCFGDAT_GEN_OPT_3_DEF                  0x0000u
#define ID_USRCFGDAT_GEN_OPT_4_DEF                  0x0000u
#define ID_USRCFGDAT_GEN_OPT_5_DEF                  0x0000u

#define ID_USRCFGDAT_CAN_APPL_RESERVED_1_BUS_ID_DEF     0x00u
#define ID_USRCFGDAT_CAN_APPL_RESERVED_1_STD_EXT_DEF    0x00u
#define ID_USRCFGDAT_CAN_APPL_RESERVED_1_RX_ID_DEF      0x00000000u
#define ID_USRCFGDAT_CAN_APPL_RESERVED_1_TX_ID_DEF      0x00000000u

#define ID_USRCFGDAT_CAN_APPL_RESERVED_2_BUS_ID_DEF     0x00u
#define ID_USRCFGDAT_CAN_APPL_RESERVED_2_STD_EXT_DEF    0x00u
#define ID_USRCFGDAT_CAN_APPL_RESERVED_2_RX_ID_DEF      0x00000000u
#define ID_USRCFGDAT_CAN_APPL_RESERVED_2_TX_ID_DEF      0x00000000u

#define ID_USRCFGDAT_CAN_APPL_RESERVED_3_BUS_ID_DEF     0x00u
#define ID_USRCFGDAT_CAN_APPL_RESERVED_3_STD_EXT_DEF    0x00u
#define ID_USRCFGDAT_CAN_APPL_RESERVED_3_RX_ID_DEF      0x00000000u
#define ID_USRCFGDAT_CAN_APPL_RESERVED_3_TX_ID_DEF      0x00000000u

#define ID_USRCFGDAT_CAN_APPL_RESERVED_4_BUS_ID_DEF     0x00u
#define ID_USRCFGDAT_CAN_APPL_RESERVED_4_STD_EXT_DEF    0x00u
#define ID_USRCFGDAT_CAN_APPL_RESERVED_4_RX_ID_DEF      0x00000000u
#define ID_USRCFGDAT_CAN_APPL_RESERVED_4_TX_ID_DEF      0x00000000u

#define ID_USRCFGDAT_CAN_STAT_EXT_MUX_BUS_ID_DEF        0x03u
#define ID_USRCFGDAT_CAN_STAT_EXT_MUX_STD_EXT_DEF       0x00u
#define ID_USRCFGDAT_CAN_STAT_EXT_MUX_RX_ID_DEF         0x00000000u
#define ID_USRCFGDAT_CAN_STAT_EXT_MUX_TX_ID_DEF         0x0000060Eu

#define ID_USRCFGDAT_CAN_STAT_MUX_BUS_ID_DEF            0x03u
#define ID_USRCFGDAT_CAN_STAT_MUX_STD_EXT_DEF           0x00u
#define ID_USRCFGDAT_CAN_STAT_MUX_RX_ID_DEF             0x00000000u
#define ID_USRCFGDAT_CAN_STAT_MUX_TX_ID_DEF             0x0000060Fu

#define ID_USRCFGDAT_CAN_APPL_PAR_1_BUS_ID_DEF          0x03u
#define ID_USRCFGDAT_CAN_APPL_PAR_1_STD_EXT_DEF         0x00u
#define ID_USRCFGDAT_CAN_APPL_PAR_1_RX_ID_DEF           0x0000060Bu
#define ID_USRCFGDAT_CAN_APPL_PAR_1_TX_ID_DEF           0x00000000u

#define ID_USRCFGDAT_CAN_APPL_PAR_2_BUS_ID_DEF          0x03u
#define ID_USRCFGDAT_CAN_APPL_PAR_2_STD_EXT_DEF         0x00u
#define ID_USRCFGDAT_CAN_APPL_PAR_2_RX_ID_DEF           0x0000060Cu
#define ID_USRCFGDAT_CAN_APPL_PAR_2_TX_ID_DEF           0x00000000u

#define ID_USRCFGDAT_CAN_APPL_PAR_3_BUS_ID_DEF          0x03u
#define ID_USRCFGDAT_CAN_APPL_PAR_3_STD_EXT_DEF         0x00u
#define ID_USRCFGDAT_CAN_APPL_PAR_3_RX_ID_DEF           0x00000000u
#define ID_USRCFGDAT_CAN_APPL_PAR_3_TX_ID_DEF           0x0000060Bu

#define ID_USRCFGDAT_CAN_ENTER_EOL_BUS_ID_DEF           0x03u
#define ID_USRCFGDAT_CAN_ENTER_EOL_STD_EXT_DEF          0x01u
#define ID_USRCFGDAT_CAN_ENTER_EOL_RX_ID_DEF            0x00000815u
#define ID_USRCFGDAT_CAN_ENTER_EOL_TX_ID_DEF            0x00000000u

#define ID_USRCFGDAT_CAN_RESERVED_11_BUS_ID_DEF          0x00u
#define ID_USRCFGDAT_CAN_RESERVED_11_STD_EXT_DEF         0x00u
#define ID_USRCFGDAT_CAN_RESERVED_11_RX_ID_DEF           0x00000000u
#define ID_USRCFGDAT_CAN_RESERVED_11_TX_ID_DEF           0x00000000u

#define ID_USRCFGDAT_CAN_RESERVED_12_BUS_ID_DEF          0x00u
#define ID_USRCFGDAT_CAN_RESERVED_12_STD_EXT_DEF         0x00u
#define ID_USRCFGDAT_CAN_RESERVED_12_RX_ID_DEF           0x00000000u
#define ID_USRCFGDAT_CAN_RESERVED_12_TX_ID_DEF           0x00000000u

#define ID_USRCFGDAT_CAN_RESERVED_13_BUS_ID_DEF          0x00u
#define ID_USRCFGDAT_CAN_RESERVED_13_STD_EXT_DEF         0x00u
#define ID_USRCFGDAT_CAN_RESERVED_13_RX_ID_DEF           0x00000000u
#define ID_USRCFGDAT_CAN_RESERVED_13_TX_ID_DEF           0x00000000u

#define ID_USRCFGDAT_CAN_RESERVED_14_BUS_ID_DEF          0x00u
#define ID_USRCFGDAT_CAN_RESERVED_14_STD_EXT_DEF         0x00u
#define ID_USRCFGDAT_CAN_RESERVED_14_RX_ID_DEF           0x00000000u
#define ID_USRCFGDAT_CAN_RESERVED_14_TX_ID_DEF           0x00000000u

#define ID_USRCFGDAT_CAN_RESERVED_15_BUS_ID_DEF          0x00u
#define ID_USRCFGDAT_CAN_RESERVED_15_STD_EXT_DEF         0x00u
#define ID_USRCFGDAT_CAN_RESERVED_15_RX_ID_DEF           0x00000000u
#define ID_USRCFGDAT_CAN_RESERVED_15_TX_ID_DEF           0x00000000u

#define ID_USRCFGDAT_CAN_TP_CHL_0_BUS_ID_DEF            0x03u
#define ID_USRCFGDAT_CAN_TP_CHL_0_STD_EXT_DEF           0x00u
#define ID_USRCFGDAT_CAN_TP_CHL_0_RX_ID_DEF             (TP_IL_RX_IDENTIFIER)   
#define ID_USRCFGDAT_CAN_TP_CHL_0_TX_ID_DEF             (TP_IL_TX_IDENTIFIER)   

#define ID_USRCFGDAT_CAN_TP_CHL_1_BUS_ID_DEF            0x03u
#define ID_USRCFGDAT_CAN_TP_CHL_1_STD_EXT_DEF           0x00u
#define ID_USRCFGDAT_CAN_TP_CHL_1_RX_ID_DEF             0x00000000u
#define ID_USRCFGDAT_CAN_TP_CHL_1_TX_ID_DEF             0x00000000u




#define PCM_USR_CFG_DAT_CFG \
{\
    \
    {(ID_USRCFGDAT_VERSION)                     ,       0u      ,sizeof(uint16_t)   ,(ID_USRCFGDAT_VERSION_DEF)                 },\
    {(ID_USRCFGDAT_GEN_OPT_1)                   ,       2u      ,sizeof(uint16_t)   ,(ID_USRCFGDAT_GEN_OPT_1_DEF)               },\
    {(ID_USRCFGDAT_GEN_OPT_2)                   ,       4u      ,sizeof(uint16_t)   ,(ID_USRCFGDAT_GEN_OPT_2_DEF)               },\
    {(ID_USRCFGDAT_GEN_OPT_3)                   ,       6u      ,sizeof(uint16_t)   ,(ID_USRCFGDAT_GEN_OPT_3_DEF)               },\
    {(ID_USRCFGDAT_GEN_OPT_4)                   ,       8u      ,sizeof(uint16_t)   ,(ID_USRCFGDAT_GEN_OPT_4_DEF)               },\
    {(ID_USRCFGDAT_GEN_OPT_5)                   ,      10u      ,sizeof(uint16_t)   ,(ID_USRCFGDAT_GEN_OPT_5_DEF)               },\
    \
    {(ID_USRCFGDAT_CAN_APPL_RESERVED_1_BUS_ID)      ,      32u      ,sizeof(uint8_t)    ,(ID_USRCFGDAT_CAN_APPL_RESERVED_1_BUS_ID_DEF)  },\
    {(ID_USRCFGDAT_CAN_APPL_RESERVED_1_STD_EXT)     ,      33u      ,sizeof(uint8_t)    ,(ID_USRCFGDAT_CAN_APPL_RESERVED_1_STD_EXT_DEF) },\
    {(ID_USRCFGDAT_CAN_APPL_RESERVED_1_RX_ID)       ,      34u      ,sizeof(uint32_t)   ,(ID_USRCFGDAT_CAN_APPL_RESERVED_1_RX_ID_DEF)   },\
    {(ID_USRCFGDAT_CAN_APPL_RESERVED_1_TX_ID)       ,      38u      ,sizeof(uint32_t)   ,(ID_USRCFGDAT_CAN_APPL_RESERVED_1_TX_ID_DEF)   },\
    {(ID_USRCFGDAT_CAN_APPL_RESERVED_2_BUS_ID)      ,      42u      ,sizeof(uint8_t)    ,(ID_USRCFGDAT_CAN_APPL_RESERVED_2_BUS_ID_DEF)  },\
    {(ID_USRCFGDAT_CAN_APPL_RESERVED_2_STD_EXT)     ,      43u      ,sizeof(uint8_t)    ,(ID_USRCFGDAT_CAN_APPL_RESERVED_2_STD_EXT_DEF) },\
    {(ID_USRCFGDAT_CAN_APPL_RESERVED_2_RX_ID)       ,      44u      ,sizeof(uint32_t)   ,(ID_USRCFGDAT_CAN_APPL_RESERVED_2_RX_ID_DEF)   },\
    {(ID_USRCFGDAT_CAN_APPL_RESERVED_2_TX_ID)       ,      48u      ,sizeof(uint32_t)   ,(ID_USRCFGDAT_CAN_APPL_RESERVED_2_TX_ID_DEF)   },\
    {(ID_USRCFGDAT_CAN_APPL_RESERVED_3_BUS_ID)      ,      52u      ,sizeof(uint8_t)    ,(ID_USRCFGDAT_CAN_APPL_RESERVED_3_BUS_ID_DEF)  },\
    {(ID_USRCFGDAT_CAN_APPL_RESERVED_3_STD_EXT)     ,      53u      ,sizeof(uint8_t)    ,(ID_USRCFGDAT_CAN_APPL_RESERVED_3_STD_EXT_DEF) },\
    {(ID_USRCFGDAT_CAN_APPL_RESERVED_3_RX_ID)       ,      54u      ,sizeof(uint32_t)   ,(ID_USRCFGDAT_CAN_APPL_RESERVED_3_RX_ID_DEF)   },\
    {(ID_USRCFGDAT_CAN_APPL_RESERVED_3_TX_ID)       ,      58u      ,sizeof(uint32_t)   ,(ID_USRCFGDAT_CAN_APPL_RESERVED_3_TX_ID_DEF)   },\
    {(ID_USRCFGDAT_CAN_APPL_RESERVED_4_BUS_ID)      ,      62u      ,sizeof(uint8_t)    ,(ID_USRCFGDAT_CAN_APPL_RESERVED_4_BUS_ID_DEF)  },\
    {(ID_USRCFGDAT_CAN_APPL_RESERVED_4_STD_EXT)     ,      63u      ,sizeof(uint8_t)    ,(ID_USRCFGDAT_CAN_APPL_RESERVED_4_STD_EXT_DEF) },\
    {(ID_USRCFGDAT_CAN_APPL_RESERVED_4_RX_ID)       ,      64u      ,sizeof(uint32_t)   ,(ID_USRCFGDAT_CAN_APPL_RESERVED_4_RX_ID_DEF)   },\
    {(ID_USRCFGDAT_CAN_APPL_RESERVED_4_TX_ID)       ,      68u      ,sizeof(uint32_t)   ,(ID_USRCFGDAT_CAN_APPL_RESERVED_4_TX_ID_DEF)   },\
    \
    {(ID_USRCFGDAT_CAN_STAT_EXT_MUX_BUS_ID)     ,      72u      ,sizeof(uint8_t)    ,(ID_USRCFGDAT_CAN_STAT_EXT_MUX_BUS_ID_DEF) },\
    {(ID_USRCFGDAT_CAN_STAT_EXT_MUX_STD_EXT)    ,      73u      ,sizeof(uint8_t)    ,(ID_USRCFGDAT_CAN_STAT_EXT_MUX_STD_EXT_DEF)},\
    {(ID_USRCFGDAT_CAN_STAT_EXT_MUX_RX_ID)      ,      74u      ,sizeof(uint32_t)   ,(ID_USRCFGDAT_CAN_STAT_EXT_MUX_RX_ID_DEF)  },\
    {(ID_USRCFGDAT_CAN_STAT_EXT_MUX_TX_ID)      ,      78u      ,sizeof(uint32_t)   ,(ID_USRCFGDAT_CAN_STAT_EXT_MUX_TX_ID_DEF)  },\
    \
    {(ID_USRCFGDAT_CAN_STAT_MUX_BUS_ID)         ,      82u      ,sizeof(uint8_t)    ,(ID_USRCFGDAT_CAN_STAT_MUX_BUS_ID_DEF)     },\
    {(ID_USRCFGDAT_CAN_STAT_MUX_STD_EXT)        ,      83u      ,sizeof(uint8_t)    ,(ID_USRCFGDAT_CAN_STAT_MUX_STD_EXT_DEF)    },\
    {(ID_USRCFGDAT_CAN_STAT_MUX_RX_ID)          ,      84u      ,sizeof(uint32_t)   ,(ID_USRCFGDAT_CAN_STAT_MUX_RX_ID_DEF)      },\
    {(ID_USRCFGDAT_CAN_STAT_MUX_TX_ID)          ,      88u      ,sizeof(uint32_t)   ,(ID_USRCFGDAT_CAN_STAT_MUX_TX_ID_DEF)      },\
    \
    {(ID_USRCFGDAT_CAN_APPL_PAR_1_BUS_ID)       ,      92u      ,sizeof(uint8_t)    ,(ID_USRCFGDAT_CAN_APPL_PAR_1_BUS_ID_DEF)   },\
    {(ID_USRCFGDAT_CAN_APPL_PAR_1_STD_EXT)      ,      93u      ,sizeof(uint8_t)    ,(ID_USRCFGDAT_CAN_APPL_PAR_1_STD_EXT_DEF)  },\
    {(ID_USRCFGDAT_CAN_APPL_PAR_1_RX_ID)        ,      94u      ,sizeof(uint32_t)   ,(ID_USRCFGDAT_CAN_APPL_PAR_1_RX_ID_DEF)    },\
    {(ID_USRCFGDAT_CAN_APPL_PAR_1_TX_ID)        ,      98u      ,sizeof(uint32_t)   ,(ID_USRCFGDAT_CAN_APPL_PAR_1_TX_ID_DEF)    },\
    {(ID_USRCFGDAT_CAN_APPL_PAR_2_BUS_ID)       ,     102u      ,sizeof(uint8_t)    ,(ID_USRCFGDAT_CAN_APPL_PAR_2_BUS_ID_DEF)   },\
    {(ID_USRCFGDAT_CAN_APPL_PAR_2_STD_EXT)      ,     103u      ,sizeof(uint8_t)    ,(ID_USRCFGDAT_CAN_APPL_PAR_2_STD_EXT_DEF)  },\
    {(ID_USRCFGDAT_CAN_APPL_PAR_2_RX_ID)        ,     104u      ,sizeof(uint32_t)   ,(ID_USRCFGDAT_CAN_APPL_PAR_2_RX_ID_DEF)    },\
    {(ID_USRCFGDAT_CAN_APPL_PAR_2_TX_ID)        ,     108u      ,sizeof(uint32_t)   ,(ID_USRCFGDAT_CAN_APPL_PAR_2_TX_ID_DEF)    },\
    {(ID_USRCFGDAT_CAN_APPL_PAR_3_BUS_ID)       ,     112u      ,sizeof(uint8_t)    ,(ID_USRCFGDAT_CAN_APPL_PAR_3_BUS_ID_DEF)   },\
    {(ID_USRCFGDAT_CAN_APPL_PAR_3_STD_EXT)      ,     113u      ,sizeof(uint8_t)    ,(ID_USRCFGDAT_CAN_APPL_PAR_3_STD_EXT_DEF)  },\
    {(ID_USRCFGDAT_CAN_APPL_PAR_3_RX_ID)        ,     114u      ,sizeof(uint32_t)   ,(ID_USRCFGDAT_CAN_APPL_PAR_3_RX_ID_DEF)    },\
    {(ID_USRCFGDAT_CAN_APPL_PAR_3_TX_ID)        ,     118u      ,sizeof(uint32_t)   ,(ID_USRCFGDAT_CAN_APPL_PAR_3_TX_ID_DEF)    },\
    \
    {(ID_USRCFGDAT_CAN_ENTER_EOL_BUS_ID)        ,     122u      ,sizeof(uint8_t)    ,(ID_USRCFGDAT_CAN_ENTER_EOL_BUS_ID_DEF)    },\
    {(ID_USRCFGDAT_CAN_ENTER_EOL_STD_EXT)       ,     123u      ,sizeof(uint8_t)    ,(ID_USRCFGDAT_CAN_ENTER_EOL_STD_EXT_DEF)   },\
    {(ID_USRCFGDAT_CAN_ENTER_EOL_RX_ID)         ,     124u      ,sizeof(uint32_t)   ,(ID_USRCFGDAT_CAN_ENTER_EOL_RX_ID_DEF)     },\
    {(ID_USRCFGDAT_CAN_ENTER_EOL_TX_ID)         ,     128u      ,sizeof(uint32_t)   ,(ID_USRCFGDAT_CAN_ENTER_EOL_TX_ID_DEF)     },\
    \
    {(ID_USRCFGDAT_CAN_RESERVED_11_BUS_ID)       ,     132u      ,sizeof(uint8_t)    ,(ID_USRCFGDAT_CAN_RESERVED_11_BUS_ID_DEF)   },\
    {(ID_USRCFGDAT_CAN_RESERVED_11_STD_EXT)      ,     133u      ,sizeof(uint8_t)    ,(ID_USRCFGDAT_CAN_RESERVED_11_STD_EXT_DEF)  },\
    {(ID_USRCFGDAT_CAN_RESERVED_11_RX_ID)        ,     134u      ,sizeof(uint32_t)   ,(ID_USRCFGDAT_CAN_RESERVED_11_RX_ID_DEF)    },\
    {(ID_USRCFGDAT_CAN_RESERVED_11_TX_ID)        ,     138u      ,sizeof(uint32_t)   ,(ID_USRCFGDAT_CAN_RESERVED_11_TX_ID_DEF)    },\
    {(ID_USRCFGDAT_CAN_RESERVED_12_BUS_ID)       ,     142u      ,sizeof(uint8_t)    ,(ID_USRCFGDAT_CAN_RESERVED_12_BUS_ID_DEF)   },\
    {(ID_USRCFGDAT_CAN_RESERVED_12_STD_EXT)      ,     143u      ,sizeof(uint8_t)    ,(ID_USRCFGDAT_CAN_RESERVED_12_STD_EXT_DEF)  },\
    {(ID_USRCFGDAT_CAN_RESERVED_12_RX_ID)        ,     144u      ,sizeof(uint32_t)   ,(ID_USRCFGDAT_CAN_RESERVED_12_RX_ID_DEF)    },\
    {(ID_USRCFGDAT_CAN_RESERVED_12_TX_ID)        ,     148u      ,sizeof(uint32_t)   ,(ID_USRCFGDAT_CAN_RESERVED_12_TX_ID_DEF)    },\
    {(ID_USRCFGDAT_CAN_RESERVED_13_BUS_ID)       ,     152u      ,sizeof(uint8_t)    ,(ID_USRCFGDAT_CAN_RESERVED_13_BUS_ID_DEF)   },\
    {(ID_USRCFGDAT_CAN_RESERVED_13_STD_EXT)      ,     153u      ,sizeof(uint8_t)    ,(ID_USRCFGDAT_CAN_RESERVED_13_STD_EXT_DEF)  },\
    {(ID_USRCFGDAT_CAN_RESERVED_13_RX_ID)        ,     154u      ,sizeof(uint32_t)   ,(ID_USRCFGDAT_CAN_RESERVED_13_RX_ID_DEF)    },\
    {(ID_USRCFGDAT_CAN_RESERVED_13_TX_ID)        ,     158u      ,sizeof(uint32_t)   ,(ID_USRCFGDAT_CAN_RESERVED_13_TX_ID_DEF)    },\
    {(ID_USRCFGDAT_CAN_RESERVED_14_BUS_ID)       ,     162u      ,sizeof(uint8_t)    ,(ID_USRCFGDAT_CAN_RESERVED_14_BUS_ID_DEF)   },\
    {(ID_USRCFGDAT_CAN_RESERVED_14_STD_EXT)      ,     163u      ,sizeof(uint8_t)    ,(ID_USRCFGDAT_CAN_RESERVED_14_STD_EXT_DEF)  },\
    {(ID_USRCFGDAT_CAN_RESERVED_14_RX_ID)        ,     164u      ,sizeof(uint32_t)   ,(ID_USRCFGDAT_CAN_RESERVED_14_RX_ID_DEF)    },\
    {(ID_USRCFGDAT_CAN_RESERVED_14_TX_ID)        ,     168u      ,sizeof(uint32_t)   ,(ID_USRCFGDAT_CAN_RESERVED_14_TX_ID_DEF)    },\
    {(ID_USRCFGDAT_CAN_RESERVED_15_BUS_ID)       ,     172u      ,sizeof(uint8_t)    ,(ID_USRCFGDAT_CAN_RESERVED_15_BUS_ID_DEF)   },\
    {(ID_USRCFGDAT_CAN_RESERVED_15_STD_EXT)      ,     173u      ,sizeof(uint8_t)    ,(ID_USRCFGDAT_CAN_RESERVED_15_STD_EXT_DEF)  },\
    {(ID_USRCFGDAT_CAN_RESERVED_15_RX_ID)        ,     174u      ,sizeof(uint32_t)   ,(ID_USRCFGDAT_CAN_RESERVED_15_RX_ID_DEF)    },\
    {(ID_USRCFGDAT_CAN_RESERVED_15_TX_ID)        ,     178u      ,sizeof(uint32_t)   ,(ID_USRCFGDAT_CAN_RESERVED_15_TX_ID_DEF)    },\
    \
    {(ID_USRCFGDAT_CAN_TP_CHL_0_BUS_ID)         ,     182u      ,sizeof(uint8_t)    ,(ID_USRCFGDAT_CAN_TP_CHL_0_BUS_ID_DEF)     },\
    {(ID_USRCFGDAT_CAN_TP_CHL_0_STD_EXT)        ,     183u      ,sizeof(uint8_t)    ,(ID_USRCFGDAT_CAN_TP_CHL_0_STD_EXT_DEF)    },\
    {(ID_USRCFGDAT_CAN_TP_CHL_0_RX_ID)          ,     184u      ,sizeof(uint32_t)   ,(ID_USRCFGDAT_CAN_TP_CHL_0_RX_ID_DEF)      },\
    {(ID_USRCFGDAT_CAN_TP_CHL_0_TX_ID)          ,     188u      ,sizeof(uint32_t)   ,(ID_USRCFGDAT_CAN_TP_CHL_0_TX_ID_DEF)      },\
    {(ID_USRCFGDAT_CAN_TP_CHL_1_BUS_ID)         ,     192u      ,sizeof(uint8_t)    ,(ID_USRCFGDAT_CAN_TP_CHL_1_BUS_ID_DEF)     },\
    {(ID_USRCFGDAT_CAN_TP_CHL_1_STD_EXT)        ,     193u      ,sizeof(uint8_t)    ,(ID_USRCFGDAT_CAN_TP_CHL_1_STD_EXT_DEF)    },\
    {(ID_USRCFGDAT_CAN_TP_CHL_1_RX_ID)          ,     194u      ,sizeof(uint32_t)   ,(ID_USRCFGDAT_CAN_TP_CHL_1_RX_ID_DEF)      },\
    {(ID_USRCFGDAT_CAN_TP_CHL_1_TX_ID)          ,     198u      ,sizeof(uint32_t)   ,(ID_USRCFGDAT_CAN_TP_CHL_1_TX_ID_DEF)      },\
    \
    {(ID_USRCFGDAT_GEN_DETECTION_RETRY_CNT)     ,     202u      ,sizeof(uint8_t)    ,0u    },\
    \
    {(ID_USRCFGDAT_FORCE_CAN_STAT_MUX)          ,     203u      ,sizeof(uint8_t)    ,0u    },\
    {(ID_USRCFGDAT_FORCE_CAN_WEBASTO_BRIDGE)    ,     204u      ,sizeof(uint8_t)    ,0u    },\
    \
    {(ID_USRCFGDAT_TRG_DEF_USR_CFG_DATA)        ,     504u      ,sizeof(uint32_t)   ,0u    },\
}




#endif  


