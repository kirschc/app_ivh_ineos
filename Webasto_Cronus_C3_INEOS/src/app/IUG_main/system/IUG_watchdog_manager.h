#ifndef IUG_WATCHDOG_MANAGER_H
#define IUG_WATCHDOG_MANAGER_H


#define WD_MANAGER_CFG_INIT_CMP_TIME_MS   1000u 
#define WD_MANAGER_CFG_DURATION_MULTI     5u    

#define SET_WD_TIME_COUNTER_OF_OS_BY_TIMER()   ext_pmic_wd_counter = ext_minimum_watchdog_trigger_time_ms \
                                                                   - (uint16_t)((uint32_t)(TAUB0CNT0 * 823u) / 1000u)

#define SET_WD_TIME_COUNTER_OF_OS_TO_MAX()     ext_pmic_wd_counter = ext_minimum_watchdog_trigger_time_ms;

typedef enum
{
    WD_MANAGER_ERR_NONE                = 0u, 
    WD_MANAGER_ERR_TASK_INVAL              , 
    WD_MANAGER_ERR_TASK_NOT_CHECKED_IN     , 
    WD_MANAGER_ERR_TASK_BSW_DEAD           , 
    WD_MANAGER_ERR_TASK_CUSTOMER_DEAD      , 
} enum_WD_MANAGER_ERROR_STATES;

typedef enum
{
    WD_MANAGER_OS_TASK_1MS                  = 0u, 
    WD_MANAGER_OS_TASK_APP_INIT_AND_EOL_1MS     , 
    WD_MANAGER_OS_TASK_10MS                     , 
    WD_MANAGER_OS_TASK_20MS                     , 
    WD_MANAGER_OS_TASK_100MS                    , 
    WD_MANAGER_OS_TASK_CUSTOMER                 , 
    WD_MANAGER_OS_TASK_IDLE                     , 
    WD_MANAGER_OS_TASK_ENUM_MAX                   
} enum_WD_MANAGER_OS_TASKS;

typedef struct
{
    enum_WD_MANAGER_OS_TASKS     os_task;     
    uint8_t                      os_task_pos; 
    enum_WD_MANAGER_ERROR_STATES os_task_err; 
} struct_wd_manager_os_task_map_table;

typedef struct
{
    uint8_t  os_task_skip_verify;   
    uint8_t  os_task_register;      
    uint8_t  os_task_state;         
    uint16_t os_task_verify_cnt;    
    uint16_t os_task_cmp_time_rt;   
    uint16_t os_task_cmp_time_init; 
    boolean  wait_for_wd_trg_flg;   
} struct_wd_manager_data_handle;


void IUG_sys_wd_manager_init(const uint16_t task_duration);

void IUG_sys_wd_manager_cyclic(void);

static enum_WD_MANAGER_ERROR_STATES IUG_sys_wd_manager_os_monitor_cmp_task_status(void);

static void IUG_sys_wd_manager_system_task_faulty(void);

void IUG_sys_wd_manager_customer_task_faulty(void);

extern inline void IUG_sys_wd_manager_os_monitor_lock_task_verify(void);

extern inline void IUG_sys_wd_manager_os_monitor_unlock_task_verify(void);

enum_WD_MANAGER_ERROR_STATES IUG_sys_wd_manager_os_monitor_check_in_task(const enum_WD_MANAGER_OS_TASKS os_task);

enum_WD_MANAGER_ERROR_STATES IUG_sys_wd_manager_os_monitor_check_out_task(const enum_WD_MANAGER_OS_TASKS os_task);

enum_WD_MANAGER_ERROR_STATES IUG_sys_wd_manager_os_monitor_set_task_alive(const enum_WD_MANAGER_OS_TASKS os_task);

extern inline void IUG_sys_wd_manager_os_monitor_reset(void);

void IUG_sys_wd_manager_wait_for_wd_trigger(void);


#endif 


