#ifndef _IUG_SYSTEM_FUNCTION_H_
#define _IUG_SYSTEM_FUNCTION_H_


#define IUG_CFG_BOOT_DELAY_TIME_MS          500u          
#define IUG_EVAL_VCC_7V_ERR_THOLD           2000u         

#define POR_RESET_TIMER                     60u           

#define IUG_API_FEAT_UNIQUE_KEY             (uint8_t)0x24 

#define IUG_CFG_ACT_EXT_DIAG_DATA_TIME_MS   4000u         

#define MASK_RESET_IS_TASK_DEAD          0x0001u 
#define MASK_RESET_WAS_TASK_DEAD         0x0002u 
#define MASK_RESET_IS_TASK_OVERFLOW      0x0004u 
#define MASK_RESET_WAS_TASK_OVERFLOW     0x0008u 
#define MASK_RESET_IS_DEFAULT_LOADING    0x0010u 
#define MASK_RESET_WAS_DEFAULT_LOADING   0x0020u 
#define MASK_RESET_WAS_PMIC_FAIL_SAFE    0x0040u 

#define KEEP_ALIVE_TRG_MASK_CLEAN          (uint16_t)0x0000  
#define KEEP_ALIVE_TRG_MASK_CMD_BUTTON     (uint16_t)0x0001
#define KEEP_ALIVE_TRG_MASK_KL15           (uint16_t)0x0002
#define KEEP_ALIVE_TRG_MASK_PIGGY          (uint16_t)0x0004
#define KEEP_ALIVE_TRG_MASK_BLE            (uint16_t)0x0008
#define KEEP_ALIVE_TRG_MASK_LIN1_PWM_IN    (uint16_t)0x0010
#define KEEP_ALIVE_TRG_MASK_LIN3           (uint16_t)0x0020   
#define KEEP_ALIVE_TRG_MASK_LIN5           (uint16_t)0x0040
#define KEEP_ALIVE_TRG_MASK_CAN1           (uint16_t)0x0080
#define KEEP_ALIVE_TRG_MASK_CAN2           (uint16_t)0x0100
#define KEEP_ALIVE_TRG_MASK_CAN3           (uint16_t)0x0200
#define KEEP_ALIVE_TRG_MASK_LIN4           (uint16_t)0x0400   
#define KEEP_ALIVE_TRG_MASK_API            (uint16_t)0x0800
#define KEEP_ALIVE_TRG_MASK_SLEEP_CNT      (uint16_t)0x4000  
#define KEEP_ALIVE_TRG_MASK_ALIVE_CNT      (uint16_t)0x8000  

#define KEEP_ALIVE_TRG_SET_STATUS(x, y)    ( (x & (uint16_t)0x0FFF) | y )
#define KEEP_ALIVE_TRG_CLR_WU_SOURCES(x)     (x & (uint16_t)0xF000)

#define KEEP_ALIVE_TRG_WBUS_DELAY_MS       2000u 

#define CFG_WAKEUP_HISTORY_ENTRIES   10u 


typedef enum
{
    SM_POWER_MODE_MCU_ACTIVE    = 0u,
    SM_POWER_MODE_MCU_DEEP_STOP     ,
    SM_POWER_MODE_MCU_OFF

}enum_SM_MCU_POWER_MODES;

typedef enum
{
    SM_WU_DETECT_EDGE_RISING  = 1u,
    SM_WU_DETECT_EDGE_FALLING     ,
    SM_WU_DETECT_EDGE_BOTH
}enum_SM_WU_DETECTION;

typedef enum
{
    KEEP_ALIVE_TRG_CMD_BUTTON  = 0u,
    KEEP_ALIVE_TRG_KL15            ,
    KEEP_ALIVE_TRG_PIGGY           ,
    KEEP_ALIVE_TRG_BLE             ,
    KEEP_ALIVE_TRG_LIN1_PWM_IN     ,
    KEEP_ALIVE_TRG_LIN3            ,
#ifdef ___IUG_UNCLEAR_TOPIC___KEEP_ALIVE_TRG_MASK_LIN5
    KEEP_ALIVE_TRG_LIN5            ,
#endif
    KEEP_ALIVE_TRG_CAN1            ,
    KEEP_ALIVE_TRG_CAN2            ,
    KEEP_ALIVE_TRG_CAN3            ,
    KEEP_ALIVE_TRG_LIN4            ,
    KEEP_ALIVE_TRG_API             ,
    KEEP_ALIVE_TRG_ENUM_MAX         
} enum_IUG_KEEP_ALIVE_TRG_SOURCES;

typedef enum
{
    WAKEUP_HISTORY_ENTRY_ACCESS_NONE = 0u,      
    WAKEUP_HISTORY_ENTRY_ACCESS_WAKEUP,         
    WAKEUP_HISTORY_ENTRY_ACCESS_SLEEP           
} enum_WAKEUP_HISTORY_ENTRY_ACCESS;

typedef enum
{
    WAKEUP_HISTORY_WU_REASON_NONE         = (uint16_t)0x0000, 
    WAKEUP_HISTORY_WU_REASON_MCU_LIN1     = (uint16_t)0x0001, 
    WAKEUP_HISTORY_WU_REASON_MCU_PMIC     = (uint16_t)0x0002, 
    WAKEUP_HISTORY_WU_REASON_MCU_RTC      = (uint16_t)0x0004, 
    WAKEUP_HISTORY_WU_REASON_PMIC_WU1     = (uint16_t)0x0008, 
    WAKEUP_HISTORY_WU_REASON_PMIC_WU2     = (uint16_t)0x0010, 
    WAKEUP_HISTORY_WU_REASON_PMIC_WU3     = (uint16_t)0x0020, 
    WAKEUP_HISTORY_WU_REASON_PMIC_LIN     = (uint16_t)0x0040, 
    WAKEUP_HISTORY_WU_REASON_PMIC_CAN     = (uint16_t)0x0080, 
    WAKEUP_HISTORY_WU_REASON_WAKEUP       = (uint16_t)0x0100, 
    WAKEUP_HISTORY_WU_REASON_SLEEP        = (uint16_t)0x0200, 
    WAKEUP_HISTORY_WU_REASON_PWR_ON_RESET = (uint16_t)0x0400, 
    WAKEUP_HISTORY_WU_REASON_PWR_ON       = (uint16_t)0x0800  
} enum_WAKEUP_HISTORY_WU_REASON;

#pragma pack(1) 
typedef struct
{
    uint8_t                       ary_rtc_val[5u]; 
    enum_WAKEUP_HISTORY_WU_REASON wakeup_states;   
} struct_ecu_ext_sys_data_wakeup_history_entries;
#pragma pack() 

#pragma pack(1) 
typedef struct
{
    uint8_t                                        next_used_entry;                                          
    struct_ecu_ext_sys_data_wakeup_history_entries ary_wakeup_history_entries[(CFG_WAKEUP_HISTORY_ENTRIES)]; 
} struct_ecu_ext_sys_data_wakeup_history_data;
#pragma pack() 

#pragma pack(1) 
typedef struct
{
    uint32_t                                    start_up_cnt_appl;                
    uint8_t                                     por_state;                        
    uint8_t                                     unused;                           
    uint32_t                                    reset_reason_task_dead_count;     
    uint32_t                                    reset_reason_task_overflow_count; 
    uint32_t                                    reset_reason_pmic_count;          
    uint8_t                                     os_task_state;                    
    uint8_t                                     pmic_gsr;                         
    uint32_t                                    mcu_reset_reg;                    
    struct_ecu_ext_sys_data_wakeup_history_data wakeup_history_data;              
} struct_ecu_ext_sys_data_appl;
#pragma pack() 

#pragma pack(1) 
typedef struct
{
    uint32_t                     unique_key;           
    uint32_t                     start_up_cnt_bsw;     
    uint16_t                     reset_reason_record;  
    uint16_t                     reset_reason_last;    
    uint32_t                     default_load_count;   
    struct_ecu_ext_sys_data_appl ecu_ext_sys_data_appl; 
} struct_ecu_ext_sys_data_main;
#pragma pack() 

typedef struct
{
    uint8_t  trg_source;
    uint16_t trg_source_mask;
    uint8_t  trg_source_used;
} struct_IUG_keep_alive_sources;

extern struct_IUG_SYSTEM_STATE_t    IUG_SYSTEM_STATE;
extern struct_ecu_ext_sys_data_main ext_ecu_ext_sys_data_main;
extern uint8_t                      IUG_SYSTEM_STATE_unit_fast_access_en;   
extern volatile uint16_t            ext_IUG_keep_alive_sources_status;



void IUG_system_standby_process(void);

void IUG_system_set_power_mode(const enum_SM_MCU_POWER_MODES pwr_mode_mcu);

static void IUG_system_process_standby_mode(const enum_L99PM72_MODE_STANDBY_TYPE pwr_mode_pmic);

void IUG_system_set_outputs_defined_to_off(const enum_HAL_SYS_RESET);

static void IUG_system_set_transceiver_to_sleep(void);

void IUG_system_energy_manager( void );


void IUG_system_init( void );

void IUG_system_io_init(void);


void IUG_system_rtc_time_cyclic( void );


void IUG_system_handle_reset_of_por_counter(void);

void IUG_system_execute_reset(void);

void IUG_system_handle_keep_alive_cyclic(void);

void IUG_system_collect_ext_diag_data_cyclic(void);

void IUG_system_collect_ext_diag_data_reset_reason(void);

void IUG_system_collect_ext_diag_data_for_wakeup_history(const enum_WAKEUP_HISTORY_ENTRY_ACCESS entry_access);

void IUG_system_ecu_develop_leds(void);


boolean IUG_system_is_reset_reason_por(void);

#endif 


