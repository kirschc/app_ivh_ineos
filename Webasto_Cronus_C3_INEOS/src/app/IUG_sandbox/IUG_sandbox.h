#ifndef _IUG_SANDBOX_H_
#define _IUG_SANDBOX_H_


#ifdef IUG_NO_SOFTWARE_DELIVERY_RELEASE
#define CAR_CLIMATE_PWM
    #define CAR_CLIMATE_CRONTROLED_BY_BSW
#else
    #undef CAR_CLIMATE_PWM
    #undef CAR_CLIMATE_LIN
    #undef CAR_CLIMATE_LIN_LIN
#endif



void sandbox_code_cyclic_1ms(void);


void sandbox_code_cyclic_20ms(void);

#endif 


