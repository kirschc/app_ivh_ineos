#ifndef __IUG_CALLBACK_H_
#define __IUG_CALLBACK_H_

#define IUG_CAR_STATE_DETECT_DELAY_TIME_MIN_MS 1000u 

typedef enum
{
    CAR_STATE_SOURCE_KL15               = 0u, 
    CAR_STATE_SOURCE_LIN1               = 1u, 
    CAR_STATE_SOURCE_CAN1               = 2u, 
    CAR_STATE_SOURCE_CAN3               = 3u, 
    CAR_STATE_SOURCE_ENUM_MAX               , 
    CAR_STATE_SOURCE_ENUM_FORCE_TYPE = 0x7Fu  
} enum_CAR_STATE_SOURCES;

typedef struct
{
    uint8_t  state;
    uint32_t reset_timer;
} struct_car_source_state;

typedef struct
{
    enum_CAR_STATE_SOURCES     source;
    struct_car_source_state    *ptr_car_source_state;
    enum_parameter_id          source_avail_para_id;
    enum_parameter_id          source_state_para_id;
    enum_parameter_diagnose_id source_state_para_diag_id;
} struct_car_sources_config;


void IUG_callback_03_FahrzeugZustandserkennung_Cyclic( void );

void IUG_callback_31_MV_32_UP_Ansteuerung_pre_cond( void );

void IUG_callback_get_car_state_sources_cfg(const struct_car_sources_config **const ptr_car_state_sources);

uint32_t IUG_callback_get_car_state_off_delay_time_left(void);


#endif 


