#ifndef _IUG_EOL_CAN_DB_TABLES_H_
#define _IUG_EOL_CAN_DB_TABLES_H_




#if 0 
#define CAN_BAUDRATE BIOS_CAN_EXT_BAUDRATE


#define  CAN_BAUDRATE_def_BEZEICHNUNG  125 kBit
#define  CAN_BAUDRATE_def_sj           2
#define  CAN_BAUDRATE_def_pre          4
#define  CAN_BAUDRATE_def_t_seg1       13
#define  CAN_BAUDRATE_def_t_seg2       2
#define  CAN_BAUDRATE_def_source       0



#define USER_CAN_FILTER_ACTIVE

#ifdef USER_CAN_FILTER_ACTIVE

    #define CAN0_FILTER_A 0x704
    #define CAN0_FILTER_B 0x705
    #define CAN0_MASK_A   0x000
    #define CAN0_MASK_B   0x000
    #define CAN0_MASK_DIVISION BIOS_CAN_ID_NORM_A_AND_B

    #define CAN1_FILTER_A 0x000
    #define CAN1_FILTER_B 0x000
    #define CAN1_MASK_A   0x000
    #define CAN1_MASK_B   0x000
    #define CAN1_MASK_DIVISION BIOS_CAN_ID_ALL_CLOSED

    #define CAN2_FILTER_A 0x000
    #define CAN2_FILTER_B 0x000
    #define CAN2_MASK_A   0x000
    #define CAN2_MASK_B   0x000
    #define CAN2_MASK_DIVISION BIOS_CAN_ID_ALL_CLOSED

    #define CAN3_FILTER_A 0x000
    #define CAN3_FILTER_B 0x000
    #define CAN3_MASK_A   0x000
    #define CAN3_MASK_B   0x000
    #define CAN3_MASK_DIVISION BIOS_CAN_ID_ALL_CLOSED

    #define CAN4_FILTER_A 0x000
    #define CAN4_FILTER_B 0x000
    #define CAN4_MASK_A   0x000
    #define CAN4_MASK_B   0x000
    #define CAN4_MASK_DIVISION BIOS_CAN_ID_ALL_CLOSED

#endif

#define FLOAT   0x24
#define ULONG   0x04
#define SLONG   0x14
#define UINT    0x02
#define SINT    0x12
#define UBYTE   0x01
#define SBYTE   0x11



#define IS_GW_INPUT      1
#define IS_NO_GW_INPUT   0


#define CAN_NO_EEPROM_INIT 255
#define CAN_INIT_EEPROM_SIZE 20
#endif


typedef enum
{
    CAN_GATEWAY_DB_MAX_EOL    =   0
}can_gateway_id_eol;

typedef enum
{
    CAN_BUS_0_EOL = 0,
    CAN_BUS_1_EOL    ,
    CAN_BUS_2_EOL    ,
    CAN_BUS_3_EOL    ,
    CAN_BUS_4_EOL    ,
    CAN_BUS_MAX_EOL
} can_bus_id_eol;



typedef enum
{
    CAN_DP_STAT_BUS_LIN1    = 0,
    CAN_DP_STAT_BUS_LIN2       ,
    CAN_DP_STAT_BUS_LIN3       ,
    CAN_DP_STAT_BUS_LIN4       ,
    CAN_DP_STAT_BUS_LIN5       ,
    CAN_DP_STAT_BUS_CAN1       ,
    CAN_DP_STAT_BUS_CAN2       ,
    CAN_DP_STAT_BUS_CAN3       ,
    CAN_DP_AI_KL15             ,
    CAN_DP_AI_KL30             ,
    CAN_DP_AI_PUSH_BUTTON      ,
    CAN_DP_AI_ANA              ,
    CAN_DP_AI_12V_OUT          ,
    CAN_DP_AI_PWM_PEAK_DETECT  ,
    CAN_DP_AI_SENSE_PANEL      ,
    CAN_DP_AI_LIN3             ,
    CAN_DP_AI_PORT_1           ,
    CAN_DP_AI_PORT_4           ,
    CAN_DP_AI_PORT_2           ,
    CAN_DP_AI_PORT_3           ,
    CAN_DP_AI_PWM_IN           ,
    CAN_DP_AI_INT_PWM          ,
    CAN_DP_AI_INT_IO3          ,
    CAN_DP_AI_PIGGY            ,
    CAN_DP_AI_PWM_OUT          ,
    CAN_STAT_BMP280_TEMPERATURE,
    CAN_STAT_BMP280_PRESSURE   ,
    CAN_DP_DI_CAN1_EER         ,
    CAN_DP_DI_CAN2_EER         ,
    CAN_DP_DI_SPI2_MISO        ,
    CAN_DP_DI_INT_PIGGY_INH    ,
    CAN_DP_HW_VERSION          ,
    CAN_DP_HW_REVISION         ,
    CAN_DP_SW_RELEASE          ,
    CAN_DP_SW_MAJOR            ,
    CAN_DP_SW_MINOR            ,
    CAN_DP_VOLTAGE_HSD_7040    ,
    CAN_DP_SERIAL_NUMBER       ,
    CAN_DP_STAT_PWM_CALIB      ,
    CAN_DP_STAT_BLE_INIT       ,
    CAN_DP_STAT_SUPPLY_VOLT    ,
    CAN_STAT_PWM_IN_FREQ       ,
    CAN_STAT_PWM_IN_DUTY       ,
    CAN_STAT_RTC_SEC           ,
    CAN_STAT_RTC_MIN           ,
    CAN_STAT_RTC_HOUR          ,
    CAN_STAT_RTC_DAY           ,
    CAN_STAT_RTC_WDAY          ,
    CAN_STAT_RTC_MONTH         ,
    CAN_STAT_RTC_YEAR          ,
    CAN_DP_CAN_Test            ,
    CAN_DP_PMIC_HSD_OUT_1      ,
    CAN_DP_PMIC_HSD_OUT_2      ,
    CAN_DP_PMIC_HSD_OUT_3      ,
    CAN_DP_PMIC_HSD_OUT_4      ,
    CAN_DP_PMIC_HSD_OUT_HS     ,
    CAN_DP_PMIC_LSD_OUT_1      ,
    CAN_DP_PMIC_LSD_OUT_2      ,
    CAN_DP_LIN_IO_TEST         ,
    CAN_DP_PWM1_DUTY           ,
    CAN_DP_PWM2_DUTY           ,
    CAN_DP_PWM3_DUTY           ,
    CAN_DP_PWM4_DUTY           ,
    CAN_DP_PWM_FREQ            ,
    CAN_DP_CONFIG_SPI2         ,
    CAN_DP_HIBERNATION_MODES   ,
    CAN_DP_HIBERNATION_ACTIVATION,
    CAN_DP_ACT_PWM_IN          ,
    CAN_DP_ACT_PWM_OUT         ,
    CAN_DP_DO_IO1_PU           ,
    CAN_DP_DO_IO2_PU           ,
    CAN_DP_DO_IO3_PU           ,
    CAN_DP_DO_IO4_PU           ,
    CAN_DP_DO_FAULT_RST        ,
    CAN_DP_DO_LIN1_WAKE        ,
    CAN_DP_DO_LIN2_WAKE        ,
    CAN_DP_DO_LIN3_WAKE        ,
    CAN_DP_DO_LIN5_WAKE        ,
    CAN_DP_DO_LIN1_EN          ,
    CAN_DP_DO_LIN2_EN          ,
    CAN_DP_DO_LIN3_EN          ,
    CAN_DP_DO_CAN1_TERMINATION ,
    CAN_DP_DO_CAN2_TERMINATION ,
    CAN_DP_DO_CAN3_TERMINATION ,
    CAN_DP_DO_WBUS3_TER        ,
    CAN_DP_DO_EN_VCC7          ,
    CAN_DP_DO_RESET_BT         ,
    CAN_DP_DO_LIN3_TER         ,
    CAN_DP_DO_BEDIENTEIL       ,
    CAN_DP_DO_PIGGY_ENABLE     ,
    CAN_DP_DO_CS_SPI2_PIGGY    ,
    CAN_DP_DO_RELAIS_CAN       ,
    CAN_DP_DO_RELAIS_LIN       ,
    CAN_DP_DO_CS_SPI2_PIGGY_2  ,
    CAN_DP_DO_REL_OUT          ,
    CAN_DP_DO_I2C_SCL          ,
    CAN_DP_DO_LIN2_TER         ,
    CAN_DP_DO_CAN1_WAKE        ,
    CAN_DP_DO_CAN2_WAKE        ,
    CAN_DP_DO_LIN5_EN          ,
    CAN_DP_DO_I2C_SDA          ,
    CAN_DP_DO_INT_PIGGY_INH    ,
    CAN_DP_DO_PIGGY_RXD        ,
    CAN_DP_DO_PIGGY_TXD        ,
    CAN_STIM_PWM_MODULE        ,
    CAN_STIM_PWM_OUTPUT_MODE   ,
    CAN_STIM_PWM_CAL_ENABLE    ,
    CAN_STIM_PWM_TIMER_AI_ACTIVE,
    CAN_STIM_PWM_OUT_AMPL      ,
    CAN_STIM_PWM_OUT_FREQ      ,
    CAN_STIM_PWM_OUT_DUTY      ,
    CAN_DP_MAX_EOL            
}can_dp_id_eol;




typedef enum
{
    CAN_STIM_DO             = 0,
    CAN_STIM_1                 ,
    CAN_STIM_PWM               ,
    CAN_STAT_RTC               ,
    CAN_STAT_PWM               ,
    CAN_STAT_BMP280            ,
    CAN_STAT_DI                ,
    CAN_STAT_AI3               ,
    CAN_STAT_AI0               ,
    CAN_STAT_AI1               ,
    CAN_STAT_AI2               ,
    CAN_STAT_IUG               ,
    CAN_STAT_AI4               ,
    CAN_STAT_IUG_2             ,
    CAN_FRM_STAT_BUS           ,
    CAN_BLOCK_MAX_EOL         
} can_block_id_eol;






#endif




