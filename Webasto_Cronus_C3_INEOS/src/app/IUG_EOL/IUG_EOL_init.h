#ifndef _IUG_EOL_INIT_H_
#define _IUG_EOL_INIT_H_







static uint8_t IUG_EOL_entry_secure_check(void);

void IUG_EOL_entry(void);

void IUG_EOL_init(void);

static void IUG_EOL_digital_io_init(void);


#endif


