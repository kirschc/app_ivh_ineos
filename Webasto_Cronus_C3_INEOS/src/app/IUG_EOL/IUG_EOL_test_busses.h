#ifndef IUG_EOL_TEST_BUSSES_H
#define IUG_EOL_TEST_BUSSES_H


#define EOL_CFG_UART_STRING_RES        "LIN%d=%c:ok\r" 
#define EOL_CFG_UART_STRING_SIZE       10u             
#define EOL_CFG_UART_TIMEOUT_MS        50u             

#define EOL_CFG_LIN_IO_CYCLE_TIME_MS   5u             
#define EOL_CFG_LIN_IO_CHECK_STAT_MS   ( (EOL_CFG_LIN_IO_CYCLE_TIME_MS) * 5u ) 

#define EOL_CFG_CAN_CYCLE_TIME_MS      50u             
#define EOL_CFG_CAN_CHECK_STAT_MS      ( (EOL_CFG_CAN_CYCLE_TIME_MS) * 5u ) 
#define EOL_CFG_CAN_ID_BUS1            0x101u          
#define EOL_CFG_CAN_ID_BUS2            0x102u          
#define EOL_CFG_CAN_ID_BUS3            0x103u          

#define EOL_FLOW_CTRL_CMP_ACT_RX(x)   ( (x) & EOL_UART_FLOW_CTRL_____RX_REQ )
#define EOL_FLOW_CTRL_SET_REQ_RX(x)   (x) |= EOL_UART_FLOW_CTRL_____RX_REQ; \
                                      (x) &= ~EOL_UART_FLOW_CTRL___COM_IDLE; \
                                      (x) &= ~EOL_UART_FLOW_CTRL_ERR_TX_BAD; \
                                      (x) &= ~EOL_UART_FLOW_CTRL_ERR_TX_RES
#define EOL_FLOW_CTRL_SET_CFM_RX(x)   (x) &= ~EOL_UART_FLOW_CTRL_____RX_REQ; \
                                      (x) |= EOL_UART_FLOW_CTRL_____RX_CFM

#define EOL_FLOW_CTRL_CMP_ACT_TX(x)   ( (x) & EOL_UART_FLOW_CTRL_____TX_REQ )
#define EOL_FLOW_CTRL_SET_REQ_TX(x)   (x) |= EOL_UART_FLOW_CTRL_____TX_REQ
#define EOL_FLOW_CTRL_SET_CFM_TX(x)   (x) &= ~EOL_UART_FLOW_CTRL_____TX_REQ; \
                                      (x) |= EOL_UART_FLOW_CTRL_____TX_CFM

#define EOL_LIN_IO_MASK_LVL_CURRENT       0x01u
#define EOL_LIN_IO_MASK_LVL_CHANGED       0x10u

#define EOL_LIN_IO_SET_LVL_CURRENT(x,y)   (x) = ( ((x) & ~(1u << 0u)) | ((y) << 0u) )
#define EOL_LIN_IO_SET_LVL_CHANGED(x)     (x) |= (EOL_LIN_IO_MASK_LVL_CHANGED)
#define EOL_LIN_IO_CLR_LVL_CHANGED(x)     (x) &= ~(EOL_LIN_IO_MASK_LVL_CHANGED)

typedef enum
{
    EOL_CFG_BUS_TEST_DEFAULT = 0u, 
    EOL_CFG_BUS_TEST_DUT         , 
    EOL_CFG_BUS_TEST_RMC         , 
    EOL_CFG_BUS_TEST_INVALID       
} enum_EOL_CFG_BUS_TEST;

typedef enum
{
    EOL_STAT_BUS_IDLE    = 0u, 
    EOL_STAT_BUS_OK          , 
    EOL_STAT_BUS_NOK         , 
    EOL_STAT_BUS_INVALID       
} enum_EOL_STAT_BUS;

typedef enum
{
    EOL_LIN_UART_RLIN31   = 0u, 
    EOL_LIN_UART_RLIN32       , 
    EOL_LIN_UART_RLIN33       , 
    EOL_LIN_UART_RLIN34       , 
    EOL_LIN_UART_ENUM_MAX       
} enum_EOL_LIN_UART_CHANNELS;

typedef enum
{
    EOL_LIN_IO_RLIN31   = 0u, 
    EOL_LIN_IO_RLIN32       , 
    EOL_LIN_IO_RLIN33       , 
    EOL_LIN_IO_RLIN34       , 
    EOL_LIN_IO_RLIN25       , 
    EOL_LIN_IO_ENUM_MAX       
} enum_EOL_LIN_IO_CHANNELS;

typedef enum
{
    EOL_UART_FLOW_CTRL___COM_IDLE = 0x00u, 
    EOL_UART_FLOW_CTRL_____RX_REQ = 0x01u, 
    EOL_UART_FLOW_CTRL_____RX_CFM = 0x02u, 
    EOL_UART_FLOW_CTRL_____TX_REQ = 0x04u, 
    EOL_UART_FLOW_CTRL_____TX_CFM = 0x08u, 
    EOL_UART_FLOW_CTRL_ERR_TX_BAD = 0x10u, 
    EOL_UART_FLOW_CTRL_ERR_TX_RES = 0x20u, 
    EOL_UART_FLOW_CTRL_ERR__Z_PTR = 0x40u  
} enum_EOL_UART_FLOW_CTRL;

typedef enum
{
    EOL_LIN_IO_FLOW_CTRL_____IDLE = 0x00u, 
    EOL_LIN_IO_FLOW_CTRL_____SYNC = 0x01u, 
    EOL_LIN_IO_FLOW_CTRL_____EVAL = 0x02u, 
    EOL_LIN_IO_FLOW_CTRL_ERR_SYNC = 0x10u, 
    EOL_LIN_IO_FLOW_CTRL_ERR_EVAL = 0x20u  
} enum_EOL_LIN_IO_FLOW_CTRL;

typedef enum
{
    EOL_CAN_1        = 0u, 
    EOL_CAN_2            , 
    EOL_CAN_3            , 
    EOL_CAN_ENUM_MAX       
} enum_EOL_CAN_BUSSES;

typedef struct
{
    enum_EOL_LIN_UART_CHANNELS eol_ch; 
    uint8_t                    sw_ch;  
    uint8_t                    hw_ch;  
} struct_eol_cfg_lin_uart_channel_mapping;

typedef struct
{
    enum_EOL_LIN_IO_CHANNELS eol_ch;      
    Dio_PinIdType            pin_wr;      
    Dio_PinIdType            pin_rd_rmc;  
    Dio_PinIdType            pin_rd_dut;  
    can_dp_id_eol            can_dp_stat; 
} struct_eol_cfg_lin_io_channel_mapping;

typedef struct
{
    enum_EOL_CAN_BUSSES   eol_ch;                    
    struct_hal_can_handle *ptr_handle;               
    uint32_t              id;                        
    uint8_t               data[ (HAL_CAN_DLC_MAX) ]; 
    can_dp_id_eol         can_dp_stat;               
} struct_eol_cfg_can_channel_mapping;

typedef struct
{
    volatile enum_EOL_UART_FLOW_CTRL    flow_ctrl;                                 
    volatile enum_EOL_LIN_UART_CHANNELS rx_eol_ch;                                 
    volatile uint8_t                    rx_data;                                   
    uint8_t                             ary_tx_data[ (EOL_CFG_UART_STRING_SIZE) ]; 
    uint8_t                             *ptr_tx_data;                              
    uint32_t                            timeout;                                   
} struct_eol_lin_uart_data;

typedef struct
{
    boolean           test_act;                          
    uint8_t           bus_stat[EOL_LIN_IO_ENUM_MAX];     
    enum_EOL_STAT_BUS bus_stat_loc[EOL_LIN_IO_ENUM_MAX]; 
    uint32_t          cycle_timer;                       
    uint32_t          check_timer;                       
} struct_eol_lin_io_data;

typedef struct
{
    boolean           test_act;                       
    volatile boolean  bus_stat_isr[EOL_CAN_ENUM_MAX]; 
    enum_EOL_STAT_BUS bus_stat_loc[EOL_CAN_ENUM_MAX]; 
    uint32_t          cycle_timer;                    
    uint32_t          check_timer;                    
} struct_eol_can_data;

typedef struct
{
    enum_EOL_CFG_BUS_TEST    cfg_bus_test;  
    struct_eol_lin_uart_data lin_uart_data; 
    struct_eol_lin_io_data   lin_io_data;   
    struct_eol_can_data      can_data;      
} struct_eol_test_busses_main_data;


void IUG_EOL_test_busses_init(void);

static void IUG_EOL_test_busses_lin_mode_uart_init(void);

static void IUG_EOL_test_busses_lin_mode_io_init(void);

void IUG_EOL_test_busses_cyclic(void);

static void IUG_EOL_test_busses_collect_data(void);

static void IUG_EOL_test_busses_provide_data(void);

static void IUG_EOL_test_busses_lin_mode_uart(void);

static void IUG_EOL_test_busses_lin_mode_uart_timeout(void);

static void IUG_EOL_test_busses_lin_mode_io_send(void);

static void IUG_EOL_test_busses_lin_mode_io_check_stat(void);

static void IUG_EOL_test_busses_rx_cb_rlin31(const uint8_t rx_data);

static void IUG_EOL_test_busses_rx_cb_rlin32(const uint8_t rx_data);

static void IUG_EOL_test_busses_rx_cb_rlin33(const uint8_t rx_data);

static void IUG_EOL_test_busses_rx_cb_rlin34(const uint8_t rx_data);

static void IUG_EOL_test_busses_can_msg_send(void);

static void IUG_EOL_test_busses_can_msg_check_stat(void);

void IUG_EOL_can_irq_receive_callback(const enum_CAN_INTERFACES can_bus, const struct_hal_can_frame *const ptr_can_msg);


#endif 


