#ifndef _IUG_EOL_MAIN_H_
#define _IUG_EOL_MAIN_H_





typedef struct
{
    uint8_t entry_key;
#ifndef EOL_EN_SEC_CHECK
    uint8_t no_sec_check; 
#endif
    uint8_t can_data[ (HAL_CAN_DLC_MAX) ];
} struct_eol_entry;

extern volatile struct_eol_entry ext_iug_eol_entry;


void IUG_EOL_main_cyclic(void);

static void IUG_EOL_process_ble(void);

static void IUG_EOL_set_data_to_can3(void);

static void IUG_EOL_get_data_from_can3(void);

static void IUG_EOL_hibernation_conditions(void);

static void IUG_EOL_config_spi2(void);

static void IUG_EOL_set_serial_number_to_can3(void);

static void IUG_EOL_set_supply_volt_range_to_can3(void);


#endif


