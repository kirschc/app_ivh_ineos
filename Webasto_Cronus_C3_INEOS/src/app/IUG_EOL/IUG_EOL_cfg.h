#ifndef _IUG_EOL_CFG_H_
#define _IUG_EOL_CFG_H_

#include "IUG_adjust.h"

#define EOL_ACTIVE_SEC_KEY  (uint32_t)0x4a4f4b49 
#ifndef IUG_NO_SOFTWARE_DELIVERY_RELEASE
    #define EOL_EN_SEC_CHECK                     
#endif

#define EOL_CFG_MASK_CAN_DP_EOL_CFG_ID_OFFSET(x)   ( ((uint8_t)(x) >> 1u) & 0x01u )
#define EOL_CFG_MASK_CAN_DP_EOL_CFG_BUS_TEST(x)    ( ((uint8_t)(x) >> 2u) & 0x03u )





#endif


