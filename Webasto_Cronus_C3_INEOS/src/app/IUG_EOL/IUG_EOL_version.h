#ifndef _IUG_EOL_VERSION_H_
#define _IUG_EOL_VERSION_H_

#define EOL_SW_VERSION_MAJOR   1u 
#define EOL_SW_VERSION_MINOR   0u 
#define EOL_SW_VERSION_BUILD   2u 

#if( (((EOL_SW_VERSION_MAJOR) < 0) || ((EOL_SW_VERSION_MAJOR) > 9))||\
     (((EOL_SW_VERSION_MINOR) < 0) || ((EOL_SW_VERSION_MINOR) > 9))  )
    #error "The 'EOL_SW_VERSION_MAJOR' and 'EOL_SW_VERSION_MINOR' numbers must be in range of '0...9'!"
#endif

#if( (((EOL_SW_VERSION_BUILD) < 0) || ((EOL_SW_VERSION_BUILD) > 99)) )
    #error "The 'BASE_SW_VERSION_BUILD' number must be in range of '0...99'!"
#endif


#endif 


