
#include "IUG_user_api_include.h"




#ifdef IUG_USER_API_CMD_BUTTON_DEMO_EN
const int16_t   API_CMDBTN_DEMO_PAT_DRV_HEATING_WAT_x_ms[] =
{
    {     0u  }, 
    {   100u  }, 
    {   150u  }, 
    {   200u  }, 
    {   250u  }, 
    {   300u  }, 
    {   400u  }, 
};

const int16_t   API_CMDBTN_DEMO_PAT_DRV_HEATING_WAT_y_bright[] =
{
    {   7u  },  
    {  38u  },  
    {  20u  },  
    {  38u  },  
    {  20u  },  
    {  38u  },  
    {   7u  },  
};

_Static_assert((sizeof(API_CMDBTN_DEMO_PAT_DRV_HEATING_WAT_x_ms))==(sizeof(API_CMDBTN_DEMO_PAT_DRV_HEATING_WAT_y_bright)),"Analyze: 'mgl_cmd_button_LED_CHARACTERISTIC_x_bright'");

#endif


#ifdef IUG_USER_API_CMD_BUTTON_DEMO_EN
typedef struct
{
    uint8_t     pat_operable_enabled;           
    uint8_t     pat_drv_heat_wat_enabled;       
    uint8_t     pat_btn_usr_dev_enabled;        

    API_CMDBTN_LED_PATTERN_t       pat_id_enabled;

} struct_API_CMD_BUTTON_DEMO_DATA_t;
typedef struct_API_CMD_BUTTON_DEMO_DATA_t      API_CMD_BUTTON_DEMO_DATA_t;

API_CMD_BUTTON_DEMO_DATA_t     API_CMDBTN_DEMO_DATA;
#endif


#ifdef IUG_USER_API_CMD_BUTTON_DEMO_EN
void USRAPI_cmd_button_cyclic_20ms_demo( void )
{

    static uint8_t  pat_operable_enabled_old     = 0u;
    static uint8_t  pat_drv_heat_wat_enabled_old = 0u;
    static uint8_t  pat_btn_usr_dev_enabled_old  = 0u;

    if( API_CMDBTN_DEMO_DATA.pat_operable_enabled != pat_operable_enabled_old )
    {
        pat_operable_enabled_old = API_CMDBTN_DEMO_DATA.pat_operable_enabled;

        if( 0u != API_CMDBTN_DEMO_DATA.pat_operable_enabled )
        {
            API_CMDBTN_DEMO_DATA.pat_id_enabled = IUG_cmd_button_usrapi_set_led_pattern_cfg(
                                                        (API_CMDBTN_PAT_IUG_OPERABLE),
                                                        5000u,
                                                        (API_CMDBTN_PAT_MODE_BINARY),
                                                        (sizeof(API_CMDBTN_DEMO_PAT_DRV_HEATING_WAT_x_ms)/sizeof(int16_t)),
                                                        (int16_t *)&API_CMDBTN_DEMO_PAT_DRV_HEATING_WAT_x_ms,
                                                        (int16_t *)&API_CMDBTN_DEMO_PAT_DRV_HEATING_WAT_y_bright );
        }
        else
        {
            API_CMDBTN_DEMO_DATA.pat_id_enabled = IUG_cmd_button_usrapi_del_led_pattern_cfg( (API_CMDBTN_PAT_IUG_OPERABLE) );

        }
    }

    if( API_CMDBTN_DEMO_DATA.pat_drv_heat_wat_enabled != pat_drv_heat_wat_enabled_old )
    {
        pat_drv_heat_wat_enabled_old = API_CMDBTN_DEMO_DATA.pat_drv_heat_wat_enabled;

        if( 0u != API_CMDBTN_DEMO_DATA.pat_drv_heat_wat_enabled )
        {
            API_CMDBTN_DEMO_DATA.pat_id_enabled = IUG_cmd_button_usrapi_set_led_pattern_cfg(
                                                        (API_CMDBTN_PAT_DRV_HEATING_WAT),
                                                        0u,
                                                        (API_CMDBTN_PAT_MODE_RAMP),
                                                        (sizeof(API_CMDBTN_DEMO_PAT_DRV_HEATING_WAT_x_ms)/sizeof(int16_t)),
                                                        (int16_t *)&API_CMDBTN_DEMO_PAT_DRV_HEATING_WAT_x_ms,
                                                        (int16_t *)&API_CMDBTN_DEMO_PAT_DRV_HEATING_WAT_y_bright );
        }
        else
        {
            API_CMDBTN_DEMO_DATA.pat_id_enabled = IUG_cmd_button_usrapi_del_led_pattern_cfg( (API_CMDBTN_PAT_DRV_HEATING_WAT) );
        }
    }    

    if( API_CMDBTN_DEMO_DATA.pat_btn_usr_dev_enabled != pat_btn_usr_dev_enabled_old )
    {
        pat_btn_usr_dev_enabled_old = API_CMDBTN_DEMO_DATA.pat_btn_usr_dev_enabled;

        if( 0u != API_CMDBTN_DEMO_DATA.pat_btn_usr_dev_enabled )
        {
            API_CMDBTN_DEMO_DATA.pat_id_enabled = IUG_cmd_button_usrapi_set_led_pattern_cfg(
                                                        (API_CMDBTN_PAT_BUTTON_USER_DEVICE),
                                                        1000u,
                                                        (API_CMDBTN_PAT_MODE_RAMP),
                                                        (sizeof(API_CMDBTN_DEMO_PAT_DRV_HEATING_WAT_x_ms)/sizeof(int16_t)),
                                                        (int16_t *)&API_CMDBTN_DEMO_PAT_DRV_HEATING_WAT_x_ms,
                                                        (int16_t *)&API_CMDBTN_DEMO_PAT_DRV_HEATING_WAT_y_bright );
        }
        else
        {
            API_CMDBTN_DEMO_DATA.pat_id_enabled = IUG_cmd_button_usrapi_del_led_pattern_cfg( (API_CMDBTN_PAT_BUTTON_USER_DEVICE) );
        }
    }    

}
#endif



