#ifndef OBD_DIAGNOSIS_CFG_H
#define OBD_DIAGNOSIS_CFG_H

#include "hal_data_types.h"

#define OBD_PID_REQUEST_INTERVAL_MS   200u

#define OBD_CAN_ID_11BIT_REQ          0x7DFu      
#define OBD_CAN_ID_11BIT_RES          0x7E8u      
#define OBD_CAN_ID_29BIT_REQ          0x18DB33F1u 
#define OBD_CAN_ID_29BIT_RES          0x18DAF110u 

#define OBD_CAN_BUS_HW_IDX            3u

#define OBD_WITH_CAR_STATE

typedef int32_t (*funct_ptr)(const uint8_t, const uint8_t , const uint8_t , const uint8_t);

typedef enum
{
    OBD_ENGINE_COOLANT_TEMP = 0u, 
    OBD_ENGINE_RPM              , 
    OBD_VEHICLE_SPEED           , 
    OBD_PID_ENUM_MAX              
} enum_PIDS_DESCRIPTION;

typedef struct
{
    int32_t value;  
    uint8_t status; 
} struct_obd_pid_values;

typedef struct
{
    enum_PIDS_DESCRIPTION pid_name_idx; 
    uint8_t               pid;          
    funct_ptr             funct_calc;   
} struct_obd_pid_config;

extern const struct_obd_pid_config ext_ary_obd_pid_config[OBD_PID_ENUM_MAX];
extern const uint8_t ext_entries_ext_ary_obd_pid_config;
extern struct_obd_pid_values ext_obd_pid_values[OBD_PID_ENUM_MAX];
extern enum_PIDS_DESCRIPTION ext_ary_pid_queue[OBD_PID_ENUM_MAX];


static int32_t obd_cb_calc_pid_05(const uint8_t data_byte_1, const uint8_t data_byte_2, const uint8_t data_byte_3, const uint8_t data_byte_4);

static int32_t obd_cb_calc_pid_0C(const uint8_t data_byte_1, const uint8_t data_byte_2, const uint8_t data_byte_3, const uint8_t data_byte_4);

static int32_t obd_cb_calc_pid_0D(uint8_t const data_byte_1, const uint8_t data_byte_2, const uint8_t data_byte_3, const uint8_t data_byte_4);

#endif 

