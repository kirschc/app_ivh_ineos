
#include "obd_diagnosis_cfg.h"


const struct_obd_pid_config ext_ary_obd_pid_config[OBD_PID_ENUM_MAX] =
{
    { OBD_ENGINE_COOLANT_TEMP , 0x05u     , obd_cb_calc_pid_05 },
    { OBD_ENGINE_RPM          , 0x0Cu     , obd_cb_calc_pid_0C },
    { OBD_VEHICLE_SPEED       , 0x0Du     , obd_cb_calc_pid_0D }
};

const uint8_t ext_entries_ext_ary_obd_pid_config = ( sizeof(ext_ary_obd_pid_config) / sizeof(struct_obd_pid_config) );

struct_obd_pid_values ext_obd_pid_values[OBD_PID_ENUM_MAX] = {0u}; 
enum_PIDS_DESCRIPTION ext_ary_pid_queue[OBD_PID_ENUM_MAX];         


static int32_t obd_cb_calc_pid_05(const uint8_t data_byte_1, const uint8_t data_byte_2, const uint8_t data_byte_3, const uint8_t data_byte_4)
{
    int32_t data;

    data = data_byte_1 - 40u;

    return data;
}

static int32_t obd_cb_calc_pid_0C(const uint8_t data_byte_1, const uint8_t data_byte_2, const uint8_t data_byte_3, const uint8_t data_byte_4)
{
    int32_t data;

    data = ( 256u * data_byte_1 + data_byte_2 ) / 4u;

    return data;
}

static int32_t obd_cb_calc_pid_0D(const uint8_t data_byte_1, const uint8_t data_byte_2, const uint8_t data_byte_3, const uint8_t data_byte_4)
{
    int32_t data;

    data = data_byte_1;

    return data;
}


#if ( (OBD_PID_REQUEST_INTERVAL_MS) < (200u) )
    #error "'OBD_PID_REQUEST_INTERVAL_MS' must be at least minimal '200 ms'. Check OBD Config!"
#endif

#if ( ((OBD_CAN_BUS_HW_IDX) != (1u)) && \
      ((OBD_CAN_BUS_HW_IDX) != (2u)) && \
      ((OBD_CAN_BUS_HW_IDX) != (3u))   )
    #error "Application supports only CAN1, CAN2 or CAN3. Check 'OBD_CAN_BUS_HW_IDX' in OBD Config!"
#endif

_Static_assert( (OBD_PID_ENUM_MAX) == (sizeof(ext_ary_obd_pid_config) / sizeof(struct_obd_pid_config) ), \
                "'OBD_PID_ENUM_MAX' must match the entries in 'ext_ary_obd_pid_config[]'. Check OBD Config!" );



