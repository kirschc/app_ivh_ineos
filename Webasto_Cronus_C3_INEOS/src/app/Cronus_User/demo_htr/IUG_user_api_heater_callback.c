
#include "IUG_user_api_include.h"



IUG_UNIT_API_COMMAND_t USRAPI_heater_get_cmd_confirmation( const IUG_UNIT_API_UNIT_ID_t unt_api_id,
                                                           const IUG_UNIT_API_COMMAND_t unt_api_cmd )
{
    IUG_UNIT_API_COMMAND_t   ret_cfm;

    ret_cfm = (IUG_UNIT_API_CMD_OFF);


    #ifndef IUG_USER_API_CONTROL_HEATER_DEMO_EN
    switch( unt_api_cmd )
    {
        case (IUG_UNIT_API_CMD_HEATING):
        case (IUG_UNIT_API_CMD_BOOST):
        case (IUG_UNIT_API_CMD_ECO):
        case (IUG_UNIT_API_CMD_VENTILATION):
            ret_cfm = unt_api_cmd;
            break;
    }
    #endif


    #ifdef IUG_USER_API_CONTROL_HEATER_DEMO_EN
    ret_cfm = USRAPI_heater_get_cmd_confirmation_demo( unt_api_id, unt_api_cmd );
    #endif

    return ret_cfm;
}



