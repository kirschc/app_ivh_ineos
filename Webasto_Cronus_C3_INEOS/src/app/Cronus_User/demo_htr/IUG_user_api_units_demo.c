
#include "IUG_user_api_include.h"



#ifdef IUG_USER_API_CONTROL_HEATER_DEMO_EN
extern iug_usr_api_heater_demo_ctl_t       usrapi_htr_demo_ctl;
#endif




#ifdef IUG_USER_API_CONTROL_HEATER_DEMO_EN
IUG_UNIT_API_COMMAND_t USRAPI_heater_get_cmd_confirmation_demo( const IUG_UNIT_API_UNIT_ID_t unt_api_id,
                                                                const IUG_UNIT_API_COMMAND_t unt_api_cmd )
{
    IUG_UNIT_API_COMMAND_t   ret_cfm;

    ret_cfm = (IUG_UNIT_API_CMD_OFF);

    usrapi_htr_demo_ctl.info_api_cmd_confirm[unt_api_id]++;


    switch( unt_api_id )
    {
        case (IUG_UNIT_API_UNIT_HEATER_HG1):
        case (IUG_UNIT_API_UNIT_HEATER_HG2):
        case (IUG_UNIT_API_UNIT_HEATER_HG3):
        case (IUG_UNIT_API_UNIT_HEATER_HG4):
        case (IUG_UNIT_API_UNIT_HEATER_HG5):
        case (IUG_UNIT_API_UNIT_HEATER_HG6):
        case (IUG_UNIT_API_UNIT_HEATER_HG7):
        case (IUG_UNIT_API_UNIT_HEATER_HG8):
            switch( unt_api_cmd )
            {
                case (IUG_UNIT_API_CMD_HEATING):
                case (IUG_UNIT_API_CMD_BOOST):
                case (IUG_UNIT_API_CMD_ECO):
                case (IUG_UNIT_API_CMD_VENTILATION):
                    if( 0u != usrapi_htr_demo_ctl.htr_ctl[unt_api_id].keep_alive )
                    {
                        ret_cfm = unt_api_cmd;
                    }
                    break;
            }
            break;
        default:
            break;
    }

    return ret_cfm;
}
#endif



