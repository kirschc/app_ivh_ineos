#ifndef __IUG_USER_API_HEATER_DEMO_CFG_H_
#define __IUG_USER_API_HEATER_DEMO_CFG_H_






#ifdef IUG_USER_API_CONTROL_HEATER_DEMO_EN




typedef enum
{
    API_HTR_ID_1st      = 0u,   
    API_HTR_ID_2nd      = 1u,   
    API_HTR_ID_3rd      = 2u,   
    API_HTR_ID_4th      = 3u,   
    API_HTR_ID_5th      = 4u,   
    API_HTR_ID_6th      = 5u,   
    API_HTR_ID_7th      = 6u,   
    API_HTR_ID_8th      = 7u,   
    API_HTR_ID_ENUM_MAX  = 8u   
} API_HTR_ID_t;


typedef struct
{
    IUG_UNIT_API_UNIT_ID_t                  usr_api_unit;
    structure_IUG_UNIT_API_UNIT_CTL_CMD_t   usr_api_cmd;
    IUG_UNIT_API_STATE_t                    usr_api_state;
    IUG_UNIT_API_RETVAL_t                   usr_api_ret;
    IUG_SITE_TARGET_ID_t                    usr_api_tgt_id; 
    uint8_t                                 keep_alive;

} iug_usr_api_heater_demo_ety_t;


#ifdef IUG_USER_API_CONTROL_HEATER_DEMO_HISTORY_SIZE
typedef struct
{
    uint8_t                                 hst_cmd[(IUG_USER_API_CONTROL_HEATER_DEMO_HISTORY_SIZE)];
    uint8_t                                 hst_ret[(IUG_USER_API_CONTROL_HEATER_DEMO_HISTORY_SIZE)];
    uint8_t                                 hst_sta[(IUG_USER_API_CONTROL_HEATER_DEMO_HISTORY_SIZE)];
    uint8_t                                 hst_cmd_old;
    uint8_t                                 hst_ret_old;
    uint8_t                                 hst_sta_old;
} iug_usr_api_heater_demo_history;
#endif


typedef struct
{
    uint16_t                                found_htr_available_mask;   
    uint16_t                                aply__htr_selection_mask;   
    IUG_UNIT_API_COMMAND_t                  aply_cmd_api_cmd;           
    uint8_t                                 aply_cmd_ACTIVATION_PERIOD; 
    uint8_t                                 aply_cmd_SETPOINT;          
    uint8_t                                 aply_cmd_cyclically;
    IUG_UNIT_API_RETVAL_t                   info_api_ret_last;          
    IUG_UNIT_API_COMMAND_t                  info_api_cmd_last;          
    uint8_t                                 info_api_cmd___event[(API_HTR_ID_ENUM_MAX)];
    uint8_t                                 info_api_cmd_confirm[(API_HTR_ID_ENUM_MAX)];

    #ifdef IUG_USER_API_CONTROL_HEATER_DEMO_HISTORY_SIZE
    iug_usr_api_heater_demo_history         history;
    #endif

    IUG_UNIT_API_STATE_t                    htr_state[(API_HTR_ID_ENUM_MAX)];
    iug_usr_api_heater_demo_ety_t           htr_ctl[(API_HTR_ID_ENUM_MAX)];

    uint8_t                                 ghi_com_dev_webpar_max;     

    struct
    {
        structure_API_ERROR_HANDLER_DTC_ERROR_INFO_t    api_dtc_info;       
        enum_API_ERROR_HANDLER_DTC_ERROR_STATES         ret_val;            
    } api_dtc_info;

} iug_usr_api_heater_demo_ctl_t;




void demo_api_ctl_htr_cyclic( void );
void demo_api_ctl_htr_init( void );




#endif 

#endif 




