
#include "IUG_user_api_include.h"



#ifdef IUG_USER_API_CONTROL_HEATER_DEMO_EN


iug_usr_api_heater_demo_ctl_t       usrapi_htr_demo_ctl;

#endif 


#ifdef IUG_USER_API_CONTROL_HEATER_DEMO_EN

void demo_api_ctl_htr_cyclic( void )
{

    #ifdef IUG_USER_API_CONTROL_HEATER_DEMO_EN
    #endif

    uint16_t                htr_id_msk;
    uint16_t                htr_cur_msk;
    IUG_UNIT_API_UNIT_ID_t  unt_id;
    API_HTR_ID_t            htr_id;
    IUG_UNIT_API_HEATER_PROPERTY_t  htr_pty_hgX;
    iug_usr_api_heater_demo_ety_t   *p_dem_ctl;
    structure_API_ERROR_HANDLER_DTC_ERROR_INFO_t    dtc_info;
    #ifdef IUG_USER_API_CONTROL_HEATER_DEMO_HISTORY_SIZE
    iug_usr_api_heater_demo_history *p_hst_upd;
    iug_usr_api_heater_demo_history *p_dem_hst;
    uint16_t                        hst_htr_msk;
    #endif

    static uint8_t  initialized = 0u;

    #ifdef IUG_USER_API_UNITS_DATA_SUPPORT_DEMO_EN
    demo_api_units_data_support_cyclic();
    #endif


    usrapi_htr_demo_ctl.found_htr_available_mask &= 0x00FFu;
    usrapi_htr_demo_ctl.aply__htr_selection_mask &= 0x00FFu;

    for( unt_id = (IUG_UNIT_API_UNIT_HEATER_HG1), htr_id = (API_HTR_ID_1st); 
         unt_id <= (IUG_UNIT_API_UNIT_HEATER_HG8);
         unt_id++, htr_id++ )
    {
        p_dem_ctl   = &usrapi_htr_demo_ctl.htr_ctl[htr_id];
        htr_id_msk  = 0x01u << htr_id;
        htr_cur_msk = 0u;
        #ifdef IUG_USER_API_CONTROL_HEATER_DEMO_HISTORY_SIZE
        p_dem_hst   = &usrapi_htr_demo_ctl.history;
        p_hst_upd   = (NULL);
        hst_htr_msk = 0u;
        #endif

        p_dem_ctl->usr_api_unit = unt_id;
        if( 0x5Au != initialized )
        {
            if( 0u == p_dem_ctl->usr_api_cmd.ACTIVATION_PERIOD )
            {
                p_dem_ctl->usr_api_cmd.ACTIVATION_PERIOD = 10u + htr_id;
            }
            if( 0u == p_dem_ctl->usr_api_cmd.SETPOINT )
            {
                p_dem_ctl->usr_api_cmd.SETPOINT = 50u + htr_id;
            }

            usrapi_htr_demo_ctl.aply_cmd_ACTIVATION_PERIOD = 0u;
            usrapi_htr_demo_ctl.aply_cmd_SETPOINT          = 0u;
        }


        (void)API_units_get_heater_property( p_dem_ctl->usr_api_unit, &htr_pty_hgX );

        if( 0u != htr_pty_hgX.unit_available )
        {
            usrapi_htr_demo_ctl.found_htr_available_mask |= htr_id_msk;
        }
        else
        {
            usrapi_htr_demo_ctl.found_htr_available_mask &= ~(htr_id_msk);
            usrapi_htr_demo_ctl.aply__htr_selection_mask &= ~(htr_id_msk);
        }

        if( usrapi_htr_demo_ctl.aply__htr_selection_mask & htr_id_msk )
        {
            if( 0u != htr_pty_hgX.unit_available )
            {
                htr_cur_msk = htr_id_msk;
            }

            #ifdef IUG_USER_API_CONTROL_HEATER_DEMO_HISTORY_SIZE
            if( 0u == (usrapi_htr_demo_ctl.aply__htr_selection_mask & ~(htr_id_msk)) )
            {
                hst_htr_msk = htr_cur_msk;
            }
            #endif

            if( (IUG_UNIT_API_CMD_ENUM_MAX) != usrapi_htr_demo_ctl.aply_cmd_api_cmd )
            {
                p_dem_ctl->usr_api_cmd.cmd            = usrapi_htr_demo_ctl.aply_cmd_api_cmd;
                usrapi_htr_demo_ctl.info_api_cmd_last = usrapi_htr_demo_ctl.aply_cmd_api_cmd;

                if( 0u != usrapi_htr_demo_ctl.aply_cmd_ACTIVATION_PERIOD )
                {
                    p_dem_ctl->usr_api_cmd.ACTIVATION_PERIOD = usrapi_htr_demo_ctl.aply_cmd_ACTIVATION_PERIOD;
                }

                if( 0u != usrapi_htr_demo_ctl.aply_cmd_SETPOINT )
                {
                    p_dem_ctl->usr_api_cmd.SETPOINT = usrapi_htr_demo_ctl.aply_cmd_SETPOINT;
                }
            }
        }


        if( 0u != htr_cur_msk )
        {
            if( ((IUG_UNIT_API_CMD_ENUM_MAX) != p_dem_ctl->usr_api_cmd.cmd) ||
                ( 0u                         != usrapi_htr_demo_ctl.aply_cmd_cyclically) ) 
            {
                #ifdef IUG_USER_API_CONTROL_HEATER_DEMO_HISTORY_SIZE
                if( 0u != hst_htr_msk )
                {
                    if( p_dem_ctl->usr_api_cmd.cmd != p_dem_hst->hst_cmd_old )
                    {
                        p_hst_upd = p_dem_hst;
                    }
                }
                #endif

                p_dem_ctl->usr_api_ret                = API_units_set_unit_cmd( p_dem_ctl->usr_api_unit, p_dem_ctl->usr_api_cmd );
                usrapi_htr_demo_ctl.info_api_ret_last = p_dem_ctl->usr_api_ret;

                #ifdef IUG_USER_API_CONTROL_HEATER_DEMO_HISTORY_SIZE                
                if( 0u != hst_htr_msk )
                {
                    if( p_dem_ctl->usr_api_ret != p_dem_hst->hst_ret_old )
                    {
                        p_hst_upd = p_dem_hst;
                    }
                }
                #endif

                if( (IUG_UNIT_API_RETVAL_OK) == p_dem_ctl->usr_api_ret )
                {
                    if( ((IUG_UNIT_API_CMD_OFF)  != p_dem_ctl->usr_api_cmd.cmd) &&
                        ((IUG_UNIT_API_CMD_NONE) != p_dem_ctl->usr_api_cmd.cmd) )
                    {
                        p_dem_ctl->keep_alive = 1u;
                    }
                }

                if( (IUG_UNIT_API_CMD_OFF) == p_dem_ctl->usr_api_cmd.cmd )
                {
                    p_dem_ctl->keep_alive = 0u;
                }
            }
        } 

        if( 0u != htr_pty_hgX.unit_available )
        {
            p_dem_ctl->usr_api_tgt_id = htr_pty_hgX.unit_target_id;

            (void)API_units_get_unit_state(  p_dem_ctl->usr_api_unit, &(p_dem_ctl->usr_api_state) );

            #ifdef IUG_USER_API_CONTROL_HEATER_DEMO_HISTORY_SIZE                
            if( 0u != hst_htr_msk )
            {
                if( p_dem_ctl->usr_api_state != p_dem_hst->hst_sta_old )
                {
                    p_hst_upd = p_dem_hst;
                }
            }
            #endif

            usrapi_htr_demo_ctl.htr_state[htr_id] = p_dem_ctl->usr_api_state;
        }

        #ifdef IUG_USER_API_CONTROL_HEATER_DEMO_HISTORY_SIZE
        if( (NULL) != p_hst_upd )
        {
            memmove( &p_dem_hst->hst_cmd[1u], &p_dem_hst->hst_cmd[0u], ((IUG_USER_API_CONTROL_HEATER_DEMO_HISTORY_SIZE) - 1u));
            memmove( &p_dem_hst->hst_ret[1u], &p_dem_hst->hst_ret[0u], ((IUG_USER_API_CONTROL_HEATER_DEMO_HISTORY_SIZE) - 1u));
            memmove( &p_dem_hst->hst_sta[1u], &p_dem_hst->hst_sta[0u], ((IUG_USER_API_CONTROL_HEATER_DEMO_HISTORY_SIZE) - 1u));

            p_dem_hst->hst_cmd[0u] = p_dem_ctl->usr_api_cmd.cmd;
            p_dem_hst->hst_ret[0u] = p_dem_ctl->usr_api_ret;
            p_dem_hst->hst_sta[0u] = p_dem_ctl->usr_api_state;

            p_dem_hst->hst_cmd_old = p_dem_ctl->usr_api_cmd.cmd;
            p_dem_hst->hst_ret_old = p_dem_ctl->usr_api_ret;
            p_dem_hst->hst_sta_old = p_dem_ctl->usr_api_state;
        }
        #endif

        p_dem_ctl->usr_api_cmd.cmd = (IUG_UNIT_API_CMD_ENUM_MAX);

    } 



    if( 0u == usrapi_htr_demo_ctl.aply_cmd_cyclically )
    {
        usrapi_htr_demo_ctl.aply_cmd_api_cmd = (IUG_UNIT_API_CMD_ENUM_MAX);
        #ifdef IUG_USER_API_CONTROL_HEATER_DEMO_HISTORY_SIZE
        p_dem_hst->hst_cmd_old               = (IUG_UNIT_API_CMD_ENUM_MAX);
        #endif
    }

    usrapi_htr_demo_ctl.api_dtc_info.ret_val = API_error_handler_get_dtc( usrapi_htr_demo_ctl.api_dtc_info.api_dtc_info.request__dtc_index , &dtc_info );
    usrapi_htr_demo_ctl.api_dtc_info.api_dtc_info.request__dtc_index   = dtc_info.request__dtc_index;
    usrapi_htr_demo_ctl.api_dtc_info.api_dtc_info.response_dtc_active  = dtc_info.response_dtc_active;
    usrapi_htr_demo_ctl.api_dtc_info.api_dtc_info.response_dtc_symptom = dtc_info.response_dtc_symptom;

    initialized = 0x5Au;
}


void demo_api_ctl_htr_init( void )
{

    #ifdef IUG_USER_API_CONTROL_HEATER_DEMO_EN
    #endif

    API_HTR_ID_t            htr_id;

    usrapi_htr_demo_ctl.aply_cmd_api_cmd = (IUG_UNIT_API_CMD_ENUM_MAX);

    for( htr_id = (API_HTR_ID_1st); htr_id < (API_HTR_ID_ENUM_MAX); htr_id++ )
    {
        usrapi_htr_demo_ctl.htr_ctl[htr_id].usr_api_cmd.cmd = (IUG_UNIT_API_CMD_ENUM_MAX);
    }

}


#ifdef IUG_USER_API_CONTROL_HEATER_DEMO_EN
    #warning "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
    #warning "Enable 'IUG_USER_API_CONTROL_HEATER_DEMO_EN' for development/debugging only!!!".
    #warning "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"

    #ifdef IUG_USER_API_CONTROL_HEATER_DEMO_HISTORY_SIZE
        #warning "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
        #warning "Enable 'IUG_USER_API_CONTROL_HEATER_DEMO_HISTORY_SIZE' for development/debugging only!!!".
        #warning "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
    #endif
#endif

#endif 



