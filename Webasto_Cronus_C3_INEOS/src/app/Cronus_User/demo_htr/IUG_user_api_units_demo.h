#ifndef _IUG_USER_API_UNITS_DEMO_H_
#define _IUG_USER_API_UNITS_DEMO_H_






#ifdef IUG_USER_API_CONTROL_HEATER_DEMO_EN
IUG_UNIT_API_COMMAND_t USRAPI_heater_get_cmd_confirmation_demo( const IUG_UNIT_API_UNIT_ID_t unt_api_id,
                                                                const IUG_UNIT_API_COMMAND_t unt_api_cmd );
#endif


#endif 

