
#include "IUG_user_api_include.h"



uint8_t API_callback_03_FahrzeugZustandserkennung_X1_9_KL15( void )
{
    uint8_t car_state;
    uint16_t val_KL15_mV;
    enum_API_IO_CTRL_ERROR_STATES ret_err;

    ret_err = API_IO_ctrl_get_ana_input(API_IO_CTRL_AI_KL15, &val_KL15_mV);

    if( (ret_err     == API_IO_CTRL_ERR_NONE)                &&
        (val_KL15_mV >= (IUG_API_CB_CFG_CAR_DET_KL15_LVL_MV))  )
    {
        car_state = (TRUE);
    }
    else
    {
        car_state = (FALSE);
    }

    return car_state;
}

uint8_t API_callback_03_FahrzeugZustandserkennung_X1_3_LIN1_PWM_IN( void )
{
    uint8_t  edge_flg;
    uint8_t  car_active_state_timer_flg;
    uint8_t  status_fzg_climate_timer_flg;
    uint8_t  edge_changed_flg;
    uint16_t ai_temp_val_mV;
    enum_API_IO_CTRL_STATES  temp_state_of_do_relais_lin;
    static uint8_t car_state = (FALSE);
    static uint8_t last_digital_pin_value = (TRUE);
    static uint32_t car_active_state_timer = 0x00u;
    static uint32_t status_fzg_climate_timer = 0x00u;
    static enum_API_IO_CTRL_STATES last_status_of_relais_out = API_IO_CTRL_STATE_STATIC_OFF;

    edge_flg = (FALSE);
    edge_changed_flg = (FALSE);
    temp_state_of_do_relais_lin = API_IO_CTRL_STATE_STATIC_OFF;

    (void)API_IO_ctrl_get_ana_input(API_IO_CTRL_AI_PWM_IN, &ai_temp_val_mV);

    if( ai_temp_val_mV <= (IUG_API_CB_CFG_CAR_DET_LIN1_LVL_LOW_MV)  )
    {
        edge_flg = (FALSE);
    }
    else if( ai_temp_val_mV >= (IUG_API_CB_CFG_CAR_DET_LIN1_LVL_HIGH_MV)  )
    {
        edge_flg = (TRUE);
    }
    else
    {
        edge_flg = last_digital_pin_value;
    }

    if( last_digital_pin_value != edge_flg )
    {
        edge_changed_flg = (TRUE);

        last_digital_pin_value = edge_flg;

        (void)API_utilities_timer_set_timestamp(&car_active_state_timer, API_UTILITIES_TIMER_PRECISION_1MS);
    }
    else
    {
        edge_changed_flg = (FALSE);
    }

    (void)API_IO_ctrl_get_dig_output(API_IO_CTRL_DO_RELAIS_LIN, &temp_state_of_do_relais_lin);

    if( temp_state_of_do_relais_lin == last_status_of_relais_out )
    {
        (void)API_utilities_timer_check_timestamp(&status_fzg_climate_timer_flg, status_fzg_climate_timer, 100u , API_UTILITIES_TIMER_PRECISION_1MS);

        if( status_fzg_climate_timer_flg != (FALSE) )
        {
            (void)API_utilities_timer_check_timestamp(&car_active_state_timer_flg, car_active_state_timer, 2000u , API_UTILITIES_TIMER_PRECISION_1MS);

            if( edge_changed_flg != (FALSE) )
            {
                car_state = (TRUE);
            }
            else
            {
                if( car_active_state_timer_flg == (TRUE) )
                {
                    car_state = (FALSE);
                }
                else
                {
                }
            }
        }
        else
        {
        }
    }
    else 
    {
        (void)API_utilities_timer_set_timestamp(&status_fzg_climate_timer, API_UTILITIES_TIMER_PRECISION_1MS);

        last_status_of_relais_out = temp_state_of_do_relais_lin;
    }

    return car_state;
}

IUG_UNIT_API_COMMAND_t API_callback_27_AutomatischesHeizenLueften_Decide( const IUG_UNIT_API_PHYS_TEMP_1_1DEG_T cfg_level_auto_heat_vent, \
                                                                          const IUG_UNIT_API_PHYS_TEMP_1_1DEG_T room_temp )
{
    IUG_UNIT_API_COMMAND_t          ret_val;
#ifdef DPL
    IUG_UNIT_API_HEATER_PROPERTY_t  htr_pty_hg1;
    IUG_UNIT_API_HEATER_PROPERTY_t  htr_pty_hg2;

    ret_val = (IUG_UNIT_API_CMD_NONE);
    API_units_get_heater_property((IUG_UNIT_API_UNIT_HEATER_HG1), &htr_pty_hg1);
    API_units_get_heater_property((IUG_UNIT_API_UNIT_HEATER_HG2), &htr_pty_hg2);

    if( ((0u != htr_pty_hg1.unit_available) && ((IUG_UNIT_API_HEATER_TYPE_WAT) == htr_pty_hg1.unit_htr_typ) ) ||
        ((0u != htr_pty_hg2.unit_available) && ((IUG_UNIT_API_HEATER_TYPE_WAT) == htr_pty_hg2.unit_htr_typ) ) )
    {
        ret_val = (IUG_UNIT_API_CMD_HEATING);
        if( room_temp >= cfg_level_auto_heat_vent )
        {
            ret_val = (IUG_UNIT_API_CMD_VENTILATION);
        }
    }
#endif

    ret_val = (IUG_UNIT_API_CMD_HEATING);

    if( room_temp >= cfg_level_auto_heat_vent )
    {
        ret_val = (IUG_UNIT_API_CMD_VENTILATION);
    }
    else
    {
    }

    return ret_val;
}

void API_callback_31_MV_Ansteuerung( void )
{
}

void API_callback_32_UP_Ansteuerung( void )
{
}



