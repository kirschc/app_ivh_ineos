#ifndef __IUG_USER_API_UNITS_DATA_SUPPORT_DEMO_CFG_H_
#define __IUG_USER_API_UNITS_DATA_SUPPORT_DEMO_CFG_H_


#define API_UNITS_DATA_SUPPORT_DEMO_BUFFER_SZE              258U        





typedef struct
{
    struct
    {
        uint16_t                        request_api_49_dat_sel;     

        uint16_t                        request_api_51_dat_sel;     

        #ifdef IUG_UNIT_API_UNIT_DATA_RDWR_VIA_BUFFER_EN
        uint8_t                         request_buf_service;        

        uint8_t                         request_buf_service_sub;    
        #endif

        IUG_UNIT_API_UNIT_ID_t          request_api_unit_id;        

        uint8_t                         cyc_demo_enable;            

    } user_cmd;

    API_UNITS_DATA_SUPPORT_ID_t             api_dat_sup_trg;		
    API_UNITS_DATA_SUPPORT_ID_t             api_dat_sup_get;		

    IUG_UNIT_API_RETVAL_t                   uds_ret;                
    API_UNITS_DATA_SUPPORT_PARAMETER_t      uds_param;				

    #ifdef IUG_UNIT_API_UNIT_DATA_RDWR_VIA_BUFFER_EN
    uint8_t     buf_req[(IUG_QUEUE_DATBUF_SIZE_RD_ID_MSG)]; 
    #endif
    uint8_t     buf_rsp[(API_UNITS_DATA_SUPPORT_DEMO_BUFFER_SZE)];  

    struct
    {
        structure_API_ERROR_HANDLER_DTC_ERROR_INFO_t    api_dtc_info;       
        enum_API_ERROR_HANDLER_DTC_ERROR_STATES         ret_val;            
    } api_dtc_info;

    int16_t                         TEMP_raw;
    uint16_t                        RESISTOR_VAL_raw;
    uint16_t                        RESISTOR_SETPOINT;
    uint8_t                         RELAIS_STATE_raw;
    uint8_t                         RELAIS_SETPOINT;
    uint8_t                         RESISTOR_120_kOhm_STATE_raw;
    uint8_t                         RESISTOR_120_kOhm_SETPOINT;

    uint16_t                        cyc_demo_cnt;
    API_UNITS_DATA_SUPPORT_ID_t     cyc_demo_unit_data_id;
    API_UNITS_DATA_SUPPORT_ID_t     cyc_demo_api_dat_sup_get;

} unit_data_support_demo_t;

typedef struct
{
    struct
    {
        uint16_t                        request_api_49_dat_sel;     

        uint16_t                        request_api_51_dat_sel;     


        IUG_UNIT_API_UNIT_ID_t          request_api_unit_id;        


    } user_cmd;

    uint16_t                            resistor_setpoint;
    uint8_t                             relais_setpoint;
    uint8_t                             resistor_120kOhm_setpoint;
} untdatsup_demo_can_control_data_t;

typedef struct
{
    IUG_UNIT_API_UNIT_ID_t              api_unit_id;
    int16_t                             temp_val_raw;
    uint16_t                            resistor_val_raw;
    uint8_t                             relais_val_raw;
    uint8_t                             resistor_120kOhm_val_raw;
    uint8_t                             *buf_rsp_val;  
    uint16_t                            buf_rsp_sze_val;
} untdatsup_demo_can_evaluate_data_t;



void demo_api_units_data_support_cyclic( void );
void demo_api_units_data_support_control_by_can(untdatsup_demo_can_control_data_t can_control_data);
void demo_api_units_data_support_evaluate_by_can(untdatsup_demo_can_evaluate_data_t* can_evaluate_data);

#endif 




