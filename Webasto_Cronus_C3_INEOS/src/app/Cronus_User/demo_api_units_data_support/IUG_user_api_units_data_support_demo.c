
#include "IUG_include.h"





unit_data_support_demo_t      untdatsup_demo;



void demo_api_units_data_support_cyclic( void )
{

    structure_API_ERROR_HANDLER_DTC_ERROR_INFO_t    dtc_info;

    if(0u != untdatsup_demo.user_cmd.request_api_49_dat_sel )
    {
        #ifdef IUG_UNIT_API_UNIT_DATA_RDWR_VIA_BUFFER_EN
        untdatsup_demo.uds_param.unit_data_storage.buf_req     = (NULL);
        untdatsup_demo.uds_param.unit_data_storage.buf_req_sze = 0u;
        #endif
        untdatsup_demo.uds_param.unit_data_storage.buf_rsp     = &untdatsup_demo.buf_rsp[0u];
        untdatsup_demo.uds_param.unit_data_storage.buf_rsp_sze = 0u; 
        untdatsup_demo.uds_param.unit_data_storage.buf_rsp_max = sizeof( untdatsup_demo.buf_rsp );


        #ifdef IUG_UNIT_API_UNIT_DATA_RDWR_VIA_BUFFER_EN
        memset( untdatsup_demo.uds_param.unit_data_storage.buf_req, 0u, sizeof( untdatsup_demo.buf_req ) );
        #endif
        memset( untdatsup_demo.uds_param.unit_data_storage.buf_rsp, 0u, sizeof( untdatsup_demo.buf_rsp ) );

        switch( untdatsup_demo.user_cmd.request_api_49_dat_sel )
        {
            case (0x0001u): untdatsup_demo.api_dat_sup_trg = (API_UNITS_DATA_SUPPORT_TLINR_RD_TEMP_SEN_VALUE);    break;
            case (0x0002u): untdatsup_demo.api_dat_sup_trg = (API_UNITS_DATA_SUPPORT_TLINR_RD_RESISTOR_VALUE);    break;
            case (0x0004u): untdatsup_demo.api_dat_sup_trg = (API_UNITS_DATA_SUPPORT_TLINR_WR_RESISTOR_VALUE);    break;
            case (0x0008u): untdatsup_demo.api_dat_sup_trg = (API_UNITS_DATA_SUPPORT_TLINR_RD_RELAIS_STATE);      break;
            case (0x0010u): untdatsup_demo.api_dat_sup_trg = (API_UNITS_DATA_SUPPORT_TLINR_WR_RELAIS_STATE);      break;
            case (0x0020u): untdatsup_demo.api_dat_sup_trg = (API_UNITS_DATA_SUPPORT_TLINR_RD_120_kOHM_STATE);    break;
            case (0x0040u): untdatsup_demo.api_dat_sup_trg = (API_UNITS_DATA_SUPPORT_TLINR_WR_120_kOHM_STATE);    break;
            #ifdef IUG_UNIT_API_UNIT_DATA_RDWR_VIA_BUFFER_EN
            case (0x8000u):
                if( 0x00u == untdatsup_demo.user_cmd.request_buf_service )
                {
                    untdatsup_demo.user_cmd.request_buf_service     = 0x30u; 
                    untdatsup_demo.user_cmd.request_buf_service_sub = (API_UNITS_DATA_SUPPORT_SRV_TLINR_RD_TEMP_SEN_VALUE); 
                }
                untdatsup_demo.api_dat_sup_trg = (API_UNITS_DATA_SUPPORT_RDWR_VIA_BUFFER);
                untdatsup_demo.buf_req[0u] = 0x49u;    
                untdatsup_demo.buf_req[1u] = untdatsup_demo.user_cmd.request_buf_service;
                untdatsup_demo.buf_req[2u] = untdatsup_demo.user_cmd.request_buf_service_sub;
                untdatsup_demo.buf_req[3u] = 0x00u; 

                #if 0 
                untdatsup_demo.buf_req[0u] = 0x51u;    
                untdatsup_demo.buf_req[1u] = 0x30u;
                untdatsup_demo.buf_req[2u] = 2u;
                untdatsup_demo.buf_req[3u] = 3u;
                untdatsup_demo.buf_req[4u] = 5u;
                untdatsup_demo.buf_req[5u] = 7u;
                untdatsup_demo.buf_req[6u] = 0x00u; 
                #endif

                #ifdef IUG_UNIT_API_UNIT_DATA_RDWR_VIA_BUFFER_EN
                untdatsup_demo.uds_param.unit_data_storage.buf_req     = &untdatsup_demo.buf_req[0u];
                untdatsup_demo.uds_param.unit_data_storage.buf_req_sze = strlen( (const char *)untdatsup_demo.buf_req );
                #endif
                break;
            #else
            case (0x8000u):
                untdatsup_demo.api_dat_sup_trg = (API_UNITS_DATA_SUPPORT_ENUM_MAX);
                break;
            #endif
            default:
                untdatsup_demo.api_dat_sup_trg = (API_UNITS_DATA_SUPPORT_ENUM_MAX);
                break;
        }
        untdatsup_demo.user_cmd.request_api_49_dat_sel = 0u;
    }
    if(0u != untdatsup_demo.user_cmd.request_api_51_dat_sel )
    {
        #ifdef IUG_UNIT_API_UNIT_DATA_RDWR_VIA_BUFFER_EN
        untdatsup_demo.uds_param.unit_data_storage.buf_req     = (NULL);
        untdatsup_demo.uds_param.unit_data_storage.buf_req_sze = 0u;
        #endif
        untdatsup_demo.uds_param.unit_data_storage.buf_rsp     = &untdatsup_demo.buf_rsp[0u];
        untdatsup_demo.uds_param.unit_data_storage.buf_rsp_sze = 0u; 
        untdatsup_demo.uds_param.unit_data_storage.buf_rsp_max = sizeof( untdatsup_demo.buf_rsp );

        switch( untdatsup_demo.user_cmd.request_api_51_dat_sel )
        {
            case (0x0001u): untdatsup_demo.api_dat_sup_trg = (API_UNITS_DATA_SUPPORT_RD_ID_WBUS_SPEC_VERSION);    break;
            case (0x0002u): untdatsup_demo.api_dat_sup_trg = (API_UNITS_DATA_SUPPORT_RD_ID_ECU_ID_NUMBER);        break;
            case (0x0004u): untdatsup_demo.api_dat_sup_trg = (API_UNITS_DATA_SUPPORT_RD_ID_HW_DATE);              break;
            case (0x0008u): untdatsup_demo.api_dat_sup_trg = (API_UNITS_DATA_SUPPORT_RD_ID_SW_DATE);              break;
            case (0x0010u): untdatsup_demo.api_dat_sup_trg = (API_UNITS_DATA_SUPPORT_RD_ID_SW_VERSION);           break;
            case (0x0020u): untdatsup_demo.api_dat_sup_trg = (API_UNITS_DATA_SUPPORT_RD_ID_ECU_MANUF_DATE);       break;
            case (0x0040u): untdatsup_demo.api_dat_sup_trg = (API_UNITS_DATA_SUPPORT_RD_ID_ECU_SERIAL_NUMBER);    break;
            case (0x0080u): untdatsup_demo.api_dat_sup_trg = (API_UNITS_DATA_SUPPORT_RD_ID_SW_ID_NUMBER);         break;
            case (0x0100u): untdatsup_demo.api_dat_sup_trg = (API_UNITS_DATA_SUPPORT_RD_ID_HW_ID_NUMBER);         break;
            #ifdef IUG_UNIT_API_UNIT_DATA_RDWR_VIA_BUFFER_EN
            case (0x8000u):
                untdatsup_demo.api_dat_sup_trg = (API_UNITS_DATA_SUPPORT_RDWR_VIA_BUFFER);
                untdatsup_demo.buf_req[0u] = 0x51u;    
                untdatsup_demo.buf_req[1u] = 0x30u;    
                untdatsup_demo.buf_req[2u] = (API_UNITS_DATA_SUPPORT_SRV_RD_ID_SW_VERSION);
                untdatsup_demo.buf_req[3u] = (API_UNITS_DATA_SUPPORT_SRV_RD_ID_SW_ID_NUMBER);
                untdatsup_demo.buf_req[4u] = (API_UNITS_DATA_SUPPORT_SRV_RD_ID_ECU_MANUF_DATE);
                untdatsup_demo.buf_req[5u] = (API_UNITS_DATA_SUPPORT_SRV_RD_ID_ECU_SERIAL_NUMBER);
                untdatsup_demo.buf_req[6u] = 0x00u; 

                untdatsup_demo.uds_param.unit_data_storage.buf_req     = &untdatsup_demo.buf_req[0u];
                untdatsup_demo.uds_param.unit_data_storage.buf_req_sze = strlen( (const char *)untdatsup_demo.buf_req );
                break;
            #else
            case (0x8000u):
                untdatsup_demo.api_dat_sup_trg = (API_UNITS_DATA_SUPPORT_ENUM_MAX);
                break;
            #endif
            default:
                untdatsup_demo.api_dat_sup_trg = (API_UNITS_DATA_SUPPORT_ENUM_MAX);
                break;
        }
        untdatsup_demo.user_cmd.request_api_51_dat_sel = 0u;
    }

    if( 0u != untdatsup_demo.api_dat_sup_trg )
    {
        untdatsup_demo.uds_param.unit_data_id = untdatsup_demo.api_dat_sup_trg;
        switch( untdatsup_demo.api_dat_sup_trg )
        {
            case (API_UNITS_DATA_SUPPORT_TLINR_WR_RESISTOR_VALUE):
                untdatsup_demo.uds_param.unit_data_storage.data_ui32 = untdatsup_demo.RESISTOR_SETPOINT;
                break;
            case (API_UNITS_DATA_SUPPORT_TLINR_WR_RELAIS_STATE):
                untdatsup_demo.uds_param.unit_data_storage.data_ui32 = untdatsup_demo.RELAIS_SETPOINT;
                break;
            case (API_UNITS_DATA_SUPPORT_TLINR_WR_120_kOHM_STATE):
                untdatsup_demo.uds_param.unit_data_storage.data_ui32 = untdatsup_demo.RESISTOR_120_kOhm_SETPOINT;
                break;
            default: break;
        }
        if( (enum_IUG_UNIT_API_UNIT_ID_t)0u == untdatsup_demo.user_cmd.request_api_unit_id )
        {
            untdatsup_demo.user_cmd.request_api_unit_id = (IUG_UNIT_API_UNIT_TLINR_EXT);
        }
        untdatsup_demo.uds_ret = API_units_data_support_trigger( untdatsup_demo.user_cmd.request_api_unit_id, &untdatsup_demo.uds_param );

        untdatsup_demo.api_dat_sup_get = untdatsup_demo.api_dat_sup_trg;
        untdatsup_demo.api_dat_sup_trg = (API_UNITS_DATA_SUPPORT_UNDEFINED);
    }

    if( 0u != untdatsup_demo.api_dat_sup_get )
    {
        untdatsup_demo.uds_param.unit_data_id = untdatsup_demo.api_dat_sup_get;
        untdatsup_demo.uds_ret = API_units_data_support_status( untdatsup_demo.user_cmd.request_api_unit_id, untdatsup_demo.uds_param.unit_data_id );

        if( (IUG_UNIT_API_RETVAL_DATA_AVAILABLE) == untdatsup_demo.uds_ret )
        {
            switch( untdatsup_demo.api_dat_sup_get )
            {
                case (API_UNITS_DATA_SUPPORT_TLINR_RD_TEMP_SEN_VALUE):
                    untdatsup_demo.TEMP_raw = untdatsup_demo.uds_param.unit_data_storage.data_ui32;
                    break;
                case (API_UNITS_DATA_SUPPORT_TLINR_RD_RESISTOR_VALUE):
                    untdatsup_demo.RESISTOR_VAL_raw = untdatsup_demo.uds_param.unit_data_storage.data_ui32;
                    break;
                case (API_UNITS_DATA_SUPPORT_TLINR_RD_RELAIS_STATE):
                    untdatsup_demo.RELAIS_STATE_raw = untdatsup_demo.uds_param.unit_data_storage.data_ui32;
                    break;
                case (API_UNITS_DATA_SUPPORT_TLINR_RD_120_kOHM_STATE):
                    untdatsup_demo.RESISTOR_120_kOhm_STATE_raw = untdatsup_demo.uds_param.unit_data_storage.data_ui32;
                    break;
                default: break;
            }
            untdatsup_demo.api_dat_sup_get = (API_UNITS_DATA_SUPPORT_UNDEFINED);
        }

    }

    untdatsup_demo.api_dtc_info.ret_val = API_error_handler_get_dtc( untdatsup_demo.api_dtc_info.api_dtc_info.request__dtc_index , &dtc_info );
    untdatsup_demo.api_dtc_info.api_dtc_info.request__dtc_index   = dtc_info.request__dtc_index;
    untdatsup_demo.api_dtc_info.api_dtc_info.response_dtc_active  = dtc_info.response_dtc_active;
    untdatsup_demo.api_dtc_info.api_dtc_info.response_dtc_symptom = dtc_info.response_dtc_symptom;

    if( 0u != untdatsup_demo.user_cmd.cyc_demo_enable )
    {
        if( 0u != untdatsup_demo.cyc_demo_cnt )
        {
            untdatsup_demo.cyc_demo_cnt--;
        }
        else
        {
            untdatsup_demo.cyc_demo_cnt = 10000u / 20u;
        }

        if( 0u == untdatsup_demo.cyc_demo_cnt )
        {
            switch( untdatsup_demo.cyc_demo_unit_data_id )
            {
                case (API_UNITS_DATA_SUPPORT_TLINR_RD_TEMP_SEN_VALUE):
                    untdatsup_demo.cyc_demo_unit_data_id = (API_UNITS_DATA_SUPPORT_TLINR_RD_RESISTOR_VALUE);
                    break;
                case (API_UNITS_DATA_SUPPORT_TLINR_RD_RESISTOR_VALUE):
                    untdatsup_demo.cyc_demo_unit_data_id = (API_UNITS_DATA_SUPPORT_TLINR_RD_RELAIS_STATE);
                    break;
                case (API_UNITS_DATA_SUPPORT_TLINR_RD_RELAIS_STATE):
                    untdatsup_demo.cyc_demo_unit_data_id = (API_UNITS_DATA_SUPPORT_TLINR_RD_TEMP_SEN_VALUE);
                    break;
                default:
                    untdatsup_demo.cyc_demo_unit_data_id = (API_UNITS_DATA_SUPPORT_TLINR_RD_RESISTOR_VALUE);
                    break;
            }

            untdatsup_demo.uds_param.unit_data_id = untdatsup_demo.cyc_demo_unit_data_id;
            if( (enum_IUG_UNIT_API_UNIT_ID_t)0u == untdatsup_demo.user_cmd.request_api_unit_id )
            {
                untdatsup_demo.user_cmd.request_api_unit_id = (IUG_UNIT_API_UNIT_TLINR_EXT);
            }
            untdatsup_demo.uds_ret = API_units_data_support_trigger( untdatsup_demo.user_cmd.request_api_unit_id, &untdatsup_demo.uds_param );

            untdatsup_demo.api_dat_sup_trg = (API_UNITS_DATA_SUPPORT_UNDEFINED);
            untdatsup_demo.api_dat_sup_get = (API_UNITS_DATA_SUPPORT_UNDEFINED);

            if( (IUG_UNIT_API_RETVAL_OK) == untdatsup_demo.uds_ret )
            {
                untdatsup_demo.cyc_demo_api_dat_sup_get = untdatsup_demo.cyc_demo_unit_data_id;
            }
        }

        if( (API_UNITS_DATA_SUPPORT_UNDEFINED) != untdatsup_demo.cyc_demo_api_dat_sup_get )
        {
            untdatsup_demo.uds_ret = API_units_data_support_status( untdatsup_demo.user_cmd.request_api_unit_id, untdatsup_demo.cyc_demo_api_dat_sup_get );

            if( (IUG_UNIT_API_RETVAL_DATA_AVAILABLE) == untdatsup_demo.uds_ret )
            {
                switch( untdatsup_demo.cyc_demo_api_dat_sup_get )
                {
                    case (API_UNITS_DATA_SUPPORT_TLINR_RD_TEMP_SEN_VALUE):
                        untdatsup_demo.TEMP_raw = untdatsup_demo.uds_param.unit_data_storage.data_ui32;
                        break;
                    case (API_UNITS_DATA_SUPPORT_TLINR_RD_RESISTOR_VALUE):
                        untdatsup_demo.RESISTOR_VAL_raw = untdatsup_demo.uds_param.unit_data_storage.data_ui32;
                        break;
                    case (API_UNITS_DATA_SUPPORT_TLINR_RD_RELAIS_STATE):
                        untdatsup_demo.RELAIS_STATE_raw = untdatsup_demo.uds_param.unit_data_storage.data_ui32;
                        break;
                    default: break;
                }

                untdatsup_demo.cyc_demo_api_dat_sup_get = (API_UNITS_DATA_SUPPORT_UNDEFINED);
            }
        }
    }
    else
    {
        untdatsup_demo.cyc_demo_cnt = 1u;
    }    


}

void demo_api_units_data_support_control_by_can(untdatsup_demo_can_control_data_t can_control_data)
{
    untdatsup_demo.RESISTOR_SETPOINT = can_control_data.resistor_setpoint;
    untdatsup_demo.RELAIS_SETPOINT = can_control_data.relais_setpoint;
    untdatsup_demo.RESISTOR_120_kOhm_SETPOINT = can_control_data.resistor_120kOhm_setpoint;

    untdatsup_demo.user_cmd.request_api_unit_id = can_control_data.user_cmd.request_api_unit_id;
    untdatsup_demo.user_cmd.request_api_49_dat_sel = can_control_data.user_cmd.request_api_49_dat_sel;
    untdatsup_demo.user_cmd.request_api_51_dat_sel = can_control_data.user_cmd.request_api_51_dat_sel;
}

void demo_api_units_data_support_evaluate_by_can(untdatsup_demo_can_evaluate_data_t* can_evaluate_data)
{
    can_evaluate_data->api_unit_id = untdatsup_demo.user_cmd.request_api_unit_id;
    can_evaluate_data->temp_val_raw = untdatsup_demo.TEMP_raw;
    can_evaluate_data->resistor_val_raw = untdatsup_demo.RESISTOR_VAL_raw;
    can_evaluate_data->relais_val_raw = untdatsup_demo.RELAIS_STATE_raw;
    can_evaluate_data->resistor_120kOhm_val_raw = untdatsup_demo.RESISTOR_120_kOhm_STATE_raw;
    can_evaluate_data->buf_rsp_val = &untdatsup_demo.buf_rsp[0u];
    can_evaluate_data->buf_rsp_sze_val = untdatsup_demo.uds_param.unit_data_storage.buf_rsp_sze;

}

#ifdef IUG_USER_API_UNITS_DATA_SUPPORT_DEMO_EN
    #warning "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
    #warning "Enable 'IUG_USER_API_UNITS_DATA_SUPPORT_DEMO_EN' for development/debugging only!!!".
    #warning "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
#endif



