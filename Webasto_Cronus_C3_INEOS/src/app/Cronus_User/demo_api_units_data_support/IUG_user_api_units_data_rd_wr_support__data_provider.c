
#include "IUG_include.h"
#include "IUG_utility_unit_data_rd_wr_support__include.h"
#include "IUG_user_api_units_data_rd_wr_support__include.h"

#ifdef IUG_USER_API_GHI_HEATER_DEMO_EN
extern iug_usr_api_ghi_heater_demo_ctl_t       usrapi_ghi_htr_demo_ctl;
#endif




#ifdef API_UNITS_DATA_RD_WR_SUPPORT_PROVIDE_DATA_FUNC
IUG_UNIT_API_RETVAL_t API_units_data_rdwr_support__provide_data( 
                                const IUG_UNIT_API_UNIT_ID_t                unit_id_api,
                                API_UNITS_DATA_RDWRSUP_GET_DESCRIPTION_t    data_descrp,
                                uint32_t    * const p_data,
                                uint16_t    data_size_max )
{
    uint16_t    src_sze_ary;
    uint32_t    src_integral;
    uint8_t     *p_src_ary;
    enum_IUG_UNIT_API_RETVAL_t  ret_val;
    #ifdef IUG_USER_API_GHI_HEATER_DEMO_EN
    IUG_UNIT_API_UNIT_ID_t      ghi_htr_idx;
    iug_usr_api_ghi_heater_demo_ety_t   *p_htr_ctl;
    #endif

    ret_val = (IUG_UNIT_API_RETVAL_ERR_UNIT_ID);
    if( ((IUG_UNIT_API_UNIT_HEATER_HG1) <= unit_id_api) &&
        ((IUG_UNIT_API_UNIT_HEATER_HG8) >= unit_id_api) )
    {
        #ifdef IUG_USER_API_GHI_HEATER_DEMO_EN
        ghi_htr_idx = (IUG_UNIT_API_UNIT_ID_t)(unit_id_api - (IUG_UNIT_API_UNIT_HEATER_HG1));

        p_htr_ctl   = &usrapi_ghi_htr_demo_ctl.htr_ctl[ghi_htr_idx];
        #endif


        #if 0 
        if( (IUG_UNIT_API_UNIT_HEATER_HG1) == unit_id_api )
        {
            ret_val = (IUG_UNIT_API_RETVAL_OK);
        }
        #else
        ret_val = (IUG_UNIT_API_RETVAL_OK);
        #endif
    }

    if( (IUG_UNIT_API_RETVAL_OK) != ret_val )
    {
        return ret_val;
    }

    src_sze_ary  = data_size_max;
    p_src_ary    = (NULL);
    src_integral = 0u;

    switch( data_descrp.topic_id )
    {
        case (API_UNITS_DATA_RDWRSUP_STATUS):
            switch( data_descrp.data_id.id )
            {
                case (API_UNITS_DATA_RDWRSUP_ST_OUTPORT_HEATER):        src_integral = 0u;  break;
                case (API_UNITS_DATA_RDWRSUP_ST_COOL_MEDIUM_TEMP1):     src_integral = 0u;  break;
                case (API_UNITS_DATA_RDWRSUP_ST_SUPPLY_VOLTAGE):        src_integral =  12000u;  break;
                #ifdef IUG_USER_API_GHI_HEATER_DEMO_EN
                case (API_UNITS_DATA_RDWRSUP_ST_STATE_NUM_1BYTE):       src_integral = p_htr_ctl->htr_state_sub;        break;
                case (API_UNITS_DATA_RDWRSUP_ST_STATE_NUM_2BYTE):       src_integral = p_htr_ctl->htr_state_sub << 8u;  break;
                #else
                case (API_UNITS_DATA_RDWRSUP_ST_STATE_NUM_1BYTE):       src_integral =   0x71u;  break;
                case (API_UNITS_DATA_RDWRSUP_ST_STATE_NUM_2BYTE):       src_integral = 0x7100u;  break;
                #endif
                case (API_UNITS_DATA_RDWRSUP_ST_STATE_STFL_FAULTY):     src_integral = 0u;  break;
                case (API_UNITS_DATA_RDWRSUP_ST_STATE_UEHFL_OVERHEAT):  src_integral = 0u;  break;
                case (API_UNITS_DATA_RDWRSUP_ST_STATE_HGV_LOCKED):      src_integral = 0u;  break;
                case (API_UNITS_DATA_RDWRSUP_ST_AIR_PRESSURE):          src_integral = 1013u;  break;
                default: ret_val = (IUG_UNIT_API_RETVAL_DATA_INVALID);  break;
            }
            break;
        case (API_UNITS_DATA_RDWRSUP_IDENT):
            switch( data_descrp.data_id.id )
            {
                case (API_UNITS_DATA_RDWRSUP_ID_WBUS_SPEC_VERSION):     src_integral =   0x43u;  break;
                case (API_UNITS_DATA_RDWRSUP_ID_ECU_ID_NUMBER):
                    p_src_ary = (uint8_t *)&"0123456789012345678901234567689";
                    break;
                case (API_UNITS_DATA_RDWRSUP_ID_SW_VERSION):
                    p_src_ary = (uint8_t *)&"0123456789012345678901234567689";
                    break;
                default:
                    ret_val = (IUG_UNIT_API_RETVAL_DATA_INVALID);
                    break;
            }
            break;
        default:
            ret_val = (IUG_UNIT_API_RETVAL_DATA_INVALID);
            break;
    }

    if( (NULL) != p_src_ary )
    {
        memcpy( p_data, p_src_ary, src_sze_ary );
    }
    else
    {
        *p_data = src_integral;
    }

    return ret_val;
}
#endif 




