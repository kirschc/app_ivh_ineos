
#include "IUG_user_api_include.h"



#ifdef IUG_USER_API_USER_DEVICE_ENHANCED_DEMO_EN
typedef struct
{
    uint8_t     udv_enabled[(IUG_USER_DEVICE_MAX)];      
    uint8_t     udv_active[(IUG_USER_DEVICE_MAX)];       
    uint8_t     udv_keep_alive[(IUG_USER_DEVICE_MAX)];   

    API_USER_DEVICE_CMD_DATA_t  udv_cmd_data[(IUG_USER_DEVICE_MAX)];

    API_USER_DEVICE_CMD_t        udv_cmd_last[(IUG_USER_DEVICE_MAX)];

} struct_API_USER_DEVICE_DEMO_DATA_t;
typedef struct_API_USER_DEVICE_DEMO_DATA_t      API_USER_DEVICE_DEMO_DATA_t;

API_USER_DEVICE_DEMO_DATA_t     API_UDV_DEMO_DATA;
#endif



API_USER_DEVICE_CMD_CFM_t API_user_device_get_cmd_confirmation( const API_USER_DEVICE_ID_t     user_device_id,
                                                                const API_USER_DEVICE_CMD_t    user_device_cmd,
                                                                const API_USER_DEVIC_CMD_EVT_t user_device_cmd_event,
                                                                API_USER_DEVICE_CMD_DATA_t     * const user_device_cmd_data )
{
    API_USER_DEVICE_CMD_CFM_t   ret_cfm;
    #ifndef IUG_USER_API_USER_DEVICE_ENHANCED_DEMO_EN
    static API_USER_DEVICE_CMD_t udv_cmd_last[(IUG_USER_DEVICE_MAX)] = { (API_USRDVC_CMD_NONE), (API_USRDVC_CMD_NONE), (API_USRDVC_CMD_NONE) };
    #endif
    #ifdef IUG_USER_API_USER_DEVICE_ENHANCED_DEMO_EN
    API_USER_DEVICE_CMD_DATA_t  udv_cmd_data;
    #endif

    ret_cfm = (API_USRDVC_CMDCFM_ENUM_MAX);

    #ifndef IUG_USER_API_USER_DEVICE_ENHANCED_DEMO_EN
    switch( user_device_cmd_event )
    {
        case (API_USRDVC_CMDEVT_ACTIVATION):
            ret_cfm = (API_USRDVC_CMDCFM_DEACTIVATE);

            if( ((API_USRDVC_CMD_OFF) < user_device_cmd ) && ((API_USRDVC_CMD_ENUM_MAX) > user_device_cmd ) )
            {
                if( user_device_cmd != udv_cmd_last[user_device_id] )
                {
                    ret_cfm = (API_USRDVC_CMDCFM_ACTIVATE_ALIVE);
                    udv_cmd_last[user_device_id] = user_device_cmd;
                }
                else
                {
                    ret_cfm = (API_USRDVC_CMDCFM_DEACTIVATE);
                }
            }
            break;
        case (API_USRDVC_CMDEVT_DEACTIVATION):
            ret_cfm = (API_USRDVC_CMDCFM_DEACTIVATE);

            udv_cmd_last[user_device_id] = (API_USRDVC_CMD_NONE);
            break;
        case (API_USRDVC_CMDEVT_CYCLIC):
            ret_cfm = (API_USRDVC_CMDCFM_ACTIVATE_ALIVE);
            break;
        default:
            break;
    }

    if( (API_USRDVC_CMDCFM_ACTIVATE_ALIVE) != ret_cfm )
    {
        udv_cmd_last[user_device_id] = (API_USRDVC_CMD_NONE);
    }
    #endif


    #ifdef IUG_USER_API_USER_DEVICE_ENHANCED_DEMO_EN
    switch( user_device_cmd_event )
    {
        case (API_USRDVC_CMDEVT_ACTIVATION):
            ret_cfm = (API_USRDVC_CMDCFM_DEACTIVATE);

            if( ((API_USRDVC_CMD_OFF) < user_device_cmd ) && ((API_USRDVC_CMD_ENUM_MAX) > user_device_cmd ) )
            {
                if( 0u == API_UDV_DEMO_DATA.udv_active[user_device_id] )
                {
                    if( 0u != API_UDV_DEMO_DATA.udv_enabled[user_device_id] )
                    {
                        ret_cfm = (API_USRDVC_CMDCFM_ACTIVATE_ALIVE);
                    }
                }
                else
                {

                    if( user_device_cmd != API_UDV_DEMO_DATA.udv_cmd_last[user_device_id] )
                    {
                        if( 0u != API_UDV_DEMO_DATA.udv_enabled[user_device_id] )
                        {
                            ret_cfm = (API_USRDVC_CMDCFM_ACTIVATE_ALIVE);
                        }
                    }
                    else
                    {
                        API_user_device_get_cmd_param( user_device_id, &udv_cmd_data );

                        if( (user_device_cmd_data->cmd               != udv_cmd_data.cmd               ) ||
                            (user_device_cmd_data->ACTIVATION_PERIOD != udv_cmd_data.ACTIVATION_PERIOD ) ||
                            (user_device_cmd_data->SETPOINT          != udv_cmd_data.SETPOINT          ) )
                        {
                            ret_cfm = (API_USRDVC_CMDCFM_ACTIVATE_ALIVE);
                        }
                        else
                        {
                            ret_cfm = (API_USRDVC_CMDCFM_DEACTIVATE);
                        }
                    }
                }
            }
            API_UDV_DEMO_DATA.udv_cmd_last[user_device_id] = user_device_cmd;
            break;
        case (API_USRDVC_CMDEVT_CYCLIC):

            ret_cfm = (API_USRDVC_CMDCFM_DEACTIVATE);

            if( (1u == API_UDV_DEMO_DATA.udv_enabled[user_device_id] ) &&
                (1u == API_UDV_DEMO_DATA.udv_active[user_device_id]  ) )
            {
                if( 0u != API_UDV_DEMO_DATA.udv_keep_alive[user_device_id] )
                {
                    ret_cfm = (API_USRDVC_CMDCFM_ACTIVATE_ALIVE);
                }
            }
            break;
        default:
            break;
    }

    if( (API_USRDVC_CMDCFM_ACTIVATE_ALIVE) != ret_cfm )
    {
        API_UDV_DEMO_DATA.udv_cmd_last[user_device_id] = (API_USRDVC_CMD_NONE);
    }
    #endif

    return ret_cfm;
}


#ifdef IUG_USER_API_USER_DEVICE_ENHANCED_DEMO_EN
void USRAPI_user_device_cyclic_20ms_demo( void )
{

    API_USER_DEVICE_ID_t    udv_idx;
    API_USER_DEVICE_CMD_t   udv_cmd;

    for( udv_idx = (API_USER_DEVICE_ID_t)0u; udv_idx < (IUG_USER_DEVICE_MAX); udv_idx++ )
    {

        if( 0u == API_UDV_DEMO_DATA.udv_active[udv_idx] )
        {
            API_UDV_DEMO_DATA.udv_keep_alive[udv_idx] = 0u;
        }

        udv_cmd = API_user_device_get_cmd( udv_idx );

        if( ((API_USRDVC_CMD_OFF) < udv_cmd ) && ((API_USRDVC_CMD_ENUM_MAX) > udv_cmd ) )
        {
            if( 0u == API_UDV_DEMO_DATA.udv_active[udv_idx] )
            {
                API_UDV_DEMO_DATA.udv_active[udv_idx] = 1u;
                API_UDV_DEMO_DATA.udv_keep_alive[udv_idx] = 1u;

                API_user_device_get_cmd_param( udv_idx, &API_UDV_DEMO_DATA.udv_cmd_data[udv_idx] );
            }
        }
        else
        {
            API_UDV_DEMO_DATA.udv_active[udv_idx] = 0u;
        }
    }
}
#endif



