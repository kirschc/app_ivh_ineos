#ifndef __IUG_USER_API_USER_DEVICE_CFG_H_
#define __IUG_USER_API_USER_DEVICE_CFG_H_






typedef enum
{
    API_USRDVC_USR_ID_FIRST     = 0u,   
    API_USRDVC_USR_ID_SECOND    = 1u,   
    API_USRDVC_USR_ID_THIRD     = 2u,   
    API_USRDVC_USR_ID_ENUM_MAX  = 3u    
} enum_API_USER_DEVICE_USR_ID_t;
typedef enum_API_USER_DEVICE_USR_ID_t       API_USER_DEVICE_USR_ID_t;




#endif 

