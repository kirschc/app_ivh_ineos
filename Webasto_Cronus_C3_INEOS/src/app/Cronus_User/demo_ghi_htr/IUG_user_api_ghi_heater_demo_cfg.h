#ifndef IUG_USER_API_GHI_HEATER_DEMO_CFG_H_
#define IUG_USER_API_GHI_HEATER_DEMO_CFG_H_






#ifdef IUG_USER_API_GHI_HEATER_DEMO_EN



typedef enum
{
    API_GHI_HTR_ID_1st      = 0u,   
    API_GHI_HTR_ID_2nd      = 1u,   
    API_GHI_HTR_ID_3rd      = 2u,   
    API_GHI_HTR_ID_4th      = 3u,   
    API_GHI_HTR_ID_5th      = 4u,   
    API_GHI_HTR_ID_6th      = 5u,   
    API_GHI_HTR_ID_7th      = 6u,   
    API_GHI_HTR_ID_8th      = 7u,   
    API_GHI_HTR_ID_ENUM_MAX  = 8u   
} API_GHI_HTR_ID_t;


typedef struct
{
    IUG_UNIT_API_UNIT_ID_t                      usr_api_unit;
    IUG_UNIT_HEATER_SIGN_t                      htr_sgn;                
    uint8_t                                     htr_active;             

    struct_api_ghi_heater_interface_htr_cmd_req bsw_cmd_data;           
    uint8_t                                     bsw_cmd_cfm;            
    uint8_t                                     bsw_cmd_cfm_old;        

    struct_api_ghi_heater_interface_htr_cmd_ack bsw_ack_data;           
    uint8_t                                     htr_nak_code;           
    uint8_t                                     htr_ack_delay_cnt;


    uint8_t                                     htr_rd_id_old;
    uint8_t                                     htr_rd_id_delay_cnt;

    IUG_UNIT_API_STATE_t                        usr_api_htr_state;
    uint8_t                                     htr_state_sub;          
    uint16_t                                    htr_state_sub_cnt;      

    uint8_t                                     dbg_sim_cmd_ack;        
    uint8_t                                     dbg_sim_keep_alive;     
    uint8_t                                     dbg_sim_read_id;        
    uint8_t                                     dbg_sim_read_status;    

} iug_usr_api_ghi_heater_demo_ety_t;

typedef struct
{
    uint8_t                                     htr_count;              

    iug_usr_api_ghi_heater_demo_ety_t           htr_ctl[(API_GHI_HTR_ID_ENUM_MAX)];

    uint8_t                                     comm_err_ena;           
    uint8_t                                     comm_err_evt;           
    uint8_t                                     comm_err_per;           

} iug_usr_api_ghi_heater_demo_ctl_t;


void demo_ghi_htr_ctrl_cyclic( void );
void demo_ghi_htr_ctrl_init( void );



#endif 


#endif 

