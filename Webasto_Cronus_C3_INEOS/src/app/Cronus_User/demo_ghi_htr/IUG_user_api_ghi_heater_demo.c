
#include "IUG_user_api_include.h"

#ifdef IUG_USER_API_GHI_HEATER_DEMO_EN

#include "WBusConstantMessage.h"
#include "WBusConstantHeater.h"
#include "IUG_comm_config.h"


#define WTT_DIAG_STATUS_DEMO

#ifdef WTT_DIAG_STATUS_DEMO
#include "IUG_user_api_units_data_rd_wr_support__include.h"
#endif


typedef struct
{
    enum_IUG_UNIT_API_HTR_MSK_ID_t   delay_ack_ena;  
    enum_IUG_UNIT_API_HTR_MSK_ID_t    delay_ack_evt;  
    enum_IUG_UNIT_API_HTR_MSK_ID_t    delay_ack_per;  
    uint8_t             ack_dely[(API_GHI_HTR_ID_ENUM_MAX)];    
    uint8_t             nak_code[(API_GHI_HTR_ID_ENUM_MAX)];    

} demo_ghi_htr_if_preset_cmd_simulation_t;

typedef struct
{
    enum_IUG_UNIT_API_HTR_MSK_ID_t  status_29_ena;  
    enum_IUG_UNIT_API_HTR_MSK_ID_t  status_29_evt;  
    enum_IUG_UNIT_API_HTR_MSK_ID_t  status_29_per;  
    uint8_t                         status_sub[(API_GHI_HTR_ID_ENUM_MAX)];  

    enum_IUG_UNIT_API_HTR_MSK_ID_t  stfl_ena;       
    enum_IUG_UNIT_API_HTR_MSK_ID_t  stfl_evt;       
    enum_IUG_UNIT_API_HTR_MSK_ID_t  stfl_per;       

    enum_IUG_UNIT_API_HTR_MSK_ID_t  hgvp_ena;       
    enum_IUG_UNIT_API_HTR_MSK_ID_t  hgvp_evt;       
    enum_IUG_UNIT_API_HTR_MSK_ID_t  hgvp_per;       

    enum_IUG_UNIT_API_HTR_MSK_ID_t  uehfl_ena;      
    enum_IUG_UNIT_API_HTR_MSK_ID_t  uehfl_evt;      
    enum_IUG_UNIT_API_HTR_MSK_ID_t  uehfl_per;      

    enum_IUG_UNIT_API_HTR_MSK_ID_t  outport_ena;    
    enum_IUG_UNIT_API_HTR_MSK_ID_t  outport_evt;    
    enum_IUG_UNIT_API_HTR_MSK_ID_t  outport_per;    

    enum_IUG_UNIT_API_HTR_MSK_ID_t  temp_ena;       
    enum_IUG_UNIT_API_HTR_MSK_ID_t  temp_evt;       
    enum_IUG_UNIT_API_HTR_MSK_ID_t  temp_per;       

    enum_IUG_UNIT_API_HTR_MSK_ID_t  supply_vtg_ena; 
    enum_IUG_UNIT_API_HTR_MSK_ID_t  supply_vtg_evt; 
    enum_IUG_UNIT_API_HTR_MSK_ID_t  supply_vtg_per; 

} demo_ghi_htr_if_provide_status_simulation_t;

demo_ghi_htr_if_preset_cmd_simulation_t         demo_ghi_htr_if_preset_cmd_sim;
demo_ghi_htr_if_provide_status_simulation_t     demo_ghi_htr_if_provide_status_sim;


iug_usr_api_ghi_heater_demo_ctl_t       usrapi_ghi_htr_demo_ctl;
const uint8_t ext_iug_usr_api_ghi_heater_demo_cfg_main_entries = (IUG_UNIT_API_HEATER_UNIT_NUM_MAX);

static void demo_ghi_htr_ctrl_process_heater( const IUG_UNIT_API_UNIT_ID_t ghi_htr_idx );
static void demo_ghi_htr_if_preset_cmd_from_bsw(const IUG_UNIT_API_UNIT_ID_t ghi_htr_idx, const struct_api_ghi_heater_interface_htr_cmd_req cmd_req);
static void demo_ghi_htr_if_provide_cmd_ack_to_bsw(const IUG_UNIT_API_UNIT_ID_t ghi_htr_idx, struct_api_ghi_heater_interface_htr_cmd_ack * const cmd_ack );
static void demo_ghi_htr_if_provide_id_to_bsw(const IUG_UNIT_API_UNIT_ID_t ghi_htr_idx, struct_api_ghi_heater_interface_htr_id * const htr_id );
static void demo_ghi_htr_if_provide_keep_alive_info_to_bsw(const IUG_UNIT_API_UNIT_ID_t ghi_htr_idx, uint8_t * const keep_alive_info);
static void demo_ghi_htr_if_provide_property_to_bsw(const IUG_UNIT_API_UNIT_ID_t ghi_htr_idx, struct_api_ghi_heater_interface_htr_property * const property );
static void demo_ghi_htr_if_provide_status_to_bsw(const IUG_UNIT_API_UNIT_ID_t ghi_htr_idx, struct_api_ghi_heater_interface_htr_status * const htr_status);
static void demo_ghi_htr_if_provide_status_simulation( const IUG_UNIT_API_UNIT_ID_t ghi_htr_idx, struct_api_ghi_heater_interface_htr_status *const htr_status );

#endif 


void demo_ghi_htr_ctrl_cyclic( void )
{

    #ifdef IUG_USER_API_GHI_HEATER_DEMO_EN
    #endif

    IUG_UNIT_API_UNIT_ID_t          ghi_htr_idx;
    IUG_UNIT_API_HEATER_PROPERTY_t  htr_pty_hgX;


    if( 1 )
    {
        for( ghi_htr_idx = (IUG_UNIT_API_UNIT_ID_t)0u; ghi_htr_idx < usrapi_ghi_htr_demo_ctl.htr_count; ghi_htr_idx++ )
        {
            if( (IUG_UNIT_API_RETVAL_OK) == API_units_get_heater_property( ghi_htr_idx, &htr_pty_hgX ) )
            {
                if( ( 0u              != htr_pty_hgX.unit_available) &&
                    ((TGT_COM_GHI_ID) == ((TGT_COM_GHI_ID) & htr_pty_hgX.unit_target_id)) )
                {
                    demo_ghi_htr_ctrl_process_heater( ghi_htr_idx );
                }
            }
            else
            {
            }
        }
    }
    else
    {
    }
}


void demo_ghi_htr_ctrl_init( void )
{

    #ifdef IUG_USER_API_GHI_HEATER_DEMO_EN
    #endif

    uint8_t idx;

    memset( &usrapi_ghi_htr_demo_ctl, 0x00u, sizeof( iug_usr_api_ghi_heater_demo_ctl_t ) );

    API_ghi_heater_interface_init(demo_ghi_htr_if_provide_property_to_bsw, \
                                  demo_ghi_htr_if_preset_cmd_from_bsw, \
                                  demo_ghi_htr_if_provide_cmd_ack_to_bsw, \
                                  demo_ghi_htr_if_provide_id_to_bsw, \
                                  demo_ghi_htr_if_provide_status_to_bsw, \
                                  demo_ghi_htr_if_provide_keep_alive_info_to_bsw);

    if( ext_iug_usr_api_ghi_heater_demo_cfg_main_entries > (IUG_UNIT_API_HEATER_UNIT_NUM_MAX) )
    {
        usrapi_ghi_htr_demo_ctl.htr_count = (IUG_UNIT_API_HEATER_UNIT_NUM_MAX);

    }
    else
    {
        usrapi_ghi_htr_demo_ctl.htr_count = ext_iug_usr_api_ghi_heater_demo_cfg_main_entries;
    }

    for( idx = 0u; idx < usrapi_ghi_htr_demo_ctl.htr_count; idx++ )
    {
        usrapi_ghi_htr_demo_ctl.htr_ctl[idx].usr_api_unit = (IUG_UNIT_API_UNIT_ID_t)((IUG_UNIT_API_UNIT_HEATER_HG1) + idx);
    }

}


static void demo_ghi_htr_ctrl_process_heater( const IUG_UNIT_API_UNIT_ID_t ghi_htr_idx )
{

    uint8_t     new_cmd_req;
    uint8_t     htr_ste_grow;
    enum_IUG_UNIT_API_HTR_MSK_ID_t    htr_msk;
    iug_usr_api_ghi_heater_demo_ety_t   *p_htr_ctl;

    new_cmd_req = (WBUS_MSGMOD_00_NONE);

    if( (API_GHI_HTR_ID_ENUM_MAX) <= ghi_htr_idx )
    {
        return;
    }

    htr_msk   = (IUG_UNIT_API_MSKHTR_1) << ghi_htr_idx;
    p_htr_ctl = &usrapi_ghi_htr_demo_ctl.htr_ctl[ghi_htr_idx];


    if( p_htr_ctl->bsw_cmd_cfm != p_htr_ctl->bsw_cmd_data.wbus_cmd_req )
    {
        new_cmd_req = p_htr_ctl->bsw_cmd_data.wbus_cmd_req;
        switch( new_cmd_req )
        {
            case (WBUS_MSGMOD_10_OFF):
            case (WBUS_MSGMOD_20_ON_REC):
            case (WBUS_MSGMOD_21_ON_DOM):
            case (WBUS_MSGMOD_22_ON_VENT):
            case (WBUS_MSGMOD_23_ON_AUX):
            case (WBUS_MSGMOD_25_ON_BOOST):
            case (WBUS_MSGMOD_26_ON_ECO):
            case (WBUS_MSGMOD_2A_ON_TEMP_MOD):
                break;
            default:
                new_cmd_req = (WBUS_MSGMOD_00_NONE);
                break;
        }
    }
    p_htr_ctl->bsw_cmd_cfm = p_htr_ctl->bsw_cmd_data.wbus_cmd_req;

    if( (WBUS_MSGMOD_00_NONE) != new_cmd_req )
    {
        if( new_cmd_req != p_htr_ctl->bsw_cmd_cfm_old )
        {
            p_htr_ctl->bsw_cmd_cfm_old = new_cmd_req;

            p_htr_ctl->htr_ack_delay_cnt = 2u;

            if( (IUG_UNIT_API_MSKHTR__) != (htr_msk & demo_ghi_htr_if_preset_cmd_sim.delay_ack_ena) )
            {
                if( ((IUG_UNIT_API_MSKHTR__) != (htr_msk & demo_ghi_htr_if_preset_cmd_sim.delay_ack_per) ) ||
                    ((IUG_UNIT_API_MSKHTR__) != (htr_msk & demo_ghi_htr_if_preset_cmd_sim.delay_ack_evt) ) )
                {
                    p_htr_ctl->htr_ack_delay_cnt = demo_ghi_htr_if_preset_cmd_sim.ack_dely[ghi_htr_idx];
                    if( 0u == p_htr_ctl->htr_ack_delay_cnt )
                    {
                        p_htr_ctl->htr_ack_delay_cnt = ((100u + (10u * ghi_htr_idx)) / (IUG_UNIT_CTL_CYLIC_PERIOD)); 
                    }
                }
                demo_ghi_htr_if_preset_cmd_sim.delay_ack_evt &= ~(htr_msk);
            }
        }
        else
        {
            if( 0u == p_htr_ctl->htr_ack_delay_cnt )
            {
                p_htr_ctl->htr_ack_delay_cnt = 1u;
            }
        }
    }


    if( 0u != p_htr_ctl->htr_ack_delay_cnt )
    {
        (p_htr_ctl->htr_ack_delay_cnt)--;

        if( 0u == p_htr_ctl->htr_ack_delay_cnt )
        {
            if( (WBUSCONF_NEGRESPCODE_00_RESERVED) == p_htr_ctl->htr_nak_code )
            {
                switch( p_htr_ctl->bsw_cmd_data.wbus_cmd_req )
                {
                    case (WBUS_MSGMOD_10_OFF):
                        p_htr_ctl->htr_active = 0u; 
                        break;
                    case (WBUS_MSGMOD_20_ON_REC):
                    case (WBUS_MSGMOD_21_ON_DOM):
                    case (WBUS_MSGMOD_22_ON_VENT):
                    case (WBUS_MSGMOD_23_ON_AUX):
                    case (WBUS_MSGMOD_25_ON_BOOST):
                    case (WBUS_MSGMOD_26_ON_ECO):
                    case (WBUS_MSGMOD_2A_ON_TEMP_MOD):
                        p_htr_ctl->htr_active = p_htr_ctl->bsw_cmd_data.wbus_cmd_req; 
                        break;
                    default:
                        p_htr_ctl->htr_nak_code = (WBUSCONF_NEGRESPCODE_22_CONDITIONS_NOT_CORRECT);
                        break;
                }
            }

            if( (WBUSCONF_NEGRESPCODE_00_RESERVED) == p_htr_ctl->htr_nak_code )
            {
                memcpy( &p_htr_ctl->bsw_ack_data.htr_cmd_ack, &p_htr_ctl->bsw_cmd_data, sizeof(struct_api_ghi_heater_interface_htr_cmd_req) );
                p_htr_ctl->bsw_ack_data.htr_nak_code = (WBUSCONF_NEGRESPCODE_00_RESERVED);
            }
            else
            {
                memcpy( &p_htr_ctl->bsw_ack_data.htr_cmd_ack, &p_htr_ctl->bsw_cmd_data, sizeof(struct_api_ghi_heater_interface_htr_cmd_req) );
                p_htr_ctl->bsw_ack_data.htr_nak_code = p_htr_ctl->htr_nak_code;
            }
        }
    }


    htr_ste_grow = 0u;
    if( (WBUS_MSGMOD_20_ON_REC) <= p_htr_ctl->htr_active )
    {
        htr_ste_grow = 1u;
        if( 0xFFFFu > p_htr_ctl->htr_state_sub_cnt )
        {
            (p_htr_ctl->htr_state_sub_cnt)++;
        }
        p_htr_ctl->usr_api_htr_state = (IUG_UNIT_API_STATE_HEATING);
    }
    else
    {
        if( 0x0000u < p_htr_ctl->htr_state_sub_cnt )
        {
            (p_htr_ctl->htr_state_sub_cnt)--;
        }
        p_htr_ctl->usr_api_htr_state = (IUG_UNIT_API_STATE_SWITCH_OFF);
    }

    if( 0u == p_htr_ctl->htr_state_sub_cnt )
    {
        p_htr_ctl->htr_active        = 0u;
        p_htr_ctl->usr_api_htr_state = (IUG_UNIT_API_STATE_WAIT_STATE);
        p_htr_ctl->htr_state_sub     = (HTRST_71_WST_WaitState);
    }
    else
    {
        if( 0u < htr_ste_grow )
        {
            if( (3000u / (IUG_UNIT_CTL_CYLIC_PERIOD)) >= p_htr_ctl->htr_state_sub_cnt )
            {
                p_htr_ctl->htr_state_sub = (HTRST_08_BMS_BrennermotorLosr);
            }
            else if( (6000u / (IUG_UNIT_CTL_CYLIC_PERIOD)) >= p_htr_ctl->htr_state_sub_cnt )
            {
                p_htr_ctl->htr_state_sub = (HTRST_0F_FLA_Flammwaechterabfrg);
            }
            else if( (10000u / (IUG_UNIT_CTL_CYLIC_PERIOD)) >= p_htr_ctl->htr_state_sub_cnt )
            {
                p_htr_ctl->htr_state_sub = (HTRST_07_BFOE_BrennstoffFoerd);
            }
            else if( (15000u / (IUG_UNIT_CTL_CYLIC_PERIOD)) >= p_htr_ctl->htr_state_sub_cnt )
            {
                p_htr_ctl->htr_state_sub     = (HTRST_06_BBVL_BrennerVollast);
            }
            else
            {
                p_htr_ctl->htr_state_sub_cnt -= 2u;
            }
        }
        else
        {
            if( (7000u / (IUG_UNIT_CTL_CYLIC_PERIOD)) >= p_htr_ctl->htr_state_sub_cnt )
            {
                p_htr_ctl->htr_state_sub = (HTRST_1A_KUEG_Kuehlung);
            }
            else
            {
                p_htr_ctl->htr_state_sub = (HTRST_03_AKUE_AusbrennRampe);
            }
        }
    }

    if( ((IUG_UNIT_API_STATE_WAIT_STATE) == p_htr_ctl->usr_api_htr_state ) ||
        ((IUG_UNIT_API_STATE_FAULT)      <= p_htr_ctl->usr_api_htr_state ) )
    {
        p_htr_ctl->dbg_sim_keep_alive = 0u;
    }

}


static void demo_ghi_htr_if_preset_cmd_from_bsw(const IUG_UNIT_API_UNIT_ID_t ghi_htr_idx, const struct_api_ghi_heater_interface_htr_cmd_req cmd_req)
{
    uint8_t     cmd_ack;
    uint8_t     nak_code;
    IUG_UNIT_API_HEATER_PROPERTY_t      htr_pty_hgX;
    iug_usr_api_ghi_heater_demo_ety_t   *p_htr_ctl;

    nak_code  = (WBUSCONF_NEGRESPCODE_00_RESERVED);
    p_htr_ctl = (NULL);

    switch( ghi_htr_idx )
    {
        case( IUG_UNIT_API_UNIT_HEATER_HG1 ):
        case( IUG_UNIT_API_UNIT_HEATER_HG2 ):
        case( IUG_UNIT_API_UNIT_HEATER_HG3 ):
        case( IUG_UNIT_API_UNIT_HEATER_HG4 ):
        case( IUG_UNIT_API_UNIT_HEATER_HG5 ):
        case( IUG_UNIT_API_UNIT_HEATER_HG6 ):
        case( IUG_UNIT_API_UNIT_HEATER_HG7 ):
        case( IUG_UNIT_API_UNIT_HEATER_HG8 ):

            p_htr_ctl = &usrapi_ghi_htr_demo_ctl.htr_ctl[ghi_htr_idx];

            if( (IUG_UNIT_API_RETVAL_OK) == API_units_get_heater_property( ghi_htr_idx, &htr_pty_hgX ) )
            {

                if( ( 0u              != htr_pty_hgX.unit_available) &&
                    ((TGT_COM_GHI_ID) == ((TGT_COM_GHI_ID) & htr_pty_hgX.unit_target_id)) )
                {
                    cmd_ack = 0u;

                    switch( cmd_req.wbus_cmd_req )
                    {
                        case (WBUS_MSGMOD_10_OFF):
                        case (WBUS_MSGMOD_20_ON_REC):
                        case (WBUS_MSGMOD_21_ON_DOM):
                        case (WBUS_MSGMOD_22_ON_VENT):
                        case (WBUS_MSGMOD_23_ON_AUX):
                        case (WBUS_MSGMOD_25_ON_BOOST):
                        case (WBUS_MSGMOD_26_ON_ECO):

                            cmd_ack = 1u; 

                            if( 0u != cmd_ack )
                            {
                                memcpy(&p_htr_ctl->bsw_cmd_data, &cmd_req, sizeof(struct_api_ghi_heater_interface_htr_cmd_req));
                                memset( &p_htr_ctl->bsw_ack_data, 0u, sizeof(struct_api_ghi_heater_interface_htr_cmd_ack) );
                                p_htr_ctl->bsw_cmd_cfm  = (WBUS_MSGMOD_00_NONE);
                                p_htr_ctl->htr_nak_code = (WBUSCONF_NEGRESPCODE_00_RESERVED);
                            }
                            break;
                        default:
                            nak_code = (WBUSCONF_NEGRESPCODE_22_CONDITIONS_NOT_CORRECT);
                            break;
                    }
                }
                else
                {
                    nak_code = (WBUSCONF_NEGRESPCODE_33_ACCESS_DENIED);
                }
            }
            else
            {
                nak_code = (WBUSCONF_NEGRESPCODE_33_ACCESS_DENIED);
            }
            break;
        default:
            nak_code = (WBUSCONF_NEGRESPCODE_33_ACCESS_DENIED);
            break;
    } 


    if( (NULL) != p_htr_ctl )
    {
        if( (WBUSCONF_NEGRESPCODE_00_RESERVED) != nak_code )
        {
            p_htr_ctl->htr_nak_code = nak_code;
        }
    }
}


static void demo_ghi_htr_if_provide_cmd_ack_to_bsw(const IUG_UNIT_API_UNIT_ID_t ghi_htr_idx, struct_api_ghi_heater_interface_htr_cmd_ack * const htr_cmd_ack )
{
    uint8_t     cmd_ack;
    uint8_t     hgi_htr_msk;
    iug_usr_api_ghi_heater_demo_ety_t   *p_htr_ctl;

    switch( ghi_htr_idx )
    {
        case( IUG_UNIT_API_UNIT_HEATER_HG1 ):
        case( IUG_UNIT_API_UNIT_HEATER_HG2 ):
        case( IUG_UNIT_API_UNIT_HEATER_HG3 ):
        case( IUG_UNIT_API_UNIT_HEATER_HG4 ):
        case( IUG_UNIT_API_UNIT_HEATER_HG5 ):
        case( IUG_UNIT_API_UNIT_HEATER_HG6 ):
        case( IUG_UNIT_API_UNIT_HEATER_HG7 ):
        case( IUG_UNIT_API_UNIT_HEATER_HG8 ):
            p_htr_ctl   = &usrapi_ghi_htr_demo_ctl.htr_ctl[ghi_htr_idx];
            hgi_htr_msk = 1u << ghi_htr_idx;

            memset( htr_cmd_ack, 0u, sizeof(struct_api_ghi_heater_interface_htr_cmd_ack) );
            cmd_ack = 1u;

            if( 0u != p_htr_ctl->dbg_sim_cmd_ack )
            {
                if( 1u != p_htr_ctl->dbg_sim_cmd_ack )
                {
                    cmd_ack = 0u;
                }

            }

            if( (hgi_htr_msk == (hgi_htr_msk & usrapi_ghi_htr_demo_ctl.comm_err_ena) ) &&
                (hgi_htr_msk == (hgi_htr_msk & usrapi_ghi_htr_demo_ctl.comm_err_per) ) )
            {
                    cmd_ack = 0u;
            }

            if( (NULL) != htr_cmd_ack)
            {
                if( 0u == p_htr_ctl->htr_ack_delay_cnt )
                {
                    if( 0u != cmd_ack )
                    {
                        memcpy( htr_cmd_ack, &p_htr_ctl->bsw_ack_data, sizeof(struct_api_ghi_heater_interface_htr_cmd_ack) );
                    }
                }
            }
            break;
        default:
            break;
    } 
}


static void demo_ghi_htr_if_provide_id_to_bsw(const IUG_UNIT_API_UNIT_ID_t ghi_htr_idx, struct_api_ghi_heater_interface_htr_id *const htr_id )
{

    uint8_t     rd_sze;
    uint8_t     rsp_buf_sze;
    uint8_t     idx_rsp;
    uint8_t     hgi_htr_msk;
    uint8_t     *p_rsp_dat;
    uint16_t    tmp16;
    int32_t     ghi_com_dev_webpar;
    IUG_UNIT_HEATER_SIGN_t              htr_sgn;
    IUG_GEN_HEATER_SIGN_DESCR_ENTRY_t   *p_sgn_ety;
    iug_usr_api_ghi_heater_demo_ety_t   *p_htr_ctl;

    idx_rsp     = 0u;
    rd_sze      = 0u;
    rsp_buf_sze = (API_GHI_HTR_IF_HTR_ID_RESPONSE_BUF_MAX);
    p_rsp_dat   = htr_id->rsp_buf;
    htr_id->rsp_sze = 0u;
    p_htr_ctl   = &usrapi_ghi_htr_demo_ctl.htr_ctl[ghi_htr_idx];
    hgi_htr_msk = 1u << ghi_htr_idx;

    if( htr_id->req_id != p_htr_ctl->htr_rd_id_old )
    {
        p_htr_ctl->htr_rd_id_old   = htr_id->req_id;

        p_htr_ctl->htr_rd_id_delay_cnt = (100u + (10u * ghi_htr_idx)) / (IUG_UNIT_CTL_CYLIC_PERIOD);    
        p_htr_ctl->htr_rd_id_delay_cnt = 2u;
    }

    if( 0u != p_htr_ctl->htr_rd_id_delay_cnt )
    {
        (p_htr_ctl->htr_rd_id_delay_cnt)--;
    }

    if( (1) && 
        (1) && 
        (0u == p_htr_ctl->htr_rd_id_delay_cnt ) )
    {
        switch( htr_id->req_id )
        {
            case (WBUS_MSGSRV_300B_ID_GET_SERIAL_NUMBER):
                if( (IUG_HTR_SIGN_ENUM_MAX) > p_htr_ctl->htr_sgn )
                {
                    if( (idx_rsp + (1u + 5u)) <= rsp_buf_sze )
                    {
                        if( 5u == 5u ) 
                        {
                            *(p_rsp_dat + 1u) = 'G';
                            *(p_rsp_dat + 2u) = 'H';
                            *(p_rsp_dat + 3u) = 'I';
                            *(p_rsp_dat + 4u) = '-';
                            *(p_rsp_dat + 5u) = ' ' + htr_id->ghi_com_dev;
                            rd_sze = (1u + 5u); 
                        }
                    }
                }
                break;
            case (WBUS_MSGSRV_300D_ID_WBUS_VERSION):
                *(p_rsp_dat + 1u) = 0x00u;

                ghi_com_dev_webpar = get_para_by_id( (ID_CONFIG_GHI_HEATER) );

                #if 0 
                if( 0u < usrapi_htr_demo_ctl.ghi_com_dev_webpar_max )
                {
                    if( usrapi_htr_demo_ctl.ghi_com_dev_webpar_max < ghi_com_dev_webpar )
                    {
                        ghi_com_dev_webpar = usrapi_htr_demo_ctl.ghi_com_dev_webpar_max;
                    }
                }
                #endif

                if( htr_id->ghi_com_dev <= ghi_com_dev_webpar )
                {
                    *(p_rsp_dat + 1u) = 0x43u; 
                }
                rd_sze = (1u + 1u);
                break;
            case (WBUS_MSGSRV_300E_ID_GET_HEATER_SIGN):
                p_htr_ctl->htr_sgn = (IUG_HTR_SIGN_ENUM_MAX);

                if( (idx_rsp + (1u + (IUG_GEN_HEATER_SIGN_NAME_LEN))) <= rsp_buf_sze )
                {
                    switch( htr_id->ghi_com_dev )
                    {
                        #if 1
                        case (1u): htr_sgn = (IUG_HTR_SIGN_GHI_GEN4_WAT); break;
                        case (2u): htr_sgn = (IUG_HTR_SIGN_GHI_HVH_AIR); break;
                        case (3u): htr_sgn = (IUG_HTR_SIGN_GHI_HVH_WAT); break;
                        case (4u): htr_sgn = (IUG_HTR_SIGN_GHI_HVH_WAT); break;
                        case (5u): htr_sgn = (IUG_HTR_SIGN_GHI_HVH_WAT); break;
                        case (6u): htr_sgn = (IUG_HTR_SIGN_GHI_HVH_WAT); break;
                        case (7u): htr_sgn = (IUG_HTR_SIGN_GHI_HVH_WAT); break;
                        case (8u): htr_sgn = (IUG_HTR_SIGN_GHI_HVH_WAT); break;
                        #else
                        case ((IUG_COMDEV_GHI_FIRST) + 0u): htr_sgn = (IUG_HTR_SIGN_GHI_DEF_AIR); break;
                        case ((IUG_COMDEV_GHI_FIRST) + 1u): htr_sgn = (IUG_HTR_SIGN_GHI_DEF_WAT); break;
                        case ((IUG_COMDEV_GHI_FIRST) + 2u): htr_sgn = (IUG_HTR_SIGN_GHI_HVH_AIR); break;
                        case ((IUG_COMDEV_GHI_FIRST) + 3u): htr_sgn = (IUG_HTR_SIGN_GHI_HVH_WAT); break;
                        case ((IUG_COMDEV_GHI_FIRST) + 4u): htr_sgn = (IUG_HTR_SIGN_GHI_HVH_WAT); break;
                        case ((IUG_COMDEV_GHI_FIRST) + 5u): htr_sgn = (IUG_HTR_SIGN_GHI_HVH_WAT); break;
                        case ((IUG_COMDEV_GHI_FIRST) + 6u): htr_sgn = (IUG_HTR_SIGN_GHI_HVH_WAT); break;
                        case ((IUG_COMDEV_GHI_FIRST) + 7u): htr_sgn = (IUG_HTR_SIGN_GHI_HVH_WAT); break;
                        #endif
                        default: htr_sgn = (IUG_HTR_SIGN_ENUM_MAX); break;
                    }

                    p_sgn_ety = IUG_gen_heater_type_test_sign_id( htr_sgn );
                    if( (NULL) != p_sgn_ety )
                    {
                        p_htr_ctl->htr_sgn = htr_sgn;
                        memcpy( (p_rsp_dat + 1u), p_sgn_ety->sign_name, (IUG_GEN_HEATER_SIGN_NAME_LEN) );
                        rd_sze = (1u + (IUG_GEN_HEATER_SIGN_NAME_LEN));
                    }
                }
                break;
            case (WBUS_MSGSRV_3012_ID_GET_MAX_HEATING_TIME):
                if( (IUG_HTR_SIGN_ENUM_MAX) > p_htr_ctl->htr_sgn )
                {
                    *(p_rsp_dat + 1u) = (100u + (2u * ghi_htr_idx ));
                    rd_sze = (1u + 1u);
                }
                break;
            case (WBUS_MSGSRV_301D_ID_GET_HEATER_TYPE):
                if( (IUG_HTR_SIGN_ENUM_MAX) > p_htr_ctl->htr_sgn )
                {
                    *(p_rsp_dat + 1u) = (IUG_UNIT_HEATER_TYPE_INFO_WATER);
                    switch( p_htr_ctl->htr_sgn )
                    {
                        case (IUG_HTR_SIGN_GHI_DEF_AIR):
                        case (IUG_HTR_SIGN_GHI_HVH_AIR):
                            *(p_rsp_dat + 1u) = 0x00u; 
                            break;
                    }
                    rd_sze = (1u + 1u);
                }
                break;
            case (WBUS_MSGSRV_3020_ID_GET_HEATER_CAPABLTY):
                if( (IUG_HTR_SIGN_ENUM_MAX) > p_htr_ctl->htr_sgn )
                {
                    tmp16 =  (  (HTRCAP_HT_DOM)  |
 0u );
                    *(p_rsp_dat + 1u) = tmp16 >> 8u; 
                    *(p_rsp_dat + 2u) = tmp16;       
                    rd_sze = (1u + 2u);
                }
                break;
            case (WBUS_MSGSRV_3022_ID_GET_HEATER_MODE_FUNC):
                tmp16 = 0u;
                if( 0x00u == (0x01u & ghi_htr_idx) )
                {
                    tmp16 |= (HTR_MF_AIRCOR); 
                }

                *(p_rsp_dat + 1u)  = tmp16 >> 8u; 
                *(p_rsp_dat + 2u)  = tmp16;       
                rd_sze = (1u + 2u);
                break;
            default:
                rd_sze = 0u;
                break;
        } 
    }


    if( 0 != rd_sze )
    {

        if( 0u != p_htr_ctl->dbg_sim_read_id )
        {
            if( 1u != p_htr_ctl->dbg_sim_read_id )
            {
                rd_sze = 0u;
            }

        }

        if( (hgi_htr_msk == (hgi_htr_msk & usrapi_ghi_htr_demo_ctl.comm_err_ena) ) &&
            (hgi_htr_msk == (hgi_htr_msk & usrapi_ghi_htr_demo_ctl.comm_err_per) ) )
        {
            rd_sze = 0u;
        }

        if( 0 != rd_sze )
        {
            *(p_rsp_dat + 0u) = htr_id->req_id;

            if( (idx_rsp + rd_sze) <= rsp_buf_sze )
            {
                idx_rsp         += rd_sze;
                htr_id->rsp_id   = htr_id->req_id;
                htr_id->rsp_sze  = idx_rsp;
            }
        }
    }
}


static void demo_ghi_htr_if_provide_keep_alive_info_to_bsw(const IUG_UNIT_API_UNIT_ID_t ghi_htr_idx, uint8_t *const keep_alive_info)
{
    uint8_t     keep_alv;       
    uint8_t     hgi_htr_msk; 
    iug_usr_api_ghi_heater_demo_ety_t   *p_htr_ctl;

    switch( ghi_htr_idx )
    {
        case( IUG_UNIT_API_UNIT_HEATER_HG1 ):
        case( IUG_UNIT_API_UNIT_HEATER_HG2 ):
        case( IUG_UNIT_API_UNIT_HEATER_HG3 ):
        case( IUG_UNIT_API_UNIT_HEATER_HG4 ):
        case( IUG_UNIT_API_UNIT_HEATER_HG5 ):
        case( IUG_UNIT_API_UNIT_HEATER_HG6 ):
        case( IUG_UNIT_API_UNIT_HEATER_HG7 ):
        case( IUG_UNIT_API_UNIT_HEATER_HG8 ):
            p_htr_ctl   = &usrapi_ghi_htr_demo_ctl.htr_ctl[ghi_htr_idx];
            hgi_htr_msk = 1u << ghi_htr_idx;

            keep_alv = 1u; 

            if( ((IUG_UNIT_API_STATE_WAIT_STATE) != p_htr_ctl->usr_api_htr_state ) &&
                ((IUG_UNIT_API_STATE_SWITCH_OFF) != p_htr_ctl->usr_api_htr_state ) &&
                ((IUG_UNIT_API_STATE_FAULT)       > p_htr_ctl->usr_api_htr_state ) )
            {
                keep_alv = 0u; 
            }

            if( 0u != p_htr_ctl->dbg_sim_keep_alive )
            {
                keep_alv = p_htr_ctl->dbg_sim_keep_alive;

                if( 0xFFu != keep_alv )
                {
                    p_htr_ctl->dbg_sim_keep_alive = 0u;
                }
            }

            if( (hgi_htr_msk == (hgi_htr_msk & usrapi_ghi_htr_demo_ctl.comm_err_ena) ) &&
                (hgi_htr_msk == (hgi_htr_msk & usrapi_ghi_htr_demo_ctl.comm_err_per) ) )
            {
                keep_alv = 0xFFu;
            }

            *keep_alive_info = keep_alv;
            break;
        default:
            break;
    }
}


static void demo_ghi_htr_if_provide_property_to_bsw(const IUG_UNIT_API_UNIT_ID_t ghi_htr_idx, struct_api_ghi_heater_interface_htr_property * const property )
{



    if( (NULL) != property )
    {
        property->comm_timeout_ms = 100u; 
    }
}


static void demo_ghi_htr_if_provide_status_to_bsw( const IUG_UNIT_API_UNIT_ID_t ghi_htr_idx, struct_api_ghi_heater_interface_htr_status *const htr_status )
{
    uint8_t     hgi_htr_msk;
    iug_usr_api_ghi_heater_demo_ety_t   *p_htr_ctl;

#ifdef WTT_DIAG_STATUS_DEMO
    IUG_UNIT_API_RETVAL_t   ret_val;
    uint16_t    rsp_sze;
    uint8_t     req_buf[10];
    uint8_t     rsp_buf[100];
#endif

#if 0 

req_buf[0u] = 0x50u;
req_buf[1u] = 0x30u;
req_buf[2u] = (API_UNITS_DATA_RDWRSUP_ST_STATE_NUM_2BYTE);
req_buf[3u] = (API_UNITS_DATA_RDWRSUP_ST_SUPPLY_VOLTAGE);
req_buf[4u] = (API_UNITS_DATA_RDWRSUP_ST_STATE_STFL_FAULTY);
req_buf[5u] = (API_UNITS_DATA_RDWRSUP_ST_STATE_NUM_2BYTE);

ret_val =  API_units_data_rdwr_support_wtt_diag_response_create( 
            ghi_htr_idx,
            (API_UNITS_DATA_RDWRSUP_STATUS),
            &req_buf[0u],
            6u,
            &rsp_buf[0u],
            &rsp_sze,
            100u );

req_buf[0u] = 0x51u;
req_buf[1u] = 0x30u;
req_buf[2u] = (API_UNITS_DATA_RDWRSUP_ID_WBUS_SPEC_VERSION);
req_buf[3u] = (API_UNITS_DATA_RDWRSUP_ID_ECU_SERIAL_NUMBER);
req_buf[4u] = (API_UNITS_DATA_RDWRSUP_ID_ECU_ID_NUMBER);
req_buf[5u] = (API_UNITS_DATA_RDWRSUP_ID_SW_VERSION);

ret_val =  API_units_data_rdwr_support_wtt_diag_response_create( 
            ghi_htr_idx,
            (API_UNITS_DATA_RDWRSUP_STATUS),
            &req_buf[0u],
            6u,
            &rsp_buf[0u],
            &rsp_sze,
            100u );

#endif

    switch( ghi_htr_idx )
    {
        case( IUG_UNIT_API_UNIT_HEATER_HG1 ):
        case( IUG_UNIT_API_UNIT_HEATER_HG2 ):
        case( IUG_UNIT_API_UNIT_HEATER_HG3 ):
        case( IUG_UNIT_API_UNIT_HEATER_HG4 ):
        case( IUG_UNIT_API_UNIT_HEATER_HG5 ):
        case( IUG_UNIT_API_UNIT_HEATER_HG6 ):
        case( IUG_UNIT_API_UNIT_HEATER_HG7 ):
        case( IUG_UNIT_API_UNIT_HEATER_HG8 ):
            p_htr_ctl   = &usrapi_ghi_htr_demo_ctl.htr_ctl[ghi_htr_idx];
            hgi_htr_msk = 1u << ghi_htr_idx;

            htr_status->htr_status_29  = (p_htr_ctl->htr_state_sub) << 8u;
            htr_status->htr_err_STFL   = 0u;
            htr_status->htr_err_HGV    = 0u;
            htr_status->htr_err_UEHFL  = 0u;
            htr_status->htr_outport    = 0u; 
            htr_status->htr_temp       = 0u;
            htr_status->supply_voltage = 13000u + (ghi_htr_idx * 5u); 


            demo_ghi_htr_if_provide_status_simulation( ghi_htr_idx, htr_status );

#ifdef joki_STATUS            
            ret_val =  API_units_data_rdwr_support_wtt_diag_response_create( 
                        ghi_htr_idx, (API_UNITS_DATA_RDWRSUP_STATUS),
                        htr_status->req_buf,
                        htr_status->req_sze,
                        htr_status->rsp_buf,
                        &htr_status->rsp_sze,
                        htr_status->rsp_sze_max );

            if( (IUG_UNIT_API_RETVAL_OK) != ret_val )
            {
                htr_status->htr_status_29 = 0xFFFFu;
            }
#endif            

            if( 0u != p_htr_ctl->dbg_sim_read_status )
            {
                if( 2u <= p_htr_ctl->dbg_sim_read_status )
                {
                    htr_status->htr_status_29 = 0xFFFFu;
                }

            }

            if( (hgi_htr_msk == (hgi_htr_msk & usrapi_ghi_htr_demo_ctl.comm_err_ena) ) &&
                (hgi_htr_msk == (hgi_htr_msk & usrapi_ghi_htr_demo_ctl.comm_err_per) ) )
            {
                htr_status->htr_status_29 = 0xFFFFu;
            }
            break;
    }
}


static void demo_ghi_htr_if_provide_status_simulation( const IUG_UNIT_API_UNIT_ID_t ghi_htr_idx, struct_api_ghi_heater_interface_htr_status *const htr_status )
{
    uint16_t            sta;
    enum_IUG_UNIT_API_HTR_MSK_ID_t    htr_msk;
    iug_usr_api_ghi_heater_demo_ety_t   *p_htr_ctl;

    if( ((API_GHI_HTR_ID_ENUM_MAX)  > ghi_htr_idx) &&
        ((NULL)                    != htr_status ) )
    {
        htr_msk   = (IUG_UNIT_API_MSKHTR_1) << ghi_htr_idx;
        p_htr_ctl = &usrapi_ghi_htr_demo_ctl.htr_ctl[ghi_htr_idx];

        if( (IUG_UNIT_API_MSKHTR__) != (htr_msk & demo_ghi_htr_if_provide_status_sim.status_29_ena) )
        {
            sta = 0u;
            if( ((IUG_UNIT_API_MSKHTR__) != (htr_msk & demo_ghi_htr_if_provide_status_sim.status_29_per) ) ||
                ((IUG_UNIT_API_MSKHTR__) != (htr_msk & demo_ghi_htr_if_provide_status_sim.status_29_evt) ) )
            {
                sta = (demo_ghi_htr_if_provide_status_sim.status_sub[ghi_htr_idx] << 8u);
            }
            demo_ghi_htr_if_provide_status_sim.status_29_evt &= ~(htr_msk);
            htr_status->htr_status_29 = sta;
        }
        if( ((IUG_UNIT_API_MSKHTR__) == (htr_msk & demo_ghi_htr_if_provide_status_sim.status_29_ena) ) ||
            ((IUG_UNIT_API_MSKHTR__) == (htr_msk & demo_ghi_htr_if_provide_status_sim.status_29_per) ) )
        {
            demo_ghi_htr_if_provide_status_sim.status_sub[ghi_htr_idx] = p_htr_ctl->htr_state_sub;
        }

        if( (IUG_UNIT_API_MSKHTR__) != (htr_msk & demo_ghi_htr_if_provide_status_sim.stfl_ena) )
        {
            sta = 0u;
            if( ((IUG_UNIT_API_MSKHTR__) != (htr_msk & demo_ghi_htr_if_provide_status_sim.stfl_per) ) ||
                ((IUG_UNIT_API_MSKHTR__) != (htr_msk & demo_ghi_htr_if_provide_status_sim.stfl_evt) ) )
            {
                sta = 1u;
            }
            demo_ghi_htr_if_provide_status_sim.stfl_evt &= ~(htr_msk);
            htr_status->htr_err_STFL = sta;
        }

        if( (IUG_UNIT_API_MSKHTR__) != (htr_msk & demo_ghi_htr_if_provide_status_sim.hgvp_ena) )
        {
            sta = 0u;
            if( ((IUG_UNIT_API_MSKHTR__) != (htr_msk & demo_ghi_htr_if_provide_status_sim.hgvp_per) ) ||
                ((IUG_UNIT_API_MSKHTR__) != (htr_msk & demo_ghi_htr_if_provide_status_sim.hgvp_evt) ) )
            {
                sta = 1u;
            }
            demo_ghi_htr_if_provide_status_sim.hgvp_evt &= ~(htr_msk);
            htr_status->htr_err_HGV = sta;
        }

        if( (IUG_UNIT_API_MSKHTR__) != (htr_msk & demo_ghi_htr_if_provide_status_sim.uehfl_ena) )
        {
            sta = 0u;
            if( ((IUG_UNIT_API_MSKHTR__) != (htr_msk & demo_ghi_htr_if_provide_status_sim.uehfl_per) ) ||
                ((IUG_UNIT_API_MSKHTR__) != (htr_msk & demo_ghi_htr_if_provide_status_sim.uehfl_evt) ) )
            {
                sta = 1u;
            }
            demo_ghi_htr_if_provide_status_sim.uehfl_evt &= ~(htr_msk);
            htr_status->htr_err_UEHFL = sta;
        }

        if( (IUG_UNIT_API_MSKHTR__) != (htr_msk & demo_ghi_htr_if_provide_status_sim.outport_ena) )
        {
            sta = 0u;
            if( ((IUG_UNIT_API_MSKHTR__) != (htr_msk & demo_ghi_htr_if_provide_status_sim.outport_per) ) ||
                ((IUG_UNIT_API_MSKHTR__) != (htr_msk & demo_ghi_htr_if_provide_status_sim.outport_evt) ) )
            {
                sta = 1u;
            }
            demo_ghi_htr_if_provide_status_sim.outport_evt &= ~(htr_msk);
            htr_status->htr_outport = sta;
        }

        if( (IUG_UNIT_API_MSKHTR__) != (htr_msk & demo_ghi_htr_if_provide_status_sim.temp_ena) )
        {
            sta = 0u;
            if( ((IUG_UNIT_API_MSKHTR__) != (htr_msk & demo_ghi_htr_if_provide_status_sim.temp_per) ) ||
                ((IUG_UNIT_API_MSKHTR__) != (htr_msk & demo_ghi_htr_if_provide_status_sim.temp_evt) ) )
            {
                sta = 1u;
            }
            demo_ghi_htr_if_provide_status_sim.temp_evt &= ~(htr_msk);
            htr_status->htr_temp = sta;
        }

        if( (IUG_UNIT_API_MSKHTR__) != (htr_msk & demo_ghi_htr_if_provide_status_sim.supply_vtg_ena) )
        {
            sta = 0u;
            if( ((IUG_UNIT_API_MSKHTR__) != (htr_msk & demo_ghi_htr_if_provide_status_sim.supply_vtg_per) ) ||
                ((IUG_UNIT_API_MSKHTR__) != (htr_msk & demo_ghi_htr_if_provide_status_sim.supply_vtg_evt) ) )
            {
                sta  = 14000u; 
                sta += (ghi_htr_idx * 5u); 
            }
            demo_ghi_htr_if_provide_status_sim.supply_vtg_evt &= ~(htr_msk);
            htr_status->supply_voltage = sta;
        }
    }
}


#ifdef IUG_USER_API_GHI_HEATER_DEMO_EN
    #warning "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
    #warning "Enable 'IUG_USER_API_GHI_HEATER_DEMO_EN' for development only!"
    #warning "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
#endif


