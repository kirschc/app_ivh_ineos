#ifndef USER_LIN_H
#define USER_LIN_H

#include "user_code.h"




enum_header_received_callback user_lin_header_received_callback(const uint8_t lin_module, const uint8_t rx_header_id, const uint8_t frame_index);

void user_lin_frame_received_callback(const uint8_t lin_module, const struct_lin_rx_msg_type lin_msg, const uint8_t frame_index);

void user_lin_schedule_complete_callback(const uint8_t lin_module, const uint8_t lin_schedule_table_index);

void user_lin_frame_transmission_complete_callback(const uint8_t lin_module, const uint8_t tx_header_id, const uint8_t frame_index);


#endif 

