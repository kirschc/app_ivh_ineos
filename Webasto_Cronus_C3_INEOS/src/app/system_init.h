#ifndef _SYSTEM_INIT_H_
#define _SYSTEM_INIT_H_

#include "hal_data_types.h"
#include "sfl_bl_protocol.h"



typedef struct
{
    uint8_t appl_sw_ver[ (PAR_LEN_SW_VERSION) ];      
    uint8_t appl_module_name[ (PAR_LEN_MODULENAME) ]; 
} struct_dyn_system_data;

typedef struct
{
    uint8_t  hw_can_bus_idx;   
    uint16_t pid_req_interval; 
    uint16_t can_id_11bit_req; 
    uint16_t can_id_11bit_res; 
    uint32_t can_id_29bit_req; 
    uint32_t can_id_29bit_res; 
} struct_dyn_obd_data;

typedef struct
{
    struct_dyn_system_data dyn_sys_data; 
    struct_dyn_obd_data    dyn_obd_data; 
} struct_shared_dyn_data;

extern struct_shared_dyn_data ext_shared_dyn_data;


void sys_init_set_dyn_values(void);


#endif 


