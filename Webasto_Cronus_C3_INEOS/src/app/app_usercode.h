#ifndef _USERCODE_H_
#define _USERCODE_H_


#define CAN1   (&ext_can_handle[CAN_1]) 
#define CAN2   (&ext_can_handle[CAN_2]) 
#define CAN3   (&ext_can_handle[CAN_3]) 

#define APP_USRCOD_INIT_ERRFLG_NONE                    0x00u
#define APP_USRCOD_INIT_ERRFLG_SET_DTC_IUG_SW_ERROR    0x01u
#define APP_USRCOD_INIT_ERRFLG_SET_DTC_IUG_NVM_ERROR   0x02u
#define APP_USRCOD_04                                  0x04u
#define APP_USRCOD_08                                  0x08u
#define APP_USRCOD_INIT_ERRFLG_REINIT_PCM_STORAGE      0x10u
#define APP_USRCOD_INIT_ERRFLG_CREATE_CAN_RX_BUF       0x20u
#define APP_USRCOD_40                                  0x40u
#define APP_USRCOD_80                                  0x80u


typedef enum
{
    CAN_0 = 0u, 
    CAN_1 = 1u, 
    CAN_2 = 2u, 
    CAN_3 = 3u, 
    CAN_4 = 4u, 
    CAN_5 = 5u, 
    CAN_MAX     
} enum_CAN_INTERFACES;

extern struct_hal_can_handle ext_can_handle[CAN_MAX];


void app_usercode_init(void);

void usercode_1ms(void);

void usercode_10ms(void);

void usercode_20ms(void);

void usercode_100ms(void);

void usercode_customer(void);

enum_HAL_CAN_RETURN_VALUE can1_rx_callback(struct_hal_can_frame* ptr_can_msg);

enum_HAL_CAN_RETURN_VALUE can2_rx_callback(struct_hal_can_frame* ptr_can_msg);

enum_HAL_CAN_RETURN_VALUE can3_rx_callback(struct_hal_can_frame* ptr_can_msg);


#endif


