#ifndef _IUG_UTILITY_GHI_READ_ID_H_
#define _IUG_UTILITY_GHI_READ_ID_H_



extern const IUG_UNIT_READ_ID_CFG_t         IUG_UNIT_GHI_READ_ID_CFG[];
extern const uint8_t                        ext_iug_entries_IUG_UNIT_GHI_READ_ID_CFG;



void IUG_ghi_heater_read_id_interface( IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl, const IUG_HEATER_UNIT_ENUM_t idx_htr );
boolean IUG_ghi_heater_read_id_receive_response( const IUG_HEATER_UNIT_ENUM_t rsp_for_ghi_htr,
                                                 IUG_QUEUE_KEY_t  wbus_read_id_req,
                                                 struct_api_ghi_heater_interface_htr_id  * const p_htr_id );
boolean IUG_ghi_heater_read_id_send_request( const IUG_HEATER_UNIT_ENUM_t cmd_for_ghi_htr,
                                             IUG_QUEUE_KEY_t wbus_read_id_data );
boolean IUG_ghi_heater_read_id_test_free( void );


#endif 

