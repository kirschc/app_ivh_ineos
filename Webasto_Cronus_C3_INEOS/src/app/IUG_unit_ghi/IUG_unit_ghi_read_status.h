#ifndef _IUG_UTILITY_GHI_READ_STATUS_H_
#define _IUG_UTILITY_GHI_READ_STATUS_H_


#define IUG_UNIT_GHI_READ_STATUS_NONE                       0x00u
#define IUG_UNIT_GHI_READ_STATUS_LIST_HEATER_HIGH           0x01u 
#if 0
#define IUG_UNIT_GHI_READ_STATUS_LIST_HEATER_MEDIUM         0x02u 
#define IUG_UNIT_GHI_READ_STATUS_LIST_HEATER_LOW            0x04u 
#else
#define IUG_UNIT_GHI_READ_STATUS_LIST_HEATER_MEDIUM         0x00u 
#define IUG_UNIT_GHI_READ_STATUS_LIST_HEATER_LOW            0x00u 
#endif
#define IUG_UNIT_GHI_READ_STATUS_08                         0x08u
#define IUG_UNIT_GHI_READ_STATUS_10                         0x10u
#define IUG_UNIT_GHI_READ_STATUS_20                         0x20u
#define IUG_UNIT_GHI_READ_STATUS_40                         0x40u
#define IUG_UNIT_GHI_READ_STATUS_80                         0x80u

#define IUG_UNIT_GHI_READ_STATUS_HG_RELATED     ((IUG_UNIT_GHI_READ_STATUS_LIST_HEATER_HIGH)    |\
                                                 (IUG_UNIT_GHI_READ_STATUS_LIST_HEATER_MEDIUM)  |\
                                                 (IUG_UNIT_GHI_READ_STATUS_LIST_HEATER_LOW)      \
                                                 )


#define IUG_RDST_ID_GHI_NONE                            0x00u   
#define IUG_GHI_RDST_ID_STATUS_STATE_NUM_2BYTE          0x29u   




extern const IUG_UNIT_READ_STATUS_CFG_t     IUG_UNIT_GHI_READ_STATUS_CFG[];
extern const uint8_t                        ext_iug_entries_IUG_UNIT_GHI_READ_STATUS_CFG;



void    IUG_ghi_heater_read_status_cyclic( IUG_UNIT_READ_STATUS_t * const p_rd_st, const IUG_HEATER_UNIT_ENUM_t idx_htr );
void    IUG_ghi_heater_read_status_interface( IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl, const IUG_HEATER_UNIT_ENUM_t idx_htr );
boolean IUG_ghi_heater_read_status_send_request( const IUG_HEATER_UNIT_ENUM_t cmd_for_ghi_htr,
                                                 IUG_QUEUE_KEY_t wbus_read_st_req );
boolean IUG_ghi_heater_read_status_receive_response( const IUG_HEATER_UNIT_ENUM_t ack_for_htr,
                                                     struct_api_ghi_heater_interface_htr_status * const ghi_htr_status );



#endif 

