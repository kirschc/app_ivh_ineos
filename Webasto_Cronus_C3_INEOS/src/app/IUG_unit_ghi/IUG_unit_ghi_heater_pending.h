#ifndef __IUG_UNIT_LIN_HEATER_PENDING_H_
#define __IUG_UNIT_LIN_HEATER_PENDING_H_




void    IUG_ghi_heater_pending_interface( IUG_UNIT_GHI_HEATER_CONTROL_t * const p_unt_ctl );
boolean IUG_ghi_heater_pending_send_request( const IUG_HEATER_UNIT_ENUM_t pending_for_ghi_htr, IUG_QUEUE_KEY_t pending_req_key );
boolean IUG_ghi_heater_pending_receive_response( const IUG_HEATER_UNIT_ENUM_t rsp_for_ghi_htr, const uint8_t pend_ctl );


#endif 

