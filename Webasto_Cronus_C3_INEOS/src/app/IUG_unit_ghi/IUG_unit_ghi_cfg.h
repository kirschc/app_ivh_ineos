#ifndef _IUG_UNIT_GHI_CFG_H_
#define _IUG_UNIT_GHI_CFG_H_





#define IUG_UNT_GHI_HEATER_1_READ_ID_DATA_ADDRESS_CFG \
{\
     \
    \
    { (WBUS_MSGSRV_0A_ID_WBUS_VERSION)      ,0u                           ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_1_CTL_UNI.htr_ghi_ctl.heater_data_raw.WBUS_VERSION_raw        },\
    { (WBUS_MSGSRV_0C_ID_ECU_CODING)        ,0u                           ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_1_CTL_UNI.htr_ghi_ctl.heater_data_raw.ECU_CODING_1_raw        },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_300B_ID_GET_SERIAL_NUMBER)        ,(IUG_RDST_SIZE_5)  ,&IUG_UNIT_HEATER_1_CTL_UNI.htr_ghi_ctl.heater_data_raw.SERIAL_NUMBER           },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_300D_ID_WBUS_VERSION)             ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_1_CTL_UNI.htr_ghi_ctl.heater_data_raw.WBUS_VERSION_raw        },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_300E_ID_GET_HEATER_SIGN)          ,(IUG_RDST_SIZE_U8) ,&IUG_HEATER_PROPERTY_NVM[(IUG_HEATER_UNIT_HG1)].unit_htr_sign                  },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_3012_ID_GET_MAX_HEATING_TIME)     ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_1_CTL_UNI.htr_ghi_ctl.heater_data_raw.MAX_HEATING_TIME_raw    },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_301D_ID_GET_HEATER_TYPE)          ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_1_CTL_UNI.htr_ghi_ctl.heater_data_raw.HEATER_TYPE_INFO_raw    },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_301F_ID_GET_HEATER_NAME)          ,(IUG_RDST_SIZE_24)  ,(NULL)                                          },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_3020_ID_GET_HEATER_CAPABLTY)      ,(IUG_RDST_SIZE_U16),&IUG_UNIT_HEATER_1_CTL_UNI.htr_ghi_ctl.heater_data_raw.HEATER_CAPABILITY_raw   },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_3022_ID_GET_HEATER_MODE_FUNC)     ,(IUG_RDST_SIZE_U16),&IUG_UNIT_HEATER_1_CTL_UNI.htr_wbus_ctl.heater_data_raw.HEATER_MODE_FUNC_raw   } \
}


#define IUG_UNT_GHI_HEATER_2_READ_ID_DATA_ADDRESS_CFG \
{\
     \
    \
    { (WBUS_MSGSRV_0A_ID_WBUS_VERSION)      ,0u                           ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_2_CTL_UNI.htr_ghi_ctl.heater_data_raw.WBUS_VERSION_raw        },\
    { (WBUS_MSGSRV_0C_ID_ECU_CODING)        ,0u                           ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_2_CTL_UNI.htr_ghi_ctl.heater_data_raw.ECU_CODING_1_raw        },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_300B_ID_GET_SERIAL_NUMBER)        ,(IUG_RDST_SIZE_5)  ,&IUG_UNIT_HEATER_2_CTL_UNI.htr_ghi_ctl.heater_data_raw.SERIAL_NUMBER           },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_300D_ID_WBUS_VERSION)             ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_2_CTL_UNI.htr_ghi_ctl.heater_data_raw.WBUS_VERSION_raw        },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_300E_ID_GET_HEATER_SIGN)          ,(IUG_RDST_SIZE_U8) ,&IUG_HEATER_PROPERTY_NVM[(IUG_HEATER_UNIT_HG2)].unit_htr_sign                  },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_3012_ID_GET_MAX_HEATING_TIME)     ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_2_CTL_UNI.htr_ghi_ctl.heater_data_raw.MAX_HEATING_TIME_raw    },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_301D_ID_GET_HEATER_TYPE)          ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_2_CTL_UNI.htr_ghi_ctl.heater_data_raw.HEATER_TYPE_INFO_raw    },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_301F_ID_GET_HEATER_NAME)          ,(IUG_RDST_SIZE_24)  ,(NULL)                                          },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_3020_ID_GET_HEATER_CAPABLTY)      ,(IUG_RDST_SIZE_U16),&IUG_UNIT_HEATER_2_CTL_UNI.htr_ghi_ctl.heater_data_raw.HEATER_CAPABILITY_raw   },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_3022_ID_GET_HEATER_MODE_FUNC)     ,(IUG_RDST_SIZE_U16),&IUG_UNIT_HEATER_2_CTL_UNI.htr_wbus_ctl.heater_data_raw.HEATER_MODE_FUNC_raw   } \
}


#define IUG_UNT_GHI_HEATER_3_READ_ID_DATA_ADDRESS_CFG \
{\
     \
    \
    { (WBUS_MSGSRV_0A_ID_WBUS_VERSION)      ,0u                           ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_3_CTL_UNI.htr_ghi_ctl.heater_data_raw.WBUS_VERSION_raw        },\
    { (WBUS_MSGSRV_0C_ID_ECU_CODING)        ,0u                           ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_3_CTL_UNI.htr_ghi_ctl.heater_data_raw.ECU_CODING_1_raw        },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_300B_ID_GET_SERIAL_NUMBER)        ,(IUG_RDST_SIZE_5)  ,&IUG_UNIT_HEATER_3_CTL_UNI.htr_ghi_ctl.heater_data_raw.SERIAL_NUMBER           },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_300D_ID_WBUS_VERSION)             ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_3_CTL_UNI.htr_ghi_ctl.heater_data_raw.WBUS_VERSION_raw        },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_300E_ID_GET_HEATER_SIGN)          ,(IUG_RDST_SIZE_U8) ,&IUG_HEATER_PROPERTY_NVM[(IUG_HEATER_UNIT_HG3)].unit_htr_sign      },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_3012_ID_GET_MAX_HEATING_TIME)     ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_3_CTL_UNI.htr_ghi_ctl.heater_data_raw.MAX_HEATING_TIME_raw    },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_301D_ID_GET_HEATER_TYPE)          ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_3_CTL_UNI.htr_ghi_ctl.heater_data_raw.HEATER_TYPE_INFO_raw    },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_301F_ID_GET_HEATER_NAME)          ,(IUG_RDST_SIZE_24)  ,(NULL)                              },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_3020_ID_GET_HEATER_CAPABLTY)      ,(IUG_RDST_SIZE_U16),&IUG_UNIT_HEATER_3_CTL_UNI.htr_ghi_ctl.heater_data_raw.HEATER_CAPABILITY_raw   },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_3022_ID_GET_HEATER_MODE_FUNC)     ,(IUG_RDST_SIZE_U16),&IUG_UNIT_HEATER_3_CTL_UNI.htr_wbus_ctl.heater_data_raw.HEATER_MODE_FUNC_raw   } \
}


#define IUG_UNT_GHI_HEATER_4_READ_ID_DATA_ADDRESS_CFG \
{\
     \
    \
    { (WBUS_MSGSRV_0A_ID_WBUS_VERSION)      ,0u                           ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_4_CTL_UNI.htr_ghi_ctl.heater_data_raw.WBUS_VERSION_raw        },\
    { (WBUS_MSGSRV_0C_ID_ECU_CODING)        ,0u                           ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_4_CTL_UNI.htr_ghi_ctl.heater_data_raw.ECU_CODING_1_raw        },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_300B_ID_GET_SERIAL_NUMBER)        ,(IUG_RDST_SIZE_5)  ,&IUG_UNIT_HEATER_4_CTL_UNI.htr_ghi_ctl.heater_data_raw.SERIAL_NUMBER           },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_300D_ID_WBUS_VERSION)             ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_4_CTL_UNI.htr_ghi_ctl.heater_data_raw.WBUS_VERSION_raw        },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_300E_ID_GET_HEATER_SIGN)          ,(IUG_RDST_SIZE_U8) ,&IUG_HEATER_PROPERTY_NVM[(IUG_HEATER_UNIT_HG4)].unit_htr_sign      },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_3012_ID_GET_MAX_HEATING_TIME)     ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_4_CTL_UNI.htr_ghi_ctl.heater_data_raw.MAX_HEATING_TIME_raw    },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_301D_ID_GET_HEATER_TYPE)          ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_4_CTL_UNI.htr_ghi_ctl.heater_data_raw.HEATER_TYPE_INFO_raw    },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_301F_ID_GET_HEATER_NAME)          ,(IUG_RDST_SIZE_24)  ,(NULL)                              },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_3020_ID_GET_HEATER_CAPABLTY)      ,(IUG_RDST_SIZE_U16),&IUG_UNIT_HEATER_4_CTL_UNI.htr_ghi_ctl.heater_data_raw.HEATER_CAPABILITY_raw   },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_3022_ID_GET_HEATER_MODE_FUNC)     ,(IUG_RDST_SIZE_U16),&IUG_UNIT_HEATER_4_CTL_UNI.htr_wbus_ctl.heater_data_raw.HEATER_MODE_FUNC_raw   } \
}


#define IUG_UNT_GHI_HEATER_5_READ_ID_DATA_ADDRESS_CFG \
{\
     \
    \
    { (WBUS_MSGSRV_0A_ID_WBUS_VERSION)      ,0u                           ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_5_CTL_UNI.htr_ghi_ctl.heater_data_raw.WBUS_VERSION_raw        },\
    { (WBUS_MSGSRV_0C_ID_ECU_CODING)        ,0u                           ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_5_CTL_UNI.htr_ghi_ctl.heater_data_raw.ECU_CODING_1_raw        },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_300B_ID_GET_SERIAL_NUMBER)        ,(IUG_RDST_SIZE_5)  ,&IUG_UNIT_HEATER_5_CTL_UNI.htr_ghi_ctl.heater_data_raw.SERIAL_NUMBER           },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_300D_ID_WBUS_VERSION)             ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_5_CTL_UNI.htr_ghi_ctl.heater_data_raw.WBUS_VERSION_raw        },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_300E_ID_GET_HEATER_SIGN)          ,(IUG_RDST_SIZE_U8) ,&IUG_HEATER_PROPERTY_NVM[(IUG_HEATER_UNIT_HG5)].unit_htr_sign      },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_3012_ID_GET_MAX_HEATING_TIME)     ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_5_CTL_UNI.htr_ghi_ctl.heater_data_raw.MAX_HEATING_TIME_raw    },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_301D_ID_GET_HEATER_TYPE)          ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_5_CTL_UNI.htr_ghi_ctl.heater_data_raw.HEATER_TYPE_INFO_raw    },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_301F_ID_GET_HEATER_NAME)          ,(IUG_RDST_SIZE_24)  ,(NULL)                              },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_3020_ID_GET_HEATER_CAPABLTY)      ,(IUG_RDST_SIZE_U16),&IUG_UNIT_HEATER_5_CTL_UNI.htr_ghi_ctl.heater_data_raw.HEATER_CAPABILITY_raw   },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_3022_ID_GET_HEATER_MODE_FUNC)     ,(IUG_RDST_SIZE_U16),&IUG_UNIT_HEATER_5_CTL_UNI.htr_wbus_ctl.heater_data_raw.HEATER_MODE_FUNC_raw   } \
}


#define IUG_UNT_GHI_HEATER_6_READ_ID_DATA_ADDRESS_CFG \
{\
     \
    \
    { (WBUS_MSGSRV_0A_ID_WBUS_VERSION)      ,0u                           ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_6_CTL_UNI.htr_ghi_ctl.heater_data_raw.WBUS_VERSION_raw        },\
    { (WBUS_MSGSRV_0C_ID_ECU_CODING)        ,0u                           ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_6_CTL_UNI.htr_ghi_ctl.heater_data_raw.ECU_CODING_1_raw        },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_300B_ID_GET_SERIAL_NUMBER)        ,(IUG_RDST_SIZE_5)  ,&IUG_UNIT_HEATER_6_CTL_UNI.htr_ghi_ctl.heater_data_raw.SERIAL_NUMBER           },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_300D_ID_WBUS_VERSION)             ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_6_CTL_UNI.htr_ghi_ctl.heater_data_raw.WBUS_VERSION_raw        },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_300E_ID_GET_HEATER_SIGN)          ,(IUG_RDST_SIZE_U8) ,&IUG_HEATER_PROPERTY_NVM[(IUG_HEATER_UNIT_HG6)].unit_htr_sign      },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_3012_ID_GET_MAX_HEATING_TIME)     ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_6_CTL_UNI.htr_ghi_ctl.heater_data_raw.MAX_HEATING_TIME_raw    },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_301D_ID_GET_HEATER_TYPE)          ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_6_CTL_UNI.htr_ghi_ctl.heater_data_raw.HEATER_TYPE_INFO_raw    },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_301F_ID_GET_HEATER_NAME)          ,(IUG_RDST_SIZE_24)  ,(NULL)                              },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_3020_ID_GET_HEATER_CAPABLTY)      ,(IUG_RDST_SIZE_U16),&IUG_UNIT_HEATER_6_CTL_UNI.htr_ghi_ctl.heater_data_raw.HEATER_CAPABILITY_raw   },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_3022_ID_GET_HEATER_MODE_FUNC)     ,(IUG_RDST_SIZE_U16),&IUG_UNIT_HEATER_6_CTL_UNI.htr_wbus_ctl.heater_data_raw.HEATER_MODE_FUNC_raw   } \
}


#define IUG_UNT_GHI_HEATER_7_READ_ID_DATA_ADDRESS_CFG \
{\
     \
    \
    { (WBUS_MSGSRV_0A_ID_WBUS_VERSION)      ,0u                           ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_7_CTL_UNI.htr_ghi_ctl.heater_data_raw.WBUS_VERSION_raw        },\
    { (WBUS_MSGSRV_0C_ID_ECU_CODING)        ,0u                           ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_7_CTL_UNI.htr_ghi_ctl.heater_data_raw.ECU_CODING_1_raw        },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_300B_ID_GET_SERIAL_NUMBER)        ,(IUG_RDST_SIZE_5)  ,&IUG_UNIT_HEATER_7_CTL_UNI.htr_ghi_ctl.heater_data_raw.SERIAL_NUMBER           },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_300D_ID_WBUS_VERSION)             ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_7_CTL_UNI.htr_ghi_ctl.heater_data_raw.WBUS_VERSION_raw        },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_300E_ID_GET_HEATER_SIGN)          ,(IUG_RDST_SIZE_U8) ,&IUG_HEATER_PROPERTY_NVM[(IUG_HEATER_UNIT_HG7)].unit_htr_sign      },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_3012_ID_GET_MAX_HEATING_TIME)     ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_7_CTL_UNI.htr_ghi_ctl.heater_data_raw.MAX_HEATING_TIME_raw    },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_301D_ID_GET_HEATER_TYPE)          ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_7_CTL_UNI.htr_ghi_ctl.heater_data_raw.HEATER_TYPE_INFO_raw    },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_301F_ID_GET_HEATER_NAME)          ,(IUG_RDST_SIZE_24)  ,(NULL)                              },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_3020_ID_GET_HEATER_CAPABLTY)      ,(IUG_RDST_SIZE_U16),&IUG_UNIT_HEATER_7_CTL_UNI.htr_ghi_ctl.heater_data_raw.HEATER_CAPABILITY_raw   },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_3022_ID_GET_HEATER_MODE_FUNC)     ,(IUG_RDST_SIZE_U16),&IUG_UNIT_HEATER_7_CTL_UNI.htr_wbus_ctl.heater_data_raw.HEATER_MODE_FUNC_raw   } \
}


#define IUG_UNT_GHI_HEATER_8_READ_ID_DATA_ADDRESS_CFG \
{\
     \
    \
    { (WBUS_MSGSRV_0A_ID_WBUS_VERSION)      ,0u                           ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_8_CTL_UNI.htr_ghi_ctl.heater_data_raw.WBUS_VERSION_raw        },\
    { (WBUS_MSGSRV_0C_ID_ECU_CODING)        ,0u                           ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_8_CTL_UNI.htr_ghi_ctl.heater_data_raw.ECU_CODING_1_raw        },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_300B_ID_GET_SERIAL_NUMBER)        ,(IUG_RDST_SIZE_5)  ,&IUG_UNIT_HEATER_8_CTL_UNI.htr_ghi_ctl.heater_data_raw.SERIAL_NUMBER           },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_300D_ID_WBUS_VERSION)             ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_8_CTL_UNI.htr_ghi_ctl.heater_data_raw.WBUS_VERSION_raw        },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_300E_ID_GET_HEATER_SIGN)          ,(IUG_RDST_SIZE_U8) ,&IUG_HEATER_PROPERTY_NVM[(IUG_HEATER_UNIT_HG8)].unit_htr_sign      },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_3012_ID_GET_MAX_HEATING_TIME)     ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_8_CTL_UNI.htr_ghi_ctl.heater_data_raw.MAX_HEATING_TIME_raw    },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_301D_ID_GET_HEATER_TYPE)          ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_8_CTL_UNI.htr_ghi_ctl.heater_data_raw.HEATER_TYPE_INFO_raw    },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_301F_ID_GET_HEATER_NAME)          ,(IUG_RDST_SIZE_24)  ,(NULL)                              },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_3020_ID_GET_HEATER_CAPABLTY)      ,(IUG_RDST_SIZE_U16),&IUG_UNIT_HEATER_8_CTL_UNI.htr_ghi_ctl.heater_data_raw.HEATER_CAPABILITY_raw   },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_3022_ID_GET_HEATER_MODE_FUNC)     ,(IUG_RDST_SIZE_U16),&IUG_UNIT_HEATER_8_CTL_UNI.htr_wbus_ctl.heater_data_raw.HEATER_MODE_FUNC_raw   } \
}



#define IUG_UNT_GHI_HEATER_1_READ_STATUS_DATA_ADDRESS_CFG \
{\
     \
    \
    { (IUG_RDST_ID_0A_STATUS_OUTPORT_HEATER)        ,(&IUG_UNIT_HEATER_1_CTL_UNI.htr_ghi_ctl.heater_data_raw.STATUS_OUTPORT_raw) },\
    { (IUG_RDST_ID_0C_STATUS_COOL_MEDIUM_TEMP1)     ,(&IUG_UNIT_HEATER_1_CTL_UNI.htr_ghi_ctl.heater_data_raw.COOLANT_TEMP_1_raw) },\
    { (IUG_RDST_ID_0E_STATUS_SUPPLY_VOLTAGE)        ,(&IUG_UNIT_HEATER_1_CTL_UNI.htr_ghi_ctl.heater_data_raw.SUPPLY_VOLTAGE_raw) },\
    { (IUG_RDST_ID_29_STATUS_STATE_NUM_2BYTE)       ,(&IUG_UNIT_HEATER_1_CTL_UNI.htr_ghi_ctl.heater_data_raw.HG_STATE_NUM_2_raw) },\
    { (IUG_RDST_ID_2A_STATUS_STATE_STFL_FAULTY)     ,(&IUG_UNIT_HEATER_1_CTL_UNI.htr_ghi_ctl.heater_data_raw.HG_STATE_STFL_raw)  },\
    { (IUG_RDST_ID_2B_STATUS_STATE_UEHFL_OVERHEAT)  ,(&IUG_UNIT_HEATER_1_CTL_UNI.htr_ghi_ctl.heater_data_raw.HG_STATE_UEHFL_raw) },\
    { (IUG_RDST_ID_2C_STATUS_STATE_HGV_LOCKED)      ,(&IUG_UNIT_HEATER_1_CTL_UNI.htr_ghi_ctl.heater_data_raw.HG_STATE_HGVP_raw)  },\
    \
}


#define IUG_UNT_GHI_HEATER_2_READ_STATUS_DATA_ADDRESS_CFG \
{\
     \
    \
    { (IUG_RDST_ID_0A_STATUS_OUTPORT_HEATER)        ,(&IUG_UNIT_HEATER_2_CTL_UNI.htr_ghi_ctl.heater_data_raw.STATUS_OUTPORT_raw) },\
    { (IUG_RDST_ID_0C_STATUS_COOL_MEDIUM_TEMP1)     ,(&IUG_UNIT_HEATER_2_CTL_UNI.htr_ghi_ctl.heater_data_raw.COOLANT_TEMP_1_raw) },\
    { (IUG_RDST_ID_0E_STATUS_SUPPLY_VOLTAGE)        ,(&IUG_UNIT_HEATER_2_CTL_UNI.htr_ghi_ctl.heater_data_raw.SUPPLY_VOLTAGE_raw) },\
    { (IUG_RDST_ID_29_STATUS_STATE_NUM_2BYTE)       ,(&IUG_UNIT_HEATER_2_CTL_UNI.htr_ghi_ctl.heater_data_raw.HG_STATE_NUM_2_raw) },\
    { (IUG_RDST_ID_2A_STATUS_STATE_STFL_FAULTY)     ,(&IUG_UNIT_HEATER_2_CTL_UNI.htr_ghi_ctl.heater_data_raw.HG_STATE_STFL_raw)  },\
    { (IUG_RDST_ID_2B_STATUS_STATE_UEHFL_OVERHEAT)  ,(&IUG_UNIT_HEATER_2_CTL_UNI.htr_ghi_ctl.heater_data_raw.HG_STATE_UEHFL_raw) },\
    { (IUG_RDST_ID_2C_STATUS_STATE_HGV_LOCKED)      ,(&IUG_UNIT_HEATER_2_CTL_UNI.htr_ghi_ctl.heater_data_raw.HG_STATE_HGVP_raw)  },\
    \
}


#define IUG_UNT_GHI_HEATER_3_READ_STATUS_DATA_ADDRESS_CFG \
{\
     \
    \
    { (IUG_RDST_ID_0A_STATUS_OUTPORT_HEATER)        ,(&IUG_UNIT_HEATER_3_CTL_UNI.htr_ghi_ctl.heater_data_raw.STATUS_OUTPORT_raw) },\
    { (IUG_RDST_ID_0C_STATUS_COOL_MEDIUM_TEMP1)     ,(&IUG_UNIT_HEATER_3_CTL_UNI.htr_ghi_ctl.heater_data_raw.COOLANT_TEMP_1_raw) },\
    { (IUG_RDST_ID_0E_STATUS_SUPPLY_VOLTAGE)        ,(&IUG_UNIT_HEATER_3_CTL_UNI.htr_ghi_ctl.heater_data_raw.SUPPLY_VOLTAGE_raw) },\
    { (IUG_RDST_ID_29_STATUS_STATE_NUM_2BYTE)       ,(&IUG_UNIT_HEATER_3_CTL_UNI.htr_ghi_ctl.heater_data_raw.HG_STATE_NUM_2_raw) },\
    { (IUG_RDST_ID_2A_STATUS_STATE_STFL_FAULTY)     ,(&IUG_UNIT_HEATER_3_CTL_UNI.htr_ghi_ctl.heater_data_raw.HG_STATE_STFL_raw)  },\
    { (IUG_RDST_ID_2B_STATUS_STATE_UEHFL_OVERHEAT)  ,(&IUG_UNIT_HEATER_3_CTL_UNI.htr_ghi_ctl.heater_data_raw.HG_STATE_UEHFL_raw) },\
    { (IUG_RDST_ID_2C_STATUS_STATE_HGV_LOCKED)      ,(&IUG_UNIT_HEATER_3_CTL_UNI.htr_ghi_ctl.heater_data_raw.HG_STATE_HGVP_raw)  },\
    \
}


#define IUG_UNT_GHI_HEATER_4_READ_STATUS_DATA_ADDRESS_CFG \
{\
     \
    \
    { (IUG_RDST_ID_0A_STATUS_OUTPORT_HEATER)        ,(&IUG_UNIT_HEATER_4_CTL_UNI.htr_ghi_ctl.heater_data_raw.STATUS_OUTPORT_raw) },\
    { (IUG_RDST_ID_0C_STATUS_COOL_MEDIUM_TEMP1)     ,(&IUG_UNIT_HEATER_4_CTL_UNI.htr_ghi_ctl.heater_data_raw.COOLANT_TEMP_1_raw) },\
    { (IUG_RDST_ID_0E_STATUS_SUPPLY_VOLTAGE)        ,(&IUG_UNIT_HEATER_4_CTL_UNI.htr_ghi_ctl.heater_data_raw.SUPPLY_VOLTAGE_raw) },\
    { (IUG_RDST_ID_29_STATUS_STATE_NUM_2BYTE)       ,(&IUG_UNIT_HEATER_4_CTL_UNI.htr_ghi_ctl.heater_data_raw.HG_STATE_NUM_2_raw) },\
    { (IUG_RDST_ID_2A_STATUS_STATE_STFL_FAULTY)     ,(&IUG_UNIT_HEATER_4_CTL_UNI.htr_ghi_ctl.heater_data_raw.HG_STATE_STFL_raw)  },\
    { (IUG_RDST_ID_2B_STATUS_STATE_UEHFL_OVERHEAT)  ,(&IUG_UNIT_HEATER_4_CTL_UNI.htr_ghi_ctl.heater_data_raw.HG_STATE_UEHFL_raw) },\
    { (IUG_RDST_ID_2C_STATUS_STATE_HGV_LOCKED)      ,(&IUG_UNIT_HEATER_4_CTL_UNI.htr_ghi_ctl.heater_data_raw.HG_STATE_HGVP_raw)  },\
    \
}


#define IUG_UNT_GHI_HEATER_5_READ_STATUS_DATA_ADDRESS_CFG \
{\
     \
    \
    { (IUG_RDST_ID_0A_STATUS_OUTPORT_HEATER)        ,(&IUG_UNIT_HEATER_5_CTL_UNI.htr_ghi_ctl.heater_data_raw.STATUS_OUTPORT_raw) },\
    { (IUG_RDST_ID_0C_STATUS_COOL_MEDIUM_TEMP1)     ,(&IUG_UNIT_HEATER_5_CTL_UNI.htr_ghi_ctl.heater_data_raw.COOLANT_TEMP_1_raw) },\
    { (IUG_RDST_ID_0E_STATUS_SUPPLY_VOLTAGE)        ,(&IUG_UNIT_HEATER_5_CTL_UNI.htr_ghi_ctl.heater_data_raw.SUPPLY_VOLTAGE_raw) },\
    { (IUG_RDST_ID_29_STATUS_STATE_NUM_2BYTE)       ,(&IUG_UNIT_HEATER_5_CTL_UNI.htr_ghi_ctl.heater_data_raw.HG_STATE_NUM_2_raw) },\
    { (IUG_RDST_ID_2A_STATUS_STATE_STFL_FAULTY)     ,(&IUG_UNIT_HEATER_5_CTL_UNI.htr_ghi_ctl.heater_data_raw.HG_STATE_STFL_raw)  },\
    { (IUG_RDST_ID_2B_STATUS_STATE_UEHFL_OVERHEAT)  ,(&IUG_UNIT_HEATER_5_CTL_UNI.htr_ghi_ctl.heater_data_raw.HG_STATE_UEHFL_raw) },\
    { (IUG_RDST_ID_2C_STATUS_STATE_HGV_LOCKED)      ,(&IUG_UNIT_HEATER_5_CTL_UNI.htr_ghi_ctl.heater_data_raw.HG_STATE_HGVP_raw)  },\
    \
}


#define IUG_UNT_GHI_HEATER_6_READ_STATUS_DATA_ADDRESS_CFG \
{\
     \
    \
    { (IUG_RDST_ID_0A_STATUS_OUTPORT_HEATER)        ,(&IUG_UNIT_HEATER_6_CTL_UNI.htr_ghi_ctl.heater_data_raw.STATUS_OUTPORT_raw) },\
    { (IUG_RDST_ID_0C_STATUS_COOL_MEDIUM_TEMP1)     ,(&IUG_UNIT_HEATER_6_CTL_UNI.htr_ghi_ctl.heater_data_raw.COOLANT_TEMP_1_raw) },\
    { (IUG_RDST_ID_0E_STATUS_SUPPLY_VOLTAGE)        ,(&IUG_UNIT_HEATER_6_CTL_UNI.htr_ghi_ctl.heater_data_raw.SUPPLY_VOLTAGE_raw) },\
    { (IUG_RDST_ID_29_STATUS_STATE_NUM_2BYTE)       ,(&IUG_UNIT_HEATER_6_CTL_UNI.htr_ghi_ctl.heater_data_raw.HG_STATE_NUM_2_raw) },\
    { (IUG_RDST_ID_2A_STATUS_STATE_STFL_FAULTY)     ,(&IUG_UNIT_HEATER_6_CTL_UNI.htr_ghi_ctl.heater_data_raw.HG_STATE_STFL_raw)  },\
    { (IUG_RDST_ID_2B_STATUS_STATE_UEHFL_OVERHEAT)  ,(&IUG_UNIT_HEATER_6_CTL_UNI.htr_ghi_ctl.heater_data_raw.HG_STATE_UEHFL_raw) },\
    { (IUG_RDST_ID_2C_STATUS_STATE_HGV_LOCKED)      ,(&IUG_UNIT_HEATER_6_CTL_UNI.htr_ghi_ctl.heater_data_raw.HG_STATE_HGVP_raw)  },\
    \
}


#define IUG_UNT_GHI_HEATER_7_READ_STATUS_DATA_ADDRESS_CFG \
{\
     \
    \
    { (IUG_RDST_ID_0A_STATUS_OUTPORT_HEATER)        ,(&IUG_UNIT_HEATER_7_CTL_UNI.htr_ghi_ctl.heater_data_raw.STATUS_OUTPORT_raw) },\
    { (IUG_RDST_ID_0C_STATUS_COOL_MEDIUM_TEMP1)     ,(&IUG_UNIT_HEATER_7_CTL_UNI.htr_ghi_ctl.heater_data_raw.COOLANT_TEMP_1_raw) },\
    { (IUG_RDST_ID_0E_STATUS_SUPPLY_VOLTAGE)        ,(&IUG_UNIT_HEATER_7_CTL_UNI.htr_ghi_ctl.heater_data_raw.SUPPLY_VOLTAGE_raw) },\
    { (IUG_RDST_ID_29_STATUS_STATE_NUM_2BYTE)       ,(&IUG_UNIT_HEATER_7_CTL_UNI.htr_ghi_ctl.heater_data_raw.HG_STATE_NUM_2_raw) },\
    { (IUG_RDST_ID_2A_STATUS_STATE_STFL_FAULTY)     ,(&IUG_UNIT_HEATER_7_CTL_UNI.htr_ghi_ctl.heater_data_raw.HG_STATE_STFL_raw)  },\
    { (IUG_RDST_ID_2B_STATUS_STATE_UEHFL_OVERHEAT)  ,(&IUG_UNIT_HEATER_7_CTL_UNI.htr_ghi_ctl.heater_data_raw.HG_STATE_UEHFL_raw) },\
    { (IUG_RDST_ID_2C_STATUS_STATE_HGV_LOCKED)      ,(&IUG_UNIT_HEATER_7_CTL_UNI.htr_ghi_ctl.heater_data_raw.HG_STATE_HGVP_raw)  },\
    \
}


#define IUG_UNT_GHI_HEATER_8_READ_STATUS_DATA_ADDRESS_CFG \
{\
     \
    \
    { (IUG_RDST_ID_0A_STATUS_OUTPORT_HEATER)        ,(&IUG_UNIT_HEATER_8_CTL_UNI.htr_ghi_ctl.heater_data_raw.STATUS_OUTPORT_raw) },\
    { (IUG_RDST_ID_0C_STATUS_COOL_MEDIUM_TEMP1)     ,(&IUG_UNIT_HEATER_8_CTL_UNI.htr_ghi_ctl.heater_data_raw.COOLANT_TEMP_1_raw) },\
    { (IUG_RDST_ID_0E_STATUS_SUPPLY_VOLTAGE)        ,(&IUG_UNIT_HEATER_8_CTL_UNI.htr_ghi_ctl.heater_data_raw.SUPPLY_VOLTAGE_raw) },\
    { (IUG_RDST_ID_29_STATUS_STATE_NUM_2BYTE)       ,(&IUG_UNIT_HEATER_8_CTL_UNI.htr_ghi_ctl.heater_data_raw.HG_STATE_NUM_2_raw) },\
    { (IUG_RDST_ID_2A_STATUS_STATE_STFL_FAULTY)     ,(&IUG_UNIT_HEATER_8_CTL_UNI.htr_ghi_ctl.heater_data_raw.HG_STATE_STFL_raw)  },\
    { (IUG_RDST_ID_2B_STATUS_STATE_UEHFL_OVERHEAT)  ,(&IUG_UNIT_HEATER_8_CTL_UNI.htr_ghi_ctl.heater_data_raw.HG_STATE_UEHFL_raw) },\
    { (IUG_RDST_ID_2C_STATUS_STATE_HGV_LOCKED)      ,(&IUG_UNIT_HEATER_8_CTL_UNI.htr_ghi_ctl.heater_data_raw.HG_STATE_HGVP_raw)  },\
    \
}


#define IUG_GHI_HEATER_X_PROPERTY_ST_AVAILABLE_DEF \
            ( \
              (IUG_UNIT_GHI_READ_STATUS_LIST_HEATER_HIGH)  |\
              (IUG_UNIT_GHI_READ_STATUS_LIST_HEATER_MEDIUM)|\
              (IUG_UNIT_GHI_READ_STATUS_LIST_HEATER_LOW)    \
            )



extern const IUG_READ_ID_DATA_ADDRESS_CFG_t                 IUG_UNT_GHI_HEATER_1_RD_ID_DAT_ADDR_CFG[];
extern const uint8_t                                        ext_iug_entries_IUG_UNT_GHI_HEATER_1_RD_ID_DAT_ADDR_CFG;

extern const IUG_READ_STATUS_DATA_ADDRESS_CFG_t             IUG_UNT_GHI_HEATER_1_RD_ST_DAT_ADDR_CFG[];
extern const uint8_t                                        ext_iug_entries_IUG_UNT_GHI_HEATER_1_RD_ST_DAT_ADDR_CFG;


extern const IUG_READ_ID_DATA_ADDRESS_CFG_t                 IUG_UNT_GHI_HEATER_2_RD_ID_DAT_ADDR_CFG[];
extern const uint8_t                                        ext_iug_entries_IUG_UNT_GHI_HEATER_2_RD_ID_DAT_ADDR_CFG;

extern const IUG_READ_STATUS_DATA_ADDRESS_CFG_t             IUG_UNT_GHI_HEATER_2_RD_ST_DAT_ADDR_CFG[];
extern const uint8_t                                        ext_iug_entries_IUG_UNT_GHI_HEATER_2_RD_ST_DAT_ADDR_CFG;


#if( (IUG_UNT_HEATER_HG_LAST) == (IUG_UNT_HEATER_HG8_DEF) )
extern const IUG_READ_ID_DATA_ADDRESS_CFG_t                 IUG_UNT_GHI_HEATER_3_RD_ID_DAT_ADDR_CFG[];
extern const uint8_t                                        ext_iug_entries_IUG_UNT_GHI_HEATER_3_RD_ID_DAT_ADDR_CFG;

extern const IUG_READ_STATUS_DATA_ADDRESS_CFG_t             IUG_UNT_GHI_HEATER_3_RD_ST_DAT_ADDR_CFG[];
extern const uint8_t                                        ext_iug_entries_IUG_UNT_GHI_HEATER_3_RD_ST_DAT_ADDR_CFG;
#endif


#if( (IUG_UNT_HEATER_HG_LAST) == (IUG_UNT_HEATER_HG8_DEF) )
extern const IUG_READ_ID_DATA_ADDRESS_CFG_t                 IUG_UNT_GHI_HEATER_4_RD_ID_DAT_ADDR_CFG[];
extern const uint8_t                                        ext_iug_entries_IUG_UNT_GHI_HEATER_4_RD_ID_DAT_ADDR_CFG;

extern const IUG_READ_STATUS_DATA_ADDRESS_CFG_t             IUG_UNT_GHI_HEATER_4_RD_ST_DAT_ADDR_CFG[];
extern const uint8_t                                        ext_iug_entries_IUG_UNT_GHI_HEATER_4_RD_ST_DAT_ADDR_CFG;
#endif


#if( (IUG_UNT_HEATER_HG_LAST) == (IUG_UNT_HEATER_HG8_DEF) )
extern const IUG_READ_ID_DATA_ADDRESS_CFG_t                 IUG_UNT_GHI_HEATER_5_RD_ID_DAT_ADDR_CFG[];
extern const uint8_t                                        ext_iug_entries_IUG_UNT_GHI_HEATER_5_RD_ID_DAT_ADDR_CFG;

extern const IUG_READ_STATUS_DATA_ADDRESS_CFG_t             IUG_UNT_GHI_HEATER_5_RD_ST_DAT_ADDR_CFG[];
extern const uint8_t                                        ext_iug_entries_IUG_UNT_GHI_HEATER_5_RD_ST_DAT_ADDR_CFG;
#endif


#if( (IUG_UNT_HEATER_HG_LAST) == (IUG_UNT_HEATER_HG8_DEF) )
extern const IUG_READ_ID_DATA_ADDRESS_CFG_t                 IUG_UNT_GHI_HEATER_6_RD_ID_DAT_ADDR_CFG[];
extern const uint8_t                                        ext_iug_entries_IUG_UNT_GHI_HEATER_6_RD_ID_DAT_ADDR_CFG;

extern const IUG_READ_STATUS_DATA_ADDRESS_CFG_t             IUG_UNT_GHI_HEATER_6_RD_ST_DAT_ADDR_CFG[];
extern const uint8_t                                        ext_iug_entries_IUG_UNT_GHI_HEATER_6_RD_ST_DAT_ADDR_CFG;
#endif


#if( (IUG_UNT_HEATER_HG_LAST) == (IUG_UNT_HEATER_HG8_DEF) )
extern const IUG_READ_ID_DATA_ADDRESS_CFG_t                 IUG_UNT_GHI_HEATER_7_RD_ID_DAT_ADDR_CFG[];
extern const uint8_t                                        ext_iug_entries_IUG_UNT_GHI_HEATER_7_RD_ID_DAT_ADDR_CFG;

extern const IUG_READ_STATUS_DATA_ADDRESS_CFG_t             IUG_UNT_GHI_HEATER_7_RD_ST_DAT_ADDR_CFG[];
extern const uint8_t                                        ext_iug_entries_IUG_UNT_GHI_HEATER_7_RD_ST_DAT_ADDR_CFG;
#endif


#if( (IUG_UNT_HEATER_HG_LAST) == (IUG_UNT_HEATER_HG8_DEF) )
extern const IUG_READ_ID_DATA_ADDRESS_CFG_t                 IUG_UNT_GHI_HEATER_8_RD_ID_DAT_ADDR_CFG[];
extern const uint8_t                                        ext_iug_entries_IUG_UNT_GHI_HEATER_8_RD_ID_DAT_ADDR_CFG;

extern const IUG_READ_STATUS_DATA_ADDRESS_CFG_t             IUG_UNT_GHI_HEATER_8_RD_ST_DAT_ADDR_CFG[];
extern const uint8_t                                        ext_iug_entries_IUG_UNT_GHI_HEATER_8_RD_ST_DAT_ADDR_CFG;
#endif


#endif 



