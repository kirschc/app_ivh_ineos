#ifndef _IUG_UNIT_GHI_DETECTION_H_
#define _IUG_UNIT_GHI_DETECTION_H_






boolean     IUG_unit_ghi_detection_cyclic_Heater( const IUG_UNIT_GENDET_STATE_t detection_state );
boolean     IUG_unit_ghi_detection_cyclic_HeaterTrigger( const IUG_UNIT_GENDET_STATE_t detection_state );
IUG_HG_AVAILABLE_t      IUG_unit_ghi_detection_is_htr_available( const IUG_HEATER_UNIT_ENUM_t idx_htr );
void        IUG_unit_ghi_detection_determine_heater_type_ghi( void );
IUG_HEATER_UNIT_ENUM_t  IUG_unit_ghi_detection_is_scanning( void );
void        IUG_unit_ghi_detection_scan_cyclic( void );

inline IUG_COMM_DEVICE_ID_t     IUG_unit_ghi_detection_get_ghi_com_dev( void ) { return IUG_UNIT_GEN_DETECTION.detc_ghi_com_dev; };


#endif 


