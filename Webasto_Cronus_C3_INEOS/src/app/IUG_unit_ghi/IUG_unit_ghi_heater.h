#ifndef _IUG_UNIT_GHI_HEATER_H_
#define _IUG_UNIT_GHI_HEATER_H_



typedef struct
{
    IUG_HTR_MSK_ID_t                msk_htr_trg;        
    IUG_HTR_MSK_ID_t                msk_htr_req;        
    IUG_HTR_MSK_ID_t                msk_htr_que;        
    IUG_HTR_MSK_ID_t                msk_htr_nxt;        

    IUG_HTR_MSK_ID_t                msk_req_snd;        
    IUG_HTR_MSK_ID_t                msk_req_ack;        
    IUG_QUEUE_KEY_t                 req_key;            

    IUG_HTR_MSK_ID_t                mon_htr_msk;        
    IUG_HTR_MSK_ID_t                mon_timeout_msk;    
    uint16_t                        mon_timeout_cnt;    

} IUG_GHI_COMM_CTL_REQ_MSK_HTR_t;


typedef struct
{
    IUG_QUEUE_KEY_t                 comm_rx_act[(GHIBUS_INS_ENUM_MAX)];

    IUG_QUEUE_KEY_t                 comm_tx_act[(GHIBUS_INS_ENUM_MAX)];

    uint8_t                         RX_ind_tp;          
    uint16_t                        RX_rec_number;      
    WBUS_DRV_CALLBACK_t             *RX_callback;       
    uint16_t       RX_com_med;         

    uint8_t                         TX_ind_tp;          

    WBUS_INSTANCE_t                 TX_wbus_ins;
    uint16_t                        TX_trm_number;      
    WBUS_DRV_CALLBACK_t             *TX_callback;       
    uint16_t       TX_com_med;         

    uint8_t                         get_htr_property_cnt[IUG_HEATER_UNIT_ENUM_MAX];  

    uint8_t                         comm_rx_rec_err[(GHIBUS_INS_ENUM_MAX)][(IUG_HEATER_UNIT_ENUM_MAX)]; 
    uint8_t                         comm_tx_trm_err[(GHIBUS_INS_ENUM_MAX)][(IUG_HEATER_UNIT_ENUM_MAX)]; 

    uint8_t                         comm_rx_hdl_install[(GHIBUS_INS_ENUM_MAX)];

    IUG_GHI_COMM_CTL_REQ_MSK_HTR_t      ctl_cmd;

    IUG_GHI_COMM_CTL_REQ_MSK_HTR_t      ctl_pending;

    IUG_GHI_COMM_CTL_REQ_MSK_HTR_t      ctl_rd_id;

    IUG_GHI_COMM_CTL_REQ_MSK_HTR_t      ctl_rd_st;

} IUG_GHI_COMM_CTL_t;



extern IUG_GHI_COMM_CTL_t      IUG_GHI_COMM_CTL;



void    IUG_ghi_heater_cmd_create( IUG_UNIT_GHI_HEATER_CONTROL_t * const p_unt_ctl );
IUG_COMMAND_t   IUG_ghi_heater_cmd_evaluate( IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl, const IUG_COMMAND_t cmd_req,
                                             IUG_GEN_CTL_DISPLAY_ID_t * const p_display_info );
void    IUG_ghi_heater_cyclic_get_htr_property( IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl );
uint8_t IUG_ghi_heater_get_comm_timeout( IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl, 
                                         uint16_t * const p_retry_timeout_ms,
                                         uint16_t * const p_ack_timeout_ms );
void    IUG_ghi_heater_comm_ctl_req_clear( IUG_GHI_COMM_CTL_REQ_MSK_HTR_t * const p_ctl_req, const IUG_HTR_MSK_ID_t htr_msk );
void    IUG_ghi_heater_ctl_cyclic( const uint8_t idx_unit_tree );
void    IUG_ghi_heater_ctl_cyclic_independent( void );
void    IUG_ghi_heater_ctl_loop_post( void );
void    IUG_ghi_heater_ctl_loop_pre( void );
void    IUG_ghi_heater_ctl_init( const uint8_t idx_unit_tree );
void    IUG_ghi_heater_ctl_init_comm_cfg( void );
void    IUG_ghi_heater_cyclic_services( IUG_UNIT_GHI_HEATER_CONTROL_t * const p_unt_ctl );
void    IUG_ghi_heater_ctl_state( IUG_UNIT_GHI_HEATER_CONTROL_t * const p_unt_ctl );
void    IUG_ghi_heater_handle_acknowlege( IUG_UNIT_GHI_HEATER_CONTROL_t * const p_unt_ctl );


#ifdef IUG_GHI_BUS_HEATER_SIMULATE_EN
int8_t  IUG_ghi_heater_simulation_if_fetch_htr_cmd_ack( const IUG_HEATER_UNIT_ENUM_t htr_idx, struct_api_ghi_heater_interface_htr_cmd_ack * const cmd_ack );
uint8_t IUG_ghi_heater_simulation_cyclic( void );
int8_t IUG_ghi_heater_simulation_if_fetch_htr_property_ms( const IUG_HEATER_UNIT_ENUM_t htr_idx, struct_api_ghi_heater_interface_htr_property * const htr_property );
int8_t  IUG_ghi_heater_simulation_if_fetch_htr_id( const IUG_HEATER_UNIT_ENUM_t htr_idx, struct_api_ghi_heater_interface_htr_id * const htr_id );
int8_t  IUG_ghi_heater_simulation_if_fetch_htr_keep_alive_info( const IUG_HEATER_UNIT_ENUM_t htr_idx, uint8_t * const keep_alive_info );
int8_t  IUG_ghi_heater_simulation_if_fetch_htr_status( const IUG_HEATER_UNIT_ENUM_t htr_idx ,
                                              struct_api_ghi_heater_interface_htr_status * const ghi_htr_status );
int8_t  IUG_ghi_heater_simulation_if_preset_cmd_from_bsw( const IUG_HEATER_UNIT_ENUM_t htr_idx, const struct_api_ghi_heater_interface_htr_cmd_req cmd_req );

uint8_t IUG_ghi_heater_sim_read_id( const IUG_HEATER_UNIT_ENUM_t ghi_htr_idx,
                                    uint8_t * const p_req_rd_id_list,
                                    const uint8_t   req_rd_id_size_list,
                                    uint8_t * const p_rsp_rd_id,
                                    const uint8_t   rsp_rd_id_buffer_size );
#endif


#endif 


