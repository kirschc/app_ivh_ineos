#ifndef _IUG_UTILITY_LIN_CMD_H_
#define _IUG_UTILITY_LIN_CMD_H_





void IUG_ghi_heater_cmd_interface( IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl, const IUG_HEATER_UNIT_ENUM_t idx_htr );
boolean IUG_ghi_heater_cmd_send_request( const IUG_HEATER_UNIT_ENUM_t cmd_for_ghi_htr,
                                         IUG_QUEUE_KEY_t cmd_req_key,
                                         const uint8_t wbus_meta_mode,
                                         const uint8_t wbus_cmd_req,
                                         const IUG_PHYS_TIME_1M_T     cmd_ACTIVATION_PERIOD,
                                         const IUG_PHYS_TEMP_1_1DEG_T cmd_SETPOINT );
boolean IUG_ghi_heater_cmd_receive_response( const IUG_HEATER_UNIT_ENUM_t ack_for_htr,
                                             const uint8_t wbus_meta_cmd_ack,
                                             const uint8_t wbus_cmd_ack,
                                             const IUG_PHYS_TIME_1M_T     cmd_ACTIVATION_PERIOD,
                                             const IUG_PHYS_TEMP_1_1DEG_T cmd_SETPOINT );


#endif 


