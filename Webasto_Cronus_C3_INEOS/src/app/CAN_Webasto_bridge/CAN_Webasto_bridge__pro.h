#ifndef _CAN_WEBASTO_BRIDGE__PRO_H__
#define _CAN_WEBASTO_BRIDGE__PRO_H__




#ifdef CAN_WEBASTO_BRIDGE_IMPLEMENT







extern tp_il_config_type    mgl_cfl_iso_tp_data_cfg;

extern const CANWEB_BRIDGE_MSG_HDR_CFG_t    CANWEB_BRIDGE_MSG_HDR_CFG[];
extern const uint8_t                        ext_iug_entries_CANWEB_BRIDGE_MSG_HDR_CFG;

extern CANWEB_BRIDGE_BridgeCfg_t            CANWEB_BRIDGE_BridgeCfg[(CANWEB_BRIDGE_CHANNEL_MAX)];
extern CANWEB_BRIDGE_BridgeCtl_t            CANWEB_BRIDGE_BridgeCtl[(CANWEB_BRIDGE_CHANNEL_MAX)];


#ifdef CAN_WEBASTO_BRIDGE_CAN_SIMULATION
extern uint16_t IUG_CAN_WBUS_BRIDGE_trg_rx;
extern uint16_t IUG_CAN_WBUS_BRIDGE_trg_rx_ack;

extern uint8_t              mgl_CANWEBBRG_sim_rx_buf[(CANWEBBRG_MSG_BUFFER_SIZE_MAX)];
extern uint8_t              mgl_CANWEBBRG_sim_tx_buf[(CANWEBBRG_MSG_BUFFER_SIZE_MAX)];
#endif



void                CANWEB_BRIDGE_can_message_receive_isr( const enum_CAN_INTERFACES can_bus, const struct_hal_can_frame * const ptr_can_msg );
void                CANWEB_BRIDGE_comm( void );
IUG_QUEUE_KEY_t     CANWEB_BRIDGE_comm_reserve_buffer( IUG_QUEUE_ENTRY_DATA_t * * const que_dat, const uint16_t buf_size );
void                CANWEB_BRIDGE_comm_rx( CANWEB_BRIDGE_BridgeCfg_t *p_brg_cfg, CANWEB_BRIDGE_BridgeCtl_t *p_brg_ctl );
void                CANWEB_BRIDGE_comm_tx( CANWEB_BRIDGE_BridgeCfg_t *p_brg_cfg, CANWEB_BRIDGE_BridgeCtl_t *p_brg_ctl );
void                CANWEB_BRIDGE_cyclic( void );

CANWEB_BRIDGE_CANCHL_ID_t CANWEB_BRIDGE_cfg_set_can_link( const CANWEB_BRIDGE_CANCHL_ID_t canwebbrg_chl_idx,
                                                          const enum_CAN_INTERFACES       can_bus,
                                                          const uint32_t                  can_id_rx,
                                                          const uint32_t                  can_id_tx );
CANWEBBRG_PROC_FNC_RESULT_t     CANWEB_BRIDGE_process_cfg_sim_unit( CANWEBBRG_PROC_FNC_PARAM_t * const p_fnc_par );
void                CANWEB_BRIDGE_init( void );
CANWEB_BRIDGE_MSG_HDR_CFG_t * CANWEB_BRIDGE_msg_hdr_cfg_get_entry_by_type( const CANWEB_BRIDGE_MSG_HDR_TYP_t msg_hdr_type );
CANWEBBRG_PROC_FNC_RESULT_t     CANWEB_BRIDGE_process_sig_cmd_button( CANWEBBRG_PROC_FNC_PARAM_t * const p_fnc_par );
CANWEBBRG_PROC_FNC_RESULT_t     CANWEB_BRIDGE_process_signal_enumerated_rx( CANWEBBRG_PROC_FNC_PARAM_t * const p_fnc_par );
CANWEBBRG_PROC_FNC_RESULT_t     CANWEB_BRIDGE_process_wbus_msg( CANWEBBRG_PROC_FNC_PARAM_t * const p_fnc_par );
CANWEB_BRIDGE_CANCHL_ID_t       CANWEB_BRIDGE_test_bridge_chl_configured( const CANWEB_BRIDGE_CANCHL_ID_t brg_chl,
                                                                          CANWEB_BRIDGE_BridgeCfg_t * * const p_brg_chl_cfg );
IUG_UNIT_ID_t       CANWEB_BRIDGE_test_unit_id_configured( const IUG_UNIT_ID_t unit_id, CANWEB_BRIDGE_CANCHL_ID_t * const p_idx_brg_cfg );
WBUS_MEMBER_t       CANWEB_BRIDGE_test_wbus_mbr_configured( uint8_t * const p_wbus_msg, 
                                                            const boolean b_test_transmitter,
                                                            CANWEB_BRIDGE_CANCHL_ID_t * const p_idx_brg_cfg );

boolean             CANWEB_BRIDGE_tp_receive_can( const CANWEB_BRIDGE_CANCHL_ID_t canwebbrg_chl_idx, uint8_t * const p_can_msg_data );
CANWEB_BRIDGE_MSG_HDR_INFO_t CANWEB_BRIDGE_tp_receive_get_header( CANWEB_BRIDGE_BridgeCtl_t * const p_brg_ctl );
boolean             CANWEB_BRIDGE_tp_receive_notification( const CANWEB_BRIDGE_CANCHL_ID_t canwebbrg_chl_idx, uint8_t * const tp_dat_ptr, const uint16_t tp_dat_sze );
void                CANWEB_BRIDGE_tp_receive_release( CANWEB_BRIDGE_BridgeCtl_t * const p_brg_ctl );
void                CANWEB_BRIDGE_tp_transmit_close( CANWEB_BRIDGE_BridgeCfg_t *p_brg_cfg, CANWEB_BRIDGE_BridgeCtl_t *p_brg_ctl );
boolean             CANWEB_BRIDGE_tp_transmit_notification( const CANWEB_BRIDGE_CANCHL_ID_t canwebbrg_chl_idx, uint8_t * const tp_dat_ptr, const uint16_t tp_dat_sze );

WBUS_DRIVER_ERROR_t CANWEB_BRIDGE_WBus_WriteFrameAsync( const CANWEB_BRIDGE_CANCHL_ID_t ety_brg_cfg,
                                                        const IUG_COMM_MEDIUM_ID_t com_med,
                                                        const WBUS_INSTANCE_t wbus_ins,
                                                        uint8_t * const       ptrToFrameToSend,
                                                        const uint16_t        sizeOfFrameToSend,
                                                        WBUS_DRV_CALLBACK_t * const Callback );


#ifdef CAN_WEBASTO_BRIDGE_CAN_SIMULATION
void            CANWEBBRG__sim_can_rx( void );
#endif



#endif 

#endif 


