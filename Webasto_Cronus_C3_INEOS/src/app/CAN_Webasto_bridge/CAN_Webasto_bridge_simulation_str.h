#ifndef _CAN_WEBASTO_BRIDGE_SIMULATION_STR_H__
#define _CAN_WEBASTO_BRIDGE_SIMULATION_STR_H__







#ifdef CAN_WEBASTO_BRIDGE_IMPLEMENT
#ifdef CAN_WEBASTO_BRIDGE_CAN_SIMULATION

typedef struct
{
    uint8_t     sel_Se;     
    uint8_t     sel_sE;     
    uint8_t     sel_Time;   
    uint8_t     sel_Cmd;    

    uint8_t     brg_chl_id;
    uint8_t     wbus_SE;
    uint8_t     wbus_size;
    uint8_t     wbus_meta;  
    uint8_t     wbus_mode;  
    uint8_t     wbus_par1;
    uint8_t     wbus_par2;
    uint8_t     wbus_par3;
    uint8_t     wbus_par4;
    uint8_t     wbus_par5;
} struct_CANWEB_BRIDGE_sim_WBUS_INPUT_OUTPUT_t;
typedef struct_CANWEB_BRIDGE_sim_WBUS_INPUT_OUTPUT_t    CANWEBBRG_sim_WBUS_INP_OUT_t;


#endif 
#endif 

#endif 


