#ifndef _CAN_WEBASTO_BRIDGE__ADJUST_H__
#define _CAN_WEBASTO_BRIDGE__ADJUST_H__




#ifdef CAN_WEBASTO_BRIDGE_IMPLEMENT



#define CAN_WEBASTO_BRIDGE_CHANNEL_MAX      1u

#if( (CAN_WEBASTO_BRIDGE_CHANNEL_MAX) <= 1)
    #define CANWEB_BRIDGE_CHANNEL_MAX       1u
#else
    #define CANWEB_BRIDGE_CHANNEL_MAX       4u
#endif


#define CANWEB_BRIDGE_UNIT_ID_AVAILABLE         ((0u) | \
                                                 (1u << (IUG_UNT_HEATER_HG1))     |\
                                                 (1u << (IUG_UNT_HEATER_HG2))     |\
                                                 (1u << (IUG_UNT_HEATER_HG3))     |\
                                                 (1u << (IUG_UNT_MULTI_CTL_3TO4)) |\
                                                 (1u << (IUG_UNT_THERMO_CONNECT)) |\
                                                 (1u << (IUG_UNT_THERMO_CALL))    |\
                                                 (1u << (IUG_UNT_TELE_START))     |\
                                                 (1u << (IUG_UNT_CMD_BUTTON))     |\
                                                 (1u << (IUG_UNT_USER_DEVICE))    |\
                                                 (0u) )


#define CANWEB_BRIDGE_WBUS_MEMBER_AVAILABLE     ((0u) | \
                                                 (1u << (WBUS_MBR_REMOTE_CTL_TELESTART)) |\
                                                 (1u << (WBUS_MBR_CTL_MULTI))      |\
                                                 (1u << (WBUS_MBR_HEATER_UNIT_1))  |\
                                                 (1u << (WBUS_MBR_HEATER_UNIT_2))  |\
                                                 (1u << (WBUS_MBR_THERMO_CONNECT)) |\
                                                 (1u << (WBUS_MBR_THERMO_CALL))    |\
                                                 (1u << (WBUS_MBR_TEST_UNIT_WTT))  |\
                                                 (0u) )



#if( (WBUS_MAX_SEND_PACKET_LEN) >= (WBUS_MAX_RECV_PACKET_LEN) )
    #define CANWEBBRG_MSG_BUFFER_SIZE_MAX       ((WBUS_MAX_SEND_PACKET_LEN) + (CANWEBBRG_MSGHDRTYP_ENVELOPE_SIZE))
#else
    #define CANWEBBRG_MSG_BUFFER_SIZE_MAX       ((WBUS_MAX_RECV_PACKET_LEN) + (CANWEBBRG_MSGHDRTYP_ENVELOPE_SIZE))
#endif



#endif 

#endif 


