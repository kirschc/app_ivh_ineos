#ifndef _CAN_WEBASTO_BRIDGE__INCLUDE_H__
#define _CAN_WEBASTO_BRIDGE__INCLUDE_H__

#include "tp_il_advanced_include.h"

#include "CAN_Webasto_bridge__con.h"
#include "CAN_Webasto_bridge__adjust.h"
#include "CAN_Webasto_bridge__typ.h"
#include "CAN_Webasto_bridge__enu.h"
#include "CAN_Webasto_bridge__str.h"
#include "CAN_Webasto_bridge__cfg.h"
#include "CAN_Webasto_bridge__pro.h"

#ifdef CAN_WEBASTO_BRIDGE_CAN_SIMULATION
#include "CAN_Webasto_bridge_simulation_str.h"
#endif


#include "CAN_Webasto_bridge_main.h"
#include "CAN_Webasto_bridge_signal_enumerated.h"



#endif 


