#ifndef _CAN_WEBASTO_BRIDGE__STR_H__
#define _CAN_WEBASTO_BRIDGE__STR_H__







#ifdef CAN_WEBASTO_BRIDGE_IMPLEMENT

typedef struct
{
    CANWEB_BRIDGE_MSG_HDR_TYP_t hdr_typ;        


    uint16_t                    msg_size_min_req;   

    uint16_t                    msg_size_max_ack;   

} struct_CANWEB_BRIDGE_MESSAGE_HEADER_CFG_t;
typedef struct_CANWEB_BRIDGE_MESSAGE_HEADER_CFG_t   CANWEB_BRIDGE_MSG_HDR_CFG_t;


typedef struct
{

    CANWEB_BRIDGE_MSG_HDR_TYP_t hdr_typ;    

} struct_CANWEB_BRIDGE_MESSAGE_HEADER_t;
typedef struct_CANWEB_BRIDGE_MESSAGE_HEADER_t   CANWEB_BRIDGE_MSG_HEADER_t;


typedef struct
{
    CANWEB_BRIDGE_MSG_HDR_TYP_t     hdr_typ;        
    CANWEB_BRIDGE_MSG_HDR_TYP_t     hdr_typ_msg;    

    uint8_t                         *p_msg;         
    uint8_t                         *p_msg_idv;     

    CANWEB_BRIDGE_MSG_HDR_CFG_t     *p_msg_cfg;     

} struct_CANWEB_BRIDGE_MESSAGE_HEADER_INFO_t;
typedef struct_CANWEB_BRIDGE_MESSAGE_HEADER_INFO_t   CANWEB_BRIDGE_MSG_HDR_INFO_t;


typedef struct
{
    enum_CAN_INTERFACES             can_bus;        
    uint32_t                        can_id_rx;      
    uint32_t                        can_id_tx;      
    CANWEB_BRIDGE_WBUS_MEMBER_MSK_t wbus_mbr_msk;   
    CANWEB_BRIDGE_SIM_UNIT_MSK_t    sim_unit_msk;   

} struct_CANWEB_BRIDGE_BridgeCfg_t;
typedef struct_CANWEB_BRIDGE_BridgeCfg_t   CANWEB_BRIDGE_BridgeCfg_t;


typedef struct
{
    volatile struct_hal_can_frame   rx_buf_isr;
    volatile uint8_t                rx_ind_isr;             

    uint8_t                         RX_tp_ind;              
    uint16_t                        RX_tp_ind_data_size;    
    uint8_t                         *RX_tp_ind_data_ptr;    


    uint8_t                         TX_tp_trg;              
    uint16_t                        TX_tp_trg_data_size;    
    uint8_t                         *TX_tp_trg_data_ptr;    

    WBUS_INSTANCE_t                 TX_wbus_ins;
    IUG_QUEUE_KEY_t                 TX_que_key;             
    WBUS_DRV_CALLBACK_t             *TX_callback;           


    uint8_t                         TX_tp_ind;              
    uint16_t                        TX_tp_ind_data_size;    
    uint8_t                         *TX_tp_ind_data_ptr;    

    uint16_t                        TX_AwaitAck_cnt;        

} struct_CANWEB_BRIDGE_BridgeCtl_t;
typedef struct_CANWEB_BRIDGE_BridgeCtl_t   CANWEB_BRIDGE_BridgeCtl_t;


typedef struct
{
    CANWEB_BRIDGE_MSG_HDR_INFO_t    *p_msg_hdr_inf;         

    uint8_t                         *p_msg_ack;             
    uint16_t                        msg_ack_size;           

} struct_CANWEBBRG_PROC_FNC_PARAMETER_t;
typedef struct_CANWEBBRG_PROC_FNC_PARAMETER_t   CANWEBBRG_PROC_FNC_PARAM_t;


typedef struct
{
    uint8_t                         res_ack_code;           
    uint16_t                        res_msg_size;           
    uint8_t                         *p_msg_ack;             

} struct_CANWEBBRG_PROC_FNC_RESULT_t;
typedef struct_CANWEBBRG_PROC_FNC_RESULT_t   CANWEBBRG_PROC_FNC_RESULT_t;

#endif 

#endif 


