#ifndef _CAN_WEBASTO_BRIDGE_H__
#define _CAN_WEBASTO_BRIDGE_H__



#ifdef CAN_WEBASTO_BRIDGE_IMPLEMENT



#define CANWEBBRG_MSGHDRTYP_NAK_MSG_SIZE            7u  


#define CANWEBBRG_MSGHDRTYP_NO_PROC              0xFFu  



#define CANWEBBRG_MSG_GET_HDR_TYP( p_msg ) \
    (CANWEB_BRIDGE_MSG_HDR_TYP_t) ( *((p_msg)+0u) )


#define CANWEBBRG_MSG_GET_SIZE( p_msg ) \
    ( (*((p_msg)+1u) << 8u)  + *((p_msg)+2u) )

#define CANWEBBRG_MSG_SET_SIZE( p_msg, sze ) \
    { *((p_msg)+1u) = ((sze) >> 8u); \
      *((p_msg)+2u) =  (sze); }


#define CANWEBBRG_MSG_GET_BRG_CHL_ID( p_msg ) \
    (CANWEB_BRIDGE_CANCHL_ID_t) ( *((p_msg)+3u) )


#define CANWEBBRG_MSG_GET_HDR_TYP_FOR_NAK( p_msg ) \
    (CANWEB_BRIDGE_MSG_HDR_TYP_t)( *((p_msg)+5u) )


#define CANWEBBRG_MSG_SET_HEADER( p_msg, typ, sze, chl ) \
    {   if( (NULL) != (p_msg) )\
        {\
            *((p_msg)+0u) =  (typ)       ;\
            *((p_msg)+1u) = ((sze) >> 8u);\
            *((p_msg)+2u) =  (sze)       ;\
            *((p_msg)+3u) =  (chl)       ;\
        }\
    }







#endif 

#endif 


