#ifndef _IUG_COMM_UTILITY_DEBUG_TRACE_RECORD_H_
#define _IUG_COMM_UTILITY_DEBUG_TRACE_RECORD_H_





#define Trc_____MSB      0x80000000u 




typedef enum
{
    TrcNONE         = (Trc_____MSB|0x00000000u),    
    TrcRxMsgEVT     = (Trc_____MSB|0x00000001u),    
    TrcRxMsg_OK     = (Trc_____MSB|0x00000004u),    
    TrcRxMsgACK     = (Trc_____MSB|0x00000010u),    
    TrcRxBrdCas     = (Trc_____MSB|0x00000800u),    
    TrcRxDbg__1     = (Trc_____MSB|0x00001000u),    
    TrcRxDbg__2     = (Trc_____MSB|0x00002000u),    
    TrcRxDbg__3     = (Trc_____MSB|0x00004000u),    
    TrcRxDbg__4     = (Trc_____MSB|0x00008000u),    
    TrcTxMsgTRG     = (Trc_____MSB|0x00010000u),    
    TrcTxMsgREP     = (Trc_____MSB|0x00020000u),    
    TrcTxMsg_OK     = (Trc_____MSB|0x00040000u),    
    TrcTxMsgERR     = (Trc_____MSB|0x00080000u),    
    TrcTxMsgACK     = (Trc_____MSB|0x00100000u),    
    TrcTxRefuse     = (Trc_____MSB|0x00200000u),    
    TrcTxTimeou     = (Trc_____MSB|0x00400000u),    
    TrcTxBrdCas     = (Trc_____MSB|0x08000000u),    
    TrcTxDbg__1     = (Trc_____MSB|0x10000000u),    
    TrcTxDbg__2     = (Trc_____MSB|0x20000000u),    
    TrcTxDbg__3     = (Trc_____MSB|0x40000000u),    
    Trc___Clear     =              0xFFFFFFFFu,     
    Trc_ENUM_FORCE_TYPE  = 0x7FFFFFFF               

} IUG_COMM_DBG_TRACE_SELECTOR_t;    


#ifdef IUG_COMM_DBG_TRACE_EN

typedef struct
{

    IUG_QUEUE_TIMESTAMP_t           t;          
    IUG_COMM_DBG_TRACE_SELECTOR_t   s;          

    IUG_QUEUE_KEY_t                 p;                          
    uint8_t                         i;                          
    uint8_t                         d[(sizeof(uint32_t)*2u)];   

    IUG_QUEUE_TIMESTAMP_t           dt;         

} IUG_COMM_DBG_TRACE_ENTRY_t;


typedef struct
{
    uint8_t                         retrigger;                      
    IUG_COMM_DBG_TRACE_SELECTOR_t   trg_selector;                   
    uint8_t                         trg_msg[(sizeof(uint32_t)*2u)]; 

} IUG_COMM_DBG_TRACE_TRIGGER_t;


typedef struct
{
    IUG_COMM_DBG_TRACE_TRIGGER_t    trigger;            
    uint8_t                         entries;            
    IUG_COMM_DBG_TRACE_ENTRY_t      trace[(IUG_COMM_DEBUG_TRACE_ENTRY_MAX)];

} IUG_COMM_DBG_TRACE_t;


void    IUG_comm_dbg_trace_record( const IUG_COMM_DBG_TRACE_SELECTOR_t selector, const IUG_QUEUE_ENTRY_DATA_t * const que_dat );
void    IUG_comm_dbg_trace_record_RX_received( const IUG_QUEUE_ENTRY_DATA_t * const que_dat, const WBUS_INSTANCE_t wbus_ins );
void    IUG_comm_dbg_trace_record_TX_triggered( const IUG_QUEUE_ENTRY_DATA_t * const que_dat, const WBUS_INSTANCE_t wbus_ins );


#endif  


#endif 

