#ifndef _IUG_COMM_UTILITY_DEBUG_TRACE_RECORD_ADJUST_H_
#define _IUG_COMM_UTILITY_DEBUG_TRACE_RECORD_ADJUST_H_


#ifdef IUG_COMM_DBG_TRACE_EN

#define IUG_COMM_DEBUG_TRACE_ENTRY_MAX          50u 



#define IUG_COMM_DBG_TRACE_RXTX_PROTOCOL            






#if( defined( IUG_COMM_DBG_TRACE_RXTX_PROTOCOL ) ||\
     defined( IUG_COMM_DBG_TRACE_RXTX_DETAILED ) ||\
     defined( IUG_COMM_DBG_TRACE_RX_PROTOCOL   ) ||\
     defined( IUG_COMM_DBG_TRACE_RX_DETAILED   ) )
    #define IUG_COMM_DBG_TRACE_RECORD_RX_PROTOCOL( selector, que_dat )      IUG_comm_dbg_trace_record( (selector), (que_dat) )
    #define IUG_COMM_DBG_TRACE_RECORD_RX_RECEIVED( que_dat, wbus_ins )      IUG_comm_dbg_trace_record_RX_received( (que_dat), (wbus_ins) )
#else
    #define IUG_COMM_DBG_TRACE_RECORD_RX_PROTOCOL( selector, que_dat )
    #define IUG_COMM_DBG_TRACE_RECORD_RX_RECEIVED( que_dat, wbus_ins )
#endif


#if( defined( IUG_COMM_DBG_TRACE_RXTX_DETAILED ) ||\
     defined( IUG_COMM_DBG_TRACE_RX_DETAILED   ) )
    #define IUG_COMM_DBG_TRACE_RECORD_RX_DETAILED( selector, que_dat )      IUG_comm_dbg_trace_record( (selector), (que_dat) )
#else
    #define IUG_COMM_DBG_TRACE_RECORD_RX_DETAILED( selector, que_dat )
#endif


#if( defined( IUG_COMM_DBG_TRACE_RXTX_PROTOCOL ) ||\
     defined( IUG_COMM_DBG_TRACE_RXTX_DETAILED ) ||\
     defined( IUG_COMM_DBG_TRACE_TX_PROTOCOL   ) ||\
     defined( IUG_COMM_DBG_TRACE_TX_DETAILED   ) )
    #define IUG_COMM_DBG_TRACE_RECORD_TX_PROTOCOL( selector, que_dat )      IUG_comm_dbg_trace_record( (selector), (que_dat) )
    #define IUG_COMM_DBG_TRACE_RECORD_TX_TRIGGERED( que_dat, wbus_ins )     IUG_comm_dbg_trace_record_TX_triggered( (que_dat), (wbus_ins) )
#else
    #define IUG_COMM_DBG_TRACE_RECORD_TX_PROTOCOL( selector, que_dat )
    #define IUG_COMM_DBG_TRACE_RECORD_TX_TRIGGERED( que_dat, wbus_ins )
#endif


#if( defined( IUG_COMM_DBG_TRACE_RXTX_DETAILED ) ||\
     defined( IUG_COMM_DBG_TRACE_TX_DETAILED   ) )
    #define IUG_COMM_DBG_TRACE_RECORD_TX_DETAILED( selector, que_dat )  IUG_comm_dbg_trace_record( (selector), (que_dat) )
#else
    #define IUG_COMM_DBG_TRACE_RECORD_TX_DETAILED( selector, que_dat )
#endif


#define IUG_COMM_DBG_TRACE_RECORD( selector, que_dat )                  IUG_comm_dbg_trace_record( (selector), (que_dat) )

#define IUG_COMM_DBG_TRACE_RECORD_RETRIGGER( selector, que_dat )        IUG_comm_dbg_trace_record( (selector), (que_dat) )


#else 
    #define IUG_COMM_DBG_TRACE_RECORD( selector, que_dat )
    #define IUG_COMM_DBG_TRACE_RECORD_RETRIGGER( selector, que_dat )
    #define IUG_COMM_DBG_TRACE_RECORD_RX_PROTOCOL( selector, que_dat )
    #define IUG_COMM_DBG_TRACE_RECORD_RX_DETAILED( selector, que_dat )
    #define IUG_COMM_DBG_TRACE_RECORD_RX_RECEIVED( que_dat, wbus_ins )
    #define IUG_COMM_DBG_TRACE_RECORD_TX_PROTOCOL( selector, que_dat )
    #define IUG_COMM_DBG_TRACE_RECORD_TX_DETAILED( selector, que_dat )
    #define IUG_COMM_DBG_TRACE_RECORD_TX_TRIGGERED( que_dat, wbus_ins )
#endif 


#ifdef IUG_COMM_DBG_TRACE_EN

    #ifdef IUG_COMM_DBG_TRACE_RXTX_DETAILED_BTM
        #define IUG_COMM_DBG_TRACE_RECORD_RX_DETAILED_BTM( selector, que_dat )  IUG_comm_dbg_trace_record( (selector), (que_dat) )
        #define IUG_COMM_DBG_TRACE_RECORD_TX_DETAILED_BTM( selector, que_dat )  IUG_comm_dbg_trace_record( (selector), (que_dat) )
    #endif    
#endif 

#if( !defined( IUG_COMM_DBG_TRACE_EN                ) ||\
     !defined( IUG_COMM_DBG_TRACE_RXTX_DETAILED_BTM ) )
    #define IUG_COMM_DBG_TRACE_RECORD_RX_DETAILED_BTM( selector, que_dat )
    #define IUG_COMM_DBG_TRACE_RECORD_TX_DETAILED_BTM( selector, que_dat )
#endif


#endif 


