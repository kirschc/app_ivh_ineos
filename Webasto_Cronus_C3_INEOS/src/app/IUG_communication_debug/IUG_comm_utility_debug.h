#ifndef _IUG_COMM_UTILITY_DEBUG_H_
#define _IUG_COMM_UTILITY_DEBUG_H_



#ifdef IUG_COMM_DBG_CATCH_MSG_EN
    void IUG_COMM_DBG_CATCH_MSG( const uint8_t ena, IUG_QUEUE_ENTRY_DATA_t * const que_dat, 
                                 const uint8_t SE,  const uint8_t mod, const uint8_t serv, const uint8_t sub_serv,
                                 const uint8_t lin_dev );
#else
    #define IUG_COMM_DBG_CATCH_MSG( enable, msg, SE, mod, serv, sub_serv, lin_dev )
#endif


#ifdef IUG_COMM_DBG_DISPLAY_RX_TX_HISTORY_NUM
typedef struct 
{
    uint8_t                             r;                  
    IUG_QUEUE_KEY_t                     k;                  
    IUG_QUEUE_TIMESTAMP_t               t;                  
    WBUS_INSTANCE_t                     i;                  

    uint8_t                             SE;                 
    uint8_t                             mod;                
    uint16_t                            d;                  
    uint16_t                            e;                  

} IUG_COMM_DBG_RX_TX_HISTORY_ENTRY_t;


typedef struct 
{
    IUG_COMM_DBG_RX_TX_HISTORY_ENTRY_t  history[(IUG_COMM_DBG_DISPLAY_RX_TX_HISTORY_NUM)];
    uint8_t                             entry;              
    uint8_t                             running;            
    uint8_t                             no_wtt_frames;      

} IUG_COMM_DBG_RX_TX_HISTORY_CTL_t;
#endif


#ifdef IUG_COMM_DBG_DISPLAY_RX_TX_HISTORY_NUM
extern IUG_COMM_DBG_RX_TX_HISTORY_CTL_t     IUG_COMM_DBG_RX_TX_HISTORY;
#endif


#ifdef IUG_COMM_DBG_DISPLAY_RX_TX_HISTORY_NUM
    #define IUG_COMM_DBG_SET_RX_TX_HISTORY( key, tim, ins, mSE, mode, _d, _e ) \
    { \
        \
        if( (IUG_COMM_DBG_DISPLAY_RX_TX_HISTORY_NUM) <= IUG_COMM_DBG_RX_TX_HISTORY.entry )\
        {\
            IUG_COMM_DBG_RX_TX_HISTORY.entry = 0u;\
        }\
        \
        if( (0u == IUG_COMM_DBG_RX_TX_HISTORY.no_wtt_frames ) ||\
            ( ((WBUS_MBR_Se_WTT_REQ) != ((WBUS_MBR_Se_MSK) & (*key))) && \
              ((WBUS_MBR_sE_WTT_ACK) != ((WBUS_MBR_sE_MSK) & (*key))) ) )\
        {\
            IUG_COMM_DBG_RX_TX_HISTORY.history[IUG_COMM_DBG_RX_TX_HISTORY.entry].r   = IUG_COMM_DBG_RX_TX_HISTORY.running++; \
            IUG_COMM_DBG_RX_TX_HISTORY.history[IUG_COMM_DBG_RX_TX_HISTORY.entry].k   = (IUG_QUEUE_KEY_t)      (key);\
            IUG_COMM_DBG_RX_TX_HISTORY.history[IUG_COMM_DBG_RX_TX_HISTORY.entry].t   = (IUG_QUEUE_TIMESTAMP_t)(tim);\
            IUG_COMM_DBG_RX_TX_HISTORY.history[IUG_COMM_DBG_RX_TX_HISTORY.entry].i   = (WBUS_INSTANCE_t)      (ins);\
            IUG_COMM_DBG_RX_TX_HISTORY.history[IUG_COMM_DBG_RX_TX_HISTORY.entry].SE  = (mSE);\
            IUG_COMM_DBG_RX_TX_HISTORY.history[IUG_COMM_DBG_RX_TX_HISTORY.entry].mod = (mode);\
            IUG_COMM_DBG_RX_TX_HISTORY.history[IUG_COMM_DBG_RX_TX_HISTORY.entry].d   = (_d);\
            IUG_COMM_DBG_RX_TX_HISTORY.history[IUG_COMM_DBG_RX_TX_HISTORY.entry].e   = (_e);\
            IUG_COMM_DBG_RX_TX_HISTORY.entry++;\
        }\
    }
#else
    #define IUG_COMM_DBG_SET_RX_TX_HISTORY( key, tim, ins, mSE, mode, _d, _e )
#endif



#endif 


