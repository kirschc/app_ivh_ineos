
#ifndef _IUG_COMMUNICATION_LIN_H_
#define _IUG_COMMUNICATION_LIN_H_






void    IUG_CommGHI_COMSTA_TRM_WAIT_ACK( IUG_COMM_RX_TX_STATEMACHINE_PARAM_t * const stmpar );
void    IUG_CommGHI_Handler_RX_fast( void );
void    IUG_CommLin_HandlerCyclic_fast( void );
WBUS_DRIVER_ERROR_t     IUG_CommGHI_ReadFrameAsync( const WBUS_INSTANCE_t wbus_ins,
                                                          IUG_QUEUE_ENTRY_DATA_t * const que_dat,
                                                          WBUS_DRV_CALLBACK_t    * const Callback,
                                                    const uint16_t com_med );
void    IUG_CommGHI_TransmitCtl( void );
WBUS_DRIVER_ERROR_t     IUG_CommGHI_WriteFrameAsync( const WBUS_INSTANCE_t wbus_ins,
                                                     IUG_QUEUE_ENTRY_DATA_t * const que_dat,
                                                     WBUS_DRV_CALLBACK_t    * const Callback,
                                                     const uint16_t com_med );


#endif 


