#ifndef _IUG_COMMUNICATION_INTERFACE_GHI_H_
#define _IUG_COMMUNICATION_INTERFACE_GHI_H_








#define IUG_COMM_IF_GHI_GET_RX_HANDLER( ghibus_ins )        \
    *((&IUG_GHI_COMM_CTL.comm_rx_act[0u])+(ghibus_ins))


#define IUG_COMM_IF_GHI_SET_RX_HANDLER( ghibus_ins,         \
                                        que_key )           \
    *((&IUG_GHI_COMM_CTL.comm_rx_act[0u])+(ghibus_ins)) = (que_key);


#define IUG_COMM_IF_GHI_GET_TX_HANDLER( ghibus_ins )        \
    *((&IUG_GHI_COMM_CTL.comm_tx_act[0u])+(ghibus_ins))


#define IUG_COMM_IF_GHI_SET_TX_HANDLER( ghibus_ins,         \
                                        que_key )           \
    *((&IUG_GHI_COMM_CTL.comm_tx_act[0u])+(ghibus_ins)) = (que_key);




#endif 


