#ifndef _GHIBUS_INSTANCE_CFG_H_
#define _GHIBUS_INSTANCE_CFG_H_


typedef enum
{
    GHIBUS_COM_HG1 = 0u,    
    GHIBUS_COM_HG2 = 1u,    
    #if( (IUG_UNT_HEATER_HG_LAST) == (IUG_UNT_HEATER_HG8_DEF) )
    GHIBUS_COM_HG3 = 2u,    
    GHIBUS_COM_HG4 = 3u,    
    GHIBUS_COM_HG5 = 4u,    
    GHIBUS_COM_HG6 = 5u,    
    GHIBUS_COM_HG7 = 6u,    
    GHIBUS_COM_HG8 = 7u,    
    #endif
    GHIBUS_COM_HGX_ENUM_MAX 

} GHIBUS_COM_HEATER_t;


#if( 2u == (IUG_UNT_HEATER_HG_LAST) )
_Static_assert((2u)==(GHIBUS_COM_HGX_ENUM_MAX),"Analyze: 'GHIBUS_COM_HGX_ENUM_MAX'");
#elif( 8u == (IUG_UNT_HEATER_HG_LAST) )
_Static_assert((5u)==(GHIBUS_COM_HGX_ENUM_MAX),"Analyze: 'GHIBUS_COM_HGX_ENUM_MAX'");
#endif


extern IUG_GHI_HEATER_GHI_BUS_COMM_CFG_t           IUG_GHI_HEATER_GHI_COMM_CFG[(GHIBUS_COM_HGX_ENUM_MAX)];



#endif 


