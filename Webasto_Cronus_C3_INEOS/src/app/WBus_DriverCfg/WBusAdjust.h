#ifndef __WBUS_ADJUST_H_
#define __WBUS_ADJUST_H_




#define WBUS_MAX_RECV_PACKET_LEN                                 258U

#define WBUS_MAX_SEND_PACKET_LEN                 (WBUS_MAX_RECV_PACKET_LEN)




#define  WBus_RTOS_Timer_Simulation  

#define WBUS_DEV_RLIN33UART_EN    
#define WBUS_DEV_RLIN34UART_EN    



#define WBUS_DRV_BAUDRATE_DEF_ID        (WBUS_DRV_BAUDRATE_2400)      


#define WBUS_DRV_BAUDRATE_MAX_ID        (WBUS_DRV_BAUDRATE_38400)     
#define WBUS_DRV_BAUDRATE_MAX_BD        (WBUS_DRV_BAUDRATE_38400_BD)  



#endif 


