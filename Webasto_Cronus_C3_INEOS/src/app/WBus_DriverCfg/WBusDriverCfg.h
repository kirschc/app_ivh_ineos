#ifndef __WBUS_DRIVER_CFG_H_
#define __WBUS_DRIVER_CFG_H_



#define WBUS_DRIVER_UNIT_CONFIGURATION \
{\
    { \
     \
    (WBUS_MBR_NULL),                       \
    (WBUS_DRV_WAIT_TIME_UNIT_0_US),        \
    (WBUS_DRV_RETRY_MAX_ERR_UNIT_0),       \
    (WBUS_DRV_RETRY_TIME_ERR_UNIT_0_MS),   \
    (WBUS_DRV_RETRY_MAX_REQ_UNIT_0),       \
    (WBUS_DRV_RETRY_TIME_REQ_UNIT_0_MS),   \
    }, \
    { \
     \
    (WBUS_MBR_1),                          \
    (WBUS_DRV_WAIT_TIME_UNIT_1_US),        \
    (WBUS_DRV_RETRY_MAX_ERR_UNIT_1),       \
    (WBUS_DRV_RETRY_TIME_ERR_UNIT_1_MS),   \
    (WBUS_DRV_RETRY_MAX_REQ_UNIT_1),       \
    (WBUS_DRV_RETRY_TIME_REQ_UNIT_1_MS),   \
    }, \
    { \
     \
    (WBUS_MBR_REMOTE_CTL_TELESTART),       \
    (WBUS_DRV_WAIT_TIME_UNIT_2_US),        \
    (WBUS_DRV_RETRY_MAX_ERR_UNIT_2),       \
    (WBUS_DRV_RETRY_TIME_ERR_UNIT_2_MS),   \
    (WBUS_DRV_RETRY_MAX_REQ_UNIT_2),       \
    (WBUS_DRV_RETRY_TIME_REQ_UNIT_2_MS),   \
    }, \
    { \
     \
    (WBUS_MBR_CTL_MULTI),                  \
    (WBUS_DRV_WAIT_TIME_UNIT_3_US),        \
    (WBUS_DRV_RETRY_MAX_ERR_UNIT_3),       \
    (WBUS_DRV_RETRY_TIME_ERR_UNIT_3_MS),   \
    (WBUS_DRV_RETRY_MAX_REQ_UNIT_3),       \
    (WBUS_DRV_RETRY_TIME_REQ_UNIT_3_MS),   \
    }, \
    { \
     \
    (WBUS_MBR_HEATER_UNIT_1),             \
    (WBUS_DRV_WAIT_TIME_UNIT_4_US),        \
    (WBUS_DRV_RETRY_MAX_ERR_UNIT_4),       \
    (WBUS_DRV_RETRY_TIME_ERR_UNIT_4_MS),   \
    (WBUS_DRV_RETRY_MAX_REQ_UNIT_4),       \
    (WBUS_DRV_RETRY_TIME_REQ_UNIT_4_MS),   \
    }, \
    { \
     \
    (WBUS_MBR_HEATER_UNIT_2),             \
    (WBUS_DRV_WAIT_TIME_UNIT_5_US),        \
    (WBUS_DRV_RETRY_MAX_ERR_UNIT_5),       \
    (WBUS_DRV_RETRY_TIME_ERR_UNIT_5_MS),   \
    (WBUS_DRV_RETRY_MAX_REQ_UNIT_5),       \
    (WBUS_DRV_RETRY_TIME_REQ_UNIT_5_MS),   \
    }, \
    { \
     \
    (WBUS_MBR_TEMP_SENSOR),                \
    (WBUS_DRV_WAIT_TIME_UNIT_6_US),        \
    (WBUS_DRV_RETRY_MAX_ERR_UNIT_6),       \
    (WBUS_DRV_RETRY_TIME_ERR_UNIT_6_MS),   \
    (WBUS_DRV_RETRY_MAX_REQ_UNIT_6),       \
    (WBUS_DRV_RETRY_TIME_REQ_UNIT_6_MS),   \
    }, \
    { \
     \
    (WBUS_MBR_THERMO_CONNECT),             \
    (WBUS_DRV_WAIT_TIME_UNIT_7_US),        \
    (WBUS_DRV_RETRY_MAX_ERR_UNIT_7),       \
    (WBUS_DRV_RETRY_TIME_ERR_UNIT_7_MS),   \
    (WBUS_DRV_RETRY_MAX_REQ_UNIT_7),       \
    (WBUS_DRV_RETRY_TIME_REQ_UNIT_7_MS),   \
    }, \
    { \
     \
    (WBUS_MBR_THERMO_CALL),                \
    (WBUS_DRV_WAIT_TIME_UNIT_8_US),        \
    (WBUS_DRV_RETRY_MAX_ERR_UNIT_8),       \
    (WBUS_DRV_RETRY_TIME_ERR_UNIT_8_MS),   \
    (WBUS_DRV_RETRY_MAX_REQ_UNIT_8),       \
    (WBUS_DRV_RETRY_TIME_REQ_UNIT_8_MS),   \
    }, \
    { \
     \
    (WBUS_MBR_IUG),                         \
    (WBUS_DRV_WAIT_TIME_UNIT_9_US),         \
    (WBUS_DRV_RETRY_MAX_ERR_UNIT_9),        \
    (WBUS_DRV_RETRY_TIME_ERR_UNIT_9_MS),    \
    (WBUS_DRV_RETRY_MAX_REQ_UNIT_9),        \
    (WBUS_DRV_RETRY_TIME_REQ_UNIT_9_MS),    \
    }, \
    { \
     \
    (WBUS_MBR_PIGGYBACK),                   \
    (WBUS_DRV_WAIT_TIME_UNIT_10_US),        \
    (WBUS_DRV_RETRY_MAX_ERR_UNIT_10),       \
    (WBUS_DRV_RETRY_TIME_ERR_UNIT_10_MS),   \
    (WBUS_DRV_RETRY_MAX_REQ_UNIT_10),       \
    (WBUS_DRV_RETRY_TIME_REQ_UNIT_10_MS),   \
    }, \
    { \
     \
    (WBUS_MBR_ROOM_THERMOSTAT),             \
    (WBUS_DRV_WAIT_TIME_UNIT_11_US),        \
    (WBUS_DRV_RETRY_MAX_ERR_UNIT_11),       \
    (WBUS_DRV_RETRY_TIME_ERR_UNIT_11_MS),   \
    (WBUS_DRV_RETRY_MAX_REQ_UNIT_11),       \
    (WBUS_DRV_RETRY_TIME_REQ_UNIT_11_MS),   \
    }, \
    { \
     \
    (WBUS_MBR_CTL_HEATER_BOOST),            \
    (WBUS_DRV_WAIT_TIME_UNIT_12_US),        \
    (WBUS_DRV_RETRY_MAX_ERR_UNIT_12),       \
    (WBUS_DRV_RETRY_TIME_ERR_UNIT_12_MS),   \
    (WBUS_DRV_RETRY_MAX_REQ_UNIT_12),       \
    (WBUS_DRV_RETRY_TIME_REQ_UNIT_12_MS),   \
    }, \
    { \
     \
    (WBUS_MBR_13),                          \
    (WBUS_DRV_WAIT_TIME_UNIT_13_US),        \
    (WBUS_DRV_RETRY_MAX_ERR_UNIT_13),       \
    (WBUS_DRV_RETRY_TIME_ERR_UNIT_13_MS),   \
    (WBUS_DRV_RETRY_MAX_REQ_UNIT_13),       \
    (WBUS_DRV_RETRY_TIME_REQ_UNIT_13_MS),   \
    }, \
    { \
     \
    (WBUS_MBR_TEMP_SENSOR_T_LIN_R),         \
    (WBUS_DRV_WAIT_TIME_UNIT_14_US),        \
    (WBUS_DRV_RETRY_MAX_ERR_UNIT_14),       \
    (WBUS_DRV_RETRY_TIME_ERR_UNIT_14_MS),   \
    (WBUS_DRV_RETRY_MAX_REQ_UNIT_14),       \
    (WBUS_DRV_RETRY_TIME_REQ_UNIT_14_MS),   \
    }, \
    { \
     \
    (WBUS_MBR_TEST_UNIT_WTT),               \
    (WBUS_DRV_WAIT_TIME_UNIT_15_US),        \
    (WBUS_DRV_RETRY_MAX_ERR_UNIT_15),       \
    (WBUS_DRV_RETRY_TIME_ERR_UNIT_15_MS),   \
    (WBUS_DRV_RETRY_MAX_REQ_UNIT_15),       \
    (WBUS_DRV_RETRY_TIME_REQ_UNIT_15_MS),   \
    }, \
} 


#define WBUS_DRIVER_BAUDRATE_CONFIGURATION \
{\
    { \
     \
    (WBUS_DRV_BAUDRATE_300),                 \
    (WBUS_DRV_BAUDRATE_300_BD),              \
    (WBUS_DRV_INT_BYTE_TIMEOUT_300_US),      \
    (WBUS_DRV_RESPONSE_TIMEOUT_MIN_300_US),  \
    (WBUS_DRV_RESPONSE_TIMEOUT_MAX_300_US),  \
    (WBUS_DRV_INT_MESSAGE_TIME_MIN_300_US),  \
    (WBUS_DRV_INT_MESSAGE_TIME_MAX_300_US)   \
    }, \
    { \
     \
    (WBUS_DRV_BAUDRATE_600),                 \
    (WBUS_DRV_BAUDRATE_600_BD),              \
    (WBUS_DRV_INT_BYTE_TIMEOUT_600_US),      \
    (WBUS_DRV_RESPONSE_TIMEOUT_MIN_600_US),  \
    (WBUS_DRV_RESPONSE_TIMEOUT_MAX_600_US),  \
    (WBUS_DRV_INT_MESSAGE_TIME_MIN_600_US),  \
    (WBUS_DRV_INT_MESSAGE_TIME_MAX_600_US)   \
    }, \
    { \
     \
    (WBUS_DRV_BAUDRATE_1200),                 \
    (WBUS_DRV_BAUDRATE_1200_BD),              \
    (WBUS_DRV_INT_BYTE_TIMEOUT_1200_US),      \
    (WBUS_DRV_RESPONSE_TIMEOUT_MIN_1200_US),  \
    (WBUS_DRV_RESPONSE_TIMEOUT_MAX_1200_US),  \
    (WBUS_DRV_INT_MESSAGE_TIME_MIN_1200_US),  \
    (WBUS_DRV_INT_MESSAGE_TIME_MAX_1200_US)   \
    }, \
    { \
     \
    (WBUS_DRV_BAUDRATE_2400),                 \
    (WBUS_DRV_BAUDRATE_2400_BD),              \
    (WBUS_DRV_INT_BYTE_TIMEOUT_2400_US),      \
    (WBUS_DRV_RESPONSE_TIMEOUT_MIN_2400_US),  \
    (WBUS_DRV_RESPONSE_TIMEOUT_MAX_2400_US),  \
    (WBUS_DRV_INT_MESSAGE_TIME_MIN_2400_US),  \
    (WBUS_DRV_INT_MESSAGE_TIME_MAX_2400_US)   \
    }, \
    { \
     \
    (WBUS_DRV_BAUDRATE_4800),                 \
    (WBUS_DRV_BAUDRATE_4800_BD),              \
    (WBUS_DRV_INT_BYTE_TIMEOUT_4800_US),      \
    (WBUS_DRV_RESPONSE_TIMEOUT_MIN_4800_US),  \
    (WBUS_DRV_RESPONSE_TIMEOUT_MAX_4800_US),  \
    (WBUS_DRV_INT_MESSAGE_TIME_MIN_4800_US),  \
    (WBUS_DRV_INT_MESSAGE_TIME_MAX_4800_US)   \
    }, \
    { \
     \
    (WBUS_DRV_BAUDRATE_9600),                 \
    (WBUS_DRV_BAUDRATE_9600_BD),              \
    (WBUS_DRV_INT_BYTE_TIMEOUT_9600_US),      \
    (WBUS_DRV_RESPONSE_TIMEOUT_MIN_9600_US),  \
    (WBUS_DRV_RESPONSE_TIMEOUT_MAX_9600_US),  \
    (WBUS_DRV_INT_MESSAGE_TIME_MIN_9600_US),  \
    (WBUS_DRV_INT_MESSAGE_TIME_MAX_9600_US)   \
    }, \
    { \
     \
    (WBUS_DRV_BAUDRATE_19200),                 \
    (WBUS_DRV_BAUDRATE_19200_BD),              \
    (WBUS_DRV_INT_BYTE_TIMEOUT_19200_US),      \
    (WBUS_DRV_RESPONSE_TIMEOUT_MIN_19200_US),  \
    (WBUS_DRV_RESPONSE_TIMEOUT_MAX_19200_US),  \
    (WBUS_DRV_INT_MESSAGE_TIME_MIN_19200_US),  \
    (WBUS_DRV_INT_MESSAGE_TIME_MAX_19200_US)   \
    }, \
    { \
     \
    (WBUS_DRV_BAUDRATE_38400),                 \
    (WBUS_DRV_BAUDRATE_38400_BD),              \
    (WBUS_DRV_INT_BYTE_TIMEOUT_38400_US),      \
    (WBUS_DRV_RESPONSE_TIMEOUT_MIN_38400_US),  \
    (WBUS_DRV_RESPONSE_TIMEOUT_MAX_38400_US),  \
    (WBUS_DRV_INT_MESSAGE_TIME_MIN_38400_US),  \
    (WBUS_DRV_INT_MESSAGE_TIME_MAX_38400_US)   \
    }, \
    { \
     \
    (WBUS_DRV_BAUDRATE_76800),                 \
    (WBUS_DRV_BAUDRATE_76800_BD),              \
    (WBUS_DRV_INT_BYTE_TIMEOUT_76800_US),      \
    (WBUS_DRV_RESPONSE_TIMEOUT_MIN_76800_US),  \
    (WBUS_DRV_RESPONSE_TIMEOUT_MAX_76800_US),  \
    (WBUS_DRV_INT_MESSAGE_TIME_MIN_76800_US),  \
    (WBUS_DRV_INT_MESSAGE_TIME_MAX_76800_US)   \
    }, \
    { \
     \
    (WBUS_DRV_BAUDRATE_115000),                 \
    (WBUS_DRV_BAUDRATE_115000_BD),              \
    (WBUS_DRV_INT_BYTE_TIMEOUT_115000_US),      \
    (WBUS_DRV_RESPONSE_TIMEOUT_MIN_115000_US),  \
    (WBUS_DRV_RESPONSE_TIMEOUT_MAX_115000_US),  \
    (WBUS_DRV_INT_MESSAGE_TIME_MIN_115000_US),  \
    (WBUS_DRV_INT_MESSAGE_TIME_MAX_115000_US)   \
    } \
} 



void WBusDriver_InitAll( void );

#define WBUS_DRV_WAIT_TIME_START( wbus_dev )          WBusDriver_WaitTimeStart( (wbus_dev) )

#define WBUS_DRV_INTERBYTE_TIMEOUT_START( wbus_dev )  WBusDriver_InterbyteTimeoutStart( (wbus_dev) )


#endif 


