#ifndef _IUG_INCLUDE_H_
#define _IUG_INCLUDE_H_

#if defined(SOFTWARETEST)
    #include "../test_src/stubs/IUG_include.h"
#else
#include "IUG_appl_version.h"
#include "IUG_adjust.h"
#include "IUG_comm_utility_debug_adjust.h"
#include "IUG_adjust_task_monitoring.h"
#include "WBusAdjust.h"


#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#ifndef PROJECT_H
#include "Project.h"
#endif
#include "Std_Types.h"


#include "rh850_main.h"


#ifndef DIO_H
#include "Dio.h"
#endif
#ifndef FEE_H
#include "Fee.h"
#endif
#ifndef SCI_H
#include "Sci.h"
#endif
#ifndef ADC_H
#include "Adc.h"
#endif
#ifndef DET_H
#include "Det.h"
#endif
#ifndef ECUM_H
#include "EcuM.h"
#endif
#ifndef OSTM_H
#include "Ostm.h"
#endif

#include "Rst.h"

#include "hal_io.h"
#include "hal_nvm.h"
#include "hal_nvm_asynch.h"
#include "hal_can.h"
#include "hal_spi.h"
#include "hal_sys.h"
#include "Rtca.h"
#include "Mem_Cfg.h"
#include "Tau_Cfg.h"


#include "graph_code.h"
#include "IUG_user_api_include.h"
#include "IUG_user_api_priv_prototypes.h"


#include "app_main.h"
#include "app_usercode.h"


#include "sfl_db.h"
#include "sfl_can_db.h"
#include "sfl_diagnostic_trouble_code_cfg.h"
#include "sfl_diagnostic_trouble_code.h"
#include "sfl_math.h"
#include "sfl_pattern_generator.h"
#include "sfl_filter.h"
#include "sfl_crc.h"
#include "sfl_bl_protocol.h"
#include "sfl_utils.h"
#include "IUG_error_handler_freeze_frame.h"
#include "IUG_error_handler_nvm_diagnosis_storage.h"


#include "bgm111.h"


#include "FreeRTOS.h"


#include "IUG_types_basic.h"
#include "os_task_management.h"
#include "IUG_task_monitoring_include.h"
#include "IUG_stack_monitoring_include.h"
#include "app_tasks.h"


#include "system_init.h"
#include "Stbc.h"


#include "IUG_comm_config.h"
#include "IUG_comm_defines.h"
#include "webasto_parameter.h"
#include "IUG_webasto_parameter_if.h"


#include "WBusInclude.h"


#include "IUG_utility_parlist_abstraction.h"
#include "IUG_site__con_target.h"
#include "IUG_unit_cmd_button_constant.h"
#include "IUG_utility_comm_abstraction.h"
#include "IUG_constant_cronus.h"
#include "IUG_constant.h"
#include "IUG_unit_constant.h"
#include "IUG_unit_constant_user_api.h"
#include "IUG_unit_constant_data_support.h"
#include "IUG_communication__con.h"
#include "IUG_comm_state.h"
#include "IUG_error_handler__constant.h"
#include "IUG_utility_read_dtc_heater_constant_dtc.h"
#include "IUG_utility_read_dtc_heater_constant.h"
#include "IUG_unit_gen_detection_constant.h"
#include "IUG_unit_gen_heater_type.h"
#include "IUG_unit_gen_heater_type_cfg.h"
#include "IUG_init_op_global_cfg_constant.h"

#include "IUG_queue.h"

#include "IUG_utility_rd_wr_interface.h"
#include "IUG_utility_read_id.h"
#include "IUG_utility_unit_specific_data_constant.h"
#include "IUG_utility_unit_data_support_constant_int.h"
#include "IUG_utility_unit_data_support_constant.h"
#include "IUG_utility_component.h"
#include "IUG_types.h"
#include "IUG_unit_types.h"
#include "IUG_unit_ghi_types.h"


#include "IUG_cfg.h"
#include "IUG_unit_cfg.h"


#include "l99pm72.h"
#include "dfl_bmp280.h"


#include "lin_stack.h"
#include "ble_flasher.h"


#include "IUG_wbus_message_cfg.h"
#include "IUG_comm_utility.h"
#include "IUG_comm_utility_debug.h"
#include "IUG_comm_utility_debug_trace_record_adjust.h"
#include "IUG_comm_utility_debug_trace_record.h"
#include "IUG_communication.h"
#include "IUG_communication_ghi.h"
#include "IUG_communication_interface.h"
#include "IUG_communication_interface_ghi.h"
#include "IUG_comm_bluetooth.h"
#include "IUG_comm_broadcast.h"
#include "IUG_comm_btm.h"
#include "IUG_comm_can.h"
#include "IUG_comm_comp_test.h"
#include "IUG_comm_dtc.h"
#include "IUG_comm_eol_param.h"
#include "IUG_comm_eeprom.h"
#include "IUG_comm_lock.h"
#include "IUG_comm_mab.h"
#include "IUG_comm_msg_cmd.h"
#include "IUG_comm_msg_IBN.h"
#include "IUG_comm_read_id.h"
#include "IUG_comm_read_status.h"
#include "IUG_comm_routine.h"
#include "IUG_comm_routine_appl.h"
#include "IUG_comm_sam.h"
#include "IUG_comm_special.h"
#include "IUG_comm_task_fast.h"
#include "IUG_comm_unit_spec_data.h"
#include "IUG_diagnosis.h"
#include "IUG_diagnosis_dtc_filter.h"
#include "IUG_diagnosis_dtc_share.h"
#include "IUG_diagnosis_units_status.h"
#include "IUG_diagnosis_units_status_mux.h"
#include "IUG_diagnosis_units_status_extended_mux.h"
#include "IUG_diagnosis_units_status_cfg.h"
#include "IUG_watchdog_manager.h"
#include "IUG_system_function.h"
#include "IUG_unit.h"
#include "IUG_unit_cmd_button.h"
#include "IUG_unit_cmd_button_display_cfg.h"
#include "IUG_unit_ghi_cfg.h"
#include "IUG_init_op_rout_module_1_cfg.h"
#include "IUG_init_op_rout_module_2_cfg.h"
#include "IUG_init_op_rout_module_3_cfg.h"
#include "IUG_init_op_global_cfg.h"
#include "IUG_init_op_utilities.h"
#include "IUG_init_op_handler.h"
#include "IUG_init_op_handler_auto.h"
#include "IUG_init_op_rout_module_1.h"
#include "IUG_init_op_rout_module_2.h"
#include "IUG_init_op_rout_module_3.h"
#include "IUG_unit_gen_control.h"
#include "IUG_unit_gen_control_display_cfg.h"
#include "IUG_unit_gen_control_lock.h"
#include "IUG_unit_gen_detection.h"
#include "IUG_unit_gen_heater.h"
#include "IUG_unit_ghi_detection.h"
#include "IUG_unit_ghi_heater.h"
#include "IUG_unit_ghi_heater_pending.h"
#include "IUG_unit_gen_temp_sensor.h"
#include "IUG_unit_bluetooth_ctl.h"
#ifdef IUG_BLUETOOTH_SIMULATION
#include "IUG_unit_bluetooth_ctl_simulation.h"
#endif
#include "IUG_unit_iug.h"
#include "IUG_unit_iug_user_api.h"
#include "IUG_unit_heater_air.h"
#include "IUG_unit_heater_water.h"
#include "IUG_unit_multi_ctl.h"
#include "IUG_unit_telestart_ctl.h"
#include "IUG_unit_thermo_call.h"
#include "IUG_unit_thermo_connect.h"
#include "IUG_unit_user_device.h"
#ifdef IUG_UNT_SIMULATION_EN
#include "IUG_unit_simulation.h"
#endif
#include "IUG_callback.h"
#include "IUG_task.h"
#include "IUG_unit_cfg_union.h"


#include "IUG_sandbox.h"


#include "param_config_management.h"
#include "param_config_management_usr_cfg_data.h"
#include "param_config_management_interface.h"
#include "IUG_appl_para_cfg.h"


#include "IUG_utility_signal_filter.h"
#include "IUG_utility_signal_filter_cfg.h"
#include "IUG_utility_access.h"
#include "IUG_utility_calc.h"
#include "IUG_utility_read_dtc_heater.h"
#include "IUG_utility_read_id_interface.h"
#include "IUG_utility_component_interface.h"
#include "IUG_utility_read_status.h"
#include "IUG_utility_read_status_list.h"
#include "IUG_utility_test.h"
#include "IUG_utility_test_ghi.h"
#include "IUG_utility_fast_access.h"
#include "IUG_utility_eeprom_rd_wr.h"
#include "IUG_unit_ghi_cmd.h"
#include "IUG_unit_ghi_read_id.h"
#include "IUG_unit_ghi_read_status.h"
#include "IUG_utility_unit_specific_data.h"
#include "IUG_utility_unit_data_support.h"


#include "IUG_EOL_version.h"
#include "IUG_EOL_cfg.h"
#include "IUG_EOL_can_db_tables.h"
#include "IUG_EOL_test_busses.h"
#include "IUG_EOL_init.h"
#include "IUG_EOL_main.h"


#include "sfl_pwm_io_main.h"
#include "IUG_car_climate_control_handler.h"
#include "dfl_bmp280_main.h"
#include "cfl_obd_diagnosis.h"
#include "cfl_obd_diagnosis_main.h"
#include "dfl_hsd_vn7040.h"
#include "IUG_environment_data.h"
#include "sfl_pwm_io.h"


#include "user_code.h"


#endif 


#endif 


