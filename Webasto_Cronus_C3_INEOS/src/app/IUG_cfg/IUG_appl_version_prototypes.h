#ifndef IUG_APPL_VERSION_PROTOTYPES_H
#define IUG_APPL_VERSION_PROTOTYPES_H
/*----------------------------------------------------------------------------*/
/**
* \file         IUG_appl_version_prototypes.h
* \brief
* \date         20210426
* \author       dpaul
*
*/
/*----------------------------------------------------------------------------*/
/**
* \defgroup
* \brief
* \details
* <pre>
* Version     based on BSW       Date        Author      Change
* V1.0.0      V1.2.4             20210927    DPL         WIBUA-1700 bugfix for support of thermocall (TC4) after initial operation
* </pre>
*/
/*----------------------------------------------------------------------------*/

// ---------------------------------------------------------------------------------------------------
// defines
// ---------------------------------------------------------------------------------------------------
// Basesoftware Version --> BASESOFTWARE_VERSION
#define BASE_SW_VERSION_PROTO_MAJOR     1  ///< Software Major number      !only one sign is supported --> struct_PCM_DATA_SYSTEM uint8_t
#define BASE_SW_VERSION_PROTO_MINOR     0  ///< Software Minor number      !only one sign is supported --> struct_PCM_DATA_SYSTEM uint8_t
#define BASE_SW_VERSION_PROTO_BUILD     0  ///< Software Revision number   2 sign is supported with BCD use and save binary, reason is we want 2 and have one byte --> struct_PCM_DATA_SYSTEM uint8_t

#define BASE_SW_VERSION_BUILD_OFFSET_PT 25 ///< Offset value for the build version of the BSW, to identify the build type: prototype
/*----------------------------------------------------------------------------*/
// Check the Firmware revision numbers.
#if( (((BASE_SW_VERSION_PROTO_MAJOR) < 0) || ((BASE_SW_VERSION_PROTO_MAJOR) > 9))||\
     (((BASE_SW_VERSION_PROTO_MINOR) < 0) || ((BASE_SW_VERSION_PROTO_MINOR) > 9))  )
    #error "The 'BASE_SW_VERSION_MAJOR' and 'BASE_SW_VERSION_MINOR' numbers must be in range of '0...9'!"
#endif

#if( (((BASE_SW_VERSION_PROTO_BUILD) < 0) || ((BASE_SW_VERSION_PROTO_BUILD) > 99)) )
    #error "The 'BASE_SW_VERSION_BUILD' number must be in range of '0...99'!"
#endif

/*----------------------------------------------------------------------------*/
#endif // IUG_APPL_VERSION_PROTOTYPES_H
