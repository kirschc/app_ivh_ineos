#ifndef IUG_APPL_VERSION_RELEASE_CANDIDATE_H
#define IUG_APPL_VERSION_RELEASE_CANDIDATE_H

#define BASE_SW_VERSION_RC_MAJOR        1  
#define BASE_SW_VERSION_RC_MINOR        0  
#define BASE_SW_VERSION_RC_BUILD        3  

#define BASE_SW_VERSION_BUILD_OFFSET_RC 50 
#if( (((BASE_SW_VERSION_RC_MAJOR) < 0) || ((BASE_SW_VERSION_RC_MAJOR) > 9))||\
     (((BASE_SW_VERSION_RC_MINOR) < 0) || ((BASE_SW_VERSION_RC_MINOR) > 9))  )
    #error "The 'BASE_SW_VERSION_RC_MAJOR' and 'BASE_SW_VERSION_RC_MINOR' numbers must be in range of '0...9'!"
#endif

#if( (((BASE_SW_VERSION_RC_BUILD) < 0) || ((BASE_SW_VERSION_RC_BUILD) > 99)) )
    #error "The 'BASE_SW_VERSION_RC_BUILD' number must be in range of '0...99'!"
#endif

#endif 


