#ifndef __IUG_WEBASTO_PARAMETER_IF_H_
#define __IUG_WEBASTO_PARAMETER_IF_H_


#define IUG_AUXDRVFLG_NONE                          0x00u
#define IUG_AUXDRVFLG_UI_AUTOMATIC_AUX_HEATING      0x01u 
#define IUG_AUXDRVFLG_FCN_AUTOMATIC_AUX_HEATING     0x02u 
#define IUG_AUXDRVFLG_AUT_AUX_HEATING_REQ_ON        0x04u 
#define IUG_AUXDRVFLG_AUT_AUX_HEATING_REQ_OFF       0x08u 
#define IUG_AUXDRVFLG_CONFIG_DRIVE_HEATING          0x10u 
#define IUG_AUXDRVFLG_SWITCH_DRIVE_HEATING          0x20u 
#define IUG_AUXDRVFLG_40                            0x40u
#define IUG_AUXDRVFLG_80                            0x80u



#define IUG_IS_UI_AUTOMATIC_AUX_HEATING \
            (0u != (IUG_UNT_IUG_CTL.aux_drv_heating_flag & (IUG_AUXDRVFLG_UI_AUTOMATIC_AUX_HEATING)) )

#define IUG_IS_FCN_AUTOMATIC_AUX_HEATING \
            (0u != (IUG_UNT_IUG_CTL.aux_drv_heating_flag & (IUG_AUXDRVFLG_FCN_AUTOMATIC_AUX_HEATING)) )

#define IUG_IS_AUT_AUX_HEATING_REQ_ON \
            (0u != (IUG_UNT_IUG_CTL.aux_drv_heating_flag & (IUG_AUXDRVFLG_AUT_AUX_HEATING_REQ_ON)) )
#define IUG_CLR_AUT_AUX_HEATING_REQ_ON() \
            (IUG_UNT_IUG_CTL.aux_drv_heating_flag &= ~(IUG_AUXDRVFLG_AUT_AUX_HEATING_REQ_ON))
#define IUG_SET_AUT_AUX_HEATING_REQ_ON() \
            (IUG_UNT_IUG_CTL.aux_drv_heating_flag |= (IUG_AUXDRVFLG_AUT_AUX_HEATING_REQ_ON))


#define IUG_IS_AUT_AUX_HEATING_REQ_OFF \
            (0u != (IUG_UNT_IUG_CTL.aux_drv_heating_flag & (IUG_AUXDRVFLG_AUT_AUX_HEATING_REQ_OFF)) )
#define IUG_CLR_AUT_AUX_HEATING_REQ_OFF() \
            (IUG_UNT_IUG_CTL.aux_drv_heating_flag &= ~(IUG_AUXDRVFLG_AUT_AUX_HEATING_REQ_OFF))
#define IUG_SET_AUT_AUX_HEATING_REQ_OFF() \
            (IUG_UNT_IUG_CTL.aux_drv_heating_flag |= (IUG_AUXDRVFLG_AUT_AUX_HEATING_REQ_OFF))


#define IUG_IS_CONFIG_DRIVE_HEATING \
            (0u != (IUG_UNT_IUG_CTL.aux_drv_heating_flag & (IUG_AUXDRVFLG_CONFIG_DRIVE_HEATING)) )

#define IUG_IS_SWITCH_DRIVE_HEATING \
            (0u != (IUG_UNT_IUG_CTL.aux_drv_heating_flag & (IUG_AUXDRVFLG_SWITCH_DRIVE_HEATING)) )





#define ID_WEBPAR_ID_MAX                    ext_parameter_db_access.entries_parameter_id

#define ID_WEBPAR_ID_INVALID                (enum_parameter_id)0x7FFFu     


#define ID_WEBPAR_DIAG_ID_MAX               ext_parameter_db_access.entries_parameter_diagnose_id

#define ID_WEBPAR_DIAG_ID_INVALID           (enum_parameter_diagnose_id)0x7FFFu     


#define WEBPAR_PARAMETER_DATA_SIZE          ext_parameter_db_access.size_type_parameterdataflash    
#define WEBPAR_PARAMETER_DB_ADDR            ext_parameter_db_access.addr_parameter_db               
#define WEBPAR_PARAMETER_DIAG_DB_ADDR       ext_parameter_db_access.addr_parameter_diag_db          
#define WEBPAR_PARAMETER_TOOL_ADDR          ext_parameter_db_access.addr_parameter_tool_xml_zip     
#define WEBPAR_PARAMETER_TOOL_SIZE          ext_parameter_db_access.size_parameter_tool_xml_zip     


#define IUG_WEBPAR_HG_1_AVAILABLE           ext_iug_webpar_hgx_available[(IUG_HEATER_UNIT_HG1)]
#define IUG_WEBPAR_HG_2_AVAILABLE           ext_iug_webpar_hgx_available[(IUG_HEATER_UNIT_HG2)]



typedef enum
{
    ID_PARAMETER_CHECKED_ENUM_FIRST = 0x4000u,  
    ID_WBUS_COMMUNICATION_RETRY_CHECKED,
    ID_LIMIT_12V_LOW_VOLTAGE_CHECKED,
    ID_LIMIT_12V_HIGH_VOLTAGE_CHECKED,
    ID_LIMIT_24V_LOW_VOLTAGE_CHECKED,
    ID_LIMIT_24V_HIGH_VOLTAGE_CHECKED,
    ID_TIME_BTS_WAT_CHECKED,
    ID_MODE_BTS_AIR_CHECKED,
    ID_LEVEL_BTS_AIR_CHECKED,
    ID_SETPOINT_BTS_AIR_CHECKED,
    ID_SETPOINT_BOOST_BTS_AIR_CHECKED,
    ID_SETPOINT_ECO_BTS_AIR_CHECKED,
    ID_MAX_HEATING_TIME_IPD_AIR_CHECKED,
    ID_MAX_HEATING_TIME_IPD_WAT_CHECKED,
    ID_DEFAULT_MAX_TIME_AIR_CHECKED,
    ID_DEFAULT_MAX_TIME_WAT_CHECKED,
    ID_TEMP_BLOWER_ON_WAT_CHECKED,
    ID_BUFFER_WAT_HG_TEMP_CHECKED,
    ID_UI_AUTOMATIC_AUX_HEATING_CHECKED,
    ID_LEVEL_AUX_HEATING_CONTROL_OFF_CHECKED,
    ID_LEVEL_AUX_HEATING_CONTROL_ON_CHECKED,
    ID_LEVEL_OUT_TEMP_AUX_HEATING_CHECKED,
    ID_TIMER_AVAILABLE_CHECKED,
    ID_TIMER_DISPOSABLE_DAY_CHECKED,
    ID_TIMER_HG_ASSIGNMENT_CHECKED,
    ID_TIME_BUTTON_KONFIG_CHECKED,
    ID_EXTERNAL_AIR_PRESSURE_AVAILABLE_CHECKED,
    ID_VAL_EXTERNAL_AIR_PRESSURE_CHECKED,
    ID_CONFIG_BLE_CHECKED,
    ID_FCN_NONSTOP_AIR_HEATING_CHECKED,
    ID_FCN_NONSTOP_WAT_HEATING_CHECKED,
    ID_FCN_AUTOMATIC_AUX_HEATING_CHECKED,
    ID_FCN_ALTITUDE_CORRECTION_CHECKED,
    ID_PARAMETER_CHECKED_ENUM_MAX,                  
    ID_PARAMETER_CHECKED_ENUM_FORCE_TYPE = 0x7FFF   
} enum_parameter_id_checked;


typedef enum
{
    ERR_WEBPAR_NONE           = 0u,
    ERR_WEBPAR_INVALID_ID,
    ERR_WEBPAR_INVALID_TYPE,
    ERR_WEBPAR_PARA_ADDRESS,
    ERR_WEBPAR_ENUM_MAX
} enum_ERR_WEBPAR_t;


typedef struct
{
    enum_parameter_id_checked   id_checked;     
    enum_parameter_id           parameter_id;   

} struct_parameter_id_checked_cfg;


typedef struct_parameter_id_checked_cfg     struct_parameter_id_checked_sorted;
#if 0
typedef struct
{
    enum_parameter_id_checked   id_checked;     

    enum_parameter_id           parameter_id;   

} struct_parameter_id_checked_sorted;
#endif


typedef uint16_t    parameter_id_checked_value_t;


void    check_parameter_check_para_by_id( const enum_parameter_id para_id );
void    check_parameter_checked_cyclic( void );
void    check_parameter_checked_init( void );
void    check_parameter_checked_db_cfg( void );
void    check_parameter_db_cfg( void );
void    check_parameter_diag_db_cfg( void );


int32_t get_para_by_id( const enum_parameter_id para_id );

uint8_t* get_para_ary_by_id( const enum_parameter_id para_id );


enum_ERR_WEBPAR_t get_para_by_id_checked( const enum_parameter_id param_id, uint32_t * const para_val );

enum_parameter_diagnose_id get_para_elements_diag_by_id( const enum_parameter_diagnose_id  diag_id,
                                                         struct_parameter_diag_db const * * const diag_db );

enum_parameter_id get_para_elements_by_id( const enum_parameter_id  param_id,
                                           struct_parameter_db_type const * * const p_ext_param_db,
                                           uint8_t * * const p_param_def,
                                           uint32_t  * const p_param_val,
                                           uint32_t  * const p_param_def_val,
                                           uint8_t   * const p_data_size     );


boolean set_para_by_id( const enum_parameter_id para_id, const int32_t val );

boolean set_para_ary_by_id( const enum_parameter_id para_id, const uint8_t * const ptr_val);


boolean test_para_by_description( const struct_parameter_db_type * const p_param_db, int32_t * const p_param_val );



_Static_assert((ID_WEBPAR_ID_INVALID)==((enum_parameter_id)0x7FFFu),"Analyze: 'ID_WEBPAR_ID_INVALID'!");


_Static_assert((ID_WEBPAR_DIAG_ID_INVALID)==((enum_parameter_diagnose_id)0x7FFFu),"Analyze: 'ID_WEBPAR_DIAG_ID_INVALID'!");


#endif 


