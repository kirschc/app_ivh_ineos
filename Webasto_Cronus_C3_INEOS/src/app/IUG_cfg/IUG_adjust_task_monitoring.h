#ifndef __IUG_ADJUST_TASK_MONITORING_H__
#define __IUG_ADJUST_TASK_MONITORING_H__


#ifdef TSKMON_TASK_LOAD_SIMULATION_EN
    #warning "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    #warning "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    #warning "Enable 'TSKMON_TASK_LOAD_SIMULATION_EN' for development/debugging only!!!"
    #warning "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
    #warning "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
#endif


#ifdef IUG_STARTUP_TIME_INDICATOR
    #define IUG_STARTUP_TIME_INDICATOR_START()    (void)hal_io_do_set( (DO_IO3_PU), 1u )
    #define IUG_STARTUP_TIME_INDICATOR_STOP()     (void)hal_io_do_set( (DO_IO3_PU), 0u )
    #warning "Read comments for 'IUG_STARTUP_TIME_INDICATOR'!"
#else
    #define IUG_STARTUP_TIME_INDICATOR_START()
    #define IUG_STARTUP_TIME_INDICATOR_STOP()
#endif


#ifdef IUG_IDLE_TASK_LOAD_INDICATOR
    #define IUG_IDLE_TASK_LOAD_INDICATOR_START()    (void)hal_io_do_set( (DO_IO2_PU) , 1u )
    #define IUG_IDLE_TASK_LOAD_INDICATOR_STOP()     (void)hal_io_do_set( (DO_IO2_PU), 0u )
    #warning "Read comments for 'IUG_IDLE_TASK_LOAD_INDICATOR'!"
#else
    #define IUG_IDLE_TASK_LOAD_INDICATOR_START()
    #define IUG_IDLE_TASK_LOAD_INDICATOR_STOP()
#endif


#ifdef IUG_1MS_TASK_LOAD_INDICATOR
    #define IUG_1MS_TASK_LOAD_INDICATOR_START()    (void)hal_io_do_set( (DO_IO2_PU), 1u )
    #define IUG_1MS_TASK_LOAD_INDICATOR_STOP()     (void)hal_io_do_set( (DO_IO2_PU), 0u )
    #warning "Read comments for 'IUG_1MS_TASK_LOAD_INDICATOR'!"
#else
    #define IUG_1MS_TASK_LOAD_INDICATOR_START()
    #define IUG_1MS_TASK_LOAD_INDICATOR_STOP()
#endif


#ifdef IUG_10MS_TASK_LOAD_INDICATOR
    #define IUG_10MS_TASK_LOAD_INDICATOR_START()    (void)hal_io_do_set( (DO_IO2_PU), 1u )
    #define IUG_10MS_TASK_LOAD_INDICATOR_STOP()     (void)hal_io_do_set( (DO_IO2_PU), 0u )
    #warning "Read comments for 'IUG_10MS_TASK_LOAD_INDICATOR'!"
#else
    #define IUG_10MS_TASK_LOAD_INDICATOR_START()
#define IUG_10MS_TASK_LOAD_INDICATOR_STOP()
#endif


#ifdef IUG_20MS_TASK_LOAD_INDICATOR
    #define IUG_20MS_TASK_LOAD_INDICATOR_START()    (void)hal_io_do_set( (DO_IO2_PU), 1u )
    #define IUG_20MS_TASK_LOAD_INDICATOR_STOP()     (void)hal_io_do_set( (DO_IO2_PU), 0u )
    #warning "Read comments for 'IUG_20MS_TASK_LOAD_INDICATOR'!"
#else
    #define IUG_20MS_TASK_LOAD_INDICATOR_START()
#define IUG_20MS_TASK_LOAD_INDICATOR_STOP()
#endif


#ifdef IUG_100MS_TASK_LOAD_INDICATOR
    #define IUG_100MS_TASK_LOAD_INDICATOR_START()    (void)hal_io_do_set( (DO_IO2_PU), 1u )
    #define IUG_100MS_TASK_LOAD_INDICATOR_STOP()     (void)hal_io_do_set( (DO_IO2_PU), 0u )
    #warning "Read comments for 'IUG_100MS_TASK_LOAD_INDICATOR'!"
#else
    #define IUG_100MS_TASK_LOAD_INDICATOR_START()
    #define IUG_100MS_TASK_LOAD_INDICATOR_STOP()
#endif


#ifdef IUG_SYSTEM_1MS_TASK_LOAD_INDICATOR
    #define IUG_SYSTEM_1MS_TASK_LOAD_INDICATOR_START()    (void)hal_io_do_set( (DO_IO2_PU), 1u )
    #define IUG_SYSTEM_1MS_TASK_LOAD_INDICATOR_STOP()     (void)hal_io_do_set( (DO_IO2_PU), 0u )
    #warning "Read comments for 'IUG_SYSTEM_1MS_TASK_LOAD_INDICATOR'!"
#else
    #define IUG_SYSTEM_1MS_TASK_LOAD_INDICATOR_START()
    #define IUG_SYSTEM_1MS_TASK_LOAD_INDICATOR_STOP()
#endif


#ifdef IUG_CUSTOMER_xMS_TASK_LOAD_INDICATOR
    #define IUG_CUSTOMER_xMS_TASK_LOAD_INDICATOR_START()    (void)hal_io_do_set( (DO_IO2_PU), 1u )
    #define IUG_CUSTOMER_xMS_TASK_LOAD_INDICATOR_STOP()     (void)hal_io_do_set( (DO_IO2_PU), 0u )
    #warning "Read comments for 'IUG_CUSTOMER_xMS_TASK_LOAD_INDICATOR'!"
#else
    #define IUG_CUSTOMER_xMS_TASK_LOAD_INDICATOR_START()
    #define IUG_CUSTOMER_xMS_TASK_LOAD_INDICATOR_STOP()
#endif


#endif 



