#ifndef __IUG_WBUS_MESSAGE_CFG_H_
#define __IUG_WBUS_MESSAGE_CFG_H_



void IUG_WBusMsgDescription_Init ( void );


int16_t IUG_WBusMsgDescription_TestMsg( struct_WBUS_MSG_DESCRIPTION_CFG_t * const wbus_msg );


uint8_t IUG_WBusMsgDescription_TestMsgBTM( const uint8_t * const wbus_data, struct_WBUS_MSG_BTM_DESCRIPTION_CFG_t * const wbus_msg_btm );


extern const struct_WBUS_MSG_DESCRIPTION_CFG_t      WBUS_MSG_DESCRIPTION[];

#ifdef IUG_WBusMsgDescription_SortCmp_SORT_IN_RAM_BUFFER
extern const struct_WBUS_MSG_DESCRIPTION_CFG_t      WBUS_MSG_DESCRIPTION_DEF[];
#endif

extern const uint16_t   ext_iug_entries_WBUS_MSG_DESCRIPTION;

extern const uint8_t    ext_IUG_wbus_msg_btm_description_entries;


#endif 


