#ifndef _IUG_APPL_VERSION_H_
#define _IUG_APPL_VERSION_H_

#include "IUG_appl_version_prototypes.h"
#include "IUG_appl_version_release_candidate.h"


#define BASE_SW_VERSION_MAJOR                 1   
#define BASE_SW_VERSION_MINOR                 2   
#define BASE_SW_VERSION_BUILD                 29



#define PCM_DATA_VERSION_MAJOR          ('0' + (BASE_SW_VERSION_MAJOR))
#define PCM_DATA_VERSION_MINOR          ('0' + (BASE_SW_VERSION_MINOR))
#define PCM_DATA_VERSION_BUILD          (BASE_SW_VERSION_BUILD)         

#define PCM_DATA_VERSION    ( (0xA5000000u) | (PCM_DATA_VERSION_MAJOR << 16u) | (PCM_DATA_VERSION_MINOR << 8u) | PCM_DATA_VERSION_BUILD )


#if( (((BASE_SW_VERSION_MAJOR) < 0) || ((BASE_SW_VERSION_MAJOR) > 9))||\
     (((BASE_SW_VERSION_MINOR) < 0) || ((BASE_SW_VERSION_MINOR) > 9))  )
    #error "The 'BASE_SW_VERSION_MAJOR' and 'BASE_SW_VERSION_MINOR' numbers must be in range of '0...9'!"
#endif

#if( (((BASE_SW_VERSION_BUILD) < 0) || ((BASE_SW_VERSION_BUILD) > 99)) )
    #error "The 'BASE_SW_VERSION_BUILD' number must be in range of '0...99'!"
#endif


#define     QUOTE(x) #x
#define     QUOTE2(xx) #xx
#define     BUILD_BASE_SW_VERSION_STRING(x,y,z) QUOTE(x) "." QUOTE(y) "." QUOTE2(z) "      "
#define     SW_VERSION_NAME "BASE SW:" BUILD_BASE_SW_VERSION_STRING(BASE_SW_VERSION_MAJOR, BASE_SW_VERSION_MINOR, BASE_SW_VERSION_BUILD)


#endif 


