
#include <string.h>
#include "system_init.h"
#include "dsl_cfg.h"
#include "obd_diagnosis_cfg.h"
#include "FreeRTOSConfig.h"


uint8_t ucHeap[ (configTOTAL_HEAP_SIZE) ];

struct_shared_dyn_data ext_shared_dyn_data = {0u};

void sys_init_set_dyn_values(void)
{
    memcpy(&ext_shared_dyn_data.dyn_sys_data.appl_sw_ver, (APPL_SW_VERSION), (PAR_LEN_SW_VERSION));

    memcpy(&ext_shared_dyn_data.dyn_sys_data.appl_module_name, (APPL_MODULE_NAME), (PAR_LEN_MODULENAME));

    ext_shared_dyn_data.dyn_obd_data.hw_can_bus_idx   = (OBD_CAN_BUS_HW_IDX);
    ext_shared_dyn_data.dyn_obd_data.pid_req_interval = (OBD_PID_REQUEST_INTERVAL_MS);
    ext_shared_dyn_data.dyn_obd_data.can_id_11bit_req = (OBD_CAN_ID_11BIT_REQ);
    ext_shared_dyn_data.dyn_obd_data.can_id_11bit_res = (OBD_CAN_ID_11BIT_RES);
    ext_shared_dyn_data.dyn_obd_data.can_id_29bit_req = (OBD_CAN_ID_29BIT_REQ);
    ext_shared_dyn_data.dyn_obd_data.can_id_29bit_res = (OBD_CAN_ID_29BIT_RES);
}




