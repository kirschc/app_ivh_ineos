
#ifndef _APP_TASKS_H_
#define _APP_TASKS_H_

#ifdef _MASTER_APP_TASKS_C_            
#define _AC_                                                     
#else
#define _AC_                           extern                    
#endif

#include "task.h"



#pragma pack(1) 
typedef struct
{
    enum_OS_TASK_MGT_TASKS     task_id;                
    uint8_t                    stack_used_max_percent; 
    uint8_t                    unused2;                
    uint8_t                    unused3;                

    uint16_t                   stack_unused_max_bytes; 
    uint16_t                   stack_res;              

    uint32_t                   res2[1u];           

    uint16_t                   task_duty_min_us;   
    uint16_t                   task_duty_max_us;   
    uint16_t                   task_duty_avg_us;   
    uint8_t                    task_duty_avg_perc; 
    uint8_t                    task_res0;          

    uint32_t                   task_res[1u];       

} struct_task_monitoring_data_nvm;
#pragma pack() 



#pragma pack(1) 
typedef struct
{
    uint32_t                        unique_key; 

    char                            task_stackoverflow[(configMAX_TASK_NAME_LEN)]; 

    struct_task_monitoring_data_nvm task_monitoring_nvm[(OS_TASK_MGT_TASK_ENUM_MAX)];

    uint8_t                         task_total_duty_min_perc;   
    uint8_t                         task_total_duty_max_perc;   
    uint8_t                         task_total_duty_avg_perc;   
    uint8_t                         unused; 

} struct_task_statistic_data;
#pragma pack() 


typedef struct
{
    enum_OS_TASK_MGT_TASKS              task;                   
    TaskHandle_t                        *ptr_task_handle;       

    struct_task_monitoring_data_nvm    *ptr_task_mon_nvm;       

    STKMON_STACK_ID_t            stack_id_monitoring;    
    TSKMON_TASK_ID_t              task_id_monitoring;     

} struct_task_free_stack_space_cfg;



_AC_ void OS_ApplTaskMonitoring_Stack_Task(void);

_AC_ void OS_ApplStackOverflowHook(
  TaskHandle_t *pxTask,
  signed char *pcTaskName);

_AC_ void OS_ApplTickHook(void);

_AC_ void OS_ApplIdleHook(void);

_AC_ void OS_ApplStartUpHook(void);

_AC_ void OS_ApplShutDownHook(void);

_AC_ void OS_SystemTask_1ms(
  void *param);

_AC_ void OS_ApplTask_1ms(
  void *param);

_AC_ void OS_ApplTask_10ms(
  void *param);

_AC_ void OS_ApplTask_20ms(
  void *param);

_AC_ void OS_ApplTask_100ms(
  void *param);

_AC_ void OS_ApplTask_Customer(
  void *param);

_AC_ void OS_AppInit_and_EOL_1ms(
  void *param);

uint64_t AppTask_getOperatingTime(void);

extern uint64_t AppTask_portTickCounter;
extern struct_task_statistic_data ext_task_statistic_data;

#ifdef _AC_
#undef _AC_
#endif

#else
#error Multiple include of "app_tasks.h"
#endif                                 


