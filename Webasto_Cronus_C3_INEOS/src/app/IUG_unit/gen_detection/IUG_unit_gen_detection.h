#ifndef _IUG_UNIT_GEN_DETECTION_H_
#define _IUG_UNIT_GEN_DETECTION_H_


#ifdef IUG_DEVELOPMENT_ACTIVE
#endif


#define IUG_UNIT_GEN_DETECTION_TIMEOUT_S                          90u   


#define IUG_UNIT_GEN_DETECTION_STATE_TIMEOUT_MS                10000u   

#define IUG_UNIT_GEN_DETECTION_SERVICE_TIMEOUT_MS               3000u   



typedef enum
{
    DPY_NAK_NONE    = 0x00u,  
    DPY_NAK_RD_ID   = 0x01u,  
    DPY_NAK_RD_ST   = 0x02u,  
    GENDPY_NAK_ENUM_FORCE_TYPE     = 0x7F    

} IUG_UNIT_GENDET_NAK_INFO_t;


typedef enum
{
    dpyNONE         =     0x0000u,  
    dpyWBUVER       =     0x0001u,  
    dpyECUCOD       =     0x0002u,  
    dpyHTRSGN       =     0x0004u,  
    dpyHTRTYP       =     0x0008u,  
    dpyHTRCAP       =     0x0010u,  
    dpyHTR_MF       =     0x0020u,  
    dpyMAXTIM       =     0x0040u,  
    dpyTSTIMP       =     0x0080u,  
    dpySERNUM       =     0x0100u,  
    dpySERN09       =     0x0200u,  
    dpyEE0107       =     0x0400u,  
    dpyEE0261       =     0x0800u,  
    dpyEE_APC       = 0x00001000u,  
    dpyEE_CPV       = 0x00002000u,  
    dpyEE_CPA       = 0x00004000u,  
    dpySTAIMP       = 0x00008000u,  
    dpySTALST       = 0x00010000u,  
    dpySTAFNC       = 0x00020000u,  
    GENDPY_ENUM_FORCE_TYPE     = 0x7FFFFFFF 
} IUG_UNIT_GENDET_PROPERTY_t;


typedef enum
{
    dpyRXNONE       = 0x0000u,  
    dpy_RO_01       = 0x0001u,  
    dpy_RO_02       = 0x0002u,  
    dpy_RO_03       = 0x0004u,  
    dpy_RO_04       = 0x0008u,  
    dpy_RO_05       = 0x0010u,  

    dpy_RW_01       = 0x0100u,  
    dpy_RW_02       = 0x0200u,  
    dpy_RW_03       = 0x0400u,  
    dpy_RW_04       = 0x0800u,  
    dpy_RW_05       = 0x1000u,  
    GENDPYTLR_ENUM_FORCE_TYPE     = 0x7FFF  
} IUG_UNIT_GENDET_T_LIN_R_PROPERTY_t;

#define IUG_UNIT_GENDET_T_LIN_R_PROPERTY_MSK   ((dpy_RO_01)|(dpy_RO_02)|(dpy_RO_03)|(dpy_RO_04)|(dpy_RO_05)|\
                                                (dpy_RW_01)|(dpy_RW_02)|(dpy_RW_03)|(dpy_RW_04)|(dpy_RW_05))


typedef struct
{
    IUG_UNIT_GENDET_STATE_t     state;                          
    IUG_UNIT_GENDET_STATE_t     state_next;                     
    IUG_UNIT_GENDET_STATE_t     trigger_state;                  

    IUG_HTR_MSK_ID_t            htr_available;                  

    uint8_t                     read_unit_id_en;                
    uint8_t                     comp_test_en;                   

    uint16_t                    read_id_st_cur;                 

    uint8_t                     htr_read_err_req_ibn_scanning;  

    WBUS_INSTANCE_t             detc_scan_wbus_ins;             
    WBUS_MEMBER_t               detc_scan_wbus_mbr;             
    IUG_HEATER_UNIT_ENUM_t      detc_idx_htr;                   

    uint8_t                     detc_scan_progress;             
    uint8_t                     detc_scan_turn_or_pause;        
    uint8_t                     detc_retry_cnt;                 

    IUG_UNIT_GENDET_STATE_t     detc_ghi_state;                 
    IUG_HEATER_UNIT_ENUM_t      detc_ghi_idx_htr;               
    IUG_COMM_DEVICE_ID_t        detc_ghi_com_dev;               
    IUG_HTR_MSK_ID_t            detc_ghi_hgX_possible;          

    IUG_HTR_MSK_ID_t            detc_rsp_hgX;                   
    IUG_HTR_MSK_ID_t            detc_err_hgX;                   
    IUG_UNIT_GENDET_NAK_INFO_t  detc_nak_info;                  

    IUG_UNIT_GENDET_STATE_t     wait_cnt_trg_state;             
    IUG_UNIT_GENDET_STATE_t     state_loop;                     
    uint8_t                     wait_cnt;                       
    uint16_t                    detc_timeout_cnt;               
    uint16_t                    state_timeout_cnt;              

    IUG_UNIT_GENDET_PROPERTY_t          htr_property[(IUG_HEATER_UNIT_ENUM_MAX)];           
    IUG_UNIT_GENDET_T_LIN_R_PROPERTY_t  tlinr_property[(IUG_T_LIN_R_TEMP_SEN_ENUM_MAX)];    

    #ifdef IUG_TRIGGER_WBUS_SCANNING_DEVELOPMENT_EN
    uint8_t                     fast_ibn_triggering_flg_0x0B;   
    #endif

    #ifdef IUG_UNIT_GEN_DETECTION_DEV_SUPPORT_EN
    IUG_UNIT_GENDET_STATE_t     dbg_state_cyc_call;             
    enum_INIT_OP_ROUT_STATES    dbg_ibn_state_status;           

    IUG_UNIT_GENDET_STATE_t     dbg_state_trg;                  
    IUG_UNIT_GENDET_STATE_t     dbg_state_his[(IUG_UNIT_GEN_DETECTION_DEV_SUPPORT_EN)]; 
    #endif

} IUG_UNIT_GEN_DETECTION_t;


typedef struct
{
    IUG_UNIT_GENDET_STATE_t     state_cur;
    IUG_UNIT_GENDET_STATE_t     state_nxt;

} IUG_UNIT_GEN_DETECTION_STATE_SEQ_t;


typedef struct
{
    IUG_UNIT_GENDET_STATE_t     state_cur;
    uint16_t                    read_id_st;     

} IUG_UNIT_GEN_DETECTION_READ_ID_ST_t;


extern IUG_UNIT_GEN_DETECTION_t                     IUG_UNIT_GEN_DETECTION;
extern const IUG_UNIT_GEN_DETECTION_READ_ID_ST_t    IUG_UNIT_GENDET_READ_ID_ST[];
extern const uint8_t                                ext_entries_IUG_UNIT_GENDET_READ_ID_ST;



void                        IUG_unit_gen_detection_clr_heater_pty_nvm( const IUG_HEATER_UNIT_ENUM_t idx_htr );
enum_INIT_OP_ROUT_STATES    IUG_unit_gen_detection_cyclic( void );
void                        IUG_unit_gen_detection_cyclic_HeaterTrigger( void );
void                        IUG_unit_gen_detection_cyclic_task( void );
IUG_UNIT_GENDET_STATE_t     IUG_unit_gen_detection_get_state_next( const IUG_UNIT_GENDET_STATE_t sta_cur );
IUG_HTR_MSK_ID_t            IUG_unit_gen_detection_heater_available( void );
void                        IUG_unit_gen_detection_heater_set_retry_cnt( void );
uint8_t                     IUG_unit_gen_detection_is_active( void );
IUG_HG_AVAILABLE_t          IUG_unit_gen_detection_is_htr_available( const IUG_HEATER_UNIT_ENUM_t idx_htr, const uint8_t get_raw );
boolean                     IUG_unit_gen_detection_is_req_comp_test( void );
boolean                     IUG_unit_gen_detection_is_req_read_id_st( void );
WBUS_INSTANCE_t             IUG_unit_gen_detection_is_scanning( void );
boolean                     IUG_unit_gen_detection_read_dtc_required( void );
uint8_t                     IUG_unit_gen_detection_read_id_set_retry_cnt( void );
uint8_t                     IUG_unit_gen_detection_read_id_test_retry_cnt( void );
void                        IUG_unit_gen_detection_state_trigger_test_wait( void );


#endif 


