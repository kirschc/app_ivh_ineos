#ifndef __IUG_UNIT_BLUETOOTH_CTL_H_
#define __IUG_UNIT_BLUETOOTH_CTL_H_


#define IUG_BLUETOOTH_TIMER_DAY_T1                      0u    
#define IUG_BLUETOOTH_TIMER_DAY_T2                      1u    
#define IUG_BLUETOOTH_TIMER_DAY_T3                      2u    

#define IUG_BLUETOOTH_TIMER_WEEKDAY_MO                  0u    
#define IUG_BLUETOOTH_TIMER_WEEKDAY_TU                  1u    
#define IUG_BLUETOOTH_TIMER_WEEKDAY_WE                  2u    
#define IUG_BLUETOOTH_TIMER_WEEKDAY_TH                  3u    
#define IUG_BLUETOOTH_TIMER_WEEKDAY_FR                  4u    
#define IUG_BLUETOOTH_TIMER_WEEKDAY_SA                  5u    
#define IUG_BLUETOOTH_TIMER_WEEKDAY_SU                  6u    


#define IUG_BLUETOOTH_TIMSRV_ACTIVATE_SEL               0xA0u   
#define IUG_BLUETOOTH_TIMSRV_CHECK_SEL                  0xC0u   
#define IUG_BLUETOOTH_TIMSRV_DEACTIVATE_SEL             0xD0u   
#define IUG_BLUETOOTH_TIMSRV_DEACTIVATE_ALL             0xDAu   
#define IUG_BLUETOOTH_TIMSRV_DEACTIVATE_ALL_DAY         0xDDu   
#define IUG_BLUETOOTH_TIMSRV_UPDATE_TIMER_ACTIVATED     0xF0u   


#define IUG_BLUETOOTH_NVMWR_NONE                            0x00u
#define IUG_BLUETOOTH_NVMWR_WEBASTO_PARAMETER               0x01u
#define IUG_BLUETOOTH_NVMWR_BLE_TIMER_CTL                   0x02u


#define CRONUS_BLUETOOTH_CMD_CONFIG_BUTTON_BTS                  0x0Fu
#define CRONUS_BLUETOOTH_CMD_CFG_HEAT_VENT_BTS_WAT 0x10u
#define CRONUS_BLUETOOTH_CMD_HG1_AVAILABLE                      0x11u
#define CRONUS_BLUETOOTH_CMD_TIME_BTS_WAT                       0x12u
#define CRONUS_BLUETOOTH_CMD_NONSTOP_HEATING_AIR                0x13u
#define CRONUS_BLUETOOTH_CMD_NONSTOP_HEATING_WAT                0x14u
#define CRONUS_BLUETOOTH_CMD_FINAL_DURATION_AIR                 0x15u
#define CRONUS_BLUETOOTH_CMD_FINAL_DURATION_WAT                 0x16u
#define CRONUS_BLUETOOTH_CMD_BLUETOOTH_AIR_PRESSURE             0x17u
#define CRONUS_BLUETOOTH_CMD_BLUETOOTH_BATTERY_VOLTAGE          0x18u
#define CRONUS_BLUETOOTH_CMD_GET_CRONUS_STATE_WAT               0x19u
#define CRONUS_BLUETOOTH_CMD_GET_CRONUS_STATE_AIR               0x1Au
#define CRONUS_BLUETOOTH_CMD_SETPOINT_BOOST_BTS_AIR             0x1Bu
#define CRONUS_BLUETOOTH_CMD_SETPOINT_ECO_BTS_AIR               0x1Cu

#define CRONUS_BLUETOOTH_CMD_TEMP_INT                           0x20u
#define CRONUS_BLUETOOTH_CMD_TEMP_EXT                           0x21u
#define CRONUS_BLUETOOTH_CMD_TEMP_COOLANT                       0x22u
#define CRONUS_BLUETOOTH_CMD_GET_HG_STATE_AIR                   0x24u
#define CRONUS_BLUETOOTH_CMD_GET_HG_STATE_WAT                   0x25u

#define CRONUS_BLUETOOTH_CMD_HEATER_INTERLOCK_WAT               0x27u
#define CRONUS_BLUETOOTH_CMD_HEATER_INTERLOCK_AIR               0x28u
#define CRONUS_BLUETOOTH_CMD_MODE_BTS_AIR                       0x30u
#define CRONUS_BLUETOOTH_CMD_LEVEL_BTS_AIR                      0x31u
#define CRONUS_BLUETOOTH_CMD_SETPOINT_BTS_AIR                   0x32u
#define CRONUS_BLUETOOTH_CMD_AUTOMATIC_AUX_HEAT_ALLOWED         0x33u
#define CRONUS_BLUETOOTH_CMD_HG2_AVAILABLE                      0x34u
#define CRONUS_BLUETOOTH_CMD_TIME_BTS_AIR                       0x35u
#define CRONUS_BLUETOOTH_CMD_CONFIG_AUX_HEAT_AVAILABLE          0x36u
#define CRONUS_BLUETOOTH_CMD_TIMER_HG_ASSIGNMENT                0x47u   

#define CRONUS_BLUETOOTH_CMD_TIMER_ACTIVATED                    0x48u   

#define CRONUS_BLUETOOTH_CMD_TIMER_AVAILABLE                    0x49u   
#define CRONUS_BLUETOOTH_CMD_TIMER_DISPOSABLE_DAY               0x4Au   

#define CRONUS_BLUETOOTH_CMD_TIMER_MO                           0x4Bu   
#define CRONUS_BLUETOOTH_CMD_TIMER_TU                           0x4Cu   
#define CRONUS_BLUETOOTH_CMD_TIMER_WE                           0x4Du   
#define CRONUS_BLUETOOTH_CMD_TIMER_TH                           0x4Eu   
#define CRONUS_BLUETOOTH_CMD_TIMER_FR                           0x4Fu   
#define CRONUS_BLUETOOTH_CMD_TIMER_SA                           0x50u   
#define CRONUS_BLUETOOTH_CMD_TIMER_SU                           0x51u   

#define CRONUS_BLUETOOTH_CMD_BLE_AVAILABLE_TIME                 0x60u   
#define CRONUS_BLUETOOTH_CMD_GET_SET_TIME_CRONUS                0x61u   
#define CRONUS_BLUETOOTH_CMD_BLE_HG1_SWITCH                     0x81u   
#define CRONUS_BLUETOOTH_CMD_BLE_HG2_SWITCH                     0x82u   



#define IUG_BLUETOOTH_DEPLOY_READ           0u  
#define IUG_BLUETOOTH_DEPLOY_WRITE          1u  

#define IUG_BLE_CMD_ACS_RD                  0x01u   
#define IUG_BLE_CMD_ACS_WR                  0x02u   
#define IUG_BLE_CMD_ACS_RDWR                ((IUG_BLE_CMD_ACS_RD)|(IUG_BLE_CMD_ACS_WR))

#define IUG_BLE_CMD_HDL_NONE                0u  
#define IUG_BLE_CMD_HDL_CMD                 1u  
#define IUG_BLE_CMD_HDL_TIME                2u  


typedef struct
{
    uint8_t         ble_cmd;

    uint8_t         access_ctl;     
    uint8_t         hdl_func;       

    enum_parameter_id   par_id;     

    enum_parameter_id   par_chk;    

} structure_IUG_BLUETOOTH_CTL_COMMAND_IF_DESCRIPTION_t;
typedef structure_IUG_BLUETOOTH_CTL_COMMAND_IF_DESCRIPTION_t    IUG_BLE_CTL_COMMAND_IF_DESCR_t;


extern uint8_t IUG_bluetooth_ctl_awoken_cnt;

extern const IUG_BLE_CTL_COMMAND_IF_DESCR_t IUG_BLE_COMMAND_IF_DESCR[];
extern uint8_t                              ext_iug_entries_IUG_BLE_COMMAND_IF_DESCR;


uint8_t         IUG_bluetooth_ctl_ble_cmd_config_get( const uint8_t ble_cmd );
IUG_COMMAND_t   IUG_bluetooth_ctl_cmd_pre_evaluate( const IUG_UNIT_ID_t unit_id, 
                                                    const IUG_HEATER_UNIT_ENUM_t evaluate_for_htr_idx,  
                                                    IUG_UNIT_COMMAND_REQUEST_t * const req_cmd_ble );
void            IUG_bluetooth_ctl_ctl_cyclic( const uint8_t idx_unit_tree );
void            IUG_bluetooth_ctl_ctl_init( const uint8_t idx_unit_tree );
void            IUG_bluetooth_ctl_cyclic_non_unit_related( void );
uint8_t         IUG_bluetooth_ctl_ble_cmd_received_dispatch( const uint8_t rd_wr, const uint8_t * const p_data_rec, uint8_t * const p_data_snd );
uint8_t         IUG_bluetooth_ctl_ble_cmd_request_cmd_get_set( const uint8_t cfg_entry, const uint8_t rd_wr, const uint8_t * const p_data_rec, uint8_t * const p_data_snd );
uint8_t         IUG_bluetooth_ctl_ble_cmd_request_timer_get_set( const uint8_t cfg_entry, const uint8_t rd_wr, const uint8_t * const p_data_rec, uint8_t * const p_data_snd );
void            IUG_bluetooth_ctl_test_failure( structure_IUG_UNIT_CONTROL_HEADER_t * const p_unt_ctl );
boolean         IUG_bluetooth_ctl_timer_service( const uint8_t service, const uint8_t timer_day, const uint8_t timer_week );
boolean         IUG_bluetooth_ctl_timer_test_available_disposable( const uint8_t idx_timer, const uint8_t idx_wday );
void            IUG_bluetooth_ctl_timer_alarm_callback( const uint8_t cbk_code );
void            IUG_bluetooth_ctl_timer_processing( void );
boolean         IUG_bluetooth_ctl_update_heater_binding( void );


#endif 


