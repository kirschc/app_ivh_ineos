#ifndef _IUG_UNIT_CONSTANT_T_LIN_R_H_
#define _IUG_UNIT_CONSTANT_T_LIN_R_H_



#define IUG_T_LIN_R_TEMP_SENSOR_UNIT_NUMBER_MAX          2u 


typedef enum
{
    IUG_T_LIN_R_TEMP_SEN_INT =  0u,  
    IUG_T_LIN_R_TEMP_SEN_EXT =  1u,  
    IUG_T_LIN_R_TEMP_SEN_ENUM_MAX,               
    IUG_T_LIN_R_TEMP_SEN_ENUM_FORCE_TYPE = 0x7F  

} IUG_T_LIN_R_TEMP_SEN_ENUM_t;

_Static_assert((IUG_T_LIN_R_TEMP_SEN_INT)     ==(0u),"Analyze: 'IUG_T_LIN_R_TEMP_SEN_INT'!");
_Static_assert((IUG_T_LIN_R_TEMP_SEN_EXT)     ==(1u),"Analyze: 'IUG_T_LIN_R_TEMP_SEN_EXT'!");
_Static_assert((IUG_T_LIN_R_TEMP_SEN_ENUM_MAX)==(IUG_T_LIN_R_TEMP_SENSOR_UNIT_NUMBER_MAX),"Analyze: 'IUG_T_LIN_R_TEMP_SEN_ENUM_t'!");



typedef enum
{
    IUG_T_LIN_R_CFG_NOT_CONFIGURED   = 0x00u,    
    IUG_T_LIN_R_CFG_VARIANT_RES_NONE = 0x01u,    
    IUG_T_LIN_R_CFG_VARIANT_RES_20   = 0x02u,    
    IUG_T_LIN_R_CFG_VARIANT_RES_50   = 0x03u,    
    IUG_T_LIN_R_CFG_VARIANT_RES_100  = 0x04u,    

    IUG_T_LIN_R_CFG_ENUM_INVALID     = 0xFFu,    
    IUG_T_LIN_R_CFG_ENUM_FORCE_TYPE  = 0x7F      

} IUG_T_LIN_R_CFG_TYPE_ID_t;


_Static_assert(0u==(IUG_T_LIN_R_CFG_NOT_CONFIGURED)     ,"Change encoding for webasto parameter 'ID_CONFIG_T_LIN_R_VARIANT_BE_WBUS, ID_CONFIG_T_LIN_R_VARIANT_HG_WBUS, ID_DETECTED_T_LIN_R_VARIANT_BE_WBUS, ID_DETECTED_T_LIN_R_VARIANT_HG_WBUS' too!!!");
_Static_assert(1u==(IUG_T_LIN_R_CFG_VARIANT_RES_NONE)   ,"Change encoding for webasto parameter 'ID_CONFIG_T_LIN_R_VARIANT_BE_WBUS, ID_CONFIG_T_LIN_R_VARIANT_HG_WBUS, ID_DETECTED_T_LIN_R_VARIANT_BE_WBUS, ID_DETECTED_T_LIN_R_VARIANT_HG_WBUS' too!!!");
_Static_assert(2u==(IUG_T_LIN_R_CFG_VARIANT_RES_20)     ,"Change encoding for webasto parameter 'ID_CONFIG_T_LIN_R_VARIANT_BE_WBUS, ID_CONFIG_T_LIN_R_VARIANT_HG_WBUS, ID_DETECTED_T_LIN_R_VARIANT_BE_WBUS, ID_DETECTED_T_LIN_R_VARIANT_HG_WBUS' too!!!");
_Static_assert(3u==(IUG_T_LIN_R_CFG_VARIANT_RES_50)     ,"Change encoding for webasto parameter 'ID_CONFIG_T_LIN_R_VARIANT_BE_WBUS, ID_CONFIG_T_LIN_R_VARIANT_HG_WBUS, ID_DETECTED_T_LIN_R_VARIANT_BE_WBUS, ID_DETECTED_T_LIN_R_VARIANT_HG_WBUS' too!!!");
_Static_assert(4u==(IUG_T_LIN_R_CFG_VARIANT_RES_100)    ,"Change encoding for webasto parameter 'ID_CONFIG_T_LIN_R_VARIANT_BE_WBUS, ID_CONFIG_T_LIN_R_VARIANT_HG_WBUS, ID_DETECTED_T_LIN_R_VARIANT_BE_WBUS, ID_DETECTED_T_LIN_R_VARIANT_HG_WBUS' too!!!");

typedef enum
{
    IUG_T_LIN_R_DET_NOT_CONFIGURED   = 0x00u,    
    IUG_T_LIN_R_DET_UNKNOWN          = 0x01u,    
    IUG_T_LIN_R_DET_VARIANT_RES_NONE = 0x02u,    
    IUG_T_LIN_R_DET_VARIANT_RES_20   = 0x03u,    
    IUG_T_LIN_R_DET_VARIANT_RES_50   = 0x04u,    
    IUG_T_LIN_R_DET_VARIANT_RES_100  = 0x05u,    

    IUG_T_LIN_R_DET_ENUM_INVALID     = 0xFFu,    
    IUG_T_LIN_R_DET_ENUM_FORCE_TYPE  = 0x7F      

} IUG_T_LIN_R_DET_TYPE_ID_t;

_Static_assert(0u==(IUG_T_LIN_R_DET_NOT_CONFIGURED)     ,"Change encoding for webasto parameter 'ID_DETECTED_T_LIN_R_VARIANT_BE_WBUS, ID_DETECTED_T_LIN_R_VARIANT_HG_WBUS' too!!!");
_Static_assert(1u==(IUG_T_LIN_R_DET_UNKNOWN)            ,"Change encoding for webasto parameter 'ID_DETECTED_T_LIN_R_VARIANT_BE_WBUS, ID_DETECTED_T_LIN_R_VARIANT_HG_WBUS' too!!!");
_Static_assert(2u==(IUG_T_LIN_R_DET_VARIANT_RES_NONE)   ,"Change encoding for webasto parameter 'ID_DETECTED_T_LIN_R_VARIANT_BE_WBUS, ID_DETECTED_T_LIN_R_VARIANT_HG_WBUS' too!!!");
_Static_assert(3u==(IUG_T_LIN_R_DET_VARIANT_RES_20)     ,"Change encoding for webasto parameter 'ID_DETECTED_T_LIN_R_VARIANT_BE_WBUS, ID_DETECTED_T_LIN_R_VARIANT_HG_WBUS' too!!!");
_Static_assert(4u==(IUG_T_LIN_R_DET_VARIANT_RES_50)     ,"Change encoding for webasto parameter 'ID_DETECTED_T_LIN_R_VARIANT_BE_WBUS, ID_DETECTED_T_LIN_R_VARIANT_HG_WBUS' too!!!");
_Static_assert(5u==(IUG_T_LIN_R_DET_VARIANT_RES_100)    ,"Change encoding for webasto parameter 'ID_DETECTED_T_LIN_R_VARIANT_BE_WBUS, ID_DETECTED_T_LIN_R_VARIANT_HG_WBUS' too!!!");

typedef enum
{
    MSKTLINR__                 = 0x00u,    
    MSKTLINR_1                 = 0x01u,    
    MSKTLINR_2                 = 0x02u,    
    MSKTLINR__ENUM_FORCE_TYPE  = 0x7F      

} IUG_T_LIN_R_MSK_ID_t;


typedef enum
{
    T_LIN_R_TYPE_IDX_BY_CFG_VAL = 0u,
    T_LIN_R_TYPE_IDX_BY_DET_VAL     ,
    T_LIN_R_TYPE_IDX_ENUM_MAX       
} enum_T_LIN_R_TYPE_IDX;



#endif 


