#ifndef __IUG_UNT_USER_DEVICE_H_
#define __IUG_UNT_USER_DEVICE_H_


_Static_assert((IUG_USER_DEVICE_MAX)==(3u),"Analyze: 'IUG_USER_DEVICE_MAX'");
_Static_assert((IUG_USER_DEVICE_MAX)==(API_USRDVC_ID_ENUM_MAX),"Analyze: 'API_USRDVC_ID_ENUM_MAX'");
_Static_assert((IUG_USER_DEVICE_MAX)==(API_USRDVC_USR_ID_ENUM_MAX),"Analyze: 'API_USRDVC_USR_ID_ENUM_MAX'");


_Static_assert((API_USRDVC_CMD_ENUM_MAX)==(8u),"Analyze: 'API_USRDVC_CMD_ENUM_MAX'");


#define IUG_USER_DEVICE_ASSIGN_TO_FIRST_WEBPAR_CFG       3u 
#define IUG_USER_DEVICE_ASSIGN_TO_LAST_WEBPAR_CFG       ((IUG_USER_DEVICE_ASSIGN_TO_FIRST_WEBPAR_CFG)+(IUG_USER_DEVICE_MAX)-1u)
_Static_assert((IUG_USER_DEVICE_ASSIGN_TO_FIRST_WEBPAR_CFG)==(3u),"Analyze: 'IUG_USER_DEVICE_ASSIGN_TO_FIRST_WEBPAR_CFG'");
_Static_assert((IUG_USER_DEVICE_ASSIGN_TO_LAST_WEBPAR_CFG) ==(5u),"Analyze: 'IUG_USER_DEVICE_ASSIGN_TO_LAST_WEBPAR_CFG'");

#define IUG_TARGET_ASSIGN_TO_LAST_WEBPAR_CFG        ((IUG_HEATER_UNIT_ENUM_MAX) - 1u)
_Static_assert((IUG_TARGET_ASSIGN_TO_LAST_WEBPAR_CFG)==((IUG_HEATER_UNIT_ENUM_MAX) - 1u),"Analyze: 'IUG_TARGET_ASSIGN_TO_LAST_WEBPAR_CFG'");


_Static_assert((IUG_HTRTRMFLG_OFF_REGULAR)                  <=(0xFFFFu),"Analyze: 'IUG_HTRTRMFLG_OFF_REGULAR'");
_Static_assert((IUG_HTRTRMFLG_OFF_DUE_MONITORING_OFF_CMD)   <=(0xFFFFu),"Analyze: 'IUG_HTRTRMFLG_OFF_DUE_MONITORING_OFF_CMD'");
_Static_assert((IUG_HTRTRMFLG_OFF_DUE_IUG)                  <=(0xFFFFu),"Analyze: 'IUG_HTRTRMFLG_OFF_DUE_IUG'");
_Static_assert((IUG_HTRTRMFLG_CMD_INVALID)                  <=(0xFFFFu),"Analyze: 'IUG_HTRTRMFLG_CMD_INVALID'");
_Static_assert((IUG_HTRTRMFLG_PND_MSG_NAK_TOUT)             <=(0xFFFFu),"Analyze: 'IUG_HTRTRMFLG_PND_MSG_NAK_TOUT'");
_Static_assert((IUG_HTRTRMFLG_OFF_DUE_MONITORING)           <=(0xFFFFu),"Analyze: 'IUG_HTRTRMFLG_OFF_DUE_MONITORING'");
_Static_assert((IUG_HTRTRMFLG_PND_TIMEOUT)                  <=(0xFFFFu),"Analyze: 'IUG_HTRTRMFLG_PND_TIMEOUT'");
_Static_assert((IUG_HTRTRMFLG_OFF_DUE_DTC_STFL_HGVP)        <=(0xFFFFu),"Analyze: 'IUG_HTRTRMFLG_OFF_DUE_DTC_STFL_HGVP'");
_Static_assert((IUG_HTRTRMFLG_OFF_DUE_MONITORING_INT)       <=(0xFFFFu),"Analyze: 'IUG_HTRTRMFLG_OFF_DUE_MONITORING_INT'");

#define IUG_UDVTRMFLG_REQ_TERMINATION                 0u \
                                                    |(IUG_HTRTRMFLG_OFF_REGULAR) \
                                                    |(IUG_HTRTRMFLG_OFF_DUE_MONITORING_OFF_CMD) \
                                                    |(IUG_HTRTRMFLG_OFF_DUE_IUG) \
                                                    |(IUG_HTRTRMFLG_CMD_INVALID) \
                                                    |(IUG_HTRTRMFLG_PND_MSG_NAK_TOUT) \
                                                    |(IUG_HTRTRMFLG_OFF_DUE_MONITORING) \
                                                    |(IUG_HTRTRMFLG_PND_TIMEOUT) \
                                                    |(IUG_HTRTRMFLG_OFF_DUE_DTC_STFL_HGVP) \
                                                    |(IUG_HTRTRMFLG_OFF_DUE_MONITORING_INT) \
                                                    | 0u



typedef struct
{
    IUG_COMMAND_t           iug_cmd;    
    API_USER_DEVICE_CMD_t   udv_cmd;    

    uint8_t                 set_on;     

}  structure_IUG_USER_DEVICE_COMMAND_TRANSLATION_t;
typedef structure_IUG_USER_DEVICE_COMMAND_TRANSLATION_t    IUG_USER_DEVICE_CMD_TRANSLATION_t;


void            IUG_user_device_cmd_create( IUG_UNT_USER_DEVICE_CTL_t * const p_unt_ctl );
API_USER_DEVICE_CMD_t    IUG_user_device_cmd_for_all_bound( const API_USER_DEVICE_CMD_t udv_cmd_reg,
                                                            const IUG_USRDEV_MSK_ID_t cmd_req_for_udv );
IUG_COMMAND_t   IUG_user_device_cmd_pre_evaluate( const IUG_UNIT_ID_t unit_id_cmd, 
                                                  IUG_UNIT_COMMAND_REQUEST_t * const req_cmd_inp,
                                                  API_USER_DEVICE_ID_t       * const cmd_req_for_udv,
                                                  API_USER_DEVICE_CMD_t      * const p_udv_cmd );
boolean         IUG_user_device_cmd0_decide_udv( const IUG_UNIT_ID_t unit_id_cmd, 
                                                 IUG_UNIT_COMMAND_REQUEST_PENDING_t * const req_cmd_inp );
IUG_COMMAND_t   IUG_user_device_cmd2_evaluate_and_provide_udv(
                                                IUG_UNIT_COMMAND_REQUEST_t  * const req_cmd_inp,
                                                const IUG_COMMAND_t         udv_iug_cmd,
                                                const API_USER_DEVICE_CMD_t udv_cmd, 
                                                API_USER_DEVICE_ID_t        * const cmd_req_for_udv,
                                                API_USER_DEVICE_CMD_t       * const cmd_for_udv,
                                                const uint8_t               req_cmd_used_flag );
void            IUG_user_device_ctl_cyclic( const uint8_t idx_unit_tree );
void            IUG_user_device_ctl_cyclic_slow( void );
void            IUG_user_device_ctl_init( const uint8_t idx_unit_tree );
void            IUG_user_device_cyclic_services( IUG_UNT_USER_DEVICE_CTL_t * const p_unt_ctl );
uint16_t        IUG_user_device_diag_serve_unit_status( const IUG_UNITS_STA_SEL_MUX_t selector_mux, uint8_t * const p_can_msg );
void            IUG_user_device_handle_acknowlege( IUG_UNT_USER_DEVICE_CTL_t * const p_unt_ctl );
void            IUG_user_device_handle_pending_int( IUG_UNT_USER_DEVICE_CTL_t * const p_unt_ctl );
void            IUG_user_device_handle_termination( IUG_UNT_USER_DEVICE_CTL_t * const p_unt_ctl );
API_USER_DEVICE_CMD_t IUG_user_device_iug_cmd_translation( const IUG_COMMAND_t iug_cmd, IUG_USER_DEVICE_CMD_TRANSLATION_t * const cmd_translation );
void            IUG_user_device_process_failure( IUG_UNT_USER_DEVICE_CTL_t * const p_unt_ctl );
void            IUG_user_device_stop_activity( IUG_UNT_USER_DEVICE_CTL_t * const p_unt_ctl, const API_USER_DEVICE_ID_t udv_idx );
void            IUG_user_device_test_failure( IUG_UNT_USER_DEVICE_CTL_t * const p_unt_ctl );
void            IUG_user_device_update_property_fast( void );


#endif 


