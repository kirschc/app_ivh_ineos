#ifndef _IUG_UNIT_CONSTANT_H_
#define _IUG_UNIT_CONSTANT_H_



#define IUG_HEATER_UNIT_NUMBER_MAX          8u


#define IUG_HEATER_UNIT_WBUS_NUMBER_MAX     2u


#define IUG_UNT_HEATER_GHI_WBUS_VERSION_MIN     0x40u   
#define IUG_UNT_HEATER_GHI_WBUS_VERSION_MAX     0x9Fu   


#define IUG_UNT_HEATER_HG1_DEF      1u
#define IUG_UNT_HEATER_HG2_DEF      ((IUG_UNT_HEATER_HG1_DEF) + 1u)
#define IUG_UNT_HEATER_HG3_DEF      ((IUG_UNT_HEATER_HG1_DEF) + 2u)
#define IUG_UNT_HEATER_HG4_DEF      ((IUG_UNT_HEATER_HG1_DEF) + 3u)
#define IUG_UNT_HEATER_HG5_DEF      ((IUG_UNT_HEATER_HG1_DEF) + 4u)
#define IUG_UNT_HEATER_HG6_DEF      ((IUG_UNT_HEATER_HG1_DEF) + 5u)
#define IUG_UNT_HEATER_HG7_DEF      ((IUG_UNT_HEATER_HG1_DEF) + 6u)
#define IUG_UNT_HEATER_HG8_DEF      ((IUG_UNT_HEATER_HG1_DEF) + 7u)



typedef enum
{
    IUG_UNT_IUG = 0,                    
    IUG_UNT_HEATER_HG1 = (IUG_UNT_HEATER_HG1_DEF),  
    IUG_UNT_HEATER_HG2 = (IUG_UNT_HEATER_HG2_DEF),  
    IUG_UNT_HEATER_HG3 = (IUG_UNT_HEATER_HG3_DEF),  
    IUG_UNT_HEATER_HG4 = (IUG_UNT_HEATER_HG4_DEF),  
    IUG_UNT_HEATER_HG5 = (IUG_UNT_HEATER_HG5_DEF),  
    IUG_UNT_HEATER_HG6 = (IUG_UNT_HEATER_HG6_DEF),  
    IUG_UNT_HEATER_HG7 = (IUG_UNT_HEATER_HG7_DEF),  
    IUG_UNT_HEATER_HG8 = (IUG_UNT_HEATER_HG8_DEF),  
    IUG_UNT_MULTI_CTL_3TO4,             
    IUG_UNT_THERMO_CONNECT,             
    IUG_UNT_THERMO_CALL,                
    IUG_UNT_TELE_START,                 
    IUG_UNT_BLUETOOTH_CTL_AIR,          
    IUG_UNT_BLUETOOTH_CTL_WAT,          
    IUG_UNT_TEMP_SENSOR_INT,            
    IUG_UNT_TEMP_SENSOR_EXT,            
    IUG_UNT_TEMP_SENSOR_T_LIN_R_INT,    
    IUG_UNT_TEMP_SENSOR_T_LIN_R_EXT,    
    IUG_UNT_CMD_BUTTON,                 
    #ifdef IUG_UNT_SIMULATION_EN
    IUG_UNT_SIMULATION,                 
    #endif
    IUG_UNT_USER_API,                   
    IUG_UNT_USER_DEVICE,                
    IUG_UNT_ENUM_MAX,               
    IUG_UNT_ENUM_FORCE_TYPE = 0x7F  

} enum_IUG_UNIT_ID_t;
typedef enum_IUG_UNIT_ID_t  IUG_UNIT_ID_t;

_Static_assert((IUG_UNT_HEATER_HG2) ==((IUG_UNT_HEATER_HG1)+1u),"Analyze: 'IUG_UNT_HEATER_HG2'!");
_Static_assert((IUG_UNT_HEATER_HG3) ==((IUG_UNT_HEATER_HG2)+1u),"Analyze: 'IUG_UNT_HEATER_HG3'!");
_Static_assert((IUG_UNT_HEATER_HG4) ==((IUG_UNT_HEATER_HG3)+1u),"Analyze: 'IUG_UNT_HEATER_HG4'!");
_Static_assert((IUG_UNT_HEATER_HG5) ==((IUG_UNT_HEATER_HG4)+1u),"Analyze: 'IUG_UNT_HEATER_HG5'!");
_Static_assert((IUG_UNT_HEATER_HG6) ==((IUG_UNT_HEATER_HG5)+1u),"Analyze: 'IUG_UNT_HEATER_HG6'!");
_Static_assert((IUG_UNT_HEATER_HG7) ==((IUG_UNT_HEATER_HG6)+1u),"Analyze: 'IUG_UNT_HEATER_HG7'!");
_Static_assert((IUG_UNT_HEATER_HG8) ==((IUG_UNT_HEATER_HG7)+1u),"Analyze: 'IUG_UNT_HEATER_HG8'!");


typedef enum
{
    IUG_HEATER_UNIT_HG1 =  0u,  
    IUG_HEATER_UNIT_HG2 =  1u,  
    IUG_HEATER_UNIT_HG3 =  2u,  
    IUG_HEATER_UNIT_HG4 =  3u,  
    IUG_HEATER_UNIT_HG5 =  4u,  
    IUG_HEATER_UNIT_HG6 =  5u,  
    IUG_HEATER_UNIT_HG7 =  6u,  
    IUG_HEATER_UNIT_HG8 =  7u,  
    IUG_HEATER_UNIT_ENUM_MAX,               
    IUG_HEATER_UNIT_ENUM_FORCE_TYPE = 0x7F  

} IUG_HEATER_UNIT_ENUM_t;

#if( 8u == (IUG_HEATER_UNIT_NUMBER_MAX) )
_Static_assert((IUG_HEATER_UNIT_HG1)==(0u),"Analyze: 'IUG_HEATER_UNIT_HG1'");
_Static_assert((IUG_HEATER_UNIT_HG2)==(1u),"Analyze: 'IUG_HEATER_UNIT_HG2'");
_Static_assert((IUG_HEATER_UNIT_HG3)==(2u),"Analyze: 'IUG_HEATER_UNIT_HG3'");
_Static_assert((IUG_HEATER_UNIT_HG4)==(3u),"Analyze: 'IUG_HEATER_UNIT_HG4'");
_Static_assert((IUG_HEATER_UNIT_HG5)==(4u),"Analyze: 'IUG_HEATER_UNIT_HG5'");
_Static_assert((IUG_HEATER_UNIT_HG6)==(5u),"Analyze: 'IUG_HEATER_UNIT_HG6'");
_Static_assert((IUG_HEATER_UNIT_HG7)==(6u),"Analyze: 'IUG_HEATER_UNIT_HG7'");
_Static_assert((IUG_HEATER_UNIT_HG8)==(7u),"Analyze: 'IUG_HEATER_UNIT_HG8'");
#endif

#if( 8u == (IUG_HEATER_UNIT_NUMBER_MAX) )
_Static_assert((IUG_HEATER_UNIT_ENUM_MAX)==(8u),"Analyze: 'IUG_HEATER_UNIT_ENUM_MAX'");
_Static_assert((IUG_HEATER_UNIT_NUMBER_MAX)==(IUG_HEATER_UNIT_ENUM_MAX),"Analyze: 'IUG_HEATER_UNIT_NUMBER_MAX'");
#else
#error "Analyze: 'IUG_HEATER_UNIT_NUMBER_MAX'"    
#endif
_Static_assert((IUG_HEATER_UNIT_ENUM_MAX)==(GHIBUS_COM_HGX_ENUM_MAX),"Analyze: 'GHIBUS_COM_HGX_ENUM_MAX'");

_Static_assert((8u)>=(IUG_HEATER_UNIT_NUMBER_MAX)     ,"Analyze: 'IUG_HEATER_UNIT_NUMBER_MAX'");
_Static_assert((8u)>=(IUG_HEATER_UNIT_ENUM_MAX)       ,"Analyze: 'IUG_HEATER_UNIT_ENUM_MAX'");
_Static_assert((2u)==(IUG_HEATER_UNIT_WBUS_NUMBER_MAX),"Analyze: 'IUG_HEATER_UNIT_WBUS_NUMBER_MAX'");


#define IUG_UNT_HEATER_HG_FIRST         (IUG_UNT_HEATER_HG1_DEF)

#if( 8u == (IUG_HEATER_UNIT_NUMBER_MAX) )
    #define IUG_UNT_HEATER_HG_LAST      (IUG_UNT_HEATER_HG8_DEF)
#else
    #error "Analyze: 'IUG_UNT_HEATER_HG_LAST'!"
#endif

_Static_assert((IUG_UNT_HEATER_HG_FIRST)==(IUG_UNT_HEATER_HG1),"Analyze: 'IUG_UNT_HEATER_HG_FIRST'!");
#if( 8u == (IUG_UNT_HEATER_HG_LAST) )
_Static_assert((IUG_UNT_HEATER_HG_LAST) ==(IUG_UNT_HEATER_HG8),"Analyze: 'IUG_UNT_HEATER_HG_LAST'!");
#endif



typedef enum
{
    IUG_UNT_HEATER_TYP_AIR,     
    IUG_UNT_HEATER_TYP_WAT,     
    IUG_UNT_HEATER_TYP_ENUM_MAX,               
    IUG_UNT_HEATER_TYP_ENUM_FORCE_TYPE = 0x7F  
} IUG_UNIT_HEATER_TYPE_ID_t;


_Static_assert((IUG_UNT_HEATER_TYP_AIR)     ==(IUG_UNIT_HEATER_TYPE_ID_t)(IUG_UNIT_API_HEATER_TYPE_AIR),"Analyze: IUG_UNIT_API_HEATER_TYPE_AIR");
_Static_assert((IUG_UNT_HEATER_TYP_WAT)     ==(IUG_UNIT_HEATER_TYPE_ID_t)(IUG_UNIT_API_HEATER_TYPE_WAT),"Analyze: IUG_UNIT_API_HEATER_TYPE_WAT");
_Static_assert((IUG_UNT_HEATER_TYP_ENUM_MAX)==(IUG_UNIT_HEATER_TYPE_ID_t)(IUG_UNIT_API_HEATER_TYPE_ENUM_MAX),"Analyze: IUG_UNIT_API_HEATER_TYPE_ENUM_MAX");


#define IUG_HETRTRMFLG_HISTORY_MAX              5u


#define IUG_UNIT_COMMTX_FLG_NONE                0x00u
#define IUG_UNIT_COMMTX_FLG_GEN_CTL_UNIT        0x01u   
#define IUG_UNIT_COMMTX_FLG_CMD_BTN_UNIT        0x02u   
#define IUG_UNIT_COMMTX_FLG_BLUETOOTH_UNIT      0x04u   
#define IUG_UNIT_COMMTX_FLG_08                  0x08u
#define IUG_UNIT_COMMTX_FLG_10                  0x10u
#define IUG_UNIT_COMMTX_FLG_20                  0x20u
#define IUG_UNIT_COMMTX_FLG_40                  0x40u
#define IUG_UNIT_COMMTX_FLG_80                  0x80u



typedef enum
{
    IUG_UNTSIG_NONE                         = 0x0000u,
    IUG_UNTSIG_FORCE_NEG_ACK                = 0x0001u,      
    IUG_UNTSIG_CMD_REFUSE                   = 0x0002u,      
    IUG_UNTSIG_CMD_DOUBLED                  = 0x0004u,      
    IUG_UNITSIG_0008                        = 0x0008u,
    IUG_UNTSIG_AIR_PRESSURE_RX              = 0x0010u,      
    IUG_UNITSIG_0020                        = 0x0020u,
    IUG_UNTSIG_AFTER_CMD_EVALUATION         = 0x0040u,      
    IUG_UNTSIG_CMD_ACTIVATION               = 0x0080u,      
    IUG_UNTSIG_TRG_BROADCAST_OFF            = 0x0100u,
    IUG_UNTSIG_ENUM_FORCE_TYPE              = 0x7FFF 

} IUG_UNIT_SIGNAL_ID_t;


typedef enum
{
    IUG_HTRSIG_NONE                         = 0x0000u,
    IUG_HTRSIG_LATE_INITIALIZATION_1        = 0x0001u,  
    IUG_HTRSIG_LATE_INITIALIZATION_2        = 0x0002u,  
    IUG_HTRSIG_SYNCHRONIZATION              = 0x0004u,  
    IUG_HTRSIG_INITIAL_OPERATION            = 0x0008u,  
    IUG_HTRSIG_BETRIEBSZUSTAND_EVENT        = 0x0010u,  
    IUG_HTRSIG_CLEAR_FEATING_TIME           = 0x0020u,  
    IUG_HTRSIG_0040                         = 0x0040u,
    IUG_HTRSIG_0080                         = 0x0080u,
    IUG_HTRSIG_ECU_CODING_1_INV             = 0x0100u,  
    IUG_HTRSIG_HEATER_TYPE_INFO_INV         = 0x0200u,  
    IUG_HTRSIG_HEATER_CAPABILITY_INV        = 0x0400u,  
    IUG_HTRSIG_HEATER_MODE_FUNC_INV         = 0x0800u,  
    IUG_HTRSIG_1000                         = 0x1000u,
    IUG_HTRSIG_2000                         = 0x2000u,
    IUG_HTRSIG_4000                         = 0x4000u,
    IUG_HTRSIG_8000                         = 0x8000u,
    IUG_HTRSIG_ENUM_FORCE_TYPE              = 0x7FFF 

} IUG_UNIT_HEATER_SIGNAL_ID_t;


#define IUG_UNIT_HEATER_FAILURE_SUPPRESSION_CNT         (((IUG_HEATER_FAILURE_SUPPRESSION_MS)+(IUG_UNIT_CTL_CYLIC_PERIOD))/(IUG_UNIT_CTL_CYLIC_PERIOD))

#define IUG_UNIT_USER_DEVICE_FAILURE_SUPPRESSION_CNT    (((IUG_HEATER_FAILURE_SUPPRESSION_MS)+(IUG_UNIT_CTL_CYLIC_PERIOD))/(IUG_UNIT_CTL_CYLIC_PERIOD))


#define IUG_UNIT_IUG_KEEP_ALIVE_CNT             (((IUG_UNIT_IUG_KEEP_ALIVE_MS)+(IUG_UNIT_CTL_CYLIC_PERIOD))/(IUG_UNIT_CTL_CYLIC_PERIOD))
#define IUG_UNIT_IUG_SLEEP_CNT                  (((IUG_UNIT_IUG_SLEEP_MS)+(IUG_UNIT_CTL_CYLIC_PERIOD))/(IUG_UNIT_CTL_CYLIC_PERIOD))
#define IUG_UNIT_IUG_SLEEP_CNT_WBUS_DELAY       ((((IUG_UNIT_IUG_SLEEP_MS) - (KEEP_ALIVE_TRG_WBUS_DELAY_MS))+(IUG_UNIT_CTL_CYLIC_PERIOD))/(IUG_UNIT_CTL_CYLIC_PERIOD))


#define IUG_UNIT_READ_STATUS_KEEP_ACTIVE_CNT    (((IUG_UNIT_READ_STATUS_KEEP_ACTIVE_MS)+(IUG_UNIT_CTL_CYLIC_PERIOD))/(IUG_UNIT_CTL_CYLIC_PERIOD))


#define IUG_UNIT_IUG_CMD_OFF_COVERAGE_PERIOD_MS    5000u 
#define IUG_UNIT_IUG_CMD_OFF_BURST_ON_FAULT           5u 
#define IUG_UNIT_IUG_CMD_OFF_BURST_PERIOD_MS       2000u 



typedef enum
{
    CATEGORY_UNDEFINED          =   0u, 
    CATEGORY_OFF                =   1u, 
    CATEGORY_ERR                =   2u, 
    CATEGORY_STARTUP            =   3u, 
    CATEGORY_STARTUP_HI_CURRENT =   4u, 
    CATEGORY_PARTIAL_LOAD       =   5u, 
    CATEGORY_FULL_LOAD          =   6u, 
    CATEGORY_VENTILATING        =   7u, 
    CATEGORY_CTL_PAUSE          =   8u, 
    CATEGORY_BURNOUT_COOLING    =   9u, 
    CATEGORY_IUG_WAKEUP         = 255u, 

} enum_IUG_UNIT_HEATER_STATE_CATEGORY_t;
typedef enum_IUG_UNIT_HEATER_STATE_CATEGORY_t   IUG_UNIT_HEATER_STATE_CATEGORY_t;


#define IUG_UNIT_HEATER_STATE_STFL_OK               0x00u   
#define IUG_UNIT_HEATER_STATE_STFL_FAILURE          0x01u   


#define IUG_UNIT_HEATER_STATE_HGVP_OK               0x00u   
#define IUG_UNIT_HEATER_STATE_HGVP_FAILURE          0x01u   


#define IUG_UNIT_HEATER_STATE_UEHFL_OK              0x00u   
#define IUG_UNIT_HEATER_STATE_UEHFL_FAILURE         0x01u   


#define IUG_UNIT_BLUETOOTH_TIMER_EACH_DAY             3u    
#define IUG_UNIT_BLUETOOTH_TIMER_EACH_WEEK            7u    


#define IUG_UNTGPS_TRG_AIR_PRESSURE_CHANGED         0x01u
#define IUG_UNTGPS_TRG_STARTUP                      0x02u
#define IUG_UNTGPS_TRG_START_HEATING                0x04u


typedef enum
{
    IUG_TASK_ID_IDLE     = 0u, 
    IUG_TASK_ID_FAST     = 1u, 
    IUG_TASK_ID_MEDIUM   = 2u, 
    IUG_TASK_ID_NORMAL   = 3u, 
    IUG_TASK_ID_MODERATE = 4u, 
    IUG_TASK_ID_SLOW     = 5u, 
    IUG_TASK_ID_ENUM_MAX,               
    IUG_TASK_ID_ENUM_FORCE_TYPE = 0x7F  

} enum_IUG_TASK_ID_t;
typedef enum_IUG_TASK_ID_t      IUG_TASK_ID_t;


typedef enum
{
    UNTFLG_NONE                 = 0x0000u,
    UNTFLG_TYP_HTR              = 0x0001u,  
    UNTFLG_TYP_CTL              = 0x0002u,  
    UNTFLG_TYP_GENCTL           = 0x0004u,  
    UNTFLG_0008                 = 0x0008u,
    UNTFLG_TYP_TEMPSEN          = 0x0010u,  
    UNTFLG_0020                 = 0x0020u,
    UNTFLG_0040                 = 0x0040u,
    UNTFLG_TYP_HTRGHI           = 0x0080u,  
    UNTFLG_COOL_PUMP_AVAIL      = 0x0100u,  
    UNTFLG_COOL_PUMP_ACT_AT_AH  = 0x0200u,  
    UNTFLG_SERNUM_5109          = 0x0400u,  
    UNTFLG_AIR_PRESS_CORR_AVAIL = 0x0800u,  
    UNTFLG_SWAP_ID_STATUS_DATA  = 0x1000u,  
    UNTFLG_2000                 = 0x2000u,
    UNTFLG_4000                 = 0x4000u,
    UNTFLG_8000                 = 0x8000u,

    UNTFLG_ENUM_FORCE_TYPE      = 0x7FFF  

} IUG_UNIT_FLAG_t;

_Static_assert((sizeof(uint16_t))==(sizeof(IUG_UNIT_FLAG_t)),"Analyze: IUG_UNIT_FLAG_t");


#define IUG_UNTPTYFLG_NONE                  0x00u
#define IUG_UNTPTYFLG_DETECTED              0x01u   
#define IUG_UNTPTYFLG_02                    0x02u
#define IUG_UNTPTYFLG_04                    0x04u
#define IUG_UNTPTYFLG_08                    0x08u
#define IUG_UNTPTYFLG_10                    0x10u
#define IUG_UNTPTYFLG_20                    0x20u
#define IUG_UNTPTYFLG_40                    0x40u
#define IUG_UNTPTYFLG_80                    0x80u


typedef enum
{
    IUG_TGT_WAIT_STATE              =  0u,
    IUG_TGT_HEATING                 =  1u,
    IUG_TGT_BOOST                   =  2u,
    IUG_TGT_ECO                     =  3u,
    IUG_TGT_AUX_HEATING             =  4u,
    IUG_TGT_AUT_AUX_HEATING         =  5u,
    IUG_TGT_DRIVE_HEATING           =  6u,
    IUG_TGT_VENTILATION             =  7u,
    IUG_TGT_SWITCH_OFF              =  8u,
    IUG_TGT_FAULT                   =  9u,
    IUG_TGT_INITIAL_OPERATION       = 10u,      
    IUG_TGT_INITIAL_WAIT_STATE      = 11u,      
    IUG_TGT_STATE_ENUM_MAX,                 
    IUG_TGT_STATE_ENUM_FORCE_TYPE   = 0x7F    

} IUG_TARGET_STATE_t;

_Static_assert((IUG_TGT_WAIT_STATE)==(0u),"Analyze: IUG_WAIT_STATE_AIR");
_Static_assert((IUG_TGT_FAULT)     ==((IUG_TGT_INITIAL_OPERATION)-1),"Analyze: IUG_TGT_FAULT");


typedef enum
{
    TGT_HTR_WAIT_STATE       = 0u,
    TGT_HTR_HEATING          = 1u,
    TGT_HTR_BOOST            = 2u,
    TGT_HTR_ECO              = 3u,
    TGT_HTR_AUX_HEATING      = 4u,
    TGT_HTR_VENTILATION      = 5u,
    TGT_HTR_SWITCH_OFF       = 6u,
    TGT_HTR_FAULT            = 7u,
    TGT_HTR_STATE_ENUM_MAX   = 8u,       
    TGT_HTR_STATE_ENUM_FORCE_TYPE = 0x7F 

} TARGET_HTR_STATE_t;

_Static_assert((TGT_HTR_WAIT_STATE)     ==(0u),"Analyze: TGT_HTR_WAIT_STATE");
_Static_assert((TGT_HTR_HEATING)        ==(1u),"Analyze: TGT_HTR_HEATING");
_Static_assert((TGT_HTR_BOOST)          ==(2u),"Analyze: TGT_HTR_BOOST");
_Static_assert((TGT_HTR_ECO)            ==(3u),"Analyze: TGT_HTR_ECO");
_Static_assert((TGT_HTR_AUX_HEATING)    ==(4u),"Analyze: TGT_HTR_AUX_HEATING");
_Static_assert((TGT_HTR_VENTILATION)    ==(5u),"Analyze: TGT_HTR_VENTILATION");
_Static_assert((TGT_HTR_SWITCH_OFF)     ==(6u),"Analyze: TGT_HTR_SWITCH_OFF");
_Static_assert((TGT_HTR_FAULT)          ==(7u),"Analyze: TGT_HTR_FAULT");
_Static_assert((TGT_HTR_FAULT)          ==((TGT_HTR_STATE_ENUM_MAX)-1),"Analyze: TGT_HTR_FAULT");


typedef enum
{
    IUG_CMD_NONE = 0u,
    IUG_CMD_OFF,                
    IUG_CMD_OFF_AIR,            
    IUG_CMD_HEATING_REC,        
    IUG_CMD_HEATING_REC_AIR,    
    IUG_CMD_HEATING,            
    IUG_CMD_HEATING_AIR,        
    IUG_CMD_VENTILATION,        
    IUG_CMD_VENTILATION_AIR,    
    IUG_CMD_AUX_HEATING,        
    IUG_CMD_BOOST_AIR,          
    IUG_CMD_ECO_AIR,            
    IUG_CMD_ON_TEMP_MODE,       
    IUG_CMD_BUTTON_PUSHED,      
    IUG_CMD_BUTTON_OFF,         
    IUG_CMD_BUTTON_OFF_AIR,     
    IUG_CMD_BUTTON_ON,          
    IUG_CMD_BUTTON_ON_AIR,      
    IUG_CMD_BLUETOOTH_OFF_HGx,
    IUG_CMD_BLUETOOTH_ON_HGx,     
    IUG_CMD_REFUSE,                 
    IUG_CMD_ENUM_MAX,               
    IUG_CMD_ENUM_FORCE_TYPE = 0x7F  

} enum_IUG_COMMAND_t;

_Static_assert((IUG_CMD_BUTTON_OFF)   ==(14),"Analyze: enum_IUG_COMMAND_t");    
_Static_assert((IUG_CMD_BUTTON_ON)    ==(16),"Analyze: enum_IUG_COMMAND_t");    
_Static_assert((IUG_CMD_BUTTON_ON_AIR)==(17),"Analyze: enum_IUG_COMMAND_t");    


typedef enum
{
    IUG_ERR_NONE = 0u,
    IUG_ERR_CMD,
    IUG_ERR_CMD_INVALID,
    IUG_ERR_STA,
    IUG_ERR_WBUS_INS_INVALID,
    IUG_ERR_ENUM_MAX,               
    IUG_ERR_ENUM_FORCE_TYPE = 0x7F  
} enum_IUG_ERROR_t;


typedef enum
{
    IUG_LEVEL_AIR_1 = 0u,
    IUG_LEVEL_AIR_2 = 1u,
    IUG_LEVEL_AIR_3 = 2u,
    IUG_LEVEL_AIR_4 = 3u,
    IUG_LEVEL_AIR_ENUM_MAX,                 
    IUG_LEVEL_AIR_ENUM_FORCE_TYPE = 0x7F    
} enum_IUG_LEVEL_AIR_t;


typedef enum
{
    IUG_MODE_BTS_AIR_HEATING = 0u,
    IUG_MODE_BTS_AIR_VENTILATION = 1u,
    IUG_MODE_BTS_AIR_BOOST = 2u,
    IUG_MODE_BTS_AIR_ECO = 3u,
    IUG_MODE_BTS_AIR_ENUM_MAX,              
    IUG_MODE_BTS_AIR_ENUM_FORCE_TYPE = 0x7F 
} enum_IUG_MODE_BTS_AIR_t;


typedef enum
{
    IUG_HG_AVAIL_NON        = 0u,   
    IUG_HG_AVAIL_WAT        = 1u,   
    IUG_HG_AVAIL_AIR        = 2u,   
    IUG_HG_AVAIL_TEMP       = 3u,   
    IUG_HG_AVAIL_ENUM_MAX,              
    IUG_HG_AVAIL_ENUM_FORCE_TYPE = 0x7F 

} IUG_HG_AVAILABLE_t;

typedef enum
{
    IUG_HG_ALTITUDE_CORR_NOT_AVAIL        = 0u,     
    IUG_HG_ALTITUDE_CORR_AVAIL            = 1u,     

    IUG_HG_ALTITUDE_CORR_ENUM_MAX,                  
    IUG_HG_ALTITUDE_CORR_ENUM_FORCE_TYPE  = 0x7F    

} IUG_HG_ALTITUDE_CORR_t;

typedef enum
{
    MSKHTR__                = 0x0000u,    

    MSKHTR_1                = 0x0001u,    
    MSKHTR_2                = 0x0002u,    
    MSKHTR_3                = 0x0004u,    
    MSKHTR_4                = 0x0008u,    
    MSKHTR_5                = 0x0010u,    
    MSKHTR_6                = 0x0020u,    
    MSKHTR_7                = 0x0040u,    
    MSKHTR_8                = 0x0080u,    

    MSKHTR_9                = 0x0100u,    
    MSKHTR10                = 0x0200u,    
    MSKHTR11                = 0x0400u,    
    MSKHTR12                = 0x0800u,    
    MSKHTR13                = 0x1000u,    
    MSKHTR14                = 0x2000u,    
    MSKHTR15                = 0x4000u,    
    MSKHTR16                = 0x8000u,    
    MSKHTR__ENUM_FORCE_TYPE = 0x7FFF      

} IUG_HTR_MSK_ID_t;


typedef enum
{
    MSKUDV__                 = 0x00u,    
    MSKUDV_1                 = 0x01u,    
    MSKUDV_2                 = 0x02u,    
    MSKUDV_3                 = 0x04u,    
    MSKUDV_4                 = 0x08u,    
    MSKUDV_5                 = 0x10u,    
    MSKUDV_6                 = 0x20u,    
    MSKUDV_7                 = 0x40u,    
    MSKUDV_8                 = 0x80u,    
    MSKUDV__ENUM_FORCE_TYPE  = 0x7F      

} IUG_USRDEV_MSK_ID_t;


typedef enum
{
    IUG_BILLING_FCN_DISABLED      = 0u,   
    IUG_BILLING_FCN_CHECK         = 1u,   
    IUG_BILLING_FCN_NOT_AVAILABLE = 2u,   
    IUG_BILLING_FCN_AVAILABLE     = 3u,   
    IUG_BILLING_FCN_BOUGHT        = 4u,   
    IUG_BILLING_ENUM_FORCE_TYPE   = 0x7Fu 

} enum_IUG_BILLING_FCN_STATES_t;
_Static_assert( 0u == (IUG_BILLING_FCN_DISABLED     ) ,"Change encoding for webasto parameter too!!!");
_Static_assert( 1u == (IUG_BILLING_FCN_CHECK        ) ,"Change encoding for webasto parameter too!!!");
_Static_assert( 2u == (IUG_BILLING_FCN_NOT_AVAILABLE) ,"Change encoding for webasto parameter too!!!");
_Static_assert( 3u == (IUG_BILLING_FCN_AVAILABLE    ) ,"Change encoding for webasto parameter too!!!");
_Static_assert( 4u == (IUG_BILLING_FCN_BOUGHT       ) ,"Change encoding for webasto parameter too!!!");




#endif 


