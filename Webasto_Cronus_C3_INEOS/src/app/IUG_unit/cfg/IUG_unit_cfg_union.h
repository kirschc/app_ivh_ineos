#ifndef __IUG_UNIT_CFG_UNION_H_
#define __IUG_UNIT_CFG_UNION_H_



#define IUG_UNT_HEATER_CONFIGURATION \
{\
    { \
        { ##IUG_UNT_HEATER_1_CONFIGURATION}, \
    } \
}






typedef struct
{
    IUG_UNIT_HEATER_CONFIGURATION_t         * const htr_wbus_cfg;   

} struct_IUG_UNIT_HEATER_CONFIGURATION_ARRAY_t;
typedef struct_IUG_UNIT_HEATER_CONFIGURATION_ARRAY_t   IUG_UNIT_HEATER_CFG_ARY_t;


typedef union
{
    IUG_UNIT_HEATER_CONTROL_t           htr_wbus_ctl;   
    IUG_UNIT_GHI_HEATER_CONTROL_t       htr_ghi_ctl;    

} IUG_UNIT_HEATER_CTL_UNION_t;


typedef union
{
    void                                *p_htr_ctl;         
    IUG_UNIT_HEATER_CONTROL_t           *p_htr_ctl_wbus;    
    IUG_UNIT_GHI_HEATER_CONTROL_t       *p_htr_ctl_ghi;     

} IUG_UNIT_PTR_HEATER_CTL_UNION_t;



extern const IUG_UNIT_HEATER_CFG_ARY_t      IUG_UNIT_HEATER_CFG_ARY[];

extern IUG_UNIT_PTR_HEATER_CTL_UNION_t      IUG_UNT_HEATER_CTL[(IUG_HEATER_UNIT_ENUM_MAX)];

extern IUG_UNIT_HEATER_CTL_UNION_t          IUG_UNIT_HEATER_1_CTL_UNI;
extern IUG_UNIT_HEATER_CTL_UNION_t          IUG_UNIT_HEATER_2_CTL_UNI;
#if( (IUG_UNT_HEATER_HG_LAST) == (IUG_UNT_HEATER_HG8_DEF) )
extern IUG_UNIT_HEATER_CTL_UNION_t          IUG_UNIT_HEATER_3_CTL_UNI;
extern IUG_UNIT_HEATER_CTL_UNION_t          IUG_UNIT_HEATER_4_CTL_UNI;
extern IUG_UNIT_HEATER_CTL_UNION_t          IUG_UNIT_HEATER_5_CTL_UNI;
extern IUG_UNIT_HEATER_CTL_UNION_t          IUG_UNIT_HEATER_6_CTL_UNI;
extern IUG_UNIT_HEATER_CTL_UNION_t          IUG_UNIT_HEATER_7_CTL_UNI;
extern IUG_UNIT_HEATER_CTL_UNION_t          IUG_UNIT_HEATER_8_CTL_UNI;
#endif


#endif 



