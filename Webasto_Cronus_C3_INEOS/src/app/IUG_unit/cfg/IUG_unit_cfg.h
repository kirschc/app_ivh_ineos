#ifndef _IUG_UNIT_CFG_H_
#define _IUG_UNIT_CFG_H_



#define IUG_UNT_HEADER_PATTERN   (('U' << 24u) + ('H' << 16u) + ('P' << 8u) + ('!' << 0u))


#define IUG_UNT_HEATER_1_READ_ID_DATA_ADDRESS_CFG \
{\
     \
    \
    { (WBUS_MSGSRV_09_GET_SERIAL_NUMBER)    ,0u                           ,(IUG_RDST_SIZE_5)  ,&IUG_UNIT_HEATER_1_CTL_UNI.htr_wbus_ctl.heater_data_raw.SERIAL_NUMBER        },\
    { (WBUS_MSGSRV_0A_ID_WBUS_VERSION)      ,0u                           ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_1_CTL_UNI.htr_wbus_ctl.heater_data_raw.WBUS_VERSION_raw     },\
    { (WBUS_MSGSRV_0C_ID_ECU_CODING)        ,0u                           ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_1_CTL_UNI.htr_wbus_ctl.heater_data_raw.ECU_CODING_1_raw     },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_300B_ID_GET_SERIAL_NUMBER)        ,(IUG_RDST_SIZE_5)  ,&IUG_UNIT_HEATER_1_CTL_UNI.htr_wbus_ctl.heater_data_raw.SERIAL_NUMBER        },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_300D_ID_WBUS_VERSION)             ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_1_CTL_UNI.htr_wbus_ctl.heater_data_raw.WBUS_VERSION_raw     },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_300E_ID_GET_HEATER_SIGN)          ,(IUG_RDST_SIZE_U8) ,&IUG_HEATER_PROPERTY_NVM[(IUG_HEATER_UNIT_HG1)].unit_htr_sign                },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_3012_ID_GET_MAX_HEATING_TIME)     ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_1_CTL_UNI.htr_wbus_ctl.heater_data_raw.MAX_HEATING_TIME_raw },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_301D_ID_GET_HEATER_TYPE)          ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_1_CTL_UNI.htr_wbus_ctl.heater_data_raw.HEATER_TYPE_INFO_raw },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_301F_ID_GET_HEATER_NAME)          ,(IUG_RDST_SIZE_24)  ,(NULL) },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_3020_ID_GET_HEATER_CAPABLTY)      ,(IUG_RDST_SIZE_U16),&IUG_UNIT_HEATER_1_CTL_UNI.htr_wbus_ctl.heater_data_raw.HEATER_CAPABILITY_raw},\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_3022_ID_GET_HEATER_MODE_FUNC)     ,(IUG_RDST_SIZE_U16),&IUG_UNIT_HEATER_1_CTL_UNI.htr_wbus_ctl.heater_data_raw.HEATER_MODE_FUNC_raw } \
}


#define IUG_UNT_HEATER_2_READ_ID_DATA_ADDRESS_CFG \
{\
     \
    \
    { (WBUS_MSGSRV_09_GET_SERIAL_NUMBER)    ,0u                           ,(IUG_RDST_SIZE_5)  ,&IUG_UNIT_HEATER_2_CTL_UNI.htr_wbus_ctl.heater_data_raw.SERIAL_NUMBER        },\
    { (WBUS_MSGSRV_0A_ID_WBUS_VERSION)      ,0u                           ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_2_CTL_UNI.htr_wbus_ctl.heater_data_raw.WBUS_VERSION_raw     },\
    { (WBUS_MSGSRV_0C_ID_ECU_CODING)        ,0u                           ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_2_CTL_UNI.htr_wbus_ctl.heater_data_raw.ECU_CODING_1_raw     },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_300B_ID_GET_SERIAL_NUMBER)        ,(IUG_RDST_SIZE_5)  ,&IUG_UNIT_HEATER_2_CTL_UNI.htr_wbus_ctl.heater_data_raw.SERIAL_NUMBER        },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_300D_ID_WBUS_VERSION)             ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_2_CTL_UNI.htr_wbus_ctl.heater_data_raw.WBUS_VERSION_raw     },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_300E_ID_GET_HEATER_SIGN)          ,(IUG_RDST_SIZE_U8) ,&IUG_HEATER_PROPERTY_NVM[(IUG_HEATER_UNIT_HG2)].unit_htr_sign                },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_3012_ID_GET_MAX_HEATING_TIME)     ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_2_CTL_UNI.htr_wbus_ctl.heater_data_raw.MAX_HEATING_TIME_raw },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_301D_ID_GET_HEATER_TYPE)          ,(IUG_RDST_SIZE_U8) ,&IUG_UNIT_HEATER_2_CTL_UNI.htr_wbus_ctl.heater_data_raw.HEATER_TYPE_INFO_raw },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_301F_ID_GET_HEATER_NAME)          ,(IUG_RDST_SIZE_24)  ,(NULL) },\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_3020_ID_GET_HEATER_CAPABLTY)      ,(IUG_RDST_SIZE_U16),&IUG_UNIT_HEATER_2_CTL_UNI.htr_wbus_ctl.heater_data_raw.HEATER_CAPABILITY_raw},\
    { (WBUS_MSGSRV_30_ID_GET_STATUS)        ,(WBUS_MSGSRV_3022_ID_GET_HEATER_MODE_FUNC)     ,(IUG_RDST_SIZE_U16),&IUG_UNIT_HEATER_2_CTL_UNI.htr_wbus_ctl.heater_data_raw.HEATER_MODE_FUNC_raw } \
}


#define IUG_UNT_TEMP_SENSOR_INT_READ_ID_DATA_ADDRESS_CFG \
{\
     \
    \
    { (WBUS_MSGSRV_0A_ID_WBUS_VERSION)      ,0u                               ,(IUG_RDST_SIZE_U8) ,(NULL)    },\
}


#define IUG_UNT_TEMP_SENSOR_EXT_READ_ID_DATA_ADDRESS_CFG \
{\
     \
    \
    { (WBUS_MSGSRV_0A_ID_WBUS_VERSION)      ,0u                               ,(IUG_RDST_SIZE_U8) ,(NULL)    },\
}


#define IUG_UNT_TEMP_SENSOR_T_LIN_R_INT_READ_ID_DATA_ADDRESS_CFG \
{\
     \
    \
    { (WBUS_MSGSRV_0A_ID_WBUS_VERSION)      ,0u                               ,(IUG_RDST_SIZE_U8) ,(NULL)    },\
}


#define IUG_UNT_TEMP_SENSOR_T_LIN_R_EXT_READ_ID_DATA_ADDRESS_CFG \
{\
     \
    \
    { (WBUS_MSGSRV_0A_ID_WBUS_VERSION)      ,0u                               ,(IUG_RDST_SIZE_U8) ,(NULL)    },\
}


#define IUG_UNT_HEATER_1_READ_STATUS_DATA_ADDRESS_CFG \
{\
     \
    \
    { (IUG_RDST_ID_0A_STATUS_OUTPORT_HEATER)        ,(&IUG_UNIT_HEATER_1_CTL_UNI.htr_wbus_ctl.heater_data_raw.STATUS_OUTPORT_raw) },\
    { (IUG_RDST_ID_0C_STATUS_COOL_MEDIUM_TEMP1)     ,(&IUG_UNIT_HEATER_1_CTL_UNI.htr_wbus_ctl.heater_data_raw.COOLANT_TEMP_1_raw) },\
    { (IUG_RDST_ID_0E_STATUS_SUPPLY_VOLTAGE)        ,(&IUG_UNIT_HEATER_1_CTL_UNI.htr_wbus_ctl.heater_data_raw.SUPPLY_VOLTAGE_raw) },\
    { (IUG_RDST_ID_28_STATUS_STATE_NUM_1BYTE)       ,(&IUG_UNIT_HEATER_1_CTL_UNI.htr_wbus_ctl.heater_data_raw.HG_STATE_NUM_2_raw) },\
    { (IUG_RDST_ID_29_STATUS_STATE_NUM_2BYTE)       ,(&IUG_UNIT_HEATER_1_CTL_UNI.htr_wbus_ctl.heater_data_raw.HG_STATE_NUM_2_raw) },\
    { (IUG_RDST_ID_2A_STATUS_STATE_STFL_FAULTY)     ,(&IUG_UNIT_HEATER_1_CTL_UNI.htr_wbus_ctl.heater_data_raw.HG_STATE_STFL_raw)  },\
    { (IUG_RDST_ID_2B_STATUS_STATE_UEHFL_OVERHEAT)  ,(&IUG_UNIT_HEATER_1_CTL_UNI.htr_wbus_ctl.heater_data_raw.HG_STATE_UEHFL_raw) },\
    { (IUG_RDST_ID_2C_STATUS_STATE_HGV_LOCKED)      ,(&IUG_UNIT_HEATER_1_CTL_UNI.htr_wbus_ctl.heater_data_raw.HG_STATE_HGVP_raw)  },\
    { (IUG_RDST_ID_3A_STATUS_AIR_PRESSURE)          ,(&IUG_UNIT_HEATER_1_CTL_UNI.htr_wbus_ctl.heater_data_raw.AIR_PRESSURE_raw)   } \
}


#define IUG_UNT_HEATER_2_READ_STATUS_DATA_ADDRESS_CFG \
{\
     \
    \
    { (IUG_RDST_ID_0A_STATUS_OUTPORT_HEATER)        ,(&IUG_UNIT_HEATER_2_CTL_UNI.htr_wbus_ctl.heater_data_raw.STATUS_OUTPORT_raw) },\
    { (IUG_RDST_ID_0C_STATUS_COOL_MEDIUM_TEMP1)     ,(&IUG_UNIT_HEATER_2_CTL_UNI.htr_wbus_ctl.heater_data_raw.COOLANT_TEMP_1_raw) },\
    { (IUG_RDST_ID_0E_STATUS_SUPPLY_VOLTAGE)        ,(&IUG_UNIT_HEATER_2_CTL_UNI.htr_wbus_ctl.heater_data_raw.SUPPLY_VOLTAGE_raw) },\
    { (IUG_RDST_ID_28_STATUS_STATE_NUM_1BYTE)       ,(&IUG_UNIT_HEATER_2_CTL_UNI.htr_wbus_ctl.heater_data_raw.HG_STATE_NUM_2_raw) },\
    { (IUG_RDST_ID_29_STATUS_STATE_NUM_2BYTE)       ,(&IUG_UNIT_HEATER_2_CTL_UNI.htr_wbus_ctl.heater_data_raw.HG_STATE_NUM_2_raw) },\
    { (IUG_RDST_ID_2A_STATUS_STATE_STFL_FAULTY)     ,(&IUG_UNIT_HEATER_2_CTL_UNI.htr_wbus_ctl.heater_data_raw.HG_STATE_STFL_raw)  },\
    { (IUG_RDST_ID_2B_STATUS_STATE_UEHFL_OVERHEAT)  ,(&IUG_UNIT_HEATER_2_CTL_UNI.htr_wbus_ctl.heater_data_raw.HG_STATE_UEHFL_raw) },\
    { (IUG_RDST_ID_2C_STATUS_STATE_HGV_LOCKED)      ,(&IUG_UNIT_HEATER_2_CTL_UNI.htr_wbus_ctl.heater_data_raw.HG_STATE_HGVP_raw)  },\
    { (IUG_RDST_ID_3A_STATUS_AIR_PRESSURE)          ,(&IUG_UNIT_HEATER_2_CTL_UNI.htr_wbus_ctl.heater_data_raw.AIR_PRESSURE_raw)   } \
}


#define IUG_UNT_IUG_CONFIGURATION \
{\
    { \
     \
    (IUG_UNT_HEADER_PATTERN),   \
    (IUG_UNT_IUG),               \
    (UNTFLG_NONE),              \
    (WBUS_MBR_IUG),              \
    (IUG_TASK_ID_NORMAL)         \
    } \
}


#define IUG_UNT_HEATER_1_CONFIGURATION \
{\
    { \
     \
    (IUG_UNT_HEADER_PATTERN),   \
    (IUG_UNT_HEATER_HG1),        \
    (UNTFLG_TYP_HTR),           \
    (WBUS_MBR_ENUM_MAX),         \
    (IUG_TASK_ID_FAST)           \
    } \
}


#define IUG_UNT_HEATER_2_CONFIGURATION \
{\
    { \
     \
    (IUG_UNT_HEADER_PATTERN),       \
    (IUG_UNT_HEATER_HG2),            \
    (UNTFLG_TYP_HTR),               \
    (WBUS_MBR_ENUM_MAX),             \
    (IUG_TASK_ID_FAST)               \
    } \
}


#define IUG_UNT_HEATER_3_CONFIGURATION \
{\
    { \
     \
    (IUG_UNT_HEADER_PATTERN),       \
    (IUG_UNT_HEATER_HG3),            \
    (UNTFLG_TYP_HTR),               \
    (WBUS_MBR_ENUM_MAX),             \
    (IUG_TASK_ID_FAST)               \
    } \
}


#define IUG_UNT_HEATER_4_CONFIGURATION \
{\
    { \
     \
    (IUG_UNT_HEADER_PATTERN),       \
    (IUG_UNT_HEATER_HG4),            \
    (UNTFLG_TYP_HTR),               \
    (WBUS_MBR_ENUM_MAX),             \
    (IUG_TASK_ID_FAST)               \
    } \
}


#define IUG_UNT_HEATER_5_CONFIGURATION \
{\
    { \
     \
    (IUG_UNT_HEADER_PATTERN),       \
    (IUG_UNT_HEATER_HG5),            \
    (UNTFLG_TYP_HTR),               \
    (WBUS_MBR_ENUM_MAX),             \
    (IUG_TASK_ID_FAST)               \
    } \
}


#define IUG_UNT_HEATER_6_CONFIGURATION \
{\
    { \
     \
    (IUG_UNT_HEADER_PATTERN),       \
    (IUG_UNT_HEATER_HG6),            \
    (UNTFLG_TYP_HTR),               \
    (WBUS_MBR_ENUM_MAX),             \
    (IUG_TASK_ID_FAST)               \
    } \
}


#define IUG_UNT_HEATER_7_CONFIGURATION \
{\
    { \
     \
    (IUG_UNT_HEADER_PATTERN),       \
    (IUG_UNT_HEATER_HG7),            \
    (UNTFLG_TYP_HTR),               \
    (WBUS_MBR_ENUM_MAX),             \
    (IUG_TASK_ID_FAST)               \
    } \
}


#define IUG_UNT_HEATER_8_CONFIGURATION \
{\
    { \
     \
    (IUG_UNT_HEADER_PATTERN),       \
    (IUG_UNT_HEATER_HG8),            \
    (UNTFLG_TYP_HTR),               \
    (WBUS_MBR_ENUM_MAX),             \
    (IUG_TASK_ID_FAST)               \
    } \
}


#define IUG_UNT_MULTI_CTL_3TO4_CONFIGURATION \
{\
    { \
     \
    (IUG_UNT_HEADER_PATTERN),               \
    (IUG_UNT_MULTI_CTL_3TO4),                \
    (IUG_UNIT_FLAG_t)((UNTFLG_TYP_CTL)|     \
                      (UNTFLG_TYP_GENCTL)), \
    (WBUS_MBR_CTL_MULTI),                    \
    (IUG_TASK_ID_FAST)                       \
    } \
}


#define IUG_UNT_MULTI_CTL_3TO5_CONFIGURATION \
{\
    { \
     \
    (IUG_UNT_HEADER_PATTERN),               \
    (IUG_UNT_MULTI_CTL_3TO5),                \
    (IUG_UNIT_FLAG_t)((UNTFLG_TYP_CTL)|     \
                      (UNTFLG_TYP_GENCTL)), \
    (WBUS_MBR_CTL_MULTI),                    \
    (IUG_TASK_ID_FAST)                       \
    } \
}


#define IUG_UNT_TELESTART_CONFIGURATION \
{\
    { \
     \
    (IUG_UNT_HEADER_PATTERN),               \
    (IUG_UNT_TELE_START),                    \
    (IUG_UNIT_FLAG_t)((UNTFLG_TYP_CTL)|     \
                      (UNTFLG_TYP_GENCTL)), \
    (WBUS_MBR_REMOTE_CTL_TELESTART),         \
    (IUG_TASK_ID_FAST)                       \
    } \
}


#define IUG_UNT_THERMO_CALL_CONFIGURATION \
{\
    { \
     \
    (IUG_UNT_HEADER_PATTERN),               \
    (IUG_UNT_THERMO_CALL),                   \
    (IUG_UNIT_FLAG_t)((UNTFLG_TYP_CTL)|     \
                      (UNTFLG_TYP_GENCTL)), \
    (WBUS_MBR_THERMO_CALL),                  \
    (IUG_TASK_ID_FAST)                       \
    } \
}


#define IUG_UNT_THERMO_CONNECT_CONFIGURATION \
{\
    { \
     \
    (IUG_UNT_HEADER_PATTERN),               \
    (IUG_UNT_THERMO_CONNECT),                \
    (IUG_UNIT_FLAG_t)((UNTFLG_TYP_CTL)|     \
                      (UNTFLG_TYP_GENCTL)), \
    (WBUS_MBR_THERMO_CONNECT),               \
    (IUG_TASK_ID_FAST)                       \
    } \
}


#define IUG_UNT_BLUETOOTH_CTL_AIR_CONFIGURATION \
{\
    { \
     \
    (IUG_UNT_HEADER_PATTERN),       \
    (IUG_UNT_BLUETOOTH_CTL_AIR),     \
    (UNTFLG_TYP_CTL),               \
    (WBUS_MBR_ENUM_MAX),             \
    (IUG_TASK_ID_NORMAL)             \
    } \
}


#define IUG_UNT_BLUETOOTH_CTL_WAT_CONFIGURATION \
{\
    { \
     \
    (IUG_UNT_HEADER_PATTERN),       \
    (IUG_UNT_BLUETOOTH_CTL_WAT),     \
    (UNTFLG_TYP_CTL),               \
    (WBUS_MBR_ENUM_MAX),             \
    (IUG_TASK_ID_NORMAL)             \
    } \
}


#define IUG_UNT_TEMP_SENSOR_INT_CONFIGURATION \
{\
    { \
     \
    (IUG_UNT_HEADER_PATTERN),       \
    (IUG_UNT_TEMP_SENSOR_INT),       \
    (UNTFLG_TYP_TEMPSEN),           \
    (WBUS_MBR_TEMP_SENSOR),          \
    (IUG_TASK_ID_NORMAL)             \
    } \
}


#define IUG_UNT_TEMP_SENSOR_EXT_CONFIGURATION \
{\
    { \
     \
    (IUG_UNT_HEADER_PATTERN),       \
    (IUG_UNT_TEMP_SENSOR_EXT),       \
    (UNTFLG_TYP_TEMPSEN),           \
    (WBUS_MBR_TEMP_SENSOR),          \
    (IUG_TASK_ID_NORMAL)             \
    } \
}


#define IUG_UNT_TEMP_SENSOR_T_LIN_R_INT_CONFIGURATION \
{\
    { \
     \
    (IUG_UNT_HEADER_PATTERN),               \
    (IUG_UNT_TEMP_SENSOR_T_LIN_R_INT),       \
    (UNTFLG_TYP_TEMPSEN),                   \
    (WBUS_MBR_TEMP_SENSOR_T_LIN_R),          \
    (IUG_TASK_ID_NORMAL)                     \
    } \
}


#define IUG_UNT_TEMP_SENSOR_T_LIN_R_EXT_CONFIGURATION \
{\
    { \
     \
    (IUG_UNT_HEADER_PATTERN),               \
    (IUG_UNT_TEMP_SENSOR_T_LIN_R_EXT),       \
    (UNTFLG_TYP_TEMPSEN),                   \
    (WBUS_MBR_TEMP_SENSOR_T_LIN_R),          \
    (IUG_TASK_ID_NORMAL)                     \
    } \
}


#define IUG_UNT_CMD_BUTTON_CONFIGURATION \
{\
    { \
     \
    (IUG_UNT_HEADER_PATTERN),   \
    (IUG_UNT_CMD_BUTTON),        \
    (UNTFLG_TYP_CTL),           \
    (WBUS_MBR_ENUM_MAX),         \
    (IUG_TASK_ID_NORMAL)         \
    } \
}


#define IUG_UNT_SIMULATION_CONFIGURATION \
{\
    { \
     \
    (IUG_UNT_HEADER_PATTERN),   \
    (IUG_UNT_SIMULATION),        \
    (UNTFLG_TYP_CTL),           \
    (WBUS_MBR_ENUM_MAX),         \
    (IUG_TASK_ID_NORMAL)         \
    } \
}


#define IUG_UNT_USER_API_CONFIGURATION \
{\
    { \
     \
    (IUG_UNT_HEADER_PATTERN),   \
    (IUG_UNT_USER_API),          \
    (UNTFLG_NONE),              \
    (WBUS_MBR_ENUM_MAX),         \
    (IUG_TASK_ID_NORMAL)         \
    } \
}


#define IUG_UNT_USER_DEVICE_CONFIGURATION \
{\
    { \
     \
    (IUG_UNT_HEADER_PATTERN),   \
    (IUG_UNT_USER_DEVICE),       \
    (UNTFLG_NONE),              \
    (WBUS_MBR_ENUM_MAX),         \
    (IUG_TASK_ID_NORMAL)         \
    } \
}




#if( 8u == (IUG_HEATER_UNIT_NUMBER_MAX) )
#define IUG_UNIT_COMMTX_CFG_HTR_BOUND_IUG \
     \
    { (IUG_HTR_MSK_ID_t)((MSKHTR_1)|    \
                         (MSKHTR_2)|    \
                         (MSKHTR_3)|    \
                         (MSKHTR_4)|    \
                         (MSKHTR_5)|    \
                         (MSKHTR_6)|    \
                         (MSKHTR_7)|    \
                         (MSKHTR_8) )   \
    }
#else
    #error "Exactly a number of 'IUG_HEATER_UNIT_NUMBER_MAX' elements must be initialized!"
#endif


#if( 8u == (IUG_HEATER_UNIT_NUMBER_MAX) )
#define IUG_UNIT_COMMTX_CFG_HTR_BOUND_USR_API \
     \
    { (IUG_HTR_MSK_ID_t)((MSKHTR_1)|    \
                         (MSKHTR_2)|    \
                         (MSKHTR_3)|    \
                         (MSKHTR_4)|    \
                         (MSKHTR_5)|    \
                         (MSKHTR_6)|    \
                         (MSKHTR_7)|    \
                         (MSKHTR_8) )   \
    }
#else
    #error "Exactly a number of 'IUG_HEATER_UNIT_NUMBER_MAX' elements must be initialized!"
#endif


#ifdef IUG_UNT_SIMULATION_EN
    #define IUG_UNIT_COMMTX_CFG_CMD_SIMULATION \
    { \
         \
       (IUG_UNT_SIMULATION),                     \
       (WBUS_MBR_ENUM_MAX),                     \
       (IUG_UNIT_COMMTX_FLG_NONE),              \
       \
       (MSKHTR__),                              \
       (MSKUDV__)                               \
    },
#else
    #define IUG_UNIT_COMMTX_CFG_CMD_SIMULATION
#endif


#ifdef IUG_UNT_SIMULATION_EN
#define IUG_UNIT_COMMTX_ENTRIES         10  
#else
#define IUG_UNIT_COMMTX_ENTRIES         9   
#endif

#define IUG_UNIT_COMMTX_NVM_DEF_CFG \
{ \
    \
    { \
        \
       (IUG_UNT_MULTI_CTL_3TO4),                 \
       (WBUS_MBR_CTL_MULTI),                    \
       (IUG_UNIT_COMMTX_FLG_GEN_CTL_UNIT),      \
       \
       (MSKHTR__),                              \
       (MSKUDV__)                               \
    }, \
    \
    { \
        \
       (IUG_UNT_TELE_START),                     \
       (WBUS_MBR_REMOTE_CTL_TELESTART),         \
       (IUG_UNIT_COMMTX_FLG_GEN_CTL_UNIT),      \
       \
       (MSKHTR__),                              \
       (MSKUDV__)                               \
    }, \
    \
    { \
        \
       (IUG_UNT_THERMO_CONNECT),                 \
       (WBUS_MBR_THERMO_CONNECT),               \
       (IUG_UNIT_COMMTX_FLG_GEN_CTL_UNIT),      \
       \
       (MSKHTR__),                              \
       (MSKUDV__)                               \
    }, \
    \
    { \
        \
       (IUG_UNT_THERMO_CALL),                    \
       (WBUS_MBR_THERMO_CALL),                  \
       (IUG_UNIT_COMMTX_FLG_GEN_CTL_UNIT),      \
       \
       (MSKHTR__),                              \
       (MSKUDV__)                               \
    }, \
    \
    \
    \
     \
    { \
        \
       (IUG_UNT_BLUETOOTH_CTL_AIR),              \
       (WBUS_MBR_ENUM_MAX),                     \
       (IUG_UNIT_COMMTX_FLG_BLUETOOTH_UNIT),    \
       \
       (MSKHTR__),                              \
       (MSKUDV__)                               \
    }, \
    \
    { \
        \
       (IUG_UNT_BLUETOOTH_CTL_WAT),              \
       (WBUS_MBR_ENUM_MAX),                     \
       (IUG_UNIT_COMMTX_FLG_BLUETOOTH_UNIT),    \
       \
       (MSKHTR__),                              \
       (MSKUDV__)                               \
    }, \
    \
    { \
        \
       (IUG_UNT_CMD_BUTTON),                     \
       (WBUS_MBR_ENUM_MAX),                     \
       (IUG_UNIT_COMMTX_FLG_CMD_BTN_UNIT),      \
       \
       (MSKHTR__),                              \
       (MSKUDV__)                               \
    }, \
    \
    { \
        \
       (IUG_UNT_IUG),                            \
       (WBUS_MBR_IUG),                          \
       (IUG_UNIT_COMMTX_FLG_NONE),              \
       ##IUG_UNIT_COMMTX_CFG_HTR_BOUND_IUG,     \
       (MSKUDV__)                               \
    }, \
    \
    { \
        \
       (IUG_UNT_USER_API),                       \
       (WBUS_MBR_ENUM_MAX),                     \
       (IUG_UNIT_COMMTX_FLG_NONE),              \
       \
       ##IUG_UNIT_COMMTX_CFG_HTR_BOUND_USR_API, \
       (MSKUDV__)                               \
    }, \
    \
     \
    ##IUG_UNIT_COMMTX_CFG_CMD_SIMULATION \
}


#if( (IUG_UNT_HEATER_HG_LAST) == (IUG_UNT_HEATER_HG8_DEF) )
    #if( (8u == (IUG_HEATER_UNIT_NUMBER_MAX)) )
        #define IUG_UNIT_PROPERTY_NVM_DEF_HGn \
        { \
             \
            (IUG_UNT_HEATER_HG3),            \
            (IUG_UNTPTYFLG_NONE),            \
            (0x00u),                         \
            (0x00u),                         \
        },\
        { \
             \
            (IUG_UNT_HEATER_HG4),            \
            (IUG_UNTPTYFLG_NONE),            \
            (0x00u),                         \
            (0x00u),                         \
        },\
        { \
             \
            (IUG_UNT_HEATER_HG5),            \
            (IUG_UNTPTYFLG_NONE),            \
            (0x00u),                         \
            (0x00u),                         \
        },\
        { \
             \
            (IUG_UNT_HEATER_HG6),            \
            (IUG_UNTPTYFLG_NONE),            \
            (0x00u),                         \
            (0x00u),                         \
        },\
        { \
             \
            (IUG_UNT_HEATER_HG7),            \
            (IUG_UNTPTYFLG_NONE),            \
            (0x00u),                         \
            (0x00u),                         \
        },\
        { \
             \
            (IUG_UNT_HEATER_HG8),            \
            (IUG_UNTPTYFLG_NONE),            \
            (0x00u),                         \
            (0x00u),                         \
        },
    #else
        #error "Define a certain configuration for 'IUG_UNIT_PROPERTY_NVM_DEF_HGn'!"
    #endif
#else
    #define IUG_UNIT_PROPERTY_NVM_DEF_HGn
#endif


#ifdef IUG_UNT_SIMULATION_EN
    #define IUG_UNIT_PROPERTY_NVM_DEF_SIMULATION \
    { \
         \
        (IUG_UNT_SIMULATION),            \
        (IUG_UNTPTYFLG_NONE),            \
        (0x00u),                         \
        (0x00u),                         \
    },
#else
    #define IUG_UNIT_PROPERTY_NVM_DEF_SIMULATION
#endif


#define IUG_UNIT_PROPERTY_NVM_DEF_CFG \
{\
     \
    \
    { \
    (IUG_UNT_IUG),                   \
    (IUG_UNTPTYFLG_NONE),            \
    (0x00u),                         \
    (0x00u),                         \
    }, \
    \
    { \
    (IUG_UNT_HEATER_HG1),            \
    (IUG_UNTPTYFLG_NONE),            \
    (0x00u),                         \
    (0x00u),                         \
    }, \
    \
    { \
    (IUG_UNT_HEATER_HG2),            \
    (IUG_UNTPTYFLG_NONE),            \
    (0x00u),                         \
    (0x00u),                         \
    }, \
    \
    ##IUG_UNIT_PROPERTY_NVM_DEF_HGn  \
    \
    { \
    (IUG_UNT_MULTI_CTL_3TO4),        \
    (IUG_UNTPTYFLG_NONE),            \
    (0x00u),                         \
    (0x00u),                         \
    }, \
    \
    { \
    (IUG_UNT_THERMO_CONNECT),        \
    (IUG_UNTPTYFLG_NONE),            \
    (0x00u),                         \
    (0x00u),                         \
    }, \
    \
    { \
    (IUG_UNT_THERMO_CALL),           \
    (IUG_UNTPTYFLG_NONE),            \
    (0x00u),                         \
    (0x00u),                         \
    }, \
    \
    { \
    (IUG_UNT_TELE_START),            \
    (IUG_UNTPTYFLG_NONE),            \
    (0x00u),                         \
    (0x00u),                         \
    }, \
    \
    { \
    (IUG_UNT_BLUETOOTH_CTL_AIR),     \
    (IUG_UNTPTYFLG_NONE),            \
    (0x00u),                         \
    (0x00u),                         \
    }, \
    \
    { \
    (IUG_UNT_BLUETOOTH_CTL_WAT),     \
    (IUG_UNTPTYFLG_NONE),            \
    (0x00u),                         \
    (0x00u),                         \
    }, \
    \
    { \
    (IUG_UNT_TEMP_SENSOR_INT),       \
    (IUG_UNTPTYFLG_NONE),            \
    (0x00u),                         \
    (0x00u),                         \
    }, \
    \
    { \
    (IUG_UNT_TEMP_SENSOR_EXT),       \
    (IUG_UNTPTYFLG_NONE),            \
    (0x00u),                         \
    (0x00u),                         \
    }, \
    \
    { \
    (IUG_UNT_TEMP_SENSOR_T_LIN_R_INT),   \
    (IUG_UNTPTYFLG_NONE),                \
    (0x00u),                             \
    (0x00u),                             \
    }, \
    \
    { \
    (IUG_UNT_TEMP_SENSOR_T_LIN_R_EXT),   \
    (IUG_UNTPTYFLG_NONE),                \
    (0x00u),                             \
    (0x00u),                             \
    }, \
    \
    { \
    (IUG_UNT_CMD_BUTTON),            \
    (IUG_UNTPTYFLG_NONE),            \
    (0x00u),                         \
    (0x00u),                         \
    }, \
    \
    ##IUG_UNIT_PROPERTY_NVM_DEF_SIMULATION  \
    \
    { \
    (IUG_UNT_USER_API),              \
    (IUG_UNTPTYFLG_NONE),            \
    (0x00u),                         \
    (0x00u),                         \
    }, \
    \
    { \
    (IUG_UNT_USER_DEVICE),           \
    (IUG_UNTPTYFLG_NONE),            \
    (0x00u),                         \
    (0x00u),                         \
    } \
    \
} 


#define IUG_HEATER_PROPERTY_STATUS_LIST_AVAILABLE_DEF \
            ( \
              (IUG_UNIT_READ_STATUS_LIST_HEATER_HIGH)  |\
              (IUG_UNIT_READ_STATUS_LIST_HEATER_MEDIUM)|\
              (IUG_UNIT_READ_STATUS_LIST_HEATER_LOW)    \
            )


#if( (IUG_UNT_HEATER_HG_LAST) == (IUG_UNT_HEATER_HG8_DEF) )
    #define IUG_HEATER_PROPERTY_NVM_DEF_HGn \
    ,{ \
         \
        (IUG_UNT_ENUM_MAX),              \
        (UNTFLG_NONE),                   \
        (WBUS_MBR_NULL),                 \
        (IUG_COMDEV_ENUM_MAX),           \
        (IUG_HTR_SIGN_ENUM_MAX),         \
        (HTRCAP_NONE),                   \
        (0x00u),                         \
        (0x00u),                         \
        (0x00u),                        \
        {0u,0u,0u,0u,0u}                \
    },\
    { \
         \
        (IUG_UNT_ENUM_MAX),              \
        (UNTFLG_NONE),                   \
        (WBUS_MBR_NULL),                 \
        (IUG_COMDEV_ENUM_MAX),           \
        (IUG_HTR_SIGN_ENUM_MAX),         \
        (HTRCAP_NONE),                   \
        (0x00u),                         \
        (0x00u),                         \
        (0x00u),                        \
        {0u,0u,0u,0u,0u}                \
    },\
    { \
         \
        (IUG_UNT_ENUM_MAX),              \
        (UNTFLG_NONE),                   \
        (WBUS_MBR_NULL),                 \
        (IUG_COMDEV_ENUM_MAX),           \
        (IUG_HTR_SIGN_ENUM_MAX),         \
        (HTRCAP_NONE),                   \
        (0x00u),                         \
        (0x00u),                         \
        (0x00u),                        \
        {0u,0u,0u,0u,0u}                \
    },\
    { \
         \
        (IUG_UNT_ENUM_MAX),              \
        (UNTFLG_NONE),                   \
        (WBUS_MBR_NULL),                 \
        (IUG_COMDEV_ENUM_MAX),           \
        (IUG_HTR_SIGN_ENUM_MAX),         \
        (HTRCAP_NONE),                   \
        (0x00u),                         \
        (0x00u),                         \
        (0x00u),                        \
        {0u,0u,0u,0u,0u}                \
    },\
    { \
         \
        (IUG_UNT_ENUM_MAX),              \
        (UNTFLG_NONE),                   \
        (WBUS_MBR_NULL),                 \
        (IUG_COMDEV_ENUM_MAX),           \
        (IUG_HTR_SIGN_ENUM_MAX),         \
        (HTRCAP_NONE),                   \
        (0x00u),                         \
        (0x00u),                         \
        (0x00u),                        \
        {0u,0u,0u,0u,0u}                \
    },\
    { \
         \
        (IUG_UNT_ENUM_MAX),              \
        (UNTFLG_NONE),                   \
        (WBUS_MBR_NULL),                 \
        (IUG_COMDEV_ENUM_MAX),           \
        (IUG_HTR_SIGN_ENUM_MAX),         \
        (HTRCAP_NONE),                   \
        (0x00u),                         \
        (0x00u),                         \
        (0x00u),                        \
        {0u,0u,0u,0u,0u}                \
    }
#else
    #define IUG_HEATER_PROPERTY_NVM_DEF_HGn
#endif


#define IUG_HEATER_PROPERTY_NVM_DEF_CFG \
{\
     \
    \
    { \
     \
    (IUG_UNT_ENUM_MAX),              \
    (UNTFLG_NONE),                   \
    (WBUS_MBR_ENUM_MAX),             \
    (IUG_COMDEV_ENUM_MAX),           \
    (IUG_HTR_SIGN_ENUM_MAX),         \
    (HTRCAP_NONE),                   \
    (0x00u),                         \
    (0x00u),                         \
    (0x00u),                        \
    {0u,0u,0u,0u,0u}                \
    }, \
    \
    { \
     \
    (IUG_UNT_ENUM_MAX),              \
    (UNTFLG_NONE),                   \
    (WBUS_MBR_ENUM_MAX),             \
    (IUG_COMDEV_ENUM_MAX),           \
    (IUG_HTR_SIGN_ENUM_MAX),         \
    (HTRCAP_NONE),                   \
    (0x00u),                         \
    (0x00u),                         \
    (0x00u),                        \
    {0u,0u,0u,0u,0u}                \
    } \
    \
    ##IUG_HEATER_PROPERTY_NVM_DEF_HGn  \
    \
}


#define IUG_UNT_HEATER_COMP_PROP_NVM_DEF_CFG \
{ \
     \
    { (WBUS_COMP_ENUM_MAX), (WBUS_PHYSVALM45_ENUM_MAX) },\
    { (WBUS_COMP_ENUM_MAX), (WBUS_PHYSVALM45_ENUM_MAX) },\
    { (WBUS_COMP_ENUM_MAX), (WBUS_PHYSVALM45_ENUM_MAX) },\
    { (WBUS_COMP_ENUM_MAX), (WBUS_PHYSVALM45_ENUM_MAX) },\
    { (WBUS_COMP_ENUM_MAX), (WBUS_PHYSVALM45_ENUM_MAX) },\
    { (WBUS_COMP_ENUM_MAX), (WBUS_PHYSVALM45_ENUM_MAX) },\
    { (WBUS_COMP_ENUM_MAX), (WBUS_PHYSVALM45_ENUM_MAX) },\
    { (WBUS_COMP_ENUM_MAX), (WBUS_PHYSVALM45_ENUM_MAX) },\
    \
     \
    { (WBUS_COMP_ENUM_MAX), (WBUS_PHYSVALM45_ENUM_MAX) },\
    { (WBUS_COMP_ENUM_MAX), (WBUS_PHYSVALM45_ENUM_MAX) },\
    { (WBUS_COMP_ENUM_MAX), (WBUS_PHYSVALM45_ENUM_MAX) },\
    { (WBUS_COMP_ENUM_MAX), (WBUS_PHYSVALM45_ENUM_MAX) },\
    { (WBUS_COMP_ENUM_MAX), (WBUS_PHYSVALM45_ENUM_MAX) },\
    { (WBUS_COMP_ENUM_MAX), (WBUS_PHYSVALM45_ENUM_MAX) },\
    { (WBUS_COMP_ENUM_MAX), (WBUS_PHYSVALM45_ENUM_MAX) },\
    { (WBUS_COMP_ENUM_MAX), (WBUS_PHYSVALM45_ENUM_MAX) } \
}


#define IUG_LEVEL_AIR_VENTILATION_DEF \
{ \
\
    { \
         \
        (60u)                    \
    }, \
    \
    { \
         \
        (67u)                    \
    }, \
    \
    { \
         \
        (75u)                    \
    }, \
    \
    { \
         \
        (80u)                    \
    } \
    \
}    
#if 0
    { 
        (90u)                    
    } 
}
#endif



#define IUG_MODE_BTS_AIR_SETPOINT_HEATING_1DEG      50 
#define IUG_MODE_BTS_AIR_SETPOINT_VENTILATION_1DEG  20 
#define IUG_MODE_BTS_AIR_SETPOINT_BOOST_1DEG        70 
#define IUG_MODE_BTS_AIR_SETPOINT_ECO_1DEG          40 


#define IUG_MODE_BTS_AIR_SETPOINT_DEF \
{ \
    \
    { \
         \
        (ID_UI_SETPOINT_BTS_AIR)         \
    }, \
    \
    { \
         \
        \
        (ID_WEBPAR_ID_INVALID)          \
    }, \
    \
    { \
         \
        (ID_UI_SETPOINT_BOOST_BTS_AIR)   \
    }, \
    \
    { \
         \
        (ID_UI_SETPOINT_ECO_BTS_AIR)     \
    } \
    \
}


#define IUG_HEATER_STATE_NUMBER_DEF \
{ \
    \
    \
    \
    \
    \
    \
    \
    {(CATEGORY_OFF)                     ,(HTRST_04_AUS_State)               },\
    {(CATEGORY_OFF)                     ,(HTRST_16_INIT_State)              },\
    {(CATEGORY_OFF)                     ,(HTRST_1E_NEW_INIT_State)          },\
    {(CATEGORY_OFF)                     ,(HTRST_71_WST_WaitState)           },\
    {(CATEGORY_OFF)                     ,(HTRST_8F_PAUSE)                   },\
    {(CATEGORY_OFF)                     ,(HTRST_70_CPU_OFF)                 },\
    \
    \
    \
    {(CATEGORY_STARTUP)                 ,(HTRST_08_BMS_BrennermotorLosr)    },\
    {(CATEGORY_STARTUP)                 ,(HTRST_13_GA_Geblaeseanlauf)       },\
    {(CATEGORY_STARTUP)                 ,(HTRST_14_GSR_GluehstiftRampe)     },\
    {(CATEGORY_STARTUP)                 ,(HTRST_2D_VFOE_Vorfoerdern)        },\
    {(CATEGORY_STARTUP)                 ,(HTRST_2E_VGL_Vorgluehen)          },\
    {(CATEGORY_STARTUP)                 ,(HTRST_2F_VGLP_VorgluehenReg)      },\
    {(CATEGORY_STARTUP)                 ,(HTRST_56_LOS_BrennMotLosreissen)  },\
    {(CATEGORY_STARTUP)                 ,(HTRST_5D_SV_Startversuch)         },\
    {(CATEGORY_STARTUP)                 ,(HTRST_5E_VLV_Vorlaufverlaeng)     },\
    {(CATEGORY_STARTUP)                 ,(HTRST_75_PGL1_Vorgluehen)         },\
    {(CATEGORY_STARTUP)                 ,(HTRST_76_PGL2_Vorgluehen)         },\
    {(CATEGORY_STARTUP)                 ,(HTRST_77_PGL2_PLS_PartLoadStart)  },\
    {(CATEGORY_STARTUP)                 ,(HTRST_78_FMC_S_FlameMonMeasStart) },\
    {(CATEGORY_STARTUP)                 ,(HTRST_79_S1_START1)               },\
    {(CATEGORY_STARTUP)                 ,(HTRST_7A_S2_START2)               },\
    {(CATEGORY_STARTUP)                 ,(HTRST_7B_S3_START3)               },\
    {(CATEGORY_STARTUP)                 ,(HTRST_7C_S4_START4)               },\
    {(CATEGORY_STARTUP)                 ,(HTRST_7D_S5_START5)               },\
    {(CATEGORY_STARTUP)                 ,(HTRST_7E_S6_START6)               },\
    {(CATEGORY_STARTUP)                 ,(HTRST_82_S1_PLS_START1)           },\
    {(CATEGORY_STARTUP)                 ,(HTRST_83_S2_PLS_START2)           },\
    {(CATEGORY_STARTUP)                 ,(HTRST_84_S3_PLS_START3)           },\
    {(CATEGORY_STARTUP)                 ,(HTRST_85_S4_PLS_START4)           },\
    {(CATEGORY_STARTUP)                 ,(HTRST_86_S5_PLS_START5)           },\
    {(CATEGORY_STARTUP)                 ,(HTRST_87_S6_PLS_START6)           },\
    {(CATEGORY_STARTUP)                 ,(HTRST_A1_STRK_StartrKaltstart)    },\
    {(CATEGORY_STARTUP)                 ,(HTRST_A2_STRK_StartrWarmstart)    },\
    \
    \
    \
    {(CATEGORY_STARTUP_HI_CURRENT)      ,(HTRST_14_GSR_GluehstiftRampe)     },\
    {(CATEGORY_STARTUP_HI_CURRENT)      ,(HTRST_2E_VGL_Vorgluehen)          },\
    {(CATEGORY_STARTUP_HI_CURRENT)      ,(HTRST_2F_VGLP_VorgluehenReg)      },\
    {(CATEGORY_STARTUP_HI_CURRENT)      ,(HTRST_32_ZGL_Zugluehen)           },\
    {(CATEGORY_STARTUP_HI_CURRENT)      ,(HTRST_35_ZWGL_Zwischengluehen)    },\
    {(CATEGORY_STARTUP_HI_CURRENT)      ,(HTRST_75_PGL1_Vorgluehen)         },\
    {(CATEGORY_STARTUP_HI_CURRENT)      ,(HTRST_76_PGL2_Vorgluehen)         },\
    {(CATEGORY_STARTUP_HI_CURRENT)      ,(HTRST_77_PGL2_PLS_PartLoadStart)  },\
    {(CATEGORY_STARTUP_HI_CURRENT)      ,(HTRST_7F_GPR_GluestiftRampe)      },\
    {(CATEGORY_STARTUP_HI_CURRENT)      ,(HTRST_88_GPR_PLS_GluestiftRampe)  },\
    \
    \
    \
    {(CATEGORY_PARTIAL_LOAD)            ,(HTRST_05_BBTL_BrennerTeillast)    },\
    {(CATEGORY_PARTIAL_LOAD)            ,(HTRST_8D_PL_PartialLoad)          },\
    {(CATEGORY_PARTIAL_LOAD)            ,(HTRST_9A_CCTC_H_ContCoolTempCtl)  },\
    \
    \
    \
    {(CATEGORY_FULL_LOAD)               ,(HTRST_06_BBVL_BrennerVollast)     },\
    {(CATEGORY_FULL_LOAD)               ,(HTRST_1F_RB_Regelbetrieb)         },\
    {(CATEGORY_FULL_LOAD)               ,(HTRST_41_BBS_BrennerStandheizen)  },\
    {(CATEGORY_FULL_LOAD)               ,(HTRST_42_BBZ_BrennerZuheizen)     },\
    {(CATEGORY_FULL_LOAD)               ,(HTRST_8B_FL_FullLoad)             },\
    {(CATEGORY_FULL_LOAD)               ,(HTRST_99_CCTC_ContCoolTempCtl)    },\
    {(CATEGORY_FULL_LOAD)               ,(HTRST_1F_RB_Regelbetrieb)         },\
    \
    \
    \
    {(CATEGORY_VENTILATING)             ,(HTRST_1C_LUE_Lueften)             },\
    \
    \
    \
    {(CATEGORY_BURNOUT_COOLING)         ,(HTRST_00_ABR_Ausbrennen)              },\
    {(CATEGORY_BURNOUT_COOLING)         ,(HTRST_01_ABSCH_Abschaltung)           },\
    {(CATEGORY_BURNOUT_COOLING)         ,(HTRST_02_ABT_AusbrennenTRS)           },\
    {(CATEGORY_BURNOUT_COOLING)         ,(HTRST_03_AKUE_AusbrennRampe)          },\
    {(CATEGORY_BURNOUT_COOLING)         ,(HTRST_1A_KUEG_Kuehlung)               },\
    {(CATEGORY_BURNOUT_COOLING)         ,(HTRST_53_ABK_Abkuehlen)               },\
    {(CATEGORY_BURNOUT_COOLING)         ,(HTRST_6D_NLR_Nachlauframpe)           },\
    {(CATEGORY_BURNOUT_COOLING)         ,(HTRST_90_BO_FL_BurnOut_FullLoad)      },\
    {(CATEGORY_BURNOUT_COOLING)         ,(HTRST_91_BO_PL_BurnOut_PartLoad)      },\
    {(CATEGORY_BURNOUT_COOLING)         ,(HTRST_92_BO_S_BurnOut_Start)          },\
    {(CATEGORY_BURNOUT_COOLING)         ,(HTRST_93_BO_PLS_BurnOut_PartStrt)     },\
    {(CATEGORY_BURNOUT_COOLING)         ,(HTRST_94_BO_OVH_BurnOut_Overheat)     },\
    {(CATEGORY_BURNOUT_COOLING)         ,(HTRST_95_CL1_Cooling1)                },\
    {(CATEGORY_BURNOUT_COOLING)         ,(HTRST_97_CL2_Cooling2)                },\
    {(CATEGORY_BURNOUT_COOLING)         ,(HTRST_1C_LUE_Lueften)                 },\
    \
    \
    \
    \
    \
    \
    \
    \
    \
    {(CATEGORY_CTL_PAUSE)                ,(HTRST_20_RP_Regelpause)              },\
    {(CATEGORY_CTL_PAUSE)                ,(HTRST_46_RPN_Regelpause_Nachlauf)    },\
    {(CATEGORY_CTL_PAUSE)                ,(HTRST_4A_RPS_Regelpause_Standheizen) },\
    {(CATEGORY_CTL_PAUSE)                ,(HTRST_8F_PAUSE)                      } \
}


extern const structure_IUG_UNIT_IUG_CONFIGURATION_t             IUG_UNT_IUG_CFG;
extern structure_IUG_UNIT_IUG_CONTROL_t                         IUG_UNT_IUG_CTL;


extern const IUG_UNIT_HEATER_CONFIGURATION_t                    IUG_UNT_HEATER_1_CFG;
extern IUG_UNIT_HEATER_HEATING_TIME_CTL_t                       IUG_UNT_HEATER_1_HEATING_TIME_CTL;
extern IUG_UNIT_EXT_COM_OUT_t                                   IUG_UNT_HEATER_1_CTL_EXT_OUT;
extern IUG_UNIT_PND_COM_OUT_t                                   IUG_UNT_HEATER_1_CTL_PND_OUT;
extern IUG_UNIT_READ_ID_t                                       IUG_UNT_HEATER_1_CTL_READ_ID;
extern const IUG_READ_ID_DATA_ADDRESS_CFG_t                     IUG_UNT_HEATER_1_RD_ID_DAT_ADDR_CFG[];
extern const uint8_t                                            ext_iug_entries_IUG_UNT_HEATER_1_RD_ID_DAT_ADDR_CFG;
extern IUG_UNIT_READ_DTC_CTL_t                                  IUG_UNT_HEATER_1_CTL_READ_DTC;
extern IUG_UNIT_COMP_TEST_t                                     IUG_UNT_HEATER_1_CTL_COMP_TEST;
extern IUG_UNIT_EEROM_RD_WR_t                                   IUG_UNT_HEATER_1_CTL_EEP_RD_WR;
extern IUG_COMM_CASC_CTL_t                                      IUG_UNT_HEATER_1_CTL_COMM_CASC;
extern const IUG_READ_STATUS_DATA_ADDRESS_CFG_t                 IUG_UNT_HEATER_1_RD_ST_DAT_ADDR_CFG[];
extern const uint8_t                                            ext_iug_entries_IUG_UNT_HEATER_1_RD_ST_DAT_ADDR_CFG;
extern IUG_UNIT_COMP_PROP_M45_NVM_ENTRY_t                       IUG_UNT_HEATER_1_COMP_PROP_NVM[(WBUS_COMP_PROPERTY_HEATER_MAX)];


extern const IUG_UNIT_HEATER_CONFIGURATION_t                    IUG_UNT_HEATER_2_CFG;
extern IUG_UNIT_HEATER_HEATING_TIME_CTL_t                       IUG_UNT_HEATER_2_HEATING_TIME_CTL;
extern IUG_UNIT_EXT_COM_OUT_t                                   IUG_UNT_HEATER_2_CTL_EXT_OUT;
extern IUG_UNIT_PND_COM_OUT_t                                   IUG_UNT_HEATER_2_CTL_PND_OUT;
extern IUG_UNIT_READ_ID_t                                       IUG_UNT_HEATER_2_CTL_READ_ID;
extern const IUG_READ_ID_DATA_ADDRESS_CFG_t                     IUG_UNT_HEATER_2_RD_ID_DAT_ADDR_CFG[];
extern const uint8_t                                            ext_iug_entries_IUG_UNT_HEATER_2_RD_ID_DAT_ADDR_CFG;
extern IUG_UNIT_READ_DTC_CTL_t                                  IUG_UNT_HEATER_2_CTL_READ_DTC;
extern IUG_UNIT_COMP_TEST_t                                     IUG_UNT_HEATER_2_CTL_COMP_TEST;
extern IUG_UNIT_EEROM_RD_WR_t                                   IUG_UNT_HEATER_2_CTL_EEP_RD_WR;
extern IUG_COMM_CASC_CTL_t                                      IUG_UNT_HEATER_2_CTL_COMM_CASC;
extern const IUG_READ_STATUS_DATA_ADDRESS_CFG_t                 IUG_UNT_HEATER_2_RD_ST_DAT_ADDR_CFG[];
extern const uint8_t                                            ext_iug_entries_IUG_UNT_HEATER_2_RD_ST_DAT_ADDR_CFG;
extern IUG_UNIT_COMP_PROP_M45_NVM_ENTRY_t                       IUG_UNT_HEATER_2_COMP_PROP_NVM[(WBUS_COMP_PROPERTY_HEATER_MAX)];


#if( (IUG_UNT_HEATER_HG_LAST) == (IUG_UNT_HEATER_HG8_DEF) )
extern const IUG_UNIT_HEATER_CONFIGURATION_t                    IUG_UNT_HEATER_3_CFG;
extern IUG_UNIT_HEATER_HEATING_TIME_CTL_t                       IUG_UNT_HEATER_3_HEATING_TIME_CTL;
extern IUG_UNIT_EXT_COM_OUT_t                                   IUG_UNT_HEATER_3_CTL_EXT_OUT;
extern IUG_UNIT_PND_COM_OUT_t                                   IUG_UNT_HEATER_3_CTL_PND_OUT;
extern IUG_UNIT_READ_ID_t                                       IUG_UNT_HEATER_3_CTL_READ_ID;
#endif
extern IUG_UNIT_COMP_PROP_M45_NVM_ENTRY_t                       IUG_UNT_HEATER_3_COMP_PROP_NVM[(WBUS_COMP_PROPERTY_HEATER_MAX)];


#if( (IUG_UNT_HEATER_HG_LAST) == (IUG_UNT_HEATER_HG8_DEF) )
extern const IUG_UNIT_HEATER_CONFIGURATION_t                    IUG_UNT_HEATER_4_CFG;
extern IUG_UNIT_HEATER_HEATING_TIME_CTL_t                       IUG_UNT_HEATER_4_HEATING_TIME_CTL;
extern IUG_UNIT_EXT_COM_OUT_t                                   IUG_UNT_HEATER_4_CTL_EXT_OUT;
extern IUG_UNIT_PND_COM_OUT_t                                   IUG_UNT_HEATER_4_CTL_PND_OUT;
extern IUG_UNIT_READ_ID_t                                       IUG_UNT_HEATER_4_CTL_READ_ID;
#endif
extern IUG_UNIT_COMP_PROP_M45_NVM_ENTRY_t                       IUG_UNT_HEATER_4_COMP_PROP_NVM[(WBUS_COMP_PROPERTY_HEATER_MAX)];


#if( (IUG_UNT_HEATER_HG_LAST) == (IUG_UNT_HEATER_HG8_DEF) )
extern const IUG_UNIT_HEATER_CONFIGURATION_t                    IUG_UNT_HEATER_5_CFG;
extern IUG_UNIT_HEATER_HEATING_TIME_CTL_t                       IUG_UNT_HEATER_5_HEATING_TIME_CTL;
extern IUG_UNIT_EXT_COM_OUT_t                                   IUG_UNT_HEATER_5_CTL_EXT_OUT;
extern IUG_UNIT_PND_COM_OUT_t                                   IUG_UNT_HEATER_5_CTL_PND_OUT;
extern IUG_UNIT_READ_ID_t                                       IUG_UNT_HEATER_5_CTL_READ_ID;
#endif
extern IUG_UNIT_COMP_PROP_M45_NVM_ENTRY_t                       IUG_UNT_HEATER_5_COMP_PROP_NVM[(WBUS_COMP_PROPERTY_HEATER_MAX)];


#if( (IUG_UNT_HEATER_HG_LAST) == (IUG_UNT_HEATER_HG8_DEF) )
extern const IUG_UNIT_HEATER_CONFIGURATION_t                    IUG_UNT_HEATER_6_CFG;
extern IUG_UNIT_HEATER_HEATING_TIME_CTL_t                       IUG_UNT_HEATER_6_HEATING_TIME_CTL;
extern IUG_UNIT_EXT_COM_OUT_t                                   IUG_UNT_HEATER_6_CTL_EXT_OUT;
extern IUG_UNIT_PND_COM_OUT_t                                   IUG_UNT_HEATER_6_CTL_PND_OUT;
extern IUG_UNIT_READ_ID_t                                       IUG_UNT_HEATER_6_CTL_READ_ID;
#endif
extern IUG_UNIT_COMP_PROP_M45_NVM_ENTRY_t                       IUG_UNT_HEATER_6_COMP_PROP_NVM[(WBUS_COMP_PROPERTY_HEATER_MAX)];


#if( (IUG_UNT_HEATER_HG_LAST) == (IUG_UNT_HEATER_HG8_DEF) )
extern const IUG_UNIT_HEATER_CONFIGURATION_t                    IUG_UNT_HEATER_7_CFG;
extern IUG_UNIT_HEATER_HEATING_TIME_CTL_t                       IUG_UNT_HEATER_7_HEATING_TIME_CTL;
extern IUG_UNIT_EXT_COM_OUT_t                                   IUG_UNT_HEATER_7_CTL_EXT_OUT;
extern IUG_UNIT_PND_COM_OUT_t                                   IUG_UNT_HEATER_7_CTL_PND_OUT;
extern IUG_UNIT_READ_ID_t                                       IUG_UNT_HEATER_7_CTL_READ_ID;
#endif
extern IUG_UNIT_COMP_PROP_M45_NVM_ENTRY_t                       IUG_UNT_HEATER_7_COMP_PROP_NVM[(WBUS_COMP_PROPERTY_HEATER_MAX)];


#if( (IUG_UNT_HEATER_HG_LAST) == (IUG_UNT_HEATER_HG8_DEF) )
extern const IUG_UNIT_HEATER_CONFIGURATION_t                    IUG_UNT_HEATER_8_CFG;
extern IUG_UNIT_HEATER_HEATING_TIME_CTL_t                       IUG_UNT_HEATER_8_HEATING_TIME_CTL;
extern IUG_UNIT_EXT_COM_OUT_t                                   IUG_UNT_HEATER_8_CTL_EXT_OUT;
extern IUG_UNIT_PND_COM_OUT_t                                   IUG_UNT_HEATER_8_CTL_PND_OUT;
extern IUG_UNIT_READ_ID_t                                       IUG_UNT_HEATER_8_CTL_READ_ID;
#endif
extern IUG_UNIT_COMP_PROP_M45_NVM_ENTRY_t                       IUG_UNT_HEATER_8_COMP_PROP_NVM[(WBUS_COMP_PROPERTY_HEATER_MAX)];


extern const IUG_UNIT_MULTI_CTL_CONFIGURATION_t                IUG_UNT_MULTI_CTL_3TO4_CFG;
extern IUG_UNIT_MULTI_CTL_CONTROL_t                            IUG_UNT_MULTI_CTL_3TO4_CTL;
extern structure_IUG_UNIT_EXT_COM_INP_t                        IUG_UNT_MULTI_CTL_3TO4_CTL_EXT_INP;
extern IUG_UNIT_PND_COM_INP_t                                  IUG_UNT_MULTI_CTL_3TO4_CTL_PND_INP;
extern IUG_UNIT_READ_ID_t                                      IUG_UNT_MULTI_CTL_3TO4_CTL_READ_ID;
extern IUG_COMM_CASC_CTL_t                                     IUG_UNT_MULTI_CTL_3TO4_CTL_COMM_CASC;
extern IUG_UNIT_SPEC_DATA_t                                    IUG_UNT_MULTI_CTL_3TO4_CTL_UNIT_SPEC_DATA;


extern const structure_IUG_UNIT_TELESTART_CONFIGURATION_t      IUG_UNT_TELESTART_CFG;
extern structure_IUG_UNIT_TELESTART_CONTROL_t                  IUG_UNT_TELESTART_CTL;
extern structure_IUG_UNIT_EXT_COM_INP_t                        IUG_UNT_TELESTART_CTL_EXT_INP;
extern IUG_UNIT_PND_COM_INP_t                                  IUG_UNT_TELESTART_CTL_PND_INP;
extern IUG_UNIT_READ_ID_t                                      IUG_UNT_TELESTART_CTL_READ_ID;
extern IUG_COMM_CASC_CTL_t                                     IUG_UNT_TELESTART_CTL_COMM_CASC;


extern const structure_IUG_UNIT_THERMO_CALL_CONFIGURATION_t    IUG_UNT_THERMO_CALL_CFG;
extern structure_IUG_UNIT_THERMO_CALL_CONTROL_t                IUG_UNT_THERMO_CALL_CTL;
extern structure_IUG_UNIT_EXT_COM_INP_t                        IUG_UNT_THERMO_CALL_CTL_EXT_INP;
extern IUG_UNIT_PND_COM_INP_t                                  IUG_UNT_THERMO_CALL_CTL_PND_INP;
extern IUG_UNIT_READ_ID_t                                      IUG_UNT_THERMO_CALL_CTL_READ_ID;
extern IUG_COMM_CASC_CTL_t                                     IUG_UNT_THERMO_CALL_CTL_COMM_CASC;
extern IUG_UNIT_SPEC_DATA_t                                    IUG_UNT_THERMO_CALL_CTL_UNIT_SPEC_DATA;


extern const IUG_UNIT_THERMO_CONNECT_CONFIGURATION_t           IUG_UNT_THERMO_CONNECT_CFG;
extern IUG_UNIT_THERMO_CONNECT_CONTROL_t                       IUG_UNT_THERMO_CONNECT_CTL;
extern IUG_UNIT_EXT_COM_INP_t                                  IUG_UNT_THERMO_CONNECT_CTL_EXT_INP;
extern IUG_UNIT_PND_COM_INP_t                                  IUG_UNT_THERMO_CONNECT_CTL_PND_INP;
extern IUG_UNIT_READ_ID_t                                      IUG_UNT_THERMO_CONNECT_CTL_READ_ID;
extern IUG_COMM_CASC_CTL_t                                     IUG_UNT_THERMO_CONNECT_CTL_COMM_CASC;


extern const structure_IUG_UNIT_BLUETOOTH_CONFIGURATION_t      IUG_UNT_BLUETOOTH_CTL_AIR_CFG;
extern structure_IUG_UNIT_BLUETOOTH_CONTROL_CONTROL_t          IUG_UNT_BLUETOOTH_CTL_AIR_CTL;

extern const structure_IUG_UNIT_BLUETOOTH_CONFIGURATION_t      IUG_UNT_BLUETOOTH_CTL_WAT_CFG;
extern structure_IUG_UNIT_BLUETOOTH_CONTROL_CONTROL_t          IUG_UNT_BLUETOOTH_CTL_WAT_CTL;

extern structure_IUG_UNIT_BLUETOOTH_TIMER_CTL_t                IUG_UNIT_BLUETOOTH_TIMER_CTL;


extern const structure_IUG_UNIT_TEMP_SENSOR_CONFIGURATION_t    IUG_UNT_TEMP_SENSOR_INT_CFG;
extern structure_IUG_UNIT_TEMP_SENSOR_CONTROL_t                IUG_UNT_TEMP_SENSOR_INT_CTL;
extern structure_IUG_UNIT_EXT_COM_OUT_t                        IUG_UNT_TEMP_SENSOR_INT_CTL_EXT_OUT;
extern IUG_UNIT_READ_ID_t                                      IUG_UNT_TEMP_SENSOR_INT_CTL_READ_ID;
extern const IUG_READ_ID_DATA_ADDRESS_CFG_t                    IUG_UNT_TEMP_SENSOR_INT_RD_ID_DAT_ADDR_CFG[];
extern const uint8_t                                           ext_iug_entries_IUG_UNT_TEMP_SENSOR_INT_RD_ID_DAT_ADDR_CFG;


extern const structure_IUG_UNIT_TEMP_SENSOR_CONFIGURATION_t    IUG_UNT_TEMP_SENSOR_EXT_CFG;
extern structure_IUG_UNIT_TEMP_SENSOR_CONTROL_t                IUG_UNT_TEMP_SENSOR_EXT_CTL;
extern structure_IUG_UNIT_EXT_COM_OUT_t                        IUG_UNT_TEMP_SENSOR_EXT_CTL_EXT_OUT;
extern IUG_UNIT_READ_ID_t                                      IUG_UNT_TEMP_SENSOR_EXT_CTL_READ_ID;
extern const IUG_READ_ID_DATA_ADDRESS_CFG_t                    IUG_UNT_TEMP_SENSOR_EXT_RD_ID_DAT_ADDR_CFG[];
extern const uint8_t                                           ext_iug_entries_IUG_UNT_TEMP_SENSOR_EXT_RD_ID_DAT_ADDR_CFG;


extern const IUG_UNIT_TEMP_SENSOR_T_LIN_R_CONFIGURATION_t      IUG_UNT_TEMP_SENSOR_T_LIN_R_INT_CFG;
extern IUG_UNIT_TEMP_SENSOR_T_LIN_R_CONTROL_t                  IUG_UNT_TEMP_SENSOR_T_LIN_R_INT_CTL;
extern structure_IUG_UNIT_EXT_COM_OUT_t                        IUG_UNT_TEMP_SENSOR_T_LIN_R_INT_CTL_EXT_OUT;
extern IUG_UNIT_READ_ID_t                                      IUG_UNT_TEMP_SENSOR_T_LIN_R_INT_CTL_READ_ID;
extern IUG_UNIT_SPEC_DATA_t                                    IUG_UNT_TEMP_SENSOR_T_LIN_R_INT_CTL_UNIT_SPEC_DATA;
extern const IUG_READ_ID_DATA_ADDRESS_CFG_t                    IUG_UNT_TEMP_SENSOR_T_LIN_R_INT_RD_ID_DAT_ADDR_CFG[];
extern const uint8_t                                           ext_iug_entries_IUG_UNT_TEMP_SENSOR_T_LIN_R_INT_RD_ID_DAT_ADDR_CFG;
extern IUG_UNT_USER_API_UNIT_DATA_CONTROL_t                    IUG_UNT_TEMP_SENSOR_T_LIN_R_INT_CTL_API_UNIT_DATA;


extern const IUG_UNIT_TEMP_SENSOR_T_LIN_R_CONFIGURATION_t      IUG_UNT_TEMP_SENSOR_T_LIN_R_EXT_CFG;
extern IUG_UNIT_TEMP_SENSOR_T_LIN_R_CONTROL_t                  IUG_UNT_TEMP_SENSOR_T_LIN_R_EXT_CTL;
extern structure_IUG_UNIT_EXT_COM_OUT_t                        IUG_UNT_TEMP_SENSOR_T_LIN_R_EXT_CTL_EXT_OUT;
extern IUG_UNIT_READ_ID_t                                      IUG_UNT_TEMP_SENSOR_T_LIN_R_EXT_CTL_READ_ID;
extern IUG_UNIT_SPEC_DATA_t                                    IUG_UNT_TEMP_SENSOR_T_LIN_R_EXT_CTL_UNIT_SPEC_DATA;
extern const IUG_READ_ID_DATA_ADDRESS_CFG_t                    IUG_UNT_TEMP_SENSOR_T_LIN_R_EXT_RD_ID_DAT_ADDR_CFG[];
extern const uint8_t                                           ext_iug_entries_IUG_UNT_TEMP_SENSOR_T_LIN_R_EXT_RD_ID_DAT_ADDR_CFG;
extern IUG_UNT_USER_API_UNIT_DATA_CONTROL_t                    IUG_UNT_TEMP_SENSOR_T_LIN_R_EXT_CTL_API_UNIT_DATA;


extern const structure_IUG_UNIT_CMD_BUTTON_CONFIGURATION_t     IUG_UNT_CMD_BUTTON_CFG;
extern structure_IUG_UNIT_CMD_BUTTON_CONTROL_t                 IUG_UNT_CMD_BUTTON_CTL;


#ifdef IUG_UNT_SIMULATION_EN
extern const structure_IUG_UNIT_SIMULATION_CONFIGURATION_t     IUG_UNT_SIMULATION_CFG;
extern structure_IUG_UNIT_SIMULATION_CONTROL_t                 IUG_UNT_SIMULATION_CTL;
#endif


extern const structure_IUG_UNT_USER_API_CONFIGURATION_t        IUG_UNT_USER_API_CFG;
extern structure_IUG_UNT_USER_API_CONTROL_t                    IUG_UNT_USER_API_CTL;


extern const IUG_UNT_USER_DEVICE_CFG_t                         IUG_UNT_USER_DEVICE_CFG;
extern IUG_UNT_USER_DEVICE_CTL_t                               IUG_UNT_USER_DEVICE_CTL;
extern IUG_UNIT_EXT_COM_OUT_t                                  IUG_UNT_USER_DEVICE_CTL_EXT_OUT;
extern IUG_UNIT_PND_COM_OUT_t                                  IUG_UNT_USER_DEVICE_CTL_PND_OUT;


extern const structure_IUG_UNIT_TREE_CONFIGURATION_t           IUG_UNT_TREE_CFG[];
extern const uint8_t ext_iug_entries_IUG_UNT_TREE_CFG;


extern IUG_UNIT_COMMAND_REQUEST_HANDLER_t             IUG_UNT_CMD_REQ_HANDLER_CFG[];
extern const uint8_t ext_iug_entries_IUG_UNT_CMD_REQ_HANDLER_CFG;


extern const IUG_WBUS_MEMBER_DESCRIPTION_t                      IUG_WBUS_MBR_DESCRIPTION[(WBUS_MBR_ENUM_MAX)];
extern const IUG_CMD_TO_WBUS_MSG_TRANSLATION_t        IUG_CMD_TO_WBUS_MSG[];
extern const uint8_t ext_iug_entries_IUG_CMD_TO_WBUS_MSG;


extern const IUG_CMD_TO_IUG_CMD_AIR_TRANSLATION_t     IUG_CMD_TO_IUG_CMD_AIR[];
extern const uint8_t ext_iug_entries_IUG_CMD_TO_IUG_CMD_AIR;


extern const IUG_UNIT_COMM_MATRIX_NVM_t                         IUG_UNIT_COMMTX_NVM_DEF[(IUG_UNIT_COMMTX_ENTRIES)];
extern IUG_UNIT_COMM_MATRIX_NVM_t                               IUG_UNIT_COMMTX_NVM[(IUG_UNIT_COMMTX_ENTRIES)];
extern IUG_UNIT_COMM_MTX_BINDING_t                              IUG_UNIT_COMMTX_BINDING[(IUG_UNIT_COMMTX_ENTRIES)];


extern const structure_IUG_LEVEL_AIR_VENTILATION_CFG_t          IUG_LEVEL_AIR_VENTILATION_CFG[(IUG_LEVEL_AIR_ENUM_MAX)];
extern const uint8_t ext_iug_entries_IUG_LEVEL_AIR_VENTILATION;


extern const structure_IUG_MODE_BTS_AIR_SETPOINT_CFG_t          IUG_MODE_BTS_AIR_SETPOINT_CFG[(IUG_MODE_BTS_AIR_ENUM_MAX)];
extern const uint8_t ext_iug_entries_IUG_MODE_BTS_AIR_SETPOINT;


extern const IUG_HEATER_STATE_NUMBER_CFG_t                      IUG_HEATER_STATE_NUMBER_CFG[];
extern const uint8_t ext_iug_entries_IUG_HEATER_STATE_NUMBER;


extern const structure_IUG_POWER_SUPPLY_CFG_t                   IUG_POWER_SUPPLY_CFG[(IUG_POWER_SUPPLY_VOLTAGE_MAX)];
extern structure_IUG_POWER_SUPPLY_CTL_t                         IUG_POWER_SUPPLY_CTL;

boolean     IUG_unit_cfg_init( void );


#endif 


