#ifndef __IUG_UNIT_IUG_H_
#define __IUG_UNIT_IUG_H_


#define IUG_UNIT_IUG_EVT_NONE                       0x00u
#define IUG_UNIT_IUG_EVT_01                         0x01u
#define IUG_UNIT_IUG_EVT_02                         0x02u
#define IUG_UNIT_IUG_EVT_04                         0x04u
#define IUG_UNIT_IUG_EVT_08                         0x08u
#define IUG_UNIT_IUG_EVT_10                         0x10u
#define IUG_UNIT_IUG_EVT_20                         0x20u
#define IUG_UNIT_IUG_EVT_40                         0x40u
#define IUG_UNIT_IUG_EVT_80                         0x80u



boolean         IUG_iug_cmd0_decide( const enum_IUG_TASK_ID_t task_id,
                                     IUG_UNIT_COMMAND_REQUEST_PENDING_t * const cmd_pnd_hgX );
IUG_COMMAND_t   IUG_iug_cmd2_evaluate_and_provide( IUG_UNIT_COMMAND_REQUEST_PENDING_t * const cmd_pnd_hgX );
boolean         IUG_iug_cmd_trigger( const IUG_COMMAND_t cmd_req );
void            IUG_iug_ctl_cyclic( const uint8_t idx_unit_tree );
void            IUG_iug_ctl_init( const uint8_t idx_unit_tree );
void            IUG_iug_ctl_cyclic_preparation( void );
void            IUG_iug_ctl_cyclic_preparation_signals( void );
void            IUG_iug_handler_read_id_serial_number( void );

void IUG_iug_diag_webpar_trg_by_wbus(const enum_parameter_id para_id, const uint8_t* const ptr_val);

static void IUG_iug_diag_webpar_process_shared_para(void);

static void IUG_iug_diag_webpar_shared_para_check_default(enum_IUG_SHARED_PARA_CTRL *const ptr_trigger);

IUG_UNIT_ID_t   IUG_iug_translate_api_unit_2_iug_unit( const IUG_UNIT_API_UNIT_ID_t api_unit );
void            IUG_iug_update_state( void );
void            IUG_iug_update_unit_state( void );
void            IUG_iug_update_unit_state_air( const IUG_HEATER_UNIT_ENUM_t idx_htr );
void            IUG_iug_update_unit_state_ghi( const IUG_HEATER_UNIT_ENUM_t idx_htr );
void            IUG_iug_update_unit_state_wat( const IUG_HEATER_UNIT_ENUM_t idx_htr );


#if defined (IUG_DIAGMTX0_IMPLEMENTED) ||\
    defined (IUG_DIAGMTX1_IMPLEMENTED) ||\
    defined (IUG_DIAGMTX2_IMPLEMENTED)
void IUG_iug_diag_mtx( void );
#endif


void IUG_iug_diag_webpar(void);



#endif 


