#ifndef __IUG_UNT_IUG_USER_API_H_
#define __IUG_UNT_IUG_USER_API_H_



IUG_COMMAND_t IUG_unt_user_api_cmd_pre_evaluate( const IUG_UNIT_ID_t unit_id,
                                                 const IUG_HEATER_UNIT_ENUM_t evaluate_for_htr_idx,
                                                 IUG_UNIT_COMMAND_REQUEST_t * const req_user_api );
void    IUG_unt_user_api_ctl_cyclic( const uint8_t idx_unit_tree );
void    IUG_unt_user_api_ctl_init( const uint8_t idx_unit_tree );

#ifdef IUG_UNIT_USER_API_DEBUGGING_SUPPORT_EN
typedef struct
{
    IUG_UNIT_API_UNIT_ID_t                  usr_api_unit;
    structure_IUG_UNIT_API_UNIT_CTL_CMD_t   usr_api_cmd;
    IUG_UNIT_API_STATE_t                    usr_api_state;
    IUG_UNIT_API_RETVAL_t                   usr_api_ret;

} struct_unit_iug_usr_api_debug_t;
#endif


typedef struct
{
    IUG_UNIT_API_UNIT_ID_t                  usr_api_unit;
    structure_IUG_UNIT_API_UNIT_CTL_CMD_t   usr_api_cmd;
    IUG_UNIT_API_STATE_t                    usr_api_state;
    IUG_UNIT_API_RETVAL_t                   usr_api_ret;

} struct_unit_iug_usr_api_debug_t;


#endif 


