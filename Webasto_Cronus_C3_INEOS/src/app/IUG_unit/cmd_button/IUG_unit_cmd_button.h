#ifndef __IUG_UNIT_CMD_BUTTON_H_
#define __IUG_UNIT_CMD_BUTTON_H_


#include "IUG_environment_data.h"

#define IUG_UNIT_CMDBTN_PTC_LUT_IDX                 24u 

#define IUG_UNIT_CMDBTN_STA_NONE                    0x00u
#define IUG_UNIT_CMDBTN_STA_RELEASED                0x01u 
#define IUG_UNIT_CMDBTN_STA_PUSHED                  0x02u 
#define IUG_UNIT_CMDBTN_STA_04                      0x04u
#define IUG_UNIT_CMDBTN_STA_08                      0x08u
#define IUG_UNIT_CMDBTN_STA_10                      0x10u
#define IUG_UNIT_CMDBTN_STA_20                      0x20u
#define IUG_UNIT_CMDBTN_STA_40                      0x40u
#define IUG_UNIT_CMDBTN_STA_SIGNAL                  0x80u 


#define IUG_UNIT_CMDBTN_GEN_CMD_TIMEOUT_MS          2000u 

#define IUG_UNIT_CMDBTN_SIGNAL_LOW_LIMIT_mV         5000u 

#define IUG_UNIT_CMDBTN_SIGNAL_FLT_CNT              ((50u + (IUG_UNIT_CTL_CYLIC_PERIOD)) / (IUG_UNIT_CTL_CYLIC_PERIOD)) 

#define IUG_UNIT_CMDBTN_SIGNAL_SHORT_GND_UBAT_CNT   ((60000u + (IUG_UNIT_CTL_CYLIC_PERIOD)) / (IUG_UNIT_CTL_CYLIC_PERIOD))


#define IUG_UNIT_CMDBTN_INHIBIT_DUE_PRESSED_LONG_S      30u  


#define IUG_UNIT_CMDBTN_TIME_BUTTON_CONFIG_DEF_S        5u  
#define IUG_UNIT_CMDBTN_TIME_BUTTON_CONFIG_MIN_S        3u  
#define IUG_UNIT_CMDBTN_TIME_BUTTON_CONFIG_MAX_S       10u  


#define IUG_UNIT_CMDBTN_BLE_PAIRING_WINDOW_S          (IUG_UNIT_CMDBTN_TIME_BUTTON_CONFIG_DEF_S)  


#define IUG_CMDBTN_LED_BRIGHT_OFF                    0u 
#define IUG_CMDBTN_LED_BRIGHT_MAX_PERMANENT         14u 
#define IUG_CMDBTN_LED_BRIGHT_MAX_PERMANENT_50       7u 
#define IUG_CMDBTN_LED_BRIGHT_MAX_FLASHING          38u 
#define IUG_CMDBTN_LED_BRIGHT_MAX_FLASHING_50       19u 

#if( (IUG_CMDBTN_LED_BRIGHT_MAX_FLASHING) >= (IUG_CMDBTN_LED_BRIGHT_MAX_PERMANENT) )
    #define IUG_CMDBTN_LED_BRIGHT_MAX_LIMIT     (IUG_CMDBTN_LED_BRIGHT_MAX_FLASHING)
#else
    #define IUG_CMDBTN_LED_BRIGHT_MAX_LIMIT     (IUG_CMDBTN_LED_BRIGHT_MAX_PERMANENT)
#endif

#define IUG_CMDBTN_LED_PWM_MAX_LIMIT            (( 127u * (IUG_CMDBTN_LED_BRIGHT_MAX_LIMIT)) / 100u )


#define IUG_CMDBTN_USR_API_CMD_BTN_PAT_ID_MAX       1u








typedef struct
{
    IUG_CMD_BTN_PATTERN_t           pattern_id;     
    API_CMDBTN_LED_PATTERN_t        api_pattern_id; 

} structure_IUG_CMD_BUTTON_API_LED_PATTERN_CONFIGURATION_t;
typedef structure_IUG_CMD_BUTTON_API_LED_PATTERN_CONFIGURATION_t    IUG_CMD_BTN_API_LED_PATTERN_CFG_t;


typedef struct
{
    uint8_t ptc_temp_count;
    int16_t ptc_temp_raw;
    int16_t ptc_temp_flt_pt1;
    int16_t ptc_temp_final;
    struct_PT1_FILTER ptc_pt1_filter;  
} struct_IUG_CMD_BUTTON_PTC_VAL_t;


extern const uint8_t                            ext_iug_entries_IUG_CMD_BUTTON_LED_PATTERN_CFG;

extern const IUG_CMD_BTN_API_LED_PATTERN_CFG_t  IUG_CMDBTN_USR_API_PAT_ID_CFG[];
extern const uint8_t                            ext_iug_entries_IUG_CMDBTN_USR_API_PAT_ID_CFG;

extern IUG_CMD_BUTTON_LED_PATTERN_CFG_t         IUG_CMDBTN_USR_API_LED_PAT_CFG[(IUG_CMDBTN_USR_API_CMD_BTN_PAT_ID_MAX)];




void IUG_cmd_button_cmd_preset( void );


void IUG_cmd_button_ctl_cyclic( const uint8_t idx_unit_tree );


void IUG_cmd_button_ctl_display_dev_mode( void );


void IUG_cmd_button_ctl_init( const uint8_t idx_unit_tree );


inline IUG_CMD_BTN_PATTERN_t IUG_cmd_button_ctl_display_get_hmi_pattern( void ) { return IUG_UNT_CMD_BUTTON_CTL.led_pat_display_ctl.dpy_pat_hmi; }


inline void IUG_cmd_button_ctl_display_set_hmi_pattern( const IUG_CMD_BTN_PATTERN_t pat_hmi ) { IUG_UNT_CMD_BUTTON_CTL.led_pat_display_ctl.dpy_pat_hmi = pat_hmi; }


IUG_CMD_BTN_PATTERN_t   IUG_cmd_button_ctl_display_test_led_pattern( const IUG_CMD_BTN_PATTERN_t pattern_id );


IUG_COMMAND_t   IUG_cmd_button_cmd_pre_evaluate( const IUG_UNIT_ID_t unit_id, 
                                                 const IUG_HEATER_UNIT_ENUM_t evaluate_for_htr_idx,  
                                                 IUG_UNIT_COMMAND_REQUEST_t * const req_cmd_btn );
boolean         IUG_cmd_button_is_pushed(void);
void            IUG_cmd_button_test_failure( void );

extern boolean IUG_cmd_button_is_pushed(void);


void IUG_cmd_button_ctl_LED_set_duty( const uint8_t pwm_value );

void IUG_cmd_button_env_init(const int16_t __packed* * const ptr_temp_val, const enum_ENV_TEMP_SENS temp_sens, funct_ptr_ibn_temp_sens* funct_get_temp);

static void IUG_cmd_button_calc_temp_cyclic(void);

static enum_ENV_ERR_TEMP_SENS IUG_cmd_button_get_temp( int16_t * const temp_value, const enum_ENV_TEMP_SENS temp_sens );

static void IUG_cmd_button_diag_webpar(void);

static void IUG_cmd_button_eval_err_info_for_dtc(const enum_ENV_ERR_TEMP_SENS error_info);


void    IUG_cmd_button_ctl_display_cyclic( void );

void IUG_cmd_button_ctl_display_dev_mode( void );

uint8_t IUG_cmd_button_ctl_display_check_pattern( const uint8_t curve_selector, int16_t * const p_curve_val, const uint8_t point_num );
void    IUG_cmd_button_ctl_display_init( void );

IUG_CMD_BUTTON_LED_PATTERN_CFG_t * IUG_cmd_button_usrapi_get_led_pattern_cfg( const uint8_t usrapi_idx_pat );
uint8_t IUG_cmd_button_usrapi_test_led_pattern( const IUG_CMD_BTN_PATTERN_t pattern_id );


void    IUG_cmd_button_evt_AUT_AUX_HEATING_configuration_update( void );
void    IUG_cmd_button_evt_AUT_AUX_HEATING_slot_activation( void );
void    IUG_cmd_button_evt_BLE_PAIRING_advertising_activation( const uint8_t ava_ble, const uint8_t ava_piggy );
void    IUG_cmd_button_evt_BLE_PAIRING_advertising_reset( const uint8_t ava_ble, const uint8_t ava_piggy );
void    IUG_cmd_button_evt_BLE_PAIRING_slot_activation( const uint8_t ava_ble, const uint8_t ava_piggy );

_Static_assert((PG_MODE_BINARY)  ==(enum_PG_PATTERN_MODE_t)(API_CMDBTN_PAT_MODE_BINARY)  ,"Review encoding of 'API_CMDBTN_PAT_MODE_BINARY'!!!");
_Static_assert((PG_MODE_RAMP)    ==(enum_PG_PATTERN_MODE_t)(API_CMDBTN_PAT_MODE_RAMP)    ,"Review encoding of 'API_CMDBTN_PAT_MODE_RAMP'!!!");
_Static_assert((PG_MODE_ENUM_MAX)==(enum_PG_PATTERN_MODE_t)(API_CMDBTN_PAT_MODE_ENUM_MAX),"Review encoding of 'API_CMDBTN_PAT_MODE_ENUM_MAX'!!!");


#endif 


