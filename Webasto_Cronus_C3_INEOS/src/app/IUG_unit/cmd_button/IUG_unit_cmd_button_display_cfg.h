#ifndef __IUG_UNIT_CMD_BUTTON_DISPLAY_CFG_H_
#define __IUG_UNIT_CMD_BUTTON_DISPLAY_CFG_H_



#ifdef IUG_TRIGGER_WBUS_SCANNING_DEVELOPMENT_EN
    #define IUG_CMD_BUTTON_LED_PATTERN_TRG_HG_SCANNING \
        { (CMD_BTN_PAT_TRG_HG_SCANNING),                        0L  ,(PG_PATTERN_CFG_t *)&IUG_CMD_BTN_PAT_TRG_HG_SCANNING_cfg        },
#else
    #define IUG_CMD_BUTTON_LED_PATTERN_TRG_HG_SCANNING
#endif


#define IUG_CMD_BTN_LED_PATTERN_CONFIGURATION \
{ \
    \
    \
    \
    \
  \
  \
  \
    { (CMD_BTN_PAT_NONE),                                                               0L      , (NULL)                                                            },\
    { (CMD_BTN_PAT_IUG_OPERABLE),                 -(10L * 60L                )  ,(PG_PATTERN_CFG_t *)&IUG_CMD_BTN_PAT_IUG_OPERABLE_cfg              },\
    { (CMD_BTN_PAT_IUG_OPERABLE_DEV),                            +( 3L * 1000L)  ,(PG_PATTERN_CFG_t *)&IUG_CMD_BTN_PAT_IUG_OPERABLE_cfg              },\
    { (CMD_BTN_PAT_HEATING_AIR),                                                        0L      ,(PG_PATTERN_CFG_t *)&IUG_CMD_BTN_PAT_HEATING_AIR_cfg               },\
    { (CMD_BTN_PAT_HEATING_WAT),                                                        0L      ,(PG_PATTERN_CFG_t *)&IUG_CMD_BTN_PAT_HEATING_WAT_cfg               },\
    { (CMD_BTN_PAT_DRV_HEATING_WAT),                                                    0L      ,(PG_PATTERN_CFG_t *)&IUG_CMD_BTN_PAT_DRV_HEATING_WAT_cfg           },\
    { (CMD_BTN_PAT_VENTILATING_WAT),                                                    0L      ,(PG_PATTERN_CFG_t *)&IUG_CMD_BTN_PAT_VENTILATING_WAT_cfg           },\
    { (CMD_BTN_PAT_VENTILATING_AIR),                                                    0L      ,(PG_PATTERN_CFG_t *)&IUG_CMD_BTN_PAT_VENTILATING_AIR_cfg           },\
    { (CMD_BTN_PAT_BLINKING_FAULTY),                                                    0L      ,(PG_PATTERN_CFG_t *)&IUG_CMD_BTN_PAT_BLINKING_FAULTY_cfg           },\
    { (CMD_BTN_PAT_BLINKING_FAULTY_DEV),                                                0L      ,(PG_PATTERN_CFG_t *)&IUG_CMD_BTN_PAT_BLINKING_FAULTY_cfg           },\
    { (CMD_BTN_PAT_BLINKING_SWITCH_OFF),          -( 1L * 60L                )  ,(PG_PATTERN_CFG_t *)&IUG_CMD_BTN_PAT_BLINKING_SWITCH_OFF_cfg       },\
    { (CMD_BTN_PAT_BLINKING_SWITCH_OFF_DEV),                     +( 3L * 1000L)  ,(PG_PATTERN_CFG_t *)&IUG_CMD_BTN_PAT_BLINKING_SWITCH_OFF_cfg       },\
    { (CMD_BTN_PAT_HMI_OFF_DARK),                                                       0L      ,(PG_PATTERN_CFG_t *)&IUG_CMD_BTN_PAT_HMI_OFF_DARK_cfg              },\
    { (CMD_BTN_PAT_HMI_ON_BRIGHT),                                                      0L      ,(PG_PATTERN_CFG_t *)&IUG_CMD_BTN_PAT_HMI_ON_BRIGHT_cfg             },\
    { (CMD_BTN_PAT_HMI_INVALID_CMD),                                                    0L      ,(PG_PATTERN_CFG_t *)&IUG_CMD_BTN_PAT_HMI_INVALID_CMD_cfg           },\
    { (CMD_BTN_PAT_HMI_AUX_HEATING_CONFIGURED),                                         0L      ,(PG_PATTERN_CFG_t *)&IUG_CMD_BTN_PAT_HMI_AUX_HEATING_CFG_cfg       },\
    { (CMD_BTN_PAT_HMI_AUX_HEATING_NOT_CONFIGURED),                                     0L      ,(PG_PATTERN_CFG_t *)&IUG_CMD_BTN_PAT_HMI_AUX_HEATING_NOT_CFG_cfg   },\
    { (CMD_BTN_PAT_HMI_BLE_PAIRING_SLOT_ACTIVATION),        +( 400L)     ,(PG_PATTERN_CFG_t *)&IUG_CMD_BTN_PAT_HMI_BLE_PAIRING_SLOT_cfg      },\
    { (CMD_BTN_PAT_HMI_BLE_PAIRING_SOFTRESET),              +(  40L)     ,(PG_PATTERN_CFG_t *)&IUG_CMD_BTN_PAT_HMI_BLE_PAIRING_RESET_cfg     },\
    { (CMD_BTN_PAT_HMI_ERROR_GENERAL),                                                  0L      ,(PG_PATTERN_CFG_t *)&IUG_CMD_BTN_PAT_HMI_ERROR_GENERAL_cfg         },\
    { (CMD_BTN_PAT_BUTTON_USER_DEVICE),                                              2000L      ,(PG_PATTERN_CFG_t *)&IUG_CMD_BTN_PAT_BUTTON_USER_DEVICE_cfg        },\
    IUG_CMD_BUTTON_LED_PATTERN_TRG_HG_SCANNING \
    { (CMD_BTN_PAT_SLEEP_NOTICE),                                                    5000L      ,(PG_PATTERN_CFG_t *)&IUG_CMD_BTN_PAT_SLEEP_NOTICE_cfg              },\
}


#define IUG_CMD_BTN_LED_PATTERN_HIRARCHY_CONFIGURATION \
{ \
  \
  \
  \
  \
    {(IUG_TGT_WAIT_STATE)             ,(CMD_BTN_PAT_NONE)                 ,(CMD_BTN_PAT_IUG_OPERABLE_DEV)         ,  0u   },\
    {(IUG_TGT_HEATING)                ,(CMD_BTN_PAT_HEATING_WAT)          ,(CMD_BTN_PAT_HEATING_WAT)              , 20u   },\
    {(IUG_TGT_BOOST)                  ,(CMD_BTN_PAT_HEATING_AIR)          ,(CMD_BTN_PAT_HEATING_AIR)              , 21u   },\
    {(IUG_TGT_ECO)                    ,(CMD_BTN_PAT_HEATING_AIR)          ,(CMD_BTN_PAT_HEATING_AIR)              , 22u   },\
    {(IUG_TGT_AUX_HEATING)            ,(CMD_BTN_PAT_HEATING_WAT)          ,(CMD_BTN_PAT_HEATING_WAT)              , 30u   },\
    {(IUG_TGT_AUT_AUX_HEATING)        ,(CMD_BTN_PAT_HEATING_WAT)          ,(CMD_BTN_PAT_HEATING_WAT)              , 31u   },\
    {(IUG_TGT_DRIVE_HEATING)          ,(CMD_BTN_PAT_DRV_HEATING_WAT)      ,(CMD_BTN_PAT_DRV_HEATING_WAT)          , 40u   },\
  \
  \
    {(IUG_TGT_VENTILATION)            ,(CMD_BTN_PAT_VENTILATING_WAT)      ,(CMD_BTN_PAT_VENTILATING_WAT)          , 10u   },\
  \
    {(IUG_TGT_SWITCH_OFF)             ,(CMD_BTN_PAT_NONE)                 ,(CMD_BTN_PAT_BLINKING_SWITCH_OFF_DEV)  ,  1u   },\
    {(IUG_TGT_FAULT)                  ,(CMD_BTN_PAT_BLINKING_FAULTY)      ,(CMD_BTN_PAT_BLINKING_FAULTY_DEV)      ,  2u   },\
    {(IUG_TGT_INITIAL_OPERATION)      ,(CMD_BTN_PAT_BLINKING_FAULTY)      ,(CMD_BTN_PAT_BLINKING_FAULTY)          ,  0u   },\
    {(IUG_TGT_INITIAL_WAIT_STATE)     ,(CMD_BTN_PAT_NONE)                 ,(CMD_BTN_PAT_NONE)                     ,  0u   } \
}



#define IUG_CMDBTN_LED_BRIGHT_MIN_OFF                       (IUG_CMDBTN_LED_BRIGHT_OFF)
#define IUG_CMDBTN_LED_BRIGHT_MIN_HMI_OFF_DARK              (10u)
#define IUG_CMDBTN_LED_BRIGHT_MAX_HMI_ON_BRIGHT             (IUG_CMDBTN_LED_BRIGHT_MAX_FLASHING)


#define IUG_CMDBTN_LED_BRIGHT_MIN_IUG_OPERABLE              (0u)
#define IUG_CMDBTN_LED_BRIGHT_MAX_IUG_OPERABLE              (IUG_CMDBTN_LED_BRIGHT_MAX_LIMIT)

#define IUG_CMDBTN_LED_BRIGHT_MIN_HEATING_AIR               (0u)
#define IUG_CMDBTN_LED_BRIGHT_MAX_HEATING_AIR               (IUG_CMDBTN_LED_BRIGHT_MAX_PERMANENT)

#define IUG_CMDBTN_LED_BRIGHT_MIN_HEATING_WAT               (0u)
#define IUG_CMDBTN_LED_BRIGHT_MAX_HEATING_WAT               (IUG_CMDBTN_LED_BRIGHT_MAX_PERMANENT)

#define IUG_CMDBTN_LED_BRIGHT_MIN_DRV_HEATING_WAT           (0u)
#define IUG_CMDBTN_LED_BRIGHT_MAX_DRV_HEATING_WAT           (IUG_CMDBTN_LED_BRIGHT_MAX_PERMANENT_50)

#define IUG_CMDBTN_LED_BRIGHT_MIN_VENTILATING_AIR           (0u)
#define IUG_CMDBTN_LED_BRIGHT_MAX_VENTILATING_AIR           (IUG_CMDBTN_LED_BRIGHT_MAX_FLASHING_50)

#define IUG_CMDBTN_LED_BRIGHT_MIN_VENTILATING_WAT           (0u)
#define IUG_CMDBTN_LED_BRIGHT_MAX_VENTILATING_WAT           (IUG_CMDBTN_LED_BRIGHT_MAX_FLASHING_50)

#define IUG_CMDBTN_LED_BRIGHT_MIN_BLINKING_FAULTY           (IUG_CMDBTN_LED_BRIGHT_MIN_OFF)
#define IUG_CMDBTN_LED_BRIGHT_MAX_BLINKING_FAULTY           (IUG_CMDBTN_LED_BRIGHT_MAX_FLASHING)

#define IUG_CMDBTN_LED_BRIGHT_MIN_BLINKING_SWITCH_OFF       (0u)
#define IUG_CMDBTN_LED_BRIGHT_MAX_BLINKING_SWITCH_OFF       (IUG_CMDBTN_LED_BRIGHT_MAX_FLASHING_50)

#define IUG_CMDBTN_LED_BRIGHT_MIN_HMI_INVALID_CMD           (IUG_CMDBTN_LED_BRIGHT_MIN_HMI_OFF_DARK)
#define IUG_CMDBTN_LED_BRIGHT_MAX_HMI_INVALID_CMD           (IUG_CMDBTN_LED_BRIGHT_MAX_HMI_ON_BRIGHT)

#define IUG_CMDBTN_LED_BRIGHT_MIN_HMI_ERROR_GENERAL         (IUG_CMDBTN_LED_BRIGHT_MIN_HMI_OFF_DARK)
#define IUG_CMDBTN_LED_BRIGHT_MAX_HMI_ERROR_GENERAL         (IUG_CMDBTN_LED_BRIGHT_MAX_HMI_ON_BRIGHT)

#define IUG_CMDBTN_LED_BRIGHT_MIN_HMI_AUX_HEATING_CFG       (IUG_CMDBTN_LED_BRIGHT_MIN_HMI_OFF_DARK)
#define IUG_CMDBTN_LED_BRIGHT_MAX_HMI_AUX_HEATING_CFG       (IUG_CMDBTN_LED_BRIGHT_MAX_HMI_ON_BRIGHT)
#define IUG_CMDBTN_LED_BRIGHT_MIN_HMI_AUX_HEATING_NOT_CFG   (IUG_CMDBTN_LED_BRIGHT_MIN_HMI_OFF_DARK)
#define IUG_CMDBTN_LED_BRIGHT_MAX_HMI_AUX_HEATING_NOT_CFG   (IUG_CMDBTN_LED_BRIGHT_MAX_HMI_ON_BRIGHT)
#define IUG_CMDBTN_LED_BRIGHT_MIN_HMI_BLE_PAIRING_CFG       (IUG_CMDBTN_LED_BRIGHT_MIN_HMI_OFF_DARK)
#define IUG_CMDBTN_LED_BRIGHT_MAX_HMI_BLE_PAIRING_CFG       (IUG_CMDBTN_LED_BRIGHT_MAX_HMI_ON_BRIGHT)

#define IUG_CMDBTN_LED_BRIGHT_MIN_BUTTON_USER_DEVICE        (5u)
#define IUG_CMDBTN_LED_BRIGHT_MED_BUTTON_USER_DEVICE        ((IUG_CMDBTN_LED_BRIGHT_MAX_FLASHING) / 4u)
#define IUG_CMDBTN_LED_BRIGHT_MAX_BUTTON_USER_DEVICE        ((IUG_CMDBTN_LED_BRIGHT_MAX_FLASHING) / 2u)


#ifdef IUG_TRIGGER_WBUS_SCANNING_DEVELOPMENT_EN
#define IUG_CMDBTN_LED_BRIGHT_MIN_TRG_HG_SCANNING           (5u)
#define IUG_CMDBTN_LED_BRIGHT_MAX_TRG_HG_SCANNING           (IUG_CMDBTN_LED_BRIGHT_MAX_FLASHING)
#endif








extern const int16_t            mgl_cmd_button_LED_CHARACTERISTIC_x_bright[];
extern const int16_t            mgl_cmd_button_LED_CHARACTERISTIC_y_pwm[];
extern const uint8_t            ext_iug_entries_LED_CHARACTERISTIC;


extern const PG_PATTERN_CFG_t   IUG_CMD_BTN_PAT_IUG_OPERABLE_cfg;

extern const PG_PATTERN_CFG_t   IUG_CMD_BTN_PAT_HEATING_AIR_cfg;
extern const PG_PATTERN_CFG_t   IUG_CMD_BTN_PAT_HEATING_WAT_cfg;
extern const PG_PATTERN_CFG_t   IUG_CMD_BTN_PAT_DRV_HEATING_WAT_cfg;

extern const PG_PATTERN_CFG_t   IUG_CMD_BTN_PAT_VENTILATING_AIR_cfg;
extern const PG_PATTERN_CFG_t   IUG_CMD_BTN_PAT_VENTILATING_WAT_cfg;

extern const PG_PATTERN_CFG_t   IUG_CMD_BTN_PAT_BLINKING_FAULTY_cfg;

extern const PG_PATTERN_CFG_t   IUG_CMD_BTN_PAT_BLINKING_SWITCH_OFF_cfg;

extern const PG_PATTERN_CFG_t   IUG_CMD_BTN_PAT_HMI_OFF_DARK_cfg;
extern const PG_PATTERN_CFG_t   IUG_CMD_BTN_PAT_HMI_ON_BRIGHT_cfg;
extern const PG_PATTERN_CFG_t   IUG_CMD_BTN_PAT_HMI_INVALID_CMD_cfg;
extern const PG_PATTERN_CFG_t   IUG_CMD_BTN_PAT_HMI_ERROR_GENERAL_cfg;
extern const PG_PATTERN_CFG_t   IUG_CMD_BTN_PAT_HMI_AUX_HEATING_CFG_cfg;
extern const PG_PATTERN_CFG_t   IUG_CMD_BTN_PAT_HMI_AUX_HEATING_NOT_CFG_cfg;
extern const PG_PATTERN_CFG_t   IUG_CMD_BTN_PAT_HMI_BLE_PAIRING_SLOT_cfg;
extern const PG_PATTERN_CFG_t   IUG_CMD_BTN_PAT_HMI_BLE_PAIRING_RESET_cfg;

extern const PG_PATTERN_CFG_t  IUG_CMD_BTN_PAT_BUTTON_USER_DEVICE_cfg;
extern const PG_PATTERN_CFG_t   IUG_CMD_BTN_PAT_SLEEP_NOTICE_cfg;

#ifdef IUG_TRIGGER_WBUS_SCANNING_DEVELOPMENT_EN
extern const PG_PATTERN_CFG_t   IUG_CMD_BTN_PAT_TRG_HG_SCANNING_cfg;
#endif





#endif 


