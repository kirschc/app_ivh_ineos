#ifndef _IUG_UNIT_GEN_HEATER_TYPE_CFG_H_
#define _IUG_UNIT_GEN_HEATER_TYPE_CFG_H_




#ifdef IUG_DEVELOPMENT_ACTIVE
	#define IUG_HTR_SIGN_TTEVO0x5        

    #ifdef IUG_HTR_SIGN_TTEVO0x5
        #define IUG_HTR_SIGN_TTEVO0x5_CFG \
            { (IUG_HTR_SIGN_TT_EVO_GEN_1), "TTEVO0x5" ,(TGT_TT_EVO_GEN_1_WAT_5) },
    #else
        #define IUG_HTR_SIGN_TTEVO0x5_CFG
    #endif
#else
    #define IUG_HTR_SIGN_TTEVO0x5_CFG
#endif


#define IUG_GEN_HEATER_SIGN_DESCR_CFG \
{\
    \
    \
    \
    \
    \
    \
    \
    \
    \
    {(IUG_HTR_SIGN_DEF_HEATER_AIR)      ,"IiIiIiIi" ,(TGT_WBUS_DEFAULT_AIR_4)   },\
    {(IUG_HTR_SIGN_DEF_HEATER_WAT)      ,"IiIiIiIi" ,(TGT_WBUS_DEFAULT_WAT_4)   },\
    {(IUG_HTR_SIGN_AT_2000STC)          ,"IiIiIiIi" ,(TGT_AT_2000_STC_AIR_4)    },\
    \
    \
    \
    {(IUG_HTR_SIGN_TT_EVO_GEN_1)        ,"TTEVO 1G" ,(TGT_TT_EVO_GEN_1_WAT_4)   },\
    {(IUG_HTR_SIGN_TT_EVO_GEN_4)        ,"TTEVOAMW" ,(TGT_TT_EVO_GEN_4_WAT_4)   },\
    {(IUG_HTR_SIGN_TT_EVO_GEN_4)        ,"PQ 46 SH" ,(TGT_TT_EVO_GEN_4_WAT_4)   },\
    {(IUG_HTR_SIGN_TP_50)               ,"SG1597  " ,(TGT_TP_50_WAT_4)          },\
    {(IUG_HTR_SIGN_TP_90)               ,"SG1577  " ,(TGT_TP_90_WAT_4)          },\
    {(IUG_HTR_SIGN_TP_120)              ,"SGTP120 " ,(TGT_TP_120_WAT_4)         },\
    {(IUG_HTR_SIGN_AT_40_55)            ,"SG1580  " ,(TGT_AT_40_55_AIR_4)       },\
    {(IUG_HTR_SIGN_AT_EVO_2000)         ,"SG1574  " ,(TGT_AT_EVO_2000_AIR_4)    },\
    \
    {(IUG_HTR_SIGN_AT_3500)             ,"1580    " ,(TGT_AT_3500_AIR_4)        },\
    {(IUG_HTR_SIGN_TT_VEVO)             ,"SG1582  " ,(TGT_TT_VEVO_WAT_4)        },\
    {(IUG_HTR_SIGN_TT_Z_C)              ,"SG1576  " ,(TGT_TT_Z_C_WAT_4)         },\
    \
    \
    ##IUG_HTR_SIGN_TTEVO0x5_CFG \
    \
    \
    \
    \
    \
    \
    \
    \
    {(IUG_HTR_SIGN_GHI_DEF_AIR)         ,"IiIiIiIi" ,(TGT_GHI_DEFAULT_AIR_1)    },\
    {(IUG_HTR_SIGN_GHI_DEF_WAT)         ,"IiIiIiIi" ,(TGT_GHI_DEFAULT_WAT_1)    },\
    \
    {(IUG_HTR_SIGN_GHI_DEF_AIR)         ,"GHIDEF-A" ,(TGT_GHI_DEFAULT_AIR_1)    },\
    {(IUG_HTR_SIGN_GHI_DEF_WAT)         ,"GHIDEF-W" ,(TGT_GHI_DEFAULT_WAT_1)    },\
    \
    \
    \
    {(IUG_HTR_SIGN_GHI_HVH_AIR)         ,"GHIHVH-A" ,(TGT_GHI_HVH_AIR_1)        },\
    {(IUG_HTR_SIGN_GHI_HVH_WAT)         ,"GHIHVH-W" ,(TGT_GHI_HVH_WAT_1)        },\
    \
    {(IUG_HTR_SIGN_GHI_GEN4_WAT)        ,"GHIGEN4W" ,(TGT_GHI_GEN4_WAT_1)       } \
}










#endif 

