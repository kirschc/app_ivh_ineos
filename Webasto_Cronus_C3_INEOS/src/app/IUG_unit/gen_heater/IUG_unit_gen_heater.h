#ifndef __IUG_UNIT_GEN_HEATER_H_
#define __IUG_UNIT_GEN_HEATER_H_



#define IUG_HTRTRMFLG_NONE                             0x00000000u 
#define IUG_HTRTRMFLG_OFF_REGULAR                      0x00000001u 
#define IUG_HTRTRMFLG_OFF_HEAT_TIME_ELAPSED            0x00000002u 
#define IUG_HTRTRMFLG_OFF_DUE_MONITORING_OFF_CMD       0x00000004u 
#define IUG_HTRTRMFLG_OFF_DUE_IUG                      0x00000008u 
#define IUG_HTRTRMFLG_CMD_INVALID                      0x00000010u 
#define IUG_HTRTRMFLG_ACK_MSG_CMD_OFF                  0x00000020u 
#define IUG_HTRTRMFLG_PND_MSG_CMD_OFF                  0x00000040u 
#define IUG_HTRTRMFLG_PND_MSG_NAK_TOUT                 0x00000080u 
#define IUG_HTRTRMFLG_ACK_TIMEOUT                      0x00000100u 
#define IUG_HTRTRMFLG_ACK_IS_NAK                       0x00000200u 
#define IUG_HTRTRMFLG_OFF_DUE_MONITORING               0x00000400u 
#define IUG_HTRTRMFLG_PND_TIMEOUT                      0x00000800u 
#define IUG_HTRTRMFLG_OFF_DUE_DTC_STFL_HGVP            0x00001000u 
#define IUG_HTRTRMFLG_OFF_DUE_MONITORING_INT           0x00002000u 
#define IUG_HTRTRMFLG_INVALID_FZG_STATE                0x00004000u 
#define IUG_HTRTRMFLG_0008000                          0x00008000u 
#define IUG_HTRTRMFLG_HTR_OFF_DUE_AUX_OR_DRV_HEATING   0x00010000u 
#define IUG_HTRTRMFLG_IUG_OFF_DUE_AUX_OR_DRV_HEATING   0x00020000u 
#define IUG_HTRTRMFLG_OFF_DUE_WTT_BAN                  0x00040000u 
#define IUG_HTRTRMFLG_OFF_DUE_POR_SYNCHRONIZATION      0x00080000u 
#define IUG_HTRTRMFLG_OFF_DUE_MONITORING_INACTIVITY    0x00100000u 
#define IUG_HTRTRMFLG_OFF_DUE_MONITORING_READ_ST       0x00200000u 
#define IUG_HTRTRMFLG_OFF_DUE_IBN_ENTRY                0x00400000u 


#define IUG_HTRTRMFLG_REQ_TERMINATION               ( 0u \
                                                    |(IUG_HTRTRMFLG_OFF_REGULAR) \
                                                    |(IUG_HTRTRMFLG_OFF_HEAT_TIME_ELAPSED) \
                                                    |(IUG_HTRTRMFLG_OFF_DUE_MONITORING_OFF_CMD) \
                                                    |(IUG_HTRTRMFLG_OFF_DUE_IUG) \
                                                    |(IUG_HTRTRMFLG_CMD_INVALID) \
                                                    |(IUG_HTRTRMFLG_ACK_MSG_CMD_OFF) \
                                                    |(IUG_HTRTRMFLG_PND_MSG_CMD_OFF) \
                                                    |(IUG_HTRTRMFLG_PND_MSG_NAK_TOUT) \
                                                    |(IUG_HTRTRMFLG_ACK_TIMEOUT) \
                                                    |(IUG_HTRTRMFLG_ACK_IS_NAK) \
                                                    |(IUG_HTRTRMFLG_OFF_DUE_MONITORING) \
                                                    |(IUG_HTRTRMFLG_PND_TIMEOUT) \
                                                    |(IUG_HTRTRMFLG_OFF_DUE_DTC_STFL_HGVP) \
                                                    |(IUG_HTRTRMFLG_OFF_DUE_MONITORING_INT) \
                                                    | 0u \
                                                    | 0u \
                                                    | 0u \
                                                    |(IUG_HTRTRMFLG_OFF_DUE_POR_SYNCHRONIZATION) \
                                                    |(IUG_HTRTRMFLG_OFF_DUE_MONITORING_INACTIVITY) \
                                                    |(IUG_HTRTRMFLG_OFF_DUE_MONITORING_READ_ST) \
                                                    |(IUG_HTRTRMFLG_OFF_DUE_IBN_ENTRY) \
                                                    )



#define IUG_UNIT_HG_INACTIVE_COVERAGE_CNT       (((IUG_UNIT_HG_INACTIVE_COVERAGE_MS)+(IUG_UNIT_CTL_CYLIC_PERIOD))/(IUG_UNIT_CTL_CYLIC_PERIOD))
#define IUG_UNIT_HG_INACTIVE_COVERAGE_OFF_CNT   (((2000u)+(IUG_UNIT_CTL_CYLIC_PERIOD))/(IUG_UNIT_CTL_CYLIC_PERIOD))


#define IUG_UNIT_HG_WAT_VENT_PROGRESS_TRIGGER_CNT   ((IUG_UNIT_CTL_CYLIC_PERIOD)+((IUG_UNIT_CTL_CYLIC_PERIOD)/2u))  


#ifdef IUG_GEN_HEATER_CHECK_FUNCTION_PARAMETERS
    #define IUG_GEN_HEATER_TEST_PTR_UNIT_CTL( ptr_unit_ctl, ptr_unit_ctl_tested ) \
         \
        if( (NULL) == IUG_gen_heater_test_pointer_to_unit_ctl( (ptr_unit_ctl) ) ) \
        { \
             \
            *(ptr_unit_ctl_tested) = (NULL); \
             \
            return; \
        } \
        else \
        { \
             \
            *(ptr_unit_ctl_tested) = (ptr_unit_ctl); \
        }
#else
    #define IUG_GEN_HEATER_TEST_PTR_UNIT_CTL( ptr_unit_ctl, ptr_unit_ctl_tested ) \
         \
        *(ptr_unit_ctl_tested) = (ptr_unit_ctl);
#endif


extern IUG_HEATER_PROPERTY_NVM_t                IUG_HEATER_PROPERTY_NVM[(IUG_HEATER_UNIT_ENUM_MAX)];
extern const IUG_HEATER_PROPERTY_NVM_t          IUG_HEATER_PROPERTY_NVM_DEF[];
extern IUG_UNIT_HEATER_PROPERTY_t               ext_iug_heater_property[(IUG_HEATER_UNIT_ENUM_MAX)];
extern const IUG_UNIT_COMP_PROP_M45_NVM_ENTRY_t IUG_UNT_HEATER_COMP_PROP_NVM_DEF[(WBUS_COMP_PROPERTY_HEATER_MAX)];

extern uint8_t                                  ext_iug_webpar_hgx_available[(IUG_HEATER_UNIT_ENUM_MAX)];
extern IUG_HG_AVAILABLE_t                       ext_iug_webpar_hgX_disply_map[(IUG_HEATER_UNIT_ENUM_MAX)];

extern IUG_HTR_MSK_ID_t         ext_iug_heater_property_msk_hgx_ava;
extern IUG_HTR_MSK_ID_t         ext_iug_heater_property_msk_ghi_ava;
extern IUG_HTR_MSK_ID_t         ext_iug_heater_property_msk_air_ava;
extern IUG_HTR_MSK_ID_t         ext_iug_heater_property_msk_wat_ava;
extern IUG_HEATER_UNIT_ENUM_t   ext_iug_heater_property_idx_hgx_ava[(IUG_HEATER_UNIT_ENUM_MAX)];
extern IUG_HEATER_UNIT_ENUM_t   ext_iug_heater_property_idx_air_ava[(IUG_HEATER_UNIT_ENUM_MAX)];
extern IUG_HEATER_UNIT_ENUM_t   ext_iug_heater_property_idx_wat_ava[(IUG_HEATER_UNIT_ENUM_MAX)];
extern IUG_HEATER_UNIT_ENUM_t   ext_iug_heater_property_idx_ghi_ava[(IUG_HEATER_UNIT_ENUM_MAX)];


void    IUG_gen_heater_cmd_create( IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl );
IUG_COMMAND_t IUG_gen_heater_cmd_evaluate_transition_heating( const IUG_HEATER_UNIT_ENUM_t idx_htr, const IUG_COMMAND_t cmd_req );
boolean IUG_gen_heater_cmd_trigger( IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl, 
                                    const IUG_COMMAND_t cmd_req, const IUG_UNIT_ID_t cmd_by_unit );
uint8_t IUG_gen_heater_comm_state_init( IUG_UNIT_CONTROL_HEADER_t * const p_gen_ctl, IUG_UNIT_HEATER_CONTROL_t * const p_htr_ctl );
uint8_t IUG_gen_heater_create_heater_name( const IUG_HEATER_UNIT_ENUM_t idx_htr, uint8_t * const htr_name, const uint8_t htr_name_max );
void    IUG_gen_heater_ctl_cyclic( const uint8_t idx_unit_tree );
IUG_UNIT_HEATER_CONTROL_t * IUG_gen_heater_ctl_test_ibn_htr_available( const uint8_t idx_unit_tree );
void    IUG_gen_heater_ctl_cyclic_independent( void );
void    IUG_gen_heater_ctl_init( const uint8_t idx_unit_tree );
void    IUG_gen_heater_ctl_init_on_cyclic( const IUG_HEATER_UNIT_ENUM_t idxhtrpty );
void    IUG_gen_heater_cyclic_services( IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl );
void    IUG_gen_heater_cyclic_services_activation( IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl );
void    IUG_gen_heater_cyclic_voltage_monitoring( IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl );
void    IUG_gen_heater_diag_webpar( IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl );
uint16_t IUG_gen_heater_diag_serve_unit_status( const IUG_UNITS_STA_SEL_MUX_t selector_mux, uint8_t * const p_can_msg );
void    IUG_gen_heater_handle_acknowlege( IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl );
void    IUG_gen_heater_handle_broadcast( IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl );
IUG_COMMAND_t  IUG_gen_heater_handle_cmd_misuse_test( IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl, const IUG_COMMAND_t cmd_req );
void    IUG_gen_heater_handle_cmd_misuse_cyclic( IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl );
void    IUG_gen_heater_handle_pending_internal( IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl );
void    IUG_gen_heater_handle_state_synchronization( IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl );
void    IUG_gen_heater_handle_termination( IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl );
boolean IUG_gen_heater_handle_WTT_connected( IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl );
IUG_DURATION_TIME_M_T   IUG_gen_heater_heating_time_calculate( IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl,
                                                               IUG_UNIT_HEATER_HEATING_TIME_CTL_t * const p_htr_tim,
                                                               const IUG_DURATION_TIME_M_T tim_max_heater );
void    IUG_gen_heater_heating_time_clr_final_duration( IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl, IUG_UNIT_HEATER_HEATING_TIME_CTL_t * const p_htr_tim );
void    IUG_gen_heater_heating_time_handle( IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl, IUG_UNIT_HEATER_HEATING_TIME_CTL_t * const p_htr_tim );
uint8_t IUG_gen_heater_test_heater_state( IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl, const TARGET_HTR_STATE_t state_test );
uint8_t IUG_gen_heater_test_swapped_id_status_data( const IUG_SITE_TARGET_ID_t tgt_id );
void    IUG_gen_heater_pending_ack_timeout( IUG_UNIT_HEATER_CONTROL_t * const p_ctl_htr );
void    IUG_gen_heater_process_heater_failure( IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl );
void    IUG_gen_heater_read_dtc_ctl( IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl );
void    IUG_gen_heater_read_id_evaluate( IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl );
void    IUG_gen_heater_read_status_evaluate( IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl );
void    IUG_gen_heater_read_status_ctl( IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl );
void    IUG_gen_heater_read_status_enable( IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl );
void    IUG_gen_heater_read_status_force( IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl );
void    IUG_gen_heater_read_status_test_err( IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl,
                                             const IUG_UNIT_READ_STATUS_CARRIER_t status_id,
                                             const IUG_UNIT_READ_STATUS_CARRIER_t err_flag,
                                             const enum_DTC_TABLE_IDX dtc_id );
IUG_UNIT_HEATER_CONTROL_t * IUG_gen_heater_set_access_pointer( const uint8_t idx_unit_tree, IUG_HEATER_UNIT_ENUM_t * const idx_heater_property );
void    IUG_gen_heater_stop_activity( IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl );
void    IUG_gen_heater_test_heater_category( IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl );
uint8_t IUG_gen_heater_test_heater_category_and_state( const IUG_UNIT_HEATER_STATE_CATEGORY_t category, const IUG_HEATER_STATE_NUMBER_t state_num );
void    IUG_gen_heater_test_heater_failure( IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl );
boolean IUG_gen_heater_test_heater_ventilating( IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl );
void    IUG_gen_heater_update_heater_property_fast( void );
void    IUG_gen_heater_update_heater_property_fast_wbus( void );

IUG_UNIT_HEATER_CONTROL_t * IUG_gen_heater_test_pointer_to_unit_ctl( IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl );



#endif 


