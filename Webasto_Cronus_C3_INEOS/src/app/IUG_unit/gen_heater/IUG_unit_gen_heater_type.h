#ifndef _IUG_UNIT_GEN_HEATER_TYPE_H_
#define _IUG_UNIT_GEN_HEATER_TYPE_H_



#define IUG_GEN_HEATER_SIGN_NAME_LEN        8u 


typedef enum
{
    IUG_HTR_SIGN_DEF_HEATER_AIR     =  0u,  
    IUG_HTR_SIGN_DEF_HEATER_WAT     =  1u,  

    IUG_HTR_SIGN_AT_3500            =  2u,  
    IUG_HTR_SIGN_TT_VEVO            =  3u,  
    IUG_HTR_SIGN_TT_Z_C             =  4u,  
    IUG_HTR_SIGN_AT_40_55           =  5u,  
    IUG_HTR_SIGN_AT_EVO_2000        =  6u,  
    IUG_HTR_SIGN_AT_2000STC         =  7u,  
    IUG_HTR_SIGN_TT_EVO_GEN_1       =  8u,  
    IUG_HTR_SIGN_TT_EVO_GEN_4       =  9u,  
    IUG_HTR_SIGN_TP_50              = 10u,  
    IUG_HTR_SIGN_TP_90              = 11u,  
    IUG_HTR_SIGN_TP_120             = 12u,  
    IUG_HTR_SIGN_WBUS_LAST          = 13u,  
    IUG_HTR_SIGN_GHI_FIRST          = 50u,  
    IUG_HTR_SIGN_GHI_DEF_AIR        = 51u,  
    IUG_HTR_SIGN_GHI_DEF_WAT        = 52u,  
    IUG_HTR_SIGN_GHI_HVH_AIR        = 53u,  
    IUG_HTR_SIGN_GHI_HVH_WAT        = 54u,  
    IUG_HTR_SIGN_GHI_GEN4_WAT       = 55u,  
    IUG_HTR_SIGN_GHI_LAST           = 56u,  
    IUG_HTR_SIGN_ENUM_MAX,                  
    IUG_HTR_SIGN_ENUM_FORCE_TYPE    = 0x7F  

} enum_IUG_UNIT_HEATER_SIGN_t;
typedef enum_IUG_UNIT_HEATER_SIGN_t   IUG_UNIT_HEATER_SIGN_t;


typedef struct
{
    IUG_UNIT_HEATER_SIGN_t      sign_id;                                    
    uint8_t                     sign_name[(IUG_GEN_HEATER_SIGN_NAME_LEN)];  

    IUG_SITE_TARGET_ID_t        tgt_id_def;     

} structure_IUG_GEN_HEATER_SIGN_DESCRIPTION_ENTRY_t;
typedef structure_IUG_GEN_HEATER_SIGN_DESCRIPTION_ENTRY_t     IUG_GEN_HEATER_SIGN_DESCR_ENTRY_t;


typedef struct
{
    IUG_SITE_TARGET_ID_t        tgt_id;         

    uint8_t                     eep_topic;      

    uint16_t                    eep_addr;       

    uint8_t                     eep_signal;     

} structure_IUG_GEN_HEATER_EEPROM_DESCRIPTION_ENTRY_t;
typedef structure_IUG_GEN_HEATER_EEPROM_DESCRIPTION_ENTRY_t     IUG_GEN_HEATER_EEP_DESCR_ENTRY_t;


extern const IUG_GEN_HEATER_EEP_DESCR_ENTRY_t       IUG_GEN_HEATER_EEP_DESCR[];
extern const uint8_t                                mgl_entries_IUG_GEN_HEATER_EEP_DESCR;


IUG_GEN_HEATER_EEP_DESCR_ENTRY_t  const * IUG_gen_heater_type_get_eeprom_description( const IUG_SITE_TARGET_ID_t tgt_id, const uint8_t eep_topic );
IUG_GEN_HEATER_SIGN_DESCR_ENTRY_t       * IUG_gen_heater_type_test_sign_id( const IUG_UNIT_HEATER_SIGN_t sign_id );
IUG_UNIT_HEATER_SIGN_t                    IUG_gen_heater_type_test_sign_name( uint8_t * const p_sign_name );


#endif 


