#ifndef __IUG_UNIT_GEN_TEMP_SENSOR_H_
#define __IUG_UNIT_GEN_TEMP_SENSOR_H_








void IUG_gen_temp_sensor_ctl_init( const uint8_t idx_unit_tree );

void IUG_gen_temp_env_init( const int16_t __packed* *const ptr_temp_val, const enum_ENV_TEMP_SENS temp_sens, funct_ptr_ibn_temp_sens* funct_get_temp);

void IUG_gen_temp_sensor_ctl_cyclic( const uint8_t idx_unit_tree );

static enum_ENV_ERR_TEMP_SENS IUG_gen_temp_sensor_get_temp( int16_t * const temp_value, const enum_ENV_TEMP_SENS temp_sens );


#endif 


