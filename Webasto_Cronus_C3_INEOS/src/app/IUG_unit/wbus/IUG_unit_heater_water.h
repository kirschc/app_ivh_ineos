#ifndef __IUG_UNIT_HEATER_WATER_H_
#define __IUG_UNIT_HEATER_WATER_H_



IUG_COMMAND_t   IUG_heater_water_cmd_evaluate( structure_IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl, const IUG_COMMAND_t cmd_req,
                                               IUG_GEN_CTL_DISPLAY_ID_t * const p_display_info );
void            IUG_heater_water_ctl_cyclic( const uint8_t idx_unit_tree );
void            IUG_heater_water_ctl_state( structure_IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl );
void            IUG_heater_water_ctl_init( const uint8_t idx_unit_tree );
void            IUG_heater_water_ctl_heating_aux( structure_IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl );
void            IUG_heater_water_ctl_heating_drive( structure_IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl );
void            IUG_heater_water_ctl_heating_handler( structure_IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl );
void            IUG_heater_water_ctl_heating_park( structure_IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl );
void            IUG_heater_water_ctl_heating_ventilation( structure_IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl );
void            IUG_heater_water_diag_mtx( structure_IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl );
boolean         IUG_heater_water_test_aut_aux_heating_condition( const uint8_t idx_htr_pty_wat );
boolean         IUG_heater_water_test_man_aux_heating_condition( const uint8_t idx_htr_pty_wat );


#endif 


