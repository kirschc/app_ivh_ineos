#ifndef __IUG_UNIT_THERMO_CONNECT_H_
#define __IUG_UNIT_THERMO_CONNECT_H_



IUG_COMMAND_t IUG_thermo_connect_cmd_pre_evaluate( const IUG_UNIT_ID_t unit_id,
                                                   const IUG_HEATER_UNIT_ENUM_t evaluate_for_htr_idx,
                                                   IUG_UNIT_COMMAND_REQUEST_t * const req_cmd_thermo_connect );
void    IUG_thermo_connect_ctl_cyclic( const uint8_t idx_unit_tree );
void    IUG_thermo_connect_ctl_init( const uint8_t idx_unit_tree );


#endif 

