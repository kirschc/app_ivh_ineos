#ifndef IUG_UNIT_T_LIN_R_H
#define IUG_UNIT_T_LIN_R_H










IUG_T_LIN_R_MSK_ID_t IUG_t_lin_r_available( void );


void IUG_t_lin_r_ctl_init( const uint8_t idx_unit_tree );

void IUG_t_lin_r_ctl_cyclic( const uint8_t idx_unit_tree );

int8_t IUG_t_lin_r_get_mapping_cfg_det( const uint8_t type_id_input_value, const uint8_t type_id_input_type );

#endif 


