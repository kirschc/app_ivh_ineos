#ifndef __IUG_UNIT_HEATER_AIR_H_
#define __IUG_UNIT_HEATER_AIR_H_



IUG_COMMAND_t   IUG_heater_air_cmd_evaluate( structure_IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl, const IUG_COMMAND_t cmd_req,
                                             IUG_GEN_CTL_DISPLAY_ID_t * const p_display_info );
void            IUG_heater_air_ctl_cyclic( const uint8_t idx_unit_tree );
void            IUG_heater_air_ctl_state( structure_IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl );
void            IUG_heater_air_ctl_init( const uint8_t idx_unit_tree );
void            IUG_heater_air_ctl_heating_park( structure_IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl );
void            IUG_heater_air_ctl_heating_ventilation( structure_IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl );
void            IUG_heater_air_diag_mtx( structure_IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl );
void            IUG_heater_air_diag_webpar_level_air( structure_IUG_UNIT_HEATER_CONTROL_t * const p_unt_ctl );
IUG_PHYS_AIR_PRESSURE_HPA_T IUG_heater_air_get_air_pressure_AT2000( void );


#endif 


