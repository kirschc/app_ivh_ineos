#ifndef __IUG_UNIT_MULTI_CTL_H_
#define __IUG_UNIT_MULTI_CTL_H_



IUG_COMMAND_t   IUG_multi_ctl_cmd_pre_evaluate( const IUG_UNIT_ID_t unit_id, 
                                                const IUG_HEATER_UNIT_ENUM_t evaluate_for_htr_idx,  
                                                IUG_UNIT_COMMAND_REQUEST_t * const req_multi_ctl );
void            IUG_multi_ctl_ctl_cyclic( const uint8_t idx_unit_tree );
void            IUG_multi_ctl_ctl_init( const uint8_t idx_unit_tree );



#endif 

