#ifndef __IUG_UNIT_SIMULATION_H_
#define __IUG_UNIT_SIMULATION_H_

#ifdef IUG_UNT_SIMULATION_EN


#define IUG_UNT_SIMULATION_CMD_NONE               0x00000000u
#define IUG_UNT_SIMULATION_CMD_OFF_AIR            0x00000001u 
#define IUG_UNT_SIMULATION_CMD_OFF                0x00000002u 
#define IUG_UNT_SIMULATION_CMD_HEATING_AIR        0x00000010u 
#define IUG_UNT_SIMULATION_CMD_HEATING            0x00000020u 
#define IUG_UNT_SIMULATION_CMD_AUX_HEATING        0x00000200u 
#define IUG_UNT_SIMULATION_CMD_BOOST_HEATING_AIR  0x00001000u 
#define IUG_UNT_SIMULATION_CMD_ECO_HEATING_AIR    0x00004000u 
#define IUG_UNT_SIMULATION_CMD_VENTILATION_AIR    0x00010000u 
#define IUG_UNT_SIMULATION_CMD_VENTILATION        0x00020000u 



void    IUG_simulation_ctl_cyclic( const uint8_t idx_unit_tree );
void    IUG_simulation_ctl_init( const uint8_t idx_unit_tree );



#endif 
#endif 


