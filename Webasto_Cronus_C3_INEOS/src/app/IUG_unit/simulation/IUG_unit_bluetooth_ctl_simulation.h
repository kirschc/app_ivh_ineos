#ifndef __IUG_UNIT_BLUETOOTH_CTL_SIMULATION_H_
#define __IUG_UNIT_BLUETOOTH_CTL_SIMULATION_H_



#ifdef IUG_BLUETOOTH_SIMULATION

#define IUG_BLESIM_CMD_NONE               0x00u
#define IUG_BLESIM_CMD_WR_VIA_REC_BUFFER  0x01u 
#define IUG_BLESIM_CMD_RD_VIA_REC_BUFFER  0x02u 
#define IUG_BLESIM_CMD_OFF_AIR            0x04u
#define IUG_BLESIM_CMD_HEATING_AIR        0x08u
#define IUG_BLESIM_CMD_OFF_WAT            0x20u
#define IUG_BLESIM_CMD_HEATING_WAT        0x40u

#endif



#ifdef IUG_BLUETOOTH_SIMULATION
extern boolean      IUG_bluetooth_b_ret;
extern uint32_t     IUG_bluetooth_sim_rec_can_id;
#endif


#ifdef IUG_BLUETOOTH_SIMULATION
void        IUG_bluetooth_sim_cyclic( void );
#endif


#endif 


