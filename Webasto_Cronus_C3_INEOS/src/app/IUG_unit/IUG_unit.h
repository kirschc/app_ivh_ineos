#ifndef __IUG_UNIT_H_
#define __IUG_UNIT_H_


extern IUG_UNIT_PROPERTY_t                  ext_iug_unit_property[(IUG_UNT_ENUM_MAX)];
extern IUG_UNIT_PROPERTY_NVM_t              IUG_UNIT_PROPERTY_NVM[(IUG_UNT_ENUM_MAX)];
extern const IUG_UNIT_PROPERTY_NVM_t        IUG_UNIT_PROPERTY_NVM_DEF[];
extern IUG_WBUS_MEMBER_PROPERTY_t           ext_iug_wbus_member_property[(WBUS_MBR_ENUM_MAX)];


void            IUG_unit_cyclic( void );
void            IUG_unit_cyclic_preparation( void );
void            IUG_unit_cyclic_preparation_fast( void );
void            IUG_unit_cyclic_preparation_fast_access( void );
void            IUG_unit_init( void );
void            IUG_unit_init_high_priority( void );
void            IUG_unit_member_WTT_cyclic_test( void );
IUG_COMMAND_t   IUG_unit_simplify_OFF_cmd( const IUG_COMMAND_t cmd_req );
void            IUG_comm_set_RX_reserved_cnt( const IUG_WBUS_INS_MSK_t wbus_ins_msk, const uint16_t cnt_val );
void            IUG_comm_set_TX_reserved_cnt( const IUG_WBUS_INS_MSK_t wbus_ins_msk, const uint16_t cnt_val );
uint8_t         IUG_unit_test_wbus_member( const enum_WBUS_MEMBER_t wbus_mbr );
void            IUG_unit_set___p_htr_ctl( void );


#endif 

