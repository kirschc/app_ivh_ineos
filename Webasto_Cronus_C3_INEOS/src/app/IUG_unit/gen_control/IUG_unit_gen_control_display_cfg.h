#ifndef __IUG_UNIT_GEN_CONTROL_DISPLAY_CFG_H__
#define __IUG_UNIT_GEN_CONTROL_DISPLAY_CFG_H__







typedef struct
{
    IUG_GEN_CTL_DISPLAY_ID_t    display_id;         
    WBUS_UNIT_DTC_ID_t          unit_dtc_id;        
    IUG_CMD_BTN_PATTERN_t       cmd_btn_pattern;    

} structure_IUG_GENERIC_CONTROL_DISPLAY_CFG_t;
typedef structure_IUG_GENERIC_CONTROL_DISPLAY_CFG_t     IUG_GEN_CTL_DISPLAY_CFG_t;


IUG_GEN_CTL_DISPLAY_CFG_t const * IUG_gen_control_display_cfg_get_pointer_entry( const IUG_GEN_CTL_DISPLAY_ID_t display_id );



#ifdef IUG_WBUS_UNIT_READ_DTC_ADVANCED_DISPLAY_EN
    #define IUG_GEN_CTL_DISPLAY_CONFIGURATION \
    { \
        \
        {(GENCTLDPY_NONE)                   ,(WBUS_UNIT_DTC_NONE)                   ,(CMD_BTN_PAT_NONE)                 },\
        {(GENCTLDPY_CMD_REFUSED)            ,(WBUS_UNIT_DTC_CMD_REFUSED)            ,(CMD_BTN_PAT_NONE)                 },\
        {(GENCTLDPY_CMD_DOUBLED)            ,(WBUS_UNIT_DTC_CMD_DOUBLED)            ,(CMD_BTN_PAT_HMI_ERROR_GENERAL)    },\
        {(GENCTLDPY_HEATER_SWITCH_OFF)      ,(WBUS_UNIT_DTC_HEATER_SWITCH_OFF)      ,(CMD_BTN_PAT_HMI_ERROR_GENERAL)    },\
        {(GENCTLDPY_NO_COND_HEATING)        ,(WBUS_UNIT_DTC_NO_COND_HEATING)        ,(CMD_BTN_PAT_HMI_ERROR_GENERAL)    },\
        {(GENCTLDPY_NO_COND_VENTILATING)    ,(WBUS_UNIT_DTC_NO_COND_VENTILATING)    ,(CMD_BTN_PAT_HMI_ERROR_GENERAL)    },\
        \
        {(GENCTLDPY_ENUM_MAX)               ,(WBUS_UNIT_DTC_UNKNOWN)                ,(CMD_BTN_PAT_HMI_ERROR_GENERAL)    },\
    }
#else
    #define IUG_GEN_CTL_DISPLAY_CONFIGURATION \
    { \
        \
        {(GENCTLDPY_NONE)                   ,(WBUS_UNIT_DTC_NONE)                   ,(CMD_BTN_PAT_NONE)                 },\
        {(GENCTLDPY_CMD_REFUSED)            ,(WBUS_UNIT_DTC_NONE)                   ,(CMD_BTN_PAT_NONE)                 },\
        {(GENCTLDPY_CMD_DOUBLED)            ,(WBUS_UNIT_DTC_NONE)                   ,(CMD_BTN_PAT_HMI_ERROR_GENERAL)    },\
        {(GENCTLDPY_HEATER_SWITCH_OFF)      ,(WBUS_UNIT_DTC_NONE)                   ,(CMD_BTN_PAT_HMI_ERROR_GENERAL)    },\
        {(GENCTLDPY_NO_COND_HEATING)        ,(WBUS_UNIT_DTC_NONE)                   ,(CMD_BTN_PAT_HMI_ERROR_GENERAL)    },\
        {(GENCTLDPY_NO_COND_VENTILATING)    ,(WBUS_UNIT_DTC_NONE)                   ,(CMD_BTN_PAT_HMI_ERROR_GENERAL)    },\
        \
        {(GENCTLDPY_ENUM_MAX)               ,(WBUS_UNIT_DTC_UNKNOWN)                ,(CMD_BTN_PAT_HMI_ERROR_GENERAL)    },\
    }
#endif


#endif 

