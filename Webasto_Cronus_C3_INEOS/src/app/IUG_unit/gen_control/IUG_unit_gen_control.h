#ifndef __IUG_UNIT_GEN_CONTROL_H_
#define __IUG_UNIT_GEN_CONTROL_H_


#define IUG_GEN_CTL_UPDATE_ENTRIES      4u



void            IUG_gen_control_cmd_event( IUG_UNIT_CONTROL_HEADER_t * const p_unt_ctl );
uint8_t         IUG_gen_control_comm_state_init( IUG_UNIT_EXT_COM_INP_t * const p_ext_inp );
boolean         IUG_gen_control_decide_cmd_parameter( const enum_IUG_UNIT_ID_t unit_id, const IUG_COMMAND_t unit_cmd, IUG_COMMAND_t * const cmd_iug, 
                                                      IUG_PHYS_TIME_1M_T * const cmd_period, IUG_PHYS_TEMP_1_1DEG_T * const cmd_setpoint );
IUG_COMMAND_t   IUG_gen_control_decide_heating_ventilating( const IUG_UNIT_ID_t unit_id, IUG_COMMAND_t cmd_req,
                                                            const IUG_HEATER_UNIT_ENUM_t evaluate_for_htr_idx,
                                                            const API_USER_DEVICE_ID_t   evaluate_for_udv_idx );
uint16_t        IUG_gen_control_diag_serve_unit_status( const IUG_UNITS_STA_SEL_MUX_t selector_mux, uint8_t * const p_can_msg );
uint16_t        IUG_gen_control_get_setpoint_via_mode_or_level_bts_air( const enum_parameter_id mode );
void            IUG_gen_control_handle_acknowlege( IUG_UNIT_CONTROL_HEADER_t * const p_unt_ctl );
void            IUG_gen_control_handle_pending_ext( IUG_UNIT_CONTROL_HEADER_t * const p_unt_ctl );
void            IUG_gen_control_read_id( IUG_UNIT_CONTROL_HEADER_t * const p_unt_ctl );
void            IUG_gen_control_test_failure( IUG_UNIT_CONTROL_HEADER_t * const p_unt_ctl );
IUG_UNIT_CONTROL_HEADER_t *IUG_gen_control_test_pointer_unit_ctl_data( IUG_UNIT_CONTROL_HEADER_t * const p_unt_ctl );
void            IUG_gen_control_display_update( const enum_IUG_TASK_ID_t task_id );
void            IUG_gen_control_display_broadcast( void );
void            IUG_gen_control_display_init( const boolean initialize_all );
void            IUG_gen_control_display_handshake( const enum_IUG_TASK_ID_t task_id );
void            IUG_gen_control_multi_ctl_settling_trigger( IUG_UNIT_MULTI_CTL_CONTROL_t * const p_unt_ctl );
uint8_t         IUG_gen_control_multi_ctl_settling_test_msg( struct_WBUS_MSG_DESCRIPTION_CTL_t * const wbus_msg_ctl_ptr );
uint8_t         IUG_GenCtl_update_heater_binding( const IUG_UNIT_ID_t ctl_unit_id );


#endif 

