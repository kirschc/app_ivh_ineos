#ifndef __IUG_UNIT_GEN_CONTROL_LOCK_H_
#define __IUG_UNIT_GEN_CONTROL_LOCK_H_

void    IUG_gen_control_lock_transport_cyclic( structure_IUG_UNIT_CONTROL_HEADER_t * const p_unt_ctl );
void    IUG_gen_control_lock_transport_init( IUG_CMD_UNIT_LOCK_CONTROL_t * const p_lck_ctl );



#endif 


