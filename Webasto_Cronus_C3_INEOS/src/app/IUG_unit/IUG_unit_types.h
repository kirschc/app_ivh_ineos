#ifndef __IUG_UNIT_TYPES_H_
#define __IUG_UNIT_TYPES_H_

#include "IUG_unit_constant_t_lin_r.h"





typedef struct
{
    uint32_t            unit_header_pattern;

    IUG_UNIT_ID_t       unit_id;            
    IUG_UNIT_FLAG_t     unit_flg;           
    WBUS_MEMBER_t       wbus_mbr_def;       
    enum_IUG_TASK_ID_t  task_id_wbus_proc;  

} structure_IUG_UNIT_HEADER_CONFIGURATION_t;
typedef structure_IUG_UNIT_HEADER_CONFIGURATION_t   IUG_UNIT_HEADER_CONFIGURATION_t;


typedef struct
{
    IUG_COMMAND_t       cmd_trg;        
    IUG_UNIT_ID_t       cmd_by_unit_id; 

    IUG_PHYS_TEMP_1_1DEG_T  cmd_SETPOINT;               
    IUG_PHYS_TIME_1M_T      cmd_ACTIVATION_PERIOD;      

} structure_IUG_UNIT_COMMAND_REQUEST_TRIGGER_t;
typedef structure_IUG_UNIT_COMMAND_REQUEST_TRIGGER_t    IUG_UNIT_COMMAND_REQUEST_TRIGGER_t;


typedef struct
{
    IUG_COMMAND_t       cmd_req;            
    IUG_COMMAND_t       cmd_cfm;            
    IUG_UNIT_ID_t       cmd_via_unit_id;    

    uint8_t             cmd_mode;           

} structure_IUG_UNIT_COMMAND_REQUEST_CONTROL_t;
typedef structure_IUG_UNIT_COMMAND_REQUEST_CONTROL_t    IUG_UNIT_COMMAND_REQUEST_CONTROL_t;


typedef struct
{
    IUG_UNIT_COMMAND_REQUEST_TRIGGER_t  trg;

    IUG_UNIT_COMMAND_REQUEST_CONTROL_t  ctl;

} IUG_UNIT_COMMAND_REQUEST_t;


typedef struct
{
    IUG_UNIT_COMMAND_REQUEST_t          *p_cmd_pnd;     

    IUG_COMMAND_t                       cmd_req_prv;    

} IUG_UNIT_COMMAND_REQUEST_PENDING_t;


typedef struct
{
    IUG_UNIT_ID_t           unit_id;    
    enum_IUG_TASK_ID_t      task_id;    

    IUG_COMMAND_t           (*cmd_pre_evaluate)( const IUG_UNIT_ID_t unit_id, const IUG_HEATER_UNIT_ENUM_t evaluate_for_htr_idxt, 
                                                 IUG_UNIT_COMMAND_REQUEST_t * const req_cmd_xxx );

} IUG_UNIT_COMMAND_REQUEST_HANDLER_t;


typedef struct
{
    uint8_t     mode_meta;      
    uint8_t     mode;           
    uint8_t     trm_rcp_SE;     

    uint8_t     cmd_propagating_dwell_cnt;    

    IUG_PHYS_TEMP_1_1DEG_T  SETPOINT;           
    IUG_PHYS_TIME_1M_T      ACTIVATION_PERIOD;  

    #ifdef IUG_COMSTA_TRM_WAIT_ACK_FOR_CMD_ACCELERATION
    uint8_t     comm_ack_inj_req_cmd_burst;     
    #endif

} structure_IUG_UNIT_EXT_REQ_INP_t;
typedef structure_IUG_UNIT_EXT_REQ_INP_t    IUG_UNIT_EXT_REQ_INP_t;


typedef struct
{
    uint8_t     mode_meta;          
    uint8_t     mode;               
    uint8_t     nak_mode;           
    uint8_t     nak_code;
    uint8_t     ack_timeout_cnt;    

    IUG_PHYS_TEMP_1_1DEG_T  SETPOINT;           
    IUG_PHYS_TIME_1M_T      ACTIVATION_PERIOD;  

} structure_IUG_UNIT_EXT_ACK_FOR_REQ_INP_t;
typedef structure_IUG_UNIT_EXT_ACK_FOR_REQ_INP_t    IUG_UNIT_EXT_ACK_FOR_REQ_INP_t;


typedef struct
{
    uint8_t             mode_meta;          
    uint8_t             mode;               

    IUG_QUEUE_KEY_t     req_key_ptr_data;   

} structure_IUG_UNIT_EXT_REQ_OUT_t;
typedef structure_IUG_UNIT_EXT_REQ_OUT_t    IUG_UNIT_EXT_REQ_OUT_t;


typedef struct
{
    uint8_t     mode_meta;              
    uint8_t     mode;                   
    uint8_t     nak_code;
    uint8_t     trm_rcp_SE;             
    uint16_t    ack_timeout_dly_cnt;    
    uint8_t     ack_timeout_cnt;        

    uint8_t     ack_received;           
    uint8_t     ack_evaluated;          

    IUG_PHYS_TIME_1M_T  REM_HEATING_TIME;   

} structure_IUG_UNIT_EXT_ACK_FOR_REQ_OUT_t;
typedef structure_IUG_UNIT_EXT_ACK_FOR_REQ_OUT_t    IUG_UNIT_EXT_ACK_FOR_REQ_OUT_t;


typedef struct
{
    IUG_COMM_STATE_BAG_t                    com_state;

    IUG_UNIT_EXT_REQ_INP_t                  req;        
    IUG_UNIT_EXT_ACK_FOR_REQ_INP_t          ack;        

} structure_IUG_UNIT_EXT_COM_INP_t;
typedef structure_IUG_UNIT_EXT_COM_INP_t    IUG_UNIT_EXT_COM_INP_t;


typedef struct
{
    IUG_COMM_STATE_BAG_t                    com_state;

    IUG_UNIT_EXT_REQ_OUT_t                  req;        
    IUG_UNIT_EXT_ACK_FOR_REQ_OUT_t          ack;        

} structure_IUG_UNIT_EXT_COM_OUT_t;
typedef structure_IUG_UNIT_EXT_COM_OUT_t    IUG_UNIT_EXT_COM_OUT_t;


typedef enum
{
    GENCTLDPY_NONE = 0u,                    
    GENCTLDPY_CMD_REFUSED,                  
    GENCTLDPY_CMD_DOUBLED,                  
    GENCTLDPY_HEATER_SWITCH_OFF,            
    GENCTLDPY_NO_COND_HEATING,              
    GENCTLDPY_NO_COND_VENTILATING,          
    GENCTLDPY_ENUM_MAX,                     
    GENCTLDPY_ENUM_FORCE_TYPE = 0x7F        

} IUG_GEN_CTL_DISPLAY_ID_t;


typedef struct
{
    IUG_UNIT_RD_WR_INTERFACE_OBJECT_t   rdwr_obj;       

    uint16_t                        retry_cnt;          

    IUG_UNIT_READ_ID_CARRIER_t      read_id_rsp;        

    IUG_READ_ID_DATA_ADDRESS_CFG_t  *p_data;            
    uint8_t                         data_entry;         

} struct_IUG_UNIT_READ_ID_t;
typedef struct_IUG_UNIT_READ_ID_t   IUG_UNIT_READ_ID_t;


typedef struct
{
    uint8_t     id;                 

    void        *p_st_data;         

} struct_IUG_READ_STATUS_DATA_ADDRESS_CFG_t;

typedef struct_IUG_READ_STATUS_DATA_ADDRESS_CFG_t   IUG_READ_STATUS_DATA_ADDRESS_CFG_t;


typedef struct
{
    IUG_UNIT_RD_WR_INTERFACE_OBJECT_t   rdwr_obj;       


    uint16_t    keep_active_cnt;        
    uint8_t     keep_active_evt_old;    

    uint8_t     err_cnt[(IUG_UNIT_READ_STATUS_NUM_STATUS_MAX)]; 
    uint16_t    activity_ctl_cnt;       
    uint16_t    cycle_cnt_fast_cnt;     

    IUG_READ_STATUS_DATA_ADDRESS_CFG_t      *p_data;        
    uint8_t                                 data_entry;     

} struct_IUG_UNIT_READ_STATUS_t;
typedef struct_IUG_UNIT_READ_STATUS_t       IUG_UNIT_READ_STATUS_t;


typedef struct
{
    IUG_UNIT_COMP_TEST_CARRIER_t    ena;        
    IUG_UNIT_COMP_TEST_CARRIER_t    req;        
    IUG_UNIT_COMP_TEST_CARRIER_t    cfm;        
    IUG_UNIT_COMP_TEST_CARRIER_t    rsp;        
    IUG_UNIT_COMP_TEST_CARRIER_t    err;        
    IUG_UNIT_COMP_TEST_CARRIER_t    mon_flg;    
    uint16_t                        mon_cnt;    

    IUG_COMM_STATE_BAG_t            com_state;          

} IUG_UNIT_COMP_TEST_t;


typedef struct
{
    IUG_COMM_STATE_BAG_t    com_state;
    uint16_t                eep_rdwr_timeout_ms;

    uint8_t         eep_addr_hi;
    uint8_t         eep_addr_lo;
    uint8_t         eep_data;
    uint8_t         eep_rd_trg;         
    uint8_t         eep_rd_ack;         
    uint8_t         eep_rd_err;         

} IUG_UNIT_EEROM_RD_WR_t;


typedef struct
{
    uint16_t    pending_timeout_cnt; 

    uint8_t     mode_meta;          
    uint8_t     mode;               
    uint8_t     cmd;                
    uint8_t     ctl;                
    uint8_t     trm_rcp_SE;         

    uint8_t     pending_ignore;     

} IUG_UNIT_PND_REQ_INP_t;


typedef struct
{
    uint16_t            pending_grid_cnt;   	

    uint8_t             pending_grid_trg;   	

    IUG_QUEUE_KEY_t     req_key_ptr_data;   	

    uint8_t             rep_cnt;            	
    uint8_t             rep_grid_ms;        	

} IUG_UNIT_PND_REQ_OUT_t;


typedef struct
{
    uint8_t     mode;                   
    uint8_t     ctl;                    

    uint16_t    ack_timeout_dly_cnt;    
    uint8_t     ack_timeout_cnt;        

} IUG_UNIT_PND_ACK_FOR_REQ_OUT_t;


typedef struct
{
    IUG_UNIT_PND_REQ_INP_t            req;

} IUG_UNIT_PND_COM_INP_t;


typedef struct
{
    IUG_COMM_STATE_BAG_t                    com_state;

    IUG_UNIT_PND_REQ_OUT_t                  req;
    IUG_UNIT_PND_ACK_FOR_REQ_OUT_t          ack;

} IUG_UNIT_PND_COM_OUT_t;


typedef struct
{
    uint8_t     lock_state;                 
    uint8_t     timeout_read_lock_state;    
    uint8_t     htr_failure;                
    uint8_t     cmd_mode_msg;               

} IUG_CMD_UNIT_LOCK_CONTROL_t;


typedef struct
{
    uint8_t     unit_dtc_active;    

    uint8_t     drc_error;          
    uint8_t     drc_inhibition;     
    uint8_t     drc_limp_home;      

    uint8_t     com_error;          

    IUG_HTR_MSK_ID_t        htr_drc_error;
    IUG_HTR_MSK_ID_t        htr_drc_inhibition;
    IUG_HTR_MSK_ID_t        htr_drc_limp_home;

    IUG_USRDEV_MSK_ID_t     udv_drc_error;
    IUG_USRDEV_MSK_ID_t     udv_drc_inhibition;
    IUG_USRDEV_MSK_ID_t     udv_drc_limp_home;


    enum_DTC_TABLE_IDX  err_due_DTC[(IUG_ERRHDL_ERR_DUE_DTC_ARY_SIZE)];

} IUG_UNIT_ERROR_HANDLER_t;


typedef struct
{
    IUG_UNIT_HEADER_CONFIGURATION_t     *hdr_cfg;           
    IUG_UNIT_FLAG_t                     unit_flg_ctl;       

    IUG_UNIT_COMMAND_REQUEST_t          cmd_req;            

    IUG_UNIT_ERROR_HANDLER_t            err_hdl;            

    WBUS_MEMBER_t                       wbus_mbr;           

    enum_IUG_TASK_ID_t                  task_id_wbus_proc;  

    IUG_UNIT_SIGNAL_ID_t                unit_signal;        

    IUG_UNIT_EXT_COM_INP_t              *extinp;     
    IUG_UNIT_EXT_COM_OUT_t              *extout;     
    IUG_UNIT_PND_COM_INP_t              *pndinp;     
    IUG_UNIT_PND_COM_OUT_t              *pndout;     

    IUG_UNIT_SPEC_DATA_t                *spec_data;  

    IUG_UNIT_READ_ID_t                  *read_id;    

    IUG_COMM_CASC_CTL_t                 *comm_casc;

    IUG_UNT_USER_API_UNIT_DATA_CONTROL_t    *api_unit_data;

} structure_IUG_UNIT_CONTROL_HEADER_t;
typedef structure_IUG_UNIT_CONTROL_HEADER_t     IUG_UNIT_CONTROL_HEADER_t;


typedef struct
{
    IUG_HTR_MSK_ID_t                gen_ctl_bound_to_htr;
    IUG_HTR_MSK_ID_t                nwb_ctl_bound_to_htr;
    IUG_USRDEV_MSK_ID_t             gen_ctl_bound_to_udv;
    IUG_USRDEV_MSK_ID_t             nwb_ctl_bound_to_udv;

    uint8_t                         gen_ctl_bound_to_diff_sink;

    uint8_t                         nwb_ctl_bound_to_diff_sink;

    uint8_t                         all_ctl_bound_to_diff_sink;

} IUG_UNIT_COMMUNICATION_CTL_t;


typedef struct
{
    IUG_BRDCST_ID_t     broadcast_trg;              
    uint8_t             broadcast_data;             

    uint8_t             broadcast_on_delay_cnt;     

    IUG_BRDCST_ID_t     broadcast_dpy_old;                             
    #ifdef IUG_BROADCAST_WBUS_INS_HG_EN
    IUG_BRDCST_ID_t     broadcast_htr_old[(IUG_HEATER_UNIT_ENUM_MAX)]; 
    #endif

    IUG_BRDCST_ID_t     ins_inhibit[(WBUS_INS_ENUM_MAX)];   

    IUG_BRDCST_INDICATION_t     broadcast_ind;

    uint8_t             webpar_broadcast_act;       

} IUG_UNIT_BROADCAST_CONTROL_t;


typedef struct
{
    IUG_PHYS_AIR_PRESSURE_HPA_T air_pressure;               
    IUG_PHYS_TEMP_1_1DEG_T      external_temp;              
    IUG_PHYS_TEMP_1_1DEG_T      obd_eng_cool_temp;          
    uint16_t                    obd_eng_rpm;                
    IUG_PHYS_TEMP_1_1DEG_T      webpar_out_temp;            
    IUG_PHYS_TEMP_1_1DEG_T      webpar_eng_cool_off_temp;   
    IUG_PHYS_TEMP_1_1DEG_T      webpar_eng_cool_on_temp;    

    uint8_t     external_temp_time_limit;
    uint8_t     obd_eng_rpm_time_limit;
    uint8_t     obd_eng_cool_time_limit;
    uint8_t     sys_tim_sec_grid_old;

    uint8_t     can_diag_flg;

    uint8_t     air_press_bc_trg;

    #ifdef IUG_DEV_ENVIRONMENTAL_SIMULATION_EN
    int32_t     dev_external_temp;
    int32_t     dev_obd_eng_rpm;
    int32_t     dev_obd_eng_cool_temp;
    #endif

} structure_IUG_UNIT_GENERAL_SIGNALS_t;


typedef struct
{
    IUG_UNIT_HEADER_CONFIGURATION_t   unit_header_cfg;

} structure_IUG_UNIT_IUG_CONFIGURATION_t;


typedef struct
{
    uint8_t           mrs_part_num[12u];    
    uint8_t           webasto_part_num[8u]; 
    enum_IUG_VARIANTS webasto_par_enum;     

} structure_IUG_UNIT_IUG_ID_t;

typedef struct
{
    uint8_t           webasto_part_num_zeroth_modification_date[8u];    
    uint8_t           webasto_part_num_first_modification_date[8u];     
    uint8_t           webasto_part_num_second_modification_date[8u];    

} structure_IUG_UNIT_IUG_ID_mapping;

typedef struct
{
    uint8_t           modification_date[8u];            
} structure_IUG_UNIT_IUG_Modification_Date;

typedef struct
{
    uint8_t     evt_aut_aux_heating_off;
    uint8_t     evt_aut_aux_heating_on;
    uint8_t     aut_aux_heating_on_locked;      
    uint8_t     cnt_aut_aux_heating_on_period;  
    uint8_t     cnt_aut_aux_heating_on_attempt; 

} struct_HG_AUX_HEATING_STATUS_t;
typedef struct_HG_AUX_HEATING_STATUS_t  HG_AUX_HEATING_ST_t;


typedef struct
{
    TARGET_HTR_STATE_t      HG_STATE_VIRT;              
    TARGET_HTR_STATE_t      HG_STATE_VIRT_last;         

    TARGET_HTR_STATE_t      virt_vent_hg_state_old;     
    uint8_t         virt_vent_emul_progress;    

} struct_HG_VIRT_VENTILATING_ST_t;
typedef struct_HG_VIRT_VENTILATING_ST_t  HG_VIRT_VENTILATING_ST_t;


typedef struct
{
    uint8_t     dummy;
} struct_HG_AIR_HEATING_STATUS_t;
typedef struct_HG_AIR_HEATING_STATUS_t  HG_AIR_HEATING_ST_t;


typedef struct
{
    TARGET_HTR_STATE_t                  TGT_HTR_STATE;          

    IUG_HEATER_STATE_NUMBER_t           HG_BETRIEBSZUSTAND;     
    IUG_UNIT_HEATER_STATE_CATEGORY_t    HG_CATEGORY;            

    IUG_HEATER_TERM_FLAG_t              termination_flag;       
    IUG_HEATER_TERM_FLAG_t              termination_flag_hist[(IUG_HETRTRMFLG_HISTORY_MAX)];    

    uint8_t                             HG_FAILURE_STFL;        
    uint8_t                             HG_FAILURE_HGVP;        
    uint8_t                             HG_FAILURE_UEHFL;       
    int16_t                             COOLANT_TEMP;           

    uint16_t                            hg_inactive_coverage_cnt;   

    struct
    {
        IUG_HEATER_UNIT_ENUM_t          idx_htr;                    
        IUG_HEATER_TERM_FLAG_t          termination_flag_old;       

        TARGET_HTR_STATE_t              TGT_HTR_STATE_last;         
        uint8_t                         cat_idx_last_found;         

        uint8_t                         hg_inactivity_delay_cnt;    
    } msc;

} HTR_META_STATE_t;


typedef union
{
    union
    {
        struct
        {
            HG_AIR_HEATING_ST_t     dummy;
        } air;  

        struct
        {
            HG_AUX_HEATING_ST_t         AUX_HEATING_ST;
            HG_VIRT_VENTILATING_ST_t    VIRT_VENT_ST;
        } wat;  

    } uni;
} union_HG_VARIOUS_HEATER_STATUS_t;
typedef union_HG_VARIOUS_HEATER_STATUS_t    HG_VAR_HEATER_ST_t;


typedef struct
{
    IUG_UNIT_CONTROL_HEADER_t   gen_hdr;

    IUG_STATE_t                 iug_state;                  

    union
    {
        IUG_TARGET_STATE_t      tgt_state;                  
    }                           tgt_state_hgX[(IUG_HEATER_UNIT_ENUM_MAX)];

    uint8_t                     tgt_state_hgX_any_active;   

    IUG_PHYS_TIME_1M_T          FINAL_DURATION;     

    IUG_PHYS_TIME_1M_T          FINAL_DURATION_HGX[(IUG_HEATER_UNIT_ENUM_MAX)];             

    IUG_DURATION_TIME_M_T       IUG_REMAINING_FINAL_DURATION[(IUG_HEATER_UNIT_ENUM_MAX)];   

    IUG_UNIT_COMMUNICATION_CTL_t            comm_ctl;

    IUG_UNIT_BROADCAST_CONTROL_t            broadcast;

    structure_IUG_UNIT_GENERAL_SIGNALS_t    gps;            

    uint16_t                    keep_alive_cnt;             

    uint16_t                    keep_alive_cnt_sleep;       

    uint16_t                    ignore_off_due_refused_cnt; 

    uint8_t                     cmd_OFF_burst_on_fault;     
    uint16_t                    cmd_OFF_burst_on_fault_grid;

    uint8_t                     event_flag;                 

    uint8_t                     aux_drv_heating_flag;       

    struct
    {
        IUG_HEATER_UNIT_ENUM_t          test_serial_number_cur_HGx;
        IUG_HEATER_UNIT_ENUM_t          test_serial_number_old_HGx;
        IUG_HTR_MSK_ID_t                test_serial_number_req_HGx;
        IUG_HTR_MSK_ID_t                test_serial_number_rsp_HGx;
        IUG_HTR_MSK_ID_t                test_serial_number_tst_HGx;         
        uint8_t                         test_serial_number_tim_sec_cnt;     

        IUG_TARGET_STATE_t              tgt_state_wat_ventilation; 

        uint16_t                        test_comm_failure_cnt;

        uint8_t                         cmd_misuse_failure;         

        IUG_HEATER_UNIT_ENUM_t          dtc_htr_idx_cur;            

        IUG_GEN_CTL_DISPLAY_ID_t        display_info;               

    } msc;

} structure_IUG_UNIT_IUG_CONTROL_t;


typedef struct
{
    IUG_UNIT_HEADER_CONFIGURATION_t     unit_header_cfg;

} structure_IUG_UNIT_HEATER_CONFIGURATION_t;
typedef structure_IUG_UNIT_HEATER_CONFIGURATION_t   IUG_UNIT_HEATER_CONFIGURATION_t;


typedef struct
{
    IUG_PHYS_TIME_1M_T          ACTIVATION_PERIOD;      

    IUG_PHYS_TIME_1M_T          FINAL_DURATION;         

    IUG_PHYS_TIME_1M_T          TIME_DURATION;          

    IUG_PHYS_TEMP_1_1DEG_T      SETPOINT;               

    IUG_PHYS_TIME_1M_T          DEFAULT_DURATION;       

    IUG_PHYS_TIME_1M_T          MAX_HEATING_TIME;       

    uint8_t                     minute_before_elapsed;              
    uint8_t                     suppress_elapsed_monitoring_cnt;    

    IUG_WEBPAR_TIME_t           TIME_ACTIVATION;        

    uint8_t                     time_1min_cnt;          
    uint8_t                     time_1min_cnt_sec_grid; 

} structure_IUG_UNIT_HEATER_HEATING_TIME_CTL_t;
typedef structure_IUG_UNIT_HEATER_HEATING_TIME_CTL_t    IUG_UNIT_HEATER_HEATING_TIME_CTL_t;


typedef struct
{
    uint8_t                     fade_out_cnt;           
    uint16_t                    fade_out_dly_cnt;       
    IUG_COMMAND_t               cmd_trg_last;           
    IUG_GEN_CTL_DISPLAY_ID_t    display_info;           

    uint8_t                     OFF_coverage_period;    
    uint8_t                     OFF_burst;              
    uint16_t                    OFF_burst_period;       

    IUG_COMMAND_t               cmd_initial;            
    IUG_COMMAND_t               cmd_transition;         

} IUG_UNIT_COMMAND_HANDLER_CTL_t;


typedef struct
{
    uint8_t                     tim_sec_grid;           
    uint8_t                     event_cnt;              
    uint8_t                     window_cnt;             
    uint16_t                    locked_cnt;             

} IUG_UNIT_COMMAND_MISUSE_CTL_t;


typedef struct
{
    IUG_UNIT_CONTROL_HEADER_t           gen_hdr;

    HTR_META_STATE_t                     HTR_META_STATE;      

    IUG_UNIT_HEATER_HEATING_TIME_CTL_t  *HG_HEATING_TIME;   

    IUG_UNIT_READ_STATUS_t              read_st;

    IUG_UNIT_READ_DTC_CTL_t             *read_dtc;

    IUG_UNIT_EEROM_RD_WR_t              *eeprom_rdwr;

    IUG_UNIT_COMP_TEST_t                *comp_test;

    IUG_UNIT_COMP_PROP_M45_NVM_ENTRY_t  *comp_pty_m45;

    HG_VAR_HEATER_ST_t                  HG_VAR_HTR_STATUS;

    IUG_UNIT_HEATER_SIGNAL_ID_t         heater_signal;

    struct
    {
        uint16_t                HG_STATE_NUM_2_raw;         

        uint8_t                 HG_STATE_STFL_raw;          
        uint8_t                 HG_STATE_HGVP_raw;          
        uint8_t                 HG_STATE_UEHFL_raw;         
        uint8_t                 STATUS_OUTPORT_raw;         
        IUG_PHYS_TEMP_1_1DEG_T  COOLANT_TEMP_1_raw;         

        IUG_PHYS_AIR_PRESSURE_HPA_T     AIR_PRESSURE_raw;   

        uint16_t                SUPPLY_VOLTAGE_raw;         

        uint8_t                 WBUS_VERSION_raw;           

        uint8_t                 ECU_CODING_1_raw;           

        uint8_t                 HEATER_TYPE_INFO_raw;       

        IUG_HEATER_CAPABILITY_t HEATER_CAPABILITY_raw;      

        IUG_HEATER_MODE_FUNC_t  HEATER_MODE_FUNC_raw;       

        IUG_PHYS_TIME_1M_T      MAX_HEATING_TIME_raw;       

        uint8_t                 SERIAL_NUMBER[(IUG_UNIT_READ_ID_SERIAL_NUMBER_SIZE)];   

    }   heater_data_raw;

    struct
    {
        IUG_UNIT_HEATER_SIGNAL_ID_t     heater_signal_msc;

        IUG_UNIT_READ_ID_CARRIER_t      read_id_req_init;       
        IUG_UNIT_READ_ID_CARRIER_t      read_id_ena_old;        

        uint16_t                SUPPLY_VOLTAGE;                 
        uint16_t                supply_voltage_monitoring_cnt;  


        uint16_t                hg_failure_suppression_cnt;     
        uint16_t                synchronization_period_cnt;     
        uint16_t                synchronization_delay_cnt;      

        IUG_UNIT_COMMAND_HANDLER_CTL_t  cmd_handler;            
        IUG_UNIT_COMMAND_MISUSE_CTL_t   cmd_misuse;             


        uint8_t                 ROUTINE_DURATION;               

    }   msc;

} structure_IUG_UNIT_HEATER_CONTROL_t;
typedef structure_IUG_UNIT_HEATER_CONTROL_t     IUG_UNIT_HEATER_CONTROL_t;


typedef struct
{
    IUG_UNIT_HEADER_CONFIGURATION_t   unit_header_cfg;

} structure_IUG_UNIT_MULTI_CONTROL_CONFIGURATION_t;
typedef structure_IUG_UNIT_MULTI_CONTROL_CONFIGURATION_t    IUG_UNIT_MULTI_CTL_CONFIGURATION_t;


typedef struct
{
    IUG_UNIT_CONTROL_HEADER_t   gen_hdr;

    WBUS_MEMBER_t           wbus_mbr_bound_htr_adr; 

    struct
    {

        uint8_t             settling_err_request_cfm;       
        uint8_t             settling_err_request_dat;       

    }   msc;

} structure_IUG_UNIT_MULTI_CTL_CONTROL_t;
typedef structure_IUG_UNIT_MULTI_CTL_CONTROL_t      IUG_UNIT_MULTI_CTL_CONTROL_t;


typedef struct
{
    IUG_UNIT_HEADER_CONFIGURATION_t   unit_header_cfg;

} structure_IUG_UNIT_TELESTART_CONFIGURATION_t;
typedef structure_IUG_UNIT_TELESTART_CONFIGURATION_t    IUG_UNIT_TELESTART_CONFIGURATION_t;


typedef struct
{
    IUG_UNIT_CONTROL_HEADER_t   gen_hdr;

    IUG_CMD_UNIT_LOCK_CONTROL_t   lock_ctl;

} structure_IUG_UNIT_TELESTART_CONTROL_t;
typedef structure_IUG_UNIT_TELESTART_CONTROL_t          IUG_UNIT_TELESTART_CONTROL_t;


typedef struct
{
    IUG_UNIT_HEADER_CONFIGURATION_t   unit_header_cfg;

} structure_IUG_UNIT_THERMO_CONNECT_CONFIGURATION_t;
typedef structure_IUG_UNIT_THERMO_CONNECT_CONFIGURATION_t   IUG_UNIT_THERMO_CONNECT_CONFIGURATION_t;


typedef struct
{
    IUG_UNIT_CONTROL_HEADER_t   gen_hdr;


    uint8_t    dummy;

} structure_IUG_UNIT_THERMO_CONNECT_CONTROL_t;
typedef structure_IUG_UNIT_THERMO_CONNECT_CONTROL_t     IUG_UNIT_THERMO_CONNECT_CONTROL_t;


typedef struct
{
    IUG_UNIT_HEADER_CONFIGURATION_t   unit_header_cfg;

} structure_IUG_UNIT_THERMO_CALL_CONFIGURATION_t;
typedef structure_IUG_UNIT_THERMO_CALL_CONFIGURATION_t      IUG_UNIT_THERMO_CALL_CONFIGURATION_t;


typedef struct
{
    IUG_UNIT_CONTROL_HEADER_t   gen_hdr;


    struct
    {

        uint8_t             settling_heater_addr;

        uint8_t             settling_monitoring;

    }   msc;

} structure_IUG_UNIT_THERMO_CALL_CONTROL_t;
typedef structure_IUG_UNIT_THERMO_CALL_CONTROL_t    IUG_UNIT_THERMO_CALL_CONTROL_t;


typedef struct
{
    IUG_UNIT_HEADER_CONFIGURATION_t   unit_header_cfg;

} structure_IUG_UNIT_BLUETOOTH_CONFIGURATION_t;


typedef struct
{
    IUG_UNIT_CONTROL_HEADER_t   gen_hdr;


    struct
    {
        uint8_t                         ble_assign_old;                 

    } msc;

} structure_IUG_UNIT_BLUETOOTH_CONTROL_CONTROL_t;


#pragma pack(1) 
typedef struct
{
    uint8_t     hour;       
    uint8_t     minute;     

} structure_IUG_UNIT_BLUETOOTH_TIMER_VALUE_t;
#pragma pack() 


#pragma pack(1) 
typedef struct
{
    structure_IUG_UNIT_BLUETOOTH_TIMER_VALUE_t  timer_value[(IUG_UNIT_BLUETOOTH_TIMER_EACH_DAY)][(IUG_UNIT_BLUETOOTH_TIMER_EACH_WEEK)];
    uint8_t     timer_activated[(IUG_UNIT_BLUETOOTH_TIMER_EACH_DAY)][(IUG_UNIT_BLUETOOTH_TIMER_EACH_WEEK)];    

} structure_IUG_UNIT_BLUETOOTH_TIMER_CTL_t;
#pragma pack() 


typedef struct
{
    IUG_UNIT_HEADER_CONFIGURATION_t   unit_header_cfg;

} structure_IUG_UNIT_TEMP_SENSOR_CONFIGURATION_t;


typedef struct
{
    IUG_UNIT_HEADER_CONFIGURATION_t   unit_header_cfg;

} IUG_UNIT_TEMP_SENSOR_T_LIN_R_CONFIGURATION_t;


typedef struct
{
    IUG_UNIT_CONTROL_HEADER_t       gen_hdr;

    IUG_UNIT_READ_STATUS_t          read_st;            
    uint8_t                         read_st_err;        

    union
    {
        IUG_TEMP_CELSIUS_05DEG_T    TEMP_raw;           
        IUG_TEMP_CELSIUS_05DEG_T    TEMP_INT_raw;       
        IUG_TEMP_CELSIUS_05DEG_T    TEMP_EXT_raw;       
    }                               TEMP_raw;

    union
    {
        int16_t                     TEMP;               
        int16_t                     TEMP_INT;           
        int16_t                     TEMP_EXT;           
    }                               TEMP;

} structure_IUG_UNIT_TEMP_SENSOR_CONTROL_t;


typedef struct
{
    IUG_UNIT_CONTROL_HEADER_t       gen_hdr;

    IUG_T_LIN_R_DET_TYPE_ID_t       typ_id;             

    IUG_UNIT_READ_STATUS_t          read_st;            
    uint8_t                         read_st_err;        

    union
    {
        IUG_TEMP_CELSIUS_05DEG_T    TEMP_raw;           
        IUG_TEMP_CELSIUS_05DEG_T    TEMP_INT_raw;       
        IUG_TEMP_CELSIUS_05DEG_T    TEMP_EXT_raw;       
    }                               TEMP_raw;

    union
    {
        int16_t                     TEMP;               
        int16_t                     TEMP_INT;           
        int16_t                     TEMP_EXT;           
    }                               TEMP;

    uint16_t                        RESISTOR_VAL_raw;

    uint16_t                        RESISTOR_SETPOINT;

    uint8_t                         RESISTOR_120_kOhm_STATE_raw;

    uint8_t                         RESISTOR_120_kOhm_SETPOINT;

    uint8_t                         RELAIS_STATE_raw;

    uint8_t                         RELAIS_SETPOINT;

} IUG_UNIT_TEMP_SENSOR_T_LIN_R_CONTROL_t;


typedef struct
{
    IUG_CMD_BTN_PATTERN_t           pattern_id;         

    int16_t                         pat_period_ms_s;    

    PG_PATTERN_CFG_t               *pat_cfg;            

} IUG_CMD_BUTTON_LED_PATTERN_CFG_t;


typedef struct
{
    IUG_TARGET_STATE_t              tgt_state;          

    IUG_CMD_BTN_PATTERN_t           pattern_id;         
    IUG_CMD_BTN_PATTERN_t           pattern_id_dev;     

    uint8_t                         hirarchy;           

} IUG_CMD_BUTTON_LED_PATTERN_HIRARCHY_CFG_t;


typedef struct
{
    uint8_t                             dpy_hry_iug;
    uint8_t                             dpy_hry_air;
    uint8_t                             dpy_hry_wat;
    uint8_t                             dpy_hry_ghi;
    IUG_CMD_BTN_PATTERN_t               dpy_pat_iug;                
    IUG_CMD_BTN_PATTERN_t               dpy_pat_iug_faulty;         
    IUG_CMD_BTN_PATTERN_t               dpy_pat_air;                
    IUG_CMD_BTN_PATTERN_t               dpy_pat_air_faulty;         
    IUG_CMD_BTN_PATTERN_t               dpy_pat_wat;                
    IUG_CMD_BTN_PATTERN_t               dpy_pat_wat_faulty;         
    IUG_CMD_BTN_PATTERN_t               dpy_pat_ghi;                
    IUG_CMD_BTN_PATTERN_t               dpy_pat_ghi_faulty;         
    IUG_CMD_BTN_PATTERN_t               dpy_pat_udv;
    IUG_CMD_BTN_PATTERN_t               dpy_pat_hmi;
    IUG_CMD_BTN_PATTERN_t               dpy_pat_hmi_cur;
    IUG_CMD_BTN_PATTERN_t               dpy_pat_hmi_old;

    IUG_CMD_BTN_PATTERN_t               led_pat_display_faulty;     
    IUG_CMD_BTN_PATTERN_t               led_pat_display_faulty_cfm; 
    IUG_CMD_BTN_PATTERN_t               dpy_pattern_old;            
    uint8_t                             led_pat_display_dev_mode;   
    uint16_t                            dpy_pat_long_period_cnt;    

    IUG_CMD_BUTTON_LED_PATTERN_CFG_t    const *p_led_pat_cfg;       

} IUG_CMD_BUTTON_LED_PATTERN_CTL_t;


typedef struct
{
    IUG_UNIT_HEADER_CONFIGURATION_t   unit_header_cfg;

} structure_IUG_UNIT_CMD_BUTTON_CONFIGURATION_t;


typedef struct
{
    IUG_UNIT_CONTROL_HEADER_t   gen_hdr;

    uint8_t                     cmd_flag;

    uint8_t     cmd_btn_status;            
    uint8_t     cmd_btn_filter_cnt;
    uint16_t    cmd_btn_pushed_cnt;
    uint16_t    cmd_btn_pushed_long_cnt;

    struct
    {
        uint16_t    cfg_AUT_AUX_HEATING_slot_cnt;            
        uint8_t     cfg_AUT_AUX_HEATING_new;                 

        uint16_t    cfg_BLE_PAIRING_slot_cnt;               
        uint16_t    cfg_BLE_PAIRING_wnd_cnt;                
        uint8_t     cfg_BLE_PAIRING_btn_cmd_cnt;            
        uint8_t     cfg_BLE_PAIRING_helper;                 
        #ifdef IUG_DEVELOPMENT_ACTIVE
        uint8_t     cfg_BLE_PAIRING_dbg_helper;             
        #endif

        uint16_t    cmd_btn_POR_cnt;

    } cmd_btn_event;    

    IUG_CMD_BUTTON_LED_PATTERN_CTL_t    led_pat_display_ctl;            

    struct
    {
        uint8_t                         can_web_brg_pressed;            
        #ifdef CAN_WEBASTO_BRIDGE_IMPLEMENT
        uint8_t                         can_web_brg_trg_unit_scan;      
        #endif

        uint8_t                         btn_assign_old;                 

        IUG_COMMAND_t                   cmd_misuse_btn_cmd;             

    } msc;

    #ifdef IUG_TRIGGER_WBUS_SCANNING_DEVELOPMENT_EN
    uint8_t                         cmd_btn_ibn_trg_flg;                    
    enum_INIT_OP_ROUTINES           cmd_btn_ibn_routine;                    
    IUG_UNIT_GENDET_STATE_t         cmd_btn_ibn_detc_gen_state;             
    WBUS_MEMBER_t                   cmd_btn_ibn_detc_scan_wbus_mbr;         
    IUG_HEATER_UNIT_ENUM_t          cmd_btn_ibn_detc_idx_htr;               
    IUG_COMM_DEVICE_ID_t            cmd_btn_ibn_detc_ghi_com_dev;           

    IUG_HTR_MSK_ID_t                cmd_btn_fast_ibn__pos_ghi_com_dev_msk;  

    uint16_t                        cmd_btn_fast_ibn__btn_detection_window; 

    uint8_t                         cmd_btn_fast_ibn__triggering_flg_0x0B;  
    #endif

} structure_IUG_UNIT_CMD_BUTTON_CONTROL_t;


#ifdef IUG_UNT_SIMULATION_EN
typedef struct
{
    IUG_UNIT_HEADER_CONFIGURATION_t   unit_header_cfg;

} structure_IUG_UNIT_SIMULATION_CONFIGURATION_t;


typedef struct
{
    IUG_UNIT_CONTROL_HEADER_t   gen_hdr;

    uint32_t            sim_cmd;        
    IUG_COMMAND_t       cmd_req;        

    uint8_t             init_flags; 

} structure_IUG_UNIT_SIMULATION_CONTROL_t;
#endif 


typedef struct
{
    IUG_UNIT_HEADER_CONFIGURATION_t   unit_header_cfg;

} structure_IUG_UNT_USER_API_CONFIGURATION_t;


typedef struct
{
    IUG_PHYS_TIME_1M_T      cmd_ACTIVATION_PERIOD;      
    IUG_PHYS_TEMP_1_1DEG_T  cmd_SETPOINT;               

} structure_IUG_UNT_USER_DEVICE_PARAMETER_t;
typedef structure_IUG_UNT_USER_DEVICE_PARAMETER_t   IUG_UNT_USER_DEVICE_PARAMETER_t;


typedef struct
{
    IUG_UNIT_HEADER_CONFIGURATION_t   unit_header_cfg;

} structure_IUG_UNT_USER_DEVICE_CONFIGURATION_t;
typedef structure_IUG_UNT_USER_DEVICE_CONFIGURATION_t   IUG_UNT_USER_DEVICE_CFG_t;


typedef struct
{
    IUG_UNIT_CONTROL_HEADER_t       gen_hdr;

    IUG_USRDEV_MSK_ID_t             udv_available;                                  

    uint8_t                         udv_status[(IUG_USER_DEVICE_MAX)];              
    API_USER_DEVICE_CMD_CFM_t       udv_cmd_cfm[(IUG_USER_DEVICE_MAX)];             

    IUG_USER_DEVICE_TERM_FLAG_t     udv_termination_flag[(IUG_USER_DEVICE_MAX)];    

    uint16_t                        udv_pending_tim_cnt[(IUG_USER_DEVICE_MAX)];     

    API_USER_DEVICE_CMD_t           udv_api_cmd[(IUG_USER_DEVICE_MAX)];
    IUG_UNT_USER_DEVICE_PARAMETER_t udv_api_parameter[(IUG_USER_DEVICE_MAX)];

    struct
    {
        IUG_USER_DEVICE_TERM_FLAG_t     termination_flag_old[(IUG_USER_DEVICE_MAX)];    

        API_USER_DEVICE_ID_t            unit_dtc_udv_idx_cur;                           

        IUG_COMMAND_t                   udv_cmd_burst_last;
        uint16_t                        ignore_cmd_due_burst_cnt;
        uint16_t                        failure_suppression_cnt[(IUG_USER_DEVICE_MAX)]; 

    } msc;

} structure_IUG_UNT_USER_DEVICE_CONTROL_t;
typedef structure_IUG_UNT_USER_DEVICE_CONTROL_t     IUG_UNT_USER_DEVICE_CTL_t;


typedef struct
{
    uint8_t x1_3_lin1_pwm_in_wu_en; 
    uint8_t sleep_enable;           
    uint8_t car_state_source_CAN1;  
    uint8_t car_state_source_CAN3;  
    uint8_t ctrl_magn_valve;        
    uint8_t ctrl_circ_pump;         
    boolean ctrl_obd_req;           
} struct_IUG_SYSTEM_API_CFG_IF;


typedef struct
{
    IUG_UNIT_CONTROL_HEADER_t       gen_hdr;

    IUG_UNIT_COMMAND_REQUEST_t          cmd_req_hgX[(IUG_HEATER_UNIT_ENUM_MAX)];            
    IUG_UNIT_COMMAND_REQUEST_TRIGGER_t  cmd_trg_last[(IUG_HEATER_UNIT_ENUM_MAX)];           

    IUG_HTR_MSK_ID_t                    cmd_activated_regular_htr;                          

    uint16_t                            cmd_release_cnt[(IUG_HEATER_UNIT_ENUM_MAX)];        

    uint16_t                            cmd_pending_tim_cnt[(IUG_HEATER_UNIT_ENUM_MAX)];    

    IUG_UNIT_API_COMMAND_t              api_cmd_hgX[(IUG_HEATER_UNIT_ENUM_MAX)];            

    IUG_UNIT_API_UNIT_ID_t              cmd_for_unit_id;                                    

    struct_IUG_SYSTEM_API_CFG_IF        iug_system_api_cfg_if;                              

} structure_IUG_UNT_USER_API_CONTROL_t;
typedef structure_IUG_UNT_USER_API_CONTROL_t    IUG_UNT_USER_API_CONTROL_t;


typedef struct
{
    IUG_UNIT_ID_t                       unit_id;        
    IUG_UNIT_HEADER_CONFIGURATION_t     *unit_cfg;      
    IUG_UNIT_CONTROL_HEADER_t           *unit_ctl;      
    void (*ini_fct)( const uint8_t idx_unit_tree );     
    void (*ctl_fct)( const uint8_t idx_unit_tree );     

    enum_parameter_id                   webpar_id;      

} structure_IUG_UNIT_TREE_CONFIGURATION_t;
typedef structure_IUG_UNIT_TREE_CONFIGURATION_t     IUG_UNIT_TREE_CONFIGURATION_t;


typedef struct
{
    uint8_t                 unit_available;     
    IUG_UNIT_ID_t           unit_id;            
    IUG_UNIT_FLAG_t         unit_flg_ctl;       
    uint8_t                 idx_unit_tree;      
    WBUS_MEMBER_t           idx_wbus_mbr_pty;   
    IUG_HEATER_UNIT_ENUM_t  idx_htr_pty;        
    uint8_t                 idx_com_mtx;        

    IUG_HTR_MSK_ID_t        msk_htr_bnd_ava;    
    IUG_USRDEV_MSK_ID_t     msk_udv_bnd_ava;    

    IUG_UNIT_CONTROL_HEADER_t   *unit_ctl;      

} IUG_UNIT_PROPERTY_t;


typedef struct
{
    uint8_t                 mbr_available;  
    WBUS_MEMBER_t           wbus_mbr;       
    uint8_t                 wbus_mbr_ins_pos[(WBUS_INS_ENUM_MAX)];  
    IUG_UNIT_ID_t           unit_id;        
    uint8_t                 idx_unit_tree;  

    IUG_UNIT_CONTROL_HEADER_t   *unit_ctl;  

} IUG_WBUS_MEMBER_PROPERTY_t;


#pragma pack(1) 
typedef struct
{
    IUG_UNIT_ID_t           unit_id;        
    WBUS_MEMBER_t           wbus_mbr;       
    uint8_t                 flags;          

    IUG_HTR_MSK_ID_t        msk_htr_bound;  
    IUG_USRDEV_MSK_ID_t     msk_udv_bound;  

} IUG_UNIT_COMM_MATRIX_NVM_t;
#pragma pack() 


typedef struct
{
    IUG_UNIT_ID_t               unit_id;

    IUG_HEATER_UNIT_ENUM_t      bound_to_htr_idx[(IUG_HEATER_UNIT_ENUM_MAX)];
    IUG_UNIT_HEATER_TYPE_ID_t   bound_to_htr_typ[(IUG_HEATER_UNIT_ENUM_MAX)];

    API_USER_DEVICE_ID_t        bound_to_udv_idx[(IUG_USER_DEVICE_MAX)];

} structure_IUG_UNIT_COMMUNICATION_MATRIX_BINDING_t;
typedef structure_IUG_UNIT_COMMUNICATION_MATRIX_BINDING_t   IUG_UNIT_COMM_MTX_BINDING_t;


typedef struct
{
    uint8_t                         unit_available;     
    IUG_UNIT_ID_t                   unit_id;            
    IUG_SITE_TARGET_ID_t            unit_tgt_id;        
    IUG_SITE_TARGET_CAPA_t          unit_tgt_capa;      

    IUG_UNIT_HEATER_SIGN_t          unit_htr_sign;      
    IUG_HEATER_CAPABILITY_t         unit_htr_capa;      

    WBUS_MEMBER_t                   wbus_mbr;           
    IUG_COMM_DEVICE_ID_t            ghi_com_dev;        

    IUG_HEATER_UNIT_ENUM_t          idx_htr;            
    uint8_t                         idx_unit_tree;      
    IUG_UNIT_FLAG_t                 unit_flg_ctl;       
    IUG_UNIT_HEATER_CONTROL_t       *unit_ctl;          

} IUG_UNIT_HEATER_PROPERTY_t;


#pragma pack(1) 
typedef union
{
    union
    {
        struct
        {
            uint8_t                     dummy;

        } iug_data;  

        struct
        {
            IUG_PHYS_AIR_PRESSURE_5HPA_T        AIR_PRESSURE_eeprom;

        } heater;  

        struct
        {
            uint8_t                     dummy;

        } multi_ctl;  

        struct
        {
            uint8_t                     dummy;

        } temp_sensor;  


    } uni;

} IUG_UNIT_PROPERTY_VAR_NVM_t;
#pragma pack() 


#pragma pack(1) 
typedef struct
{
    IUG_UNIT_ID_t               unit_id;                

    IUG_UNIT_PRPTY_FLAG_t       unit_prpty_flg;         

    uint8_t                     WBUS_VERSION;           

    IUG_UNIT_PROPERTY_VAR_NVM_t unit_data_var;          

} IUG_UNIT_PROPERTY_NVM_t;
#pragma pack() 


#pragma pack(1) 
typedef struct
{
    IUG_UNIT_ID_t                   unit_id;                

    IUG_UNIT_FLAG_t                 unit_flg_nvm;           

    WBUS_MEMBER_t                   wbus_mbr;               
    IUG_COMM_DEVICE_ID_t            ghi_com_dev;            

    IUG_UNIT_HEATER_SIGN_t          unit_htr_sign;          

    IUG_HEATER_CAPABILITY_t         unit_htr_capablty;      

    IUG_PHYS_TIME_1M_T              max_heating_time;       

    IUG_UNIT_READ_STATUS_CARRIER_t  st_available;           

    uint8_t                         WBUS_VERSION;           

    uint8_t                         SERIAL_NUMBER[(IUG_UNIT_READ_ID_SERIAL_NUMBER_SIZE)];

} IUG_HEATER_PROPERTY_NVM_t;
#pragma pack() 


#pragma pack(1) 
typedef struct
{
    uint8_t                     value;          

} structure_IUG_LEVEL_AIR_VENTILATION_CFG_t;
#pragma pack() 


#pragma pack(1) 
typedef struct
{
    enum_parameter_id       id_param;   

} structure_IUG_MODE_BTS_AIR_SETPOINT_CFG_t;
#pragma pack() 


#if 0 
typedef struct
{
    enum_IUG_MODE_BTS_AIR_t     mode_id;        
    IUG_PHYS_TEMP_1_1DEG_T      setpoint;       

} structure_IUG_MODE_BTS_AIR_SETPOINT_CTL_t;
#endif


typedef struct
{
    IUG_UNIT_HEATER_STATE_CATEGORY_t    category;   
    IUG_HEATER_STATE_NUMBER_t           state;      

} IUG_HEATER_STATE_NUMBER_CFG_t;


#endif 


