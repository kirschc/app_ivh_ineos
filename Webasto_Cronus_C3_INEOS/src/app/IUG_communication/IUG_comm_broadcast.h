#ifndef __IUG_COMM_BROADCAST_H_
#define __IUG_COMM_BROADCAST_H_


#define IUG_COMM_BRDCST_DESCRIPTION_NUM            13u 


#define IUG_BRDCST_ID_NONE                0x0000u
#define IUG_BRDCST_ID_OFF                 0x0001u   
#define IUG_BRDCST_ID_ON                  0x0002u   
#define IUG_BRDCST_ID_ON_HEAT             0x0004u
#define IUG_BRDCST_ID_ON_VENT             0x0008u
#define IUG_BRDCST_ID_ON_HEAT_AUX         0x0010u
#define IUG_BRDCST_ID_ON_UP               0x0020u
#define IUG_BRDCST_ID_ON_HEAT_BOOST       0x0040u
#define IUG_BRDCST_ID_ON_HEAT_ECO         0x0080u
#define IUG_BRDCST_ID_ON_TEMP             0x0100u
#define IUG_BRDCST_ID_ON_TEST             0x0200u
#define IUG_BRDCST_ID_ON_EMV              0x0400u
#define IUG_BRDCST_ID_ON_TEMP_MOD         0x0800u
#define IUG_BRDCST_ID_1000                0x1000u
#define IUG_BRDCST_ID_UDV                 0x2000u   
#define IUG_BRDCST_ID_FAULT               0x4000u   
#define IUG_BRDCST_ID_DATA_AIR_PRESSURE   0x8000u   

#define IUG_BRDCST_ID_ALL                  ((IUG_BRDCST_ID_OFF)|\
                                            (IUG_BRDCST_ID_ON)|\
                                            (IUG_BRDCST_ID_ON_HEAT)|\
                                            (IUG_BRDCST_ID_ON_VENT)|\
                                            (IUG_BRDCST_ID_ON_HEAT_AUX)|\
                                            (IUG_BRDCST_ID_ON_UP)|\
                                            (IUG_BRDCST_ID_ON_HEAT_BOOST)|\
                                            (IUG_BRDCST_ID_ON_HEAT_ECO)|\
                                            (IUG_BRDCST_ID_ON_TEMP)|\
                                            (IUG_BRDCST_ID_ON_TEST)|\
                                            (IUG_BRDCST_ID_ON_EMV)|\
                                            (IUG_BRDCST_ID_ON_TEMP_MOD)|\
                                            (IUG_BRDCST_ID_FAULT)|\
                                            (IUG_BRDCST_ID_DATA_AIR_PRESSURE))


#define IUG_BRDCST_IND_NONE                0x00000000u
#define IUG_BRDCST_IND_OFF                 0x00000001u 
#define IUG_BRDCST_IND_OFF_HG1             0x00010000u 
#define IUG_BRDCST_IND_OFF_HG2             0x00020000u 


boolean IUG_comm_broadcast_analyze( IUG_QUEUE_ENTRY_DATA_t * const que_dat, WBUS_MSG_DESCRIPTION_CTL_t * const wbus_msg_ctl );
boolean IUG_comm_broadcast_inhibit_clr_by_mode( const uint8_t msg_mode );
boolean IUG_comm_broadcast_inhibit_set( const IUG_BRDCST_ID_t inhibit_id, const WBUS_INSTANCE_t wbus_ins );
boolean IUG_comm_broadcast_inhibit_test_by_mode( const uint8_t msg_mode, uint8_t ins_inhibit[(WBUS_INS_ENUM_MAX)] );
void    IUG_comm_broadcast_trigger( void );



#endif 

