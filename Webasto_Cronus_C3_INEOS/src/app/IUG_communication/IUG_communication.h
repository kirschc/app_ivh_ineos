
#ifndef _IUG_COMMUNICATION_H_
#define _IUG_COMMUNICATION_H_


#ifdef TSKMON_PARTIAL_TASK_DUTY_MONITORING_EN
    #ifdef IUG_DEVELOPMENT_ACTIVE
    #endif
#endif


#define IUG_COMM_RX_EVENT_RECEIVED_REQUEST_EN


#undef IUG_WTT_DIAGNOSIS_BREAK_VIA_INVALID_MODE



#define IUG_COMM_RET_RXTX_NONE              0x0000u
#define IUG_COMM_RET_RXTX_RX                0x0001u 
#define IUG_COMM_RET_RXTX_TX                0x0002u 
#define IUG_COMM_RET_RXTX_MISC              0x0004u 
#define IUG_COMM_RET_RXTX_xxx8              0x0008u

#define IUG_COMM_RET_RXTX_RX_LOAD_LOW       ((IUG_COMM_RET_RXTX_RX) | 0x0100u)  
#define IUG_COMM_RET_RXTX_RX_LOAD_HIGH      ((IUG_COMM_RET_RXTX_RX) | 0x0200u)  
#define IUG_COMM_RET_RXTX_RX_x4xx           ((IUG_COMM_RET_RXTX_RX) | 0x0400u)
#define IUG_COMM_RET_RXTX_RX_x8xx           ((IUG_COMM_RET_RXTX_RX) | 0x0800u)

#define IUG_COMM_RET_RXTX_TX_LOAD_LOW       ((IUG_COMM_RET_RXTX_TX) | 0x1000u)  
#define IUG_COMM_RET_RXTX_TX_LOAD_HIGH      ((IUG_COMM_RET_RXTX_TX) | 0x2000u)  
#define IUG_COMM_RET_RXTX_TX_DELAY          ((IUG_COMM_RET_RXTX_TX) | 0x4000u)  
#define IUG_COMM_RET_RXTX_TX_8xxx           ((IUG_COMM_RET_RXTX_TX) | 0x8000u)



typedef uint16_t        IUG_COMM_RET_RXTX_t;




typedef struct 
{
    IUG_QUEUE_KEY_t                     que_processed[(IUG_QUEUE_ENTRY_MAX)];
    uint8_t                             que_processed_num;

    IUG_QUEUE_PRIORITY_t                que_priority_grp[(IUG_QUEUE_ENTRY_MAX)];
    uint8_t                             que_priority_grp_num;

    #ifdef IUG_DUTY_QUEUE_PROCESSING_EN
    TSKMON_PARTIAL_DUTY_MONITORING_t    que_processing_duty;
    #endif

} IUG_COMM_QUEUE_PROCESSING_t;




extern IUG_WBUS_COMM_CTL_t              IUG_WBUS_COMM_CTL;


boolean     IUG_CommAnalyze_Init( void );
boolean     IUG_CommAnalyze_WBusMessage( const IUG_QUEUE_KEY_t que_key, WBUS_MSG_DESCRIPTION_CTL_t * const wbus_msg_ctl_ptr );
IUG_COMM_RET_RXTX_t IUG_CommChangeBaudrate( const IUG_QUEUE_KEY_t que_key, const WBUS_INSTANCE_t wbus_ins );
boolean     IUG_CommChecksumCalculate( uint8_t * const ptr_msg );
void        IUG_CommCyclicMonitoring( const uint32_t delta_ms );
void        IUG_CommCyclicQueue( const uint32_t delta_ms );
uint16_t    IUG_CommDispatch( IUG_QUEUE_ENTRY_DATA_t * const que_dat, struct_IUG_EVALUATION_RESULT_t  eva_res );
IUG_COMM_RET_RXTX_t IUG_CommDoACK_POS( const IUG_QUEUE_KEY_t que_key, const WBUS_INSTANCE_t wbus_ins );
IUG_COMM_RET_RXTX_t IUG_CommDoACK_NEG( const IUG_QUEUE_KEY_t que_key, const WBUS_INSTANCE_t wbus_ins );
boolean     IUG_CommEvaluate_WBusMessage( const IUG_QUEUE_KEY_t que_key, struct_WBUS_MSG_DESCRIPTION_CTL_t * const wbus_msg_ctl );
void        IUG_CommFrameReceivedCallback( const IUG_COMM_MEDIUM_ID_t com_med, const WBUS_INSTANCE_t wbus_ins, const WBUS_DRIVER_ERROR_t wbus_err );
void        IUG_CommFrameSentCallback( const IUG_COMM_MEDIUM_ID_t com_med, const WBUS_INSTANCE_t wbus_ins, const WBUS_DRIVER_ERROR_t wbus_err );
int16_t     IUG_CommGetMessageDescription( const IUG_QUEUE_ENTRY_DATA_t * const que_dat, struct_WBUS_MSG_DESCRIPTION_CTL_t * const wbus_msg_ctl );
void        IUG_CommGetMessageDescription_BTM_related( const IUG_QUEUE_ENTRY_DATA_t * const que_dat, 
                                                       const WBUS_INSTANCE_t wbus_ins, struct_WBUS_MSG_DESCRIPTION_CTL_t * const wbus_msg_ctl );
uint8_t     IUG_CommGetRetryAttempts( uint8_t * const ack_timeout_cnt );
void        IUG_CommHandlerCyclic_fast( void );
void        IUG_CommInit( void );
void        IUG_CommMessageBTMUnknownNAK( IUG_QUEUE_ENTRY_DATA_t * const que_dat, WBUS_MSG_DESCRIPTION_CTL_t * const wbus_msg_ctl );
void        IUG_CommMessageUnknownNAK( IUG_QUEUE_ENTRY_DATA_t * const que_dat, WBUS_MSG_DESCRIPTION_CTL_t * const wbus_msg_ctl, const uint8_t nak_code );
IUG_QUEUE_ENTRY_DATA_t * IUG_CommReceive_EventReceived_ACK_NAK( const uint8_t fnd_msg_mode, const uint8_t fnd_msg_m_p1, 
                                                                IUG_QUEUE_ENTRY_DATA_t * const rec_que_dat, const WBUS_INSTANCE_t wbus_ins );
#ifdef IUG_COMM_RX_EVENT_RECEIVED_REQUEST_EN
IUG_QUEUE_ENTRY_DATA_t * IUG_CommReceive_EventReceived_Request( const uint8_t fnd_msg_mode, const uint8_t fnd_msg_mo_p1,
                                                                IUG_QUEUE_ENTRY_DATA_t * const rec_que_dat, const WBUS_INSTANCE_t wbus_ins );
#endif
IUG_COMM_RET_RXTX_t IUG_CommReceive_Statemachine( const IUG_QUEUE_KEY_t que_key, const WBUS_INSTANCE_t wbus_ins );
IUG_QUEUE_ENTRY_t * IUG_CommSelectQueue( IUG_QUEUE_KEY_t *const que_key, struct_IUG_QUEUE_ENTRY_DATA_t **const que_dat );
void        IUG_CommTaskCyclic_1ms( void );
void        IUG_CommTaskCyclic_10ms( void );
void        IUG_CommTaskCyclic_20ms( void );
void        IUG_CommTaskProcess_fast( void );
IUG_COMM_MEDIUM_ID_t IUG_CommTestCommMedium( const WBUS_INSTANCE_t wbus_ins,
                                             IUG_QUEUE_ENTRY_DATA_t * const que_dat,
                                             IUG_COMM_MEDIUM_ID_t   * const com_med_detailed );
IUG_COMM_MEDIUM_ID_t IUG_CommTestCommMedium_wbus_mbr_SE( const uint8_t wbus_mbr_SE,
                                                         IUG_QUEUE_ENTRY_DATA_t * const que_dat,
                                                         IUG_COMM_DEVICE_ID_t   * const mbr_E_is_ghi_htr, 
                                                         IUG_COMM_DEVICE_ID_t   * const mbr_S_is_ghi_htr );
IUG_COMM_RET_RXTX_t IUG_CommTransmit_Statemachine( const IUG_QUEUE_KEY_t que_key, const WBUS_INSTANCE_t wbus_ins );
boolean     IUG_CommTriggerTransmissionByIUG( const IUG_QUEUE_KEY_t que_key, const uint16_t msg_priority_cnt, const IUG_QUEUE_PRIORITY_t que_priority );
void        IUG_CommMonitoring_Update_rx_tx_error_cnt( const uint8_t rx_tx_selection, 
                                            const uint8_t cnt_mode, 
                                            const WBUS_MEMBER_t wbus_mbr_rx_tx,
                                            const WBUS_INSTANCE_t wbus_ins,
                                            IUG_QUEUE_ENTRY_DATA_t * const que_dat );


void IUG_CommReceive_ReportStatus( const WBUS_INSTANCE_t wbus_ins );


void IUG_CommTransmit_ReportStatus( const WBUS_INSTANCE_t wbus_ins );


#endif 


