#ifndef __IUG_COMM_MAB_H_
#define __IUG_COMM_MAB_H_

boolean IUG_Comm_MsgMAB( IUG_QUEUE_ENTRY_DATA_t * const que_dat, WBUS_MSG_DESCRIPTION_CTL_t * const wbus_msg_ctl );
void    IUG_Comm_MsgMAB_Timeout( uint8_t delta_ms );


#endif 


