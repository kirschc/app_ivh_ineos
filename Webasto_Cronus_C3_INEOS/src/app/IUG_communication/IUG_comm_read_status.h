#ifndef __IUG_COMM_READ_STATUS_H_
#define __IUG_COMM_READ_STATUS_H_

boolean IUG_Comm_MsgStatusRequest( IUG_QUEUE_ENTRY_DATA_t * const que_dat, WBUS_MSG_DESCRIPTION_CTL_t * const wbus_msg_ctl );



#endif 

