

#ifndef _TP_IL_ADVANCED_H__
#define _TP_IL_ADVANCED_H__



#ifdef  _MASTER_TP_IL_C_        
    #define  _AC_               
#else
    #define  _AC_  extern       
#endif



#define TP_IL_SET_TRANSMIT_EVENT()    { tpExtCtrl_t.txEvent_b    = (TRUE);\
                                        tpExtCtrl_t.txEventOld_b = (FALSE);\
                                      }


#define TP_IL_CLR_TRANSMIT_EVENT()    { tpExtCtrl_t.txEvent_b    = (FALSE);\
                                        tpExtCtrl_t.txEventOld_b = (FALSE);\
                                      }


#define TP_IL_BUFFER_RX(index_ui16)   tpExtCtrl_t.bufDataRx_pui08[index_ui16]
#define TP_IL_BUFFER_TX(index_ui16)   tpExtCtrl_t.bufDataTx_pui08[index_ui16]


#define TP_IL_BUFFER_LEN_RX           tpExtCtrl_t.lenBufDataRx_ui16

#define TP_IL_BUFFER_LEN_TX           tpExtCtrl_t.lenBufDataTx_ui16

typedef struct
{
    uint8_t* buffer_pui08;
    uint16_t bufferLength_ui16;
}
tp_il_config_type;

typedef struct
{
    uint8_t     txEvent_b;              
    uint8_t     txEventOld_b;           
    uint8_t*    bufDataRx_pui08;        
    uint8_t*    bufDataTx_pui08;        
    uint16_t    lenBufDataRx_ui16;      
    uint16_t    lenBufDataTx_ui16;      

}tp_il_extCtrl_type;



IO_U32 TP_IL_GetVersionOfDriver(void);

_AC_ IO_ErrorType TP_IL_Init(void* address);

_AC_ IO_ErrorType TP_IL_Receive(
    uint8_t* rxData_pui08);

_AC_ IO_ErrorType TP_IL_Transmit(
    const uint8_t* txData_pui08,
    uint16_t length_ui16);

_AC_ void TP_IL_Task(void);



_AC_ tp_il_extCtrl_type tpExtCtrl_t;



#ifdef  _AC_
    #undef  _AC_
#endif

#endif 


