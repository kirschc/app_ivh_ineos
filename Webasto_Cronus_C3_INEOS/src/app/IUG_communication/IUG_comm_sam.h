#ifndef __IUG_COMM_SAM_H_
#define __IUG_COMM_SAM_H_


#ifndef IUG_DEVELOPMENT_ACTIVE
#define IUG_SAM_LOCKED_TIME_S                  (60u * 60u) 
#else
#define IUG_SAM_LOCKED_TIME_S                  (      10u) 
#endif


#define IUG_SAM_TIMEOUT_MS                   3500u 



#define IUG_SAM_INVALID_ACCESS_ATTEMPT_MAX      3u

#define IUG_SAM_ENCRYPTION_SUCCESS              1u 

#define IUG_SAM_ENCRYPTION_RND_SIZE             4u 
#define IUG_SAM_ENCRYPTION_KEY_SIZE             8u 

#define IUG_SAM_ACTIVE                        0x5Au 

#define IUG_SAM_TIMER_1S_ms                   1000u 



#define IUG_SAM_SECURIZED_SERVICE_DEF \
{ \
    { \
        (WBUS_MSGMOD_33_EEPROM_WR),             \
        (WBUS_MSGSRV_00_NONE)                   \
    },\
    { \
        (WBUS_MSGMOD_42_ROUTINE),               \
        (WBUS_MSGSVR_06_ROUTINE_HG_TEST)        \
    },\
    { \
        (WBUS_MSGMOD_42_ROUTINE),               \
        (WBUS_MSGSVR_0B_ROUTINE_FBL)            \
    },\
    { \
        (WBUS_MSGMOD_48_MAB_MEM_ACC_BLKMOD),    \
        (WBUS_MSGSRV_01_MAB_ENTER)              \
    },\
    { \
        (WBUS_MSGMOD_48_MAB_MEM_ACC_BLKMOD),    \
        (WBUS_MSGSRV_03_MAB_EOL_READ)           \
    },\
    { \
        (WBUS_MSGMOD_48_MAB_MEM_ACC_BLKMOD),    \
        (WBUS_MSGSRV_04_MAB_EOL_WRITE)          \
    },\
    { \
        (WBUS_MSGMOD_48_MAB_MEM_ACC_BLKMOD),    \
        (WBUS_MSGSRV_09_MAB_EXIT)               \
    },\
    { \
        (WBUS_MSGMOD_49_UNIT_SPEC_DATA_RW),     \
        (WBUS_MSGSRV_FF_UNIT_SPEC_DATA_ALL)     \
    },\
    { \
        (WBUS_MSGMOD_52_PROD_WR),               \
        (WBUS_MSGSRV_FF_ALL)                    \
    },\
    { \
        (WBUS_MSGMOD_54_APP_DAT_WR),            \
        (WBUS_MSGSRV_FF_ALL)                    \
    },\
    { \
        (WBUS_MSGMOD_56_ERROR_MODE),            \
        (WBUS_MSGSRV_07_ERRMOD_HIST_TAB_CLEAR)  \
    }\
}


extern struct_IUG_SAM_SECURIZED_SERVICE_t       IUG_SAM_SECURIZED_SERVICE[];
extern const uint8_t ext_iug_entries_IUG_SAM_SECURIZED_SERVICE;


boolean     IUG_Comm_MsgSAM( IUG_QUEUE_ENTRY_DATA_t * const que_dat, WBUS_MSG_DESCRIPTION_CTL_t * const wbus_msg_ctl );
void        IUG_SAM_Init( void );
void        IUG_SAM_RandomNumber_Generate( void );
uint32_t    IUG_SAM_RandomNumber_Get( void );
boolean     IUG_SAM_ActiveTest( void );
boolean     IUG_SAM_ActiveTestService( const uint8_t mode, const uint8_t service );
void        IUG_SAM_Timeout( const uint8_t delta_ms );
void        IUG_SAM_TimeoutTrigger( void );


#endif 


