#ifndef __IUG_COMM_BLUETOOTH_H_
#define __IUG_COMM_BLUETOOTH_H_


#ifdef IUG_BLUETOOTH_RX_HOOK_EN
    #define IUG_BLUETOOTH_RX_FUNCTION   IUG_bluetooth_ctl_rx_data_callback
#else
    #define IUG_BLUETOOTH_RX_FUNCTION   iug_bluetooth_sci_rx_irq_callback
#endif


#define IUG_BLUETOOTH_COMM_ID_WRITE_REQUEST_FF               0x50u   
#define IUG_BLUETOOTH_COMM_ID_WRITE_REQUEST_CF               0x51u   
#define IUG_BLUETOOTH_COMM_ID_WRITE_SEND_STATUS              0x52u   

#define IUG_BLUETOOTH_COMM_ID_READ_REQUEST                   0x55u   
#define IUG_BLUETOOTH_COMM_ID_READ_RESPONSE_FF               0x56u   
#define IUG_BLUETOOTH_COMM_ID_READ_RESPONSE_CF               0x57u   

#define IUG_BLUETOOTH_COMM_STATUS_ACK                        0x00u   
#define IUG_BLUETOOTH_COMM_STATUS_NAK                        0x01u   
#define IUG_BLUETOOTH_COMM_STATUS_PENDING                    0x78u   

#define IUG_BLUETOOTH_COMM_TRM_TIMEOUT_MS                    3000u   
#define IUG_BLUETOOTH_COMM_TRM_TIMEOUT_CNT  (((IUG_BLUETOOTH_COMM_TRM_TIMEOUT_MS) + (IUG_UNIT_CTL_CYLIC_PERIOD)) / (IUG_UNIT_CTL_CYLIC_PERIOD))


#define IUG_BLUETOOTH_COMSTA_IDLE                 0x00u   
#define IUG_BLUETOOTH_COMSTA_REC_FF               0x10u   
#define IUG_BLUETOOTH_COMSTA_REC_CF               0x11u   
#define IUG_BLUETOOTH_COMSTA_REC_LF               0x1Au   
#define IUG_BLUETOOTH_COMSTA_REC_SEND_STATUS      0x1Bu   
#define IUG_BLUETOOTH_COMSTA_SND_FF               0x20u   
#define IUG_BLUETOOTH_COMSTA_SND_CF               0x21u   
#define IUG_BLUETOOTH_COMSTA_SND_STATUS           0x2Au   


#define IUG_BLUETOOTH_COMM_TRM_BUF_SIZE   (1u + 1u + 1u + \
                                           ((IUG_UNIT_BLUETOOTH_TIMER_EACH_WEEK)*(IUG_UNIT_BLUETOOTH_TIMER_EACH_DAY)))




typedef struct
{
    uint8_t     comm_state;
    uint8_t     data_len_exp;   
    uint8_t     data_len_cur;   
    uint16_t    timeout_cnt;

    uint8_t     res_cmd_addr;   
    uint8_t     res_nak_code;   

    struct_BGM111_VIRTUAL_CAN_MESSAGE   can_msg_obj;
    uint8_t     can_msg_obj_can_dat[8u];

    uint8_t     dat_img_rec[(IUG_BLUETOOTH_COMM_TRM_BUF_SIZE)];

    uint8_t     dat_img_snd[(IUG_BLUETOOTH_COMM_TRM_BUF_SIZE)];

} struct_IUG_bluetooth_ctl_comm_state_t;

extern struct_hal_sci_handle ext_sci_handle_Bluetooth;
extern uint8_t ext_ble_mac[6];



void            IUG_bluetooth_ctl_communication( void );
void            IUG_bluetooth_ctl_comm_frame_receive_collect( const struct_BGM111_VIRTUAL_CAN_MESSAGE * const p_can_rec );
void            IUG_bluetooth_ctl_comm_frame_receive_send_status( void );
uint8_t         IUG_bluetooth_ctl_comm_frame_send_fill_can_data( uint8_t * const p_can_dlc );
void            IUG_bluetooth_ctl_rx_data_callback( uint8_t rx_data );
void            bluetooth_sci_tx_func(uint16_t len, uint8_t* data);

void            iug_bluetooth_passkey_callback(struct_BGM111_SM_INFO_PASSKEY passkey);
void            iug_bluetooth_boot_callback(void);
void            iug_bluetooth_mac_callback(struct_BGM111_HW_INFO_LOCAL_MAC mac);
void            iug_bluetooth_bonding_callback(struct_BGM111_SM_INFO_BONDING bonding);
void            iug_bluetooth_connection_parameters_changed_callback(struct_BGM111_HW_INFO_CONNECTION_PARAMETERS connection_parameters);
void            iug_bluetooth_can_received_callback(struct_BGM111_VIRTUAL_CAN_MESSAGE can_msg );
void            iug_bluetooth_pairing_list_complete_callback(struct_BGM111_API_STORED_BONDING_T* ptr_bondlist, uint8_t count);
void            iug_bluetooth_state_callback(struct_BGM111_HW_INFO_GECKO_STATE gecko_state);
void            iug_bluetooth_ota_callback(struct_BGM111_CL_INFO_CHARACTERISTIC_RECEIVED info_characteristic_received);


void iug_bluetooth_sci_rx_irq_callback(uint8_t data);

void iug_bluetooth_handle_flash_state(void);




#if( ((IUG_BLUETOOTH_COMM_TRM_TIMEOUT_MS) <=    0) || \
     ((IUG_BLUETOOTH_COMM_TRM_TIMEOUT_MS) >  5000) )
    #error "Analyze value of 'IUG_BLUETOOTH_COMM_TRM_TIMEOUT_MS'!"
#endif


#endif 


