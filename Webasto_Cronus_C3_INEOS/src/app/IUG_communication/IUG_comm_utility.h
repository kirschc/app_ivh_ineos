#ifndef _IUG_COMM_UTILITY_H_
#define _IUG_COMM_UTILITY_H_


#ifdef CAN_WEBASTO_BRIDGE_IMPLEMENT
#include "CAN_Webasto_bridge__include.h"
#endif






typedef struct
{
    uint8_t             msg_SE;             
    uint8_t             msg_size;           
    uint8_t             msg_mode_meta;      
    uint8_t             msg_mode;           
    uint8_t             msg_service;        
    uint8_t             msg_service_sub;    

} IUG_COMM_MSG_PREPARE_t;


typedef struct
{
    uint8_t                     msg_iug_must_handle;
    uint8_t                     msg_gateway;
    uint8_t                     msg_suppress;

    IUG_QUEUE_ENTRY_DATA_t      * que_dat;
    WBUS_MSG_DESCRIPTION_CTL_t  * wbus_msg_ctl;

    uint8_t                     wbus_mbr_SE;            
    uint8_t                     wbus_msg_len;
    uint8_t                     wbus_msg_mod_meta;      
    uint8_t                     wbus_msg_mod;           
    uint8_t                     wbus_msg_srv;           
    uint8_t                     wbus_msg_srv_sub;       
    uint8_t                     res_nak_code;
    uint8_t                     *ptr_msg;
    uint16_t                    data_size;
    WBUS_INSTANCE_t             addr_ins_rec;           
    WBUS_INSTANCE_t             ins_mbrtrm_S;           
    WBUS_MEMBER_t               wbus_mbr_S;
    WBUS_MEMBER_t               wbus_mbr_S_control;     
    WBUS_MEMBER_t               wbus_mbr_S_heater;      
    WBUS_MEMBER_t               wbus_mbr_S_temp_sens;   
    WBUS_MEMBER_t               wbus_mbr_E;
    IUG_COMM_DEVICE_ID_t        wbus_mbr_S_is_ghi_htr;  
    IUG_COMM_DEVICE_ID_t        wbus_mbr_E_is_ghi_htr;  
    IUG_UNIT_ID_t               unit_id;

    IUG_UNIT_CONTROL_HEADER_t   *p_unt_ctl_S;           
    IUG_UNIT_READ_ID_t          *p_rd_id;
    IUG_UNIT_READ_STATUS_t      *p_rd_st;

    uint8_t                     high_load;              

} IUG_COMM_RX_PARAM_t;


typedef struct
{
    IUG_QUEUE_KEY_t             que_key;
    IUG_QUEUE_ENTRY_t           *que_ety;
    IUG_QUEUE_ENTRY_CTL_t       *que_ctl;
    IUG_QUEUE_ENTRY_DATA_t      *que_dat;
    IUG_QUEUE_TIMESTAMP_t       time_next;

    WBUS_INSTANCE_t             wbus_ins;
    IUG_COMM_MEDIUM_ID_t        com_med;
    IUG_COMMM_STATUS_t          com_state;
    IUG_COMM_DEVICE_ID_t        ghi_com_dev;
    uint16_t                    *p_comm_tx_locked_cnt;
    IUG_QUEUE_KEY_t             *p_comm_tx_locked_by_key;

    uint8_t                     wbus_mbr_SE;            
    uint8_t                     que_cfm;                

} IUG_COMM_RX_TX_STATEMACHINE_PARAM_t;




boolean         IUG_comm_cmd_trigger( const IUG_UNIT_ID_t      cmd_unit_id,
                                      const IUG_COMMAND_t      cmd_req,
                                      const IUG_PHYS_TIME_1M_T cmd_duration,
                                      const uint8_t            cmd_setpoint,
                                      const IUG_UNIT_ID_t      triggered_by_unit );
boolean         IUG_comm_get_ctl_data_pointer(  const uint8_t wbus_mbr_SE,
                                                IUG_UNIT_CONTROL_HEADER_t ** const ptr_unt_ctl_E,
                                                IUG_UNIT_CONTROL_HEADER_t ** const ptr_unt_ctl_S,
                                                const IUG_COMM_DEVICE_ID_t ctl_E_is_ghi_htr, const IUG_COMM_DEVICE_ID_t ctl_S_is_ghi_htr ) ;
uint8_t *       IUG_comm_msg_prepare(  uint8_t * const cmd_buf, IUG_COMM_MSG_PREPARE_t * const msg_dat );
void            IUG_comm_rx_par_fill( IUG_COMM_RX_PARAM_t        * const rx_par,
                                      IUG_QUEUE_ENTRY_DATA_t     * const que_dat,
                                      WBUS_MSG_DESCRIPTION_CTL_t * const wbus_msg_ctl );                                                
void            IUG_comm_rx_tx_stm_par_fill( IUG_COMM_RX_TX_STATEMACHINE_PARAM_t * const stmpar,
                                             const IUG_QUEUE_KEY_t               que_key,
                                             const WBUS_INSTANCE_t               wbus_ins );
void            IUG_comm_set_RX_reserved_cnt( const IUG_WBUS_INS_MSK_t wbus_ins_msk, const uint16_t cnt_val );
void            IUG_comm_set_TX_reserved_cnt( const IUG_WBUS_INS_MSK_t wbus_ins_msk, const uint16_t cnt_val );
WBUS_MEMBER_t   IUG_comm_test_comm_with_wtt_tester( const uint8_t wbus_mbr_SE );    
uint8_t         IUG_comm_test_iug_must_nak_the_request( const uint8_t wbus_mbr_SE );                                            
boolean         IUG_comm_test_iug_must_handle( const uint8_t wbus_mbr_SE, WBUS_MSG_DESCRIPTION_CTL_t * const wbus_msg_ctl, 
                                               uint8_t * const msg_gateway, uint8_t * const msg_suppress );
uint8_t         IUG_comm_test_msg_gateway( WBUS_MSG_DESCRIPTION_CTL_t * const wbus_msg_ctl );
IUG_WBUS_INS_MSK_t  IUG_comm_test_wbus_free( const IUG_WBUS_INS_MSK_t wbus_ins_msk );
uint8_t         IUG_comm_test_wbus_mode_direction( uint8_t * const p_wbus_msg );

#ifdef CAN_WEBASTO_BRIDGE_IMPLEMENT
WBUS_MEMBER_t   IUG_comm_test_comm_via_CanWebBridge( uint8_t * const p_wbus_msg, 
                                                     CANWEB_BRIDGE_CANCHL_ID_t * const p_idx_brg_cfg );
#endif


#endif 

