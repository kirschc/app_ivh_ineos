#ifndef _IUG_COMMUNICATION_INTERFACE_H_
#define _IUG_COMMUNICATION_INTERFACE_H_


#define IUG_QUEUE_DATBUF_SIZE_CMD_MSG               16u
#define IUG_QUEUE_DATBUF_SIZE_PENDING_MSG           16u
#define IUG_QUEUE_DATBUF_SIZE_RD_STATUS_MSG         32u
#define IUG_QUEUE_DATBUF_SIZE_RD_ID_MSG             32u
#define IUG_QUEUE_DATBUF_SIZE_COMP_TEST_MSG         32u
#define IUG_QUEUE_DATBUF_SIZE_RDWR_VIA_BUFFER_TX    16u
#define IUG_QUEUE_DATBUF_SIZE_MAX                   (IUG_QUEUE_DATA_BUFFER_SIZE_MAX)
#define IUG_QUEUE_DATBUF_SIZE_CAN_WEB_BRIDGE_NAK    7u  



IUG_QUEUE_ENTRY_t * IUG_CommIf_Queue_Select( IUG_QUEUE_ENTRY_DATA_t * * const p_que_dat, const uint16_t dat_buf_size );
IUG_QUEUE_KEY_t     IUG_CommIf_Queue_TriggerReceptionByIUG( IUG_QUEUE_ENTRY_DATA_t * const p_que_dat, const WBUS_INSTANCE_t wbus_ins,
                                                            const IUG_QUEUE_PRIORITY_t que_priority );




#define IUG_COMM_IF_QUEUE_RELEASE( que_key )            \
    IUG_QueueEntryRelease( (que_key) );


#define IUG_COMM_IF_QUEUE_SELECT( que_dat,              \
                                  dat_buf_size )        \
    IUG_CommIf_Queue_Select( (que_dat), (dat_buf_size) );


#define IUG_COMM_IF_QUEUE_SET_TIMEOUT( que_dat,         \
                                       wbus_ins,        \
                                       timeout )        \
    (que_dat)->retry_ins_res_timeout[(wbus_ins)] = (timeout);


#define IUG_COMM_IF_QUEUE_GET_COMM_MEDIUM( que_dat,      \
                                           wbus_ins )    \
    (que_dat)->addr_ins_com_med[(wbus_ins)];


#define IUG_COMM_IF_QUEUE_SET_COMM_MEDIUM( que_dat,      \
                                           wbus_ins,     \
                                           comm_medium ) \
    (que_dat)->addr_ins_com_med[(wbus_ins)] = (comm_medium);


#define IUG_COMM_IF_QUEUE_SET_COMM_STATE( que_dat,      \
                                          wbus_ins,     \
                                          comm_state )  \
    (que_dat)->ins_com_sta[(wbus_ins)] = (comm_state);


#define IUG_COMM_IF_QUEUE_TRG_RECEPTION_BY_IUG( que_dat,        \
                                                wbus_ins,       \
                                                que_priority )  \
    IUG_CommIf_Queue_TriggerReceptionByIUG( (que_dat), (wbus_ins), (que_priority) );                                       


#define IUG_COMM_IF_QUEUE_TRG_TRANSMISSION_BY_IUG( que_key,         \
                                                   msg_priority ,   \
                                                   que_priority )   \
    IUG_CommTriggerTransmissionByIUG( (que_key), (msg_priority), (que_priority) );


#define IUG_COMM_IF_GET_RX_HANDLER( wbus_ins )          \
    *((&IUG_WBUS_COMM_CTL.comm_rx_act[0u])+(wbus_ins))


#define IUG_COMM_IF_SET_RX_HANDLER( wbus_ins,           \
                                    que_key )           \
    *((&IUG_WBUS_COMM_CTL.comm_rx_act[0u])+(wbus_ins)) = (que_key);


#define IUG_COMM_IF_GET_TX_HANDLER( wbus_ins )          \
    *((&IUG_WBUS_COMM_CTL.comm_tx_act[0u])+(wbus_ins))


#define IUG_COMM_IF_SET_TX_HANDLER( wbus_ins,           \
                                    que_key )           \
    *((&IUG_WBUS_COMM_CTL.comm_tx_act[0u])+(wbus_ins)) = (que_key);






#endif 


