#ifndef _IUG_COMMUNICATION__CON_H_
#define _IUG_COMMUNICATION__CON_H_



#define IUG_COMMED_GROUP_MSK            0xF0u   

#define IUG_COMMED_WBUS                 (0x00u & (IUG_COMMED_GROUP_MSK))   
#define IUG_COMMED_GHI                  (0x10u & (IUG_COMMED_GROUP_MSK))   
#if 0
#define IUG_COMMED_CAN                  (0x20u & (IUG_COMMED_GROUP_MSK))   
#endif

#define IUG_COMDEV_WBUS_DEF             0u

#define IUG_COMDEV_GHI_1_DEF            1u
#define IUG_COMDEV_GHI_2_DEF            ((IUG_COMDEV_GHI_1_DEF) + 1u)
#define IUG_COMDEV_GHI_3_DEF            ((IUG_COMDEV_GHI_1_DEF) + 2u)
#define IUG_COMDEV_GHI_4_DEF            ((IUG_COMDEV_GHI_1_DEF) + 3u)
#define IUG_COMDEV_GHI_5_DEF            ((IUG_COMDEV_GHI_1_DEF) + 4u)
#define IUG_COMDEV_GHI_6_DEF            ((IUG_COMDEV_GHI_1_DEF) + 5u)
#define IUG_COMDEV_GHI_7_DEF            ((IUG_COMDEV_GHI_1_DEF) + 6u)
#define IUG_COMDEV_GHI_8_DEF            ((IUG_COMDEV_GHI_1_DEF) + 7u)



typedef enum
{
    IUG_COMMED_NONE         = 0x00u,    
    IUG_COMMED_W_BUS        = ((IUG_COMMED_WBUS)|0x00u),    
    IUG_COMMED_W_BUS_1      = ((IUG_COMMED_WBUS)|0x01u),    
    IUG_COMMED_W_BUS_2      = ((IUG_COMMED_WBUS)|0x02u),    
    IUG_COMMED_GHI_BUS      = ((IUG_COMMED_GHI)|0x00u),     
    IUG_COMMED_GHI_BUS_1    = ((IUG_COMMED_GHI)|0x01u),     
    IUG_COMMED_GHI_BUS_2    = ((IUG_COMMED_GHI)|0x02u),     
    IUG_COMMED_GHI_BUS_3    = ((IUG_COMMED_GHI)|0x03u),     
    IUG_COMMED_GHI_BUS_4    = ((IUG_COMMED_GHI)|0x04u),     
    IUG_COMMED_GHI_BUS_5    = ((IUG_COMMED_GHI)|0x05u),     
    #if 0
    IUG_COMMED_CAN_BUS      = ((IUG_COMMED_CAN)|0x00u),     
    IUG_COMMED_CAN_BUS_1    = ((IUG_COMMED_CAN)|0x01u),     
    IUG_COMMED_CAN_BUS_2    = ((IUG_COMMED_CAN)|0x02u),     
    IUG_COMMED_CAN_BUS_3    = ((IUG_COMMED_CAN)|0x03u),     
    IUG_COMMED_CAN_BUS_4    = ((IUG_COMMED_CAN)|0x04u),     
    IUG_COMMED_CAN_BUS_5    = ((IUG_COMMED_CAN)|0x05u),     
    #endif
    IUG_COMMED_ENUM_MAX,                
    IUG_COMMED_ENUM_FORCE_TYPE = 0x7F   

} IUG_COMM_MEDIUM_ID_t;

#define IUG_COMMED_GHI_BUS_FIRST        (IUG_COMMED_GHI_BUS)
#define IUG_COMMED_GHI_BUS_LAST         (IUG_COMMED_GHI_BUS_5)

#define GHIBUS_INS_0            0u  
#define GHIBUS_INS_ENUM_MAX     1u


typedef enum
{
    IUG_COMDEV_NONE         = 0x00u,    
    IUG_COMDEV_W_BUS        = (IUG_COMDEV_WBUS_DEF),    
    IUG_COMDEV_GHI_1        = (IUG_COMDEV_GHI_1_DEF),   
    IUG_COMDEV_GHI_2        = (IUG_COMDEV_GHI_2_DEF),   
    IUG_COMDEV_GHI_3        = (IUG_COMDEV_GHI_3_DEF),   
    IUG_COMDEV_GHI_4        = (IUG_COMDEV_GHI_4_DEF),   
    IUG_COMDEV_GHI_5        = (IUG_COMDEV_GHI_5_DEF),   
    IUG_COMDEV_GHI_6        = (IUG_COMDEV_GHI_6_DEF),   
    IUG_COMDEV_GHI_7        = (IUG_COMDEV_GHI_7_DEF),   
    IUG_COMDEV_GHI_8        = (IUG_COMDEV_GHI_8_DEF),   
    IUG_COMDEV_ENUM_MAX,                
    IUG_COMDEV_GHI_NOT_AVAILABLE    = 0x7Au,    
    IUG_COMDEV_ENUM_FORCE_TYPE      = 0x7F      

} IUG_COMM_DEVICE_ID_t;


#define IUG_COMDEV_GHI_FIRST        (IUG_COMDEV_GHI_1_DEF)

#if( 8u == (IUG_HEATER_UNIT_NUMBER_MAX) )
    #define IUG_COMDEV_GHI_LAST     (IUG_COMDEV_GHI_8_DEF)
#else
    #error "Analyze: 'IUG_COMDEV_GHI_LAST'!"
#endif


typedef enum
{
    IUG_COMTYP_NONE         	= 0x00000000u,    
    IUG_COMTYP_WBUS___CMD_ACT  	= 0x00000001u,    
    IUG_COMTYP_WBUS___CMD_PRD 	= 0x00000002u,    
    IUG_COMTYP_WBUS_PENDG_ACT   = 0x00000004u,    
    IUG_COMTYP_WBUS_PENDG_PRD	= 0x00000008u,    
    IUG_COMTYP_WBUS_RD_ID   	= 0x00000010u,    
    IUG_COMTYP_WBUS_RD_ST   	= 0x00000020u,    
    IUG_COMTYP_WBUS_RDDTC   	= 0x00000040u,    
    IUG_COMTYP_WBUS_RWEEP   	= 0x00000080u,    
    IUG_COMTYP_WBUS_UNDAT   	= 0x00000100u,    
    IUG_COMTYP_WBUS_CMP_T   	= 0x00000200u,    
    IUG_COMTYP_WBUS_0400    	= 0x00000400u,
    IUG_COMTYP_WBUS_0800    	= 0x00000800u,
    IUG_COMTYP_WBUS_BCAST   	= 0x00001000u,    
    IUG_COMTYP_WBUS_OTHER   	= 0x00002000u,    
    IUG_COMTYP_WBUS_4000    	= 0x00004000u,
    IUG_COMTYP_WBUS_8000    	= 0x00008000u,
	IUG_COMTYP_WBUS_PRIO_CMD    = 0x10000000u,	  
	IUG_COMTYP_WBUS_PRIO_PENDG  = 0x20000000u,	  
    IUG_COMTYP_ENUM_FORCE_TYPE  = 0x7FFFFFFF  

} IUG_COMM_TYPE_ID_t;


#endif 


