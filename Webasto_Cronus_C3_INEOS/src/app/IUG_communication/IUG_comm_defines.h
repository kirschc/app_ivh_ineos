#ifndef __IUG_COMM_DEFINES_H_
#define __IUG_COMM_DEFINES_H_

#define IUG_COMM_ACK_TIMEOUT_CNT_ELAPSED        0u 
#define IUG_COMM_ACK_TIMEOUT_CNT_EVENT          1u 

#define IUG_COMM_ACK_TIMEOUT_DELAY_CNT_MIN      (3000u / (IUG_UNIT_CTL_CYLIC_PERIOD))   
#define IUG_COMM_ACK_TIMEOUT_DELAY_CNT_MED      (5000u / (IUG_UNIT_CTL_CYLIC_PERIOD))   
#define IUG_COMM_ACK_TIMEOUT_DELAY_CNT_MAX      (7000u / (IUG_UNIT_CTL_CYLIC_PERIOD))   


#define IUG_COMM_RX_ACT_INIT            0x00u   
#define IUG_COMM_RX_ACT_RECEIVED        0x01u   
#define IUG_COMM_RX_ACT_RES_CALLBACK    0x02u   
#define IUG_COMM_RX_ACT_RES_RX_BROKEN   0x03u   
#define IUG_COMM_RX_ACT_RES_MONITORING  0x04u   
#define IUG_COMM_RX_ACT_INACTIVE        0x0Au   


#define IUG_COMM_TX_ACT_INIT            0x00u   
#define IUG_COMM_TX_ACT_TRANSMITTED     0x01u   
#define IUG_COMM_TX_ACT_RES_CALLBACK    0x02u   
#define IUG_COMM_TX_ACT_RES_MONITORING  0x04u   
#define IUG_COMM_TX_ACT_INACTIVE        0x0Au   









#define IUG_COMM_LOW_BYTE( w )         ( ( uint8 )( w ) )

#define IUG_COMM_HIGH_BYTE( w )        ( ( uint8 )( (w) >> 8 ) )


#define IUG_COMM_SWAP_MSG_SE( que_data ) \
    *(que_data->key_ptr_data+0u) = ( (*(que_data->key_ptr_data+0u) >> 4u) \
                                 |   (*(que_data->key_ptr_data+0u) << 4u) );



#define IUG_COMM_DATA_0BYTE( que_data, wbus_ins ) \
    (que_data->ins_data_size[(wbus_ins)]) = (4u+0u); \
    *(que_data->key_ptr_data+1u) = (2u+0u);  


#define IUG_COMM_DATA_1BYTE( que_data, wbus_ins, d0 ) \
     (que_data->ins_data_size[(wbus_ins)]) = (4u+1u); \
    *(que_data->key_ptr_data+1u) = (2u+1u);  \
    *(que_data->key_ptr_data+3u) = (d0)


#define IUG_COMM_DATA_2BYTE( que_data, wbus_ins, d0, d1 ) \
     (que_data->ins_data_size[(wbus_ins)]) = (4u+2u); \
    *(que_data->key_ptr_data+1u) = (2u+2u);  \
    *(que_data->key_ptr_data+3u) = (d0); \
    *(que_data->key_ptr_data+4u) = (d1)


#define IUG_COMM_DATA_3BYTE( que_data, wbus_ins, d0, d1, d2 ) \
     (que_data->ins_data_size[(wbus_ins)]) = (4u+3u); \
    *(que_data->key_ptr_data+1u) = (2u+3u);  \
    *(que_data->key_ptr_data+3u) = (d0); \
    *(que_data->key_ptr_data+4u) = (d1); \
    *(que_data->key_ptr_data+5u) = (d2)


#define IUG_COMM_DATA_4BYTE( que_data, wbus_ins, d0, d1, d2, d3 ) \
     (que_data->ins_data_size[(wbus_ins)]) = (4u+4u); \
    *(que_data->key_ptr_data+1u) = (2u+4u);  \
    *(que_data->key_ptr_data+3u) = (d0); \
    *(que_data->key_ptr_data+4u) = (d1); \
    *(que_data->key_ptr_data+5u) = (d2); \
    *(que_data->key_ptr_data+6u) = (d3)


#define IUG_COMM_DATA_5BYTE( que_data, wbus_ins, d0, d1, d2, d3, d4 ) \
     (que_data->ins_data_size[(wbus_ins)]) = (4u+5u); \
    *(que_data->key_ptr_data+1u) = (2u+5u);  \
    *(que_data->key_ptr_data+3u) = (d0); \
    *(que_data->key_ptr_data+4u) = (d1); \
    *(que_data->key_ptr_data+5u) = (d2); \
    *(que_data->key_ptr_data+6u) = (d3); \
    *(que_data->key_ptr_data+7u) = (d4)

#define IUG_COMM_DATA_6BYTE( que_data, wbus_ins, d0, d1, d2, d3, d4, d5 ) \
     (que_data->ins_data_size[(wbus_ins)]) = (4u+6u); \
    *(que_data->key_ptr_data+1u) = (2u+6u);  \
    *(que_data->key_ptr_data+3u) = (d0); \
    *(que_data->key_ptr_data+4u) = (d1); \
    *(que_data->key_ptr_data+5u) = (d2); \
    *(que_data->key_ptr_data+6u) = (d3); \
    *(que_data->key_ptr_data+7u) = (d4); \
    *(que_data->key_ptr_data+8u) = (d5)

#define IUG_COMM_DATA_7BYTE( que_data, wbus_ins, d0, d1, d2, d3, d4, d5, d6 ) \
     (que_data->ins_data_size[(wbus_ins)]) = (4u+7u); \
    *(que_data->key_ptr_data+1u) = (2u+7u);  \
    *(que_data->key_ptr_data+3u) = (d0); \
    *(que_data->key_ptr_data+4u) = (d1); \
    *(que_data->key_ptr_data+5u) = (d2); \
    *(que_data->key_ptr_data+6u) = (d3); \
    *(que_data->key_ptr_data+7u) = (d4); \
    *(que_data->key_ptr_data+8u) = (d5); \
    *(que_data->key_ptr_data+9u) = (d6)

#define IUG_COMM_DATA_8BYTE( que_data, wbus_ins, d0, d1, d2, d3, d4, d5, d6, d7 ) \
     (que_data->ins_data_size[(wbus_ins)]) = (4u+8u); \
    *(que_data->key_ptr_data+1u)  = (2u+8u);  \
    *(que_data->key_ptr_data+3u)  = (d0); \
    *(que_data->key_ptr_data+4u)  = (d1); \
    *(que_data->key_ptr_data+5u)  = (d2); \
    *(que_data->key_ptr_data+6u)  = (d3); \
    *(que_data->key_ptr_data+7u)  = (d4); \
    *(que_data->key_ptr_data+8u)  = (d5); \
    *(que_data->key_ptr_data+9u)  = (d6); \
    *(que_data->key_ptr_data+10u) = (d7)

#define IUG_COMM_DATA_9BYTE( que_data, wbus_ins, d0, d1, d2, d3, d4, d5, d6, d7, d8 ) \
     (que_data->ins_data_size[(wbus_ins)]) = (4u+9u); \
    *(que_data->key_ptr_data+1u)  = (2u+9u);  \
    *(que_data->key_ptr_data+3u)  = (d0); \
    *(que_data->key_ptr_data+4u)  = (d1); \
    *(que_data->key_ptr_data+5u)  = (d2); \
    *(que_data->key_ptr_data+6u)  = (d3); \
    *(que_data->key_ptr_data+7u)  = (d4); \
    *(que_data->key_ptr_data+8u)  = (d5); \
    *(que_data->key_ptr_data+9u)  = (d6); \
    *(que_data->key_ptr_data+10u) = (d7); \
    *(que_data->key_ptr_data+11u) = (d8)

#define IUG_COMM_DATA_10BYTE( que_data, wbus_ins, d0, d1, d2, d3, d4, d5, d6, d7, d8, d9 ) \
     (que_data->ins_data_size[(wbus_ins)]) = (4u+10u); \
    *(que_data->key_ptr_data+1u)  = (2u+10u);  \
    *(que_data->key_ptr_data+3u)  = (d0); \
    *(que_data->key_ptr_data+4u)  = (d1); \
    *(que_data->key_ptr_data+5u)  = (d2); \
    *(que_data->key_ptr_data+6u)  = (d3); \
    *(que_data->key_ptr_data+7u)  = (d4); \
    *(que_data->key_ptr_data+8u)  = (d5); \
    *(que_data->key_ptr_data+9u)  = (d6); \
    *(que_data->key_ptr_data+10u) = (d7); \
    *(que_data->key_ptr_data+11u) = (d8); \
    *(que_data->key_ptr_data+12u) = (d9)

#define IUG_COMM_DATA_12BYTE( que_data, wbus_ins, d0, d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11 ) \
     (que_data->ins_data_size[(wbus_ins)]) = (4u+12u); \
    *(que_data->key_ptr_data+1u)  = (2u+12u);  \
    *(que_data->key_ptr_data+3u)  = (d0); \
    *(que_data->key_ptr_data+4u)  = (d1); \
    *(que_data->key_ptr_data+5u)  = (d2); \
    *(que_data->key_ptr_data+6u)  = (d3); \
    *(que_data->key_ptr_data+7u)  = (d4); \
    *(que_data->key_ptr_data+8u)  = (d5); \
    *(que_data->key_ptr_data+9u)  = (d6); \
    *(que_data->key_ptr_data+10u) = (d7); \
    *(que_data->key_ptr_data+11u) = (d8); \
    *(que_data->key_ptr_data+12u) = (d9); \
    *(que_data->key_ptr_data+13u) = (d10); \
    *(que_data->key_ptr_data+14u) = (d11)

#define IUG_COMM_DATA_13BYTE( que_data, wbus_ins, d0, d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11, d12 ) \
     (que_data->ins_data_size[(wbus_ins)]) = (4u+13u); \
    *(que_data->key_ptr_data+1u)  = (2u+13u);  \
    *(que_data->key_ptr_data+3u)  = (d0); \
    *(que_data->key_ptr_data+4u)  = (d1); \
    *(que_data->key_ptr_data+5u)  = (d2); \
    *(que_data->key_ptr_data+6u)  = (d3); \
    *(que_data->key_ptr_data+7u)  = (d4); \
    *(que_data->key_ptr_data+8u)  = (d5); \
    *(que_data->key_ptr_data+9u)  = (d6); \
    *(que_data->key_ptr_data+10u) = (d7); \
    *(que_data->key_ptr_data+11u) = (d8); \
    *(que_data->key_ptr_data+12u) = (d9); \
    *(que_data->key_ptr_data+13u) = (d10); \
    *(que_data->key_ptr_data+14u) = (d11); \
    *(que_data->key_ptr_data+15u) = (d12)

#define IUG_COMM_DATA_14BYTE( que_data, wbus_ins, d0, d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11, d12, d13 ) \
     (que_data->ins_data_size[(wbus_ins)]) = (4u+14u); \
    *(que_data->key_ptr_data+1u)  = (2u+14u);  \
    *(que_data->key_ptr_data+3u)  = (d0); \
    *(que_data->key_ptr_data+4u)  = (d1); \
    *(que_data->key_ptr_data+5u)  = (d2); \
    *(que_data->key_ptr_data+6u)  = (d3); \
    *(que_data->key_ptr_data+7u)  = (d4); \
    *(que_data->key_ptr_data+8u)  = (d5); \
    *(que_data->key_ptr_data+9u)  = (d6); \
    *(que_data->key_ptr_data+10u) = (d7); \
    *(que_data->key_ptr_data+11u) = (d8); \
    *(que_data->key_ptr_data+12u) = (d9); \
    *(que_data->key_ptr_data+13u) = (d10); \
    *(que_data->key_ptr_data+14u) = (d11); \
    *(que_data->key_ptr_data+15u) = (d12); \
    *(que_data->key_ptr_data+16u) = (d13)

#define IUG_COMM_DATA_15BYTE( que_data, wbus_ins, d0, d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11, d12, d13, d14 ) \
     (que_data->ins_data_size[(wbus_ins)]) = (4u+15u); \
    *(que_data->key_ptr_data+1u)  = (2u+15u);  \
    *(que_data->key_ptr_data+3u)  = (d0); \
    *(que_data->key_ptr_data+4u)  = (d1); \
    *(que_data->key_ptr_data+5u)  = (d2); \
    *(que_data->key_ptr_data+6u)  = (d3); \
    *(que_data->key_ptr_data+7u)  = (d4); \
    *(que_data->key_ptr_data+8u)  = (d5); \
    *(que_data->key_ptr_data+9u)  = (d6); \
    *(que_data->key_ptr_data+10u) = (d7); \
    *(que_data->key_ptr_data+11u) = (d8); \
    *(que_data->key_ptr_data+12u) = (d9); \
    *(que_data->key_ptr_data+13u) = (d10); \
    *(que_data->key_ptr_data+14u) = (d11); \
    *(que_data->key_ptr_data+15u) = (d12); \
    *(que_data->key_ptr_data+16u) = (d13); \
    *(que_data->key_ptr_data+17u) = (d14)


#define IUG_COMM_DATA_nBYTE( que_data, wbus_ins, data_size ) \
    (que_data->ins_data_size[(wbus_ins)]) = (4u+(data_size)); \
    *(que_data->key_ptr_data+1u) = (2u+(data_size));  





#define IUG_COMM_NEG_RESP( que_data, wbus_ins, nak_code ) \
     (que_data->ins_data_size[(wbus_ins)]) = (4u+2u); \
    *(que_data->key_ptr_data+1u)  =  (2u+2u);  ; \
    *(que_data->key_ptr_data+3u)  = *(que_data->key_ptr_data+2u);  \
    *(que_data->key_ptr_data+3u) &= ~(WBUS_MSGMOD_80_POSACK_BIT);  \
    *(que_data->key_ptr_data+2u)  = (WBUS_MSGMOD_7F_NEGACK);  \
    *(que_data->key_ptr_data+4u)  = (nak_code) 


#define IUG_COMM_NEG_RESP_MAB_WITH_ADDR( que_data, wbus_ins, nak_code, addrh, addrl ) \
     (que_data->ins_data_size[(wbus_ins)]) = (4u+2u+2u); \
    *(que_data->key_ptr_data+1u)  =  (2u+4u);   \
    *(que_data->key_ptr_data+3u)  = *(que_data->key_ptr_data+2u);  \
    *(que_data->key_ptr_data+3u) &= ~(WBUS_MSGMOD_80_POSACK_BIT);  \
    *(que_data->key_ptr_data+2u)  = (WBUS_MSGMOD_7F_NEGACK);  \
    *(que_data->key_ptr_data+4u)  = (nak_code);  \
    *(que_data->key_ptr_data+5u)  = (addrh);  \
    *(que_data->key_ptr_data+6u)  = (addrl)  


#define IUG_COMM_PREPARE_WBUS_MSG_HEADER( msg_buf, msg_SE, msg_sze, msg_mod, msg_srv ) \
{\
    \
    *((msg_buf) + (WBUS_PACKET_POS_ADDR))    = (msg_SE);\
    *((msg_buf) + (WBUS_PACKET_POS_LEN))     = (msg_sze) + 1u;\
    *((msg_buf) + (WBUS_PACKET_POS_MODE))    = (msg_mod);\
    *((msg_buf) + (WBUS_PACKET_POS_SERVICE)) = (msg_srv);\
}



#endif 


