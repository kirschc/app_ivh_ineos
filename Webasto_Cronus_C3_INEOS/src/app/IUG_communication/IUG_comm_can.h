#ifndef IUG_COMM_CAN_H
#define IUG_COMM_CAN_H





void IUG_comm_eval_can_rx_buf(QueueHandle_t const can_rx_queue);

void IUG_comm_set_can_rx_buf_entry(const QueueHandle_t can_rx_queue, const can_bus_id can_bus, const struct_hal_can_frame *const ptr_can_msg);

static void IUG_comm_set_user_can_frame(const struct_hal_can_frame *const ptr_source_can_msg, struct_api_utilities_can_frame *const ptr_target_can_msg);

void IUG_comm_provide_can1_rx_msg(const struct_hal_can_frame *const ptr_can_msg);

void IUG_comm_provide_can2_rx_msg(const struct_hal_can_frame *const ptr_can_msg);

void IUG_comm_provide_can3_rx_msg(const struct_hal_can_frame *const ptr_can_msg);


#endif 


