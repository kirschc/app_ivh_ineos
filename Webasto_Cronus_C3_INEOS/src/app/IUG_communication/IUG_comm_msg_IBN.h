#ifndef _IUG_COMM_MSG_IBN_H_
#define _IUG_COMM_MSG_IBN_H_







boolean IUG_Comm_TEACHIN_CUR_ON_OFF( IUG_QUEUE_ENTRY_DATA_t * const que_dat, WBUS_MSG_DESCRIPTION_CTL_t * const wbus_msg_ctl );

boolean IUG_Comm_TEACHIN_CLOSE( IUG_QUEUE_ENTRY_DATA_t * const que_dat, WBUS_MSG_DESCRIPTION_CTL_t * const wbus_msg_ctl );

#endif


