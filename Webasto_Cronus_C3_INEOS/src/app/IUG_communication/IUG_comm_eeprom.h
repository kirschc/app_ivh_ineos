#ifndef __IUG_COMM_EEPROM_H_
#define __IUG_COMM_EEPROM_H_



#define IUG_EEP_RD_TIMEOUT_MS                   3500u 


#define IUG_EEP_RD_TIMEOUT_DETECTION_MS    1000u 


#define IUG_EEP_RD_TIMEOUT_DISPLAY_MS           1000u 



boolean     IUG_Comm_MsgEepromRead( IUG_QUEUE_ENTRY_DATA_t * const que_dat, WBUS_MSG_DESCRIPTION_CTL_t * const wbus_msg_ctl );


#endif 


