



#ifndef __IUG_COMM_CONFIG_H_
#define __IUG_COMM_CONFIG_H_



#define WBUSCONF_NEGRESPCODE_00_RESERVED                    (0x00u)

#define WBUSCONF_NEGRESPCODE_10_GENERAL_REJECT              (0x10u)

#define WBUSCONF_NEGRESPCODE_11_REQ_NOT_SUPPORTED           (0x11u)

#define WBUSCONF_NEGRESPCODE_12_PARAM_NOT_SUPPORTED         (0x12u)

#define WBUSCONF_NEGRESPCODE_13_OPERATION_FAILED            (0x13u)

#define WBUSCONF_NEGRESPCODE_14_ILLEGAL_IDENTIFIER          (0x14u)

#define WBUSCONF_NEGRESPCODE_21_DIAGROUTINE_ACTIVE          (0x21u)

#define WBUSCONF_NEGRESPCODE_22_CONDITIONS_NOT_CORRECT      (0x22u)

#define WBUSCONF_NEGRESPCODE_23_RESPONSE_TO_LONG            (0x23u)

#define WBUSCONF_NEGRESPCODE_24_WRONG_KEY                   (0x24u)

#define WBUSCONF_NEGRESPCODE_31_VALUE_OUT_OF_RANGE          (0x31u)

#define WBUSCONF_NEGRESPCODE_33_ACCESS_DENIED               (0x33u)

#define WBUSCONF_NEGRESPCODE_42_ADDRESS_INVALID_WRITE       (0x42u)

#define WBUSCONF_NEGRESPCODE_52_ADDRESS_INVALID_READ        (0x52u)

#define WBUSCONF_NEGRESPCODE_77_CHECKSUMERROR_IN_BLOCKMODE  (0x77u)

#define WBUSCONF_NEGRESPCODE_FF_RESERVED                    (0xFFu)





#endif 


