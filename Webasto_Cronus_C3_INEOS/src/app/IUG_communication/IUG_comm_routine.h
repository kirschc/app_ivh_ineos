#ifndef __IUG_COMM_ROUTINE_H_
#define __IUG_COMM_ROUTINE_H_


#define IUG_ROUTINE_FBL_RELEASED                0x5Au 
#define IUG_ROUTINE_RESET_RELEASED              0x5Bu 

#define IUG_ROUTINE_CTL_STA_NONE                0x00u   
#define IUG_ROUTINE_CTL_STA_SKIPPED             0x10u   
#define IUG_ROUTINE_CTL_STA_TRIGGERED           0x11u   
#define IUG_ROUTINE_CTL_STA_ACTIVE              0x12u   
#define IUG_ROUTINE_CTL_STA_FINISHED            0x21u   
#define IUG_ROUTINE_CTL_STA_CLOSED              0x22u   
#define IUG_ROUTINE_CTL_STA_TIMEOUT             0x2Fu   
#define IUG_ROUTINE_CTL_STA_BROKEN              0xFFu   

#define IUG_ROUTINE_RESULT_UNDONE               0x00u   
#define IUG_ROUTINE_RESULT_DONE_SUCCESS         0x01u   
#define IUG_ROUTINE_RESULT_DONE_UNSUCCESS       0x02u   
#define IUG_ROUTINE_RESULT_SKIPPED              0x03u   

#define IUG_ROUTINE_RES_CODE_ACTIVE             0x00u
#define IUG_ROUTINE_RES_CODE_FINISHED           0x01u
#define IUG_ROUTINE_RES_CODE_BROKEN             0xFFu

#define IUG_ROUTINE_START_MAX_BUFSIZE           100u


typedef enum
{
    ROUTINE_ID_00                       = 0x00u,   
    ROUTINE_ID_IBN_SCAN_WBUS_HTR        = 0x01u,   
    ROUTINE_ID_IBN_BE                   = 0x02u,   
    ROUTINE_ID_IBN_BE_ASSIGN            = 0x03u,   
    ROUTINE_ID_IBN_DOSING_PUMP_HG1      = 0x04u,   
    ROUTINE_ID_IBN_TEMP_SENS            = 0x05u,   
    ROUTINE_ID_IBN_CAR_STATE            = 0x06u,   
    ROUTINE_ID_IBN_OBD                  = 0x07u,   
    ROUTINE_ID_IBN_CAR_CLIMATE_DETECT   = 0x08u,   
    ROUTINE_ID_IBN_CAR_CLIMATE_CHECK    = 0x09u,   
#ifdef INIT_OP_CFG_CAN_LINK_EN
    ROUTINE_ID_IBN_SYS_TIME             = 0x0Du,   
#endif
    ROUTINE_ID_IBN_DISPLAY_STATE        = 0x10u,   
    ROUTINE_ID_IBN_HG_TEST_RUN_HG1      = 0x11u,   
    ROUTINE_ID_IBN_INACT_CAR_STATE      = 0x12u,   
    ROUTINE_ID_IBN_TEACH_IN_TELESTART   = 0x13u,   
    ROUTINE_ID_IBN_DOSING_PUMP_HG2      = 0x14u,   
    ROUTINE_ID_IBN_HG_TEST_RUN_HG2      = 0x15u,   
    ROUTINE_ID_IBN_COOLANT_PUMP_HG1     = 0x16u,   
    ROUTINE_ID_IBN_COOLANT_PUMP_HG2     = 0x17u,   
    ROUTINE_ID_IBN_SCAN_GHI_HTR         = 0x18u,   
    ROUTINE_ID_IBN_SCAN_T_LIN_R         = 0x19u,   

    ROUTINE_ID_ENUM_MAX,                  
    ROUTINE_ID_ENUM_FORCE_TYPE = 0x7F     
} enum_ROUTINE_ID_t;


typedef struct
{
    uint8_t     ctl_sta;

    uint8_t     req_id;
    uint8_t     req_refresh;
    uint8_t     req_break;          
    uint32_t    timeout_ms;
    uint32_t    timeout_timer;

} struct_ROUTINE_START_INTERFACE_t;


typedef struct
{
    enum_ROUTINE_ID_t           routine_id;
    uint8_t                     ibn_id;

} struct_ROUTINE_ID_DESCRIPTOR_ENTRY_t;


extern volatile struct_ROUTINE_START_INTERFACE_t    ext_IUG_ROUTINE_START_IF;
extern const struct_ROUTINE_ID_DESCRIPTOR_ENTRY_t   ext_ROUTINE_ID_DESCRIPTOR[];
extern const uint8_t                                ext_ROUTINE_ID_DESCRIPTOR_entries;


boolean     IUG_Comm_MsgROUTINE( IUG_QUEUE_ENTRY_DATA_t * const que_dat, WBUS_MSG_DESCRIPTION_CTL_t * const wbus_msg_ctl );
void        IUG_Comm_MsgROUTINE_FBL_Release( boolean release );

void IUG_Comm_MsgROUTINE_RESET_Release( uint8_t release );

uint8_t     IUG_Comm_MsgROUTINE_GetDescriptorIdx( const enum_ROUTINE_ID_t routine_id, uint16_t* routine_idx, uint8_t* intern_routine_id );
boolean     IUG_Comm_MsgROUTINE_Pending( IUG_QUEUE_ENTRY_DATA_t * const que_dat, WBUS_MSG_DESCRIPTION_CTL_t * const wbus_msg_ctl );
void        IUG_Comm_MsgROUTINE_Timeout( uint8_t delta_ms );


#endif 


