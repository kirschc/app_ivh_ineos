#ifndef __IUG_COMM_BTM_H_
#define __IUG_COMM_BTM_H_





boolean IUG_Comm_MsgBTM( IUG_QUEUE_ENTRY_DATA_t * const que_dat, WBUS_MSG_DESCRIPTION_CTL_t * const wbus_msg_ctl );
boolean IUG_Comm_MsgBTM_btm_is_active( IUG_QUEUE_ENTRY_DATA_t * const que_dat, struct_WBUS_MSG_DESCRIPTION_CTL_t * const wbus_msg_ctl_ptr );
void    IUG_Comm_MsgBTM_Timeout( uint8_t delta_ms );


#endif 


