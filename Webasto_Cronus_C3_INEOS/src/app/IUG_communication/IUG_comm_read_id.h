#ifndef __IUG_COMM_READ_ID_H_
#define __IUG_COMM_READ_ID_H_

boolean IUG_Comm_MsgIdentificationRequest( IUG_QUEUE_ENTRY_DATA_t * const que_dat, WBUS_MSG_DESCRIPTION_CTL_t * const wbus_msg_ctl );
void    IUG_Comm_MsgIdReq_Scann_GHI_Bus( IUG_COMM_RX_PARAM_t * const rx_par );
void    IUG_Comm_MsgIdReq_Scann_W_Bus( IUG_COMM_RX_PARAM_t * const rx_par );



#endif 

