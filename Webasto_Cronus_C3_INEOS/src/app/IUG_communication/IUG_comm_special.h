#ifndef __IUG_COMM_SPECIAL_H_
#define __IUG_COMM_SPECIAL_H_



#define IUG_COMM_SPECAS_NONE                        0x00u
#define IUG_COMM_SPECAS_WBUS_MBR_TEMP_SENSOR_INT    0x01u
#define IUG_COMM_SPECAS_WBUS_MBR_TEMP_SENSOR_EXT    0x02u



uint8_t     IUG_CommSpecialCase_Handler( IUG_QUEUE_ENTRY_DATA_t * const que_dat, const struct_IUG_EVALUATION_RESULT_t  eva_res );
uint8_t     IUG_CommSpecialCase_ServeTemperatureSensor( const uint8_t test_selector, IUG_QUEUE_ENTRY_DATA_t * const que_dat );
uint8_t     IUG_CommSpecialCase_Test( const uint8_t test_selector, const IUG_QUEUE_ENTRY_DATA_t * const que_dat );




#endif 


