#ifndef SFL_UTILS_H
#define SFL_UTILS_H

#include "hal_data_types.h"







void sfl_utils_init(void);

void sfl_utils_deinit(void);

uint8_t sfl_to_bcd(uint8_t natural_binary);

uint8_t sfl_to_bin(uint8_t bcd);


uint8_t sfl_one_asc_to_low_bcd(uint8_t asc_sign);

uint8_t sfl_two_asc_to_one_bcd(uint8_t* asc_sign);

#endif


