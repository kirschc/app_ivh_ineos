#ifndef SFL_CAN_DB_H
#define SFL_CAN_DB_H

#include "sfl_can_db_tables_data.h"






uint32_t sfl_can_db_get_value(uint32_t id);


uint32_t sfl_can_db_get_value_on_change(uint32_t id, uint8_t* changed);


void sfl_can_db_set_value(uint32_t id, uint32_t wert_int);


void sfl_can_db_set_transmit_flag(uint32_t id );


uint8_t* sfl_can_db_get_block_ptr(uint32_t id);


uint8_t sfl_can_db_block_received(uint32_t id, uint8_t reset);


uint8_t sfl_can_db_test_dp_value(uint32_t id);


void sfl_can_db_rx_wrapper(uint8_t p_bus_id, const struct_hal_can_frame *const ptr_can_msg);


uint8_t sfl_can_db_gateway(uint8_t bus_id, bios_can_msg_typ* msg);


void sfl_can_input_block_to_db(uint8_t bus_id, bios_can_msg_typ* msg);


void sfl_can_db_output_to_bus(void);


uint8_t sfl_can_db_stop_transmission(const uint8_t can_bus, const boolean status);

#define sfl_can_db_transmit_deactivate( status ) sfl_can_db_stop_transmission( (dyn_CAN_BUS_MAX), status )

uint8_t sfl_can_db_stop_gateway_for_known_ids(const uint8_t can_bus, const boolean status);

uint8_t sfl_can_db_stop_gateway_for_unknown_ids(const uint8_t can_bus, const boolean status);

uint8_t sfl_bit_weight_32(uint32_t val);

uint8_t sfl_bit_weight_8(uint32_t val);

void sfl_os_can_copy_msg(bios_can_msg_typ* src, bios_can_msg_typ* dst);

enum_HAL_CAN_RETURN_VALUE sfl_can_db_tx_wrapper(uint8_t p_bus_id, bios_can_msg_typ* msg);

enum_HAL_CAN_RETURN_VALUE sfl_can_send_message( const struct_hal_can_handle *const ptr_can_handle, const uint32_t can_id,  const uint8_t extended_id, const uint8_t can_dlc, \
                                                const uint8_t data_byte_0, const uint8_t data_byte_1, const uint8_t data_byte_2, const uint8_t data_byte_3, \
                                                const uint8_t data_byte_4, const uint8_t data_byte_5, const uint8_t data_byte_6, const uint8_t data_byte_7);

boolean sfl_can_check_for_any_received_msg(uint8_t p_bus_id, boolean reset_flag);

#endif



