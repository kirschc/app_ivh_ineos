#include "sfl_can_db_tables_data.h"
#include "sfl_db.h"
#include <string.h>


can_db_typ can_db;
can_block_db_ram_typ* ext_ptr_can_block_db_ram                              = (NULL_PTR); 
bios_can_baudrate_typ can_db_baudrate;
volatile const can_datenpunkt_db_const_typ* ext_ptr_can_datenpunkt_db_const = (NULL_PTR);
volatile const can_block_db_const_typ* ext_ptr_can_block_db_const           = (NULL_PTR);
volatile const can_bus_db_const_typ* ext_ptr_can_bus_db_const               = (NULL_PTR);
volatile const can_gateway_db_const_typ* ext_ptr_can_gateway_db_const       = (NULL_PTR);

const struct_can_baud_map_table mgl_can_baud_map_table[] =
{
    { "n/a"                  , (NONE)                 , HAL_CAN_BAUD_INVALID },
    { "1000 kBit"            , (BIOS_CAN_1MBIT)       , HAL_CAN_BAUD_1000    },
    { "800 kBit"             , (BIOS_CAN_800KBIT)     , HAL_CAN_BAUD_INVALID },
    { "500 kBit"             , (BIOS_CAN_500KBIT)     , HAL_CAN_BAUD_500     },
    { "250 kBit"             , (BIOS_CAN_250KBIT)     , HAL_CAN_BAUD_250     },
    { "125 kBit"             , (BIOS_CAN_125KBIT)     , HAL_CAN_BAUD_125     },
    { "100 kBit"             , (BIOS_CAN_100KBIT)     , HAL_CAN_BAUD_100     },
    { "83.33 kBit"           , (BIOS_CAN_83_33KBIT)   , HAL_CAN_BAUD_83      },
    { "50 kBit"              , (BIOS_CAN_50KBIT)      , HAL_CAN_BAUD_50      },
    { "33.33 kBit"           , (BIOS_CAN_33_33KBIT)   , HAL_CAN_BAUD_33      },
    { "20 kBit"              , (BIOS_CAN_20KBIT)      , HAL_CAN_BAUD_20      },
    { "10 kBit"              , (BIOS_CAN_10KBIT)      , HAL_CAN_BAUD_10      }
};

const uint8_t mgl_entries_can_baud_map_table = (sizeof(mgl_can_baud_map_table) / sizeof(struct_can_baud_map_table));

void sfl_can_db_tables_data_init(struct_hal_can_handle *const ptr_can_handle, const enum_CAN_DB_TABLES can_db_table)
{
    uint16_t i;

    can_db.BAUDRATE = CAN_BAUDRATE;

    if( can_db_table >= CAN_DB_TABLES_ENUM_MAX )
    {
        can_db.BUS_1          = 0x00u;
        can_db.DP_MAX         = 0x00u;
        can_db.BUS_MAX        = 0x00u;
        can_db.BLOCK_MAX      = 0x00u;
        can_db.GATEWAY_DB_MAX = 0x00u; 

        sfl_can_db_set_table_ptr(CAN_DB_TABLES_ENUM_MAX);
    }
    else
    {
        can_db.BUS_1          = ext_can_db_tables_cfg[can_db_table].can_bus_1;
        can_db.DP_MAX         = ext_can_db_tables_cfg[can_db_table].can_dp_db_cfg.can_dp_db_enum_max;
        can_db.BUS_MAX        = ext_can_db_tables_cfg[can_db_table].can_bus_db_cfg.can_bus_db_enum_max;
        can_db.BLOCK_MAX      = ext_can_db_tables_cfg[can_db_table].can_block_db_cfg.can_block_db_enum_max;
        can_db.GATEWAY_DB_MAX = ext_can_db_tables_cfg[can_db_table].can_gateway_db_cfg.can_gateway_db_enum_max; 

        sfl_can_db_set_table_ptr(can_db_table);

        sfl_can_db_tables_get_can_usr_baud(ptr_can_handle);

        for( i = 0 ; i < can_db.BLOCK_MAX ; i++ )
        {
            if( (FALSE) != ext_ptr_can_block_db_const[i].mux_length )
            {
                sfl_db_put_signal_value_to_data_block(ext_ptr_can_block_db_const[i].mux_val, \
                                                      ext_ptr_can_block_db_ram[i].msg.data, \
                                                      ext_ptr_can_block_db_const[i].mux_start, \
                                                      ext_ptr_can_block_db_const[i].mux_length, \
                                                      (DF_INTEL));
            }
            else
            {
            }
        }
    }
}

void sfl_can_db_tables_get_can_usr_baud(struct_hal_can_handle *const ptr_can_handle)
{
    uint8_t idx;
    uint8_t idx_bus;

    if( (NULL_PTR) != ptr_can_handle )
    {
        for( idx_bus = 0u; idx_bus < can_db.BUS_MAX; idx_bus++ )
        {
            ptr_can_handle[idx_bus].can_usr_baudrate = HAL_CAN_BAUD_INVALID;

            for( idx = 0u; idx < mgl_entries_can_baud_map_table; idx++ )
            {
                if( (BIOS_CAN_EXT_BAUDRATE) == ext_ptr_can_bus_db_const[idx_bus].bios_baudrate_id )
                {
                    if( 0u == strncmp( (const char*)&ext_ptr_can_bus_db_const[idx_bus].baudrate.name, \
                                       (const char*)&mgl_can_baud_map_table[idx].can_baud_usr_string[0u], \
                                       (BIOS_CAN_BAUDRATE_NAME_LEN)) )
                    {
                        ptr_can_handle[idx_bus].can_usr_baudrate = mgl_can_baud_map_table[idx].can_baud_hal;

                        idx = mgl_entries_can_baud_map_table;
                    }
                    else
                    {
                    }
                }
                else
                {
                    if( ext_ptr_can_bus_db_const[idx_bus].bios_baudrate_id == mgl_can_baud_map_table[idx].can_baud_usr_define )
                    {
                        ptr_can_handle[idx_bus].can_usr_baudrate = mgl_can_baud_map_table[idx].can_baud_hal;

                        idx = mgl_entries_can_baud_map_table;
                    }
                    else
                    {
                    }
                }
            }
        }
    }
    else
    {
    }
}

static void sfl_can_db_set_table_ptr(enum_CAN_DB_TABLES can_db_table)
{
    if( can_db_table >= CAN_DB_TABLES_ENUM_MAX )
    {
        ext_ptr_can_block_db_ram        = (NULL_PTR);
        ext_ptr_can_datenpunkt_db_const = (NULL_PTR);
        ext_ptr_can_block_db_const      = (NULL_PTR);
        ext_ptr_can_bus_db_const        = (NULL_PTR);
        ext_ptr_can_gateway_db_const    = (NULL_PTR);
    }
    else
    {
        if( &(ext_can_db_tables_cfg[can_db_table].can_block_db_ram[0u]) != (NULL_PTR) )
        {
            ext_ptr_can_block_db_ram = (can_block_db_ram_typ*)&(ext_can_db_tables_cfg[can_db_table].can_block_db_ram[0u]);
        }
        else
        {
        }

        if( &(ext_can_db_tables_cfg[can_db_table].can_dp_db_cfg.can_dp_db[0u]) != (NULL_PTR) )
        {
            ext_ptr_can_datenpunkt_db_const = (can_datenpunkt_db_const_typ*)&(ext_can_db_tables_cfg[can_db_table].can_dp_db_cfg.can_dp_db[0u]);
        }
        else
        {
        }

        if( &(ext_can_db_tables_cfg[can_db_table].can_block_db_cfg.can_block_db[0u]) != (NULL_PTR) )
        {
            ext_ptr_can_block_db_const = (can_block_db_const_typ*)&(ext_can_db_tables_cfg[can_db_table].can_block_db_cfg.can_block_db[0u]);
        }
        else
        {
        }

        if( &(ext_can_db_tables_cfg[can_db_table].can_bus_db_cfg.can_bus_db[0u]) != (NULL_PTR) )
        {
            ext_ptr_can_bus_db_const = (can_bus_db_const_typ*)&(ext_can_db_tables_cfg[can_db_table].can_bus_db_cfg.can_bus_db[0u]);
        }
        else
        {
        }

        if( &(ext_can_db_tables_cfg[can_db_table].can_gateway_db_cfg.can_gateway_db[0u]) != (NULL_PTR) )
        {
            ext_ptr_can_gateway_db_const = (can_gateway_db_const_typ*)&(ext_can_db_tables_cfg[can_db_table].can_gateway_db_cfg.can_gateway_db[0u]);
        }
        else
        {
        }
    }
}





