#ifndef SFL_BL_PROTOCOL_H
#define SFL_BL_PROTOCOL_H

#define USE_HAL             1   
#define HCS08_MRS_LIBRARY   0   

#define USE_SPECIAL_LIB_VERSION 

#ifdef USE_SPECIAL_LIB_VERSION

    #include "IUG_appl_version.h"

    #define MRS_LIBRARY_VERSION (   ( 1000u * (BASE_SW_VERSION_MAJOR) ) \
                                  + ( 100u  * (BASE_SW_VERSION_MINOR) ) \
                                  + (         (BASE_SW_VERSION_BUILD) )  )
#else
    #define MRS_LIBRARY_VERSION 1u
#endif


#include "hal_data_types.h"

#if USE_HAL == 1

    #include "sfl_timer.h"
    #include "hal_can.h"
    #include "hal_nvm.h"
    #include "hal_sys.h"

#elif HCS08_MRS_LIBRARY == 1


#endif



#define PAR_OFFSET          2u

#define ERR_OK           0u             
#define ERR_SPEED        1u             
#define ERR_RANGE        2u             
#define ERR_VALUE        3u             
#define ERR_OVERFLOW     4u             
#define ERR_MATH         5u             
#define ERR_ENABLED      6u             
#define ERR_DISABLED     7u             
#define ERR_BUSY         8u             
#define ERR_NOTAVAIL     9u             
#define ERR_RXEMPTY      10u            
#define ERR_TXFULL       11u            
#define ERR_BUSOFF       12u            
#define ERR_OVERRUN      13u            
#define ERR_FRAMING      14u            
#define ERR_PARITY       15u            
#define ERR_NOISE        16u            
#define ERR_IDLE         17u            
#define ERR_FAULT        18u            
#define ERR_BREAK        19u            
#define ERR_CRC          20u            
#define ERR_ARBITR       21u            
#define ERR_PROTECT      22u            
#define ERR_UNDERFLOW    23u            
#define ERR_UNDERRUN     24u            
#define ERR_COMMON       25u            
#define ERR_LINSYNC      26u            
#define ERR_NOT_OK       27u            

#define PAR_LEN_DATA_UNUSED      2u
#define PAR_LEN_ID               2u
#define PAR_LEN_SERIALNUMBER     4u
#define PAR_LEN_PARTNUMBER       12u
#define PAR_LEN_DRAWINGUMBER     12u
#define PAR_LEN_NAME             20u
#define PAR_LEN_ORDERNUMBER      8u
#define PAR_LEN_TEST_DATE        8u
#define PAR_LEN_HW_VERSION       2u
#define PAR_LEN_RESETCOUNTER     1u
#define PAR_LEN_LIB_VERSION      2u
#define PAR_LEN_RESETREASON_LVD  1u
#define PAR_LEN_RESETREASON_LOC  1u
#define PAR_LEN_RESETREASON_ILAD 1u
#define PAR_LEN_RESETREASON_ILOP 1u
#define PAR_LEN_RESETREASON_COP  1u
#define PAR_LEN_MCU_TYPE         1u
#define PAR_LEN_HW_CAN_ACTIVE    1u
#define PAR_LEN_RESERVE1         3u
#define PAR_LEN_BL_VERSION       2u
#define PAR_LEN_PROG_STATE       2u
#define PAR_LEN_PORTBYTE1        2u
#define PAR_LEN_PORTBYTE2        2u
#define PAR_LEN_BL_BAUDRATE1     2u
#define PAR_LEN_BL_BAUDRATE2     2u
#define PAR_LEN_BL_ID_EXT1       1u
#define PAR_LEN_BL_ID1           4u
#define PAR_LEN_BL_ID_CRC1       1u
#define PAR_LEN_BL_ID_EXT2       1u
#define PAR_LEN_BL_ID2           4u
#define PAR_LEN_BL_ID_CRC2       1u
#define PAR_LEN_SW_VERSION       20u
#define PAR_LEN_MODULENAME       30u
#define PAR_LEN_BL_CANBUS        1u
#define PAR_LEN_WD_TIMEOUT       2u
#define PAR_LEN_STARTUP_TIME     2u
#define PAR_LEN_RESERVE2         7u

#define PAR_MAX_DATA_SIZE (   (PAR_LEN_DATA_UNUSED) + (PAR_LEN_ID) + (PAR_LEN_SERIALNUMBER) + (PAR_LEN_PARTNUMBER) + (PAR_LEN_DRAWINGUMBER)    \
                            + (PAR_LEN_NAME) + (PAR_LEN_ORDERNUMBER) + (PAR_LEN_TEST_DATE) + (PAR_LEN_HW_VERSION) + (PAR_LEN_RESETCOUNTER)     \
                            + (PAR_LEN_LIB_VERSION) + (PAR_LEN_RESETREASON_LVD) + (PAR_LEN_RESETREASON_LOC) + (PAR_LEN_RESETREASON_ILAD)       \
                            + (PAR_LEN_RESETREASON_ILOP) + (PAR_LEN_RESETREASON_COP) + (PAR_LEN_MCU_TYPE) + (PAR_LEN_HW_CAN_ACTIVE)            \
                            + (PAR_LEN_RESERVE1) + (PAR_LEN_BL_VERSION) + (PAR_LEN_PROG_STATE) + (PAR_LEN_PORTBYTE1) + (PAR_LEN_PORTBYTE2)     \
                            + (PAR_LEN_BL_BAUDRATE1) + (PAR_LEN_BL_BAUDRATE2) + (PAR_LEN_BL_ID_EXT1) + (PAR_LEN_BL_ID1) + (PAR_LEN_BL_ID_CRC1) \
                            + (PAR_LEN_BL_ID_EXT2) + (PAR_LEN_BL_ID2) + (PAR_LEN_BL_ID_CRC2) + (PAR_LEN_SW_VERSION) + (PAR_LEN_MODULENAME)     \
                            + (PAR_LEN_BL_CANBUS) + (PAR_LEN_WD_TIMEOUT) + (PAR_LEN_STARTUP_TIME) + (PAR_LEN_RESERVE2)   )

#define C_BL_ID             (0x1FFFFFF0u)
#define C_BL_ID_EXT         (1u)
#define C_BL_ID_NIBBLE      (0u)


#define CHECK_BAUDRATE(x) (((x) & 0xFFu) >= E_BAUD_1MBIT && ((x) & 0xFFu) < E_BAUD_MAX)
#define INV_16(x) (uint16_t)(((~x) << 8) + (x))
#define CHECK_INV_16(x) (((x) & 0xFFu) == (~((x) >> 8) & 0xFFu))
#define SWAP_16(x) (((x) >>  8) & 0xFFu) | (((x) << 8) & 0xFF00u)
#define SWAP_32(x) (((x) >> 24) & 0xFFu) | (((x) << 8) & 0xFF0000u) | (((x) >> 8) & 0xFF00u) | (((x) << 24) & 0xFF000000u)

#define NIBBLE_H_L(a,b) ((a)|((b) << 4) & 0xFFu)

#define CALC_CRC(a,b,x) ((~((((x) >> 24) & 0xFFu)+(((x) >> 16u) & 0xFFu) + (((x) >> 8u) & 0xFFu) + ((x) & 0xFFu) + NIBBLE_H_L((a),(b)))) & 0xFFu)

#define C_BL_ID_CNT (ext_bl_par_data[PAR_BL_ID_EXT1].length + ext_bl_par_data[PAR_BL_ID1].length + ext_bl_par_data[PAR_BL_ID_CRC1].length)

#if USE_HAL == 1

    #define BL_EEPROM_GET_DATA(addr,pu8Data,d_size)   hal_nvm_eeprom_read( addr, d_size, pu8Data)
    #define BL_EEPROM_SET_DATA(addr,pu8Data,d_size)   hal_nvm_eeprom_write(addr, d_size, pu8Data)
    #define BL_EEPROM_GET_BYTE(addr, pu8Data)         hal_nvm_eeprom_read( addr, 1u,     pu8Data)
    #define BL_EEPROM_SET_BYTE(addr, pu8Data)         hal_nvm_eeprom_write(addr, 1u,     pu8Data)

    #define TRIGGER_BL_ACCESS() (void)sfl_timer_set_timestamp(&mgl_ti_access, HAL_PRECISION_1S);

    #define RESET()         0

#elif HCS08_MRS_LIBRARY == 1

    #define BL_EEPROM_GET_DATA(addr,pu8Data,d_size)   IEE1_GetData((IEE1_TAddress)addr, pu8Data, d_size)
    #define BL_EEPROM_SET_DATA(addr,pu8Data,d_size)   IEE1_SetData((IEE1_TAddress)addr, pu8Data, d_size)
    #define BL_EEPROM_GET_BYTE(addr,data)             IEE1_GetByte((IEE1_TAddress)addr,data)
    #define BL_EEPROM_SET_BYTE(addr,data)             IEE1_SetByte((IEE1_TAddress)addr,data)

    #define TRIGGER_BL_ACCESS() os_timestamp(&mgl_ti_access, OS_1000ms);

    #define ILLEGAL_OPCODE 0x9E00
    #define RESET()  __asm DCW ILLEGAL_OPCODE

#endif

typedef enum
{
    E_ACCESS_NONE                = 0u,
    E_ACCESS_BL                      ,
    E_ACCESS_CAL                     ,
    E_ACCESS_USER                    ,
    E_ACCESS_ALL                     ,
    E_ACCESS_MAX
}t_access;

typedef enum
{
    E_SUB_BL_ADDR                = 0u,
    E_SUB_PC_CMD                     ,
    E_SUB_BL_RESP                    ,  
    E_SUB_PC_FLASH_DATA              ,
    E_SUB_BL_EEP_DATA                ,
    E_SUB_PC_EEP_DATA                ,
    E_SUB_BL_HW_RESP                 ,
    E_SUB_PC_HW_CMD                  ,
    E_SUB_MAX
}t_bl_sub;

typedef enum
{
    E_CHECK_NONE                 = 0u,
    E_CHECK_SN                       ,
    E_CHECK_DATA                     ,
    E_CHECK_MAX
}t_check;

typedef enum
{
    E_SEND_SN_SCAN               = 0u,
    E_SEND_SN_DATA                   ,
    E_SEND_DATA                      ,
    E_SEND_SPECIFIC                  ,
    E_SEND_MAX
}t_send;

typedef enum
{
    E_BAUD_1MBIT                 = 1u,
    E_BAUD_800KBIT                   ,
    E_BAUD_500KBIT                   ,
    E_BAUD_250KBIT                   ,
    E_BAUD_125KBIT                   ,
    E_BAUD_50KBIT                    ,
    E_BAUD_20KBIT                    ,
    E_BAUD_10KBIT                    ,
    E_BAUD_100KBIT                   ,
    E_BAUD_83KBIT                    ,
    E_BAUD_33KBIT                    ,
    E_BAUD_MAX
} enum_bl_baudrate_t;

typedef enum
{
    E_PROG_OK                    = 0u,  
    E_PROG_STOP_PARAMETER            ,  
    E_PROG_STOP_RESET                ,  
    E_PROG_STOP_USER                 ,  
    E_PROG_STOP_MASTER               ,  
    E_PROG_MAX
} enum_prog_state_t;

typedef enum
{
    T_NUMBER                     = 0u,
    T_ASCII                          ,
    T_RESERVE                        ,
    T_MAX

}t_par_datatype;

typedef enum E_BL_MSG
{
    E_BL_SCAN                    =0u,
    E_BL_ADDRESSING                 ,
    E_BL_PROG_STOP_MASTER           ,
    E_BL_PROG_START_MASTER          ,
    E_BL_PROG_STOP_USER             ,
    E_BL_PROG_START_USER            ,
    E_BL_EEPROM_WRITE_BL            ,
    E_BL_EEPROM_WRITE_CAL           ,
    E_BL_EEPROM_WRITE_USER          ,
    E_BL_EEPROM_WRITE_ALL           ,
    E_BL_EEPROM_WRITE_STOP          ,
    E_BL_EEPROM_WRITE               ,
    E_BL_EEPROM_READ                ,
#ifdef BOOTLOADER_FLASH_ACTIVE
    E_BL_FLASH_REQUEST              ,
    E_BL_FLASH_CLEAR_APPL           ,
    E_BL_FLASH_WRITE                ,
#else
    E_BL_FLASH_REQUEST              ,
#endif
    E_BL_MAX

}t_bl_msg;

enum E_PAR_DATA
{
    PAR_ID                      = 0u,         
    PAR_SERIALNUMBER                ,
    PAR_PARTNUMBER                  ,
    PAR_DRAWINGUMBER                ,
    PAR_NAME                        ,
    PAR_ORDERNUMBER                 ,         
    PAR_TEST_DATE                   ,
    PAR_HW_VERSION                  ,
    PAR_RESETCOUNTER                ,
    PAR_LIB_VERSION                 ,
    PAR_RESETREASON_LVD             ,         
    PAR_RESETREASON_LOC             ,
    PAR_RESETREASON_ILAD            ,
    PAR_RESETREASON_ILOP            ,
    PAR_RESETREASON_COP             ,
    PAR_MCU_TYPE                    ,         
    PAR_HW_CAN_ACTIVE               ,
    PAR_RESERVE1                    ,
    PAR_BL_VERSION                  ,
    PAR_PROG_STATE                  ,
    PAR_PORTBYTE1                   ,         
    PAR_PORTBYTE2                   ,
    PAR_BL_BAUDRATE1                ,
    PAR_BL_BAUDRATE2                ,
    PAR_BL_ID_EXT1                  ,
    PAR_BL_ID1                      ,         
    PAR_BL_ID_CRC1                  ,
    PAR_BL_ID_EXT2                  ,
    PAR_BL_ID2                      ,
    PAR_BL_ID_CRC2                  ,
    PAR_SW_VERSION                  ,         
    PAR_MODULENAME                  ,
    PAR_BL_CANBUS                   ,
    PAR_WD_TIMEOUT                  ,
    PAR_STARTUP_TIME                ,
    PAR_RESERVE2                    ,         
    PAR_MAX
};

enum E_MCU_TYPE
{
    E_MCU_TYPE_NONE = 0,
    E_MCU_TYPE_HCS08,
    E_MCU_TYPE_HCS12,
    E_MCU_TYPE_HCS08DZ128,
    E_MCU_TYPE_MK10xx,
    E_MCU_TYPE_RH850
};

#define MCU_TYPE E_MCU_TYPE_RH850

enum E_RESET_TYPE
{
    E_RESET_TYPE_NONE = 0,
    E_RESET_TYPE_BL,
    E_RESET_TYPE_APP
};
extern uint8_t ext_flag_trigger_app_reset;

typedef struct
{
        uint32_t    id;             
        uint8_t     dlc;            
        uint8_t     data[8];        
        uint16_t    can_timestamp;  

}struct_bl_can_frame;

#define BL_CAN_RX_FIFO_BUFFER_LEN (16u)
typedef struct
{
    uint8_t write_ptr;
    uint8_t read_ptr;
    uint8_t overrun;
    struct_bl_can_frame buffer[BL_CAN_RX_FIFO_BUFFER_LEN];
} struct_bl_can_rx_fifo;

struct st_bl_msg
{
    t_bl_sub d_rx_sub;
    uint8_t  d_in1[2];
    t_check  d_check;
    uint8_t  d_rx_dlc;
    uint8_t  d_in2[6];
    uint8_t (*d_ptr_callback)(uint8_t, struct_bl_can_frame*, struct_bl_can_frame*);
    t_send   d_send;
    t_bl_sub d_tx_sub;
    uint8_t  d_tx_dlc;
    uint8_t  d_send_data[8];
};

struct st_ascii
{
    uint8_t  par_name;
    uint8_t  str[30];
};

struct struc_par_data
{
    uint8_t length;
    t_par_datatype data_type;
    uint32_t val_default;
};

struct struct_factory_data
{
    uint8_t factory_data_unused[2u];
    uint8_t factory_data_start_marker[2u];
    uint8_t factory_data_serialnumber[4u];        
    uint8_t factory_data_partnumber[12u];
    uint8_t factory_data_drawingumber[12u];
    uint8_t factory_data_name[20u];
    uint8_t factory_data_ordernumber[8u];
    uint8_t factory_data_test_date[8u];
    uint8_t factory_data_hw_version[2u];
    uint8_t factory_data_resetcounter;        
    uint8_t factory_data_lib_version[2u];         
    uint8_t factory_data_resetreason_counter[5u]; 
    uint8_t factory_data_mcu_type;            
    uint8_t factory_data_hw_can_active;       
    uint8_t factory_data_reserve1[3u];
    uint8_t factory_data_bl_version[2u];          
    uint8_t factory_data_prog_state[2u];          
    uint8_t factory_data_portbyte1[2u];           
    uint8_t factory_data_portbyte2[2u];           
    uint8_t factory_data_bl_baudrate1[2u];        
    uint8_t factory_data_bl_baudrate2[2u];
    uint8_t factory_data_bl_id_ext1;          
    uint8_t factory_data_bl_id1[4u];
    uint8_t factory_data_bl_id_crc1;
    uint8_t factory_data_bl_id_ext2;          
    uint8_t factory_data_bl_id2[4u];
    uint8_t factory_data_bl_id_crc2;
    uint8_t factory_data_sw_version[20u];
    uint8_t factory_data_modulename[30u];         
    uint8_t factory_data_bl_canbus;           
    uint8_t factory_data_wd_timeout[2u];          
    uint8_t factory_data_startup_time[2u];        
    uint8_t factory_data_config_data_res[7u];
};

extern uint8_t ext_bl_flasher_active;
extern uint32_t ext_bl_sub_fr[E_SUB_MAX];
extern uint8_t ext_bl_par_addr[PAR_MAX];
extern const struct struc_par_data ext_bl_par_data[PAR_MAX];
extern uint8_t ext_bl_access;
extern struct struct_factory_data ext_bl_factory_data;


#if USE_HAL == 1

    void sfl_bl_init(struct_hal_can_handle* handle);

#elif HCS08_MRS_LIBRARY == 1

    void sfl_bl_init(void);

#endif

 void sfl_bl_cyclic(void);

uint8_t sfl_bl_set_eeprom_access(uint8_t p_step, struct_bl_can_frame* p_msg_in, struct_bl_can_frame* p_msg_out);

 uint8_t sfl_bl_can_read_eeprom(uint8_t p_step, struct_bl_can_frame* p_msg_in, struct_bl_can_frame* p_msg_out);

 uint8_t sfl_bl_can_write_eeprom(uint8_t p_step, struct_bl_can_frame* p_msg_in, struct_bl_can_frame* p_msg_out);

uint8_t sfl_bl_set_program_state_can(uint8_t p_step, struct_bl_can_frame* p_msg_in, struct_bl_can_frame* p_msg_out);

uint8_t sfl_bl_set_program_state(enum_prog_state_t state);


uint8_t* sfl_bl_get_ascii_ptr(uint8_t p_index);

uint8_t sfl_bl_check_port_bit(uint8_t p_byte);

uint8_t sfl_bl_write_default_value(uint8_t i);

 uint8_t sfl_bl_check_crc(uint8_t *p_data, uint8_t p_start, uint8_t p_cnt);

uint8_t sfl_bl_can_send(uint32_t p_id, uint8_t p_dlc, uint8_t p_b0, uint8_t p_b1, uint8_t p_b2, uint8_t p_b3, uint8_t p_b4, uint8_t p_b5, uint8_t p_b6, uint8_t p_b7);

void sfl_bl_can_receive(struct_bl_can_frame* msg);

#if USE_HAL == 1

     void sfl_bl_can_receive_pre(const struct_hal_can_frame *const ptr_can_msg);

#elif HCS08_MRS_LIBRARY == 1

    void sfl_bl_can_receive_pre(bios_can_msg_typ* msg);

#endif

uint8_t sfl_bl_set_flasher_state(uint8_t p_step, struct_bl_can_frame* p_msg_in, struct_bl_can_frame* p_msg_out);

void sfl_bl_change_can_baudrate(struct_hal_can_handle* handle, enum_bl_baudrate_t baudrate);

void sfl_bl_get_hw_version(uint8_t *hw_version);

static void sfl_bl_get_factory_data(void);

void sfl_bl_set_can_baudrate(const enum_bl_baudrate_t baud_rate);

enum_prog_state_t sfl_bl_get_program_state(void);

void sfl_bl_set_reset_counter(const uint8_t reset_counter_val);

#endif


