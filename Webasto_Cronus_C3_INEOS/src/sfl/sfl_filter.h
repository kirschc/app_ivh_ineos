#ifndef SFL_FILTER_H
#define SFL_FILTER_H


#include "hal_data_types.h"

#define SFL_PT1_FILTER_CONST_MAX                100u


typedef struct
{
    uint8_t filterConst;
    int32_t internalMeasValueLast;
    int32_t internalMeasValue;

}struct_PT1_FILTER;

typedef struct
{
    uint8_t filterConst;
    uint16_t internalStateLast;
    uint16_t internalSwitchCounter;

}struct_DIGITAL_FILTER;

typedef enum
{
    SFL_FILTER_OK                               = 0u,
    SFL_FILTER_ERR_CONST_INVALID

}enum_SFL_FILTER_RETURN_VALUE;


enum_SFL_FILTER_RETURN_VALUE sfl_pt1_filter_init(struct_PT1_FILTER* ptrPt1Filter, uint8_t filterConst);


enum_SFL_FILTER_RETURN_VALUE sfl_digital_filter_init(struct_DIGITAL_FILTER* ptrDigitalFilter, uint8_t filterConst);

int32_t sfl_pt1_filter(struct_PT1_FILTER* ptrPt1Filter, int32_t MeasValue);


uint16_t sfl_digital_filter(struct_DIGITAL_FILTER* ptrDigitalFilter, uint16_t state);


#endif


