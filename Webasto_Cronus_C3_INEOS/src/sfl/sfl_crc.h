#ifndef SFL_CRC_H
#define SFL_CRC_H


#include "hal_data_types.h"

#define SFL_CRC32_START     0xFFFFFFFFu





uint32_t sfl_crc32_init( const void* data, uint32_t size);

uint32_t sfl_crc32_update(uint32_t crc, const void* data, uint32_t size);


uint32_t sfl_crc32_hw(uint32_t crc, const void* data, uint32_t size);
#endif


