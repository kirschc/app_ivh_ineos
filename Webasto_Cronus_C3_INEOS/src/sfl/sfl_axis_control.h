#ifndef SFL_AXIS_CONTROL_H
#define SFL_AXIS_CONTROL_H

#include "hal_data_types.h"




typedef enum
{
    SFL_AXIS_CONTROL_OK                     = 0u,
    SFL_AXIS_CONTROL_CONFIGURATION_ERROR

} enum_SFL_AXIS_CONTROL_RETURN_VALUE;

typedef enum
{
    SFL_AXIS_CONTROL_DRIVE_MODE_STOP            = 0u,                
    SFL_AXIS_CONTROL_DRIVE_MODE_BRAKE               ,                
    SFL_AXIS_CONTROL_DRIVE_MODE_MOVE_LEFT           ,                
    SFL_AXIS_CONTROL_DRIVE_MODE_MOVE_RIGHT          ,                
    SFL_AXIS_CONTROL_DRIVE_MODE_INVALID                              

} enum_SFL_AXIS_CONTROL_DRIVE_MODE;

typedef enum
{
    SFL_AXIS_CONTROL_STATE_IDLE                                          = 0u,  
    SFL_AXIS_CONTROL_STATE_ACTIVE                                            ,  
    SFL_AXIS_CONTROL_STATE_SUCCESS                                           ,  
    SFL_AXIS_CONTROL_STATE_CONDITION_CURRENT_EXCEEDED_PERMANENT_COMPLIED     ,  
    SFL_AXIS_CONTROL_STATE_CONDITION_CURRENT_EXCEEDED_COMPLIED               ,  
    SFL_AXIS_CONTROL_STATE_CONDITION_CURRENT_TOO_LOW_COMPLIED                ,  
    SFL_AXIS_CONTROL_STATE_CONDITION_COUNTS_DRIVEN_COMPLIED                  ,  
    SFL_AXIS_CONTROL_STATE_CONDITION_TIME_ELAPSED_COMPLIED                   ,  
    SFL_AXIS_CONTROL_STATE_CONDITION_STALLED_COMPLIED                        ,  
    SFL_AXIS_CONTROL_STATE_ERROR                                                

} enum_SFL_AXIS_CONTROL_STATE;

typedef enum
{
    SFL_AXIS_CONTROL_CONDITION_STATE_UNUSED             = 0u,  
    SFL_AXIS_CONTROL_CONDITION_STATE_INITIALIZE             ,  
    SFL_AXIS_CONTROL_CONDITION_STATE_COMPLIED               ,  
    SFL_AXIS_CONTROL_CONDITION_STATE_NOT_COMPLIED              

} enum_SFL_AXIS_CONTROL_CONDITION_STATE;

typedef enum
{
    SFL_AXIS_CONTROL_CONDITION_LIST_STATE_NOT_COMPLIED                         =0u,  
    SFL_AXIS_CONTROL_CONDITION_LIST_STATE_CURRENT_EXCEEDED_PERMANENT_COMPLIED     ,  
    SFL_AXIS_CONTROL_CONDITION_LIST_STATE_CURRENT_EXCEEDED_COMPLIED               ,  
    SFL_AXIS_CONTROL_CONDITION_LIST_STATE_CURRENT_TOO_LOW_COMPLIED                ,  
    SFL_AXIS_CONTROL_CONDITION_LIST_STATE_COUNTS_DRIVEN_COMPLIED                  ,  
    SFL_AXIS_CONTROL_CONDITION_LIST_STATE_TIME_ELAPSED_COMPLIED                   ,  
    SFL_AXIS_CONTROL_CONDITION_LIST_STATE_STALLED_COMPLIED                           

} enum_SFL_AXIS_CONTROL_CONDITION_LIST_STATE;




typedef struct
{
    int16_t                        setValuePosition;     
    int16_t                        setValueSpeed;        
    uint16_t                       setValueCurrentLimit; 

} struct_SFL_AXIS_CONTROL_SET_DATA;

typedef struct
{
    int16_t                        measValuePosition;     
    int16_t                        measValuePositionLast; 
    int16_t                        measValueVelocity;     
    uint16_t                       measValueCurrent;      

} struct_SFL_AXIS_CONTROL_MEASURED_DATA;

typedef struct
{
    uint16_t                               cmdValueDutyCycle;               
    enum_SFL_AXIS_CONTROL_DRIVE_MODE       cmdValueDriveMode;               
    uint16_t                               internalCmdValueDutyCycleLast;   
    enum_SFL_AXIS_CONTROL_DRIVE_MODE       internalCmdValueDriveModeLast;   

} struct_SFL_AXIS_CONTROL_COMMAND_DATA;


typedef struct
{
    enum_SFL_AXIS_CONTROL_CONDITION_STATE  state;                        
    uint8_t                                continueMovementOnCompliance; 
    enum_SFL_AXIS_CONTROL_DRIVE_MODE       driveModeOnCompliance;        
    uint16_t                               dutyCycleOnCompliance;        
    uint16_t                               currentLimitValue;            
    int32_t                                timeLimitValue;               
    uint32_t                               internalStartTimer;           

} struct_SFL_AXIS_CONTROL_CONDITION_CURRENT_MONITORING_DATA;

typedef struct
{
    enum_SFL_AXIS_CONTROL_CONDITION_STATE  state;                        
    uint8_t                                continueMovementOnCompliance; 
    enum_SFL_AXIS_CONTROL_DRIVE_MODE       driveModeOnCompliance;        
    uint16_t                               dutyCycleOnCompliance;        
    int16_t                                countsUpperLimitValue;        
    int16_t                                internalStartPosition;        

} struct_SFL_AXIS_CONTROL_CONDITION_COUNTS_DRIVEN_DATA;

typedef struct
{
    enum_SFL_AXIS_CONTROL_CONDITION_STATE  state;                        
    uint8_t                                continueMovementOnCompliance; 
    enum_SFL_AXIS_CONTROL_DRIVE_MODE       driveModeOnCompliance;        
    uint16_t                               dutyCycleOnCompliance;        
    int32_t                                timeUpperLimitValue;          
    uint32_t                               internalStartTimer;           

} struct_SFL_AXIS_CONTROL_CONDITION_TIME_ELAPSED_DATA;

typedef struct
{
    enum_SFL_AXIS_CONTROL_CONDITION_STATE  state;                        
    uint8_t                                continueMovementOnCompliance; 
    enum_SFL_AXIS_CONTROL_DRIVE_MODE       driveModeOnCompliance;        
    uint16_t                               dutyCycleOnCompliance;        
    int32_t                                timeLimitValue;               
    uint32_t                               internalStartTimer;           
    int16_t                                internalStartPosition;        

} struct_SFL_AXIS_CONTROL_CONDITION_STALLED_DATA;

typedef struct
{
    enum_SFL_AXIS_CONTROL_CONDITION_STATE  state;                        
    uint32_t                               timeElapsed;                  
    int16_t                                countsDriven;                 
    uint32_t                               internalStartTimer;           
    int16_t                                internalStartPosition;        

} struct_SFL_AXIS_CONTROL_CONDITION_DRIVE_STATISTICS;

typedef struct
{
    struct_SFL_AXIS_CONTROL_CONDITION_CURRENT_MONITORING_DATA currentExceededData;
    struct_SFL_AXIS_CONTROL_CONDITION_CURRENT_MONITORING_DATA currentTooLowData;
    struct_SFL_AXIS_CONTROL_CONDITION_COUNTS_DRIVEN_DATA      countsDrivenData;
    struct_SFL_AXIS_CONTROL_CONDITION_TIME_ELAPSED_DATA       timeElapsedData;
    struct_SFL_AXIS_CONTROL_CONDITION_STALLED_DATA            stalledData;
    struct_SFL_AXIS_CONTROL_CONDITION_DRIVE_STATISTICS        driveStatistics;

} struct_SFL_AXIS_CONTROL_CONDITION_LIST;

typedef struct
{
    struct_SFL_AXIS_CONTROL_CONDITION_CURRENT_MONITORING_DATA currentExceededData;
} struct_SFL_AXIS_CONTROL_CONDITION_LIST_PERMANENT;

typedef struct
{
    enum_SFL_AXIS_CONTROL_CONDITION_LIST_STATE  state;                         
    uint8_t                                     continueMovementIfComplied;    
    enum_SFL_AXIS_CONTROL_DRIVE_MODE            driveModeToSetIfComplied;      
    uint16_t                                    dutyCycleToSetIfComplied;      

} struct_SFL_AXIS_CONTROL_CONDITION_LIST_RETURN_VALUE;


enum_SFL_AXIS_CONTROL_RETURN_VALUE sfl_ac_init(struct_SFL_AXIS_CONTROL_COMMAND_DATA* ptrAxisControlCmdData);

enum_SFL_AXIS_CONTROL_RETURN_VALUE sfl_ac_init_condition_list(struct_SFL_AXIS_CONTROL_CONDITION_LIST* ptrAxisControlConditionList);

enum_SFL_AXIS_CONTROL_RETURN_VALUE sfl_ac_set_cmd_data_start_conditions(struct_SFL_AXIS_CONTROL_COMMAND_DATA* ptrAxisControlCmdData,uint16_t startDutyCycle,enum_SFL_AXIS_CONTROL_DRIVE_MODE startDriveMode);

enum_SFL_AXIS_CONTROL_RETURN_VALUE sfl_ac_init_condition_list_permanent(struct_SFL_AXIS_CONTROL_CONDITION_LIST_PERMANENT* ptrAxisControlConditionListPermanent);

uint16_t sfl_ac_get_duty_cycle_from_voltage_equivalent(uint16_t equivVoltage,uint16_t supplyVoltage);


struct_SFL_AXIS_CONTROL_CONDITION_LIST_RETURN_VALUE sfl_ac_check_condition_list_permanent(struct_SFL_AXIS_CONTROL_CONDITION_LIST_PERMANENT* ptrAxisControlConditionListPermanent,struct_SFL_AXIS_CONTROL_MEASURED_DATA const* ptrAcMeasData);


enum_SFL_AXIS_CONTROL_STATE sfl_ac_drive_duty_cycle(uint16_t                                     setValueDutyCycle        
                                                   ,enum_SFL_AXIS_CONTROL_DRIVE_MODE             setValueDriveMode        
                                                   ,struct_SFL_AXIS_CONTROL_MEASURED_DATA const* ptrAcMeasData
                                                   ,struct_SFL_AXIS_CONTROL_COMMAND_DATA*        ptrAcCmdData
                                                   ,struct_SFL_AXIS_CONTROL_CONDITION_LIST*      ptrAcConditionList);


enum_SFL_AXIS_CONTROL_STATE sfl_ac_drive_duty_cycle_to_position(int16_t                                      setValuePosition        
                                                               ,uint16_t                                     setValueDutyCycle       
                                                               ,enum_SFL_AXIS_CONTROL_DRIVE_MODE             setValueDriveMode        
                                                               ,struct_SFL_AXIS_CONTROL_MEASURED_DATA const* ptrAcMeasData
                                                               ,struct_SFL_AXIS_CONTROL_COMMAND_DATA*        ptrAcCmdData
                                                               ,struct_SFL_AXIS_CONTROL_CONDITION_LIST*      ptrAcConditionList);

enum_SFL_AXIS_CONTROL_STATE sfl_ac_drive_velocity(int16_t                                      setValueVelocity         
                                                 ,enum_SFL_AXIS_CONTROL_DRIVE_MODE             setValueDriveMode        
                                                 ,uint16_t                                     setValueGainFactor
                                                 ,struct_SFL_AXIS_CONTROL_MEASURED_DATA const* ptrAcMeasData
                                                 ,struct_SFL_AXIS_CONTROL_COMMAND_DATA*        ptrAcCmdData
                                                 ,struct_SFL_AXIS_CONTROL_CONDITION_LIST*      ptrAcConditionList);


#endif 



