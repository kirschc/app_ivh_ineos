#ifndef SFL_DIAGNOSTIC_TROUBLE_CODE_H
#define SFL_DIAGNOSTIC_TROUBLE_CODE_H

#include "sfl_diagnostic_trouble_code_adj.h"
#include "sfl_diagnostic_trouble_code_cfg.h"
#include "hal_data_types.h"

#define DTC_AGE_LIMIT                           10u
#define DTC_COUNTER_LIMIT                       0xFFu


typedef enum
{
    DTC_OK                                   = 0u,
    DTC_ERROR_WRITE_NVM                          ,
    DTC_ERROR_READ_NVM                           ,
    DTC_ERROR_DTC_DATA_TYPE_INVALID              ,
    DTC_ERROR_DTC_DATA_TYPE_NOT_IMPLEMENTED      ,
    DTC_ERROR_DTC_DATA_DIR_INVALID               ,
    DTC_ERROR_DTC_DATA_HANDLER_SETTING_INVALID   ,
    DTC_ERROR_DTC_INDEX_INVALID                  ,
    DTC_ERROR_DTC_STATUS_INVALID                 ,
    DTC_ERROR_GENERAL                            ,
    DTC_ERROR_FORCE_TYPE = 0x7F  
}enum_DTC_RETURN_VALUE;

typedef enum
{
    DTC_STATUS_PASSIVE                     = 0x60u,
    DTC_STATUS_ACTIVE                      = 0x20u,
    DTC_STATUS_INVALID                        = 0u,
    DTC_STATUS_FORCE_TYPE = 0x7F  
} enum_DTC_STATUS;


typedef enum
{
    DTC_DATA_DIR_NOT_SET                     = 0u,
    DTC_DATA_DIR_READ_FROM_NVM                   ,
    DTC_DATA_DIR_WRITE_TO_NVM                    ,
    DTC_DATA_DIR_FORCE_TYPE = 0x7F  
}enum_DTC_DATA_DIRECTION;

typedef enum
{
    DTC_DATA_TYPE_NOT_SET                   = 0u,
    DTC_DATA_TYPE_SINGLE_DTC                    ,
    DTC_DATA_TYPE_ALL_DTC                       ,
    DTC_DATA_TYPE_FORCE_TYPE = 0x7F  
}enum_DTC_DATA_TYPE;

typedef enum
{
    DTC_ACK_SRC_NOT_SET                  = 0u,  
    DTC_ACK_SRC_POWER_CYCLE                  ,  
    DTC_ACK_SRC_BACK_IN_RANGE                ,  
    DTC_ACK_SRC_SIGNAL_EDGE                  ,  
    DTC_ACK_SRC_FORCE_TYPE = 0x7F  
}enum_DTC_ACK_SOURCE;

typedef struct
{
  enum_DTC_TABLE_IDX index;
  uint16_t dtc_no;
  uint8_t priority;
  enum_DTC_ACK_SOURCE ack_src;


}struct_DTC_CONFIG_TABLE_ENTRY;

#pragma pack(SFL_DTC_PRAGMA_PACK_NVM_DATA) 
typedef struct
{
    enum_DTC_TABLE_IDX index;
    uint16_t dtc_no;
    uint8_t priority;
    enum_DTC_STATUS status;
    uint8_t err_freq_cnt;
    uint8_t mon_cycle_cnt;
    uint8_t active_cycle_cnt;

}struct_DTC_TABLE_ENTRY;
#pragma pack() 

typedef struct
{
    enum_DTC_DATA_TYPE          DataType;
    enum_DTC_DATA_DIRECTION     DataDirection;

}struct_DTC_HANDLER;


enum_DTC_RETURN_VALUE sfl_dtc_init(void);


#ifdef SFL_DTC_SUPPORT_IMMEDIATE_NVM_ACCESS
enum_DTC_RETURN_VALUE sfl_dtc_data_handler(enum_DTC_TABLE_IDX      table_index,
                                           struct_DTC_HANDLER*     ptrDtcHandler,
                                           struct_DTC_TABLE_ENTRY* ptrDtcTable);
#endif


enum_DTC_RETURN_VALUE sfl_dtc_set_dtc(enum_DTC_TABLE_IDX table_index);

enum_DTC_RETURN_VALUE sfl_dtc_reset_dtc(enum_DTC_TABLE_IDX table_index);

enum_DTC_RETURN_VALUE sfl_dtc_clear_table(void);

uint8_t sfl_dtc_check_if_all_passive(void);

void sfl_dtc_set_all_passive(void);

enum_DTC_TABLE_IDX sfl_dtc_get_active_dtc_idx_highest_priority(void);

uint16_t sfl_dtc_get_active_dtc_no_highest_priority(void);

enum_DTC_RETURN_VALUE sfl_dtc_get_dtc_status_from_nvm(enum_DTC_TABLE_IDX table_index, enum_DTC_STATUS* status);


enum_DTC_STATUS sfl_dtc_get_dtc_status_from_table( enum_DTC_TABLE_IDX table_index );


enum_DTC_RETURN_VALUE sfl_dtc_get_dtc_no_from_nvm(enum_DTC_TABLE_IDX table_index, uint16_t* dtc_no);

enum_DTC_RETURN_VALUE sfl_dtc_get_dtc_priority_from_nvm(enum_DTC_TABLE_IDX table_index, uint8_t* priority);

#endif


