#ifndef SFL_CAN_DB_TABLES_DATA_H
#define SFL_CAN_DB_TABLES_DATA_H

#include "hal_data_types.h"
#include "hal_can.h"
#include "can_db_tables.h"
#include "sfl_timer.h"
#include "can_db_tables_cfg.h"

#define dyn_CAN_DP_MAX                  can_db.DP_MAX
#define dyn_CAN_BUS_MAX                 can_db.BUS_MAX
#define dyn_CAN_BLOCK_MAX               can_db.BLOCK_MAX
#define dyn_CAN_BAUDRATE                can_db.BAUDRATE
#define dyn_CAN_BUS_1                   can_db.BUS_1
#define dyn_OS_PORT_INPUT_PIN_MAX       os_io->PORT_INPUT_PIN_MAX
#define dyn_OS_PORT_PIN_MAX             os_io->PORT_PIN_MAX
#define dyn_OS_ANALOG_PIN_MAX           os_io->PORT_ANALOG_PIN_MAX
#define BIOS_CAN_BAUDRATE_NAME_LEN      10
#define NONE                            23
#define BIOS_CAN_EXT_BAUDRATE           0
#define BIOS_CAN_1MBIT                  1
#define BIOS_CAN_800KBIT                2
#define BIOS_CAN_500KBIT                3
#define BIOS_CAN_250KBIT                4
#define BIOS_CAN_125KBIT                5
#define BIOS_CAN_100KBIT                9
#define BIOS_CAN_83_33KBIT              10
#define BIOS_CAN_50KBIT                 6
#define BIOS_CAN_33_33KBIT              11
#define BIOS_CAN_20KBIT                 7
#define BIOS_CAN_10KBIT                 8
#define DF_INTEL                        0
#define DF_MOTOROLA                     1

#define MACRO_DEFINE_TO_STR_QUOTE(arg) #arg
#define MACRO_DEFINE_TO_STR(arg) MACRO_DEFINE_TO_STR_QUOTE(arg)


typedef struct
{
    uint32_t id;
    uint16_t id_ext    :1;
    uint16_t remote_tx :1;
    uint8_t data[8];
    uint8_t len;
    uint8_t prty;
    uint32_t timestamp;

} bios_can_msg_typ;

typedef struct
{
    uint8_t name[BIOS_CAN_BAUDRATE_NAME_LEN+1];
    uint8_t sj;
    uint8_t pre;
    uint8_t t_seg1;
    uint8_t t_seg2;
    uint8_t clk_src;

} bios_can_baudrate_typ;

typedef struct
{  
  uint8_t bus_id_in;
  uint8_t bus_id_out;

} can_gateway_db_const_typ;

typedef struct
{
    uint32_t  filter[2];
    uint32_t  mask[2];
    uint8_t   mask_division;

} can_db_filter_typ;

typedef struct
{
    uint16_t   DP_MAX;
    uint8_t    BUS_MAX;
    uint16_t   BLOCK_MAX;
    uint8_t    GATEWAY_DB_MAX;  
    uint16_t   BAUDRATE;
    uint8_t    BUS_1;
    uint8_t    user_filter_active;
    uint8_t    sa_active;         
    uint8_t    sa_db;             
    uint8_t    sa_val;            
    uint16_t   sa_db_rx;          
    uint16_t   sa_val_rx;         
    can_db_filter_typ f_set[5];

} can_db_typ;

typedef struct {
    uint8_t  hw_module_id;            
    uint8_t  hw_module_active;        
    uint8_t  bios_baudrate_id;        
    uint8_t  gw_input;                
    bios_can_baudrate_typ baudrate; 

} can_bus_db_const_typ;

typedef struct {
    uint8_t  bus_id;                  
    uint32_t can_id;                  
    uint8_t  can_id_ext;              
    uint32_t zykluszeit_ms_max;       
    uint32_t zykluszeit_ms_min;       
    uint8_t  msg_len_dlc;             
    uint8_t  tx;                    
    uint8_t  can_bus_gw;              
    uint32_t can_id_mask;             
    uint8_t  mux_start;               
    uint8_t  mux_length;              
    uint8_t  mux_val;                 

} can_block_db_const_typ;

typedef struct {
    can_block_id nr_can_block;        
    uint8_t  pos_bit_0;                            
    uint8_t  bit_laenge;                           
    uint8_t  datentyp;                             
    uint8_t  data_format;

} can_datenpunkt_db_const_typ;

typedef struct {
    uint8_t  last_data[8];          
    uint32_t time_stamp_read;
    uint32_t time_stamp_write;
    uint32_t time_stamp_transmit;
    uint8_t  received         :1;
    uint8_t  transmit         :1; 
    uint8_t  transmit_stop    :1; 
    boolean  stop_gw_known_ids:1; 
    bios_can_msg_typ msg;

} can_block_db_ram_typ;

typedef struct
{
    uint8_t               can_baud_usr_string[ (BIOS_CAN_BAUDRATE_NAME_LEN) ]; 
    uint8_t               can_baud_usr_define;                                 
    enum_HAL_CAN_BAUDRATE can_baud_hal;                                        
} struct_can_baud_map_table;

typedef struct
{
    const volatile can_datenpunkt_db_const_typ *can_dp_db;
    uint16_t                                   can_dp_db_enum_max;
} struct_can_datenpunkt_db_cfg;

typedef struct
{
    const volatile can_block_db_const_typ *can_block_db;
    uint16_t                              can_block_db_enum_max;
} struct_can_block_db_cfg;

typedef struct
{
    const volatile can_bus_db_const_typ *can_bus_db;
    uint16_t                             can_bus_db_enum_max;
} struct_can_bus_db_cfg;

typedef struct
{
    const volatile can_gateway_db_const_typ *can_gateway_db;
    uint16_t                                 can_gateway_db_enum_max;
} struct_can_gateway_db_cfg;

typedef struct
{
    struct_can_datenpunkt_db_cfg can_dp_db_cfg;
    struct_can_block_db_cfg      can_block_db_cfg;
    struct_can_bus_db_cfg        can_bus_db_cfg;
    struct_can_gateway_db_cfg    can_gateway_db_cfg;
    can_block_db_ram_typ*        can_block_db_ram;
    can_bus_id                   can_bus_1;
} struct_can_db_tables_cfg;

extern can_block_db_ram_typ                       *ext_ptr_can_block_db_ram; 
extern volatile const can_datenpunkt_db_const_typ *ext_ptr_can_datenpunkt_db_const;
extern volatile const can_block_db_const_typ      *ext_ptr_can_block_db_const;
extern volatile const can_bus_db_const_typ        *ext_ptr_can_bus_db_const;
extern volatile const can_gateway_db_const_typ    *ext_ptr_can_gateway_db_const;

extern const struct_can_db_tables_cfg ext_can_db_tables_cfg[];


void sfl_can_db_tables_data_init(struct_hal_can_handle *const ptr_can_handle, const enum_CAN_DB_TABLES can_db_table);

void sfl_can_db_tables_get_can_usr_baud(struct_hal_can_handle *const ptr_can_handle);

static void sfl_can_db_set_table_ptr(enum_CAN_DB_TABLES can_db_table);

#endif


