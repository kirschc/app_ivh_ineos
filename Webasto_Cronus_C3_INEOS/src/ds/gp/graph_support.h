#ifndef _GRAPH_SUPPORT_H_
#define _GRAPH_SUPPORT_H_

#include "IUG_user_api_include.h"

#define DATEN_IN 0
#define DATEN_OUT 1

#ifdef GRAPH_USE_DOUBLE
  #define ROUND(x)       \
   if (x < 0)            \
     (int32)(x - 0.5)    \
   else                  \
     (int32)(x + 0.5)
#else                   
  #define ROUND(x) x
#endif 

#define volt *1000L  
#define Volt *1000L

#define sec  *1000L
#define Sec  *1000L

typedef unsigned int index_type;

typedef union
  {
  uint32_t word;
  struct
    {
    uint8_t byte3;
    uint8_t byte2;
    uint8_t byte1;
    uint8_t byte0;
    }b;
  }long_byte_typ;


typedef union
  {
  uint8_t byte;
  struct
    {
    uint8_t bit0 :1;
    uint8_t bit1 :1;
    uint8_t bit2 :1;
    uint8_t bit3 :1;
    uint8_t bit4 :1;
    uint8_t bit5 :1;
    uint8_t bit6 :1;
    uint8_t bit7 :1;
    }b;
  }int_bit_typ;

extern const uint8_t bios_bitmask[];
extern uint8_t  timer_diff;
extern uint32_t user_variable[];


uint32_t get(index_type index);

void set(index_type index, uint32_t value);

uint32_t get_max(index_type index);

uint32_t get_min(index_type index);


#endif 


