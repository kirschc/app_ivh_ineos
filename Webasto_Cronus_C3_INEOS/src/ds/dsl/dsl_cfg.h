#ifndef __DSL_CFG_H
#define __DSL_CFG_H

/*--------------------------------------------------------------------------*/
/** \file     dsl_cfg.h
*   \brief    Provides define functionality for the AS
*             Project name: Webasto_Cronus.sch
*
*   \date 27.11.2022 08:02:13 \author kirschc dr/
*
*   \platform RH850
* --------------------------------------------------------------------------*/

#define RH850		                5
#define BSW_CPU_S32K144HFT0VLLT     6
#define BSW_CPU_S32K148             7
#define BSW_CPU_S32K116             8

#define HW_CC16WP                   0
#define HW_MGW                      1

#define MCU_TYP                     RH850
#define HW_TYP                      Webasto_Cronus

#define APPL_SW_VERSION             "v00.00.01           " ///< application specific sw version name -> 20 characters
#define APPL_MODULE_NAME            "INEOS                         " ///< application specific module name -> 30 characters

#endif