
/*--------------------------------------------------------------------------*/
/** \file     can_db_tables.c
*   \brief    CAN bus database
*   \date     20220222
*   \author   
*   \Applics Studio
*
*   \platform 2P
* --------------------------------------------------------------------------*/

#pragma ghs nowarning 1981 // suppress warning for "empty initializer is non-standard"

#include "can_db_tables.h" 
#include "sfl_can_db_tables_data.h"



/** \ingroup can_tables
*   \brief           Table of all CAN datapoints
* --------------------------------------------------------------------------*
*                    Every line defines a datapoint within a CAN block.
*                    Every datapoint is assigned to a CAN block ID
*                    #can_block_id, the order of the IDs has to be the same as
*                    the CAN block table #can_block_db_const.
*                    The table contains bit position, bit length and datatype.
*                    The datapoints are used with #can_db_set_value() or #can_db_get_value().
*                    As parameter use the name from the list #can_dp_id verwendet,
*                    the order has to be the same as in this table.
* --------------------------------------------------------------------------*/
 volatile const can_datenpunkt_db_const_typ can_datenpunkt_db_const[CAN_DP_MAX+1] = { // Array can1_....  with the dp of one CAN interface
//  CAN block index             Pos bit0           Length           Data type           Data format                     LineNr: datapoint-ID                             
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//  (16 Bit)                    (0-63)             (1-32)                               (0=Intel, 1=Motorola)                                                            
//------------------------------------------------------------------------------------------------------------------------------------------------------------------------
{   Klemmen_Status_01           ,0                 ,8               ,UINT               ,0                              },   //Klemmen_Status_01_CRC                     
{   Klemmen_Status_01           ,8                 ,4               ,UINT               ,0                              },   //Klemmen_Status_01_BZ                      
{   Klemmen_Status_01           ,16                ,1               ,UBYTE              ,0                              },   //ZAS_Kl_S                                  
{   Klemmen_Status_01           ,17                ,1               ,UBYTE              ,0                              },   //ZAS_Kl_15                                 
{   Klemmen_Status_01           ,18                ,1               ,UBYTE              ,0                              },   //ZAS_Kl_X                                  
{   Klemmen_Status_01           ,19                ,1               ,UBYTE              ,0                              },   //ZAS_Kl_50_Startanforderung                
{   Klemmen_Status_01           ,24                ,1               ,UBYTE              ,0                              },   //KST_Warn_P1_ZST_def                       
{   Klemmen_Status_01           ,25                ,1               ,UBYTE              ,0                              },   //KST_Warn_P2_ZST_def                       
{   Klemmen_Status_01           ,26                ,3               ,UINT               ,0                              },   //KST_Fahrer_Hinweise                       
{   STH_01                      ,22                ,1               ,UBYTE              ,0                              },   //STH_Pumpen_Anf_extern                     
{   STH_01                      ,0                 ,1               ,UBYTE              ,0                              },   //STH_Funk_ein                              
{   STH_01                      ,1                 ,1               ,UBYTE              ,0                              },   //STH_Funk_aus                              
{   STH_01                      ,2                 ,1               ,UBYTE              ,0                              },   //STH_Zusatzheizung                         
{   STH_01                      ,3                 ,1               ,UBYTE              ,0                              },   //STH_LED                                   
{   STH_01                      ,4                 ,1               ,UBYTE              ,0                              },   //STH_Pumpe_ein                             
{   STH_01                      ,5                 ,1               ,UBYTE              ,0                              },   //STH_Geblaese                              
{   STH_01                      ,6                 ,1               ,UBYTE              ,0                              },   //STH_EKP_Anst                              
{   STH_01                      ,7                 ,1               ,UBYTE              ,0                              },   //STH_Start_folgt                           
{   STH_01                      ,8                 ,6               ,UINT               ,0                              },   //STH_Ventiloeffnungszeit                   
{   STH_01                      ,14                ,1               ,UBYTE              ,0                              },   //STH_Ventil_Status                         
{   STH_01                      ,15                ,1               ,UBYTE              ,0                              },   //STH_Fehler_ElWaPu                         
{   STH_01                      ,16                ,6               ,UINT               ,0                              },   //STH_Waermeeintrag                         
{   STH_01                      ,24                ,13              ,UINT               ,0                              },   //STH_KVS                                   
{   STH_01                      ,37                ,3               ,UINT               ,0                              },   //STH_Fehlerstatus                          
{   STH_01                      ,40                ,8               ,UINT               ,0                              },   //STH_Heizleistung                          
{   STH_01                      ,48                ,8               ,UINT               ,0                              },   //STH_Wassertemp                            
{   STH_01                      ,56                ,3               ,UINT               ,0                              },   //STH_Texte                                 
{   STH_01                      ,59                ,1               ,UBYTE              ,0                              },   //STH_Motorvorwaermung                      
{   STH_01                      ,60                ,1               ,UBYTE              ,0                              },   //STH_Servicemode                           
{   STH_01                      ,61                ,1               ,UBYTE              ,0                              },   //STH_war_aktiv                             
{   STH_01                      ,62                ,1               ,UBYTE              ,0                              },   //STH_KVS_Ueberlauf                         
{   Klima_12                    ,0                 ,3               ,UINT               ,0                              },   //KL_AQS_Empfindlichkeit                    
{   Klima_12                    ,3                 ,2               ,UINT               ,0                              },   //HSH_Taster                                
{   Klima_12                    ,5                 ,1               ,UBYTE              ,0                              },   //FSH_Taster                                
{   Klima_12                    ,6                 ,1               ,UBYTE              ,0                              },   //KL_Zuheizer_Freigabe                      
{   Klima_12                    ,7                 ,1               ,UBYTE              ,0                              },   //KL_Beschlagsgefahr                        
{   Klima_12                    ,8                 ,3               ,UINT               ,0                              },   //KL_SIH_Soll_li                            
{   Klima_12                    ,11                ,3               ,UINT               ,0                              },   //KL_SIH_Soll_re                            
{   Klima_12                    ,14                ,2               ,UINT               ,0                              },   //KRH_Soll_li                               
{   Klima_12                    ,16                ,3               ,UINT               ,0                              },   //KL_SIL_Soll_li                            
{   Klima_12                    ,19                ,3               ,UINT               ,0                              },   //KL_SIL_Soll_re                            
{   Klima_12                    ,22                ,2               ,UINT               ,0                              },   //KRH_Soll_re                               
{   Klima_12                    ,24                ,8               ,UINT               ,0                              },   //KL_Geblspng_Soll                          
{   Klima_12                    ,32                ,8               ,UINT               ,0                              },   //KL_Geblspng_Fond_Soll                     
{   Klima_12                    ,40                ,8               ,UINT               ,0                              },   //KL_I_Geblaese                             
{   Klima_12                    ,48                ,10              ,UINT               ,0                              },   //KL_Kompressorstrom_soll                   
{   Klima_12                    ,58                ,4               ,UINT               ,0                              },   //KL_Umluftklappe_Status                    
{   Klima_12                    ,62                ,2               ,UINT               ,0                              },   //KL_PTC_Verbauinfo                         
{   Klima_03                    ,0                 ,1               ,UBYTE              ,0                              },   //KL_STL_aktiv                              
{   Klima_03                    ,1                 ,1               ,UBYTE              ,0                              },   //KL_STH_aktiv                              
{   Klima_03                    ,2                 ,1               ,UBYTE              ,0                              },   //KL_Solarluefter_aktiv                     
{   Klima_03                    ,3                 ,1               ,UBYTE              ,0                              },   //KL_Umluft_Taste                           
{   Klima_03                    ,8                 ,2               ,UINT               ,0                              },   //KL_STH_Ansteuerung                        
{   Klima_03                    ,10                ,6               ,UINT               ,0                              },   //KL_STH_Betriebsdauer                      
{   Klima_03                    ,16                ,1               ,UBYTE              ,0                              },   //KL_Magnetventil                           
{   Klima_03                    ,17                ,1               ,UBYTE              ,0                              },   //KL_WaPu                                   
{   Klima_03                    ,24                ,1               ,UBYTE              ,0                              },   //KL_BCmE_Livetip_Freigabe                  
{   Klima_03                    ,25                ,1               ,UBYTE              ,0                              },   //KL_HYB_ASV_hinten_schliessen_Anf          
{   Klima_03                    ,26                ,1               ,UBYTE              ,0                              },   //KL_HYB_ASV_vorne_schliessen_Anf           
{   Klima_03                    ,27                ,1               ,UBYTE              ,0                              },   //KL_HYB_Spuelbetrieb_Freigabe              
{   Klima_03                    ,32                ,8               ,UINT               ,0                              },   //KL_Innen_Temp                             
{   Klima_03                    ,40                ,8               ,UINT               ,0                              },   //KL_Sonnenintensitaet                      
{   Klima_06                    ,0                 ,6               ,UINT               ,0                              },   //KL_ZZ_Minute                              
{   Klima_06                    ,6                 ,2               ,UINT               ,0                              },   //KL_ZZ_Status_Timer                        
{   Klima_06                    ,8                 ,4               ,UINT               ,0                              },   //KL_ZZ_Monat                               
{   Klima_06                    ,16                ,5               ,UINT               ,0                              },   //KL_ZZ_Stunde                              
{   Klima_06                    ,21                ,3               ,UINT               ,0                              },   //KL_ZZ_Betriebsmodus                       
{   Klima_06                    ,24                ,5               ,UINT               ,0                              },   //KL_ZZ_Tag                                 
{   Klima_06                    ,32                ,1               ,UBYTE              ,0                              },   //KL_Stopp_Wiederstart_Anz_01               
{   Klima_06                    ,33                ,1               ,UBYTE              ,0                              },   //KL_Stopp_Wiederstart_Anz_02               
{   Klima_06                    ,34                ,1               ,UBYTE              ,0                              },   //KL_Stopp_Wiederstart_Anz_03               
{   Klima_06                    ,35                ,1               ,UBYTE              ,0                              },   //KL_Stopp_Wiederstart_Anz_04               
{   Klima_06                    ,36                ,1               ,UBYTE              ,0                              },   //KL_Stopp_Wiederstart_Anz_05               
{   Klima_06                    ,37                ,1               ,UBYTE              ,0                              },   //KL_Stopp_Wiederstart_Anz_06               
{   Klima_06                    ,38                ,1               ,UBYTE              ,0                              },   //KL_Stopp_Wiederstart_Anz_07               
{   Klima_06                    ,39                ,1               ,UBYTE              ,0                              },   //KL_Stopp_Wiederstart_Anz_08               
{   Klima_06                    ,40                ,1               ,UBYTE              ,0                              },   //KL_Stopp_Wiederstart_Anz_09               
{   STH_02                      ,12                ,4               ,UINT               ,0                              },   //STH_ZZ_Monat                              
{   STH_02                      ,16                ,5               ,UINT               ,0                              },   //STH_ZZ_Tag                                
{   STH_02                      ,21                ,3               ,UINT               ,0                              },   //STH_ZZ_Betriebsmodus                      
{   STH_02                      ,24                ,6               ,UINT               ,0                              },   //STH_ZZ_Minute                             
{   STH_02                      ,30                ,2               ,UINT               ,0                              },   //STH_ZZ_Status_Timer                       
{   STH_02                      ,32                ,5               ,UINT               ,0                              },   //STH_ZZ_Stunde                             
{   STH_02                      ,37                ,6               ,UINT               ,0                              },   //STH_ZZ_Betriebsdauer                      
{   NMH_STH                     ,39                ,1               ,UBYTE              ,0                              },   //NM_STH_NM_aktiv_Geblaese_Anf              
{   NMH_STH                     ,38                ,1               ,UBYTE              ,0                              },   //NM_STH_NM_aktiv_Datentransf_Funk          
{   NMH_STH                     ,37                ,1               ,UBYTE              ,0                              },   //NM_STH_NM_aktiv_EIN_AUS_Funk              
{   NMH_STH                     ,36                ,1               ,UBYTE              ,0                              },   //NM_STH_NM_aktiv_WaPu_Anf                  
{   NMH_STH                     ,35                ,1               ,UBYTE              ,0                              },   //NM_STH_NM_aktiv_EKP_Anf                   
{   NMH_STH                     ,0                 ,8               ,UINT               ,0                              },   //NM_STH_SNI                                
{   NMH_STH                     ,16                ,6               ,UINT               ,0                              },   //NM_STH_NM_State                           
{   NMH_STH                     ,22                ,1               ,UBYTE              ,0                              },   //NM_STH_Car_Wakeup                         
{   NMH_STH                     ,24                ,8               ,UINT               ,0                              },   //NM_STH_Wakeup                             
{   NMH_STH                     ,32                ,1               ,UBYTE              ,0                              },   //NM_STH_NM_aktiv_KL15                      
{   NMH_STH                     ,33                ,1               ,UBYTE              ,0                              },   //NM_STH_NM_aktiv_Diagnose                  
{   NMH_STH                     ,34                ,1               ,UBYTE              ,0                              },   //NM_STH_NM_aktiv_Tmin                      
{   NMH_STH                     ,48                ,1               ,UBYTE              ,0                              },   //NM_STH_NL_STH_aktiv                       
{   NMH_STH                     ,63                ,1               ,UBYTE              ,0                              },   //NM_STH_UDS_CC                             
};



/** \ingroup can_tables
*   \brief           Table of all CAN blocks
* --------------------------------------------------------------------------*
*                    Every line defines a CAN block and sets the CAN-ID,
*                    the minimal and maximal time between to messages for sending,
*                    the length of a message and the direction (0=Rx, 1=Tx).
*                    The CAN-IDs must be defined in can_db.h.
*                    Every CAN block is assigned to a ID, the order has to be
*                    the same as in this table.
* --------------------------------------------------------------------------*/
 volatile const can_block_db_const_typ can_block_db_const[CAN_BLOCK_MAX+1] = { // Array can1_....  with the CAN blocks of one CAN interface
//  CAN bus index            CAN-ID                EXT - ID           Max                Min                Msg-Len DLC           TX Flag           CAN bus gw                 CAN-ID-Mask           Mux start           Mux length           Mux val                                           
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//  8 Bit                    (32 Bit)              (0,1)              (32 Bit)           (32 Bit)           (0-8)                 (0,1)             (NONE,CAN_BUS_x)           (29bit)               0-62                0-8                  0-255                                             
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
{   CAN_BUS_1                ,0x3C0                ,0                 ,100               ,50                ,4                    ,0                ,NONE                      ,0x0                  ,0                  ,0                   ,0               },  //Klemmen_Status_01          
{   CAN_BUS_1                ,0x521                ,0                 ,200               ,50                ,8                    ,0                ,NONE                      ,0x0                  ,0                  ,0                   ,0               },  //STH_01                     
{   CAN_BUS_1                ,0x668                ,0                 ,200               ,50                ,8                    ,0                ,NONE                      ,0x0                  ,0                  ,0                   ,0               },  //Klima_12                   
{   CAN_BUS_1                ,0x66E                ,0                 ,2000              ,50                ,8                    ,0                ,NONE                      ,0x0                  ,0                  ,0                   ,0               },  //Klima_03                   
{   CAN_BUS_1                ,0x671                ,0                 ,2000              ,50                ,8                    ,0                ,NONE                      ,0x0                  ,0                  ,0                   ,0               },  //Klima_06                   
{   CAN_BUS_1                ,0x672                ,0                 ,500               ,50                ,8                    ,0                ,NONE                      ,0x0                  ,0                  ,0                   ,0               },  //STH_02                     
{   CAN_BUS_1                ,0x6B4                ,0                 ,200               ,50                ,8                    ,0                ,NONE                      ,0x0                  ,0                  ,0                   ,0               },  //VIN_01                     
{   CAN_BUS_1                ,0x1B00006A           ,1                 ,200               ,50                ,8                    ,0                ,NONE                      ,0x0                  ,0                  ,0                   ,0               },  //NMH_STH                    
};


 volatile const can_bus_db_const_typ can_bus_db_const[CAN_BUS_MAX+1] = 
{
 //                                                                             Manual setting of segment / jumper-with register
 //           HW CAN-Modul          , HW CAN-Modul active          , CAN Baudrate                   , Gateway Input           , {           Name Str.           , SJ               , Prescaler          , T_SEG1           , T_SEG2           , CLK_SRC            } }          , 
 //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 //           (8 Bit)               , (8 Bit)                      , (8 Bit)                        , (8 Bit)                 , {           (10 Byte)           , (8 Bit)          , (8 Bit)            , (8 Bit)          , (8 Bit)          , (8 Bit)            } }          , 
 //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 {            CAN_BUS_0             , FALSE                        , BIOS_CAN_EXT_BAUDRATE          , IS_NO_GW_INPUT          , {           "125 kBit"          , 2                , 4                  , 13               , 2                , 0                  } }          , 
 {            CAN_BUS_1             , TRUE                         , BIOS_CAN_EXT_BAUDRATE          , IS_NO_GW_INPUT          , {           "500 kBit"          , 2                , 1                  , 5                , 2                , 0                  } }          , 
 {            CAN_BUS_2             , TRUE                         , BIOS_CAN_EXT_BAUDRATE          , IS_NO_GW_INPUT          , {           "500 kBit"          , 2                , 1                  , 5                , 2                , 0                  } }          , 
 {            CAN_BUS_3             , TRUE                         , BIOS_CAN_500KBIT               , IS_NO_GW_INPUT          , {           "500 kBit"          , 2                , 1                  , 2                , 0                , 0                  } }          , 
 
};



 volatile const can_gateway_db_const_typ can_gateway_db_const[CAN_GATEWAY_DB_MAX+1] =
 {
 //Input Bus ID           Output Bus ID                     
 //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 //(8 Bit)                (8 Bit)                           
 //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
};

#pragma ghs endnowarning
