#ifndef _CAN_DB_TABLES_H_
#define _CAN_DB_TABLES_H_


/*--------------------------------------------------------------------------*/
/** \file     can_db_tables.h
*   \brief    CAN bus database
*
*   \date     20220222
*   \author   ParameterTool
*
*   \platform HCS08DZ / HCS12XE / HCS12XD / HCS12P
* --------------------------------------------------------------------------
*   \defgroup Name      can_tables
* --------------------------------------------------------------------------*/



// Change the Baudrate either to extended mode or a fix rate from the list below:
#define CAN_BAUDRATE BIOS_CAN_EXT_BAUDRATE
// einstellbare BAUDRATEN
// BIOS_CAN_EXT_BAUDRATE
// BIOS_CAN_1MBIT
// BIOS_CAN_800KBIT
// BIOS_CAN_500KBIT
// BIOS_CAN_250KBIT
// BIOS_CAN_125KBIT
// BIOS_CAN_100KBIT
// BIOS_CAN_83_33KBIT
// BIOS_CAN_50KBIT
// BIOS_CAN_33_33KBIT
// BIOS_CAN_20KBIT
// BIOS_CAN_10KBIT


//SETTING FOR BIOS_CAN_EXT_BAUDRATE
#define  CAN_BAUDRATE_def_BEZEICHNUNG           125          
#define  CAN_BAUDRATE_def_sj                    2            
#define  CAN_BAUDRATE_def_pre                   4            
#define  CAN_BAUDRATE_def_t_seg1                13           
#define  CAN_BAUDRATE_def_t_seg2                2            
#define  CAN_BAUDRATE_def_source                0            
#define  CAN_BAUDRATE_def_t_propSeg             0            

// Activate the user-defined filter settings:
#define USER_CAN_FILTER_ACTIVE

#ifdef USER_CAN_FILTER_ACTIVE

    #define CAN1_FILTER_A 0x672
    #define CAN1_FILTER_B 0x1B00006A
    #define CAN1_MASK_A   0x7FF
    #define CAN1_MASK_B   0x0
    #define CAN1_MASK_DIVISION BIOS_CAN_ID_NORM_A_EXT_B
    
    #define CAN2_FILTER_A 0x0
    #define CAN2_FILTER_B 0x0
    #define CAN2_MASK_A   0x0
    #define CAN2_MASK_B   0x0
    #define CAN2_MASK_DIVISION BIOS_CAN_ID_ALL_OPEN
    
    #define CAN3_FILTER_A 0x0
    #define CAN3_FILTER_B 0x0
    #define CAN3_MASK_A   0x0
    #define CAN3_MASK_B   0x0
    #define CAN3_MASK_DIVISION BIOS_CAN_ID_ALL_OPEN
    
#endif


#define CAN_RX_MSG_FULL_BUFFER_SIZE 66u ///< can rx buffer size for all used can busses
// Datentypen
#define FLOAT   0x24
//#define W_FLOAT 0x24
#define ULONG   0x04
//#define W_ULONG 0x04
#define SLONG   0x14
#define UINT    0x02
//#define W_UINT  0x02
#define SINT    0x12
//#define M_SINT  0x12   // als MEMO-Feld in der Datenbank
//#define W_SINT  0x12
#define UBYTE   0x01
//#define W_UBYTE 0x01
#define SBYTE   0x11
//#define TEXT    0x01
//#define M_TEXT  0x01



#define IS_GW_INPUT      1
#define IS_NO_GW_INPUT   0


// Defines for initialization of CAN datapoints by EEPROM
#define CAN_NO_EEPROM_INIT 255
#define CAN_INIT_EEPROM_SIZE 20

#define CAN_CFG_BLOCK_MAX 8 /// < same value as #CAN_BLOCK_MAX  

typedef enum
{
    CAN_GATEWAY_DB_MAX           = 0          ,
 
}can_gateway_id;

/// Define CAN busses: All the possible CAN busses. Only use these busses in table #can_block_db_const
typedef enum
{
    CAN_BUS_0 = 0          ,
    CAN_BUS_1              ,
    CAN_BUS_2              ,
    CAN_BUS_3              ,
    CAN_BUS_MAX
} can_bus_id;



/// Define datapoints: Order must be the same as in table#can_datenpunkt_db_const
typedef enum
{
    Klemmen_Status_01_CRC = 0                 ,
    Klemmen_Status_01_BZ                      ,
    ZAS_Kl_S                                  ,
    ZAS_Kl_15                                 ,
    ZAS_Kl_X                                  ,
    ZAS_Kl_50_Startanforderung                ,
    KST_Warn_P1_ZST_def                       ,
    KST_Warn_P2_ZST_def                       ,
    KST_Fahrer_Hinweise                       ,
    STH_Pumpen_Anf_extern                     ,
    STH_Funk_ein                              ,
    STH_Funk_aus                              ,
    STH_Zusatzheizung                         ,
    STH_LED                                   ,
    STH_Pumpe_ein                             ,
    STH_Geblaese                              ,
    STH_EKP_Anst                              ,
    STH_Start_folgt                           ,
    STH_Ventiloeffnungszeit                   ,
    STH_Ventil_Status                         ,
    STH_Fehler_ElWaPu                         ,
    STH_Waermeeintrag                         ,
    STH_KVS                                   ,
    STH_Fehlerstatus                          ,
    STH_Heizleistung                          ,
    STH_Wassertemp                            ,
    STH_Texte                                 ,
    STH_Motorvorwaermung                      ,
    STH_Servicemode                           ,
    STH_war_aktiv                             ,
    STH_KVS_Ueberlauf                         ,
    KL_AQS_Empfindlichkeit                    ,
    HSH_Taster                                ,
    FSH_Taster                                ,
    KL_Zuheizer_Freigabe                      ,
    KL_Beschlagsgefahr                        ,
    KL_SIH_Soll_li                            ,
    KL_SIH_Soll_re                            ,
    KRH_Soll_li                               ,
    KL_SIL_Soll_li                            ,
    KL_SIL_Soll_re                            ,
    KRH_Soll_re                               ,
    KL_Geblspng_Soll                          ,
    KL_Geblspng_Fond_Soll                     ,
    KL_I_Geblaese                             ,
    KL_Kompressorstrom_soll                   ,
    KL_Umluftklappe_Status                    ,
    KL_PTC_Verbauinfo                         ,
    KL_STL_aktiv                              ,
    KL_STH_aktiv                              ,
    KL_Solarluefter_aktiv                     ,
    KL_Umluft_Taste                           ,
    KL_STH_Ansteuerung                        ,
    KL_STH_Betriebsdauer                      ,
    KL_Magnetventil                           ,
    KL_WaPu                                   ,
    KL_BCmE_Livetip_Freigabe                  ,
    KL_HYB_ASV_hinten_schliessen_Anf          ,
    KL_HYB_ASV_vorne_schliessen_Anf           ,
    KL_HYB_Spuelbetrieb_Freigabe              ,
    KL_Innen_Temp                             ,
    KL_Sonnenintensitaet                      ,
    KL_ZZ_Minute                              ,
    KL_ZZ_Status_Timer                        ,
    KL_ZZ_Monat                               ,
    KL_ZZ_Stunde                              ,
    KL_ZZ_Betriebsmodus                       ,
    KL_ZZ_Tag                                 ,
    KL_Stopp_Wiederstart_Anz_01               ,
    KL_Stopp_Wiederstart_Anz_02               ,
    KL_Stopp_Wiederstart_Anz_03               ,
    KL_Stopp_Wiederstart_Anz_04               ,
    KL_Stopp_Wiederstart_Anz_05               ,
    KL_Stopp_Wiederstart_Anz_06               ,
    KL_Stopp_Wiederstart_Anz_07               ,
    KL_Stopp_Wiederstart_Anz_08               ,
    KL_Stopp_Wiederstart_Anz_09               ,
    STH_ZZ_Monat                              ,
    STH_ZZ_Tag                                ,
    STH_ZZ_Betriebsmodus                      ,
    STH_ZZ_Minute                             ,
    STH_ZZ_Status_Timer                       ,
    STH_ZZ_Stunde                             ,
    STH_ZZ_Betriebsdauer                      ,
    NM_STH_NM_aktiv_Geblaese_Anf              ,
    NM_STH_NM_aktiv_Datentransf_Funk          ,
    NM_STH_NM_aktiv_EIN_AUS_Funk              ,
    NM_STH_NM_aktiv_WaPu_Anf                  ,
    NM_STH_NM_aktiv_EKP_Anf                   ,
    NM_STH_SNI                                ,
    NM_STH_NM_State                           ,
    NM_STH_Car_Wakeup                         ,
    NM_STH_Wakeup                             ,
    NM_STH_NM_aktiv_KL15                      ,
    NM_STH_NM_aktiv_Diagnose                  ,
    NM_STH_NM_aktiv_Tmin                      ,
    NM_STH_NL_STH_aktiv                       ,
    NM_STH_UDS_CC                             ,
    CAN_DP_MAX
             
}can_dp_id;



/// Define CAN blocks: Order must be the same as in table #can_block_db_const
typedef enum
{
    Klemmen_Status_01 = 0          ,
    STH_01                         ,
    Klima_12                       ,
    Klima_03                       ,
    Klima_06                       ,
    STH_02                         ,
    VIN_01                         ,
    NMH_STH                        ,
    CAN_BLOCK_MAX
 
} can_block_id;




/// Activate direct routing of CAN messages
//#define CAN_GW_MODE_ROUTE

#endif

