#ifndef __USER_CODE_H_
#define __USER_CODE_H_
/*----------------------------------------------------------------------------*/
/**
 * \file         user_code.h
 * \brief        user code header
 *
 * \details
 * \date         
 * \author       
 *
 */
/*----------------------------------------------------------------------------*/
/**
* \defgroup     user User main with User callback functions
* \brief        Declaration of all User functions for his own C-Code.
* \details
*/
/*----------------------------------------------------------------------------*/

// ---------------------------------------------------------------------------------------------------
// includes
// ---------------------------------------------------------------------------------------------------
// Include API headers.
#include "IUG_user_api_include.h"


// ---------------------------------------------------------------------------------------------------
// defines
// ---------------------------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------------------------
// function prototypes
// ---------------------------------------------------------------------------------------------------

/*----------------------------------------------------------------------------*/
/**
* \ingroup user
* \brief   user-defined c-code INIT
* \details Callback function on module initialization to give the user a possibility to do his application specific initialization.\n
*          For example a default value configuration.
*/
void usercode_init( void );

/*----------------------------------------------------------------------------*/
/**
* \ingroup user
* \brief   user-defined c-code --> User main
* \details Will be called after graph code in the same cycle time.
*/
void usercode( void );

/*----------------------------------------------------------------------------*/
/**
* \ingroup user
* \brief   When a CAN message is received this function will be automatically called!
* \details Possible CAN hardware IDs see #enum_API_SYSTEM_CAN_BUSES.
*
* \param    hw_id       [in] const enum_API_SYSTEM_CAN_BUSES Possible CAN hardware IDs #enum_API_SYSTEM_CAN_BUSES
* \param    ptr_can_msg [in] const struct_api_utilities_can_frame *const Pointer to the received CAN message structure
*/
void user_can_message_receive( const enum_API_SYSTEM_CAN_BUSES hw_id, const struct_api_utilities_can_frame *const ptr_can_msg );

#endif 
