 /*----------------------------------------------------------------------------*/
/**
 * \file         user_code.c
 * \brief        file to write user specific code
 *
 * \details
 * \date         
 * \author       
 *
 */

// ---------------------------------------------------------------------------------------------------
// includes
// ---------------------------------------------------------------------------------------------------
#include "user_code.h"

// ---------------------------------------------------------------------------------------------------
// tables
// ---------------------------------------------------------------------------------------------------
#define INITIAL_OPERATION_OK            0x1      /**< For ID_INITIAL_OPERATION */

#define C_TI_VEHICLE_CAN_ACTIVE         200      // ms 
#define C_TI_CLIMATE_CAN_ACTIVE         1000     // ms 
#define C_TI_DEBUG_CAN_OUT              500      // ms CK CAN Wiederholungsrate Testausgabe
#define C_TI_CAN_OUT_SETTING_ADJUST     400      // ms 
#define C_TI_CAN_OUT_SETTING_RESTORE    200      // ms
#define C_TI_WAIT_FOR_TEMP              12000    // ms Wartezeit nach Start Cronus bis zur ersten gueltigen Temperatur des W-Bus-Sensors
#define C_TI_BUS_TERMINATION_DELAY      1000     // ms Wartezeit, bevor CAN-Abschlussiderstand abgeschaltet wird nach Oeffnen des Relais
#define C_TI_BLOWER_TEST_TIME           180000   // ms Wartezeit bis der Blower Test wieder ausgeschaltet wird

#define C_TI_WAKEUP                     1000     // ms Dauer der Aufweckphase
#define C_TI_GET_VALUES                 30000    // ms Timer fuer Aufnahme der Ist-Daten vom KSG
#define C_TI_POSTRUN                    1000     // ms Dauer der Nachlaufphase
#define C_TI_SEND_DELAY                 100      // ms Verzoegerung der ersten CAN-Nachrichten, um Einschaltzeit des CAN-Relais zu

#define TEMP_VALUE_OUT_OF_RANGE         0xFF
#define TEMP_VALUE_INVALID              0xFE /**< Also: Sensor not available or defect */
#define TEMP_VALUE_CAR_INIT             0xFE // aus *.dbc

#define COUNTER_RESET_ADJUST            0
#define COUNTER_RESET_RESTORE           1

#define W_BUS_TEMPERATURE_OFFSET        (int16_t)50
#define VEHICLE_TEMPERATURE_FACTOR_BASE (int16_t)100

/*  MUX IDs */
// Variante 0 (Golf 2020)
#define MUX_ID_0_ON_OFF                   0x4D
#define MUX_ID_0_TEMP_LEFT                0x50
#define MUX_ID_0_TEMP_RIGHT               0x51
#define MUX_ID_0_BLOWER_SPEED             0x52
#define MUX_ID_0_DIRECTION                0x54
#define MUX_ID_0_AUTO                     0x67

// Variante 1 (Passat 2019)
#define MUX_ID_1_ON_OFF                   0x4D
#define MUX_ID_1_TEMP_LEFT                0x5B
#define MUX_ID_1_TEMP_RIGHT               0x61
#define MUX_ID_1_BLOWER_SPEED             0x5C
#define MUX_ID_1_DIRECTION                0x5E
#define MUX_ID_1_AC_ON                    0x52  

// Variante 2 (T6.1)
#define MUX_ID_2_ON_OFF                    0x4D

#define C_OFFSET_HEATER_WATER_TEMP_AND_START_AMBIENT_TEMP   20 // �C KSK Erkennung Motorkaltstart Die Wassertemperatur vom HG darf nicht mehr als 20�C W�rmer als die Umgebungstemperatur sein.
#define C_WATER_TEMP_INCREASE_THRESHOLD                     10 // �C KSK 
#define C_ENGINE_RPM_THRESHOLD                              500 // U/min KSK
#define C_TI_KSK_SECOND                                     1000 // s KSK
#define C_TI_KSK_200MS                                      200 // 200ms KSK
#define C_TI_ENG_ON_R_CONTROL_SWITCH_OFF                    20 // s KSK
#define C_TI_HEATING_OFF_R_CONTROL_SWITCH_OFF               3600 // s KSK

/* Zuordnung Fahrzeugtyp zum zugehoerigen ASCII-Wert --> wird nicht mher genutzt, nur zur �bersicht
// Skoda
#define TYPE_NE                           0x4E45 // Octavia III
#define TYPE_NX                           0x4E58 // Octavia IV
#define TYPE_NP                           0x4E50 // Superb III FL
// VW
#define TYPE_CD                           0x4344 // Golf VIII
#define TYPE_7H                           0x3748 // T6.1 / T6(PA)   
#define TYPE_3C                           0x3343 // Passat 2019
// Seat
#define TYPE_KL                           0x4B4C // Leon IV
// Audi
#define TYPE_GY                           0x4759 // Audi A3
*/
// ---------------------------------------------------------------------------------------------------
// module globals
// ---------------------------------------------------------------------------------------------------
boolean vehicle_can_active;      // Flag bei Erkennung CAN1 Empfang
boolean climate_can_active;      // Flag bei Erkennung CAN2 Empfang
boolean status_climate_control;
boolean ign_vehicle;             // Zuendungssignal Fahrzeug
boolean ign_vehicle_buf = FALSE; // Zuendungssignal Fahrzeug Merker --> Fahrzeug Zuendung war in diesem Wachzyklus schon einmal an
boolean ibn_done;                // Status vom CRONUS ob Inbetriebnahme schon durchgefuehrt wurde
boolean init_done    = FALSE;    // Flag zur Erkennung ob alte KGS Einstellung eingelesen wurden
boolean user_present = FALSE;    // Ueberpruefung ob bereits ein Anwender anwesend war (Zuendung war in diesem Wachzyklus schon einmal eingeschaltet)
boolean driver_profiles_available = FALSE; // Fahrerprofile verfuegbar - Kriterium fuer Notwendigkeit der Rueckstellung
boolean setting_done = FALSE;    // Flag zur Erkennung ob die Klimaeinstellphase abgeschlossen ist
boolean debug = FALSE;           // wird befuellt aus Parameter APPLICATION_DEBUG
boolean climate_settings = TRUE; // wird befuellt aus Parameter CLIMATE_SETTINGS
boolean blower_test = FALSE , blower_test_buf = FALSE;     // wird befuellt aus dem Parameter BLOWER_TEST
boolean status_climate_control_car_am;  // Klimaanforderung f�r Car AM ohne HG Status



/* Timer: */
uint32_t ti_vehicle_can_active;
uint32_t ti_climate_can_active;
uint32_t ti_send_delay;
uint32_t ti_wait_for_temp;
uint32_t ti_bus_termination_delay;  // Nachlaufzeit fuer 120Ohm nach Oeffnen des CAN-Relais

/* Counter: */
uint8_t  clima_cnt_adjust  = COUNTER_RESET_ADJUST;
uint8_t  clima_cnt_restore = COUNTER_RESET_RESTORE;

// Variablen fuer Uebernahme CAN Daten vom Fahrzeug
uint8_t klsen_02_temp;
uint8_t motor_07_temp;
uint8_t kilometer[8];
uint8_t kilometer2[8];
char vehicle_type[2]; //Fahrzeugtyp bei VW in VIN Stelle 7 und 8
//uint16_t vehicle_type; //Fahrzeugtyp bei VW in VIN Stelle 7 und 8
uint8_t vin[18];
uint16_t driver_profile; // Fahrer Profil 
uint16_t driver_profile_old; // Fahrer Profil mit dem angesteuert bzw. welches verstellt worden ist
uint8_t new_driver_profile_detect; // Neues Fahrerprofil erkannt

// Temperaturen
uint8_t cronus_temperature; // zyklische Inneraum-Temperatur von Cronus ueber den Sensor mit hoechster Prioritaet (KSK)

uint8_t start_temperature;  // erste gueltige Innenraum-Temperatur von Cronus (KSK)

//  KSK
uint8_t heater_water_temp = 0x00;  // KSK Wassertemperatur des Heizger�ts
uint8_t heater_water_temp_old = 0x00;   
uint8_t heater_water_temp_start = 0xFF;  // KSK Wassertemperatur des Heizger�ts beim Start
uint8_t cold_engine_start_detect = 0xFF;  //KSK 
uint8_t r_control_active = FALSE;   // KSK Kaltstartregelung aktiv
uint16_t eng_rpm;   // KSK Motordrehzahl
boolean eng_on = FALSE; // KSK Motorlauf
uint16_t eng_on_cnt;    // Sekundenz�hler f�r Motorlauf
uint32_t heater_state_off_cnt; // Sekundenz�hler f�r den Auszustand des Heizger�ts
boolean heater_state_off_flag = FALSE; // Flag zur Erkennuung der Heizungs- Ausschaltflanke
boolean heater_state_on_flag = FALSE; // Flag zur Erkennung der Heizungs- Einschaltflanke

uint8_t blower_on_threshold; // Gebl�seeinschaltschwelle aus Parameterdatensatz

// Variablen fuer Klimaeinstellungseigenschaften
boolean climate_adjust_available;
typedef struct
{ 
    uint8_t mux_id;      
    uint8_t can_dlc;     
    uint8_t data[6u];
    uint8_t init_data[6u];
    uint8_t heating_data[6u];
} struct_mux_telegram;

static struct_mux_telegram lueftung_an;
static struct_mux_telegram temp_links;
static struct_mux_telegram temp_rechts;
static struct_mux_telegram stufe_lueftung;
static struct_mux_telegram richtung;
static struct_mux_telegram richtung_auto;
static struct_mux_telegram ac_on;

// Pointer fuer Flash-Speicherzugriff
uint8_t *ptr_vin;           // VIN 
uint8_t *ptr_vehicle_type;   // Fahrzeugtyp aus VIN Stelle 7 und 8
//char *ptr_vehicle_model;  // Fahrzeughersteller und -modell
uint8_t *ptr_vehicle_model;  // Fahrzeughersteller und -modell
uint8_t *ptr_driver_profile; // Driver Profile

typedef enum {
    AUTOMATIC_CLIMATE_CONTROL = 0,
    MANUAL_CLIMATE_CONTROL
} CLIMATE_CONTROL;
static CLIMATE_CONTROL climate_control;


typedef enum {
    STATE_IDLE = 0,                   // CAN-Relais geschlossen, Ansteuerung inaktiv
    STATE_WAKEUP,                     // mit Zuendung aus auf CAN2 das KSG wecken
    STATE_GET_CLIMATE_INIT_VALUES,    // Uebernahme alte Klimaeinstellungen
    STATE_CLIMATE_SETTING_ADJUST,     // Einstellphase
    STATE_RUN,                        // kontinuierliche Ansteuerung der Klima
    STATE_USER_SETTING_RESTORE,       // Rueckstellphase auf Kundeneinstellungen
    STATE_POSTRUN,                    // mit Zuendung aus auf CAN2 das KSG in Ruhe versetzen
    STATE_WAIT_FOR_CLIMATE_BUS_IDLE   // auf Busruhe warten vor dem Verbinden des CAN-Relais
    } STATES;
static STATES state;

// aktueller Zustand des Heizgeraetes
IUG_UNIT_API_STATE_t heater_state;

typedef enum {
    VARIANT_MQB_EVO = 0,    // ab 2019, default
    VARIANT_MQB,            // ab 2012
    VARIANT_AUDI_EVO,       // Audi A3
    VARIANT_80_PERCENT      // T6.1 SH Einbau / Taigo
} VARIANTS;
static VARIANTS variant;


typedef enum {
    AUTO_MODE_NOT_AVAILABLE = 0,   // keine Einstellung moeglich, default --> T6.1
    AUTO_MODE_SEPERATE,            // Auto-Mode in extra Identifier - Richtung steckt in den 3 halben Bytes, Richtungs-Bytes 0 und 1 --> MQB-Evo, Audi-Evo
    AUTO_MODE_COMBINED,            // Auto-Mode in Richtungs-Byte 3 - Richtung steckt in 3 einzelnen Bytes, Richtungs-Bytes 0 bis 2 --> MQB
} AUTO_MODE_CONFIG;
static AUTO_MODE_CONFIG auto_mode;

// Alle ueberprueften Fahrzeugmodelle - muss mit dem Parameter gleich gehalten werden!!!
typedef enum {
    VEHICLE_MODEL_UNKNOWN = 0, 
    VEHICLE_MODEL_SKODA_SUPERB_III_FL,            
    VEHICLE_MODEL_SKODA_OCTAVIA_III,
    VEHICLE_MODEL_VW_PASSAT_2019,
    VEHICLE_MODEL_SKODA_OCTAVIA_IV,
    VEHICLE_MODEL_VW_GOLF_VIII,
    VEHICLE_MODEL_SEAT_LEON_IV,
    VEHICLE_MODEL_AUDI_A3,
    VEHICLE_MODEL_VW_T6_PA,
    VEHICLE_MODEL_VW_TIGUAN_II_FL,
    VEHICLE_MODEL_VW_CADDY_V,
    VEHICLE_MODEL_SKODA_KODIAQ,
    VEHICLE_MODEL_SEAT_TARRACO,
    VEHICLE_MODEL_CUPRA_FORMENTOR,
    VEHICLE_MODEL_SKODA_KAMIQ,
    VEHICLE_MODEL_VW_POLO,
    VEHICLE_MODEL_VW_T_CROSS,
    VEHICLE_MODEL_SEAT_ARONA,
    VEHICLE_MODEL_SKODA_FABIA,
    VEHICLE_MODEL_VW_TAIGO,
    VEHICLE_MODEL_VW_T_ROC,
    VEHICLE_MODEL_SKODA_KAROQ
} VEHICLE_MODELS;
static VEHICLE_MODELS vehicle_model;

void debug_handling(void);
void state_machine(void);
void cyclic_can_send(void);
void hardware_handling(void);
void select_vehicle_type(void);
void old_user_setting_init(void);
void climate_setting_adjust(void);
void user_setting_restore(void);
void temperature_handling(void);
void cold_start(void);
uint8_t user_temp_sensor_to_vehicle_temperature(const int16_t real_temperature, const uint8_t temperature_offset, const uint8_t temperature_factor);


/*----------------------------------------------------------------------------*/
/**
* \internal
* user-defined c-code INIT
* \endinternal
*/
void usercode_init( void )
{
    state = STATE_IDLE;
    // bei unbekannten Fahrzeugen wird von der neuesten Generation ausgegangen
    variant = VARIANT_MQB_EVO;
    //Konfiguration Automatikmodus der Klima - default setzen
    auto_mode = AUTO_MODE_NOT_AVAILABLE;
    // initialisieren der internen Temperaturwerte mit "ungueltig"
    start_temperature = TEMP_VALUE_INVALID;
    cronus_temperature = TEMP_VALUE_INVALID;
    // initialisieren der Fahrzeug-Temperaturwerte mit "init"
    klsen_02_temp = TEMP_VALUE_CAR_INIT;
    motor_07_temp = TEMP_VALUE_CAR_INIT;

    // Diagnose: Initialisieren von Kilometerstand = Init (0xFFFFE), Uhrzeit = 11:11 Uhr , Datum = 11.11.2011
    kilometer[0] = 0xFF;
    kilometer[1] = 0xFE;
    kilometer[2] = 0xFF;
    kilometer[3] = 0xBF;
    kilometer[4] = 0xD8;
    kilometer[5] = 0xB5;
    kilometer[6] = 0x96;
    kilometer[7] = 0xC5;

    // Kombi_02: Initialisieren von Kilometerstand = Init (0xFFFFFE), Standzeit = Init, Tankinhalt = 10l, Aussentemp gefiltert = Init
    kilometer2[0] = 0xFE;
    kilometer2[1] = 0xFF;
    kilometer2[2] = 0xEF;
    kilometer2[3] = 0xFF;
    kilometer2[4] = 0x5F;
    kilometer2[5] = 0x0A;
    kilometer2[6] = 0x96;
    kilometer2[7] = 0xFE;

    ptr_vin = get_para_ary_by_id(ID_VEHICLE_IDENTIFICATION_NUMBER);

    vin[0] = ptr_vin[0];
    vin[1] = ptr_vin[1];
    vin[2] = ptr_vin[2];
    vin[3] = ptr_vin[3];
    vin[4] = ptr_vin[4];
    vin[5] = ptr_vin[5];
    vin[6] = ptr_vin[6];
    vin[7] = ptr_vin[7];
    vin[8] = ptr_vin[8];
    vin[9] = ptr_vin[9];
    vin[10] = ptr_vin[10];
    vin[11] = ptr_vin[11];
    vin[12] = ptr_vin[12];
    vin[13] = ptr_vin[13];
    vin[14] = ptr_vin[14];
    vin[15] = ptr_vin[15];
    vin[16] = ptr_vin[16];

    
    (void)API_utilities_timer_set_timestamp(&ti_vehicle_can_active, API_UTILITIES_TIMER_PRECISION_1MS);    // Timer fuer CAN-Empfangserkennung CAN1
    (void)API_utilities_timer_set_timestamp(&ti_wait_for_temp, API_UTILITIES_TIMER_PRECISION_1MS);    // Timer fuer Temperaturerkennung (Taster oder des am h�chsten priorisierten Au�entempsensor)
}
/*----------------------------------------------------------------------------*/
/**
* \internal
* user-defined c-code
* \endinternal
*/
void usercode( void )
{
    uint8_t ti_vehicle_can_active_elapsed , ti_blower_test_time_elapsed;
    static uint32_t ti_blower_test_time;

    // Abfrage der Temperatur von Cronus
    temperature_handling();

    // Heizen/Lueften aktiv
    status_climate_control = API_car_climate_ctrl_get_status_climate_ctrl();
    // Fahrzeugzustand (Fahrzeugzuendung an):
    ign_vehicle = API_car_climate_ctrl_get_car_state();
    // Inbetriebnahme durchgefuehrt:
    ibn_done = get_para_by_id(ID_INITIAL_OPERATION);
    // Heizungszustand
    API_units_get_unit_state(IUG_UNIT_API_UNIT_HEATER_HG1, &heater_state);
    // debug
    debug = get_para_by_id(ID_APPLICATION_DEBUG);
    // Steuerung ob Klimaeinstellungen verstellt werden 100% oder 80%
    climate_settings = get_para_by_id(ID_CLIMATE_SETTINGS);
    // Gebl�setest
    blower_test = get_para_by_id(ID_BLOWER_TEST);
    // Abfrage Heizger�tewassertemperatur
    heater_water_temp = get_para_by_id(ID_COOLANT_TEMP_WAT);
    // Abfrage Gebl�seeinschaltschwelle
    blower_on_threshold = get_para_by_id(ID_TEMP_BLOWER_ON_WAT);


    if ((((heater_water_temp >= blower_on_threshold) && (heater_state == IUG_UNIT_API_STATE_HEATING)) || (heater_state == IUG_UNIT_API_STATE_VENTILATION)) && (ign_vehicle == FALSE))
    {
        status_climate_control_car_am = TRUE;
    }
    else
    {
        status_climate_control_car_am = FALSE;
    }
    

    // Setzen der Pointer auf die jeweilige Variable
    ptr_vin = get_para_ary_by_id(ID_VEHICLE_IDENTIFICATION_NUMBER);
    ptr_vehicle_type = get_para_ary_by_id(ID_VEHICLE_TYPE);
    //ptr_vehicle_model = (char *)get_para_by_id(ID_VEHICLE_MODEL);
    ptr_vehicle_model = get_para_ary_by_id(ID_VEHICLE_MODEL);
    ptr_driver_profile = get_para_ary_by_id(ID_DRIVER_PROFILE);
    /* Abgleich zwischen gepufften Werten und dem Speicherwert -> nur schreiben, wenn sich etwas ge�ndert hat */
    
    if ((driver_profile != ptr_driver_profile[0]) && driver_profile != 0 )
    {
        ptr_driver_profile[0] = driver_profile;
        
        if(debug == TRUE)
        {
            (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_3,0x711,FALSE,8,ptr_driver_profile[0],driver_profile,0,0,0,0,0,0);
        }
    }

    driver_profile = ptr_driver_profile[0];

    // Fahrzeughersteller und -modell
    if (vehicle_model != ptr_vehicle_model[0]) // 0 ist zulaessig, da hier als default genutzt (="unbekannt")
    {
        ptr_vehicle_model[0] = vehicle_model;
    }
    
    
    if (   (vin[0] != ptr_vin[0]) ||
           (vin[1] != ptr_vin[1]) ||
           (vin[2] != ptr_vin[2]) ||
           (vin[3] != ptr_vin[3]) ||
           (vin[4] != ptr_vin[4]) ||
           (vin[5] != ptr_vin[5]) ||
           (vin[6] != ptr_vin[6]) ||
           (vin[7] != ptr_vin[7]) ||
           (vin[8] != ptr_vin[8]) ||
           (vin[9] != ptr_vin[9]) ||
           (vin[10] != ptr_vin[10]) ||
           (vin[11] != ptr_vin[11]) ||
           (vin[12] != ptr_vin[12]) ||
           (vin[13] != ptr_vin[13]) ||
           (vin[14] != ptr_vin[14]) ||
           (vin[15] != ptr_vin[15]) ||
           (vin[16] != ptr_vin[16])     )
    {
        if(debug == TRUE)
        {
            (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_3,0x722,FALSE,8, ptr_vin[0] , vin[0] , ptr_vin[1] , vin[1] , ptr_vin[2] , vin[2] , 0 , 0);
        }

        ptr_vin[0] = vin[0];
        ptr_vin[1] = vin[1];
        ptr_vin[2] = vin[2];
        ptr_vin[3] = vin[3];
        ptr_vin[4] = vin[4];
        ptr_vin[5] = vin[5];
        ptr_vin[6] = vin[6];
        ptr_vin[7] = vin[7];
        ptr_vin[8] = vin[8];
        ptr_vin[9] = vin[9];
        ptr_vin[10] = vin[10];
        ptr_vin[11] = vin[11];
        ptr_vin[12] = vin[12];
        ptr_vin[13] = vin[13];
        ptr_vin[14] = vin[14];
        ptr_vin[15] = vin[15];
        ptr_vin[16] = vin[16];
    }


    /*
    // VIN
    if ((vin[0] != ptr_vin[0]) && (vin[0] != 0))
    {
        if(debug == TRUE)
        {
            (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_3,0x722,FALSE,8, ptr_vin[0] , vin[0] , ptr_vin[1] , vin[1] , ptr_vin[2] , vin[2] , 0 , 0);
        }
        ptr_vin[0] = vin[0];
        ptr_vin[1] = vin[1];
        ptr_vin[2] = vin[2];
    }
    if ((vin[3] != ptr_vin[3]) && (vin[3] != 0))
    {
        ptr_vin[3] = vin[3];
        ptr_vin[4] = vin[4];
        ptr_vin[5] = vin[5];
        ptr_vin[6] = vin[6];
        ptr_vin[7] = vin[7];
        ptr_vin[8] = vin[8];
        ptr_vin[9] = vin[9];
    }
    if ((vin[10] != ptr_vin[10]) && (vin[10] != 0))
    {
        ptr_vin[10] = vin[10];
        ptr_vin[11] = vin[11];
        ptr_vin[12] = vin[12];
        ptr_vin[13] = vin[13];
        ptr_vin[14] = vin[14];
        ptr_vin[15] = vin[15];
        ptr_vin[16] = vin[16];
    }

    */

    // Fahrzeugtyp (aus VIN Stelle 7 und 8)
    if ((vin[6] != ptr_vehicle_type[0]) || (vin[7] != ptr_vehicle_type[1]))
    {
        ptr_vehicle_type[0] = vin[6];
        ptr_vehicle_type[1] = vin[7];
    }
    //vehicle_type = ptr_vehicle_type[0] << 8 | ptr_vehicle_type[1]; //ToDo
    vehicle_type[0] = ptr_vehicle_type[0];
    vehicle_type[1] = ptr_vehicle_type[1];

   // Debug-Nachrichten:
    debug_handling();

    // Relais und Abschlusswiderstand schalten:
    hardware_handling();

    // Fahrzeugkommunikation (CAN1) letzter Empfang timeout abgelaufen?
    (void)API_utilities_timer_check_timestamp(&ti_vehicle_can_active_elapsed, ti_vehicle_can_active, C_TI_VEHICLE_CAN_ACTIVE, API_UTILITIES_TIMER_PRECISION_1MS);
    if ( ti_vehicle_can_active_elapsed == TRUE )
    {
        vehicle_can_active = FALSE;
    }

    // Anwender Erkennung: Wenn in diesem Wachzyklus schon einmal die Fzg. Zuendung an war.
    if (( ign_vehicle_buf == TRUE ) && ( ibn_done == INITIAL_OPERATION_OK ))
    {
        user_present = TRUE;
    }
    else
    {
        user_present = FALSE;
    }

    
    // Steigende Flanke blower test
    if ( (blower_test == TRUE) && (blower_test_buf == FALSE) )  
    {
        (void)API_utilities_timer_set_timestamp(&ti_blower_test_time, API_UTILITIES_TIMER_PRECISION_1MS);
        blower_test_buf = TRUE;
         (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_3,0x7C1,FALSE,8, blower_test , blower_test_buf, 0 , 0,0,0 , 0 , 0);
    }

    (void)API_utilities_timer_check_timestamp(&ti_blower_test_time_elapsed, ti_blower_test_time, C_TI_BLOWER_TEST_TIME, API_UTILITIES_TIMER_PRECISION_1MS);

  
    
    if ( (ti_blower_test_time_elapsed == TRUE) && (blower_test == TRUE) )
    {
        (void)API_utilities_timer_set_timestamp(&ti_blower_test_time, API_UTILITIES_TIMER_PRECISION_1MS);
        blower_test = FALSE;
        blower_test_buf = FALSE;
        set_para_by_id(ID_BLOWER_TEST, blower_test);
        (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_3,0x7C2,FALSE,8, blower_test , blower_test_buf, ti_blower_test_time_elapsed , 0,0,0 , 0 , 0);
    }


    state_machine();
    cyclic_can_send();
    climate_setting_adjust();  // Klimaeinstellungen durchfuehren
    user_setting_restore();    // Klimarueckstellungen durchfuehren
    select_vehicle_type();     // Fahrzeugtyp identifizieren und Variablen befuellen
    
    if ( vehicle_model == VEHICLE_MODEL_VW_T6_PA )
    {
        // cold_start();              // Kaltstartfunktionen f�r Motorfehler
    }
}
/*----------------------------------------------------------------------------*/
/**
 * \brief Abfrage der Innenraum-Temperatur von Cronus
 * 
 */
void temperature_handling(void)
{
    int16_t temp_buf;   // 16 bit buffer fuer die Umrechnung von int auf uint
    uint8_t temp_error; // Pruefung der Temperatur auf Gueltigkeit
    uint8_t ti_wait_for_temp_elapsed;

    // Temperatur von Cronus abfragen
    temp_error = API_environment_get_temperature_priority(API_ENV_TEMP_SENS_INTERNAL, &temp_buf);

    // nur gueltige Werte uebernehmen
    if ( temp_error == API_ENV_ERR_TEMP_SENS_NONE )
    {
        // negative Werte eliminieren, cast int16 in uint8
        cronus_temperature = (uint8_t) temp_buf + W_BUS_TEMPERATURE_OFFSET;    
        // Initialisierungszeit der Cronus-Temperatur ueberbruecken
        (void)API_utilities_timer_check_timestamp(&ti_wait_for_temp_elapsed, ti_wait_for_temp, C_TI_WAIT_FOR_TEMP, API_UTILITIES_TIMER_PRECISION_1MS);

        if ( ti_wait_for_temp_elapsed == TRUE )
        {
            // Uebernahme der ersten gueltigen Temperatur
            if ( start_temperature == TEMP_VALUE_INVALID )
            {
                start_temperature = cronus_temperature;
                if(debug == TRUE)
                {
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_3,0x7D1,FALSE,8,cronus_temperature >> 8,cronus_temperature,start_temperature,klsen_02_temp,motor_07_temp,0,0,0);
                }
            }
            // Beschreiben der Fahrzeugwerte fuer Temperatur, falls noch nicht bekannt - wird bei Empfang auf CAN1 korrigiert
            if ( klsen_02_temp == TEMP_VALUE_CAR_INIT )
            {
                klsen_02_temp = user_temp_sensor_to_vehicle_temperature(temp_buf, 50, 50); // Offset 50, Skalierung 0,5
                if(debug == TRUE)
                {
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_3,0x7D1,FALSE,8,cronus_temperature >> 8,cronus_temperature,start_temperature,klsen_02_temp,motor_07_temp,0,0,0);
                }
            }
            if ( motor_07_temp == TEMP_VALUE_CAR_INIT )
            {
                motor_07_temp = user_temp_sensor_to_vehicle_temperature(temp_buf, 48, 75); // Offset 48, Skalierung 0,75
                if(debug == TRUE)
                {
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_3,0x7D1,FALSE,8,cronus_temperature >> 8,cronus_temperature,start_temperature,klsen_02_temp,motor_07_temp,0,0,0);
                }
            }
        }
    }
    else 
    {
        cronus_temperature = TEMP_VALUE_INVALID; // ToDo: Fallback bei ungueltigen Werten?
    }
    // cronus_temperature = get_para_by_id(ID_VAL_BUTTON_TEMP_SENSOR);
   
}
/*----------------------------------------------------------------------------*/
/**
 * \brief Zyklische Debug-Ausgabe - kann ueber Variable debug global an oder aus geschaltet werden
 * 
 */
void debug_handling(void)
{
    static uint32_t ti_can_out_debug;        // Zaehler Debug_Ausgabe
    uint8_t ti_can_out_debug_elapsed;

    (void)API_utilities_timer_check_timestamp(&ti_can_out_debug_elapsed, ti_can_out_debug, C_TI_DEBUG_CAN_OUT, API_UTILITIES_TIMER_PRECISION_1MS);

    if (( debug == TRUE ) && ( ti_can_out_debug_elapsed == TRUE ))
    {
        (void)API_utilities_timer_set_timestamp(&ti_can_out_debug, API_UTILITIES_TIMER_PRECISION_1MS);
    
        (void)API_utilities_can_send_message(
            API_SHARED_CONST_CAN_BUS_3, 
            0x1FFFFFF1,
            TRUE,
            8,
            status_climate_control,
            vehicle_can_active,
            ign_vehicle,
            state,
            init_done,
            setting_done,
            user_present,
            heater_state
            );

        (void)API_utilities_can_send_message(
            API_SHARED_CONST_CAN_BUS_3,
            0x1FFFFFF2,
            TRUE,
            8,
            variant,
            driver_profiles_available,
            vehicle_model,
            richtung_auto.init_data[0],
            richtung_auto.data[0],
            auto_mode,
            vehicle_type[0],
            vehicle_type[1]
            );
        (void)API_utilities_can_send_message(
            API_SHARED_CONST_CAN_BUS_3,
            0x1FFFFFF3,
            TRUE,
            8,
            driver_profile,
            ptr_driver_profile[0],
            driver_profile_old,
            new_driver_profile_detect,
            ac_on.init_data[0],
            ac_on.data[0],
            vin[6],
            vin[7]
            );

        (void)API_utilities_can_send_message(
            API_SHARED_CONST_CAN_BUS_3,
            0x1FFFFFF4,
            TRUE,
            8,
            climate_control,
            temp_links.data[0],
            temp_links.data[1],
            temp_links.init_data[0],
            temp_links.init_data[1],
            temp_links.heating_data[0],
            temp_links.heating_data[1],
            climate_settings
            );

            (void)API_utilities_can_send_message(
            API_SHARED_CONST_CAN_BUS_3,
            0x1FFFFFF5,
            TRUE,
            8,
            cronus_temperature,
            start_temperature,
            heater_water_temp,
            heater_water_temp_start,
            cold_engine_start_detect,
            r_control_active,
            eng_on,
            eng_on_cnt
            );

            (void)API_utilities_can_send_message(
            API_SHARED_CONST_CAN_BUS_3,
            0x1FFFFFF6,
            TRUE,
            8,
            heater_state_off_flag,
            heater_state_off_cnt >> 8,
            heater_state_off_cnt,
            blower_test,
            blower_test_buf,
            heater_state_on_flag,
            heater_water_temp_old,
            0
            );

            (void)API_utilities_can_send_message(
            API_SHARED_CONST_CAN_BUS_3,
            0x1FFFFFF7,
            TRUE,
            8,
            status_climate_control,
            status_climate_control_car_am,
            heater_water_temp,
            blower_on_threshold,
            heater_state,
            ign_vehicle,
            state,
            ibn_done
            );

    }
}

/*----------------------------------------------------------------------------*/
/**
 * \brief Zyklisches Senden der Telegramme in Richtung Klima bei offenem CAN-Relais
 * 
 */
void cyclic_can_send(void)
{
    uint8_t ig_byte_1, ig_byte_2, ig_byte_3, ig_byte_4;
    static uint32_t ti_can_out_100ms, ti_can_out_200ms, ti_can_out_500ms, ti_can_out_1000ms;
    uint8_t ti_can_out_100ms_elapsed, ti_can_out_200ms_elapsed, ti_can_out_500ms_elapsed, ti_can_out_1000ms_elapsed;
    uint8_t ti_send_delay_elapsed;

    if (( state != STATE_IDLE ) && ( state != STATE_WAIT_FOR_CLIMATE_BUS_IDLE ))
    {
        // Befuellen des Telegramms Zuendung
        if (( state == STATE_GET_CLIMATE_INIT_VALUES ) || ( state == STATE_CLIMATE_SETTING_ADJUST ) || ( state == STATE_RUN ) || ( state == STATE_USER_SETTING_RESTORE ))
        {
            // Zuendung auf 1, Checksumme anpassen
            ig_byte_1 = 0x84;
            ig_byte_2 = 0x03;
            ig_byte_3 = 0x03;
            ig_byte_4 = 0x00;
        }
        else
        {
            // Zuendung auf 0, Checksumme anpassen // ToDo: Zuendung aus Daten anpassen (Checksumme)
            ig_byte_1 = 0x00;
            ig_byte_2 = 0x00;
            ig_byte_3 = 0x00;
            ig_byte_4 = 0x00;
        }
    
        (void)API_utilities_timer_check_timestamp(&ti_send_delay_elapsed, ti_send_delay, C_TI_SEND_DELAY, API_UTILITIES_TIMER_PRECISION_1MS);

        if ( ti_send_delay_elapsed == TRUE )
        {
            // Schleife 100ms
            (void)API_utilities_timer_check_timestamp(&ti_can_out_100ms_elapsed, ti_can_out_100ms, 100, API_UTILITIES_TIMER_PRECISION_1MS);
            if ( ti_can_out_100ms_elapsed == TRUE )   
            {
                // Timer zuruecksetzen
                (void)API_utilities_timer_set_timestamp(&ti_can_out_100ms, API_UTILITIES_TIMER_PRECISION_1MS); 
            
                // Zuendung
                if ( ign_vehicle == FALSE )
                {
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x3C0, FALSE, 4, ig_byte_1, ig_byte_2, ig_byte_3, ig_byte_4, 0, 0, 0, 0); // Zuendung senden
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x040, FALSE, 8, 0xA8, 0x0B, 0x00, 0x00, 0xC1, 0x00, 0x00, 0x00);     // Airbag // ToDo: nicht bei State Wake_UP und State Post_POSTRUN
                }

                if ( vehicle_can_active == FALSE )
                {
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x663, FALSE, 8, 0x60, 0x28, 0x01, 0x09, 0x00, 0x9B, 0x00, 0x00);  // BEM_02
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x3DA, FALSE, 8, 0x20, 0x02, 0x1A, 0x00, 0x00, 0xA8, 0x1D, 0x00);  // Gateway_71
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x3DC, FALSE, 8, 0xFC, 0x08, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00);  // Gateway_73
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x3C7, FALSE, 8, 0xFD, 0x00, 0x10, 0x00, 0x00, 0x40, 0xA2, 0x00);  // Motor_06 / Motor_26 (ICAN) Motor aus
                    // (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x3C7, FALSE, 8, 0x14, 0x00, 0x00, 0x00, 0x00, 0x40, 0x00, 0x00);  // Motor_06 / Motor_26 (ICAN) Motor ein
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x3BE, FALSE, 8, 0x00, 0x20, 0x07, 0x01, 0x22, 0xC0, 0x08, 0x80);  // Motor_14 Motor aus
                    //(void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x3BE, FALSE, 8, 0x1C, 0x15, 0xE7, 0x01, 0xC8, 0xC0, 0x0C, 0x80);  // Motor_14 Motor ein
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x3D0, FALSE, 8, 0x00, 0x00, 0x00, 0x00, 0x8C, 0x00, 0x03, 0x00);  // TSG_FT_01 Tuersteuergeraet Fahrertuer (Tuer zu, Fenster zu, Spiegelheizung aus, etc.)
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x3D1, FALSE, 8, 0x00, 0x00, 0x00, 0x00, 0x8C, 0x00, 0x00, 0x00);  // TSG_BT_01 Tuersteuergeraet Beifahrertuer (Tuer zu, Fenster zu, etc.)
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x585, FALSE, 8, 0x00, 0x3C, 0x00, 0x7F, 0x12, 0x00, 0x00, 0x00);  // Systeminfo_01
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x30B, FALSE, 8, 0x50, 0x2E, 0x00, 0x00, 0x08, 0x00, 0x00, 0x10);  // Kombi_01
                }
            }

            // Schleife 200ms
            (void)API_utilities_timer_check_timestamp(&ti_can_out_200ms_elapsed, ti_can_out_200ms, 200, API_UTILITIES_TIMER_PRECISION_1MS);
            if ( ti_can_out_200ms_elapsed == TRUE)   
            {
                // Timer zuruecksetzen
                (void)API_utilities_timer_set_timestamp(&ti_can_out_200ms, API_UTILITIES_TIMER_PRECISION_1MS); 
            
                if ( ign_vehicle == FALSE )
                {
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x520, FALSE, 8, 0xF7, 0x0F, 0x00, 0x0C, 0x00, 0xAA, 0x02, 0x00);  // Airbag // ToDo: nicht bei State Wake_UP und State Post_POSTRUN
                }

                if ( vehicle_can_active == FALSE ) 
                {
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x5E1, FALSE, 8, klsen_02_temp, 0x2A, 0x00, 0x60, 0xFE, 0x00, 0x00, 0x00);  // Klima_Sensor_02 mit BCM1_AussenTemp (nicht mehr manipuliert auf -10 �C, V0.4)
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x521, FALSE, 8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xA0, 0x00);  // STH_01 manipuliert Wassertemp. 80 �C
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x5F0, FALSE, 8, 0x16, 0x00, 0x11, 0x00, 0x00, 0x0A, 0x00, 0x00);  // Dimmung_01
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x583, FALSE, 8, 0x00, 0x10, 0x80, 0x00, 0x00, 0x08, 0x40, 0x00);  // ZV_02
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x1B00000E, TRUE, 8, 0x0E, 0x00, 0x04, 0x01, 0x01, 0x08, 0x0E, 0x00);  // NMH_BCM1
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x1B000031, TRUE, 8, 0x31, 0x00, 0x04, 0x01, 0x01, 0x00, 0x00, 0x00);  // NMH_ELV
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x1B000010, TRUE, 8, 0x10, 0x00, 0x44, 0x01, 0x39, 0x10, 0x00, 0x00);  // NMH_Gateway
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x1B000032, TRUE, 8, 0x32, 0x10, 0x04, 0x03, 0x01, 0x00, 0x00, 0x00);  // NMH_Kessy
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x1B000014, TRUE, 8, 0x14, 0x00, 0x04, 0x01, 0x01, 0x00, 0x00, 0x00);  // NMH_Kombi
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x1B00000C, TRUE, 8, 0x0C, 0x00, 0x04, 0x01, 0x01, 0x00, 0x00, 0x00);  // NMH_SMLS
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x1B00004A, TRUE, 8, 0x4A, 0x00, 0x04, 0x01, 0x11, 0x00, 0x00, 0x00);  // NMH_TSG_FS
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x1B00004B, TRUE, 8, 0x4B, 0x00, 0x04, 0x01, 0x11, 0x00, 0x00, 0x00);  // NMH_TSG_BFS        
                }            
            }

            // Schleife 500ms
            (void)API_utilities_timer_check_timestamp(&ti_can_out_500ms_elapsed, ti_can_out_500ms, 500, API_UTILITIES_TIMER_PRECISION_1MS);
            if ( ti_can_out_500ms_elapsed == TRUE )   
            {
                // Timer zuruecksetzen
                (void)API_utilities_timer_set_timestamp(&ti_can_out_500ms, API_UTILITIES_TIMER_PRECISION_1MS); 
            
                if ( ign_vehicle == FALSE )
                {
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x6B0, FALSE, 6, 0x58, 0x02, 0x28, 0x01, 0x3B, 0x38,    0,    0); // Klima_Sensor_01 Frontscheibenheizung Taupunkt etc., Taupunkt manipuliert auf >20 �C, Temp_FS auf -10,  // ToDo: nicht bei State Wake_UP und State Post_POSTRUN
                }

                if ( vehicle_can_active == FALSE )
                {
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x640, FALSE, 8, 0x80, motor_07_temp, 0x8C, 0xAB, 0x82, 0x20, 0x00, 0x01); // Motor_07 manipuliert: Oel: 80 �C, Kuehlmittel: 80,25 �C (ansaug nicht mehr manipuliert, V0.4) Motor aus
                    //(void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x640, FALSE, 8, 0x00, 0x60, 0x57, 0x6A, 0x82, 0x60, 0x1A, 0x02); // Motor_07 manipuliert: Oel: 80 �C, Kuehlmittel: 80,25 �C (ansaug nicht mehr manipuliert, V0.4) Motor ein
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x5E0, FALSE, 8, 0x00, 0x00, 0x00, 0x80, 0x09, 0x00, 0x00, 0x00); // SAD_01 (?)
                }
            }

            // Schleife 1000ms
            (void)API_utilities_timer_check_timestamp(&ti_can_out_1000ms_elapsed, ti_can_out_1000ms, 1000, API_UTILITIES_TIMER_PRECISION_1MS);
            if ( ti_can_out_1000ms_elapsed == TRUE )   
            {
                // Timer zuruecksetzen
                (void)API_utilities_timer_set_timestamp(&ti_can_out_1000ms, API_UTILITIES_TIMER_PRECISION_1MS); 

                if ( ign_vehicle == FALSE )
                {
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x65A, FALSE, 8, 0x00, 0x00, 0x02, 0x08, 0x00, 0x00, 0x3C, 0x40); // BCM_01 // ToDo: nicht bei State Wake_UP und State Post_POSTRUN
                
                    
                    if (state == STATE_RUN || new_driver_profile_detect == TRUE)
                    {
                        (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x16A9540A, TRUE, 8, 0x00, driver_profile_old, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00); // Bei Erkennung des neuen Nutzers wird mit dem alten Nutzer weitergesendet
                    }
                    else
                    {
                         (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x16A9540A, TRUE, 8, 0x00, driver_profile, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00); // Benutzerauswahl deaktiviert (funktioniert auch bei verbauter Benutzerauswahl, dann wird Gast verstellt)/* code */
                    }
                    
                }

                


                if ( vehicle_can_active == FALSE )
                { 
                    /* Benutzerauswahl 
                        Byte 2 entspricht angemeldetem Benutzer
                        0x00            - keine Benutzerauswahl moeglich, im Fahrzeug nicht verbaut
                        0x10            - Gast oder keine Benutzer eingerichtet
                        0x20 ... 0x70   - angemeldeter Nutzer, scheinbar willkuerlich nummeriert */
                   
                    // (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x16A9540A, TRUE, 8, 0x00, 0x10, 0x00, 0x92, 0x40, 0x40, 0x00, 0x00); // Passat 1.5B, keine Benutzer angemeldet, daher immer "0x10"
                    // (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x16A9540A, TRUE, 8, 0x00, 0x30, 0x00, 0xD2, 0x80, 0x80, 0x00, 0x00); // Superb iV, 1 Benutzer angemeldet als user "0x30", 0x10 ist Gast
                
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x386, FALSE, 8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00); // Charisma_03
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x6B2, FALSE, 8, kilometer[0], kilometer[1], kilometer[2], kilometer[3], kilometer[4], kilometer[5], kilometer[6], kilometer[7]); // Diagnose_01 mit Kilometerstand und Datum
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x6B7, FALSE, 8, kilometer2[0], kilometer2[1], kilometer2[2], kilometer2[3], kilometer2[4], kilometer2[5], kilometer2[6], kilometer2[7]); // Kombi_02 (Aussentemp. ggf. nicht mehr manipuliert auf -10 �C, V0.4)
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x643, FALSE, 8, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00); // Einheiten_01
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x6A6, FALSE, 2, 0x10, 0x00,   0,    0,    0,    0,    0,    0); // Wischer
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x3D4, FALSE, 8, 0x2E, 0x05, 0x80, 0x02, 0x00, 0x06, 0x00, 0x00);  // SMLS_01
                }
            }
        }
    }
}
/*----------------------------------------------------------------------------*/
/**
 * \brief CAN Relais und externe Relais abhaengig vom State ein-/ausschalten, Timer fuer Sendeverzoegerung zuruecksetzen
 * 
 */
void hardware_handling(void)
{
    uint8_t ti_bus_termination_delay_elapsed;

    
    // In Ruhe, Ansteuerung inaktiv:
    if ( state == STATE_IDLE )
    {
        // externes Relais - K1, K2, ... abschalten
        // (void)API_IO_ctrl_set_X1_12_OUT_1_output(API_IO_CTRL_STATIC_OFF);

        // Delay-Timer wird beim Uebergang in STATE_IDLE gestartet, wenn Fahrzeug-CAN inaktiv ist (andernfalls sofort abgelaufen)
        (void)API_utilities_timer_check_timestamp(&ti_bus_termination_delay_elapsed, ti_bus_termination_delay, C_TI_BUS_TERMINATION_DELAY, API_UTILITIES_TIMER_PRECISION_1MS);
        // Bus termination CAN2 mit Nachlauf bei Busruhe abschalten --> ohne Nachlauf ist der Widerstand schneller als das CAN-Relais und fuehrt zum erneuten Aufwecken des CAN-Bus
        if ( ti_bus_termination_delay_elapsed == TRUE ) 
        {
            (void)API_IO_ctrl_set_dig_output(API_IO_CTRL_DO_CAN2_TERMINATION, API_IO_CTRL_STATIC_OFF);
        }

        // internes CAN Relais schliessen
        (void)API_IO_ctrl_set_dig_output(API_IO_CTRL_DO_RELAIS_CAN, API_IO_CTRL_STATIC_OFF);
    }
    // Bei Ansteuerung:
    else
    {
        // externes Relais - K1, K2, ... zuschalten
        //(void)API_IO_ctrl_set_X1_12_OUT_1_output(API_IO_CTRL_STATIC_ON);
    
        // internes CAN-Relais oeffnen
        (void)API_IO_ctrl_set_dig_output(API_IO_CTRL_DO_RELAIS_CAN, API_IO_CTRL_STATIC_ON);
    
        // Bus termination CAN2 zuschalten
        (void)API_IO_ctrl_set_dig_output(API_IO_CTRL_DO_CAN2_TERMINATION, API_IO_CTRL_STATIC_ON);
    }
}
/*----------------------------------------------------------------------------*/
/**
 * \brief   Globales Handling der Zustandsuebergaenge innerhalb der State-Machine:
 * \details Sofern die Vorraussetzungen erfuellt sind, koennen mehrere States sofort durchlaufen werden.
 * 
 */
void state_machine(void)
{
    static uint32_t ti_wakeup, ti_postrun, ti_get_values, ti_new_driver_profile_detect_delay_off;
    uint8_t ti_wakeup_elapsed, ti_postrun_elapsed, ti_get_values_elapsed, ti_climate_can_active_elapsed, ti_new_driver_profile_detect_delay_off_elapsed;

    if ( state == STATE_IDLE )
    {
        // Voraussetzung fuer Lueftung erfuellt (Zuendung aus, Lueftungsanforderung an)
        if (((status_climate_control_car_am == TRUE) || (blower_test == TRUE) || ((status_climate_control == TRUE) && (ibn_done != INITIAL_OPERATION_OK))) && ( new_driver_profile_detect == FALSE ) && ( ign_vehicle == FALSE )) 
        {
            /* Reset stuff and keep it reset: */
            state             = STATE_WAKEUP;
            init_done         = FALSE;
            setting_done      = FALSE;
            clima_cnt_adjust  = COUNTER_RESET_ADJUST;
            clima_cnt_restore = COUNTER_RESET_RESTORE;
                        
            (void)API_utilities_timer_set_timestamp(&ti_wakeup, API_UTILITIES_TIMER_PRECISION_1MS);
            
            // Puffer-Timer, um Umschaltzeit des Relais zu ueberbruecken - andernfalls kommen eventuell Telegramme ueber CAN-Relais auf CAN1 an
            (void)API_utilities_timer_set_timestamp(&ti_send_delay, API_UTILITIES_TIMER_PRECISION_1MS);
        }
        
        (void)API_utilities_timer_check_timestamp(&ti_new_driver_profile_detect_delay_off_elapsed, ti_new_driver_profile_detect_delay_off, 10000, API_UTILITIES_TIMER_PRECISION_1MS);
        if (ti_new_driver_profile_detect_delay_off_elapsed == TRUE && new_driver_profile_detect == TRUE)
        {
            new_driver_profile_detect = FALSE;
            //(void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_3, 0x7FF, FALSE, 8, 0,0,0,0,0,0,0,0);
            
        }
        
       
    }

    // Senden von Zuendung aus
    if ( state == STATE_WAKEUP )
    {
        (void)API_utilities_timer_check_timestamp(&ti_wakeup_elapsed, ti_wakeup, C_TI_WAKEUP, API_UTILITIES_TIMER_PRECISION_1MS);
    
        if (((status_climate_control_car_am == TRUE) || (blower_test == TRUE) || ((status_climate_control == TRUE) && (ibn_done != INITIAL_OPERATION_OK))) && ( ti_wakeup_elapsed == FALSE ) && ( vehicle_can_active == FALSE )) // Geblaeseanforderung aktiv und keine FahrzeugZuendung aktiv
        {
            // stay in state, do nothing
        }
        else
        {
            state = STATE_GET_CLIMATE_INIT_VALUES;
            (void)API_utilities_timer_set_timestamp(&ti_get_values, API_UTILITIES_TIMER_PRECISION_1MS); // 30 S Timer fuer Aufnahem der Ist KSG Werte starten
        }
    }

    if ( state == STATE_GET_CLIMATE_INIT_VALUES )
    {
        (void)API_utilities_timer_check_timestamp(&ti_get_values_elapsed, ti_get_values, C_TI_GET_VALUES, API_UTILITIES_TIMER_PRECISION_1MS);

        if (((status_climate_control_car_am == TRUE) || (blower_test == TRUE) || ((status_climate_control == TRUE) && (ibn_done != INITIAL_OPERATION_OK))) && ( init_done == FALSE ) && (ign_vehicle == FALSE))
        {
            if ( ti_get_values_elapsed == TRUE ) 
            {
                old_user_setting_init(); // alte Klimaeinstellung uebernehmen und init_done auf 1 (abgeschlossen) setzen 
            }
        }
        else
        {
            state = STATE_CLIMATE_SETTING_ADJUST;
        }
    }

    if ( state == STATE_CLIMATE_SETTING_ADJUST )
    {
        driver_profile_old = driver_profile; // Nutzer Profil welches verstellt wird merken und als alten Wert speichern 
        
        if (((status_climate_control_car_am == TRUE) || (blower_test == TRUE) || ((status_climate_control == TRUE) && (ibn_done != INITIAL_OPERATION_OK))) && ( setting_done == FALSE )  && (ign_vehicle == FALSE))
        {
            // stay in state, do nothing
        }
        else
        {
            state = STATE_RUN;
        }
    }

    if ( state == STATE_RUN )
    {
        if( vehicle_can_active == 1 && (driver_profile_old != driver_profile) )
        {
            new_driver_profile_detect = TRUE;
        }

        if ( ( status_climate_control_car_am == FALSE ) && ( blower_test == FALSE ) && ((status_climate_control == FALSE) || (ibn_done == INITIAL_OPERATION_OK))  || ( new_driver_profile_detect == TRUE ) || ( ign_vehicle == TRUE ) )
        {
            // Wenn Geblaeseanforderung aus -> weiter
            state = STATE_USER_SETTING_RESTORE;
        }
    }

    if ( state == STATE_USER_SETTING_RESTORE )
    {
        if ((setting_done == FALSE) || 
            (climate_adjust_available == FALSE))
        {
            // ueberspringen der Rueckstellung bei vorhandenen Fahrerprofilen oder wenn Einstellung bereits uebersprungen wurde
            // sobald das erste Telegramm vom Auto empfangen wird mit user > 0, wird automatisch die fahrerspezifische Einstellung uebernommen (vom Fahrzeug gesteuert)
            init_done = FALSE;
        }
        if ( init_done == FALSE )
        {
            // Wenn alles zurueck gesetzt -> weiter
            state = STATE_POSTRUN;
            (void)API_utilities_timer_set_timestamp(&ti_postrun, API_UTILITIES_TIMER_PRECISION_1MS);
            (void)API_utilities_timer_set_timestamp(&ti_new_driver_profile_detect_delay_off, API_UTILITIES_TIMER_PRECISION_1MS);
        }
        else
        {
            // stay in state, do nothing
        }    
    }

    if ( state == STATE_POSTRUN )
    {
        (void)API_utilities_timer_check_timestamp(&ti_postrun_elapsed, ti_postrun, C_TI_POSTRUN, API_UTILITIES_TIMER_PRECISION_1MS);
    
        // Abbruch bei Fahrzeug-CAN-Kommunikation
        if ((( vehicle_can_active == FALSE || new_driver_profile_detect == TRUE )) && ( ti_postrun_elapsed == FALSE )) //|| new_driver_profile_detect == TRUE  damit bei einem neuen driver Profile auch noch mit "Z�ndung aus" und dem "alten Profil" zum KSG geschickt wird
        {
            // stay in state, do nothing
        }
        else
        {
            state = STATE_WAIT_FOR_CLIMATE_BUS_IDLE;
        }
    }

    if ( state == STATE_WAIT_FOR_CLIMATE_BUS_IDLE )
    {
        (void)API_utilities_timer_check_timestamp(&ti_climate_can_active_elapsed, ti_climate_can_active, C_TI_CLIMATE_CAN_ACTIVE, API_UTILITIES_TIMER_PRECISION_1MS);
    
        // Abbruch bei Fahrzeug-CAN-Kommunikation oder Klima-CAN-Timeout
        if (( vehicle_can_active == FALSE ) && ( ti_climate_can_active_elapsed == FALSE ))
        {
            // stay in state, do nothing
        }
        else
        {
            if ( vehicle_can_active == FALSE )
            {
                (void)API_utilities_timer_set_timestamp(&ti_bus_termination_delay, API_UTILITIES_TIMER_PRECISION_1MS);
            }
            state = STATE_IDLE;
        }
    }
}

void select_vehicle_type(void)
{
    // Fahrzeugtyp der Variante zuordnen
    
    if ((vehicle_type[0] == 'N') &&
        (vehicle_type[1] == 'P')) 
    {
        vehicle_model = VEHICLE_MODEL_SKODA_SUPERB_III_FL;
        variant = VARIANT_MQB;    
    }
    else if ((vehicle_type[0] == 'N') &&
             (vehicle_type[1] == 'E')) 
    {
        vehicle_model = VEHICLE_MODEL_SKODA_OCTAVIA_III;
        variant = VARIANT_MQB;    
    }
    else if ((vehicle_type[0] == '3') &&
             (vehicle_type[1] == 'C')) 
    {
        vehicle_model = VEHICLE_MODEL_VW_PASSAT_2019;
        variant = VARIANT_MQB;    
    }
    else if ((vehicle_type[0] == 'N') &&
             (vehicle_type[1] == 'X')) 
    {
        vehicle_model = VEHICLE_MODEL_SKODA_OCTAVIA_IV;
        variant = VARIANT_MQB_EVO;
    }
    else if ((vehicle_type[0] == 'C') &&
             (vehicle_type[1] == 'D')) 
    {
        vehicle_model = VEHICLE_MODEL_VW_GOLF_VIII;
        variant = VARIANT_MQB_EVO;
    }
    else if ((vehicle_type[0] == 'K') &&
             (vehicle_type[1] == 'L')) 
    {
        vehicle_model = VEHICLE_MODEL_SEAT_LEON_IV;
        variant = VARIANT_MQB_EVO;
    }
    else if ((vehicle_type[0] == 'G') &&
             (vehicle_type[1] == 'Y')) 
    {
        vehicle_model = VEHICLE_MODEL_AUDI_A3;
        variant = VARIANT_AUDI_EVO;
    }
    else if ((vehicle_type[0] == '7') &&
             (vehicle_type[1] == 'H')) 
    {
        vehicle_model = VEHICLE_MODEL_VW_T6_PA;
        variant = VARIANT_80_PERCENT;
    }
    else if ((vehicle_type[0] == '5') &&
             (vehicle_type[1] == 'N')) 
    {
        vehicle_model = VEHICLE_MODEL_VW_TIGUAN_II_FL;
        variant = VARIANT_MQB;
    }
    else if ((vehicle_type[0] == 'S') &&
             (vehicle_type[1] == 'K')) 
    {
        vehicle_model = VEHICLE_MODEL_VW_CADDY_V;
        variant = VARIANT_MQB_EVO;
    }
    else if ((vehicle_type[0] == 'N') &&
             (vehicle_type[1] == 'S')) 
    {
        vehicle_model = VEHICLE_MODEL_SKODA_KODIAQ;
        variant = VARIANT_MQB;
    }
    else if ((vehicle_type[0] == 'K') &&
             (vehicle_type[1] == 'N')) 
    {
        vehicle_model = VEHICLE_MODEL_SEAT_TARRACO;
        variant = VARIANT_MQB;
    }
    else if ((vehicle_type[0] == 'K') &&
             (vehicle_type[1] == 'M')) 
    {
        vehicle_model = VEHICLE_MODEL_CUPRA_FORMENTOR;
        variant = VARIANT_MQB_EVO;
    }
    else if ((vehicle_type[0] == 'N') &&
             (vehicle_type[1] == 'W')) 
    {
        vehicle_model = VEHICLE_MODEL_SKODA_KAMIQ;
        variant = VARIANT_MQB;
    }
    else if ((vehicle_type[0] == 'A') &&
             (vehicle_type[1] == 'W')) 
    {
        vehicle_model = VEHICLE_MODEL_VW_POLO;
        variant = VARIANT_MQB;
    }
    else if ((vehicle_type[0] == 'C') &&
             (vehicle_type[1] == '1')) 
    {
        vehicle_model = VEHICLE_MODEL_VW_T_CROSS;
        variant = VARIANT_MQB;
    }
    else if ((vehicle_type[0] == 'K') &&
             (vehicle_type[1] == 'J')) 
    {
        vehicle_model = VEHICLE_MODEL_SEAT_ARONA;
        variant = VARIANT_MQB;
    }
    else if ((vehicle_type[0] == 'P') &&
             (vehicle_type[1] == 'J')) 
    {
        vehicle_model = VEHICLE_MODEL_SKODA_FABIA;
        variant = VARIANT_MQB;
    }
    else if ((vehicle_type[0] == 'C') &&
             (vehicle_type[1] == 'S')) 
    {
        vehicle_model = VEHICLE_MODEL_VW_TAIGO;
        variant = VARIANT_MQB;
    }
    else if ((vehicle_type[0] == 'A') &&
             (vehicle_type[1] == '1')) 
    {
        vehicle_model = VEHICLE_MODEL_VW_T_ROC;
        variant = VARIANT_MQB;
    }
    else if ((vehicle_type[0] == 'N') &&
             (vehicle_type[1] == 'U')) 
    {
        vehicle_model = VEHICLE_MODEL_SKODA_KAROQ;
        variant = VARIANT_MQB;
    }
    else // wenn der Fahrzeugtyp unbekannt ist:
    {
        vehicle_model = VEHICLE_MODEL_UNKNOWN;
        variant = VARIANT_MQB_EVO;
    }


    // BAP IDs abhaengig von der Variante unterscheiden
    switch (variant)
    {
        case VARIANT_MQB_EVO:
        {
            //climate adjust available - main switch
            if (climate_settings == TRUE)
            {
                climate_adjust_available = TRUE;
            }    
            else
            {
                climate_adjust_available = FALSE;
            }
            
            //MUX IDs
            lueftung_an.mux_id = MUX_ID_0_ON_OFF;
            temp_links.mux_id = MUX_ID_0_TEMP_LEFT;
            temp_rechts.mux_id = MUX_ID_0_TEMP_RIGHT;
            stufe_lueftung.mux_id = MUX_ID_0_BLOWER_SPEED;
            richtung.mux_id = MUX_ID_0_DIRECTION;
            richtung_auto.mux_id = MUX_ID_0_AUTO;
            //DLC
            lueftung_an.can_dlc = 3;
            temp_links.can_dlc = 8;
            temp_rechts.can_dlc = 8;
            stufe_lueftung.can_dlc = 6;
            richtung.can_dlc = 8;
            richtung_auto.can_dlc = 6;
            //Heating data - target values
            lueftung_an.heating_data[0] = 0x10;
            temp_links.heating_data[0] = 0xA0; // bei Klimaautomatik
            temp_links.heating_data[1] = 0x0F; // bei Klima manuell (Caddy)
            temp_rechts.heating_data[0] = 0xA0;
            stufe_lueftung.heating_data[0] = 0x30;
            richtung.heating_data[0] = 0xC0;
            richtung.heating_data[1] = 0x00;
            richtung.heating_data[2] = 0x00;
            richtung.heating_data[3] = 0x00;
            // Auto-Mode Konfiguration
            auto_mode = AUTO_MODE_SEPERATE;

            break;
        }
        case VARIANT_AUDI_EVO:
        {
            //climate adjust available - main switch
            if (climate_settings == TRUE)
            {
                climate_adjust_available = TRUE;
            }    
            else
            {
                climate_adjust_available = FALSE;
            }

            //MUX IDs
            lueftung_an.mux_id = MUX_ID_0_ON_OFF;
            temp_links.mux_id = MUX_ID_0_TEMP_LEFT;
            temp_rechts.mux_id = MUX_ID_0_TEMP_RIGHT;
            stufe_lueftung.mux_id = MUX_ID_0_BLOWER_SPEED;
            richtung.mux_id = MUX_ID_0_DIRECTION;
            richtung_auto.mux_id = MUX_ID_0_AUTO;
            //DLC
            lueftung_an.can_dlc = 3;
            temp_links.can_dlc = 8;
            temp_rechts.can_dlc = 8;
            stufe_lueftung.can_dlc = 6;
            richtung.can_dlc = 8;
            richtung_auto.can_dlc = 6;
            //Heating data - target values
            lueftung_an.heating_data[0] = 0x10;
            temp_links.heating_data[0] = 0xA0;
            temp_rechts.heating_data[0] = 0xA0;
            stufe_lueftung.heating_data[0] = 0x60;      //einziger Unterschied zu MQB-Evo
            richtung.heating_data[0] = 0xC0;
            richtung.heating_data[1] = 0x00;
            richtung.heating_data[2] = 0x00;
            richtung.heating_data[3] = 0x00;
            // Auto-Mode Konfiguration
            auto_mode = AUTO_MODE_SEPERATE;

            break;
        }
        case VARIANT_MQB:
        {
            //climate adjust available - main switch
            if (climate_settings == TRUE)
            {
                climate_adjust_available = TRUE;
            }    
            else
            {
                climate_adjust_available = FALSE;
            }

            //MUX IDs
            lueftung_an.mux_id = MUX_ID_1_ON_OFF;
            temp_links.mux_id = MUX_ID_1_TEMP_LEFT;
            temp_rechts.mux_id = MUX_ID_1_TEMP_RIGHT;
            stufe_lueftung.mux_id = MUX_ID_1_BLOWER_SPEED;
            richtung.mux_id = MUX_ID_1_DIRECTION;
            ac_on.mux_id = MUX_ID_1_AC_ON;
            //DLC
            lueftung_an.can_dlc = 3;
            temp_links.can_dlc = 4;
            temp_rechts.can_dlc = 4;
            stufe_lueftung.can_dlc = 3;
            richtung.can_dlc = 7;
            ac_on.can_dlc = 3;
            //Heating data - target values
            lueftung_an.heating_data[0] = 0x10;
            temp_links.heating_data[0] = 0xA0;
            temp_rechts.heating_data[0] = 0xA0;
            stufe_lueftung.heating_data[0] = 0x03;
            richtung.heating_data[0] = 0x0C;
            richtung.heating_data[1] = 0x00;
            richtung.heating_data[2] = 0x00;
            richtung.heating_data[3] = 0x00;
            ac_on.heating_data[0] = 0x00;
            // Auto-Mode Konfiguration
            auto_mode = AUTO_MODE_COMBINED;

            break;
        }
        case VARIANT_80_PERCENT:
        {
            //climate adjust available - main switch
            climate_adjust_available = FALSE;
            // Auto-Mode Konfiguration
            auto_mode = AUTO_MODE_NOT_AVAILABLE;
            // keine Einstellung moeglich
            
            //MUX IDs
            lueftung_an.mux_id = MUX_ID_2_ON_OFF;

             //DLC
            lueftung_an.can_dlc = 3;

            //Heating data - target values
            lueftung_an.heating_data[0] = 0x10;
            
            break;



        }
        default:
        {
            break;
        }
    }
}
/*----------------------------------------------------------------------------*/
/**
 * \brief 
 * 
 */
void old_user_setting_init(void)
{
    // Uebernahme der Werte im buffer als Ausgangswerte fuer Rueckstellroutine
    lueftung_an.init_data[0] = lueftung_an.data[0];
    if (variant == VARIANT_MQB_EVO)
    {
        if ( temp_links.data[0] >=  temp_links.data[1])
        {
            climate_control = AUTOMATIC_CLIMATE_CONTROL;
        }
        else
        {
            climate_control = MANUAL_CLIMATE_CONTROL;
        }
    }
    temp_links.init_data[0] = temp_links.data[0]; 
    temp_links.init_data[1] = temp_links.data[1]; 
    temp_rechts.init_data[0] = temp_rechts.data[0];
    stufe_lueftung.init_data[0] = stufe_lueftung.data[0];
    richtung.init_data[0] = richtung.data[0]; 
    richtung.init_data[1] = richtung.data[1]; 
    richtung.init_data[2] = richtung.data[2]; 
    richtung.init_data[3] = richtung.data[3];
    richtung_auto.init_data[0] = richtung_auto.data[0];
    ac_on.init_data[0] = ac_on.data[0];
    
    // Flag fuer erfolgte Initialisierung - bestimmt, ob Rueckstellung ausgefuehrt wird
    init_done = TRUE;

    if ( debug == TRUE )
    {
        (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_3, 0x7D3, FALSE, 8, lueftung_an.data[0], temp_links.data[0], temp_rechts.data[0], stufe_lueftung.data[0], richtung.data[0], richtung.data[1], richtung.data[2], richtung.data[3]);
    }
}

/*----------------------------------------------------------------------------*/
/**
 * \brief 
 * 
 */
void climate_setting_adjust(void)
{
    /* Timer: */
    static uint32_t ti_can_out_setting_adjust;
    uint8_t  ti_can_out_setting_adjust_elapsed;

    // Zyklustimer fuer Enstellphase
    (void)API_utilities_timer_check_timestamp(&ti_can_out_setting_adjust_elapsed, ti_can_out_setting_adjust, C_TI_CAN_OUT_SETTING_ADJUST, API_UTILITIES_TIMER_PRECISION_1MS);

    if (( state == STATE_CLIMATE_SETTING_ADJUST ) && ( ti_can_out_setting_adjust_elapsed == TRUE ))
    {
        // Alle 400ms ein check:
        switch (clima_cnt_adjust) 
        {
            // Aus- und Wiedereinschalten der Klima - bei einigen Fahrzeugen geht erst dann Geblaese an
            case 1:
            {
                if (lueftung_an.data[0] != 0x00)    
                {
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x17330100, TRUE, lueftung_an.can_dlc, 0x20, lueftung_an.mux_id, 0x00,    0,    0,    0,    0,    0);  // Ausschalten
                    API_utilities_timer_set_timestamp(&ti_can_out_setting_adjust, API_UTILITIES_TIMER_PRECISION_1MS);
                }
                break;
            }
            case 2:
            {
                if (lueftung_an.data[0] != lueftung_an.heating_data[0])
                {
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x17330100, TRUE, lueftung_an.can_dlc, 0x20, lueftung_an.mux_id, lueftung_an.heating_data[0],    0,    0,    0,    0,    0);
                    API_utilities_timer_set_timestamp(&ti_can_out_setting_adjust, API_UTILITIES_TIMER_PRECISION_1MS);
                }
                break;
            }
            case 3: // Entscheidung, ob eingestellt wird
            {
               if (climate_adjust_available == FALSE)
                {
                    // Ueberspringen der Einstellroutine
                    setting_done = TRUE; 
                    clima_cnt_adjust = 13; // Z�hler wird vorsichtshalber auch auf 13 gesetzt, weil es bei Nichteinstellung "climate_adjust_available == FALSE" zu Timing Problemem kam und die Temp_links auch noch eingestellt wurde
                    API_utilities_timer_set_timestamp(&ti_can_out_setting_adjust, API_UTILITIES_TIMER_PRECISION_1MS);
                }
                else
                {
                    // Pause fuer Wiedereinschalten der Klima
                    API_utilities_timer_set_timestamp(&ti_can_out_setting_adjust, API_UTILITIES_TIMER_PRECISION_1MS);
                }
                break;
            }
            
            case 4:
            {
                if (temp_links.data[0] != temp_links.heating_data[0])
                {
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x17330100, TRUE, temp_links.can_dlc, 0x20, temp_links.mux_id, temp_links.heating_data[0], 0, temp_links.heating_data[1],    0,    0,    0);
                    API_utilities_timer_set_timestamp(&ti_can_out_setting_adjust, API_UTILITIES_TIMER_PRECISION_1MS);
                }
                break;
            }
            case 5:
            {
                if (temp_links.data[0] != temp_links.heating_data[0])
                {
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x17330100, TRUE, temp_links.can_dlc, 0x20, temp_links.mux_id, temp_links.heating_data[0], 0, temp_links.heating_data[1],    0,    0,    0);
                    API_utilities_timer_set_timestamp(&ti_can_out_setting_adjust, API_UTILITIES_TIMER_PRECISION_1MS);
                }
                break;
            }
            case 6:
            {
                if (temp_links.data[0] != temp_links.heating_data[0])
                {
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x17330100, TRUE, temp_links.can_dlc, 0x20, temp_links.mux_id, temp_links.heating_data[0], 0, temp_links.heating_data[1],    0,    0,    0);
                    API_utilities_timer_set_timestamp(&ti_can_out_setting_adjust, API_UTILITIES_TIMER_PRECISION_1MS);
                }
                break;
            }
            case 7:
            {
                if (richtung.data[0] != richtung.heating_data[0] || 
                    richtung.data[1] != richtung.heating_data[1] || 
                    richtung.data[2] != richtung.heating_data[2])
                {
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x17330100, TRUE, richtung.can_dlc, 0x20, richtung.mux_id, richtung.heating_data[0], richtung.heating_data[1], richtung.heating_data[2], richtung.heating_data[3], 0x00,    0);
                    API_utilities_timer_set_timestamp(&ti_can_out_setting_adjust, API_UTILITIES_TIMER_PRECISION_1MS);
                }
                break;
            }
            case 8:
            {
                (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x17330100, TRUE, stufe_lueftung.can_dlc, 0x20, stufe_lueftung.mux_id, stufe_lueftung.heating_data[0],    0,    0,    0,    0,    0);
                API_utilities_timer_set_timestamp(&ti_can_out_setting_adjust, API_UTILITIES_TIMER_PRECISION_1MS);
                break;
            }
            case 9:
            {
                (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x17330100, TRUE, stufe_lueftung.can_dlc, 0x20, stufe_lueftung.mux_id, stufe_lueftung.heating_data[0],    0,    0,    0,    0,    0);
                API_utilities_timer_set_timestamp(&ti_can_out_setting_adjust, API_UTILITIES_TIMER_PRECISION_1MS);
                break;
            }
            case 10:
            {
                if (temp_rechts.data[0] != temp_rechts.heating_data[0])
                {
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x17330100, TRUE, temp_rechts.can_dlc, 0x20, temp_rechts.mux_id, temp_rechts.heating_data[0], 0x00,    0,    0,    0,    0);
                    API_utilities_timer_set_timestamp(&ti_can_out_setting_adjust, API_UTILITIES_TIMER_PRECISION_1MS);
                }
                break;
            }
            case 11:
            {
                if (temp_rechts.data[0] != temp_rechts.heating_data[0])
                {
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x17330100, TRUE, temp_rechts.can_dlc, 0x20, temp_rechts.mux_id, temp_rechts.heating_data[0], 0x00,    0,    0,    0,    0);
                    API_utilities_timer_set_timestamp(&ti_can_out_setting_adjust, API_UTILITIES_TIMER_PRECISION_1MS);
                }
                break;
            }
            case 12:
            {
                if (temp_rechts.data[0] != temp_rechts.heating_data[0])
                {
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x17330100, TRUE, temp_rechts.can_dlc, 0x20, temp_rechts.mux_id, temp_rechts.heating_data[0], 0x00,    0,    0,    0,    0);
                    API_utilities_timer_set_timestamp(&ti_can_out_setting_adjust, API_UTILITIES_TIMER_PRECISION_1MS);
                }
                break;
            }
            case 14:
            {
                setting_done = TRUE;
                break;
            }
            default:
            {
                /* case 0: just a delay; clima_cnt_adjust starts with COUNTER_RESET_ADJUST (0) */
                API_utilities_timer_set_timestamp(&ti_can_out_setting_adjust, API_UTILITIES_TIMER_PRECISION_1MS);
                break;
            }
        }

        if ( debug == TRUE )
        {
            (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_3, 0x7D4, FALSE, 8, state, 0, 0, 0, setting_done, 0, clima_cnt_adjust, 0);
        }

        clima_cnt_adjust++;
    }
}

/*----------------------------------------------------------------------------*/
/**
 * \brief Restoring settings (data arrays wit _init[]).
 * 
 */
void user_setting_restore(void)
{
    /* Timer: */
    static uint32_t ti_can_out_setting_restore;
    uint8_t ti_can_out_setting_restore_elapsed;

    // Zyklustimer fuer Rueckstellphase 200ms
    (void)API_utilities_timer_check_timestamp(&ti_can_out_setting_restore_elapsed, ti_can_out_setting_restore, C_TI_CAN_OUT_SETTING_RESTORE, API_UTILITIES_TIMER_PRECISION_1MS);

    if (( state == STATE_USER_SETTING_RESTORE ) && ( ti_can_out_setting_restore_elapsed == TRUE ))
    {
        switch (clima_cnt_restore) // clima_cnt_restore starts with COUNTER_RESET_RESTORE (1)
        {
            // jeder case prueft, ob der init-Wert schon vorliegt -> andernfalls wird versucht, den Wert zu setzen und der Timer zurueckgesetzt. 
            // Passt der Wert schon, wird nur der Counter hochgezahlt, Timer bleibt abgelaufen --> Sprung in den naechsten case
            case 1:
            {
                if ( (temp_links.data[0] != temp_links.init_data[0]) || (temp_links.data[1] != temp_links.init_data[1]) )
                {
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x17330100, TRUE, temp_links.can_dlc, 0x20, temp_links.mux_id, temp_links.init_data[0], 0x00, temp_links.init_data[1],    0,    0,    0);
                    API_utilities_timer_set_timestamp(&ti_can_out_setting_restore, API_UTILITIES_TIMER_PRECISION_1MS);
                }
                break;
            }
            case 2:
            {
                if ( (temp_links.data[0] != temp_links.init_data[0]) || (temp_links.data[1] != temp_links.init_data[1]) )
                {
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x17330100, TRUE, temp_links.can_dlc, 0x20, temp_links.mux_id, temp_links.init_data[0], 0x00, temp_links.init_data[1],    0,    0,    0);
                    API_utilities_timer_set_timestamp(&ti_can_out_setting_restore, API_UTILITIES_TIMER_PRECISION_1MS);
                }
                break;
            }
            case 3:
            {
                if ( (temp_links.data[0] != temp_links.init_data[0]) || (temp_links.data[1] != temp_links.init_data[1]) )
                {
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x17330100, TRUE, temp_links.can_dlc, 0x20, temp_links.mux_id, temp_links.init_data[0], 0x00,    0, temp_links.init_data[1],    0,    0);
                    API_utilities_timer_set_timestamp(&ti_can_out_setting_restore, API_UTILITIES_TIMER_PRECISION_1MS);
                }
                break;
            }
            case 4:
            {
                if ( stufe_lueftung.data[0] != stufe_lueftung.init_data[0] )
                {
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x17330100, TRUE, stufe_lueftung.can_dlc, 0x20, stufe_lueftung.mux_id, stufe_lueftung.init_data[0],    0,    0,    0,    0,    0);
                    API_utilities_timer_set_timestamp(&ti_can_out_setting_restore, API_UTILITIES_TIMER_PRECISION_1MS);
                }
                break;
            }
            case 5:
            {
                if (( richtung.data[0] != richtung.init_data[0] ) || 
                    ( richtung.data[1] != richtung.init_data[1] ) ||
                    ( richtung.data[2] != richtung.init_data[2] ) || 
                    ( richtung.data[3] != richtung.init_data[3] ))
                {
                    if (auto_mode == AUTO_MODE_SEPERATE)
                    {
                        (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x17330100, TRUE, richtung.can_dlc, 0x20, richtung.mux_id, richtung.init_data[0], richtung.init_data[1], 0x00, 0x00, 0x00, 0x00);
                    }
                    else if (auto_mode == AUTO_MODE_COMBINED)
                    {
                        // Auto-Modus (Byte[3]) war aus, es wird nur die Richtung vorgegeben (Byte[0] bis Byte[2])
                        if (richtung.init_data[3] == 0x00 || richtung.init_data[3] == 0x01)
                        {
                            (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x17330100, TRUE, richtung.can_dlc, 0x20, richtung.mux_id, richtung.init_data[0], richtung.init_data[1], richtung.init_data[2], 0x00, 0x00,    0);
                        }
                        // Auto-Modus (Byte[3]) war ein - es darf keine weitere Richtung gesendet werden
                        else if (richtung.init_data[3] == 0x03 || richtung.init_data[3] == 0x23)
                        {
                            (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x17330100, TRUE, richtung.can_dlc, 0x20, richtung.mux_id, 0x00, 0x00, 0x00, richtung.init_data[3], 0x00,    0); 
                        }
                    }
                    API_utilities_timer_set_timestamp(&ti_can_out_setting_restore, API_UTILITIES_TIMER_PRECISION_1MS);
                }
                break;
            }
            case 6:
            {
                if ( temp_rechts.data[0] != temp_rechts.init_data[0] )
                {
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x17330100, TRUE, temp_rechts.can_dlc, 0x20, temp_rechts.mux_id, temp_rechts.init_data[0], 0x00,    0,    0,    0,    0);
                    API_utilities_timer_set_timestamp(&ti_can_out_setting_restore, API_UTILITIES_TIMER_PRECISION_1MS);
                }
                break;
            }
            case 7:
            {
                if ( temp_rechts.data[0] != temp_rechts.init_data[0] )
                {
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x17330100, TRUE, temp_rechts.can_dlc, 0x20, temp_rechts.mux_id, temp_rechts.init_data[0], 0x00,    0,    0,    0,    0);
                    API_utilities_timer_set_timestamp(&ti_can_out_setting_restore, API_UTILITIES_TIMER_PRECISION_1MS);
                }
                break;
            }
            case 8:
            {
                if ( temp_rechts.data[0] != temp_rechts.init_data[0] ) 
                {
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x17330100, TRUE, temp_rechts.can_dlc, 0x20, temp_rechts.mux_id, temp_rechts.init_data[0], 0x00,    0,    0,    0,    0);
                    API_utilities_timer_set_timestamp(&ti_can_out_setting_restore, API_UTILITIES_TIMER_PRECISION_1MS);
                }
                break;
            }
            case 9:
            {
                if ((richtung_auto.data[0] != richtung_auto.init_data[0] ) && 
                    (auto_mode == AUTO_MODE_SEPERATE)) // nur bei MQB_evo und Audi
                {
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x17330100, TRUE, richtung_auto.can_dlc, 0x20, richtung_auto.mux_id, richtung_auto.init_data[0],    0,    0,    0,    0,    0);
                    API_utilities_timer_set_timestamp(&ti_can_out_setting_restore, API_UTILITIES_TIMER_PRECISION_1MS);
                }
                break;
            }
            case 10:
            {
                if ( (ac_on.data[0] != ac_on.init_data[0]) && (variant == VARIANT_MQB) ) 
                {
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x17330100, TRUE, ac_on.can_dlc, 0x20, ac_on.mux_id, ac_on.init_data[0],    0,    0,    0,    0,    0);
                    API_utilities_timer_set_timestamp(&ti_can_out_setting_restore, API_UTILITIES_TIMER_PRECISION_1MS);
                }
                break;
            }
            case 11:
            {
                if ( lueftung_an.data[0] != lueftung_an.init_data[0] ) 
                {
                    (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_2, 0x17330100, TRUE, lueftung_an.can_dlc, 0x20, lueftung_an.mux_id, lueftung_an.init_data[0],    0,    0,    0,    0,    0);
                    API_utilities_timer_set_timestamp(&ti_can_out_setting_restore, API_UTILITIES_TIMER_PRECISION_1MS);
                }
                break;
            }
            // Leerlaufzeit von 20 Zyklen (=4s) mit Zuendung an - wird bei Audi benoetigt, um fahrzeugseitig die neuen Werte im KSG zu speichern
            case 31:
            {
                init_done = FALSE; 
                break;
            }
            default:
            {
                API_utilities_timer_set_timestamp(&ti_can_out_setting_restore, API_UTILITIES_TIMER_PRECISION_1MS);
                break;
            }
        }
    
        if ( debug == TRUE )
        {
            (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_3, 0x7D5, FALSE, 8, state, 0, 0, 0, setting_done, 0, init_done, clima_cnt_restore);
        }

        clima_cnt_restore++;
    }
}


/*----------------------------------------------------------------------------*/
/**
* \internal
* When a CAN message is received this function will be automatically called in CAN-interrupt context!
*
* struct_api_utilities_can_frame:
*   uint8_t  extended_id --> 0 = standard 11 bit, 1 = extended 29 bit
*   uint32_t can_id;     --> CAN identifier
*   uint8_t  can_dlc;    --> CAN data length code
*   uint8_t  data[8u];   --> data of can message
*
* \endinternal
*/
void user_can_message_receive( const enum_API_SYSTEM_CAN_BUSES hw_id, const struct_api_utilities_can_frame *const ptr_can_msg )
{
    uint32_t ig_can_receive; /**< Uebernahme Kfz Daten ZAS_Kl_15 */
    uint16_t buffer; // buffer fuer Maskierung des aktuellen Fahrerprofils

    if ( hw_id == API_SHARED_CONST_CAN_BUS_1 ) // Wurde eine Nachricht auf dem CAN1 empfangen (Fahrzeugseite)
    {
         vehicle_can_active = TRUE;        // Botschaft auf FZG-CAN (CAN1)
        (void)API_utilities_timer_set_timestamp(&ti_vehicle_can_active, API_UTILITIES_TIMER_PRECISION_1MS);    // Timer fuer CAN-Empfangserkennung CAN1        
        
        if ( state != STATE_IDLE ) // Immer uebertragen wenn das CAN Relais auf ist (Relaissteuerung)
        {
            // alle Telegramme ausser Zuendung, Airbag, KlSens01, BCM vom Auto waehrend des Ansteuerns durchlassen --> andernfalls doppelt
            if (( ign_vehicle == FALSE) &&
                (( ptr_can_msg->can_id == 0x3C0 ) ||    // Klemmenstatus
                 ( ptr_can_msg->can_id == 0x040 ) ||    // Airbag_01
                 ( ptr_can_msg->can_id == 0x520 ) ||    // Airbag_02
                 ( ptr_can_msg->can_id == 0x6B0 ) ||    // Klima_Sensor_01
                 ( ptr_can_msg->can_id == 0x65A ))      // BCM_01
                )   
            {
                // nicht uebertragen
            }
            else if ((ptr_can_msg->can_id == 0x17330100) || (ptr_can_msg->can_id == 0x16A9540A)) // Klimaanforderung vom Touch- Display und Nutzerprofil
            {
                 // nicht uebertragen
            }
            else
            {
                /* Forward frame as is: */
                (void)API_utilities_can_send_message(
                    API_SHARED_CONST_CAN_BUS_2, 
                    ptr_can_msg->can_id, 
                    ptr_can_msg->extended_id, 
                    ptr_can_msg->can_dlc, 
                    ptr_can_msg->data[0u],
                    ptr_can_msg->data[1u],
                    ptr_can_msg->data[2u],
                    ptr_can_msg->data[3u],
                    ptr_can_msg->data[4u],
                    ptr_can_msg->data[5u],
                    ptr_can_msg->data[6u],
                    ptr_can_msg->data[7u]
                );
            }
        }
        
    
       
    }    
    else if ( hw_id == API_SHARED_CONST_CAN_BUS_2 )
    {
        climate_can_active = TRUE;        // Botschaft auf Klima-CAN (CAN2)
        (void)API_utilities_timer_set_timestamp(&ti_climate_can_active, API_UTILITIES_TIMER_PRECISION_1MS);    // Timer fuer CAN-Empfangserkennung CAN2

        if (( state != STATE_IDLE ) && ( vehicle_can_active == TRUE ))
        {   
            // Filter auf Netzwerkmanagement beim Ansteuern, da sonst das Fahrzeug wach gehalten wird
            if    ( ptr_can_msg->can_id == 0x1B000046 )  // in v00.05.00 entfernt: || ( status_climate_control == TRUE )
            {
                // do nothing -> Nachricht nicht uebertragen!
            }
            else
            {
                /* Forward frame as is: */
                (void)API_utilities_can_send_message(
                    API_SHARED_CONST_CAN_BUS_1,
                    ptr_can_msg->can_id,
                    ptr_can_msg->extended_id,
                    ptr_can_msg->can_dlc,
                    ptr_can_msg->data[0u],
                    ptr_can_msg->data[1u],
                    ptr_can_msg->data[2u],
                    ptr_can_msg->data[3u],
                    ptr_can_msg->data[4u],
                    ptr_can_msg->data[5u],
                    ptr_can_msg->data[6u],
                    ptr_can_msg->data[7u]
                );
            }
        } 
    }

    // Uebernahme Fahrzeugtyp und VIN aus Telegramm VIN
    if (ptr_can_msg->can_id == 0x6B4)
    {
        // MUX 0-3, Stelle 6 und 7 der VIN (=TYP) stecken in MUX 1
        if ((ptr_can_msg->data[0u] & 0x03) == 0x00)
        {
            vin[0] = ptr_can_msg->data[5u];
            vin[1] = ptr_can_msg->data[6u];
            vin[2] = ptr_can_msg->data[7u];
        }
        else if ((ptr_can_msg->data[0u] & 0x03) == 0x01)
        {
            vin[3] = ptr_can_msg->data[1u];
            vin[4] = ptr_can_msg->data[2u];
            vin[5] = ptr_can_msg->data[3u];
            vin[6] = ptr_can_msg->data[4u];
            vin[7] = ptr_can_msg->data[5u];
            vin[8] = ptr_can_msg->data[6u];
            vin[9] = ptr_can_msg->data[7u];
        }
        else if ((ptr_can_msg->data[0u] & 0x03) == 0x02)
        {
            vin[10] = ptr_can_msg->data[1u];
            vin[11] = ptr_can_msg->data[2u];
            vin[12] = ptr_can_msg->data[3u];
            vin[13] = ptr_can_msg->data[4u];
            vin[14] = ptr_can_msg->data[5u];
            vin[15] = ptr_can_msg->data[6u];
            vin[16] = ptr_can_msg->data[7u];
        }
    }

    // Staendige Uebernahme von Klimaeinstellungen. Telegramm kommt jede Sekunde mit data 0x30 oder on_change mit data 0x40.
    if (( ptr_can_msg->can_id == 0x17330110 ) && 
        (( ptr_can_msg->data[0] == 0x30 ) ||
         ( ptr_can_msg->data[0] == 0x40 )))
    {
        if (ptr_can_msg->data[1] == lueftung_an.mux_id)
        {
            lueftung_an.data[0] = ptr_can_msg->data[2]; 
        }
        else if (ptr_can_msg->data[1] == temp_links.mux_id)
        {
            temp_links.data[0] = ptr_can_msg->data[2];     // bei Klimaautomatik
            temp_links.data[1] = ptr_can_msg->data[4];     // bei Klima manuell
        }
        else if (ptr_can_msg->data[1] == stufe_lueftung.mux_id)
        {
            stufe_lueftung.data[0] = ptr_can_msg->data[2];
        }
        else if (ptr_can_msg->data[1] == richtung.mux_id)
        {
            richtung.data[0] = ptr_can_msg->data[2]; 
            richtung.data[1] = ptr_can_msg->data[3]; 
            richtung.data[2] = ptr_can_msg->data[4]; // nur bei VARIANT_MQB & Audi
            richtung.data[3] = ptr_can_msg->data[5]; // nur bei VARIANT_MQB - Automatikmodus
        }
        else if (ptr_can_msg->data[1] == temp_rechts.mux_id)
        {
            temp_rechts.data[0] = ptr_can_msg->data[2];
        }
        else if (ptr_can_msg->data[1] == richtung_auto.mux_id)
        {
            richtung_auto.data[0] = ptr_can_msg->data[2]; // nur bei VARIANT_MQB_EVO & Audi
        }
        else if (ptr_can_msg->data[1] == ac_on.mux_id)
        {
            ac_on.data[0] = ptr_can_msg->data[2]; // nur bei VARIANT_MQB_EVO & Audi
        } 
    }

    // Uebernahme Fahrzeugzuendung
    if ( ptr_can_msg->can_id == 0x3C0 )  
    {
        // Zuendung uebernehmen
        API_utilities_can_db_get_value(ZAS_Kl_15, &ig_can_receive);

        // set CAN trigger source for car state
        if ( ig_can_receive == TRUE )
        {
            ign_vehicle_buf = TRUE;    // Zuendungssignal fuer Benutzererkennung merken 
            (void)API_system_trigger_car_state_source_CAN(API_SHARED_CONST_CAN_BUS_1);
        }
    }
    // Diagnose_01 Uebernahme Kilometerstand/Uhrzeit/Datum
    else if ( ptr_can_msg->can_id == 0x6B2 )        
    {
        kilometer[0] = ptr_can_msg->data[0];  
        kilometer[1] = ptr_can_msg->data[1];
        kilometer[2] = ptr_can_msg->data[2];
        kilometer[3] = ptr_can_msg->data[3];
        kilometer[4] = ptr_can_msg->data[4];
        kilometer[5] = ptr_can_msg->data[5];
        kilometer[6] = ptr_can_msg->data[6];
        kilometer[7] = ptr_can_msg->data[7]; 
    }
    // Kombi_02 Uebernahme Kilometerstand/Standzeit/Tankinhalt
    else if ( ptr_can_msg->can_id == 0x6B7 )        
    {
        kilometer2[0] = ptr_can_msg->data[0];  
        kilometer2[1] = ptr_can_msg->data[1];
        kilometer2[2] = ptr_can_msg->data[2];
        kilometer2[3] = ptr_can_msg->data[3];
        kilometer2[4] = ptr_can_msg->data[4];
        kilometer2[5] = ptr_can_msg->data[5];
        kilometer2[6] = ptr_can_msg->data[6];
        kilometer2[7] = ptr_can_msg->data[7]; 
    }
    // Klimasensor_02 Uebernahme Aussentemeperatur ungefiltert
    else if ( ptr_can_msg->can_id == 0x5E1 )     
    {
        klsen_02_temp = ptr_can_msg->data[0];
    }
    // Motor_07 Uebernahme Ansaugtemperatur
    else if ( ptr_can_msg->can_id == 0x640 )     
    {
        motor_07_temp = ptr_can_msg->data[1];
    }
    // Fahrerprofil
    else if ( ptr_can_msg->can_id == 0x16A9540A )  
    {
        /*
        Benutzerauswahl 
            Byte 2 entspricht angemeldetem Benutzer
            0x00            - keine Benutzerauswahl moeglich, im Fahrzeug nicht verbaut
            0x10            - Gast oder keine Benutzer eingerichtet
            0x20 ... 0x70   - angemeldeter Nutzer, scheinbar willkuerlich nummeriert 
        */
        driver_profile = ptr_can_msg->data[1]; // Nutzerprofilbyte �bernehmen
        buffer = ptr_can_msg->data[1] & 0x70;
        if ( buffer > 0x10 )
        {
            driver_profiles_available = TRUE;
        }
    }
    else if ( ptr_can_msg->can_id == 0x3DC )
    {

        eng_rpm = ((ptr_can_msg->data[3] << 8) | ptr_can_msg->data[2]) / 4; // Umrechnung


    }              
}


/**
 * \brief 
 * 
 * \param real_temperature      // signed int Temperatur in �C
 * \param temperature_offset    // Offset gemaess *.dbc
 * \param temperature_factor    // um VEHICLE_TEMPERATURE_FACTOR_BASE erhoehter Faktor aus *.dbc (0,75 --> 75)
 * \return uint8_t 
 */
uint8_t user_temp_sensor_to_vehicle_temperature(const int16_t real_temperature, const uint8_t temperature_offset, const uint8_t temperature_factor)
{
    uint8_t car_temperature;

    // Offset laut *.dbc addieren, negative Werte eliminieren, um in uint8_t zu casten
    car_temperature = (uint8_t)( real_temperature + temperature_offset );

    // Wert mit Faktor laut *.dbc multiplizieren
    car_temperature = (car_temperature * VEHICLE_TEMPERATURE_FACTOR_BASE ) /  temperature_factor;  
    
    return car_temperature;
}
/*
uint16_t vehicle_type_char_to_int(const char vehicle_type_char)
{  
    uint16_t type_value;
    type_value == (int) ((unsigned char) vehicle_type_char[0] * 256 + (unsigned char) vehicle_type_char[1]))
    return type_value;
}
*/



void cold_start(void)
{

    /* Timer: */
    static uint32_t ti_ksk_second , ti_ksk_200ms;
    uint8_t ti_ksk_second_elapsed , ti_ksk_200ms_elapsed;

     
      // Zyklustimer fuer Sekundentakt
    (void)API_utilities_timer_check_timestamp(&ti_ksk_second_elapsed , ti_ksk_second , C_TI_KSK_SECOND , API_UTILITIES_TIMER_PRECISION_1MS);

     // Zyklustimer fuer 200 ms Takt
    (void)API_utilities_timer_check_timestamp(&ti_ksk_200ms_elapsed , ti_ksk_200ms , C_TI_KSK_200MS , API_UTILITIES_TIMER_PRECISION_1MS);


    
    if ( heater_state == IUG_UNIT_API_STATE_WAIT_STATE && heater_state_off_flag == FALSE && r_control_active == TRUE ) // Erkennung der Ausschaltflanke des Heizvorgangs nur bei Kaltstartregelung aktiv
    {
        heater_state_off_flag = TRUE;
        heater_state_on_flag = FALSE;
    }

     heater_water_temp = get_para_by_id(ID_COOLANT_TEMP_WAT);

    if ( heater_state == IUG_UNIT_API_STATE_HEATING && heater_state_on_flag == FALSE && r_control_active == FALSE)  // Erkennung der Einschaltflanke des Heizvorgangs
    {
        heater_state_on_flag = TRUE;
        heater_state_off_flag = FALSE;

        if ( start_temperature != TEMP_VALUE_INVALID ) // Wenn Temperatur nach Neustart schon einmal gesetzt
        {
            start_temperature = cronus_temperature;  // neue Starttemperatur setzen wenn Cronus nicht im Sleep gewesen ist
        }

        //heater_water_temp = 0x00;
        heater_water_temp_old = heater_water_temp;
        //ret_val = API_ipd_set_para_by_id(ID_COOLANT_TEMP_WAT, heater_water_temp);
        heater_water_temp_start = 0xFF;
        cold_engine_start_detect = 0xFF;
        eng_on_cnt = 0;

        (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_3,0x6D6,FALSE,8, heater_water_temp , heater_water_temp_start , cold_engine_start_detect , heater_water_temp_old ,0 ,heater_state,0,0);
        
    }
    else if ( heater_state == IUG_UNIT_API_STATE_WAIT_STATE && heater_state_on_flag == TRUE)
    {
         heater_state_on_flag = FALSE;
    }   
    
   

    if ( (heater_water_temp_old != heater_water_temp) && heater_water_temp_start == 0xFF) // Wenn HG Wassertemp empfangen und HG Starttemperatur noch nicht gesetzt �bernehme die empfangene Temperatur
	{
	    heater_water_temp_start = heater_water_temp;
        (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_3,0x6D7,FALSE,8, heater_water_temp , heater_water_temp_start , cold_engine_start_detect , heater_water_temp_old ,0 ,heater_state,0,0);
	}

   

    if ( (ign_vehicle == FALSE) && (start_temperature != TEMP_VALUE_INVALID)  && cold_engine_start_detect == 0xFF && ((heater_water_temp_start)  <= ( start_temperature + C_OFFSET_HEATER_WATER_TEMP_AND_START_AMBIENT_TEMP))) // Wenn die Wassertemperatur im Heizger�t kleiner gleich der Au�entemperatur + 10�C ist und zu diesem Zeitpunkt die Z�ndung aus ist, wird das Flag f�r Motorkaltstart gesetzt (w_temp_hg_start - 10 --> Korrektur des unterschiedlichen WBus Offset)
	{
        cold_engine_start_detect = TRUE; // Motorkaltstart erkannt

        if (debug == TRUE)
        {
            (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_3,0x7D6,FALSE,8,ign_vehicle, heater_water_temp_start , start_temperature , C_OFFSET_HEATER_WATER_TEMP_AND_START_AMBIENT_TEMP , cold_engine_start_detect ,0,0,0);
        }

    }

    if ( (r_control_active == FALSE) && (heater_state == IUG_UNIT_API_STATE_HEATING) && (cold_engine_start_detect == TRUE) && (heater_water_temp > (heater_water_temp_start + C_WATER_TEMP_INCREASE_THRESHOLD)) && (eng_on_cnt < C_TI_ENG_ON_R_CONTROL_SWITCH_OFF))
    {
        r_control_active = TRUE;

        if (debug == TRUE)
        {
            (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_3,0x7D7,FALSE,8,ign_vehicle, heater_water_temp_start , start_temperature , C_OFFSET_HEATER_WATER_TEMP_AND_START_AMBIENT_TEMP , cold_engine_start_detect ,heater_state,0,0);
        }
    }
    else if ( (r_control_active == TRUE) && ((eng_on_cnt > C_TI_ENG_ON_R_CONTROL_SWITCH_OFF) || (heater_state_off_cnt > C_TI_HEATING_OFF_R_CONTROL_SWITCH_OFF)) )
    {
        r_control_active = FALSE;
        heater_state_off_flag = FALSE;
        
        if(debug == TRUE)
        {
            (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_3,0x7D7,FALSE,8,ign_vehicle, heater_water_temp_start , start_temperature , C_OFFSET_HEATER_WATER_TEMP_AND_START_AMBIENT_TEMP , cold_engine_start_detect ,heater_state,eng_on,eng_on_cnt);
        }
    }

    if ( eng_rpm > C_ENGINE_RPM_THRESHOLD ) // Erkennung Motorlauf
    {
        eng_on = TRUE;
    }
    else
    {
        eng_on = FALSE;
    }

    if ( ti_ksk_second_elapsed == TRUE )     // Sekundenroutine f�r Zeitabl�ufe
    {
        API_utilities_timer_set_timestamp(&ti_ksk_second, API_UTILITIES_TIMER_PRECISION_1MS);

        if (eng_on == TRUE)
        {
            if (eng_on_cnt < 65535)
            {
				eng_on_cnt++;

            }
        }
               

        if ( heater_state_off_flag == TRUE)
        {
            heater_state_off_cnt++;
        }
        else
        {
            heater_state_off_cnt = 0;
        }
    
    }

    if ( ti_ksk_200ms_elapsed == TRUE )     // 200 ms Routine f�r Zeitabl�ufe
    {
        API_utilities_timer_set_timestamp(&ti_ksk_200ms, API_UTILITIES_TIMER_PRECISION_1MS);


        if (r_control_active == TRUE && vehicle_can_active == TRUE)
        {
            (void)API_utilities_can_send_message(API_SHARED_CONST_CAN_BUS_3, 0x521 , FALSE , 8 , 0x28 , 0x1E , 0x9E , 0x2C , 0x01 , 0x64 , 0x99 , 0x28 );


        }    

    }


    if (r_control_active == TRUE) // Verhinderung des Sleep Modes bei aktiver Kaltstartregelung
    {
        API_system_lock_sleep_mode();
    }
    else
    {
        // do nothing
    }
    
    

}   

