#ifndef BGM111_VERSION_H_
#define BGM111_VERSION_H_

#define     BGM111_MAJOR_V                      0       
#define     BGM111_MINOR_V                      7       
#define     BGM111_REVISION_V                   0       
#define     BGM111_BUILD_V                      1       



#define     BGM111_VERSION                      MRS_BLE_FIRMWARE_VERSION_STR   









#define     QUOTE(x) #x                                                                             
#define     BUILD_VERSION_STRING(x,y,z,b) QUOTE(x) "." QUOTE(y) "." QUOTE(z) "." QUOTE(b)           
#define     MRS_BLE_FIRMWARE_VERSION_STR "API:" BUILD_VERSION_STRING(BGM111_MAJOR_V,BGM111_MINOR_V,BGM111_REVISION_V,BGM111_BUILD_V) 



#endif 


