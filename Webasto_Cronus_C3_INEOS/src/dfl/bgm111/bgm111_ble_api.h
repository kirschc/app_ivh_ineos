#ifndef SRC_DFL_BGM111_BGM111_BLE_API_H_
#define SRC_DFL_BGM111_BGM111_BLE_API_H_


#include "bgm111_cfg.h"
#include "bgm111_protocol.h"

typedef enum
{
    BGM111_API_ERROR_NONE = 0,  
    BGM111_API_ERROR_PARAM,     
#ifdef DPL 
    BGM111_API_ERROR_BUSY,      
#endif
    BGM111_API_ERROR_MAX        
}enum_BGM111_API_ERROR_TYPE;


typedef struct
{
    struct_BT_MAC mac;          
    uint8_t addr_type;          
    uint32_t id;                
}struct_BGM111_API_STORED_BONDING_T;


typedef struct
{
    uint16_t offset;            
    uint16_t data_len;          
    uint8_t* data;              
}struct_BGM111_API_DATA_RECEIVED_T;

typedef enum
{
    BGM111_NOTIFICATION_DISABLED = 0,   
    BGM111_NOTIFICATION_ENABLED,        
    BGM111_NOTIFICATION_MAX             
}enum_BGM111_NOTIFICATION_STATE;


typedef void (*bgm111_callback_result)                  (struct_BGM111_HW_INFO_RESULT result);                  
typedef void (*bgm111_callback_boot)                    (void);                                                 
typedef void (*bgm111_callback_gecko_state)             (struct_BGM111_HW_INFO_GECKO_STATE gecko_state);        
typedef void (*bgm111_callback_mac)                     (struct_BGM111_HW_INFO_LOCAL_MAC mac);                  
typedef void (*bgm111_callback_connection_parameters)   (struct_BGM111_HW_INFO_CONNECTION_PARAMETERS connection_parameters);
typedef void (*bgm111_callback_rssi)                    (struct_BGM111_HW_INFO_RSSI rssi);                      
typedef void (*bgm111_callback_bonding)                 (struct_BGM111_SM_INFO_BONDING bonding);                
typedef void (*bgm111_callback_passkey)                 (struct_BGM111_SM_INFO_PASSKEY passkey);                
typedef void (*bgm111_callback_data_rec)                (struct_BGM111_API_DATA_RECEIVED_T data_msg);           
typedef void (*bgm111_callback_can_rec)                 (struct_BGM111_VIRTUAL_CAN_MESSAGE can_msg);            
typedef void (*bgm111_callback_data_en)                 (enum_BGM111_NOTIFICATION_STATE enabled);               
typedef void (*bgm111_callback_can_en)                  (enum_BGM111_NOTIFICATION_STATE enabled);               
typedef void (*bgm111_callback_user_en)                 (uint16_t characteristic, enum_BGM111_NOTIFICATION_STATE enabled);  
typedef void (*bgm111_callback_list_all_bondings)       (struct_BGM111_API_STORED_BONDING_T* stored_bonding_list, uint8_t count);  

typedef struct
{
        bgm111_callback_result result_callback;                                         
        bgm111_callback_boot boot_callback;                                             
        bgm111_callback_gecko_state gecko_state_callback;                               
        bgm111_callback_mac mac_callback;                                               
        bgm111_callback_connection_parameters connection_parameters_callback;           
        bgm111_callback_rssi rssi_callback;                                             
        bgm111_callback_bonding bonding_callback;                                       
        bgm111_callback_passkey passkey_callback;                                       
        bgm111_callback_data_rec data_msg_callback;                                     
        bgm111_callback_can_rec can_msg_callback;                                       
        bgm111_callback_data_en data_notification_enabled_callback;                     
        bgm111_callback_can_en  can_notification_enabled_callback;                      
        bgm111_callback_user_en user_notification_enabled_callback;						

#ifdef BGM111_ENABLE_PAIRING_STORAGE
        bgm111_callback_list_all_bondings list_bondings_callback;                       
#endif

#ifdef BGM111_ENABLE_USER_CHARACTERISTICS
    bgm111_callback_info_user_read_request user_read_request_callback;                  
    bgm111_callback_info_user_write_request user_write_request_callback;                
#endif

}struct_BGM111_API_INIT_CALLBACKS_t;



void bgm111_api_init(bgm111_sci_tx_function tx, struct_BGM111_API_INIT_CALLBACKS_t* action_callers);





enum_BGM111_API_ERROR_TYPE  bgm111_api_send_data_notification(uint16_t data_length, uint8_t* data);


enum_BGM111_API_ERROR_TYPE bgm111_api_send_can_notification(uint8_t can_bus, uint32_t can_id, uint8_t extended, uint8_t dlc, uint8_t data1, uint8_t data2, uint8_t data3, uint8_t data4, uint8_t data5, uint8_t data6, uint8_t data7, uint8_t data8 );

enum_BGM111_API_ERROR_TYPE bgm111_api_set_advertising_name_string(uint8_t len, uint8_t* name);
enum_BGM111_API_ERROR_TYPE bgm111_api_set_manufacturer_name_string(uint8_t len, uint8_t* name);

enum_BGM111_API_ERROR_TYPE bgm111_api_set_serial_number_string(uint8_t len, uint8_t* number);


enum_BGM111_API_ERROR_TYPE bgm111_api_set_product_name_string(uint8_t len, uint8_t* product_name);

enum_BGM111_API_ERROR_TYPE bgm111_api_set_hardware_revision_string(uint8_t len, uint8_t* hw_revision_string);

enum_BGM111_API_ERROR_TYPE bgm111_api_set_software_version_string(uint8_t len, uint8_t* sw_version_string);

enum_BGM111_API_ERROR_TYPE bgm111_api_clear_actual_bonding(void);

#ifdef BGM111_ENABLE_PAIRING_STORAGE
enum_BGM111_API_ERROR_TYPE bgm111_api_read_all_bondings(void);

#endif

enum_BGM111_API_ERROR_TYPE bgm111_api_read_gecko_state(void);
enum_BGM111_API_ERROR_TYPE bgm111_api_read_local_mac(void);
enum_BGM111_API_ERROR_TYPE bgm111_api_close_connection(void);


enum_BGM111_API_ERROR_TYPE bgm111_api_set_connection_parameters(uint16_t min_interval, uint16_t max_interval, uint16_t latency, uint16_t timeout);


enum_BGM111_API_ERROR_TYPE bgm111_api_set_passkey(uint32_t key);

enum_BGM111_API_ERROR_TYPE bgm111_api_bond(void);

enum_BGM111_API_ERROR_TYPE bgm111_api_set_security_config(uint8_t bonding_en, uint8_t secure_connection, uint8_t mitm_protection_en, uint8_t bonding_confirmation_en,  enum_BGM111_SM_CMD_SET_SECURITY_CONFIG_IO_CAPABILITY mode);

enum_BGM111_API_ERROR_TYPE bgm111_api_bonding_mode(enum_BGM111_SM_CMD_BONDING_MODE_BONDINGS_EN en, uint8_t count, enum_BGM111_SM_CMD_BONDING_MODE_OVERWRITE_MODE mode );

enum_BGM111_API_ERROR_TYPE bgm111_api_reject_unbonded(enum_BGM111_SM_CMD_REJECT_UNBONDED_MODE mode);


enum_BGM111_API_ERROR_TYPE bgm111_api_passkey_ack(enum_BGM111_SM_PASSKEY_STATE state, enum_BGM111_SM_CMD_PASSKEY_ACK_CONFIRMATION confirmation, uint32_t key );

enum_BGM111_API_ERROR_TYPE bgm111_api_clear_single_bonding(uint8_t id);


enum_BGM111_API_ERROR_TYPE bgm111_api_clear_all_bondings(void);



enum_BGM111_API_ERROR_TYPE bgm111_api_advertise(enum_BGM111_CL_CMD_ADVERTISE_DISCOVERABLE discoverable, enum_BGM111_CL_CMD_ADVERTISE_CONNECTABLE connectable);


enum_BGM111_API_ERROR_TYPE bgm111_api_set_adv_parameters(uint16_t min_interval, uint16_t max_interval, enum_BGM111_CL_CMD_SET_ADV_PARAMETERS_CHANNELS channel, uint16_t max_mtu);

enum_BGM111_API_ERROR_TYPE bgm111_api_set_can_value(uint16_t data_length, uint8_t* data);


enum_BGM111_API_ERROR_TYPE bgm111_api_set_user_value(uint16_t data_length, uint8_t* data);




#ifdef BGM111_ENABLE_USER_CHARACTERISTICS
enum_BGM111_API_ERROR_TYPE bgm111_api_send_user_read_response(uint16_t characteristic, enum_BGM111_CL_USER_RESPONSE_STATUS status, uint16_t data_length, uint8_t* data);

enum_BGM111_API_ERROR_TYPE bgm111_api_send_user_write_response(uint16_t characteristic, enum_BGM111_CL_USER_RESPONSE_STATUS status);


#endif


void bgm111_api_set_enhanced_characteristics_received_callback( bgm111_callback_info_characteristic_received new_callback);


void bgm111_api_set_mrs_characteristic_received_callback(bgm111_callback_data_rec new_callback);




#endif 


