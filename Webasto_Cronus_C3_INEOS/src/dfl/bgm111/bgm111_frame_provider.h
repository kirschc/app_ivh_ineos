#ifndef BGM111_PROTOCOL_H
#define BGM111_PROTOCOL_H

#include "bgm111_protocol_base.h"



typedef enum
{
    BGM111_FRAME_ERR_NONE        = 0     ,   
    BGM111_FRAME_ERR_PARAM               ,   
    BGM111_FRAME_ERR_MAX                     
}enum_BGM111_FRAME_GEN_ERROR_T;


enum_BGM111_FRAME_GEN_ERROR_T bgm111_frame_gen_cmd_reset(struct_PROTOCOL_MSG_T* tx_frame, struct_BGM111_HW_CMD_RESET reset_mode);


enum_BGM111_FRAME_GEN_ERROR_T bgm111_frame_gen_cmd_read_last_cmd(struct_PROTOCOL_MSG_T* tx_frame);

enum_BGM111_FRAME_GEN_ERROR_T bgm111_frame_gen_cmd_read_gecko_state(struct_PROTOCOL_MSG_T* tx_frame);

enum_BGM111_FRAME_GEN_ERROR_T bgm111_frame_gen_cmd_read_local_mac(struct_PROTOCOL_MSG_T* tx_frame);
enum_BGM111_FRAME_GEN_ERROR_T bgm111_frame_gen_cmd_set_tx_power_level(struct_PROTOCOL_MSG_T* tx_frame, struct_BGM111_HW_CMD_SET_TX_LVL tx_level);


enum_BGM111_FRAME_GEN_ERROR_T bgm111_frame_gen_cmd_setget_dio(struct_PROTOCOL_MSG_T* tx_frame, struct_BGM111_HW_CMD_SETGET_DIO dio_setup );


enum_BGM111_FRAME_GEN_ERROR_T bgm111_frame_gen_cmd_close_connection(struct_PROTOCOL_MSG_T* tx_frame);

enum_BGM111_FRAME_GEN_ERROR_T bgm111_frame_gen_cmd_sleep(struct_PROTOCOL_MSG_T* tx_frame);

enum_BGM111_FRAME_GEN_ERROR_T bgm111_frame_gen_cmd_set_connection_parameters(struct_PROTOCOL_MSG_T* tx_frame, struct_BGM111_HW_CMD_SET_CONNECTION_PARAMETERS con_param);

enum_BGM111_FRAME_GEN_ERROR_T bgm111_frame_gen_cmd_setup_hardware(struct_PROTOCOL_MSG_T* tx_frame, struct_BGM111_HW_CMD_SETUP_HARDWARE hw_setup );

enum_BGM111_FRAME_GEN_ERROR_T bgm111_frame_gen_cmd_testlab_dtm_tx(struct_PROTOCOL_MSG_T* tx_frame, struct_BGM111_HW_CMD_TEST_DTM_TX dtm_tx );


enum_BGM111_FRAME_GEN_ERROR_T bgm111_frame_gen_cmd_testlab_dtm_rx(struct_PROTOCOL_MSG_T* tx_frame, struct_BGM111_HW_CMD_TEST_DTM_RX dtm_rx );


enum_BGM111_FRAME_GEN_ERROR_T bgm111_frame_gen_cmd_testlab_dtm_end(struct_PROTOCOL_MSG_T* tx_frame);

enum_BGM111_FRAME_GEN_ERROR_T bgm111_frame_gen_cmd_set_passkey(struct_PROTOCOL_MSG_T* tx_frame, struct_BGM111_SM_CMD_SET_PASSKEY key);


enum_BGM111_FRAME_GEN_ERROR_T bgm111_frame_gen_cmd_increase_security(struct_PROTOCOL_MSG_T* tx_frame);


enum_BGM111_FRAME_GEN_ERROR_T bgm111_frame_gen_cmd_passkey_ack(struct_PROTOCOL_MSG_T* tx_frame, struct_BGM111_SM_CMD_PASSKEY_ACK ack );


enum_BGM111_FRAME_GEN_ERROR_T bgm111_frame_gen_cmd_set_security_config(struct_PROTOCOL_MSG_T* tx_frame, struct_BGM111_SM_CMD_SET_SECURITY_CONFIG cfg);


enum_BGM111_FRAME_GEN_ERROR_T bgm111_frame_gen_cmd_reject_unbonded(struct_PROTOCOL_MSG_T* tx_frame, struct_BGM111_SM_CMD_REJECT_UNBONDED mode);


enum_BGM111_FRAME_GEN_ERROR_T bgm111_frame_gen_cmd_clear_single_bonding(struct_PROTOCOL_MSG_T* tx_frame, struct_BGM111_SM_CMD_CLEAR_SINGLE_BONDING id);

enum_BGM111_FRAME_GEN_ERROR_T bgm111_frame_gen_cmd_clear_all_bondings(struct_PROTOCOL_MSG_T* tx_frame);


enum_BGM111_FRAME_GEN_ERROR_T bgm111_frame_gen_cmd_read_all_bondings(struct_PROTOCOL_MSG_T* tx_frame);


enum_BGM111_FRAME_GEN_ERROR_T bgm111_frame_gen_cmd_bonding_mode(struct_PROTOCOL_MSG_T* tx_frame, struct_BGM111_SM_CMD_BONDING_MODE mode);


enum_BGM111_FRAME_GEN_ERROR_T bgm111_frame_gen_cmd_advertise(struct_PROTOCOL_MSG_T* tx_frame, struct_BGM111_CL_CMD_ADVERTISE adv_setup);

enum_BGM111_FRAME_GEN_ERROR_T bgm111_frame_gen_cmd_set_adv_parameters(struct_PROTOCOL_MSG_T* tx_frame, struct_BGM111_CL_CMD_SET_ADV_PARAMETERS adv_setup);

enum_BGM111_FRAME_GEN_ERROR_T bgm111_frame_gen_cmd_set_characteristic(struct_PROTOCOL_MSG_T* tx_frame, struct_BGM111_CL_CMD_SET_CHARACTERISTIC value);

enum_BGM111_FRAME_GEN_ERROR_T bgm111_frame_gen_cmd_send_notification(struct_PROTOCOL_MSG_T* tx_frame, struct_BGM111_CL_CMD_SEND_NOTIFICATION notification );

enum_BGM111_FRAME_GEN_ERROR_T bgm111_frame_gen_cmd_send_user_read_response(struct_PROTOCOL_MSG_T* tx_frame, struct_BGM111_CL_CMD_SEND_USER_READ_RESPONSE response);

enum_BGM111_FRAME_GEN_ERROR_T bgm111_frame_gen_cmd_send_user_write_response(struct_PROTOCOL_MSG_T* tx_frame, struct_BGM111_CL_CMD_SEND_USER_WRITE_RESPONSE response );

enum_BGM111_FRAME_GEN_ERROR_T bgm111_frame_gen_cmd_read_local_characteristic(struct_PROTOCOL_MSG_T* tx_frame, struct_BGM111_CL_CMD_READ_LOCAL_CHARACTERISTIC data);






enum_BGM111_FRAME_GEN_ERROR_T bgm111_frame_prov_info_result(struct_BGM111_HW_INFO_RESULT* info_result , const struct_PROTOCOL_MSG_T* const frame );

enum_BGM111_FRAME_GEN_ERROR_T bgm111_frame_prov_info_boot(struct_BGM111_HW_INFO_BOOT* bootmode, const struct_PROTOCOL_MSG_T* const frame );


enum_BGM111_FRAME_GEN_ERROR_T bgm111_frame_prov_info_gecko_state(struct_BGM111_HW_INFO_GECKO_STATE* gecko_state, const struct_PROTOCOL_MSG_T* const frame );



enum_BGM111_FRAME_GEN_ERROR_T bgm111_frame_prov_info_local_mac(struct_BGM111_HW_INFO_LOCAL_MAC* local_mac, const struct_PROTOCOL_MSG_T* const frame );


enum_BGM111_FRAME_GEN_ERROR_T bgm111_frame_prov_info_connection_parameters(struct_BGM111_HW_INFO_CONNECTION_PARAMETERS* con_param, const struct_PROTOCOL_MSG_T* const frame );

enum_BGM111_FRAME_GEN_ERROR_T bgm111_frame_prov_info_rssi(struct_BGM111_HW_INFO_RSSI* rssi, const struct_PROTOCOL_MSG_T* const frame );

enum_BGM111_FRAME_GEN_ERROR_T bgm111_frame_prov_info_bonding(struct_BGM111_SM_INFO_BONDING* info_bonding, const struct_PROTOCOL_MSG_T* const frame );

enum_BGM111_FRAME_GEN_ERROR_T bgm111_frame_prov_info_passkey(struct_BGM111_SM_INFO_PASSKEY* info_passkey, const struct_PROTOCOL_MSG_T* const frame );

enum_BGM111_FRAME_GEN_ERROR_T bgm111_frame_prov_info_list_bonding(struct_BGM111_SM_INFO_LIST_BONDING* info_list_bonding, const struct_PROTOCOL_MSG_T* const frame );

enum_BGM111_FRAME_GEN_ERROR_T bgm111_frame_prov_info_user_read_request(struct_BGM111_CL_INFO_USER_READ_REQUEST* info_user_read_req, const struct_PROTOCOL_MSG_T* const frame );

enum_BGM111_FRAME_GEN_ERROR_T bgm111_frame_prov_info_user_write_request(struct_BGM111_CL_INFO_USER_WRITE_REQUEST* info_user_write_req, const struct_PROTOCOL_MSG_T* const frame );

enum_BGM111_FRAME_GEN_ERROR_T bgm111_frame_prov_info_characteristic_received(struct_BGM111_CL_INFO_CHARACTERISTIC_RECEIVED* info_characteristic, const struct_PROTOCOL_MSG_T* const frame );

enum_BGM111_FRAME_GEN_ERROR_T bgm111_frame_prov_info_notifications_enabled(struct_BGM111_CL_INFO_NOTIFICATIONS_ENABLED* info_notification_enabled, const struct_PROTOCOL_MSG_T* const frame );

enum_BGM111_FRAME_GEN_ERROR_T bgm111_frame_prov_virtual_can_message(struct_BGM111_VIRTUAL_CAN_MESSAGE* info_vcan, const struct_BGM111_CL_INFO_CHARACTERISTIC_RECEIVED info_datapoint );




































































































#endif


