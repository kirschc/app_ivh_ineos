#ifndef SRC_DFL_BGM111_BGM111_PROTOCOL_H_
#define SRC_DFL_BGM111_BGM111_PROTOCOL_H_


#include "bgm111_state_provider.h"
#include "bgm111_frame_provider.h"


typedef void (*bgm111_callback_info_result)                 (struct_BGM111_HW_INFO_RESULT info_result);                                     
typedef void (*bgm111_callback_info_boot)                   (struct_BGM111_HW_INFO_BOOT info_boot);                                         
typedef void (*bgm111_callback_info_gecko_state)            (struct_BGM111_HW_INFO_GECKO_STATE info_gecko_state);                           
typedef void (*bgm111_callback_info_mac)                    (struct_BGM111_HW_INFO_LOCAL_MAC info_mac);                                     
typedef void (*bgm111_callback_info_connection_parameters)  (struct_BGM111_HW_INFO_CONNECTION_PARAMETERS info_connection_parameters);       
typedef void (*bgm111_callback_info_rssi)                   (struct_BGM111_HW_INFO_RSSI info_rssi);                                         
typedef void (*bgm111_callback_info_bonding)                (struct_BGM111_SM_INFO_BONDING info_bonding);                                   
typedef void (*bgm111_callback_info_passkey)                (struct_BGM111_SM_INFO_PASSKEY info_passkey);                                   
typedef void (*bgm111_callback_info_list_bonding)           (struct_BGM111_SM_INFO_LIST_BONDING info_list_bonding);                         
typedef void (*bgm111_callback_info_user_read_request)      (struct_BGM111_CL_INFO_USER_READ_REQUEST info_user_read_request);               
typedef void (*bgm111_callback_info_user_write_request)     (struct_BGM111_CL_INFO_USER_WRITE_REQUEST info_user_write_request);             
typedef void (*bgm111_callback_info_characteristic_received)(struct_BGM111_CL_INFO_CHARACTERISTIC_RECEIVED info_characteristic_received);   
typedef void (*bgm111_callback_info_notification_enabled)   (struct_BGM111_CL_INFO_NOTIFICATIONS_ENABLED info_notification_enabled);        




typedef struct
{
    bgm111_callback_info_result info_result;                                        
    bgm111_callback_info_boot info_boot;                                            
    bgm111_callback_info_gecko_state info_gecko_state;                              
    bgm111_callback_info_mac info_mac;                                              
    bgm111_callback_info_connection_parameters info_connection_parameters;          
    bgm111_callback_info_rssi info_rssi;                                            
    bgm111_callback_info_bonding info_bonding;                                      
    bgm111_callback_info_passkey info_passkey;                                      
    bgm111_callback_info_list_bonding info_list_bonding;                            
    bgm111_callback_info_user_read_request info_user_read_request;                  
    bgm111_callback_info_user_write_request info_user_write_request;                
    bgm111_callback_info_characteristic_received info_characteristic_received;      
    bgm111_callback_info_notification_enabled info_notification_enabled;            
}struct_BGM111_PROTOCOL_CALLBACK_CONFIG_t;


typedef struct
{
    uint32_t request_timestamp;                         
    uint8_t  finished;                                  
    enum_BGM111_PROTOCOL_CMD_INFO_CODES_T cmd;          
    enum_BGM111_ERROR_STATUS_T status;                  
    uint16_t errorcode;                                 
}struct_BGM111_PROTOCOL_CMD_DONE_T;


void bgm111_prot_init(bgm111_sci_tx_function tx, struct_BGM111_PROTOCOL_CALLBACK_CONFIG_t* action_caller);


struct_BGM111_PROTOCOL_CMD_DONE_T bgm111_prot_get_action_done(void);




enum_BGM111_FRAME_GEN_ERROR_T bgm111_prot_reset(enum_BGM111_BOOT_MODE reset_mode);

enum_BGM111_FRAME_GEN_ERROR_T bgm111_prot_read_last_cmd(void);

enum_BGM111_FRAME_GEN_ERROR_T bgm111_prot_read_gecko_state(void);

enum_BGM111_FRAME_GEN_ERROR_T bgm111_prot_read_local_mac(void);

enum_BGM111_FRAME_GEN_ERROR_T bgm111_prot_set_tx_power_level(int16_t tx_level);

enum_BGM111_FRAME_GEN_ERROR_T bgm111_prot_setget_dio(enum_BGM111_HW_CMD_SETGET_DIO_MODE mode, uint8_t port, uint8_t pin, enum_BGM111_HW_CMD_SETGET_DIO_DIRECTION direction, enum_BGM111_DIO_LEVEL level );

enum_BGM111_FRAME_GEN_ERROR_T bgm111_prot_close_connection(void);

enum_BGM111_FRAME_GEN_ERROR_T bgm111_prot_sleep(void);

enum_BGM111_FRAME_GEN_ERROR_T bgm111_prot_set_connection_parameters(uint16_t min_interval, uint16_t max_interval, uint16_t latency, uint16_t timeout);

enum_BGM111_FRAME_GEN_ERROR_T bgm111_prot_setup_hardware(enum_BGM111_HW_CMD_SETUP_HARDWARE_EN_MODE en_mode, uint8_t en_port, uint8_t en_pin, enum_BGM111_HW_CMD_SETUP_HARDWARE_EXT_INH_MODE inh_mode, uint8_t inh_port, uint8_t inh_pin);


enum_BGM111_FRAME_GEN_ERROR_T bgm111_prot_testlab_dtm_tx(enum_BGM111_HW_CMD_TEST_DTM_PACKET packet_type, uint8_t length, uint8_t channel, enum_BGM111_HW_CMD_TEST_DTM_PHY phy );


enum_BGM111_FRAME_GEN_ERROR_T bgm111_prot_testlab_dtm_rx(uint8_t channel,enum_BGM111_HW_CMD_TEST_DTM_PHY phy );

enum_BGM111_FRAME_GEN_ERROR_T bgm111_prot_testlab_dtm_end(void);











enum_BGM111_FRAME_GEN_ERROR_T bgm111_prot_set_passkey(uint32_t key);


enum_BGM111_FRAME_GEN_ERROR_T bgm111_prot_increase_security(void);


enum_BGM111_FRAME_GEN_ERROR_T bgm111_prot_passkey_ack(enum_BGM111_SM_PASSKEY_STATE state, enum_BGM111_SM_CMD_PASSKEY_ACK_CONFIRMATION confirmation, uint32_t key );

enum_BGM111_FRAME_GEN_ERROR_T bgm111_prot_set_security_config(uint8_t bonding_en, uint8_t secure_connection, uint8_t mitm_protection_en, uint8_t bonding_confirmation_en,  enum_BGM111_SM_CMD_SET_SECURITY_CONFIG_IO_CAPABILITY mode);


enum_BGM111_FRAME_GEN_ERROR_T bgm111_prot_bonding_mode(enum_BGM111_SM_CMD_BONDING_MODE_BONDINGS_EN en, uint8_t count, enum_BGM111_SM_CMD_BONDING_MODE_OVERWRITE_MODE mode );

enum_BGM111_FRAME_GEN_ERROR_T bgm111_prot_clear_single_bonding(uint8_t id);



enum_BGM111_FRAME_GEN_ERROR_T bgm111_prot_reject_unbonded(enum_BGM111_SM_CMD_REJECT_UNBONDED_MODE reject);



enum_BGM111_FRAME_GEN_ERROR_T bgm111_prot_clear_all_bondings(void);


enum_BGM111_FRAME_GEN_ERROR_T bgm111_prot_read_all_bondings(void);





enum_BGM111_FRAME_GEN_ERROR_T bgm111_prot_advertise(enum_BGM111_CL_CMD_ADVERTISE_DISCOVERABLE discoverable, enum_BGM111_CL_CMD_ADVERTISE_CONNECTABLE connectable);

enum_BGM111_FRAME_GEN_ERROR_T bgm111_prot_set_adv_parameters(uint16_t min_interval, uint16_t max_interval, enum_BGM111_CL_CMD_SET_ADV_PARAMETERS_CHANNELS channel, uint16_t max_mtu);


enum_BGM111_FRAME_GEN_ERROR_T bgm111_prot_set_characteristic(uint16_t characteristic, uint16_t data_length, uint8_t* data);


enum_BGM111_FRAME_GEN_ERROR_T bgm111_prot_send_notification(uint16_t characteristic, uint16_t data_length, uint8_t* data);


enum_BGM111_FRAME_GEN_ERROR_T bgm111_prot_send_user_read_response(uint16_t characteristic, enum_BGM111_CL_USER_RESPONSE_STATUS status, uint16_t data_length, uint8_t* data);

enum_BGM111_FRAME_GEN_ERROR_T bgm111_prot_send_user_write_response(uint16_t characteristic, enum_BGM111_CL_USER_RESPONSE_STATUS status);

enum_BGM111_FRAME_GEN_ERROR_T bgm111_prot_read_local_characteristic(uint16_t characteristic, uint16_t offset);




#endif 


