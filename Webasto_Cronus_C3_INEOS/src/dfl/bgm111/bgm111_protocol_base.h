#ifndef _PROTOCOL_BASE_H_
#define _PROTOCOL_BASE_H_

#if defined __V850__ && defined __ghs__
#include "hal_data_types.h"

#else
#include <stdint.h>

#endif

#include "bgm111_version.h"         


#define BGM111_PROT_MAX_DATA_LEN                                0xFF    
#define BGM111_PROT_CMD_TIMEOUT_MS                              50u     
#define BGM111_PROT_HEADER_LENGTH                               3u      
#define BGM111_PROT_CHECKSUM_LENGTH                             4u      
#define BGM111_PROT_SYNC_BYTE                                   0x96u   
#define BGM111_PROT_SYNC_OFFSET                                 0u      
#define BGM111_PROT_CMD_OFFSET                                  1u      
#define BGM111_PROT_LEN_OFFSET                                  2u      
#define BGM111_PROT_DATA_OFFSET                                 3u      
#define BGM111_PROT_ERR_NONE                                    0u      
#define BGM111_PROT_UNBONDED_VALUE                              0xFFu   
#define BGM111_PROT_UNCONNECTED_VALUE                           0u      
#define BGM111_PROT_UNUSED_BYTES                                0xFFu   
#define BGM111_PROT_UNKNOWN_CMD_RESULT                          666u    
#define BGM111_PROT_ERROR_STATUS_UNDEF                          0xFFFFu 
#define BGM111_DEFAULT_PAIRING_KEY                              123456u 
#define BGM111_DEFAULT_MTU                                      247u    
#define BGM111_DEFAULT_TRANSMIT_POWER_DB                        104u     
#define BGM111_DEFAULT_ADV_PARAM_MIN_INTERVAL                   6u      
#define BGM111_DEFAULT_ADV_PARAM_MAX_INTERVAL                   7u      
#define BGM111_DEFAULT_ADV_PARAM_LATENCY                        0u      
#define BGM111_DEFAULT_ADV_PARAM_TIMEOUT                        100u    
#define BGM111_DEFAULT_ADV_INTERVAL                             160u    
#define BGM111_DEFAULT_BONDINGS_CNT                             8u      


#define BGM111_STAT_CON_MODE_SHIFT                              4u      
#define BGM111_STAT_CON_MODE_MASK                               0xF0u   
#define BGM111_STAT_DISC_MODE_SHIFT                             0u      
#define BGM111_STAT_DISC_MODE_MASK                              0x0Fu   
#define BGM111_STAT_GET_CON_MODE(x)                             (((x) & BGM111_STAT_CON_MODE_MASK) >> BGM111_STAT_CON_MODE_SHIFT) 
#define BGM111_STAT_GET_DISC_MODE(x)                            (((x) & BGM111_STAT_DISC_MODE_MASK) >> BGM111_STAT_DISC_MODE_SHIFT)
#define BGM111_STAT_SET_CON_DISC_MODE(con_mode,disc_mode)       (((con_mode) << (BGM111_STAT_CON_MODE_SHIFT)) | ((disc_mode)<<(BGM111_STAT_DISC_MODE_SHIFT))) 

#define BGM111_LIMIT_MIN_ADV_INTERVAL                           0x20        
#define BGM111_LIMIT_MAX_ADV_INTERVAL                           0x4000      
#define BGM111_LIMIT_MAX_ADV_LENGTH                             20u         
#define BGM111_LIMIT_PASSKEY_MAX_VAL                            999999u     
#define BGM111_LIMIT_SECURITY_CONFIG_MAX_FLAGS                  0x0fu       
#define BGM111_LIMIT_MIN_TX_LEVEL                               1u          
#define BGM111_LIMIT_MAX_TX_LEVEL                               104u        
#define BGM111_LIMIT_CON_INTERVAL_MIN                           0x06        
#define BGM111_LIMIT_CON_INTERVAL_MAX                           0x0C80      
#define BGM111_LIMIT_CON_LATENCY                                0x01F4      
#define BGM111_LIMIT_CON_TIMEOUT_MIN                            0x0A        
#define BGM111_LIMIT_CON_TIMEOUT_MAX                            0x0C80      
#define BGM111_LIMIT_MTU_MIN                                    23u         
#define BGM111_LIMIT_MTU_MAX                                    250u        
#define BGM111_LIMIT_BONDINGS_CNT                               14u         
#define BGM111_LIMIT_DTM_CHANNEL                                39u         
#define BGM111_LIMIT_CHARACTERISTIC_VALUE_LEN                   247u        
#define BGM111_DATA_LEN_MAC                                     6u          

#define BGM111_DATA_LEN_HW_INFO_RESULT                          4u          
#define BGM111_DATA_LEN_HW_CMD_RESET                            1u          
#define BGM111_DATA_LEN_HW_INFO_BOOT                            9u          
#define BGM111_DATA_LEN_HW_CMD_READ_LAST_MSG                    1u          
#define BGM111_DATA_LEN_HW_CMD_READ_GECKO_STATE                 0u          
#define BGM111_DATA_LEN_HW_INFO_GECKO_STATE                     6u          
#define BGM111_DATA_LEN_HW_CMD_READ_LOCAL_MAC                   0u          
#define BGM111_DATA_LEN_HW_INFO_LOCAL_MAC                       6u          
#define BGM111_DATA_LEN_HW_CMD_SET_TX_LVL                       2u          
#define BGM111_DATA_LEN_HW_CMD_SETGET_DIO                       5u          
#define BGM111_DATA_LEN_HW_CMD_CLOSE_CONNECTION                 0u          
#define BGM111_DATA_LEN_HW_CMD_SLEEP                            0u          
#define BGM111_DATA_LEN_HW_CMD_SET_CONNECTION_PARAMETERS        8u          
#define BGM111_DATA_LEN_HW_INFO_CONNECTION_PARAMETERS           8u          
#define BGM111_DATA_LEN_HW_CMD_SETUP_HARDWARE                   6u          
#define BGM111_DATA_LEN_HW_INFO_RSSI                            7u          
#define BGM111_DATA_LEN_HW_TEST_DTM_TX                          4u          
#define BGM111_DATA_LEN_HW_TEST_DTM_RX                          2u          
#define BGM111_DATA_LEN_HW_TEST_DTM_END                         0u          
#define BGM111_DATA_LEN_SM_CMD_SET_PASSKEY                      3u          
#define BGM111_DATA_LEN_SM_CMD_INCREASE_SECURITY                0u          
#define BGM111_DATA_LEN_SM_INFO_BONDING                         3u          
#define BGM111_DATA_LEN_SM_INFO_PASSKEY                         4u          
#define BGM111_DATA_LEN_SM_CMD_PASSKEY_ACK                      5u          
#define BGM111_DATA_LEN_SM_CMD_SET_SECURITY_CONFIG              2u          
#define BGM111_DATA_LEN_SM_CMD_REJECT_UNBONDED                  1u          
#define BGM111_DATA_LEN_SM_CMD_CLEAR_SINGLE_BONDING             1u          
#define BGM111_DATA_LEN_SM_CMD_CLEAR_ALL_BONDINGS               0u          
#define BGM111_DATA_LEN_SM_CMD_LIST_ALL_BONDINGS                0u          
#define BGM111_DATA_LEN_SM_INFO_LIST_BONDING                    9u          
#define BGM111_DATA_LEN_SM_CMD_BONDING_MODE                     3u          
#define BGM111_DATA_LEN_CL_CMD_ADVERTISE                        2u          
#define BGM111_DATA_LEN_CL_CMD_SET_ADV_PARAMETERS               7u          
#define BGM111_DATA_LEN_CL_CMD_SET_CHARACTERISTIC               2u          
#define BGM111_DATA_LEN_CL_CMD_SEND_NOTIFICATION                2u          
#define BGM111_DATA_LEN_CL_CMD_SEND_USER_READ_RESPONSE          3u          
#define BGM111_DATA_LEN_CL_INFO_USER_READ_REQUEST               5u          
#define BGM111_DATA_LEN_CL_CMD_SEND_USER_WRITE_RESPONSE         3u          
#define BGM111_DATA_LEN_CL_INFO_USER_WRITE_REQUEST              5u          
#define BGM111_DATA_LEN_CL_CMD_READ_LOCAL_CHARACTERISTIC        4u          
#define BGM111_DATA_LEN_CL_INFO_CHARACTERISTIC_RECEIVED         5u          
#define BGM111_DATA_LEN_CL_CMD_SEND_CAN_ON_BLE                  7u          
#define BGM111_DATA_LEN_CL_INFO_RECEIVED_CAN_ON_BLE             7u          
#define BGM111_DATA_LEN_CL_INFO_NOTIFICATIONS_ENABLED           5u          
#define BGM111_DATA_LEN_MA_CMD_CONNECT                          7u          
#define BGM111_DATA_LEN_MA_CMD_ENABLE_NOTIFICATIONS             3u          
#define BGM111_DATA_LEN_MA_CMD_DISCOVER_DEVICES                 2u          
#define BGM111_DATA_LEN_MA_INFO_DISCOVERED_DEVICE               10u         
#define BGM111_DATA_LEN_MA_CMD_DISCOVER_SERVICES                0u          
#define BGM111_DATA_LEN_MA_INFO_DISCOVERED_SERVICE              4u          
#define BGM111_DATA_LEN_MA_CMD_DISCOVER_CHARACTERISTICS         4u          
#define BGM111_DATA_LEN_MA_INFO_DISCOVERED_CHARACTERISTIC       3u          
#define BGM111_DATA_LEN_MA_CMD_DISCOVER_DESCRIPTORS             2u          
#define BGM111_DATA_LEN_MA_INFO_DISCOVERED_DESCRIPTOR           2u          
#define BGM111_DATA_LEN_MA_CMD_SEND_CHARACTERISTIC              3u          
#define BGM111_DATA_LEN_MA_CMD_READ_CHARACTERISTIC              2u          
#define BGM111_DATA_LEN_MA_INFO_CHARACTERISTIC_RECEIVED         5u          
#define BGM111_DATA_LEN_MA_CMD_READ_DESCRIPTOR                  2u          
#define BGM111_DATA_LEN_MA_INFO_DESCRIPTOR_RECEIVED             4u          
#define BGM111_DATA_LEN_MA_INFO_GATT_OP_COMPLETED               2u          



#define BGM111_ERROR_CODE_NONE                                  0x0000u     
#define BGM111_ERROR_CODE_FLASH_FULL                            0x0501u     
#define BGM111_ERROR_CODE_FLASH_KEY                             0x0502u     
#define BGM111_ERROR_CODE_GATT_HANDLE                           0x0101u     
#define BGM111_ERROR_CODE_GATT_BUSY                             0x0102u     
#define BGM111_ERROR_CODE_GATT_CONNECTION_TIMEOUT               0x0103u     
#define BGM111_ERROR_CODE_GATT_INV_PARAM                        0x0180u     
#define BGM111_ERROR_CODE_GATT_WRONG_STATE                      0x0181u     
#define BGM111_ERROR_CODE_GATT_MEMORY                           0x0182u     
#define BGM111_ERROR_CODE_GATT_NOT_IMPLEMENTED                  0x0183u     
#define BGM111_ERROR_CODE_GATT_INV_CMD                          0x0184u     
#define BGM111_ERROR_CODE_GATT_TIMEOUT                          0x0185u     
#define BGM111_ERROR_CODE_GATT_NOT_CONNECTED                    0x0186u     
#define BGM111_ERROR_CODE_GATT_FLOW                             0x0187u     
#define BGM111_ERROR_CODE_GATT_USER_ATTRIBUTE                   0x0188u     
#define BGM111_ERROR_CODE_GATT_LICENSCE                         0x0189u     
#define BGM111_ERROR_CODE_GATT_TOO_LONG                         0x018au     
#define BGM111_ERROR_CODE_GATT_OUT_OF_BOUNDS                    0x018bu     
#define BGM111_ERROR_CODE_GATT_UNSPECIFIED                      0x018cu     
#define BGM111_ERROR_CODE_GATT_HARDWARE                         0x018du     
#define BGM111_ERROR_CODE_GATT_BUFFER_OVERFLOW                  0x018eu     
#define BGM111_ERROR_CODE_GATT_DISCONNECTED                     0x018fu     
#define BGM111_ERROR_CODE_GATT_TOO_MANY_REQUESTS                0x0190u     
#define BGM111_ERROR_CODE_GATT_NOT_SUPPORTED                    0x0191u     
#define BGM111_ERROR_CODE_GATT_NO_BONDING                       0x0192u     
#define BGM111_ERROR_CODE_GATT_CRYPTO                           0x0193u     
#define BGM111_ERROR_CODE_GATT_DATA_CORRUPTED                   0x0194u     
#define BGM111_ERROR_CODE_SM_PASSKEY_ENTRY_FAILED               0x0301u     
#define BGM111_ERROR_CODE_SM_OOB_NOT_AVAILABLE                  0x0302u     
#define BGM111_ERROR_CODE_SM_MISSING_IO_CAPABILITIES            0x0303u     
#define BGM111_ERROR_CODE_SM_CONFIRM_VALUE_FAILED               0x0304u     
#define BGM111_ERROR_CODE_SM_PAIRING_NOT_SUPPORTED              0x0305u     
#define BGM111_ERROR_CODE_SM_ENCRYPTION_KEY_SIZE                0x0306u     
#define BGM111_ERROR_CODE_SM_CMD_NOT_SUPPORTED                  0x0307u     
#define BGM111_ERROR_CODE_SM_UNSPECIFIED                        0x0308u     
#define BGM111_ERROR_CODE_SM_REPEATED_ATTEMPTS                  0x0309u     
#define BGM111_ERROR_CODE_SM_INVALID_PARAMETERS                 0x030au     
#define BGM111_ERROR_CODE_SM_DHKEY_CHECK_FAILED                 0x030bu     
#define BGM111_ERROR_CODE_SM_NUMBERIC_COMPARESON_FAILED         0x030cu     
#define BGM111_ERROR_CODE_SM_BEDR_PAIRING_IN_PROGRESS           0x030du     
#define BGM111_ERROR_CODE_SM_TRANSPORT                          0x030eu     
#define BGM111_ERROR_CODE_BLE_CON_IDENTIFYER                    0x0202u     
#define BGM111_ERROR_CODE_BLE_PAGE_TIMEOUT                      0x0204u     
#define BGM111_ERROR_CODE_BLE_AUTHENTICATION                    0x0205u     
#define BGM111_ERROR_CODE_BLE_PIN_MISSING                       0x0206u     
#define BGM111_ERROR_CODE_BLE_MEMORY_EXCEEDED                   0x0207u     
#define BGM111_ERROR_CODE_BLE_CONNECTION_TIMEOUT                0x0208u     
#define BGM111_ERROR_CODE_BLE_CONNECTION_LIMIT                  0x0209u     
#define BGM111_ERROR_CODE_BLE_SYNCHRONUS_CON_LIMIT              0x020au     
#define BGM111_ERROR_CODE_BLE_ACL_CONNECTION_EXISTS             0x020bu     
#define BGM111_ERROR_CODE_BLE_CMD_DISALLOWED                    0x020cu     
#define BGM111_ERROR_CODE_BLE_RESSOURCES                        0x020du     
#define BGM111_ERROR_CODE_BLE_SECURITY                          0x020eu     
#define BGM111_ERROR_CODE_BLE_ADDR                              0x020fu     
#define BGM111_ERROR_CODE_BLE_TIMEOUT_EXCEEDED                  0x0210u     
#define BGM111_ERROR_CODE_BLE_FEATURE_PARAMETER                 0x0211u     
#define BGM111_ERROR_CODE_BLE_PARAMETER                         0x0212u     
#define BGM111_ERROR_CODE_BLE_REMOTE_USER_TERMINATED            0x0213u     
#define BGM111_ERROR_CODE_BLE_REMOTE_DEVICE_RESOURCE            0x0214u     
#define BGM111_ERROR_CODE_BLE_REMOTE_POWER_OFF                  0x0215u     
#define BGM111_ERROR_CODE_BLE_LOCAL_HOST                        0x0216u     
#define BGM111_ERROR_CODE_BLE_REPEATED_ATTEMPTS                 0x0217u     
#define BGM111_ERROR_CODE_BLE_PAIRING_NOT_ALLOWED               0x0218u     
#define BGM111_ERROR_CODE_BLE_UNKNOWN_LMP                       0x0219u     
#define BGM111_ERROR_CODE_BLE_UNSUPPORTED_FEATURE               0x021au     
#define BGM111_ERROR_CODE_BLE_SCO_OFFSET_REJECTED               0x021bu     
#define BGM111_ERROR_CODE_BLE_SCO_INTERVAL_REJECTED             0x021cu     
#define BGM111_ERROR_CODE_BLE_SCO_AIRMODE_REJECTED              0x021du     
#define BGM111_ERROR_CODE_BLE_INV_LMP_PARAM                     0x021eu     
#define BGM111_ERROR_CODE_BLE_UNKNOWN                           0x021fu     
#define BGM111_ERROR_CODE_BLE_INV_LMP_PARAM_VAL                 0x0220u     
#define BGM111_ERROR_CODE_BLE_ROLE_CHANGE_NOT_ALLOWED           0x0221u     
#define BGM111_ERROR_CODE_BLE_LL_RESPONSE_TIMEOUT               0x0222u     
#define BGM111_ERROR_CODE_BLE_LMP_ERR_TRANSACTION_COLLISION     0x0223u     
#define BGM111_ERROR_CODE_BLE_LMP_PDU_NOT_ALLOWED               0x0224u     
#define BGM111_ERROR_CODE_BLE_ENCRYPTION_NOT_POSSIBLE           0x0225u     
#define BGM111_ERROR_CODE_BLE_LINK_KEY_CAHNGE_FAILURE           0x0226u     
#define BGM111_ERROR_CODE_BLE_QOS_NOT_SUPPORTED                 0x0227u     
#define BGM111_ERROR_CODE_BLE_INSTANT_PASSED                    0x0228u     
#define BGM111_ERROR_CODE_BLE_UNIT_KEY                          0x0229u     
#define BGM111_ERROR_CODE_BLE_TRANSACTION_COLLISION             0x022au     
#define BGM111_ERROR_CODE_BLE_QOS_PARAM                         0x022cu     
#define BGM111_ERROR_CODE_BLE_QOS_REJECTED                      0x022du     
#define BGM111_ERROR_CODE_BLE_CHANNEL_ASSESMENT                 0x022eu     
#define BGM111_ERROR_CODE_BLE_INSUFFICIENT_SECURITY             0x022fu     
#define BGM111_ERROR_CODE_BLE_PARAM_RANGE                       0x0230u     
#define BGM111_ERROR_CODE_BLE_ROLE_SWITCH_PENDING               0x0232u     
#define BGM111_ERROR_CODE_BLE_SLOT_VIOLATION                    0x0234u     
#define BGM111_ERROR_CODE_BLE_ROLE_SWITCH_FAILED                0x0235u     
#define BGM111_ERROR_CODE_BLE_INQUIRY_RESP_SIZE                 0x0236u     
#define BGM111_ERROR_CODE_BLE_SIMPLE_PAIRING_NOT_SUPPORTED_BY_HOST 0x0237u  
#define BGM111_ERROR_CODE_BLE_HOST_BUSY                         0x0238u     
#define BGM111_ERROR_CODE_BLE_CHANNEL_REJECT                    0x0239u     
#define BGM111_ERROR_CODE_BLE_CTRL_BUSY                         0x023au     
#define BGM111_ERROR_CODE_BLE_INVALID_CONNECTION_INTERVAL       0x023bu     
#define BGM111_ERROR_CODE_BLE_ADV_TIMEOUT                       0x023cu     
#define BGM111_ERROR_CODE_BLE_MIC_FAIL                          0x023du     
#define BGM111_ERROR_CODE_BLE_LL_INIT_CONECTION_FAILED          0x023eu     
#define BGM111_ERROR_CODE_BLE_MAC_ERR                           0x023fu     
#define BGM111_ERROR_CODE_BLE_CLOCK_ERR                         0x0240u     
#define BGM111_ERROR_CODE_APPL_FILE                             0x0a01u     
#define BGM111_ERROR_CODE_APPL_XML                              0x0a02u     
#define BGM111_ERROR_CODE_APPL_DEVICE_CONNECTION                0x0a03u     
#define BGM111_ERROR_CODE_APPL_DEVICE_COMMUNICATION             0x0a04u     
#define BGM111_ERROR_CODE_APPL_AUTHENTICATION                   0x0a05u     
#define BGM111_ERROR_CODE_APPL_GATT_DATABASE                    0x0a06u     
#define BGM111_ERROR_CODE_APPL_DISCONNECT_PROCEDURE_COLLISION   0x0a07u     
#define BGM111_ERROR_CODE_APPL_DISCONNECT_SECURITY              0x0a08u     
#define BGM111_ERROR_CODE_APPL_ENCRYPTION                       0x0a09u     
#define BGM111_ERROR_CODE_APPL_MAX_RETRIES                      0x0a0au     
#define BGM111_ERROR_CODE_APPL_DATA_PARSE                       0x0a0bu     
#define BGM111_ERROR_CODE_APPL_PAIRING_REMOVED                  0x0a0cu     
#define BGM111_ERROR_CODE_ATT_HANDLE                            0x0401u     
#define BGM111_ERROR_CODE_ATT_READ_DISALLOWED                   0x0402u     
#define BGM111_ERROR_CODE_ATT_WRITE_DISALLOWED                  0x0403u     
#define BGM111_ERROR_CODE_ATT_PDU                               0x0404u     
#define BGM111_ERROR_CODE_ATT_AUTHENTICATION                    0x0405u     
#define BGM111_ERROR_CODE_ATT_REQUEST_NOT_SUPPORTED             0x0406u     
#define BGM111_ERROR_CODE_ATT_INV_OFFSET                        0x0407u     
#define BGM111_ERROR_CODE_ATT_AUTHORIZATION                     0x0408u     
#define BGM111_ERROR_CODE_ATT_QUEUE_FULL                        0x0409u     
#define BGM111_ERROR_CODE_ATT_ATT_NOT_FOUND                     0x040au     
#define BGM111_ERROR_CODE_ATT_ATT_NOT_LONG                      0x040bu     
#define BGM111_ERROR_CODE_ATT_ENC_KEY_SIZE                      0x040cu     
#define BGM111_ERROR_CODE_ATT_ATT_LENGTH                        0x040du     
#define BGM111_ERROR_CODE_ATT_UNLIKELY_ERROR                    0x040eu     
#define BGM111_ERROR_CODE_ATT_ENCRYPTION                        0x040fu     
#define BGM111_ERROR_CODE_ATT_GROUP_TYPE                        0x0410u     
#define BGM111_ERROR_CODE_ATT_RESSOURCES                        0x0411u     
#define BGM111_ERROR_CODE_ATT_APPLICATION                       0x0480u     
#define BGM111_ERROR_CODE_MESH_EXISTS                           0x0c01u     
#define BGM111_ERROR_CODE_MESH_NOT_EXIST                        0x0c02u     
#define BGM111_ERROR_CODE_MESH_LIMIT                            0x0c03u     
#define BGM111_ERROR_CODE_MESH_ADDRESS                          0x0c04u     
#define BGM111_ERROR_CODE_MESH_DATA                             0x0c05u     
#define BGM111_ERROR_CODE_FILE_NOT_FOUND                        0x0901u     
#define BGM111_ERROR_CODE_SEC_SIGNATURE_VERIFICATION            0x0b01u     
#define BGM111_ERROR_CODE_SEC_FILE_SIGNATURE                    0x0b02u     
#define BGM111_ERROR_CODE_SEC_CHECKSUM                          0x0b03u     





typedef enum
{
BGM111_HW_INFO_RESULT                        =0 ,                           

BGM111_HW_CMD_RESET                          =2 ,                           
BGM111_HW_INFO_BOOT                             ,                           

BGM111_HW_CMD_READ_LAST_MSG                  =4 ,                           

BGM111_HW_CMD_READ_GECKO_STATE               =6 ,                           
BGM111_HW_INFO_GECKO_STATE                      ,                           

BGM111_HW_CMD_READ_LOCAL_MAC                 =8 ,                           
BGM111_HW_INFO_LOCAL_MAC                        ,                           

BGM111_HW_CMD_SET_TX_LVL                    =10 ,                           

BGM111_HW_CMD_SETGET_DIO                    =12 ,                           

BGM111_HW_CMD_CLOSE_CONNECTION              =14 ,                           

BGM111_HW_CMD_SLEEP                         =16 ,                           

BGM111_HW_CMD_SET_CONNECTION_PARAMETERS     =18 ,                           
BGM111_HW_INFO_CONNECTION_PARAMETERS            ,                           

BGM111_HW_CMD_SETUP_HARDWARE                =20 ,                           

BGM111_HW_INFO_RSSI                         =23 ,                           

BGM111_HW_TEST_DTM_TX                       =24,                            

BGM111_HW_TEST_DTM_RX                       =26,                            

BGM111_HW_TEST_DTM_END                      =28,                            

BGM111_SM_CMD_SET_PASSKEY                   =60 ,                           

BGM111_SM_CMD_INCREASE_SECURITY             =62 ,                           
BGM111_SM_INFO_BONDING                          ,                           

BGM111_SM_CMD_PASSKEY_ACK                   =64 ,                           
BGM111_SM_INFO_PASSKEY                          ,                           

BGM111_SM_CMD_SET_SECURITY_CONFIG           =66 ,                           

BGM111_SM_CMD_REJECT_UNBONDED               =68 ,                           

BGM111_SM_CMD_CLEAR_SINGLE_BONDING          =70 ,                           

BGM111_SM_CMD_CLEAR_ALL_BONDINGS            =72 ,                           

BGM111_SM_CMD_LIST_ALL_BONDINGS             =74 ,                           
BGM111_SM_INFO_LIST_BONDING                     ,                           

BGM111_SM_CMD_BONDING_MODE                  =76,                            

BGM111_CL_CMD_ADVERTISE                    =120 ,                           

BGM111_CL_CMD_SET_ADV_PARAMETERS           =122 ,                           

BGM111_CL_CMD_SET_CHARACTERISTIC           =124 ,                           

BGM111_CL_CMD_SEND_NOTIFICATION            =126 ,                           

BGM111_CL_CMD_SEND_USER_READ_RESPONSE      =128 ,                           
BGM111_CL_INFO_USER_READ_REQUEST                ,                           

BGM111_CL_CMD_SEND_USER_WRITE_RESPONSE     =130 ,                           
BGM111_CL_INFO_USER_WRITE_REQUEST               ,                           

BGM111_CL_CMD_READ_LOCAL_CHARACTERISTIC    =132 ,                           
BGM111_CL_INFO_CHARACTERISTIC_RECEIVED          ,                           

BGM111_CL_CMD_SEND_CAN_ON_BLE              =134 ,                           
BGM111_CL_INFO_RECEIVED_CAN_ON_BLE              ,                           

BGM111_CL_INFO_NOTIFICATIONS_ENABLED       =137 ,                           


BGM111_MA_CMD_CONNECT                      =180 ,                           

BGM111_MA_CMD_ENABLE_NOTIFICATIONS         =182 ,                           

BGM111_MA_CMD_DISCOVER_DEVICES             =184 ,                           
BGM111_MA_INFO_DISCOVERED_DEVICE                ,                           

BGM111_MA_CMD_DISCOVER_SERVICES            =186 ,                           
BGM111_MA_INFO_DISCOVERED_SERVICE               ,                           

BGM111_MA_CMD_DISCOVER_CHARACTERISTICS     =188 ,                           
BGM111_MA_INFO_DISCOVERED_CHARACTERISTIC        ,                           

BGM111_MA_CMD_DISCOVER_DESCRIPTORS         =190 ,                           
BGM111_MA_INFO_DISCOVERED_DESCRIPTOR            ,                           

BGM111_MA_CMD_SEND_CHARACTERISTIC          =192 ,                           

BGM111_MA_CMD_READ_CHARACTERISTIC          =194 ,                           
BGM111_MA_INFO_CHARACTERISTIC_RECEIVED          ,                           

BGM111_MA_CMD_READ_DESCRIPTOR              =196 ,                           
BGM111_MA_INFO_DESCRIPTOR_RECEIVED              ,                           

BGM111_MA_INFO_GATT_OP_COMPLETED           =199 ,                           

BGM111_CMD_INFO_MAX                                                         
}enum_BGM111_PROTOCOL_CMD_INFO_CODES_T;




typedef struct
{
    uint8_t sync;                                                               
    enum_BGM111_PROTOCOL_CMD_INFO_CODES_T cmd;                                  
    uint8_t len;                                                                
    uint8_t data[BGM111_PROT_MAX_DATA_LEN];                                     
    uint8_t checksum[BGM111_PROT_CHECKSUM_LENGTH];                              
}struct_PROTOCOL_MSG_T;



typedef enum
{
BGM111_ERR_STAT_NONE                    = 0,                                    
BGM111_ERR_STAT_SCI                        ,                                    
BGM111_ERR_STAT_FIFO                       ,                                    
BGM111_ERR_STAT_ASYNC                      ,                                    
BGM111_ERR_STAT_TIMEOUT                    ,                                    
BGM111_ERR_STAT_CHECKSUM                   ,                                    
BGM111_ERR_STAT_PARAM                      ,                                    
BGM111_ERR_STAT_MODE                       ,                                    
BGM111_ERR_STAT_MAX                                                             
}enum_BGM111_ERROR_STATUS_T;








typedef struct
{
    enum_BGM111_PROTOCOL_CMD_INFO_CODES_T cmd;                                  
    enum_BGM111_ERROR_STATUS_T status;                                          
    uint16_t errorcode;                                                         
}struct_BGM111_HW_INFO_RESULT;



typedef enum
{
BGM111_BOOT_NORMAL = 0,                                                         
BGM111_BOOT_UART_DFU,                                                           
BGM111_BOOT_OTA_DFU,                                                            
BGM111_BOOT_MAX                                                                 
}enum_BGM111_BOOT_MODE;


typedef struct
{
    enum_BGM111_BOOT_MODE mode;                                                 
}struct_BGM111_HW_CMD_RESET;


typedef struct
{
    uint8_t major;                                                              
    uint8_t minor;                                                              
    uint8_t revision;                                                           
    uint8_t build;                                                              
}struct_VERSION_INFORMATION;
typedef struct
{
    enum_BGM111_BOOT_MODE mode;                                                 
    struct_VERSION_INFORMATION mrs_ble_api_version;                             
    struct_VERSION_INFORMATION gecko_firmware_version;                          
}struct_BGM111_HW_INFO_BOOT;


typedef struct
{
    uint8_t reserved;                             
}struct_BGM111_HW_CMD_READ_LAST_MSG;



typedef enum
{
BGM111_CL_CMD_ADVERTISE_NON_DISCOVERABLE = 0,                                   
BGM111_CL_CMD_ADVERTISE_LIMITED_DISCOVERABLE,                                   
BGM111_CL_CMD_ADVERTISE_DISCOVERABLE,                                           
BGM111_CL_CMD_ADVERTISE_BROADCAST,                                              
BGM111_CL_CMD_ADVERTISE_USER_DATA,                                              
BGM111_CL_CMD_ADVERTISE_DISCOVERABLE_MAX                                        
}enum_BGM111_CL_CMD_ADVERTISE_DISCOVERABLE;

typedef enum
{
BGM111_CL_CMD_ADVERTISE_NON_CONNECTABLE = 0,                                    
BGM111_CL_CMD_ADVERTISE_DIRECT_RESERVED,                                        
BGM111_CL_CMD_ADVERTISE_CONNECTABLE,                                            
BGM111_CL_CMD_ADVERTISE_SCANABLE_NOT_CONNECTABLE,                               
BGM111_CL_CMD_ADVERTISE_CONNECTABLE_MAX                                         
}enum_BGM111_CL_CMD_ADVERTISE_CONNECTABLE;


typedef struct
{
    uint8_t act_connection;                                                     
    enum_BGM111_CL_CMD_ADVERTISE_CONNECTABLE act_connectable_mode;              
    enum_BGM111_CL_CMD_ADVERTISE_DISCOVERABLE act_discoverable_mode;            
    uint8_t act_bonding;                                                        
    uint16_t act_mtu;                                                           
    uint8_t act_tx_lvl;                                                         
    uint8_t remote_mac[BGM111_DATA_LEN_MAC];                                    
}struct_BGM111_HW_INFO_GECKO_STATE;

typedef struct
{
    uint8_t bytes[BGM111_DATA_LEN_MAC];                                         
}struct_BT_MAC;

typedef struct
{
    struct_BT_MAC mac;                                                          
}struct_BGM111_HW_INFO_LOCAL_MAC;


typedef struct
{
    int16_t tx_lvl;                                                             
}struct_BGM111_HW_CMD_SET_TX_LVL;


typedef enum
{
BGM111_HW_CMD_SETGET_DIO_MODE_WRITE = 0,                                       
BGM111_HW_CMD_SETGET_DIO_MODE_READ,                                            
BGM111_HW_CMD_SETGET_DIO_MODE_MAX                                              
}enum_BGM111_HW_CMD_SETGET_DIO_MODE;

typedef enum
{
BGM111_HW_CMD_SETGET_DIO_DIRECTION_INPUT = 0,                                   
BGM111_HW_CMD_SETGET_DIO_DIRECTION_OUTPUT,                                      
BGM111_HW_CMD_SETGET_DIO_DIRECTION_MAX                                          
}enum_BGM111_HW_CMD_SETGET_DIO_DIRECTION;

typedef enum
{
BGM111_DIO_LEVEL_LOW = 0,                                                       
BGM111_DIO_LEVEL_HIGH,                                                          
BGM111_DIO_LEVEL_MAX                                                            
}enum_BGM111_DIO_LEVEL;

typedef struct
{
    enum_BGM111_HW_CMD_SETGET_DIO_MODE mode;                                    
    uint8_t port;                                                               
    uint8_t pin;                                                                
    enum_BGM111_HW_CMD_SETGET_DIO_DIRECTION direction;                          
    enum_BGM111_DIO_LEVEL level;                                                
}struct_BGM111_HW_CMD_SETGET_DIO;

typedef enum
{
BGM111_HW_INFO_CONNECTION_PARAMETERS_STATE_DISCONNECTED = 0,                    
BGM111_HW_INFO_CONNECTION_PARAMETERS_STATE_CONNECTED,                           
BGM111_HW_INFO_CONNECTION_PARAMETERS_STATE_UPDATED,                             
BGM111_HW_INFO_CONNECTION_PARAMETERS_STATE_MAX                                  
}enum_BGM111_HW_INFO_CONNECTION_PARAMETERS_STATE;


typedef enum
{
BGM111_HW_INFO_CONNECTION_PARAMETERS_SECURITY_NONE = 0,                         
BGM111_HW_INFO_CONNECTION_PARAMETERS_SECURITY_UNAUTHENTICATED,                  
BGM111_HW_INFO_CONNECTION_PARAMETERS_SECURITY_AUTHENTICATED,                    
BGM111_HW_INFO_CONNECTION_PARAMETERS_SECURITY_AUTHENTICATED_128_BIT,            
BGM111_HW_INFO_CONNECTION_PARAMETERS_SECURITY_MAX                               
}enum_BGM111_HW_INFO_CONNECTION_PARAMETERS_SECURITY;



typedef struct
{
    uint16_t min_interval;                                                      
    uint16_t max_interval;                                                      
    uint16_t timeout;                                                           
    uint16_t latency;                                                           
}struct_BGM111_HW_CMD_SET_CONNECTION_PARAMETERS;



typedef struct
{
    enum_BGM111_HW_INFO_CONNECTION_PARAMETERS_STATE state;                      
    uint16_t close_reason;                                                      
    uint16_t interval;                                                          
    uint16_t latency;                                                           
    uint16_t timeout;                                                           
    uint8_t bonding_value;                                                      
    enum_BGM111_HW_INFO_CONNECTION_PARAMETERS_SECURITY security_mode;           
    uint8_t ble_mac[BGM111_DATA_LEN_MAC];                                       
}struct_BGM111_HW_INFO_CONNECTION_PARAMETERS;


typedef enum
{
    BGM111_HW_CMD_SETUP_HARDWARE_EN_MODE_DEACTIVATED = 0,                       
    BGM111_HW_CMD_SETUP_HARDWARE_EN_MODE_ACTIVATED,                             
    BGM111_HW_CMD_SETUP_HARDWARE_EN_MODE_MAX                                    
}enum_BGM111_HW_CMD_SETUP_HARDWARE_EN_MODE;


typedef enum
{
BGM111_HW_CMD_SETUP_HARDWARE_EXT_INH_MODE_DEACTIVATED = 0,                      
BGM111_HW_CMD_SETUP_HARDWARE_EXT_INH_MODE_ACTIVATED,                            
BGM111_HW_CMD_SETUP_HARDWARE_EXT_INH_MODE_MAX                                   
}enum_BGM111_HW_CMD_SETUP_HARDWARE_EXT_INH_MODE;



typedef struct
{
    enum_BGM111_HW_CMD_SETUP_HARDWARE_EN_MODE en_mode;                          
    uint8_t en_port;                                                            
    uint8_t en_pin;                                                             
    enum_BGM111_HW_CMD_SETUP_HARDWARE_EXT_INH_MODE ext_inh_mode;                
    uint8_t ext_inh_port;                                                       
    uint8_t ext_inh_pin;                                                        
}struct_BGM111_HW_CMD_SETUP_HARDWARE;


typedef enum
{
BGM111_HW_CMD_TEST_DTM_PHY_MIN =0,                                              
BGM111_HW_CMD_TEST_DTM_PHY_1M,                                                  
BGM111_HW_CMD_TEST_DTM_PHY_2M,                                                  
BGM111_HW_CMD_TEST_DTM_PHY_125K,                                                
BGM111_HW_CMD_TEST_DTM_PHY_500K,                                                
BGM111_HW_CMD_TEST_DTM_PHY_MAX                                                  
}enum_BGM111_HW_CMD_TEST_DTM_PHY;

typedef enum
{
BGM111_HW_CMD_TEST_DTM_PACKET_PRBS9 = 0,                                        
BGM111_HW_CMD_TEST_DTM_PACKET_11110000,                                         
BGM111_HW_CMD_TEST_DTM_PACKET_10101010,                                         
BGM111_HW_CMD_TEST_DTM_PACKET_UNMODULATED_DEPRECATED,                           
BGM111_HW_CMD_TEST_DTM_PACKET_PN9 = 253,                                        
BGM111_HW_CMD_TEST_DTM_PACKET_UNMODULATED = 254,                                
BGM111_HW_CMD_TEST_DTM_PACKET_MAX                                               
}enum_BGM111_HW_CMD_TEST_DTM_PACKET;

typedef struct
{
    enum_BGM111_HW_CMD_TEST_DTM_PACKET packet_type;                             
    uint8_t length;                                                             
    uint8_t channel;                                                            
    enum_BGM111_HW_CMD_TEST_DTM_PHY phy;                                        
}struct_BGM111_HW_CMD_TEST_DTM_TX;


typedef struct
{
    uint8_t channel;                                                            
    enum_BGM111_HW_CMD_TEST_DTM_PHY phy;                                        
}struct_BGM111_HW_CMD_TEST_DTM_RX;



typedef struct
{
    int8_t rssi;                                                                
    uint8_t remote_mac[BGM111_DATA_LEN_MAC];                                    
}struct_BGM111_HW_INFO_RSSI;


typedef struct
{
    int32_t key;                                                                
}struct_BGM111_SM_CMD_SET_PASSKEY;


typedef enum
{
BGM111_SM_INFO_BONDING_STATE_SUCCEEDED = 0,                                     
BGM111_SM_INFO_BONDING_STATE_FAILED,                                            
BGM111_SM_INFO_BONDING_STATE_MAX                                                
}enum_BGM111_SM_INFO_BONDING_STATE;


typedef struct
{
    enum_BGM111_SM_INFO_BONDING_STATE bond_state;                               
    uint16_t bond_fail_reason;                                                  
}struct_BGM111_SM_INFO_BONDING;



typedef enum
{
BGM111_SM_PASSKEY_STATE_NOACK                  = 0 ,                            
BGM111_SM_PASSKEY_STATE_REQUIRES_CONFIRMATION      ,                            
BGM111_SM_PASSKEY_STATE_REQUIRES_PASSCODE          ,                            
BGM111_SM_PASSKEY_STATE_UNSURE                     ,                            
BGM111_SM_PASSKEY_STATE_MAX
}enum_BGM111_SM_PASSKEY_STATE;

typedef enum
{
BGM111_SM_CMD_PASSKEY_ACK_CONFIRMATION_DECLINE    = 0,                          
BGM111_SM_CMD_PASSKEY_ACK_CONFIRMATION_CONFIRM,                                 
BGM111_SM_CMD_PASSKEY_ACK_CONFIRMATION_MAX                                      
}enum_BGM111_SM_CMD_PASSKEY_ACK_CONFIRMATION;


typedef struct
{
    enum_BGM111_SM_PASSKEY_STATE mode;                                          
    uint32_t key;                                                               
}struct_BGM111_SM_INFO_PASSKEY;

typedef struct
{
    enum_BGM111_SM_PASSKEY_STATE  passkey_state;                                
    enum_BGM111_SM_CMD_PASSKEY_ACK_CONFIRMATION confirmation;                   
    int32_t passkey;                                                            

}struct_BGM111_SM_CMD_PASSKEY_ACK;




#define BGM111_SM_CMD_SET_SECURITY_CONFIG_FLAGS_MITM_PROTECTION_SHIFT       0u  
#define BGM111_SM_CMD_SET_SECURITY_CONFIG_FLAGS_BONDING_EN_SHIFT            1u  
#define BGM111_SM_CMD_SET_SECURITY_CONFIG_FLAGS_SECURED_ONLY_SHIFT          2u  
#define BGM111_SM_CMD_SET_SECURITY_CONFIG_FLAGS_BONDING_CONFIRMATION_SHIFT  3u  
typedef enum
{
    BGM111_SM_CMD_SET_SECURITY_CONFIG_FLAGS_MITM_PROTECTION = 0x01,             
    BGM111_SM_CMD_SET_SECURITY_CONFIG_FLAGS_BONDING_EN  = 0x02,                 
    BGM111_SM_CMD_SET_SECURITY_CONFIG_FLAGS_SECURED_ONLY = 0x04,                
    BGM111_SM_CMD_SET_SECURITY_CONFIG_FLAGS_BONDING_CONFIRMATION = 0x08,        
    BGM111_SM_CMD_SET_SECURITY_CONFIG_FLAGS_MAX
}enum_BGM111_SM_CMD_SET_SECURITY_CONFIG_FLAGS;


typedef enum
{
BGM111_SM_CMD_SET_SECURITY_CONFIG_IO_CAPABILITY_DISPLAY_ONLY = 0,               
BGM111_SM_CMD_SET_SECURITY_CONFIG_IO_CAPABILITY_DISPLAY_YES_NO,                 
BGM111_SM_CMD_SET_SECURITY_CONFIG_IO_CAPABILITY_KEYBOARD_ONLY,                  
BGM111_SM_CMD_SET_SECURITY_CONFIG_IO_CAPABILITY_NO_INPUT_NO_OUTPUT,             
BGM111_SM_CMD_SET_SECURITY_CONFIG_IO_CAPABILITY_KEYBOARD_DISPLAY,               
BGM111_SM_CMD_SET_SECURITY_CONFIG_IO_CAPABILITY_MAX                             
}enum_BGM111_SM_CMD_SET_SECURITY_CONFIG_IO_CAPABILITY;


typedef struct
{
    enum_BGM111_SM_CMD_SET_SECURITY_CONFIG_IO_CAPABILITY io_capability;         
    uint8_t mitm_protection_en;                                                 
    uint8_t bonding_en;                                                         
    uint8_t secure_connection;                                                  
    uint8_t bonding_confirmation_en;                                            
}struct_BGM111_SM_CMD_SET_SECURITY_CONFIG;


typedef enum
{
BGM111_SM_CMD_REJECT_UNBONDED_MODE_ALLOW_UNBONDED_DEVICES = 0,                  
BGM111_SM_CMD_REJECT_UNBONDED_MODE_REJECT_UNBONDED_DEVICES,                     
BGM111_SM_CMD_REJECT_UNBONDED_MODE_MAX                                          
}enum_BGM111_SM_CMD_REJECT_UNBONDED_MODE;


typedef struct
{
    enum_BGM111_SM_CMD_REJECT_UNBONDED_MODE mode;                               
}struct_BGM111_SM_CMD_REJECT_UNBONDED;


typedef struct
{
    uint8_t bonding_id;                                                            
}struct_BGM111_SM_CMD_CLEAR_SINGLE_BONDING;


typedef enum
{
BGM111_SM_INFO_LIST_BONDING_REMEANING = 0,                                      
BGM111_SM_INFO_LIST_BONDING_FINNISHED,                                          
BGM111_SM_INFO_LIST_BONDING_MAX                                                 
}enum_BGM111_SM_INFO_LIST_BONDING_FINNISHED;


typedef struct
{
    enum_BGM111_SM_INFO_LIST_BONDING_FINNISHED finnished;                       
    uint8_t addr_type;                                                          
    uint8_t bonding;                                                            
    struct_BT_MAC mac;                                                          
}struct_BGM111_SM_INFO_LIST_BONDING;



typedef enum
{
BGM111_SM_CMD_BONDING_MODE_BONDINGS_EN_FORBID_NEW_BONDINGS = 0,                 
BGM111_SM_CMD_BONDING_MODE_BONDINGS_EN_ALLOW_NEW_BONDINGS,                      
BGM111_SM_CMD_BONDING_MODE_BONDINGS_EN_MODE_MAX                                 
}enum_BGM111_SM_CMD_BONDING_MODE_BONDINGS_EN;


typedef enum
{
    BGM111_SM_CMD_BONDING_MODE_OVERWRITE_MODE_BONDFAIL = 0,                     
    BGM111_SM_CMD_BONDING_MODE_OVERWRITE_MODE_OLDEST,                           
    BGM111_SM_CMD_BONDING_MODE_OVERWRITE_MODE_UNUSED,                           
    BGM111_SM_CMD_BONDING_MODE_OVERWRITE_MODE_MAX
}enum_BGM111_SM_CMD_BONDING_MODE_OVERWRITE_MODE;



typedef struct
{
    enum_BGM111_SM_CMD_BONDING_MODE_BONDINGS_EN bondings_en;                    
    uint8_t bond_count;                                                         
    enum_BGM111_SM_CMD_BONDING_MODE_OVERWRITE_MODE overwrite_mode;              
}struct_BGM111_SM_CMD_BONDING_MODE;


typedef struct
{
    enum_BGM111_CL_CMD_ADVERTISE_DISCOVERABLE discoverable;                     
    enum_BGM111_CL_CMD_ADVERTISE_CONNECTABLE connectable;                       
}struct_BGM111_CL_CMD_ADVERTISE;

typedef enum
{
BGM111_CL_CMD_SET_ADV_PARAMETERS_ADV_CHANNELS_NONE   = 0,                                 
BGM111_CL_CMD_SET_ADV_PARAMETERS_ADV_CHANNELS_37,                               
BGM111_CL_CMD_SET_ADV_PARAMETERS_ADV_CHANNELS_38,                               
BGM111_CL_CMD_SET_ADV_PARAMETERS_ADV_CHANNELS_37_38,                            
BGM111_CL_CMD_SET_ADV_PARAMETERS_ADV_CHANNELS_39,                               
BGM111_CL_CMD_SET_ADV_PARAMETERS_ADV_CHANNELS_37_39,                            
BGM111_CL_CMD_SET_ADV_PARAMETERS_ADV_CHANNELS_38_39,                            
BGM111_CL_CMD_SET_ADV_PARAMETERS_ADV_CHANNELS_37_38_39,                         
BGM111_CL_CMD_SET_ADV_PARAMETERS_ADV_CHANNELS_MAX                               
}enum_BGM111_CL_CMD_SET_ADV_PARAMETERS_CHANNELS;


typedef struct
{
    uint16_t min_adv_interval;                                                  
    uint16_t max_adv_interval;                                                  
    enum_BGM111_CL_CMD_SET_ADV_PARAMETERS_CHANNELS advertisement_channels;      
    uint16_t maximum_mtu;                                                       
}struct_BGM111_CL_CMD_SET_ADV_PARAMETERS;



typedef struct
{
    uint16_t characteristic;                                                    
    uint16_t data_length;                                                       
    uint8_t* data;                                                              
}struct_BGM111_CL_CMD_SET_CHARACTERISTIC;


typedef struct
{
    uint16_t characteristic;                                                    
    uint16_t data_length;                                                       
    uint8_t* data;                                                              
}struct_BGM111_CL_CMD_SEND_NOTIFICATION;


typedef enum
{
BGM111_CL_USER_RESPONSE_STATUS_OK    = 0,                                       
BGM111_CL_USER_RESPONSE_STATUS_FAILED,                                          
BGM111_CL_USER_RESPONSE_STATUS_MAX                                              
}enum_BGM111_CL_USER_RESPONSE_STATUS;


typedef enum
{
BGM111_ATT_OPCODE_READ_BY_TYPE_REQUEST            =8 ,                     
BGM111_ATT_OPCODE_READ_BY_TYPE_RESPONSE           =9 ,                     
BGM111_ATT_OPCODE_READ_REQUEST                    =10,                     
BGM111_ATT_OPCODE_READ_RESPONSE                   =11,                     
BGM111_ATT_OPCODE_READ_BLOB_REQUEST               =12,                     
BGM111_ATT_OPCODE_READ_BLOB_RESPONSE              =13,                     
BGM111_ATT_OPCODE_READ_MULTIPLE_REQUEST           =14,                     
BGM111_ATT_OPCODE_READ_MULTIPLE_RESPONSE          =15,                     
BGM111_ATT_OPCODE_WRITE_REQUEST                   =18,                     
BGM111_ATT_OPCODE_WRITE_RESPONSE                  =19,                     
BGM111_ATT_OPCODE_WRITE_COMMAND                   =82,                     
BGM111_ATT_OPCODE_PREPARE_WRITE_REQUEST           =22,                     
BGM111_ATT_OPCODE_PREPARE_WRITE_RESPONSE          =23,                     
BGM111_ATT_OPCODE_EXECUTE_WRITE_REQUEST           =24,                     
BGM111_ATT_OPCODE_EXECUTE_WRITE_RESPONSE          =25,                     
BGM111_ATT_OPCODE_HANDLE_VALUE_NOTIFICATION       =27,                     
BGM111_ATT_OPCODE_HANDLE_VALUE_INDICATION         =29,                     
BGM111_ATT_OPCODE_MAX
}enum_BGM111_ATT_OPCODE;

typedef struct
{
    uint16_t characteristic;                                                    
    uint16_t offset;                                                            
    enum_BGM111_ATT_OPCODE att_opcode;                                          
}struct_BGM111_CL_INFO_USER_READ_REQUEST;


typedef struct
{
    uint16_t characteristic;                                                    
    enum_BGM111_CL_USER_RESPONSE_STATUS resp_state;                             
    uint16_t data_length;                                                       
    uint8_t* data;                                                              
}struct_BGM111_CL_CMD_SEND_USER_READ_RESPONSE;


typedef struct
{
    uint16_t characteristic;                                                    
    enum_BGM111_CL_USER_RESPONSE_STATUS resp_state;                             
}struct_BGM111_CL_CMD_SEND_USER_WRITE_RESPONSE;


typedef struct
{
    uint16_t characteristic;                                                    
    uint16_t offset;                                                            
    enum_BGM111_ATT_OPCODE att_opcode;                                          
    uint16_t data_len;                                                          
    uint8_t* data;                                                              
}struct_BGM111_CL_INFO_USER_WRITE_REQUEST;


typedef struct
{
    uint16_t characteristic;                                                    
    uint16_t offset;                                                            
}struct_BGM111_CL_CMD_READ_LOCAL_CHARACTERISTIC;

typedef struct
{
    uint16_t characteristic;                                                    
    uint16_t offset;                                                            
    enum_BGM111_ATT_OPCODE att_opcode;                                          
    uint16_t data_len;                                                          
    uint8_t* data;                                                              
}struct_BGM111_CL_INFO_CHARACTERISTIC_RECEIVED;

typedef struct
{
    uint32_t can_id;                                                            
    uint8_t is_extended;                                                        
    uint8_t dlc;                                                                
    uint8_t data[8];                                                            
    uint8_t can_bus_id;                                                         
}struct_BGM111_VIRTUAL_CAN_MESSAGE;



typedef enum
{
BGM111_CL_INFO_NOTIFICATIONS_ENABLED_MODE_DISABLED = 0,                         
BGM111_CL_INFO_NOTIFICATIONS_ENABLED_MODE_NOTIFICATION,                         
BGM111_CL_INFO_NOTIFICATIONS_ENABLED_MODE_INDICATION,                           
BGM111_CL_INFO_NOTIFICATIONS_ENABLED_MODE_MAX                                   
}enum_BGM111_CL_INFO_NOTIFICATIONS_ENABLED_MODE;

typedef enum
{
BGM111_CL_INFO_NOTIFICATIONS_ENABLED_STATUS_CONFIGURATION = 1,                  
BGM111_CL_INFO_NOTIFICATIONS_ENABLED_STATUS_CONFIRMATION,                       
BGM111_CL_INFO_NOTIFICATIONS_ENABLED_STATUS_MAX                                 
}enum_BGM111_CL_INFO_NOTIFICATIONS_ENABLED_STATUS;


typedef struct
{
    uint16_t characteristic;                                                    
    enum_BGM111_CL_INFO_NOTIFICATIONS_ENABLED_MODE mode;                        
    enum_BGM111_CL_INFO_NOTIFICATIONS_ENABLED_STATUS status;                    
}struct_BGM111_CL_INFO_NOTIFICATIONS_ENABLED;


typedef enum
{
BGM111_MA_CMD_CONNECT_ADDRESS_TYPE_PUBLIC   = 0,                                
BGM111_MA_CMD_CONNECT_ADDRESS_TYPE_RANDOM,                                      
BGM111_MA_CMD_CONNECT_ADDRESS_TYPE_PUBLIC_ID,                                   
BGM111_MA_CMD_CONNECT_ADDRESS_TYPE_RANDOM_ID,                                   
BGM111_MA_CMD_CONNECT_ADDRESS_TYPE_RANDOM_BREDR = 16,                           
BGM111_MA_CMD_CONNECT_ADDRESS_TYPE_MAX                                          
}enum_BGM111_MA_CMD_CONNECT_ADDRESS_TYPE;


typedef struct
{
    struct_BT_MAC ble_mac;                                                       
    enum_BGM111_MA_CMD_CONNECT_ADDRESS_TYPE address_type;                        
}struct_BGM111_MA_CMD_CONNECT;



typedef enum
{
BGM111_MA_CMD_ENABLE_NOTIFICATIONS_EN_STATE_DISABLED = 0,                        
BGM111_MA_CMD_ENABLE_NOTIFICATIONS_EN_STATE_ENABLED,                             
BGM111_MA_CMD_ENABLE_NOTIFICATIONS_EN_STATE_MAX                                  
}enum_BGM111_MA_CMD_ENABLE_NOTIFICATIONS_EN_STATE;

typedef struct
{
    uint16_t characteristic;                                                    
    enum_BGM111_MA_CMD_ENABLE_NOTIFICATIONS_EN_STATE state;                     
}struct_BGM111_MA_CMD_ENABLE_NOTIFICATIONS;




typedef enum
{
BGM111_MA_INFO_DISCOVERED_DEVICE_PACKET_TYPE_CONNECTABLE = 0,                   
BGM111_MA_INFO_DISCOVERED_DEVICE_PACKET_TYPE_SCANNABLE = 2,                     
BGM111_MA_INFO_DISCOVERED_DEVICE_PACKET_TYPE_NON_CONNECTABLE,                   
BGM111_MA_INFO_DISCOVERED_DEVICE_PACKET_TYPE_SCAN_RESPONSE,                     
BGM111_MA_INFO_DISCOVERED_DEVICE_PACKET_TYPE_MAX                                
}enum_BGM111_MA_INFO_DISCOVERED_DEVICE_PACKET_TYPE;


typedef enum
{
BGM111_MA_INFO_DISCOVERED_DEVICE_ADDRESS_TYPE_PUBLIC = 0,                       
BGM111_MA_INFO_DISCOVERED_DEVICE_ADDRESS_TYPE_RANDOM,                           
BGM111_MA_INFO_DISCOVERED_DEVICE_ADDRESS_TYPE_MAX                               
}enum_BGM111_MA_INFO_DISCOVERED_DEVICE_ADDRESS_TYPE;



typedef enum
{
BGM111_MA_CMD_DISCOVER_DEVICES_STATUS_STOP = 0,                                  
BGM111_MA_CMD_DISCOVER_DEVICES_STATUS_SEARCH,                                    
BGM111_MA_CMD_DISCOVER_DEVICES_STATUS_MAX                                        
}enum_BGM111_MA_CMD_DISCOVER_DEVICES_STATUS;

typedef enum
{
BGM111_MA_CMD_DISCOVER_DEVICES_PARAM_LIMITED = 0,                               
BGM111_MA_CMD_DISCOVER_DEVICES_PARAM_GENERIC,                                   
BGM111_MA_CMD_DISCOVER_DEVICES_PARAM_OBSERVATION ,                               
BGM111_MA_CMD_DISCOVER_DEVICES_PARAM_MAX                                        
}enum_BGM111_MA_CMD_DISCOVER_DEVICES_PARAM;

typedef struct
{
    enum_BGM111_MA_CMD_DISCOVER_DEVICES_STATUS status;                          
    enum_BGM111_MA_CMD_DISCOVER_DEVICES_PARAM discover_mode;                    
}struct_BGM111_MA_CMD_DISCOVER_DEVICES;


typedef struct
{
    struct_BT_MAC mac;                                                          
    int8_t rssi;                                                                
    enum_BGM111_MA_INFO_DISCOVERED_DEVICE_PACKET_TYPE packet_type;              
    enum_BGM111_MA_INFO_DISCOVERED_DEVICE_ADDRESS_TYPE addr_type;               
    uint8_t bonding_handle;                                                     
    uint16_t data_len;                                                          
    uint8_t* data;                                                              
}struct_BGM111_MA_INFO_DISCOVERED_DEVICE;


typedef struct
{
    uint32_t internal_id;                                                       
    uint16_t uuid_len;                                                          
    uint8_t* uuid;                                                              
}struct_BGM111_MA_INFO_DISCOVERED_SERVICE;


typedef struct
{
    uint32_t service;                                                           
}struct_BGM111_MA_CMD_DISCOVER_CHARACTERISTICS;


typedef struct
{
    uint8_t broadcast;                                                          
    uint8_t read;                                                               
    uint8_t write_without_response;                                             
    uint8_t write;                                                              
    uint8_t notify;                                                             
    uint8_t indicate;                                                           
    uint8_t authenticated_signed_writes;                                        
    uint8_t extended_properties;                                                
}struct_BGM111_CHARACTERISTIC_PROPERTIES;




typedef struct
{
    uint16_t characteristic;                                                    
    uint8_t properties_byte;                                                    
    struct_BGM111_CHARACTERISTIC_PROPERTIES properties;                         
    uint16_t uuid_len;                                                          
    uint8_t* uuid;                                                              
}struct_BGM111_MA_INFO_DISCOVERED_CHARACTERISTIC;

typedef struct
{
    uint16_t characteristic;                                                    
}struct_BGM111_MA_CMD_DISCOVER_DESCRIPTORS;


typedef struct
{
 uint16_t internal_id;                                                          
 uint16_t uuid_len;                                                             
 uint8_t* uuid;                                                                 
}struct_BGM111_MA_INFO_DISCOVERED_DESCRIPTOR;




typedef enum
{
BGM111_MA_CMD_SEND_CHARACTERISTIC_MODE_WRITE_WITH_RESPONSE         = 0,         
BGM111_MA_CMD_SEND_CHARACTERISTIC_MODE_WRITE_NO_RESPONSE,                       
BGM111_MA_CMD_SEND_CHARACTERISTIC_MODE_CHARACTERISTIC_CONFIRMATION,             
BGM111_MA_CMD_SEND_CHARACTERISTIC_MODE_MAX                                      
}enum_BGM111_MA_CMD_SEND_CHARACTERISTIC_MODE;

typedef struct
{
    enum_BGM111_MA_CMD_SEND_CHARACTERISTIC_MODE mode;                           
    uint16_t characteristic;                                                    
    uint16_t data_len;                                                          
    uint8_t* data;                                                              
}struct_BGM111_MA_CMD_SEND_CHARACTERISTIC;




typedef struct
{
    uint16_t characteristic;                                                    
}struct_BGM111_MA_CMD_READ_CHARACTERISTIC;


typedef struct
{
    uint16_t characteristic;                                                    
    uint16_t offset;                                                            
    enum_BGM111_ATT_OPCODE att_opcode;                                          
    uint16_t data_len;                                                          
    uint8_t* data;                                                              

}struct_BGM111_MA_INFO_CHARACTERISTIC_RECEIVED;



typedef struct
{
    uint16_t descriptor_id;                                                     
}struct_BGM111_MA_CMD_READ_DESCRIPTOR;

typedef struct
{
    uint16_t descriptor_id;                                                     
    uint16_t offset;                                                            
    uint16_t data_len;                                                          
    uint8_t* data;                                                              
}struct_BGM111_MA_INFO_DESCRIPTOR_RECEIVED;


typedef struct
{
    uint16_t result;                                                            
}struct_BGM111_MA_INFO_GATT_OP_COMPLETED;









#endif 


