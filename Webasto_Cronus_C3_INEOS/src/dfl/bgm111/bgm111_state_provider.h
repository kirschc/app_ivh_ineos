
#ifndef BGM111_STATE_PROVIDER_H_
#define BGM111_STATE_PROVIDER_H_

#include "bgm111_protocol_base.h"
#include "bgm111_cfg.h"


#define BLOCKING_PUT_RETRIES    15                                      
#define SCI_RX_BUF_SIZE         BGM111_RX_FIFO_SIZE                     


typedef void (*bgm111_sci_tx_function)(uint16_t data_len, uint8_t* data);

typedef void (*bgm111_ble_frame_rx)(struct_PROTOCOL_MSG_T* data);


typedef enum
{
BGM111_STATE_PROVIDER_ERRORS_NONE = 0,      
BGM111_STATE_PROVIDER_ERRORS_INV_CMD,       
BGM111_STATE_PROVIDER_ERRORS_PARAM,         
BGM111_STATE_PROVIDER_ERRORS_MAX            
}enum_BGM111_STATE_PROVIDER_ERRORS;


typedef enum
{
    STATE_WAITING_HEADER_COMPLETED = 0,     
    STATE_WAITING_CMD_COMPLETED,            
    STATE_MAX                               
}PROTOCOL_HANDLER_STATE_T;



void bgm111_state_provider_init(bgm111_sci_tx_function tx , bgm111_ble_frame_rx callback  );



void bgm111_state_provider_print_cmd(struct_PROTOCOL_MSG_T* cmd);

uint32_t bgm111_state_provider_crc32(struct_PROTOCOL_MSG_T* data_in);

void bgm111_state_provider_inc_timeout(uint32_t val);

void bgm111_state_provider_sci_rx_caller(uint32_t error, uint8_t* data, uint32_t len);

enum_BGM111_STATE_PROVIDER_ERRORS bgm111_state_provider_command_valid(struct_PROTOCOL_MSG_T* command);

void bgm111_state_provider_append_crc32(struct_PROTOCOL_MSG_T* data_out);

void bgm111_state_provider_cyclic(void);

void bgm111_state_provider_request_last_frame(void);

void bgm111_state_provider_print_last_cmd(void);

void bgm111_state_provider_debug_out(enum_BGM111_PROTOCOL_CMD_INFO_CODES_T cmd, enum_BGM111_ERROR_STATUS_T status, uint16_t err_no);



#endif 


