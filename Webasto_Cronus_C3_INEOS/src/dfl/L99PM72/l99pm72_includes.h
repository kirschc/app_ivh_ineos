#ifndef PMIC_INCLUDES_H
#define PMIC_INCLUDES_H




#define RH850_ACTIVE




#ifdef SW_TEST_ACTIVE
    #include <gcc-stdint.h>
#endif

#ifdef RH850_ACTIVE
    #include "hal_data_types.h"
    #include "hal_spi.h"
#endif  

#ifdef RTOS_ENABLE
    #include "FreeRTOS.h"
    #include "semphr.h"
#endif

#include <string.h>




 typedef enum
{
    L99PM72_ERR_NONE             = 0u, 
    L99PM72_ERR_PARAM                , 
    L99PM72_ERR_SPI                  , 
    L99PM72_ERR_UNEQUAL              , 
    L99PM72_ERR_FAILSAFE             , 
    L99PM72_ERR_TIMEOUT              , 
    L99PM72_ERR_UNKNOWN              , 
    L99PM72_ERR_NO_VAL_CHANGE        , 
    L99PM72_ERR_RTOS_MUT_INVALID     , 
    L99PM72_ERR_RTOS_MUT_TIMEOUT      

}enum_L99PM72_ERR_CODE_TYPE;



#endif 


