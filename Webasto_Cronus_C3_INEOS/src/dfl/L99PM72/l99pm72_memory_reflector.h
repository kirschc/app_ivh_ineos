
#ifndef L99PM72_MEMORY_REFLECTOR
#define L99PM72_MEMORY_REFLECTOR



#include "l99pm72_includes.h"
#include "l99pm72_base.h"
#include "l99pm72_protocol.h"
#include "sfl_timer.h"
#include "l99pm72_hw.h"
#include "L99PM72_Cfg.h"


typedef enum
{
    L99PM72_USE_CONFIG_INITIAL,         
    L99PM72_USE_CONFIG_PREVIOUS,        
    L99PM72_USE_CONFIG_MAX
}enum_L99PM72_USE_CONFIG_INIT_TYPE;

typedef enum
{
    L99PM72_INIT_DONE_BL   = 0u, 
    L99PM72_INIT_DONE_APPL     , 
    L99PM72_INIT_TODO_BL       , 
    L99PM72_INIT_TODO_APPL       
} enum_L99PM72_INIT_BOOTLOADER;

extern uint8_t           ext_flg_is_initialized;
extern volatile uint16_t ext_pmic_wd_counter;
extern struct_L99PM72_GLOBAL_STATUS_REGISTER_TYPE ext_l99pm72_status; 
extern volatile uint16_t ext_minimum_watchdog_trigger_time_ms;
extern struct_L99PM72_RX_REG_TYPE ext_l99pm72_rx;   
extern struct_L99PM72_TX_REG_TYPE ext_l99pm72_tx;   
extern uint8_t ext_l99pm72_initial_statusbyte;





typedef void (*l99pm72_error_callback_t)(const struct_L99PM72_GLOBAL_STATUS_REGISTER_TYPE *const);



uint16_t l99pm72_get_watchdog_min_time(enum_L99PM72_WINDOW_WATCHDOG_TRIGGER_TIME_TYPE val);

void l99pm72_configuration_copy(struct_L99PM72_RX_REG_TYPE* source, struct_L99PM72_TX_REG_TYPE* dest);


#ifdef RH850_ACTIVE
#if defined(RTOS_ENABLE) && defined(configUSE_PREEMPTION)
enum_L99PM72_ERR_CODE_TYPE l99pm72_init(const struct_hal_spi_handle *const ptr_spi_handle, \
                                        const SemaphoreHandle_t ptr_mutex, \
                                        const l99pm72_error_callback_t error_callback, \
                                        enum_L99PM72_USE_CONFIG_INIT_TYPE flg_use_device_cfg, \
                                        const enum_L99PM72_INIT_BOOTLOADER init_type);
#else
enum_L99PM72_ERR_CODE_TYPE l99pm72_init(const struct_hal_spi_handle *const ptr_spi_handle, \
                                        const l99pm72_error_callback_t error_callback, \
                                        enum_L99PM72_USE_CONFIG_INIT_TYPE flg_use_device_cfg, \
                                        const enum_L99PM72_INIT_BOOTLOADER init_type);
#endif


#endif

#if ( defined SW_TEST_ACTIVE  )
	void pmic_init(void);
#endif

void l99pm72_statusbyte_store(uint8_t status_byte);

void l99pm72_error_callback_caller(void);

uint16_t l99pm72_watchdog_mode2time( enum_L99PM72_WINDOW_WATCHDOG_TRIGGER_TIME_TYPE mode );


uint8_t l99pm72_trigger_watchdog_forced(void);

enum_L99PM72_ERR_CODE_TYPE l99pm72_unpack_receivebyte(uint8_t register_address, enum_L99PM72_OPCODE_TYPE operational_code, uint16_t data);
void l99pm72_cyclic_caller_non_irq(void);

void l99pm72_statusbyte_decode(void);

enum_L99PM72_ERR_CODE_TYPE l99pm72_clear_status_raw(uint8_t address);

enum_L99PM72_ERR_CODE_TYPE l99pm72_set_raw(uint8_t* datapoint, uint8_t value, uint8_t address);

enum_L99PM72_ERR_CODE_TYPE l99pm72_get_raw(uint8_t address, enum_L99PM72_ADDR_SPACE_TYPE is_eeprom_address);

enum_L99PM72_ERR_CODE_TYPE l99pm72_set_get_compare(uint8_t* old_val, uint8_t new_val, uint8_t address, uint8_t* readback );


#endif 


