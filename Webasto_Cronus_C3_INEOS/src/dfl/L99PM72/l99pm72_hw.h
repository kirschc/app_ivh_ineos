#ifndef L99PM72_HW
#define L99PM72_HW


#include "l99pm72_includes.h"
#include "l99pm72_base.h"

#define L99PM72_DATA_WIDTH                 3u       





void l99pm72hw_set_spi_handle(const struct_hal_spi_handle *const new_handle);

#if defined(RTOS_ENABLE) && defined(configUSE_PREEMPTION)
enum_L99PM72_ERR_CODE_TYPE l99pm72hw_set_mutex(const SemaphoreHandle_t ptr_mutex);
#endif

uint8_t l99pm72hw_spi_read_write(uint16_t* data_tx, uint16_t* data_rx);

static uint16_t l99pm72_generate_registerset(uint8_t address);

enum_L99PM72_ERR_CODE_TYPE l99pm72hw_execute_command(const enum_L99PM72_OPCODE_TYPE cmd, \
                                                     const uint8_t address, \
                                                     const boolean rd_only, \
                                                     uint16_t *const data_rx);

#endif 


