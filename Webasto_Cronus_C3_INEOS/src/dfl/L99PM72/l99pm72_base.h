#ifndef L99PM72_BASE_DEFS_H
#define L99PM72_BASE_DEFS_H

#include "l99pm72_includes.h"

typedef enum
{
    L99PM72_OPCODE_WRITE   = 0,                                     
    L99PM72_OPCODE_READ,                                            
    L99PM72_OPCODE_READ_CLEAR_STATUS,                               
    L99PM72_OPCODE_EEPROM,                                          
    L99PM72_OPCODE_MAX                                              
}enum_L99PM72_OPCODE_TYPE;

#define L99PM72_OPCODE_SHIFT_SIZE   6

typedef enum
{
    L99PM72_ADDR_SPACE_RAM = 0u,                                                     
    L99PM72_ADDR_SPACE_ROM,                                                     
    L99PM72_ADDR_SPACE_MAX                                                      
}enum_L99PM72_ADDR_SPACE_TYPE;


#define L99PM72_ADDR_MASK                                              (0x3Fu)
#define L99PM72_ROM_ADDR_ID_HEADER                                     0x00    
#define L99PM72_ROM_ADDR_SILICON_VERSION                               0x01    
#define L99PM72_ROM_ADDR_PROCUCT_CODE1                                 0x02    
#define L99PM72_ROM_ADDR_PRODUCT_CODE2                                 0x03    
#define L99PM72_ROM_ADDR_SPI_FRAME_ID                                  0x3E    


#define L99PM72_RAM_ADDR_CONTROL_1                                     0x01    
#define L99PM72_RAM_ADDR_CONTROL_2                                     0x02    
#define L99PM72_RAM_ADDR_CONTROL_3                                     0x03    
#define L99PM72_RAM_ADDR_CONTROL_4                                     0x04    
#define L99PM72_RAM_ADDR_CONTROL_5                                     0x05    
#define L99PM72_RAM_ADDR_CONTROL_6                                     0x06    
#define L99PM72_RAM_ADDR_CONTROL_7                                     0x07    
#define L99PM72_RAM_ADDR_CONTROL_8                                     0x08    
#define L99PM72_RAM_ADDR_CONTROL_9                                     0x09    
#define L99PM72_RAM_ADDR_CONTROL_10                                    0x0A    
#define L99PM72_RAM_ADDR_CONTROL_11                                    0x0B    
#define L99PM72_RAM_ADDR_CONTROL_12                                    0x0C    
#define L99PM72_RAM_ADDR_CONTROL_13                                    0x0D    
#define L99PM72_RAM_ADDR_CONTROL_14                                    0x0E    
#define L99PM72_RAM_ADDR_CONTROL_15                                    0x0F    
#define L99PM72_RAM_ADDR_CONTROL_16                                    0x10    
#define L99PM72_RAM_ADDR_CONTROL_34                                    0x22    
#define L99PM72_RAM_ADDR_CONTROL_35                                    0x23    
#define L99PM72_RAM_ADDR_CONFIG                                        0x3F    

#define L99PM72_RAM_ADDR_STATUS_1                                      0x11    
#define L99PM72_RAM_ADDR_STATUS_2                                      0x12    
#define L99PM72_RAM_ADDR_STATUS_3                                      0x13    
#define L99PM72_RAM_ADDR_STATUS_4                                      0x14    
#define L99PM72_RAM_ADDR_STATUS_5                                      0x15    




#define GLOB_STATE_OK                                               0x20    
#define GLOB_STATE_PWR_ON                                           0x80    
#define GLOB_STATE_WEAK_BAT                                         0x82    
#define GLOB_STATE_V1_FAIL                                          0xA4    
#define GLOB_STATE_COMMUNICATION_ERROR                              0xC0    
#define GLOB_STATE_OVER_UNDER_VOLTAGE_ERR                           0xA2    
#define GLOB_STATE_WD_SPI_FAIL                                      0xA1    
#define GLOB_STATE_TSD1                                             0xA8    
#define GLOB_STATE_TSD2                                             0xB9    
#define GLOB_STATE_UNKNOWN_ERR                                      0xA0    

#define GLOB_STAT_FAIL_SAFE                                         0x01u




typedef struct
{
    uint8_t wd_spi_fail;                                            
    uint8_t vs_fail;                                                
    uint8_t v1_fail;                                                
    uint8_t tsd1;                                                   
    uint8_t tsd2;                                                   
    uint8_t not_flg;                                                
    uint8_t communication_err;                                      
    uint8_t global_err;                                             
    uint8_t power_on;                                               
}struct_L99PM72_GLOBAL_STATUS_REGISTER_TYPE;

#define L99PM72_GLOB_STAT_FAIL_SAVE_POS                                0u
#define L99PM72_GLOB_STAT_FAIL_SAVE_MASK                               1u
#define L99PM72_GLOB_STAT_FAIL_SAVE_GET(x)                             ((((x)) >> L99PM72_GLOB_STAT_FAIL_SAVE_POS) & L99PM72_GLOB_STAT_FAIL_SAVE_MASK)

#define L99PM72_GLOB_STAT_VS_FAIL_POS                                  1u
#define L99PM72_GLOB_STAT_VS_FAIL_MASK                                 1u
#define L99PM72_GLOB_STAT_VS_FAIL_GET(x)                               (((x) >> L99PM72_GLOB_STAT_VS_FAIL_POS) & L99PM72_GLOB_STAT_VS_FAIL_MASK)

#define L99PM72_GLOB_STAT_V1_FAIL_POS                                  2u
#define L99PM72_GLOB_STAT_V1_FAIL_MASK                                 1u
#define L99PM72_GLOB_STAT_V1_FAIL_GET(x)                               (((x) >> L99PM72_GLOB_STAT_V1_FAIL_POS) & L99PM72_GLOB_STAT_V1_FAIL_MASK)

#define L99PM72_GLOB_STAT_TSD1_POS                                     3u
#define L99PM72_GLOB_STAT_TSD1_MASK                                    1u
#define L99PM72_GLOB_STAT_TSD1_GET(x)                                  (((x) >> L99PM72_GLOB_STAT_TSD1_POS) & L99PM72_GLOB_STAT_TSD1_MASK)

#define L99PM72_GLOB_STAT_TSD2_POS                                     4u
#define L99PM72_GLOB_STAT_TSD2_MASK                                    1u
#define L99PM72_GLOB_STAT_TSD2_GET(x)                                  (((x) >> L99PM72_GLOB_STAT_TSD2_POS) & L99PM72_GLOB_STAT_TSD2_MASK)

#define L99PM72_GLOB_STAT_NOT_POS                                      5u
#define L99PM72_GLOB_STAT_NOT_MASK                                     1u
#define L99PM72_GLOB_STAT_NOT_GET(x)                                   (((x) >> L99PM72_GLOB_STAT_NOT_POS) & L99PM72_GLOB_STAT_NOT_MASK)

#define L99PM72_GLOB_STAT_COM_ERR_POS                                  6u
#define L99PM72_GLOB_STAT_COM_ERR_MASK                                 1u
#define L99PM72_GLOB_STAT_COM_ERR_GET(x)                               (((x) >> L99PM72_GLOB_STAT_COM_ERR_POS) & L99PM72_GLOB_STAT_COM_ERR_MASK)

#define L99PM72_GLOB_STAT_GLOBALERR_POS                                7u
#define L99PM72_GLOB_STAT_GLOBALERR_MASK                               1u
#define L99PM72_GLOB_STAT_GLOBALERR_GET(x)                             (((x) >> L99PM72_GLOB_STAT_GLOBALERR_POS) & L99PM72_GLOB_STAT_GLOBALERR_MASK)





typedef enum
{
    L99PM72_MODE_VBAT_STANDBY = 0,                                          
    L99PM72_MODE_V1_STANDBY,                                                
    L99PM72_MODE_MAX
}enum_L99PM72_MODE_STANDBY_TYPE;


typedef enum
{
    L99PM72_HSD4_MODE_OFF = 0,                                              
    L99PM72_HSD4_MODE_ON,                                                   
    L99PM72_HSD4_MODE_PWM4,                                                 
    L99PM72_HSD4_MODE_TM2,                                                  
    L99PM72_HSD4_MODE_MAX
}enum_L99PM72_HSD4_MODE_TYPE;

typedef enum
{
    L99PM72_HSD3_MODE_FSO = 0,                                              
    L99PM72_HSD3_MODE_PWM3,                                                 
    L99PM72_HSD3_MODE_MAX
}enum_L99PM72_HSD3_MODE_TYPE;

typedef enum
{
    L99PM72_HSD2_MODE_PWM2 = 0,                                             
    L99PM72_HSD2_MODE_TIMER2,                                               
    L99PM72_HSD2_MODE_MAX
}enum_L99PM72_HSD2_MODE_TYPE;


typedef enum
{
    L99PM72_HSD1_MODE_PWM1 = 0,                                             
    L99PM72_HSD1_MODE_TIMER1,                                               
    L99PM72_HSD1_MODE_MAX
}enum_L99PM72_HSD1_MODE_TYPE;


typedef enum
{
    L99PM72_RELAY_STATE_DISABLED = 0,                                       
    L99PM72_RELAY_STATE_ENABLED,                                            
    L99PM72_RELAY_STATE_MAX,
}enum_L99PM72_RELAY_STATE_TYPE;

typedef enum
{
    L99PM72_V2_MODE_DISABLED = 0,                                           
    L99PM72_V2_MODE_ENABLED_ACTIVE,                                         
    L99PM72_V2_MODE_ENABLED_V1_STANDBY,                                     
    L99PM72_V2_MODE_ENABLED_V1_VBAT_STANDBY,                                
    L99PM72_V2_MODE_MAX
}enum_L99PM72_V2_MODE_STATE_TYPE;


typedef enum
{
    L99PM72_OUTHS_MODE_OFF = 0,                                             
    L99PM72_OUTHS_MODE_TM1,                                                 
    L99PM72_OUTHS_MODE_PWM4,                                                
    L99PM72_OUTHS_MODE_TM2,                                                 
    L99PM72_OUTHS_MODE_PWM3 = 6,                                            
    L99PM72_OUTHS_MODE_ON = 7,                                              
    L99PM72_OUTHS_MODE_MAX
}enum_L99PM72_OUTHS_MODE_TYPE;


typedef struct
{
    uint8_t trig;                                                   
    uint8_t go_stby;                                                
    uint8_t stby_sel;                                               
    uint8_t parity;                                                 
    uint8_t v2;                                                     
    uint8_t rel1;                                                   
    uint8_t rel2;                                                   
    uint8_t out1;                                                   
    uint8_t out2;                                                   
    uint8_t out3;                                                   
    uint8_t ouths_ext;                                              
    uint8_t out4;                                                   
    uint8_t ouths;                                                  
}struct_L99PM72_CTRL1_REG_TYPE;




#define L99PM72_CTRL1_TRIG_POS                                         0u
#define L99PM72_CTRL1_TRIG_MASK                                        1u
#define L99PM72_CTRL1_TRIG_SET(x)                                      ((((x)) & L99PM72_CTRL1_TRIG_MASK) << L99PM72_CTRL1_TRIG_POS)
#define L99PM72_CTRL1_TRIG_GET(x)                                      ((((x)) >> L99PM72_CTRL1_TRIG_POS) & L99PM72_CTRL1_TRIG_MASK)

#define L99PM72_CTRL1_GO_STBY_POS                                      1u
#define L99PM72_CTRL1_GO_STBY_MASK                                     1u
#define L99PM72_CTRL1_GO_STBY_SET(x)                                   ((((x)) & L99PM72_CTRL1_GO_STBY_MASK) << L99PM72_CTRL1_GO_STBY_POS)
#define L99PM72_CTRL1_GO_STBY_GET(x)                                   ((((x)) >> L99PM72_CTRL1_GO_STBY_POS) & L99PM72_CTRL1_GO_STBY_MASK)

#define L99PM72_CTRL1_STBY_SEL_POS                                     2u
#define L99PM72_CTRL1_STBY_SEL_MASK                                    1u
#define L99PM72_CTRL1_STBY_SEL_SET(x)                                  ((((x)) & L99PM72_CTRL1_STBY_SEL_MASK) << L99PM72_CTRL1_STBY_SEL_POS)
#define L99PM72_CTRL1_STBY_SEL_GET(x)                                  ((((x)) >> L99PM72_CTRL1_STBY_SEL_POS) & L99PM72_CTRL1_STBY_SEL_MASK)


#define L99PM72_CTRL1_PARITY_POS                                       3u
#define L99PM72_CTRL1_PARITY_MASK                                      1u
#define L99PM72_CTRL1_PARITY_SET(x)                                    ((((x)) & L99PM72_CTRL1_PARITY_MASK) << L99PM72_CTRL1_PARITY_POS)
#define L99PM72_CTRL1_PARITY_GET(x)                                    ((((x)) >> L99PM72_CTRL1_PARITY_POS) & L99PM72_CTRL1_PARITY_MASK)

#define L99PM72_CTRL1_V2_POS                                           4u
#define L99PM72_CTRL1_V2_MASK                                          3u
#define L99PM72_CTRL1_V2_SET(x)                                        ((((x)) & L99PM72_CTRL1_V2_MASK) << L99PM72_CTRL1_V2_POS)
#define L99PM72_CTRL1_V2_GET(x)                                        ((((x)) >> L99PM72_CTRL1_V2_POS) & L99PM72_CTRL1_V2_MASK)

#define L99PM72_CTRL1_REL1_POS                                         6u
#define L99PM72_CTRL1_REL1_MASK                                        1u
#define L99PM72_CTRL1_REL1_SET(x)                                      ((((x)) & L99PM72_CTRL1_REL1_MASK) << L99PM72_CTRL1_REL1_POS)
#define L99PM72_CTRL1_REL1_GET(x)                                      ((((x)) >> L99PM72_CTRL1_REL1_POS) & L99PM72_CTRL1_REL1_MASK)

#define L99PM72_CTRL1_REL2_POS                                         7u
#define L99PM72_CTRL1_REL2_MASK                                        1u
#define L99PM72_CTRL1_REL2_SET(x)                                      ((((x)) & L99PM72_CTRL1_REL2_MASK) << L99PM72_CTRL1_REL2_POS)
#define L99PM72_CTRL1_REL2_GET(x)                                      ((((x)) >> L99PM72_CTRL1_REL2_POS) & L99PM72_CTRL1_REL2_MASK)

#define L99PM72_CTRL1_OUT1_POS                                         8u
#define L99PM72_CTRL1_OUT1_MASK                                        1u
#define L99PM72_CTRL1_OUT1_SET(x)                                      ((((x)) & L99PM72_CTRL1_OUT1_MASK) << L99PM72_CTRL1_OUT1_POS)
#define L99PM72_CTRL1_OUT1_GET(x)                                      ((((x)) >> L99PM72_CTRL1_OUT1_POS) & L99PM72_CTRL1_OUT1_MASK)

#define L99PM72_CTRL1_OUT2_POS                                         9u
#define L99PM72_CTRL1_OUT2_MASK                                        1u
#define L99PM72_CTRL1_OUT2_SET(x)                                      ((((x)) & L99PM72_CTRL1_OUT2_MASK) << L99PM72_CTRL1_OUT2_POS)
#define L99PM72_CTRL1_OUT2_GET(x)                                      ((((x)) >> L99PM72_CTRL1_OUT2_POS) & L99PM72_CTRL1_OUT2_MASK)

#define L99PM72_CTRL1_OUT3_POS                                         10u
#define L99PM72_CTRL1_OUT3_MASK                                        1u
#define L99PM72_CTRL1_OUT3_SET(x)                                      ((((x)) & L99PM72_CTRL1_OUT3_MASK) << L99PM72_CTRL1_OUT3_POS)
#define L99PM72_CTRL1_OUT3_GET(x)                                      ((((x)) >> L99PM72_CTRL1_OUT3_POS) & L99PM72_CTRL1_OUT3_MASK)

#define L99PM72_CTRL1_OUTHS_EXT_POS                                    11u
#define L99PM72_CTRL1_OUTHS_EXT_MASK                                   1u
#define L99PM72_CTRL1_OUTHS_EXT_SET(x)                                 ((((x)) & L99PM72_CTRL1_OUTHS_EXT_MASK) << L99PM72_CTRL1_OUTHS_EXT_POS)
#define L99PM72_CTRL1_OUTHS_EXT_GET(x)                                 ((((x)) >> L99PM72_CTRL1_OUTHS_EXT_POS) & L99PM72_CTRL1_OUTHS_EXT_MASK)

#define L99PM72_CTRL1_OUT4_POS                                         12u
#define L99PM72_CTRL1_OUT4_MASK                                        3u
#define L99PM72_CTRL1_OUT4_SET(x)                                      ((((x)) & L99PM72_CTRL1_OUT4_MASK) << L99PM72_CTRL1_OUT4_POS)
#define L99PM72_CTRL1_OUT4_GET(x)                                      ((((x)) >> L99PM72_CTRL1_OUT4_POS) & L99PM72_CTRL1_OUT4_MASK)

#define L99PM72_CTRL1_OUTHS_POS                                        14u
#define L99PM72_CTRL1_OUTHS_MASK                                       3u
#define L99PM72_CTRL1_OUTHS_SET(x)                                     ((((x)) & L99PM72_CTRL1_OUTHS_MASK) << L99PM72_CTRL1_OUTHS_POS)
#define L99PM72_CTRL1_OUTHS_GET(x)                                     ((((x)) >> L99PM72_CTRL1_OUTHS_POS) & L99PM72_CTRL1_OUTHS_MASK)



typedef enum
{
    L99PM72_WU_STATIC_64 = 0,                                               
    L99PM72_WU_TIMER2_80,                                                   
    L99PM72_WU_TIMER2_800,                                                  
    L99PM72_WU_TIMER1_800,                                                  
    L99PM72_WU_TIMER_MAX
}enum_L99PM72_WAKEUP_FILTER_CFG_TYPE;

typedef enum
{
    L99PM72_WU_PULL_DOWN = 0,                                               
    L99PM72_WU_PULL_UP,                                                     
    L99PM72_WU_PULL_MAX
}enum_L99PM72_WU_PUPD_TYPE;


typedef enum
{
    L99PM72_WU_DISABLED = 0,                                                
    L99PM72_WU_ENABLED,                                                     
    L99PM72_WU_MAX
}enum_L99PM72_WU_STATE_TYPE;



typedef struct
{
        uint8_t wu1_en;                                             
        uint8_t wu2_en;                                             
        uint8_t wu3_en;                                             
        uint8_t wu1_pupd;                                           
        uint8_t wu2_pupd;                                           
        uint8_t wu3_pupd;                                           
        uint8_t wu1_filt;                                           
        uint8_t wu2_filt;                                           
        uint8_t wu3_filt;                                           
}struct_L99PM72_CTRL2_REG_TYPE;


#define L99PM72_CTRL2_WU1_EN_POS                                       0u
#define L99PM72_CTRL2_WU1_EN_MASK                                      1u
#define L99PM72_CTRL2_WU1_EN_SET(x)                                    ((((x)) & L99PM72_CTRL2_WU1_EN_MASK) << L99PM72_CTRL2_WU1_EN_POS)
#define L99PM72_CTRL2_WU1_EN_GET(x)                                    ((((x)) >> L99PM72_CTRL2_WU1_EN_POS) & L99PM72_CTRL2_WU1_EN_MASK)

#define L99PM72_CTRL2_WU2_EN_POS                                       1u
#define L99PM72_CTRL2_WU2_EN_MASK                                      1u
#define L99PM72_CTRL2_WU2_EN_SET(x)                                    ((((x)) & L99PM72_CTRL2_WU2_EN_MASK) << L99PM72_CTRL2_WU2_EN_POS)
#define L99PM72_CTRL2_WU2_EN_GET(x)                                    ((((x)) >> L99PM72_CTRL2_WU2_EN_POS) & L99PM72_CTRL2_WU2_EN_MASK)

#define L99PM72_CTRL2_WU3_EN_POS                                       2u
#define L99PM72_CTRL2_WU3_EN_MASK                                      1u
#define L99PM72_CTRL2_WU3_EN_SET(x)                                    ((((x)) & L99PM72_CTRL2_WU3_EN_MASK) << L99PM72_CTRL2_WU3_EN_POS)
#define L99PM72_CTRL2_WU3_EN_GET(x)                                    ((((x)) >> L99PM72_CTRL2_WU3_EN_POS) & L99PM72_CTRL2_WU3_EN_MASK)

#define L99PM72_CTRL2_RESERVED_POS                                     3u
#define L99PM72_CTRL2_RESERVED_MASK                                    1u
#define L99PM72_CTRL2_RESERVED_SET(x)                                  ((((x)) & L99PM72_CTRL2_RESERVED_MASK) << L99PM72_CTRL2_RESERVED_POS)
#define L99PM72_CTRL2_RESERVED_GET(x)                                  ((((x)) >> L99PM72_CTRL2_RESERVED_POS) & L99PM72_CTRL2_RESERVED_MASK)

#define L99PM72_CTRL2_WU1_PUPD_POS                                     4u
#define L99PM72_CTRL2_WU1_PUPD_MASK                                    1u
#define L99PM72_CTRL2_WU1_PUPD_SET(x)                                  ((((x)) & L99PM72_CTRL2_WU1_PUPD_MASK) << L99PM72_CTRL2_WU1_PUPD_POS)
#define L99PM72_CTRL2_WU1_PUPD_GET(x)                                  ((((x)) >> L99PM72_CTRL2_WU1_PUPD_POS) & L99PM72_CTRL2_WU1_PUPD_MASK)

#define L99PM72_CTRL2_WU2_PUPD_POS                                     5u
#define L99PM72_CTRL2_WU2_PUPD_MASK                                    1u
#define L99PM72_CTRL2_WU2_PUPD_SET(x)                                  ((((x)) & L99PM72_CTRL2_WU2_PUPD_MASK) << L99PM72_CTRL2_WU2_PUPD_POS)
#define L99PM72_CTRL2_WU2_PUPD_GET(x)                                  ((((x)) >> L99PM72_CTRL2_WU2_PUPD_POS) & L99PM72_CTRL2_WU2_PUPD_MASK)

#define L99PM72_CTRL2_WU3_PUPD_POS                                     6u
#define L99PM72_CTRL2_WU3_PUPD_MASK                                    1u
#define L99PM72_CTRL2_WU3_PUPD_SET(x)                                  ((((x)) & L99PM72_CTRL2_WU3_PUPD_MASK) << L99PM72_CTRL2_WU3_PUPD_POS)
#define L99PM72_CTRL2_WU3_PUPD_GET(x)                                  ((((x)) >> L99PM72_CTRL2_WU3_PUPD_POS) & L99PM72_CTRL2_WU3_PUPD_MASK)

#define L99PM72_CTRL2_RESERVED2_POS                                    7u
#define L99PM72_CTRL2_RESERVED2_MASK                                   1u
#define L99PM72_CTRL2_RESERVED2_SET(x)                                 ((((x)) & L99PM72_CTRL2_RESERVED2_MASK) << L99PM72_CTRL2_RESERVED2_POS)
#define L99PM72_CTRL2_RESERVED2_GET(x)                                 ((((x)) >> L99PM72_CTRL2_RESERVED2_POS) & L99PM72_CTRL2_RESERVED2_MASK)


#define L99PM72_CTRL2_WU1_FILT_POS                                     8u
#define L99PM72_CTRL2_WU1_FILT_MASK                                    3u
#define L99PM72_CTRL2_WU1_FILT_SET(x)                                  ((((x)) & L99PM72_CTRL2_WU1_FILT_MASK) << L99PM72_CTRL2_WU1_FILT_POS)
#define L99PM72_CTRL2_WU1_FILT_GET(x)                                  ((((x)) >> L99PM72_CTRL2_WU1_FILT_POS) & L99PM72_CTRL2_WU1_FILT_MASK)

#define L99PM72_CTRL2_WU2_FILT_POS                                     10u
#define L99PM72_CTRL2_WU2_FILT_MASK                                    3u
#define L99PM72_CTRL2_WU2_FILT_SET(x)                                  ((((x)) & L99PM72_CTRL2_WU2_FILT_MASK) << L99PM72_CTRL2_WU2_FILT_POS)
#define L99PM72_CTRL2_WU2_FILT_GET(x)                                  ((((x)) >> L99PM72_CTRL2_WU2_FILT_POS) & L99PM72_CTRL2_WU2_FILT_MASK)

#define L99PM72_CTRL2_WU3_FILT_POS                                     12u
#define L99PM72_CTRL2_WU3_FILT_MASK                                    3u
#define L99PM72_CTRL2_WU3_FILT_SET(x)                                  ((((x)) & L99PM72_CTRL2_WU3_FILT_MASK) << L99PM72_CTRL2_WU3_FILT_POS)
#define L99PM72_CTRL2_WU3_FILT_GET(x)                                  ((((x)) >> L99PM72_CTRL2_WU3_FILT_POS) & L99PM72_CTRL2_WU3_FILT_MASK)

#define L99PM72_CTRL2_RESERVED3_POS                                    14u
#define L99PM72_CTRL2_RESERVED3_MASK                                   3u
#define L99PM72_CTRL2_RESERVED3_SET(x)                                 ((((x)) & L99PM72_CTRL2_RESERVED3_MASK) << L99PM72_CTRL2_RESERVED3_POS)
#define L99PM72_CTRL2_RESERVED3_GET(x)                                 ((((x)) >> L99PM72_CTRL2_RESERVED3_POS) & L99PM72_CTRL2_RESERVED3_MASK)



typedef enum
{
    L99PM72_T1_HIGH_TIME_10MS = 0,                                          
    L99PM72_T1_HIGH_TIME_20MS,                                              
    L99PM72_T1_HIGH_TIME_MAX
}enum_L99PM72_T1_HIGH_TIME_SELECTION_TYPE;


typedef enum
{
    L99PM72_T1_PERIOD_1S = 0,                                               
    L99PM72_T1_PERIOD_2S,                                                   
    L99PM72_T1_PERIOD_3S,                                                   
    L99PM72_T1_PERIOD_4S,                                                   
    L99PM72_T1_PERIOD_MAX
}enum_L99PM72_T1_PERIOD_TIME_TYPE;


typedef enum
{
    L99PM72_T2_HIGH_TIME_100US = 0,                                         
    L99PM72_T2_HIGH_TIME_1MS,                                               
    L99PM72_T2_HIGH_TIME_MAX
}enum_L99PM72_T2_HIGH_TIME_SELECTION_TYPE;


typedef enum
{
    L99PM72_T2_PERIOD_10MS = 0,                                             
    L99PM72_T2_PERIOD_20MS,                                                 
    L99PM72_T2_PERIOD_50MS,                                                 
    L99PM72_T2_PERIOD_200MS,                                                
    L99PM72_T2_PERIOD_MAX
}enum_L99PM72_T2_PERIOD_TIME_TYPE;


typedef enum
{
    L99PM72_WD_TRIGGER_TIME_10MS = 0,                                       
    L99PM72_WD_TRIGGER_TIME_50MS,                                           
    L99PM72_WD_TRIGGER_TIME_100MS,                                          
    L99PM72_WD_TRIGGER_TIME_200MS,                                          
    L99PM72_WD_TRIGGER_TIME_MAX
}enum_L99PM72_WINDOW_WATCHDOG_TRIGGER_TIME_TYPE;


typedef enum
{
    L99PM72_LIN_WU_EN_DISABLED = 0,                                         
    L99PM72_LIN_WU_EN_ENABLED,                                              
    L99PM72_LIN_WU_EN_MAX
}enum_L99PM72_LIN_WAKEUP_STATUS_TYPE;

typedef enum
{
    L99PM72_CAN_WU_EN_DISABLED = 0,                                         
    L99PM72_CAN_WU_EN_ENABLED,                                              
    L99PM72_CAN_WU_EN_MAX
}enum_L99PM72_CAN_WAKEUP_STATUS_TYPE;

typedef enum
{
    L99PM72_WAKTETIMER_MODE_DISABLED = 0,                                        
    L99PM72_WAKTETIMER_MODE_ENABLED,                                             
    L99PM72_WAKTETIMER_MODE_MAX
}enum_L99PM72_WAKETIMER_STATUS_TYPE;

typedef enum
{
    L99PM72_WAKTETIMER_TIMER2 = 0,                                          
    L99PM72_WAKTETIMER_TIMER1,                                              
    L99PM72_WAKTETIMER_MAX
}enum_L99PM72_WAKETIMER_SOURCE_TYPE;


typedef struct
{
    uint8_t wake_timer_select;                                      
    uint8_t wake_timer_en;                                          
    uint8_t can_wu_en;                                              
    uint8_t lin_wu_en;                                              
    uint8_t wd_time;                                                
    uint8_t t2_per;                                                 
    uint8_t t2_on;                                                  
    uint8_t t1_per;                                                 
    uint8_t t1_on;                                                  
}struct_L99PM72_CTRL3_REG_TYPE;


#define L99PM72_CTRL3_WAKE_TIMER_SEL_POS                               0u
#define L99PM72_CTRL3_WAKE_TIMER_SEL_MASK                              1u
#define L99PM72_CTRL3_WAKE_TIMER_SEL_SET(x)                            ((((x)) & L99PM72_CTRL3_WAKE_TIMER_SEL_MASK) << L99PM72_CTRL3_WAKE_TIMER_SEL_POS)
#define L99PM72_CTRL3_WAKE_TIMER_SEL_GET(x)                            ((((x)) >> L99PM72_CTRL3_WAKE_TIMER_SEL_POS) & L99PM72_CTRL3_WAKE_TIMER_SEL_MASK)

#define L99PM72_CTRL3_WAKE_TIMER_EN_POS                                1u
#define L99PM72_CTRL3_WAKE_TIMER_EN_MASK                               1u
#define L99PM72_CTRL3_WAKE_TIMER_EN_SET(x)                             ((((x)) & L99PM72_CTRL3_WAKE_TIMER_EN_MASK) << L99PM72_CTRL3_WAKE_TIMER_EN_POS)
#define L99PM72_CTRL3_WAKE_TIMER_EN_GET(x)                             ((((x)) >> L99PM72_CTRL3_WAKE_TIMER_EN_POS) & L99PM72_CTRL3_WAKE_TIMER_EN_MASK)

#define L99PM72_CTRL3_CAN_WU_EN_POS                                    2u
#define L99PM72_CTRL3_CAN_WU_EN_MASK                                   1u
#define L99PM72_CTRL3_CAN_WU_EN_SET(x)                                 ((((x)) & L99PM72_CTRL3_CAN_WU_EN_MASK) << L99PM72_CTRL3_CAN_WU_EN_POS)
#define L99PM72_CTRL3_CAN_WU_EN_GET(x)                                 ((((x)) >> L99PM72_CTRL3_CAN_WU_EN_POS) & L99PM72_CTRL3_CAN_WU_EN_MASK)

#define L99PM72_CTRL3_LIN_WU_EN_POS                                    3u
#define L99PM72_CTRL3_LIN_WU_EN_MASK                                   1u
#define L99PM72_CTRL3_LIN_WU_EN_SET(x)                                 ((((x)) & L99PM72_CTRL3_LIN_WU_EN_MASK) << L99PM72_CTRL3_LIN_WU_EN_POS)
#define L99PM72_CTRL3_LIN_WU_EN_GET(x)                                 ((((x)) >> L99PM72_CTRL3_LIN_WU_EN_POS) & L99PM72_CTRL3_LIN_WU_EN_MASK)

#define L99PM72_CTRL3_WD_TIME_POS                                      4u
#define L99PM72_CTRL3_WD_TIME_MASK                                     3u
#define L99PM72_CTRL3_WD_TIME_SET(x)                                   ((((x)) & L99PM72_CTRL3_WD_TIME_MASK) << L99PM72_CTRL3_WD_TIME_POS)
#define L99PM72_CTRL3_WD_TIME_GET(x)                                   ((((x)) >> L99PM72_CTRL3_WD_TIME_POS) & L99PM72_CTRL3_WD_TIME_MASK)

#define L99PM72_CTRL3_RESERVED_POS                                     6u
#define L99PM72_CTRL3_RESERVED_MASK                                    3u
#define L99PM72_CTRL3_RESERVED_SET(x)                                  ((((x)) & L99PM72_CTRL3_RESERVED_MASK) << L99PM72_CTRL3_RESERVED_POS)
#define L99PM72_CTRL3_RESERVED_GET(x)                                  ((((x)) >> L99PM72_CTRL3_RESERVED_POS) & L99PM72_CTRL3_RESERVED_MASK)

#define L99PM72_CTRL3_T2_PER_POS                                       8u
#define L99PM72_CTRL3_T2_PER_MASK                                      3u
#define L99PM72_CTRL3_T2_PER_SET(x)                                    ((((x)) & L99PM72_CTRL3_T2_PER_MASK) << L99PM72_CTRL3_T2_PER_POS)
#define L99PM72_CTRL3_T2_PER_GET(x)                                    ((((x)) >> L99PM72_CTRL3_T2_PER_POS) & L99PM72_CTRL3_T2_PER_MASK)

#define L99PM72_CTRL3_T2_ON_POS                                        10u
#define L99PM72_CTRL3_T2_ON_MASK                                       1u
#define L99PM72_CTRL3_T2_ON_SET(x)                                     ((((x)) & L99PM72_CTRL3_T2_ON_MASK) << L99PM72_CTRL3_T2_ON_POS)
#define L99PM72_CTRL3_T2_ON_GET(x)                                     ((((x)) >> L99PM72_CTRL3_T2_ON_POS) & L99PM72_CTRL3_T2_ON_MASK)

#define L99PM72_CTRL3_RESERVED2_POS                                    11u
#define L99PM72_CTRL3_RESERVED2_MASK                                   1u
#define L99PM72_CTRL3_RESERVED2_SET(x)                                 ((((x)) & L99PM72_CTRL3_RESERVED2_MASK) << L99PM72_CTRL3_RESERVED2_POS)
#define L99PM72_CTRL3_RESERVED2_GET(x)                                 ((((x)) >> L99PM72_CTRL3_RESERVED2_POS) & L99PM72_CTRL3_RESERVED2_MASK)

#define L99PM72_CTRL3_T1_PER_POS                                       12u
#define L99PM72_CTRL3_T1_PER_MASK                                      3u
#define L99PM72_CTRL3_T1_PER_SET(x)                                    ((((x)) & L99PM72_CTRL3_T1_PER_MASK) << L99PM72_CTRL3_T1_PER_POS)
#define L99PM72_CTRL3_T1_PER_GET(x)                                    ((((x)) >> L99PM72_CTRL3_T1_PER_POS) & L99PM72_CTRL3_T1_PER_MASK)

#define L99PM72_CTRL3_T1_ON_POS                                        14u
#define L99PM72_CTRL3_T1_ON_MASK                                       1u
#define L99PM72_CTRL3_T1_ON_SET(x)                                     ((((x)) & L99PM72_CTRL3_T1_ON_MASK) << L99PM72_CTRL3_T1_ON_POS)
#define L99PM72_CTRL3_T1_ON_GET(x)                                     ((((x)) >> L99PM72_CTRL3_T1_ON_POS) & L99PM72_CTRL3_T1_ON_MASK)

#define L99PM72_CTRL3_RESERVED3_POS                                    15u
#define L99PM72_CTRL3_RESERVED3_MASK                                   1u
#define L99PM72_CTRL3_RESERVED3_SET(x)                                 ((((x)) & L99PM72_CTRL3_RESERVED3_MASK) << L99PM72_CTRL3_RESERVED3_POS)
#define L99PM72_CTRL3_RESERVED3_GET(x)                                 ((((x)) >> L99PM72_CTRL3_RESERVED3_POS) & L99PM72_CTRL3_RESERVED3_MASK)




typedef enum
{
    L99PM72_V1_WD_DISABLED_BY_LOWCURRENT = 0,                               
    L99PM72_V1_WD_DISABLED_AUTOMATIC,                                       
    L99PM72_V1_WD_DISABLED_MAX
}enum_L99PM72_V1_WATCHDOG_STANDBY_STATUS_TYPE;

typedef enum
{
    L99PM72_OUTHS_OVERCURRENT_RECOVERY_DISABLED = 0,                        
    L99PM72_OUTHS_OVERCURRENT_RECOVERY_ENABLED,                             
    L99PM72_OUTHS_OVERCURRENT_RECOVERY_MAX
}enum_L99PM72_OUTHS_OVERCURRENT_RECOVERY_STATUS_TYPE;


typedef enum
{
    L99PM72_OVERVOLTAGE_STATUS_RECOVERY_ENABLED = 0,                        
    L99PM72_OVERVOLTAGE_STATUS_RECOVERY_DISABLED,                           
    L99PM72_OVERVOLTAGE_STATUS_RECOVERY_MAX
}enum_L99PM72_OVERVOLTAGE_STATUS_RECOVERY_TYPE;


typedef enum
{
    L99PM72_LS_VFAULT_SHUTDOWN_DISABLED = 0,                                
    L99PM72_LS_VFAULT_SHUTDOWN_ENABLED,                                    
    L99PM72_LS_VFAULT_SHUTDOWN_MAX
}enum_L99PM72_LOWSIDE_VOLTAGE_FAILURE_SHUTDOWN_TYPE;


typedef enum
{
    L99PM72_RESET_LEVEL_4V6 = 0,                                            
    L99PM72_RESET_LEVEL_4V35,                                               
    L99PM72_RESET_LEVEL_4V1,                                                
    L99PM72_RESET_LEVEL_3V8,                                                
    L99PM72_RESET_LEVEL_MAX
}enum_L99PM72_RESET_VOLTAGE_LEVEL_TYPE;


typedef enum
{
    L99PM72_LIN_PULLUP_DISABLED = 0,                                        
    L99PM72_LIN_PULLUP_ENABLED,                                             
    L99PM72_LIN_PULLUP_MAX
}enum_L99PM72_LIN_PULLUP_TYPE;


typedef enum
{
    L99PM72_LIN_TRANSMIT_AUTO_OFF_DISABLED = 0,                             
    L99PM72_LIN_TRANSMIT_AUTO_OFF_ENABLED,                                  
    L99PM72_LIN_TRANSMIT_AUTO_OFF_MAX
}enum_L99PM72_LIN_TRANSMITTER_AUTO_OFF_STATUS_TYPE;


typedef enum
{
    L99PM72_CAN_MODE_STANDBY = 0,                                           
    L99PM72_CAN_MODE_ACTIVE,                                                
    L99PM72_CAN_MODE_MAX
}enum_L99PM72_CAN_STATUS_TYPE;


typedef enum
{
    L99PM72_CAN_LOOPBACK_DISABLED = 0,                                      
    L99PM72_CAN_LOOPBACK_ENABLED,                                           
    L99PM72_CAN_LOOPBACK_MAX
}enum_L99PM72_CAN_LOOPBACK_STATUS_TYPE;


typedef enum
{
    L99PM72_CAN_LISTEN_ONLY_DISABLED = 0,                                   
    L99PM72_CAN_LISTEN_ONLY_ENABLED,                                        
    L99PM72_CAN_LISTEN_ONLY_MAX
}enum_L99PM72_CAN_LISTEN_ONLY_MODE_TYPE;


typedef struct
{
    uint8_t can_rec_only;                                           
    uint8_t can_loop_en;                                            
    uint8_t can_act;                                                
    uint8_t lin_tx_tout_en;                                         
    uint8_t lin_pu_en;                                              
    uint8_t v1_reset_lvl;                                           
    uint8_t ls_shutdown_en;                                         
    uint8_t vlock_out_en;                                           
    uint8_t ouths_rec_en;                                           
    uint8_t icmp;                                                   
}struct_L99PM72_CTRL4_REG_TYPE;


#define L99PM72_CTRL4_CAN_REC_ONLY_POS                                 0u
#define L99PM72_CTRL4_CAN_REC_ONLY_MASK                                1u
#define L99PM72_CTRL4_CAN_REC_ONLY_SET(x)                              ((((x)) & L99PM72_CTRL4_CAN_REC_ONLY_MASK) << L99PM72_CTRL4_CAN_REC_ONLY_POS)
#define L99PM72_CTRL4_CAN_REC_ONLY_GET(x)                              ((((x)) >> L99PM72_CTRL4_CAN_REC_ONLY_POS) & L99PM72_CTRL4_CAN_REC_ONLY_MASK)


#define L99PM72_CTRL4_RESERVED_POS                                     1u
#define L99PM72_CTRL4_RESERVED_MASK                                    3u
#define L99PM72_CTRL4_RESERVED_SET(x)                                  ((((x)) & L99PM72_CTRL4_RESERVED_MASK) << L99PM72_CTRL4_RESERVED_POS)
#define L99PM72_CTRL4_RESERVED_GET(x)                                  ((((x)) >> L99PM72_CTRL4_RESERVED_POS) & L99PM72_CTRL4_RESERVED_MASK)


#define L99PM72_CTRL4_CAN_LOOP_EN_POS                                  3u
#define L99PM72_CTRL4_CAN_LOOP_EN_MASK                                 1u
#define L99PM72_CTRL4_CAN_LOOP_EN_SET(x)                               ((((x)) & L99PM72_CTRL4_CAN_LOOP_EN_MASK) << L99PM72_CTRL4_CAN_LOOP_EN_POS)
#define L99PM72_CTRL4_CAN_LOOP_EN_GET(x)                               ((((x)) >> L99PM72_CTRL4_CAN_LOOP_EN_POS) & L99PM72_CTRL4_CAN_LOOP_EN_MASK)

#define L99PM72_CTRL4_CAN_ACT_POS                                      4u
#define L99PM72_CTRL4_CAN_ACT_MASK                                     1u
#define L99PM72_CTRL4_CAN_ACT_SET(x)                                   ((((x)) & L99PM72_CTRL4_CAN_ACT_MASK) << L99PM72_CTRL4_CAN_ACT_POS)
#define L99PM72_CTRL4_CAN_ACT_GET(x)                                   ((((x)) >> L99PM72_CTRL4_CAN_ACT_POS) & L99PM72_CTRL4_CAN_ACT_MASK)


#define L99PM72_CTRL4_LIN_TX_TOUT_EN_POS                               5u
#define L99PM72_CTRL4_LIN_TX_TOUT_EN_MASK                              1u
#define L99PM72_CTRL4_LIN_TX_TOUT_EN_SET(x)                            ((((x)) & L99PM72_CTRL4_LIN_TX_TOUT_EN_MASK) << L99PM72_CTRL4_LIN_TX_TOUT_EN_POS)
#define L99PM72_CTRL4_LIN_TX_TOUT_EN_GET(x)                            ((((x)) >> L99PM72_CTRL4_LIN_TX_TOUT_EN_POS) & L99PM72_CTRL4_LIN_TX_TOUT_EN_MASK)


#define L99PM72_CTRL4_RESERVED2_POS                                    6u
#define L99PM72_CTRL4_RESERVED2_MASK                                   1u
#define L99PM72_CTRL4_RESERVED2_SET(x)                                 ((((x)) & L99PM72_CTRL4_RESERVED2_MASK) << L99PM72_CTRL4_RESERVED2_POS)
#define L99PM72_CTRL4_RESERVED2_GET(x)                                 ((((x)) >> L99PM72_CTRL4_RESERVED2_POS) & L99PM72_CTRL4_RESERVED2_MASK)


#define L99PM72_CTRL4_LIN_PU_EN_POS                                    7u
#define L99PM72_CTRL4_LIN_PU_EN_MASK                                   1u
#define L99PM72_CTRL4_LIN_PU_EN_SET(x)                                 ((((x)) & L99PM72_CTRL4_LIN_PU_EN_MASK) << L99PM72_CTRL4_LIN_PU_EN_POS)
#define L99PM72_CTRL4_LIN_PU_EN_GET(x)                                 ((((x)) >> L99PM72_CTRL4_LIN_PU_EN_POS) & L99PM72_CTRL4_LIN_PU_EN_MASK)



#define L99PM72_CTRL4_V1_RST_LVL_POS                                   8u
#define L99PM72_CTRL4_V1_RST_LVL_MASK                                  3u
#define L99PM72_CTRL4_V1_RST_LVL_SET(x)                                ((((x)) & L99PM72_CTRL4_V1_RST_LVL_MASK) << L99PM72_CTRL4_V1_RST_LVL_POS)
#define L99PM72_CTRL4_V1_RST_LVL_GET(x)                                ((((x)) >> L99PM72_CTRL4_V1_RST_LVL_POS) & L99PM72_CTRL4_V1_RST_LVL_MASK)


#define L99PM72_CTRL4_LS_SHUTDOWN_EN_POS                               10u
#define L99PM72_CTRL4_LS_SHUTDOWN_EN_MASK                              1u
#define L99PM72_CTRL4_LS_SHUTDOWN_EN_SET(x)                            ((((x)) & L99PM72_CTRL4_LS_SHUTDOWN_EN_MASK) << L99PM72_CTRL4_LS_SHUTDOWN_EN_POS)
#define L99PM72_CTRL4_LS_SHUTDOWN_EN_GET(x)                            ((((x)) >> L99PM72_CTRL4_LS_SHUTDOWN_EN_POS) & L99PM72_CTRL4_LS_SHUTDOWN_EN_MASK)


#define L99PM72_CTRL4_RESERVED3_POS                                    11u
#define L99PM72_CTRL4_RESERVED3_MASK                                   1u
#define L99PM72_CTRL4_RESERVED3_SET(x)                                 ((((x)) & L99PM72_CTRL4_RESERVED3_MASK) << L99PM72_CTRL4_RESERVED3_POS)
#define L99PM72_CTRL4_RESERVED3_GET(x)                                 ((((x)) >> L99PM72_CTRL4_RESERVED3_POS) & L99PM72_CTRL4_RESERVED3_MASK)


#define L99PM72_CTRL4_VLOOK_OUT_EN_POS                                 12u
#define L99PM72_CTRL4_VLOOK_OUT_EN_MASK                                1u
#define L99PM72_CTRL4_VLOOK_OUT_EN_SET(x)                              ((((x)) & L99PM72_CTRL4_VLOOK_OUT_EN_MASK) << L99PM72_CTRL4_VLOOK_OUT_EN_POS)
#define L99PM72_CTRL4_VLOOK_OUT_EN_GET(x)                              ((((x)) >> L99PM72_CTRL4_VLOOK_OUT_EN_POS) & L99PM72_CTRL4_VLOOK_OUT_EN_MASK)


#define L99PM72_CTRL4_OUTHS_REC_EN_POS                                 13u
#define L99PM72_CTRL4_OUTHS_REC_EN_MASK                                1u
#define L99PM72_CTRL4_OUTHS_REC_EN_SET(x)                              ((((x)) & L99PM72_CTRL4_OUTHS_REC_EN_MASK) << L99PM72_CTRL4_OUTHS_REC_EN_POS)
#define L99PM72_CTRL4_OUTHS_REC_EN_GET(x)                              ((((x)) >> L99PM72_CTRL4_OUTHS_REC_EN_POS) & L99PM72_CTRL4_OUTHS_REC_EN_MASK)


#define L99PM72_CTRL4_ICMP_POS                                         14u
#define L99PM72_CTRL4_ICMP_MASK                                        1u
#define L99PM72_CTRL4_ICMP_SET(x)                                      ((((x)) & L99PM72_CTRL4_ICMP_MASK) << L99PM72_CTRL4_ICMP_POS)
#define L99PM72_CTRL4_ICMP_GET(x)                                      ((((x)) >> L99PM72_CTRL4_ICMP_POS) & L99PM72_CTRL4_ICMP_MASK)


#define L99PM72_CTRL4_RESERVED4_POS                                    15u
#define L99PM72_CTRL4_RESERVED4_MASK                                   1u
#define L99PM72_CTRL4_RESERVED4_SET(x)                                 ((((x)) & L99PM72_CTRL4_RESERVED4_MASK) << L99PM72_CTRL4_RESERVED4_POS)
#define L99PM72_CTRL4_RESERVED4_GET(x)                                 ((((x)) >> L99PM72_CTRL4_RESERVED4_POS) & L99PM72_CTRL4_RESERVED4_MASK)




#define L99PM72_PWM_DUTY_MAX           0x7F

typedef enum
{
    L99PM72_PWM_FREQ_128HZ = 0,                                             
    L99PM72_PWM_FREQ_256HZ,                                                 
    L99PM72_PWM_FREQ_MAX
}enum_L99PM72_PWM_FREQUENCY_TYPE;



typedef struct
{
    uint8_t pwm1_duty;                                              
    uint8_t pwm_freq;                                               
    uint8_t pwm2_nduty;                                             
}struct_L99PM72_CTRL5_REG_TYPE;


#define L99PM72_CTRL5_PWM1_DUTY_POS                                    0u
#define L99PM72_CTRL5_PWM1_DUTY_MASK                                   0x007F
#define L99PM72_CTRL5_PWM1_DUTY_SET(x)                                 ((((x)) & L99PM72_CTRL5_PWM1_DUTY_MASK) << L99PM72_CTRL5_PWM1_DUTY_POS)
#define L99PM72_CTRL5_PWM1_DUTY_GET(x)                                 ((((x)) >> L99PM72_CTRL5_PWM1_DUTY_POS) & L99PM72_CTRL5_PWM1_DUTY_MASK)


#define L99PM72_CTRL5_PWM_FREQ_POS                                     7u
#define L99PM72_CTRL5_PWM_FREQ_MASK                                    1u
#define L99PM72_CTRL5_PWM_FREQ_SET(x)                                  ((((x)) & L99PM72_CTRL5_PWM_FREQ_MASK) << L99PM72_CTRL5_PWM_FREQ_POS)
#define L99PM72_CTRL5_PWM_FREQ_GET(x)                                  ((((x)) >> L99PM72_CTRL5_PWM_FREQ_POS) & L99PM72_CTRL5_PWM_FREQ_MASK)


#define L99PM72_CTRL5_PWM2_NDUTY_POS                                   8u
#define L99PM72_CTRL5_PWM2_NDUTY_MASK                                  0x007F
#define L99PM72_CTRL5_PWM2_NDUTY_SET(x)                                ((((x)) & L99PM72_CTRL5_PWM2_NDUTY_MASK) << L99PM72_CTRL5_PWM2_NDUTY_POS)
#define L99PM72_CTRL5_PWM2_NDUTY_GET(x)                                ((((x)) >> L99PM72_CTRL5_PWM2_NDUTY_POS) & L99PM72_CTRL5_PWM2_NDUTY_MASK)



#define L99PM72_CTRL5_RESERVED_POS                                     15u
#define L99PM72_CTRL5_RESERVED_MASK                                    1u
#define L99PM72_CTRL5_RESERVED_SET(x)                                  ((((x)) & L99PM72_CTRL5_RESERVED_MASK) << L99PM72_CTRL5_RESERVED_POS)
#define L99PM72_CTRL5_RESERVED_GET(x)                                  ((((x)) >> L99PM72_CTRL5_RESERVED_POS) & L99PM72_CTRL5_RESERVED_MASK)






typedef struct
{
    uint8_t pwm3_duty;                                              
    uint8_t pwm4_nduty;                                             
}struct_L99PM72_CTRL6_REG_TYPE;



#define L99PM72_CTRL6_PWM3_DUTY_POS                                    0u
#define L99PM72_CTRL6_PWM3_DUTY_MASK                                   0x007F
#define L99PM72_CTRL6_PWM3_DUTY_SET(x)                                 ((((x)) & L99PM72_CTRL6_PWM3_DUTY_MASK) << L99PM72_CTRL6_PWM3_DUTY_POS)
#define L99PM72_CTRL6_PWM3_DUTY_GET(x)                                 ((((x)) >> L99PM72_CTRL6_PWM3_DUTY_POS) & L99PM72_CTRL6_PWM3_DUTY_MASK)


#define L99PM72_CTRL6_RESERVED_POS                                     7u
#define L99PM72_CTRL6_RESERVED_MASK                                    1u
#define L99PM72_CTRL6_RESERVED_SET(x)                                  ((((x)) & L99PM72_CTRL6_RESERVED_MASK) << L99PM72_CTRL6_RESERVED_POS)
#define L99PM72_CTRL6_RESERVED_GET(x)                                  ((((x)) >> L99PM72_CTRL6_RESERVED_POS) & L99PM72_CTRL6_RESERVED_MASK)


#define L99PM72_CTRL6_PWM4_NDUTY_POS                                   8u
#define L99PM72_CTRL6_PWM4_NDUTY_MASK                                  0x007F
#define L99PM72_CTRL6_PWM4_NDUTY_SET(x)                                ((((x)) & L99PM72_CTRL6_PWM4_NDUTY_MASK) << L99PM72_CTRL6_PWM4_NDUTY_POS)
#define L99PM72_CTRL6_PWM4_NDUTY_GET(x)                                ((((x)) >> L99PM72_CTRL6_PWM4_NDUTY_POS) & L99PM72_CTRL6_PWM4_NDUTY_MASK)



#define L99PM72_CTRL6_RESERVED2_POS                                    15u
#define L99PM72_CTRL6_RESERVED2_MASK                                   1u
#define L99PM72_CTRL6_RESERVED2_SET(x)                                 ((((x)) & L99PM72_CTRL6_RESERVED2_MASK) << L99PM72_CTRL6_RESERVED2_POS)
#define L99PM72_CTRL6_RESERVED2_GET(x)                                 ((((x)) >> L99PM72_CTRL6_RESERVED2_POS) & L99PM72_CTRL6_RESERVED2_MASK)






typedef struct
{
    uint8_t ext_can_id_7_0;                                         
    uint8_t ext_can_id_15_8;                                        
}struct_L99PM72_CTRL7_REG_TYPE;


#define L99PM72_CTRL7_EXT_CAN_ID_7_0_POS                               0u
#define L99PM72_CTRL7_EXT_CAN_ID_7_0_MASK                              0x00FF
#define L99PM72_CTRL7_EXT_CAN_ID_7_0_SET(x)                            ((((x)) & L99PM72_CTRL7_EXT_CAN_ID_7_0_MASK) << L99PM72_CTRL7_EXT_CAN_ID_7_0_POS)
#define L99PM72_CTRL7_EXT_CAN_ID_7_0_GET(x)                            ((((x)) >> L99PM72_CTRL7_EXT_CAN_ID_7_0_POS) & L99PM72_CTRL7_EXT_CAN_ID_7_0_MASK)

#define L99PM72_CTRL7_EXT_CAN_ID_15_8_POS                              8u
#define L99PM72_CTRL7_EXT_CAN_ID_15_8_MASK                             0x00FF
#define L99PM72_CTRL7_EXT_CAN_ID_15_8_SET(x)                           ((((x)) & L99PM72_CTRL7_EXT_CAN_ID_15_8_MASK) << L99PM72_CTRL7_EXT_CAN_ID_15_8_POS)
#define L99PM72_CTRL7_EXT_CAN_ID_15_8_GET(x)                           ((((x)) >> L99PM72_CTRL7_EXT_CAN_ID_15_8_POS) & L99PM72_CTRL7_EXT_CAN_ID_15_8_MASK)









typedef struct
{
    uint8_t ext_can_id_17_16;                                       
    uint8_t can_id_5_0__ext_can_id_23_18;                           
    uint8_t can_id_10_6__ext_can_id_28_24;                          
}struct_L99PM72_CTRL8_REG_TYPE;


#define L99PM72_CTRL8_EXT_CAN_ID_17_16_POS                             0u
#define L99PM72_CTRL8_EXT_CAN_ID_17_16_MASK                            3u
#define L99PM72_CTRL8_EXT_CAN_ID_17_16_SET(x)                          ((((x)) & L99PM72_CTRL8_EXT_CAN_ID_17_16_MASK) << L99PM72_CTRL8_EXT_CAN_ID_17_16_POS)
#define L99PM72_CTRL8_EXT_CAN_ID_17_16_GET(x)                          ((((x)) >> L99PM72_CTRL8_EXT_CAN_ID_17_16_POS) & L99PM72_CTRL8_EXT_CAN_ID_17_16_MASK)


#define L99PM72_CTRL8_CAN_ID_5_0__EXT_CAN_ID_23_18_POS                 2u
#define L99PM72_CTRL8_CAN_ID_5_0__EXT_CAN_ID_23_18_MASK                0x003F
#define L99PM72_CTRL8_CAN_ID_5_0__EXT_CAN_ID_23_18_SET(x)              ((((x)) & L99PM72_CTRL8_CAN_ID_5_0__EXT_CAN_ID_23_18_MASK) << L99PM72_CTRL8_CAN_ID_5_0__EXT_CAN_ID_23_18_POS)
#define L99PM72_CTRL8_CAN_ID_5_0__EXT_CAN_ID_23_18_GET(x)              ((((x)) >> L99PM72_CTRL8_CAN_ID_5_0__EXT_CAN_ID_23_18_POS) & L99PM72_CTRL8_CAN_ID_5_0__EXT_CAN_ID_23_18_MASK)


#define L99PM72_CTRL8_CAN_ID_10_6__EXT_CAN_ID_28_24_POS                8u
#define L99PM72_CTRL8_CAN_ID_10_6__EXT_CAN_ID_28_24_MASK               0x001F
#define L99PM72_CTRL8_CAN_ID_10_6__EXT_CAN_ID_28_24_SET(x)             ((((x)) & L99PM72_CTRL8_CAN_ID_10_6__EXT_CAN_ID_28_24_MASK) << L99PM72_CTRL8_CAN_ID_10_6__EXT_CAN_ID_28_24_POS)
#define L99PM72_CTRL8_CAN_ID_10_6__EXT_CAN_ID_28_24_GET(x)             ((((x)) >> L99PM72_CTRL8_CAN_ID_10_6__EXT_CAN_ID_28_24_POS) & L99PM72_CTRL8_CAN_ID_10_6__EXT_CAN_ID_28_24_MASK)


#define L99PM72_CTRL8_RESERVED_POS                                     13u
#define L99PM72_CTRL8_RESERVED_MASK                                    7u
#define L99PM72_CTRL8_RESERVED_SET(x)                                  ((((x)) & L99PM72_CTRL8_RESERVED_MASK) << L99PM72_CTRL8_RESERVED_POS)
#define L99PM72_CTRL8_RESERVED_GET(x)                                  ((((x)) >> L99PM72_CTRL8_RESERVED_POS) & L99PM72_CTRL8_RESERVED_MASK)








typedef enum
{
    L99PM72_CAN_ID_STANDARD = 0,                                            
    L99PM72_CAN_ID_EXTENDED,                                                
    L99PM72_CAN_ID_EXT_MAX
}enum_L99PM72_CAN_MESSAGE_FORMAT_TYPE;



typedef struct
{
    uint8_t can_dlc;                                                
    uint8_t can_id_ext;                                             
}struct_L99PM72_CTRL9_REG_TYPE;


#define L99PM72_CTRL9_CAN_DLC_POS                                      0u
#define L99PM72_CTRL9_CAN_DLC_MASK                                     0x000F
#define L99PM72_CTRL9_CAN_DLC_SET(x)                                   ((((x)) & L99PM72_CTRL9_CAN_DLC_MASK) << L99PM72_CTRL9_CAN_DLC_POS)
#define L99PM72_CTRL9_CAN_DLC_GET(x)                                   ((((x)) >> L99PM72_CTRL9_CAN_DLC_POS) & L99PM72_CTRL9_CAN_DLC_MASK)




#define L99PM72_CTRL9_CAN_ID_EXT_POS                                   4u
#define L99PM72_CTRL9_CAN_ID_EXT_MASK                                  1u
#define L99PM72_CTRL9_CAN_ID_EXT_SET(x)                                ((((x)) & L99PM72_CTRL9_CAN_ID_EXT_MASK) << L99PM72_CTRL9_CAN_ID_EXT_POS)
#define L99PM72_CTRL9_CAN_ID_EXT_GET(x)                                ((((x)) >> L99PM72_CTRL9_CAN_ID_EXT_POS) & L99PM72_CTRL9_CAN_ID_EXT_MASK)


#define L99PM72_CTRL9_RESERVED_POS                                     5u
#define L99PM72_CTRL9_RESERVED_MASK                                    7u
#define L99PM72_CTRL9_RESERVED_SET(x)                                  ((((x)) & L99PM72_CTRL9_RESERVED_MASK) << L99PM72_CTRL9_RESERVED_POS)
#define L99PM72_CTRL9_RESERVED_GET(x)                                  ((((x)) >> L99PM72_CTRL9_RESERVED_POS) & L99PM72_CTRL9_RESERVED_MASK)

#define L99PM72_CTRL9_RESERVED2_POS                                    8u
#define L99PM72_CTRL9_RESERVED2_MASK                                   0x00FF
#define L99PM72_CTRL9_RESERVED2_SET(x)                                 ((((x)) & L99PM72_CTRL9_RESERVED2_MASK) << L99PM72_CTRL9_RESERVED2_POS)
#define L99PM72_CTRL9_RESERVED2_GET(x)                                 ((((x)) >> L99PM72_CTRL9_RESERVED2_POS) & L99PM72_CTRL9_RESERVED2_MASK)







typedef struct
{
    uint8_t can_data1;                                              
    uint8_t can_data2;                                              
}struct_L99PM72_CTRL10_REG_TYPE;

#define L99PM72_CTRL10_CAN_DATA1_POS                                   0u
#define L99PM72_CTRL10_CAN_DATA1_MASK                                  0x00FF
#define L99PM72_CTRL10_CAN_DATA1_SET(x)                                ((((x)) & L99PM72_CTRL10_CAN_DATA1_MASK) << L99PM72_CTRL10_CAN_DATA1_POS)
#define L99PM72_CTRL10_CAN_DATA1_GET(x)                                ((((x)) >> L99PM72_CTRL10_CAN_DATA1_POS) & L99PM72_CTRL10_CAN_DATA1_MASK)

#define L99PM72_CTRL10_CAN_DATA2_POS                                   8u
#define L99PM72_CTRL10_CAN_DATA2_MASK                                  0x00FF
#define L99PM72_CTRL10_CAN_DATA2_SET(x)                                ((((x)) & L99PM72_CTRL10_CAN_DATA2_MASK) << L99PM72_CTRL10_CAN_DATA2_POS)
#define L99PM72_CTRL10_CAN_DATA2_GET(x)                                ((((x)) >> L99PM72_CTRL10_CAN_DATA2_POS) & L99PM72_CTRL10_CAN_DATA2_MASK)





typedef struct
{
    uint8_t can_data3;                                              
    uint8_t can_data4;                                              
}struct_L99PM72_CTRL11_REG_TYPE;


#define L99PM72_CTRL11_CAN_DATA3_POS                                   0u
#define L99PM72_CTRL11_CAN_DATA3_MASK                                  0x00FF
#define L99PM72_CTRL11_CAN_DATA3_SET(x)                                ((((x)) & L99PM72_CTRL11_CAN_DATA3_MASK) << L99PM72_CTRL11_CAN_DATA3_POS)
#define L99PM72_CTRL11_CAN_DATA3_GET(x)                                ((((x)) >> L99PM72_CTRL11_CAN_DATA3_POS) & L99PM72_CTRL11_CAN_DATA3_MASK)

#define L99PM72_CTRL11_CAN_DATA4_POS                                   8u
#define L99PM72_CTRL11_CAN_DATA4_MASK                                  0x00FF
#define L99PM72_CTRL11_CAN_DATA4_SET(x)                                ((((x)) & L99PM72_CTRL11_CAN_DATA4_MASK) << L99PM72_CTRL11_CAN_DATA4_POS)
#define L99PM72_CTRL11_CAN_DATA4_GET(x)                                ((((x)) >> L99PM72_CTRL11_CAN_DATA4_POS) & L99PM72_CTRL11_CAN_DATA4_MASK)





typedef struct
{
    uint8_t can_data5;                                              
    uint8_t can_data6;                                              
}struct_L99PM72_CTRL12_REG_TYPE;


#define L99PM72_CTRL12_CAN_DATA5_POS                                   0u
#define L99PM72_CTRL12_CAN_DATA5_MASK                                  0x00FF
#define L99PM72_CTRL12_CAN_DATA5_SET(x)                                ((((x)) & L99PM72_CTRL12_CAN_DATA5_MASK) << L99PM72_CTRL12_CAN_DATA5_POS)
#define L99PM72_CTRL12_CAN_DATA5_GET(x)                                ((((x)) >> L99PM72_CTRL12_CAN_DATA5_POS) & L99PM72_CTRL12_CAN_DATA5_MASK)

#define L99PM72_CTRL12_CAN_DATA6_POS                                   8u
#define L99PM72_CTRL12_CAN_DATA6_MASK                                  0x00FF
#define L99PM72_CTRL12_CAN_DATA6_SET(x)                                ((((x)) & L99PM72_CTRL12_CAN_DATA6_MASK) << L99PM72_CTRL12_CAN_DATA6_POS)
#define L99PM72_CTRL12_CAN_DATA6_GET(x)                                ((((x)) >> L99PM72_CTRL12_CAN_DATA6_POS) & L99PM72_CTRL12_CAN_DATA6_MASK)



typedef struct
{
    uint8_t can_data7;                                              
    uint8_t can_data8;                                              
}struct_L99PM72_CTRL13_REG_TYPE;



#define L99PM72_CTRL13_CAN_DATA7_POS                                   0u
#define L99PM72_CTRL13_CAN_DATA7_MASK                                  0x00FF
#define L99PM72_CTRL13_CAN_DATA7_SET(x)                                ((((x)) & L99PM72_CTRL13_CAN_DATA7_MASK) << L99PM72_CTRL13_CAN_DATA7_POS)
#define L99PM72_CTRL13_CAN_DATA7_GET(x)                                ((((x)) >> L99PM72_CTRL13_CAN_DATA7_POS) & L99PM72_CTRL13_CAN_DATA7_MASK)

#define L99PM72_CTRL13_CAN_DATA8_POS                                   8u
#define L99PM72_CTRL13_CAN_DATA8_MASK                                  0x00FF
#define L99PM72_CTRL13_CAN_DATA8_SET(x)                                ((((x)) & L99PM72_CTRL13_CAN_DATA8_MASK) << L99PM72_CTRL13_CAN_DATA8_POS)
#define L99PM72_CTRL13_CAN_DATA8_GET(x)                                ((((x)) >> L99PM72_CTRL13_CAN_DATA8_POS) & L99PM72_CTRL13_CAN_DATA8_MASK)





typedef struct
{
    uint8_t ext_can_mask_7_0;                                       
    uint8_t ext_can_mask_15_8;                                      
}struct_L99PM72_CTRL14_REG_TYPE;



#define L99PM72_CTRL14_EXT_CAN_MASK_7_0_POS                            0u
#define L99PM72_CTRL14_EXT_CAN_MASK_7_0_MASK                           0x00FF
#define L99PM72_CTRL14_EXT_CAN_MASK_7_0_SET(x)                         ((((x)) & L99PM72_CTRL14_EXT_CAN_MASK_7_0_MASK) << L99PM72_CTRL14_EXT_CAN_MASK_7_0_POS)
#define L99PM72_CTRL14_EXT_CAN_MASK_7_0_GET(x)                         ((((x)) >> L99PM72_CTRL14_EXT_CAN_MASK_7_0_POS) & L99PM72_CTRL14_EXT_CAN_MASK_7_0_MASK)

#define L99PM72_CTRL14_EXT_CAN_MASK_15_8_POS                           8u
#define L99PM72_CTRL14_EXT_CAN_MASK_15_8_MASK                          0x00FF
#define L99PM72_CTRL14_EXT_CAN_MASK_15_8_SET(x)                        ((((x)) & L99PM72_CTRL14_EXT_CAN_MASK_15_8_MASK) << L99PM72_CTRL14_EXT_CAN_MASK_15_8_POS)
#define L99PM72_CTRL14_EXT_CAN_MASK_15_8_GET(x)                        ((((x)) >> L99PM72_CTRL14_EXT_CAN_MASK_15_8_POS) & L99PM72_CTRL14_EXT_CAN_MASK_15_8_MASK)







typedef struct
{
    uint8_t ext_can_mask17_16;                                      
    uint8_t can_mask_5_0__ext_can_mask_23_18;                       
    uint8_t can_mask_10_6__ext_can_mask_28_24;                      
}struct_L99PM72_CTRL15_REG_TYPE;


#define L99PM72_CTRL15_EXT_CAN_MASK_17_16_POS                          0u
#define L99PM72_CTRL15_EXT_CAN_MASK_17_16_MASK                         3u
#define L99PM72_CTRL15_EXT_CAN_MASK_17_16_SET(x)                       ((((x)) & L99PM72_CTRL15_EXT_CAN_MASK_17_16_MASK) << L99PM72_CTRL15_EXT_CAN_MASK_17_16_POS)
#define L99PM72_CTRL15_EXT_CAN_MASK_17_16_GET(x)                       ((((x)) >> L99PM72_CTRL15_EXT_CAN_MASK_17_16_POS) & L99PM72_CTRL15_EXT_CAN_MASK_17_16_MASK)


#define L99PM72_CTRL15_CAN_MASK_5_0__EXT_CAN_MASK_23_18_POS            2u
#define L99PM72_CTRL15_CAN_MASK_5_0__EXT_CAN_MASK_23_18_MASK           0x003F
#define L99PM72_CTRL15_CAN_MASK_5_0__EXT_CAN_MASK_23_18_SET(x)         ((((x)) & L99PM72_CTRL15_CAN_MASK_5_0__EXT_CAN_MASK_23_18_MASK) << L99PM72_CTRL15_CAN_MASK_5_0__EXT_CAN_MASK_23_18_POS)
#define L99PM72_CTRL15_CAN_MASK_5_0__EXT_CAN_MASK_23_18_GET(x)         ((((x)) >> L99PM72_CTRL15_CAN_MASK_5_0__EXT_CAN_MASK_23_18_POS) & L99PM72_CTRL15_CAN_MASK_5_0__EXT_CAN_MASK_23_18_MASK)


#define L99PM72_CTRL15_CAN_MASK_10_6__EXT_CAN_MASK_28_24_POS           8u
#define L99PM72_CTRL15_CAN_MASK_10_6__EXT_CAN_MASK_28_24_MASK          0x001F
#define L99PM72_CTRL15_CAN_MASK_10_6__EXT_CAN_MASK_28_24_SET(x)        ((((x)) & L99PM72_CTRL15_CAN_MASK_10_6__EXT_CAN_MASK_28_24_MASK) << L99PM72_CTRL15_CAN_MASK_10_6__EXT_CAN_MASK_28_24_POS)
#define L99PM72_CTRL15_CAN_MASK_10_6__EXT_CAN_MASK_28_24_GET(x)        ((((x)) >> L99PM72_CTRL15_CAN_MASK_10_6__EXT_CAN_MASK_28_24_POS) & L99PM72_CTRL15_CAN_MASK_10_6__EXT_CAN_MASK_28_24_MASK)

#define L99PM72_CTRL15_RESERVED_POS                                    13u
#define L99PM72_CTRL15_RESERVED_MASK                                   7u
#define L99PM72_CTRL15_RESERVED_SET(x)                                 ((((x)) & L99PM72_CTRL15_RESERVED_MASK) << L99PM72_CTRL15_RESERVED_POS)
#define L99PM72_CTRL15_RESERVED_GET(x)                                 ((((x)) >> L99PM72_CTRL15_RESERVED_POS) & L99PM72_CTRL15_RESERVED_MASK)







typedef enum
{
    L99PM72_CAN_SP_71P = 0,                                                 
    L99PM72_CAN_SP_73P,                                                     
    L99PM72_CAN_SP_75P,                                                     
    L99PM72_CAN_SP_77P,                                                     
    L99PM72_CAN_SP_79P,                                                     
    L99PM72_CAN_SP_81P,                                                     
    L99PM72_CAN_SP_83P,                                                     
    L99PM72_CAN_SP_85P,                                                     
    L99PM72_CAN_SP_MAX
}enum_L99PM72_CAN_SAMPLE_POINT_TYPE;

typedef enum
{
    L99PM72_SEL_CAN_BAUDRATE_500K = 0,                                          
    L99PM72_SEL_CAN_BAUDRATE_250K,                                              
    L99PM72_SEL_CAN_BAUDRATE_STOPPED,                                           
    L99PM72_SEL_CAN_BAUDRATE_125K,                                              
    L99PM72_SEL_CAN_BAUDRATE_MAX
}enum_L99PM72_CAN_BAUDRATES_TYPE;

typedef enum
{
    L99PM72_SELECTIVE_WAKE_DISABLED = 0,                                    
    L99PM72_SELECTIVE_WAKE_ENABLED,                                         
    L99PM72_SELECTIVE_WAKE_MAX
}enum_L99PM72_SELECTIVE_WAKEUP_STATUS_TYPE;



typedef struct
{
    uint8_t sw_en;                                                  
    uint8_t baudrate;                                               
    uint8_t sample;                                                 
    uint8_t cr16_10;                                                
    uint8_t cr16_11;                                                
    uint8_t cr16_12;                                                
    uint8_t cr16_13;                                                
    uint8_t cr16_14;                                                
    uint8_t cr16_20;                                                
    uint8_t cr16_21;                                                
    uint8_t cr16_30;                                                
}struct_L99PM72_CTRL16_REG_TYPE;


#define L99PM72_CTRL16_SW_EN_POS                                       0u
#define L99PM72_CTRL16_SW_EN_MASK                                      1u
#define L99PM72_CTRL16_SW_EN_SET(x)                                    ((((x)) & L99PM72_CTRL16_SW_EN_MASK) << L99PM72_CTRL16_SW_EN_POS)
#define L99PM72_CTRL16_SW_EN_GET(x)                                    ((((x)) >> L99PM72_CTRL16_SW_EN_POS) & L99PM72_CTRL16_SW_EN_MASK)


#define L99PM72_CTRL16_BAUDRATE_POS                                    1u
#define L99PM72_CTRL16_BAUDRATE_MASK                                   3u
#define L99PM72_CTRL16_BAUDRATE_SET(x)                                 ((((x)) & L99PM72_CTRL16_BAUDRATE_MASK) << L99PM72_CTRL16_BAUDRATE_POS)
#define L99PM72_CTRL16_BAUDRATE_GET(x)                                 ((((x)) >> L99PM72_CTRL16_BAUDRATE_POS) & L99PM72_CTRL16_BAUDRATE_MASK)


#define L99PM72_CTRL16_RESERVED_POS                                    3u
#define L99PM72_CTRL16_RESERVED_MASK                                   1u
#define L99PM72_CTRL16_RESERVED_SET(x)                                 ((((x)) & L99PM72_CTRL16_RESERVED_MASK) << L99PM72_CTRL16_RESERVED_POS)
#define L99PM72_CTRL16_RESERVED_GET(x)                                 ((((x)) >> L99PM72_CTRL16_RESERVED_POS) & L99PM72_CTRL16_RESERVED_MASK)


#define L99PM72_CTRL16_SAMPLE_POS                                      4u
#define L99PM72_CTRL16_SAMPLE_MASK                                     7u
#define L99PM72_CTRL16_SAMPLE_SET(x)                                   ((((x)) & L99PM72_CTRL16_SAMPLE_MASK) << L99PM72_CTRL16_SAMPLE_POS)
#define L99PM72_CTRL16_SAMPLE_GET(x)                                   ((((x)) >> L99PM72_CTRL16_SAMPLE_POS) & L99PM72_CTRL16_SAMPLE_MASK)


#define L99PM72_CTRL16_RESERVED2_POS                                   7u
#define L99PM72_CTRL16_RESERVED2_MASK                                  1u
#define L99PM72_CTRL16_RESERVED2_SET(x)                                ((((x)) & L99PM72_CTRL16_RESERVED2_MASK) << L99PM72_CTRL16_RESERVED2_POS)
#define L99PM72_CTRL16_RESERVED2_GET(x)                                ((((x)) >> L99PM72_CTRL16_RESERVED2_POS) & L99PM72_CTRL16_RESERVED2_MASK)


#define L99PM72_CTRL16_CR16_10_POS                                     8u
#define L99PM72_CTRL16_CR16_10_MASK                                    1u
#define L99PM72_CTRL16_CR16_10_SET(x)                                  ((((x)) & L99PM72_CTRL16_CR16_10_MASK) << L99PM72_CTRL16_CR16_10_POS)
#define L99PM72_CTRL16_CR16_10_GET(x)                                  ((((x)) >> L99PM72_CTRL16_CR16_10_POS) & L99PM72_CTRL16_CR16_10_MASK)


#define L99PM72_CTRL16_CR16_11_POS                                     9u
#define L99PM72_CTRL16_CR16_11_MASK                                    1u
#define L99PM72_CTRL16_CR16_11_SET(x)                                  ((((x)) & L99PM72_CTRL16_CR16_11_MASK) << L99PM72_CTRL16_CR16_11_POS)
#define L99PM72_CTRL16_CR16_11_GET(x)                                  ((((x)) >> L99PM72_CTRL16_CR16_11_POS) & L99PM72_CTRL16_CR16_11_MASK)


#define L99PM72_CTRL16_CR16_12_POS                                     10u
#define L99PM72_CTRL16_CR16_12_MASK                                    1u
#define L99PM72_CTRL16_CR16_12_SET(x)                                  ((((x)) & L99PM72_CTRL16_CR16_12_MASK) << L99PM72_CTRL16_CR16_12_POS)
#define L99PM72_CTRL16_CR16_12_GET(x)                                  ((((x)) >> L99PM72_CTRL16_CR16_12_POS) & L99PM72_CTRL16_CR16_12_MASK)


#define L99PM72_CTRL16_CR16_13_POS                                     11u
#define L99PM72_CTRL16_CR16_13_MASK                                    1u
#define L99PM72_CTRL16_CR16_13_SET(x)                                  ((((x)) & L99PM72_CTRL16_CR16_13_MASK) << L99PM72_CTRL16_CR16_13_POS)
#define L99PM72_CTRL16_CR16_13_GET(x)                                  ((((x)) >> L99PM72_CTRL16_CR16_13_POS) & L99PM72_CTRL16_CR16_13_MASK)


#define L99PM72_CTRL16_CR16_14_POS                                     12u
#define L99PM72_CTRL16_CR16_14_MASK                                    1u
#define L99PM72_CTRL16_CR16_14_SET(x)                                  ((((x)) & L99PM72_CTRL16_CR16_14_MASK) << L99PM72_CTRL16_CR16_14_POS)
#define L99PM72_CTRL16_CR16_14_GET(x)                                  ((((x)) >> L99PM72_CTRL16_CR16_14_POS) & L99PM72_CTRL16_CR16_14_MASK)


#define L99PM72_CTRL16_CR16_20_POS                                     13u
#define L99PM72_CTRL16_CR16_20_MASK                                    1u
#define L99PM72_CTRL16_CR16_20_SET(x)                                  ((((x)) & L99PM72_CTRL16_CR16_20_MASK) << L99PM72_CTRL16_CR16_20_POS)
#define L99PM72_CTRL16_CR16_20_GET(x)                                  ((((x)) >> L99PM72_CTRL16_CR16_20_POS) & L99PM72_CTRL16_CR16_20_MASK)


#define L99PM72_CTRL16_CR16_21_POS                                     14u
#define L99PM72_CTRL16_CR16_21_MASK                                    1u
#define L99PM72_CTRL16_CR16_21_SET(x)                                  ((((x)) & L99PM72_CTRL16_CR16_21_MASK) << L99PM72_CTRL16_CR16_21_POS)
#define L99PM72_CTRL16_CR16_21_GET(x)                                  ((((x)) >> L99PM72_CTRL16_CR16_21_POS) & L99PM72_CTRL16_CR16_21_MASK)


#define L99PM72_CTRL16_CR16_30_POS                                     15u
#define L99PM72_CTRL16_CR16_30_MASK                                    1u
#define L99PM72_CTRL16_CR16_30_SET(x)                                  ((((x)) & L99PM72_CTRL16_CR16_30_MASK) << L99PM72_CTRL16_CR16_30_POS)
#define L99PM72_CTRL16_CR16_30_GET(x)                                  ((((x)) >> L99PM72_CTRL16_CR16_30_POS) & L99PM72_CTRL16_CR16_30_MASK)





typedef enum
{
    L99PM72_WATCHDOG_DISABLED = 0,                                          
    L99PM72_WATCHDOG_ENABLED,                                               
    L99PM72_WATCHDOG_MAX
}enum_L99PM72_WATCHDOG_STATE_TYPE;



typedef struct
{
    uint8_t watchdog_enable;                                        
}struct_L99PM72_CTRL34_REG_TYPE;



#define L99PM72_CTRL34_WATCHDOG_EN_POS                                 0u
#define L99PM72_CTRL34_WATCHDOG_EN_MASK                                1u
#define L99PM72_CTRL34_WATCHDOG_EN_SET(x)                              ((((x)) & L99PM72_CTRL34_WATCHDOG_EN_MASK) << L99PM72_CTRL34_WATCHDOG_EN_POS)
#define L99PM72_CTRL34_WATCHDOG_EN_GET(x)                              ((((x)) >> L99PM72_CTRL34_WATCHDOG_EN_POS) & L99PM72_CTRL34_WATCHDOG_EN_MASK)


#define L99PM72_CTRL34_RESERVED_POS                                    1u
#define L99PM72_CTRL34_RESERVED_MASK                                   0x007F
#define L99PM72_CTRL34_RESERVED_SET(x)                                 ((((x)) & L99PM72_CTRL34_RESERVED_MASK) << L99PM72_CTRL34_RESERVED_POS)
#define L99PM72_CTRL34_RESERVED_GET(x)                                 ((((x)) >> L99PM72_CTRL34_RESERVED_POS) & L99PM72_CTRL34_RESERVED_MASK)


#define L99PM72_CTRL34_RESERVED2_POS                                   8u
#define L99PM72_CTRL34_RESERVED2_MASK                                  0x00FF
#define L99PM72_CTRL34_RESERVED2_SET(x)                                ((((x)) & L99PM72_CTRL34_RESERVED2_MASK) << L99PM72_CTRL34_RESERVED2_POS)
#define L99PM72_CTRL34_RESERVED2_GET(x)                                ((((x)) >> L99PM72_CTRL34_RESERVED2_POS) & L99PM72_CTRL34_RESERVED2_MASK)







typedef struct
{
    uint8_t cr35_10;                                                
    uint8_t cr35_20;                                                
    uint8_t cr35_21;                                                
    uint8_t cr35_22;                                                
    uint8_t cr35_23;                                                
    uint8_t cr35_24;                                                
    uint8_t cr35_25;                                                
}struct_L99PM72_CTRL35_REG_TYPE;


#define L99PM72_CTRL35_CR35_10_POS                                     0u
#define L99PM72_CTRL35_CR35_10_MASK                                    1u
#define L99PM72_CTRL35_CR35_10_SET(x)                                  ((((x)) & L99PM72_CTRL35_CR35_10_MASK) << L99PM72_CTRL35_CR35_10_POS)
#define L99PM72_CTRL35_CR35_10_GET(x)                                  ((((x)) >> L99PM72_CTRL35_CR35_10_POS) & L99PM72_CTRL35_CR35_10_MASK)


#define L99PM72_CTRL35_CR35_20_POS                                     1u
#define L99PM72_CTRL35_CR35_20_MASK                                    1u
#define L99PM72_CTRL35_CR35_20_SET(x)                                  ((((x)) & L99PM72_CTRL35_CR35_20_MASK) << L99PM72_CTRL35_CR35_20_POS)
#define L99PM72_CTRL35_CR35_20_GET(x)                                  ((((x)) >> L99PM72_CTRL35_CR35_20_POS) & L99PM72_CTRL35_CR35_20_MASK)


#define L99PM72_CTRL35_CR35_21_POS                                     2u
#define L99PM72_CTRL35_CR35_21_MASK                                    1u
#define L99PM72_CTRL35_CR35_21_SET(x)                                  ((((x)) & L99PM72_CTRL35_CR35_21_MASK) << L99PM72_CTRL35_CR35_21_POS)
#define L99PM72_CTRL35_CR35_21_GET(x)                                  ((((x)) >> L99PM72_CTRL35_CR35_21_POS) & L99PM72_CTRL35_CR35_21_MASK)

#define L99PM72_CTRL35_CR35_22_POS                                     3u
#define L99PM72_CTRL35_CR35_22_MASK                                    1u
#define L99PM72_CTRL35_CR35_22_SET(x)                                  ((((x)) & L99PM72_CTRL35_CR35_22_MASK) << L99PM72_CTRL35_CR35_22_POS)
#define L99PM72_CTRL35_CR35_22_GET(x)                                  ((((x)) >> L99PM72_CTRL35_CR35_22_POS) & L99PM72_CTRL35_CR35_22_MASK)

#define L99PM72_CTRL35_CR35_23_POS                                     4u
#define L99PM72_CTRL35_CR35_23_MASK                                    1u
#define L99PM72_CTRL35_CR35_23_SET(x)                                  ((((x)) & L99PM72_CTRL35_CR35_23_MASK) << L99PM72_CTRL35_CR35_23_POS)
#define L99PM72_CTRL35_CR35_23_GET(x)                                  ((((x)) >> L99PM72_CTRL35_CR35_23_POS) & L99PM72_CTRL35_CR35_23_MASK)

#define L99PM72_CTRL35_CR35_24_POS                                     5u
#define L99PM72_CTRL35_CR35_24_MASK                                    1u
#define L99PM72_CTRL35_CR35_24_SET(x)                                  ((((x)) & L99PM72_CTRL35_CR35_24_MASK) << L99PM72_CTRL35_CR35_24_POS)
#define L99PM72_CTRL35_CR35_24_GET(x)                                  ((((x)) >> L99PM72_CTRL35_CR35_24_POS) & L99PM72_CTRL35_CR35_24_MASK)

#define L99PM72_CTRL35_CR35_25_POS                                     6u
#define L99PM72_CTRL35_CR35_25_MASK                                    1u
#define L99PM72_CTRL35_CR35_25_SET(x)                                  ((((x)) & L99PM72_CTRL35_CR35_25_MASK) << L99PM72_CTRL35_CR35_25_POS)
#define L99PM72_CTRL35_CR35_25_GET(x)                                  ((((x)) >> L99PM72_CTRL35_CR35_25_POS) & L99PM72_CTRL35_CR35_25_MASK)

#define L99PM72_CTRL35_CR35_RESERVED_POS                               7u
#define L99PM72_CTRL35_CR35_RESERVED_MASK                              1u
#define L99PM72_CTRL35_CR35_RESERVED_SET(x)                            ((((x)) & L99PM72_CTRL35_CR35_RESERVED_MASK) << L99PM72_CTRL35_CR35_RESERVED_POS)
#define L99PM72_CTRL35_CR35_RESERVED_GET(x)                            ((((x)) >> L99PM72_CTRL35_CR35_RESERVED_POS) & L99PM72_CTRL35_CR35_RESERVED_MASK)

#define L99PM72_CTRL35_CR35_RESERVED2_POS                              8u
#define L99PM72_CTRL35_CR35_RESERVED2_MASK                             0x00FF
#define L99PM72_CTRL35_CR35_RESERVED2_SET(x)                           ((((x)) & L99PM72_CTRL35_CR35_RESERVED2_MASK) << L99PM72_CTRL35_CR35_RESERVED2_POS)
#define L99PM72_CTRL35_CR35_RESERVED2_GET(x)                           ((((x)) >> L99PM72_CTRL35_CR35_RESERVED2_POS) & L99PM72_CTRL35_CR35_RESERVED2_MASK)




typedef struct
{
    uint8_t trig;                                                   
}struct_L99PM72_CFG_REG_TYPE;

#define L99PM72_CFG_TRIG_POS                                           0u
#define L99PM72_CFG_TRIG_MASK                                          1u
#define L99PM72_CFG_TRIG_SET(x)                                        ((((x)) & L99PM72_CFG_TRIG_MASK) << L99PM72_CFG_TRIG_POS)
#define L99PM72_CFG_TRIG_GET(x)                                        ((((x)) >> L99PM72_CFG_TRIG_POS) & L99PM72_CFG_TRIG_MASK)

#define L99PM72_CFG_RESERVED_POS                                       1u
#define L99PM72_CFG_RESERVED_MASK                                      0x007F
#define L99PM72_CFG_RESERVED_SET(x)                                    ((((x)) & L99PM72_CFG_RESERVED_MASK) << L99PM72_CFG_RESERVED_POS)
#define L99PM72_CFG_RESERVED_GET(x)                                    ((((x)) >> L99PM72_CFG_RESERVED_POS) & L99PM72_CFG_RESERVED_MASK)

#define L99PM72_CFG_RESERVED2_POS                                      8u
#define L99PM72_CFG_RESERVED2_MASK                                     0x00FF
#define L99PM72_CFG_RESERVED2_SET(x)                                   ((((x)) & L99PM72_CFG_RESERVED2_MASK) << L99PM72_CFG_RESERVED2_POS)
#define L99PM72_CFG_RESERVED2_GET(x)                                   ((((x)) >> L99PM72_CFG_RESERVED2_POS) & L99PM72_CFG_RESERVED2_MASK)




typedef enum
{
    L99PM72_OPEN_LOAD_NOT_DETECTED = 0,                                     
    L99PM72_OPEN_LOAD_DETECTED,                                             
    L99PM72_OPEN_LOAD_MAX
}enum_L99PM72_OPEN_LOAD_STATUS_TYPE;

typedef enum
{
    L99PM72_UNDERVOLTAGE_EVENT_NOT_DETECTED = 0,                            
    L99PM72_UNDERVOLTAGE_EVENT_DETECTED,                                    
    L99PM72_UNDERVOLTAGE_EVENT_MAX
}enum_L99PM72_UNDERVOLTAGE_EVENT_TYPE;


typedef enum
{                       
    L99PM72_V2_FAILED_EVENT_NOT_DETECTED = 0,                               
    L99PM72_V2_FAILED_EVENT_DETECTED,                                       
    L99PM72_V2_FAILED_EVENT_MAX
}enum_L99PM72_V2_FAILED_STATUS_TYPE;


typedef enum
{
    L99PM72_V2_SHORTCUT_EVENT_NOT_DETECTED = 0,                             
    L99PM72_V2_SHORTCUT_EVENT_DETECTED,                                     
    L99PM72_V2_SHORTCUT_EVENT_MAX
}enum_L99PM72_V2_SHORTCUT_STATUS_TYPE;

typedef enum
{
    L99PM72_OVERVOLTAGE_EVENT_NOT_DETECTED = 0,                             
    L99PM72_OVERVOLTAGE_EVENT_DETECTED,                                     
    L99PM72_OVERVOLTAGE_EVENT_MAX
}enum_L99PM72_OVERVOLTAGE_EVENT_TYPE;



typedef enum
{
    L99PM72_OVERCURRENT_EVENT_NOT_DETECTED = 0,                             
    L99PM72_OVERCURRENT_EVENT_DETECTED,                                     
    L99PM72_OVERCURRENT_EVENT_MAX
}enum_L99PM72_OUTPUT_OVERCURRENT_EVENT_TYPE;




typedef struct
{
    uint8_t err_flg;                                                
    uint8_t overcurrent_rel1;                                       
    uint8_t overcurrent_rel2;                                       
    uint8_t overcurrent_out1;                                       
    uint8_t overcurrent_out2;                                       
    uint8_t overcurrent_out3;                                       
    uint8_t overcurrent_out4;                                       
    uint8_t overcurrent_hs;                                         
    uint8_t ov;                                                     
    uint8_t v2_short;                                               
    uint8_t v2_fail;                                                
    uint8_t uv;                                                     
    uint8_t openload_out1;                                          
    uint8_t openload_out2;                                          
    uint8_t openload_out3;                                          
    uint8_t openload_out4;                                          
    uint8_t openload_hs;                                            

    }struct_L99PM72_ST1_REG_TYPE;


#define L99PM72_ST1_OC_REL1_POS                                        0u
#define L99PM72_ST1_OC_REL1_MASK                                       1u
#define L99PM72_ST1_OC_REL1_GET(x)                                     ((((x)) >> L99PM72_ST1_OC_REL1_POS) & L99PM72_ST1_OC_REL1_MASK)

#define L99PM72_ST1_OC_REL2_POS                                        1u
#define L99PM72_ST1_OC_REL2_MASK                                       1u
#define L99PM72_ST1_OC_REL2_GET(x)                                     ((((x)) >> L99PM72_ST1_OC_REL2_POS) & L99PM72_ST1_OC_REL2_MASK)

#define L99PM72_ST1_OC_OUT1_POS                                        2u
#define L99PM72_ST1_OC_OUT1_MASK                                       1u
#define L99PM72_ST1_OC_OUT1_GET(x)                                     ((((x)) >> L99PM72_ST1_OC_OUT1_POS) & L99PM72_ST1_OC_OUT1_MASK)

#define L99PM72_ST1_OC_OUT2_POS                                        3u
#define L99PM72_ST1_OC_OUT2_MASK                                       1u
#define L99PM72_ST1_OC_OUT2_GET(x)                                     ((((x)) >> L99PM72_ST1_OC_OUT2_POS) & L99PM72_ST1_OC_OUT2_MASK)

#define L99PM72_ST1_OC_OUT3_POS                                        4u
#define L99PM72_ST1_OC_OUT3_MASK                                       1u
#define L99PM72_ST1_OC_OUT3_GET(x)                                     ((((x)) >> L99PM72_ST1_OC_OUT3_POS) & L99PM72_ST1_OC_OUT3_MASK)

#define L99PM72_ST1_OC_OUT4_POS                                        5u
#define L99PM72_ST1_OC_OUT4_MASK                                       1u
#define L99PM72_ST1_OC_OUT4_GET(x)                                     ((((x)) >> L99PM72_ST1_OC_OUT4_POS) & L99PM72_ST1_OC_OUT4_MASK)

#define L99PM72_ST1_OC_HS_POS                                          6u
#define L99PM72_ST1_OC_HS_MASK                                         1u
#define L99PM72_ST1_OC_HS_GET(x)                                       ((((x)) >> L99PM72_ST1_OC_HS_POS) & L99PM72_ST1_OC_HS_MASK)

#define L99PM72_ST1_OV_POS                                             7u
#define L99PM72_ST1_OV_MASK                                            1u
#define L99PM72_ST1_OV_GET(x)                                          ((((x)) >> L99PM72_ST1_OV_POS) & L99PM72_ST1_OV_MASK)


#define L99PM72_ST1_V2_SHORT_POS                                       8u
#define L99PM72_ST1_V2_SHORT_MASK                                      1u
#define L99PM72_ST1_V2_SHORT_GET(x)                                    ((((x)) >> L99PM72_ST1_V2_SHORT_POS) & L99PM72_ST1_V2_SHORT_MASK)

#define L99PM72_ST1_V2_FAIL_POS                                        9u
#define L99PM72_ST1_V2_FAIL_MASK                                       1u
#define L99PM72_ST1_V2_FAIL_GET(x)                                     ((((x)) >> L99PM72_ST1_V2_FAIL_POS) & L99PM72_ST1_V2_FAIL_MASK)

#define L99PM72_ST1_UV_POS                                             10u
#define L99PM72_ST1_UV_MASK                                            1u
#define L99PM72_ST1_UV_GET(x)                                          ((((x)) >> L99PM72_ST1_UV_POS) & L99PM72_ST1_UV_MASK)

#define L99PM72_ST1_OL_OUT1_POS                                        11u
#define L99PM72_ST1_OL_OUT1_MASK                                       1u
#define L99PM72_ST1_OL_OUT1_GET(x)                                     ((((x)) >> L99PM72_ST1_OL_OUT1_POS) & L99PM72_ST1_OL_OUT1_MASK)

#define L99PM72_ST1_OL_OUT2_POS                                        12u
#define L99PM72_ST1_OL_OUT2_MASK                                       1u
#define L99PM72_ST1_OL_OUT2_GET(x)                                     ((((x)) >> L99PM72_ST1_OL_OUT2_POS) & L99PM72_ST1_OL_OUT2_MASK)

#define L99PM72_ST1_OL_OUT3_POS                                        13u
#define L99PM72_ST1_OL_OUT3_MASK                                       1u
#define L99PM72_ST1_OL_OUT3_GET(x)                                     ((((x)) >> L99PM72_ST1_OL_OUT3_POS) & L99PM72_ST1_OL_OUT3_MASK)

#define L99PM72_ST1_OL_OUT4_POS                                        14u
#define L99PM72_ST1_OL_OUT4_MASK                                       1u
#define L99PM72_ST1_OL_OUT4_GET(x)                                     ((((x)) >> L99PM72_ST1_OL_OUT4_POS) & L99PM72_ST1_OL_OUT4_MASK)

#define L99PM72_ST1_OL_HS_POS                                          15u
#define L99PM72_ST1_OL_HS_MASK                                         1u
#define L99PM72_ST1_OL_HS_GET(x)                                       ((((x)) >> L99PM72_ST1_OL_HS_POS) & L99PM72_ST1_OL_HS_MASK)




typedef enum
{
    L99PM72_ACTUAL_WAKEUP_STATE_OFF = 0,                                    
    L99PM72_ACTUAL_WAKEUP_STATE_ON,                                         
    L99PM72_ACTUAL_WAKEUP_STATE_MAX
}enum_L99PM72_ACTUAL_WAKEUP_STATE_TYPE;


typedef enum
{
    L99PM72_WAKEUP_EVENT_NOT_DETECTED = 0,                                  
    L99PM72_WAKEUP_EVENT_DETECTED,                                          
    L99PM72_WAKEUP_EVENT_MAX
}enum_L99PM72_WAKEUP_EVENT_STATE_TYPE;




typedef struct
{
    uint8_t err_flg;                                                
    uint8_t can_tx_perm_dom;                                        
    uint8_t can_perm_dom;                                           
    uint8_t can_perm_rec;                                           
    uint8_t can_rx_perm_rec;                                        
    uint8_t lin_perm_rec;                                           
    uint8_t lin_tx_perm_dom;                                        
    uint8_t lin_perm_dom;                                           
    uint8_t wake_timer;                                             
    uint8_t wake_lin;                                               
    uint8_t wake_can;                                               
    uint8_t wu1_wake;                                               
    uint8_t wu2_wake;                                               
    uint8_t wu3_wake;                                               
    uint8_t wu1_state;                                              
    uint8_t wu2_state;                                              
    uint8_t wu3_state;                                              
}struct_L99PM72_ST2_REG_TYPE;



#define L99PM72_ST2_CAN_TX_PERM_DOM_POS                                0u
#define L99PM72_ST2_CAN_TX_PERM_DOM_MASK                               1u
#define L99PM72_ST2_CAN_TX_PERM_DOM_GET(x)                             ((((x)) >> L99PM72_ST2_CAN_TX_PERM_DOM_POS) & L99PM72_ST2_CAN_TX_PERM_DOM_MASK)


#define L99PM72_ST2_CAN_PERM_DOM_POS                                   1u
#define L99PM72_ST2_CAN_PERM_DOM_MASK                                  1u
#define L99PM72_ST2_CAN_PERM_DOM_GET(x)                                ((((x)) >> L99PM72_ST2_CAN_PERM_DOM_POS) & L99PM72_ST2_CAN_PERM_DOM_MASK)


#define L99PM72_ST2_CAN_PERM_REC_POS                                   2u
#define L99PM72_ST2_CAN_PERM_REC_MASK                                  1u
#define L99PM72_ST2_CAN_PERM_REC_GET(x)                                ((((x)) >> L99PM72_ST2_CAN_PERM_REC_POS) & L99PM72_ST2_CAN_PERM_REC_MASK)


#define L99PM72_ST2_CAN_RX_PERM_REC_POS                                3u
#define L99PM72_ST2_CAN_RX_PERM_REC_MASK                               1u
#define L99PM72_ST2_CAN_RX_PERM_REC_GET(x)                             ((((x)) >> L99PM72_ST2_CAN_RX_PERM_REC_POS) & L99PM72_ST2_CAN_RX_PERM_REC_MASK)

#define L99PM72_ST2_LIN_PERM_REC_POS                                   4u
#define L99PM72_ST2_LIN_PERM_REC_MASK                                  1u
#define L99PM72_ST2_LIN_PERM_REC_GET(x)                                ((((x)) >> L99PM72_ST2_LIN_PERM_REC_POS) & L99PM72_ST2_LIN_PERM_REC_MASK)

#define L99PM72_ST2_LIN_TX_PERM_DOM_POS                                5u
#define L99PM72_ST2_LIN_TX_PERM_DOM_MASK                               1u
#define L99PM72_ST2_LIN_TX_PERM_DOM_GET(x)                             ((((x)) >> L99PM72_ST2_LIN_TX_PERM_DOM_POS) & L99PM72_ST2_LIN_TX_PERM_DOM_MASK)

#define L99PM72_ST2_LIN_PERM_DOM_POS                                   6u
#define L99PM72_ST2_LIN_PERM_DOM_MASK                                  1u
#define L99PM72_ST2_LIN_PERM_DOM_GET(x)                                ((((x)) >> L99PM72_ST2_LIN_PERM_DOM_POS) & L99PM72_ST2_LIN_PERM_DOM_MASK)

#define L99PM72_ST2_WAKE_TIMER_POS                                     7u
#define L99PM72_ST2_WAKE_TIMER_MASK                                    1u
#define L99PM72_ST2_WAKE_TIMER_GET(x)                                  ((((x)) >> L99PM72_ST2_WAKE_TIMER_POS) & L99PM72_ST2_WAKE_TIMER_MASK)

#define L99PM72_ST2_WAKE_LIN_POS                                       8u
#define L99PM72_ST2_WAKE_LIN_MASK                                      1u
#define L99PM72_ST2_WAKE_LIN_GET(x)                                    ((((x)) >> L99PM72_ST2_WAKE_LIN_POS) & L99PM72_ST2_WAKE_LIN_MASK)

#define L99PM72_ST2_WAKE_CAN_POS                                       9u
#define L99PM72_ST2_WAKE_CAN_MASK                                      1u
#define L99PM72_ST2_WAKE_CAN_GET(x)                                    ((((x)) >> L99PM72_ST2_WAKE_CAN_POS) & L99PM72_ST2_WAKE_CAN_MASK)

#define L99PM72_ST2_WU1_WAKE_POS                                       10u
#define L99PM72_ST2_WU1_WAKE_MASK                                      1u
#define L99PM72_ST2_WU1_WAKE_GET(x)                                    ((((x)) >> L99PM72_ST2_WU1_WAKE_POS) & L99PM72_ST2_WU1_WAKE_MASK)

#define L99PM72_ST2_WU2_WAKE_POS                                       11u
#define L99PM72_ST2_WU2_WAKE_MASK                                      1u
#define L99PM72_ST2_WU2_WAKE_GET(x)                                    ((((x)) >> L99PM72_ST2_WU2_WAKE_POS) & L99PM72_ST2_WU2_WAKE_MASK)

#define L99PM72_ST2_WU3_WAKE_POS                                       12u
#define L99PM72_ST2_WU3_WAKE_MASK                                      1u
#define L99PM72_ST2_WU3_WAKE_GET(x)                                    ((((x)) >> L99PM72_ST2_WU3_WAKE_POS) & L99PM72_ST2_WU3_WAKE_MASK)

#define L99PM72_ST2_WU1_STATE_POS                                      13u
#define L99PM72_ST2_WU1_STATE_MASK                                     1u
#define L99PM72_ST2_WU1_STATE_GET(x)                                   ((((x)) >> L99PM72_ST2_WU1_STATE_POS) & L99PM72_ST2_WU1_STATE_MASK)

#define L99PM72_ST2_WU2_STATE_POS                                      14u
#define L99PM72_ST2_WU2_STATE_MASK                                     1u
#define L99PM72_ST2_WU2_STATE_GET(x)                                   ((((x)) >> L99PM72_ST2_WU2_STATE_POS) & L99PM72_ST2_WU2_STATE_MASK)

#define L99PM72_ST2_WU3_STATE_POS                                      15u
#define L99PM72_ST2_WU3_STATE_MASK                                     1u
#define L99PM72_ST2_WU3_STATE_GET(x)                                   ((((x)) >> L99PM72_ST2_WU3_STATE_POS) & L99PM72_ST2_WU3_STATE_MASK)



typedef enum
{
    L99PM72_DEVICE_MODE_ACTIVE = 0,                                         
    L99PM72_DEVICE_MODE_V1_STBY,                                            
    L99PM72_DEVICE_MODE_VBAT_STBY,                                          
    L99PM72_DEVICE_MODE_FLASH,                                              
    L99PM72_DEVICE_MODE_MAX
}enum_L99PM72_DEVICE_MODE_TYPE;


typedef enum
{
    L99PM72_WATCHDOG_WINDOW_AREA_0_33 = 0,                                  
    L99PM72_WATCHDOG_WINDOW_AREA_33_66,                                     
    L99PM72_WATCHDOG_WINDOW_AREA_66_100 = 3,                                
    L99PM72_WATCHDOG_WINDOW_AREA_MAX
}enum_L99PM72_WATCHDOG_WINDOW_AREA_TYPE;




typedef struct
{
    uint8_t err_flg;                                                
    uint8_t wd_timer_state;                                         
    uint8_t forced_sleep_tsd2;                                      
    uint8_t forced_sleep_wd;                                        
    uint8_t wd_fail;                                                
    uint8_t v1_restarts;                                            
    uint8_t v1_fail;                                                
    uint8_t dev_state;                                              
    uint8_t tw;                                                     
    uint8_t tsd1;                                                   
}struct_L99PM72_ST3_REG_TYPE;


#define L99PM72_ST3_WD_TIMER_STATE_POS                                 0u
#define L99PM72_ST3_WD_TIMER_STATE_MASK                                3u
#define L99PM72_ST3_WD_TIMER_STATE_GET(x)                              ((((x)) >> L99PM72_ST3_WD_TIMER_STATE_POS) & L99PM72_ST3_WD_TIMER_STATE_MASK)

#define L99PM72_ST3_FORCED_SLEEP_TSD2_POS                              2u
#define L99PM72_ST3_FORCED_SLEEP_TSD2_MASK                             1u
#define L99PM72_ST3_FORCED_SLEEP_TSD2_GET(x)                           ((((x)) >> L99PM72_ST3_FORCED_SLEEP_TSD2_POS) & L99PM72_ST3_FORCED_SLEEP_TSD2_MASK)

#define L99PM72_ST3_FORCED_SLEEP_WD_POS                                3u
#define L99PM72_ST3_FORCED_SLEEP_WD_MASK                               1u
#define L99PM72_ST3_FORCED_SLEEP_WD_GET(x)                             ((((x)) >> L99PM72_ST3_FORCED_SLEEP_WD_POS) & L99PM72_ST3_FORCED_SLEEP_WD_MASK)

#define L99PM72_ST3_WD_FAIL_POS                                        4u
#define L99PM72_ST3_WD_FAIL_MASK                                       0x000F
#define L99PM72_ST3_WD_FAIL_GET(x)                                     ((((x)) >> L99PM72_ST3_WD_FAIL_POS) & L99PM72_ST3_WD_FAIL_MASK)


#define L99PM72_ST3_V1_RESTARTS_POS                                    8u
#define L99PM72_ST3_V1_RESTARTS_MASK                                   7u
#define L99PM72_ST3_V1_RESTARTS_GET(x)                                 ((((x)) >> L99PM72_ST3_V1_RESTARTS_POS) & L99PM72_ST3_V1_RESTARTS_MASK)


#define L99PM72_ST3_V1_FAIL_POS                                        11u
#define L99PM72_ST3_V1_FAIL_MASK                                       1u
#define L99PM72_ST3_V1_FAIL_GET(x)                                     ((((x)) >> L99PM72_ST3_V1_FAIL_POS) & L99PM72_ST3_V1_FAIL_MASK)

#define L99PM72_ST3_DEV_STATE_POS                                      12u
#define L99PM72_ST3_DEV_STATE_MASK                                     3u
#define L99PM72_ST3_DEV_STATE_GET(x)                                   ((((x)) >> L99PM72_ST3_DEV_STATE_POS) & L99PM72_ST3_DEV_STATE_MASK)

#define L99PM72_ST3_TW_POS                                             14u
#define L99PM72_ST3_TW_MASK                                            1u
#define L99PM72_ST3_TW_GET(x)                                          ((((x)) >> L99PM72_ST3_TW_POS) & L99PM72_ST3_TW_MASK)

#define L99PM72_ST3_TSD1_POS                                           15u
#define L99PM72_ST3_TSD1_MASK                                          1u
#define L99PM72_ST3_TSD1_GET(x)                                        ((((x)) >> L99PM72_ST3_TSD1_POS) & L99PM72_ST3_TSD1_MASK)






typedef enum
{
    L99PM72_SELECTIVE_WAKEUP_READ_NOT_DONE = 0,                             
    L99PM72_SELECTIVE_WAKEUP_READ_DONE,                                     
    L99PM72_SELECTIVE_WAKEUP_READ_MAX
}enum_L99PM72_SELECTIVE_WAKEUP_READ_DONE_TYPE;




typedef struct
{
    uint8_t err_flg;                                                
    uint8_t fd_err;                                                 
    uint8_t can_silent;                                             
    uint8_t wuf;                                                    
    uint8_t wup;                                                    
    uint8_t can_to;                                                 
    uint8_t tx_sync;                                                
    uint8_t sys_err;                                                
    uint8_t swrd_7;                                                 
    uint8_t swrd_8;                                                 
    uint8_t swrd_9;                                                 
    uint8_t swrd_10;                                                
    uint8_t swrd_11;                                                
    uint8_t swrd_12;                                                
    uint8_t swrd_13;                                                
    uint8_t swrd_14;                                                
    uint8_t swrd_15;                                                
}struct_L99PM72_ST4_REG_TYPE;


#define L99PM72_ST4_FD_ERR_POS                                         0u
#define L99PM72_ST4_FD_ERR_MASK                                        1u
#define L99PM72_ST4_FD_ERR_GET(x)                                      ((((x)) >> L99PM72_ST4_FD_ERR_POS) & L99PM72_ST4_FD_ERR_MASK)

#define L99PM72_ST4_CAN_SILENT_POS                                     1u
#define L99PM72_ST4_CAN_SILENT_MASK                                    1u
#define L99PM72_ST4_CAN_SILENT_GET(x)                                  ((((x)) >> L99PM72_ST4_CAN_SILENT_POS) & L99PM72_ST4_CAN_SILENT_MASK)

#define L99PM72_ST4_WUF_POS                                            2u
#define L99PM72_ST4_WUF_MASK                                           1u
#define L99PM72_ST4_WUF_GET(x)                                         ((((x)) >> L99PM72_ST4_WUF_POS) & L99PM72_ST4_WUF_MASK)

#define L99PM72_ST4_WUP_POS                                            3u
#define L99PM72_ST4_WUP_MASK                                           1u
#define L99PM72_ST4_WUP_GET(x)                                         ((((x)) >> L99PM72_ST4_WUP_POS) & L99PM72_ST4_WUP_MASK)

#define L99PM72_ST4_CAN_TO_POS                                         4u
#define L99PM72_ST4_CAN_TO_MASK                                        1u
#define L99PM72_ST4_CAN_TO_GET(x)                                      ((((x)) >> L99PM72_ST4_CAN_TO_POS) & L99PM72_ST4_CAN_TO_MASK)

#define L99PM72_ST4_TX_SYNC_POS                                        5u
#define L99PM72_ST4_TX_SYNC_MASK                                       1u
#define L99PM72_ST4_TX_SYNC_GET(x)                                     ((((x)) >> L99PM72_ST4_TX_SYNC_POS) & L99PM72_ST4_TX_SYNC_MASK)

#define L99PM72_ST4_SYS_ERR_POS                                        6u
#define L99PM72_ST4_SYS_ERR_MASK                                       1u
#define L99PM72_ST4_SYS_ERR_GET(x)                                     ((((x)) >> L99PM72_ST4_SYS_ERR_POS) & L99PM72_ST4_SYS_ERR_MASK)

#define L99PM72_ST4_SWRD7_POS                                          7u
#define L99PM72_ST4_SWRD7_MASK                                         1u
#define L99PM72_ST4_SWRD7_GET(x)                                       ((((x)) >> L99PM72_ST4_SWRD7_POS) & L99PM72_ST4_SWRD7_MASK)

#define L99PM72_ST4_SWRD8_POS                                          8u
#define L99PM72_ST4_SWRD8_MASK                                         1u
#define L99PM72_ST4_SWRD8_GET(x)                                       ((((x)) >> L99PM72_ST4_SWRD8_POS) & L99PM72_ST4_SWRD8_MASK)

#define L99PM72_ST4_SWRD9_POS                                          9u
#define L99PM72_ST4_SWRD9_MASK                                         1u
#define L99PM72_ST4_SWRD9_GET(x)                                       ((((x)) >> L99PM72_ST4_SWRD9_POS) & L99PM72_ST4_SWRD9_MASK)

#define L99PM72_ST4_SWRD10_POS                                         10u
#define L99PM72_ST4_SWRD10_MASK                                        1u
#define L99PM72_ST4_SWRD10_GET(x)                                      ((((x)) >> L99PM72_ST4_SWRD10_POS) & L99PM72_ST4_SWRD10_MASK)

#define L99PM72_ST4_SWRD11_POS                                         11u
#define L99PM72_ST4_SWRD11_MASK                                        1u
#define L99PM72_ST4_SWRD11_GET(x)                                      ((((x)) >> L99PM72_ST4_SWRD11_POS) & L99PM72_ST4_SWRD11_MASK)

#define L99PM72_ST4_SWRD12_POS                                         12u
#define L99PM72_ST4_SWRD12_MASK                                        1u
#define L99PM72_ST4_SWRD12_GET(x)                                      ((((x)) >> L99PM72_ST4_SWRD12_POS) & L99PM72_ST4_SWRD12_MASK)

#define L99PM72_ST4_SWRD13_POS                                         13u
#define L99PM72_ST4_SWRD13_MASK                                        1u
#define L99PM72_ST4_SWRD13_GET(x)                                      ((((x)) >> L99PM72_ST4_SWRD13_POS) & L99PM72_ST4_SWRD13_MASK)

#define L99PM72_ST4_SWRD14_POS                                         14u
#define L99PM72_ST4_SWRD14_MASK                                        1u
#define L99PM72_ST4_SWRD14_GET(x)                                      ((((x)) >> L99PM72_ST4_SWRD14_POS) & L99PM72_ST4_SWRD14_MASK)

#define L99PM72_ST4_SWRD15_POS                                         15u
#define L99PM72_ST4_SWRD15_MASK                                        1u
#define L99PM72_ST4_SWRD15_GET(x)                                      ((((x)) >> L99PM72_ST4_SWRD15_POS) & L99PM72_ST4_SWRD15_MASK)








typedef struct
{
    uint8_t err_flg;                                                
    uint8_t osc_mon;                                                
    uint8_t osc_fail;                                               
    uint8_t fecnt;                                                  
}struct_L99PM72_ST5_REG_TYPE;



#define L99PM72_ST5_OSC_MON_POS                                        0u
#define L99PM72_ST5_OSC_MON_MASK                                       0x000F
#define L99PM72_ST5_OSC_MON_GET(x)                                     ((((x)) >> L99PM72_ST5_OSC_MON_POS) & L99PM72_ST5_OSC_MON_MASK)

#define L99PM72_ST5_RESERVED_POS                                       5u
#define L99PM72_ST5_RESERVED_MASK                                      3u
#define L99PM72_ST5_RESERVED_GET(x)                                    ((((x)) >> L99PM72_ST5_RESERVED_POS) & L99PM72_ST5_RESERVED_MASK)

#define L99PM72_ST5_SCO_FAIL_POS                                       7u
#define L99PM72_ST5_SCO_FAIL_MASK                                      1u
#define L99PM72_ST5_SCO_FAIL_GET(x)                                    ((((x)) >> L99PM72_ST5_SCO_FAIL_POS) & L99PM72_ST5_SCO_FAIL_MASK)

#define L99PM72_ST5_FECNT_POS                                          8u
#define L99PM72_ST5_FECNT_MASK                                         0x001F
#define L99PM72_ST5_FECNT_GET(x)                                       ((((x)) >> L99PM72_ST5_FECNT_POS) & L99PM72_ST5_FECNT_MASK)

#define L99PM72_ST5_RESERVED2_POS                                      13u
#define L99PM72_ST5_RESERVED2_MASK                                     7u
#define L99PM72_ST5_RESERVED2_GET(x)                                   ((((x)) >> L99PM72_ST5_RESERVED2_POS) & L99PM72_ST5_RESERVED2_MASK)







#define L99PM72_EEPROM_ID_HEADER_POS                                   8u
#define L99PM72_EEPROM_ID_HEADER_MASK                                  0x00FF
#define L99PM72_EEPROM_ID_HEADER_GET(x)                                ((((x)) >> L99PM72_EEPROM_ID_HEADER_POS) & L99PM72_EEPROM_ID_HEADER_MASK)
typedef struct
{
    uint8_t id_header;
}struct_L99PM72_ROM_ID_REG_TYPE;



#define L99PM72_EEPROM_SILICON_VERSION_POS                             8u
#define L99PM72_EEPROM_SILICON_VERSION_MASK                            0x00FF
#define L99PM72_EEPROM_SILICON_VERSION_GET(x)                          ((((x)) >> L99PM72_EEPROM_SILICON_VERSION_POS) & L99PM72_EEPROM_SILICON_VERSION_MASK)
typedef struct
{
    uint8_t silicon_version;
}struct_L99PM72_ROM_SILICON_VERSION_REG_TYPE;



#define L99PM72_EEPROM_PRODUCT_CODE1_POS                               8u
#define L99PM72_EEPROM_PRODUCT_CODE1_MASK                              0x00FF
#define L99PM72_EEPROM_PRODUCT_CODE1_GET(x)                            ((((x)) >> L99PM72_EEPROM_PRODUCT_CODE1_POS) & L99PM72_EEPROM_PRODUCT_CODE1_MASK)
typedef struct
{
    uint8_t uid1;
}struct_L99PM72_ROM_UID1_REG_TYPE;


#define L99PM72_EEPROM_PRODUCT_CODE2_POS                               8u
#define L99PM72_EEPROM_PRODUCT_CODE2_MASK                              0x00FF
#define L99PM72_EEPROM_PRODUCT_CODE2_GET(x)                            ((((x)) >> L99PM72_EEPROM_PRODUCT_CODE2_POS) & L99PM72_EEPROM_PRODUCT_CODE2_MASK)
typedef struct
{
    uint8_t uid2;
}struct_L99PM72_ROM_UID2_REG_TYPE;

#define L99PM72_EEPROM_SPI_FRAME_POS                                   8u
#define L99PM72_EEPROM_SPI_FRAME_MASK                                  0x00FF
#define L99PM72_EEPROM_SPI_FRAME_GET(x)                                ((((x)) >> L99PM72_EEPROM_SPI_FRAME_POS) & L99PM72_EEPROM_SPI_FRAME_MASK)
typedef struct
{
    uint8_t spi_frame;
}struct_L99PM72_ROM_SPI_FRAME_REG_TYPE;







typedef struct
{
    struct_L99PM72_CTRL1_REG_TYPE   ctrl1;              
    struct_L99PM72_CTRL2_REG_TYPE   ctrl2;              
    struct_L99PM72_CTRL3_REG_TYPE   ctrl3;              
    struct_L99PM72_CTRL4_REG_TYPE   ctrl4;              
    struct_L99PM72_CTRL5_REG_TYPE   ctrl5;              
    struct_L99PM72_CTRL6_REG_TYPE   ctrl6;              
    struct_L99PM72_CTRL7_REG_TYPE   ctrl7;              
    struct_L99PM72_CTRL8_REG_TYPE   ctrl8;              
    struct_L99PM72_CTRL9_REG_TYPE   ctrl9;              
    struct_L99PM72_CTRL10_REG_TYPE  ctrl10;             
    struct_L99PM72_CTRL11_REG_TYPE  ctrl11;             
    struct_L99PM72_CTRL12_REG_TYPE  ctrl12;             
    struct_L99PM72_CTRL13_REG_TYPE  ctrl13;             
    struct_L99PM72_CTRL14_REG_TYPE  ctrl14;             
    struct_L99PM72_CTRL15_REG_TYPE  ctrl15;             
    struct_L99PM72_CTRL16_REG_TYPE  ctrl16;             
    struct_L99PM72_CTRL34_REG_TYPE  ctrl34;             
    struct_L99PM72_CTRL35_REG_TYPE  ctrl35;             
    struct_L99PM72_CFG_REG_TYPE     ctrl_cfg;           
}struct_L99PM72_TX_REG_TYPE;


typedef struct
{
    struct_L99PM72_CTRL1_REG_TYPE   ctrl1;              
    struct_L99PM72_CTRL2_REG_TYPE   ctrl2;              
    struct_L99PM72_CTRL3_REG_TYPE   ctrl3;              
    struct_L99PM72_CTRL4_REG_TYPE   ctrl4;              
    struct_L99PM72_CTRL5_REG_TYPE   ctrl5;              
    struct_L99PM72_CTRL6_REG_TYPE   ctrl6;              
    struct_L99PM72_CTRL7_REG_TYPE   ctrl7;              
    struct_L99PM72_CTRL8_REG_TYPE   ctrl8;              
    struct_L99PM72_CTRL9_REG_TYPE   ctrl9;              
    struct_L99PM72_CTRL10_REG_TYPE  ctrl10;             
    struct_L99PM72_CTRL11_REG_TYPE  ctrl11;             
    struct_L99PM72_CTRL12_REG_TYPE  ctrl12;             
    struct_L99PM72_CTRL13_REG_TYPE  ctrl13;             
    struct_L99PM72_CTRL14_REG_TYPE  ctrl14;             
    struct_L99PM72_CTRL15_REG_TYPE  ctrl15;             
    struct_L99PM72_CTRL16_REG_TYPE  ctrl16;             
    struct_L99PM72_CTRL34_REG_TYPE  ctrl34;             
    struct_L99PM72_CTRL35_REG_TYPE  ctrl35;             
    struct_L99PM72_CFG_REG_TYPE     ctrl_cfg;           
    struct_L99PM72_ROM_ID_REG_TYPE  rom_id;             
    struct_L99PM72_ROM_SILICON_VERSION_REG_TYPE rom_sil;
    struct_L99PM72_ROM_UID1_REG_TYPE rom_uid1;          
    struct_L99PM72_ROM_UID2_REG_TYPE rom_uid2;          
    struct_L99PM72_ROM_SPI_FRAME_REG_TYPE rom_spi_frm;  
    struct_L99PM72_ST1_REG_TYPE     st1;                
    struct_L99PM72_ST2_REG_TYPE     st2;                
    struct_L99PM72_ST3_REG_TYPE     st3;                
    struct_L99PM72_ST4_REG_TYPE     st4;                
    struct_L99PM72_ST5_REG_TYPE     st5;                
}struct_L99PM72_RX_REG_TYPE;










#endif 




