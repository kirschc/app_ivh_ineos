#ifndef PMIC_PROTOCOL_H
#define PMIC_PROTOCOL_H



#include "l99pm72_includes.h"
#include "l99pm72_base.h"
#include "l99pm72_memory_reflector.h"




typedef enum
{
    L99PM72_WAKEUP_INPUT_1 = 1,        
    L99PM72_WAKEUP_INPUT_2,            
    L99PM72_WAKEUP_INPUT_3,            
    L99PM72_WAKEUP_INPUT_MAX           

}enum_L99PM72_WAKEUP_INPUT_TYPE;

typedef enum
{
    L99PM72_LSD1 = 1,                  
    L99PM72_LSD2,                      
    L99PM72_LSD_MAX                    

}enum_L99PM72_LOWSIDE_DRIVER_NAME_TYPE;

typedef enum
{
    L99PM72_PWM1 = 1,                  
    L99PM72_PWM2,                      
    L99PM72_PWM3,                      
    L99PM72_PWM4,                      
    L99PM72_PWM_MAX                    
}enum_L99PM72_PWM_NAME_TYPE;




enum_L99PM72_ERR_CODE_TYPE l99pm72_eeprom_read_device_info(void);



enum_L99PM72_ERR_CODE_TYPE l99pm72_partnet_canid( uint32_t can_id, uint8_t is_extended );
enum_L99PM72_ERR_CODE_TYPE l99pm72_partnet_data( uint8_t* data, uint8_t dlc );
enum_L99PM72_ERR_CODE_TYPE l99pm72_partnet_can_mask( uint32_t mask );
enum_L99PM72_ERR_CODE_TYPE l99pm72_partnet_en( enum_L99PM72_SELECTIVE_WAKEUP_STATUS_TYPE en );
enum_L99PM72_ERR_CODE_TYPE l99pm72_partnet_baudrate( enum_L99PM72_CAN_BAUDRATES_TYPE baudrate );
enum_L99PM72_ERR_CODE_TYPE l99pm72_partnet_samplepoint( enum_L99PM72_CAN_SAMPLE_POINT_TYPE sp );



enum_L99PM72_ERR_CODE_TYPE l99pm72_set_v2_mode(enum_L99PM72_V2_MODE_STATE_TYPE mode);


enum_L99PM72_ERR_CODE_TYPE l99pm72_enter_sleepmode(enum_L99PM72_MODE_STANDBY_TYPE mode);


enum_L99PM72_ERR_CODE_TYPE l99pm72_wakeup_en(enum_L99PM72_WAKEUP_INPUT_TYPE wup_input, enum_L99PM72_WU_STATE_TYPE mode );


enum_L99PM72_ERR_CODE_TYPE l99pm72_wakeup_debounce_time( enum_L99PM72_WAKEUP_INPUT_TYPE wup_input, enum_L99PM72_WAKEUP_FILTER_CFG_TYPE mode );

enum_L99PM72_ERR_CODE_TYPE l99pm72_wakeup_pullup_dir( enum_L99PM72_WAKEUP_INPUT_TYPE wup_input, enum_L99PM72_WU_PUPD_TYPE mode );

enum_L99PM72_ERR_CODE_TYPE l99pm72_wakeup_timer_enable( enum_L99PM72_WAKETIMER_STATUS_TYPE en );


enum_L99PM72_ERR_CODE_TYPE l99pm72_wakeup_timer_select( enum_L99PM72_WAKETIMER_SOURCE_TYPE timer );


enum_L99PM72_ERR_CODE_TYPE l99pm72_wakeup_lin_en( enum_L99PM72_LIN_WAKEUP_STATUS_TYPE en );


enum_L99PM72_ERR_CODE_TYPE l99pm72_wakeup_can_en( enum_L99PM72_CAN_WAKEUP_STATUS_TYPE en );




enum_L99PM72_ERR_CODE_TYPE l99pm72_set_pwm_duty(enum_L99PM72_PWM_NAME_TYPE pwm_nr, uint8_t duty);


enum_L99PM72_ERR_CODE_TYPE l99pm72_set_pwm_freq(enum_L99PM72_PWM_FREQUENCY_TYPE pwm_freq);



enum_L99PM72_ERR_CODE_TYPE l99pm72_set_hsd1_mode(enum_L99PM72_HSD1_MODE_TYPE mode);

enum_L99PM72_ERR_CODE_TYPE l99pm72_set_hsd2_mode(enum_L99PM72_HSD2_MODE_TYPE mode);


enum_L99PM72_ERR_CODE_TYPE l99pm72_set_hsd3_mode(enum_L99PM72_HSD3_MODE_TYPE mode);


enum_L99PM72_ERR_CODE_TYPE l99pm72_set_hsd4_mode(enum_L99PM72_HSD4_MODE_TYPE mode);



enum_L99PM72_ERR_CODE_TYPE l99pm72_set_ouths_mode(enum_L99PM72_OUTHS_MODE_TYPE mode);



enum_L99PM72_ERR_CODE_TYPE l99pm72_set_lsd(enum_L99PM72_LOWSIDE_DRIVER_NAME_TYPE id, enum_L99PM72_RELAY_STATE_TYPE val);




enum_L99PM72_ERR_CODE_TYPE l99pm72_timer_t1_set_hightime(enum_L99PM72_T1_HIGH_TIME_SELECTION_TYPE time);

enum_L99PM72_ERR_CODE_TYPE l99pm72_timer_t1_set_interval(enum_L99PM72_T1_PERIOD_TIME_TYPE time);

enum_L99PM72_ERR_CODE_TYPE l99pm72_timer_t2_set_hightime(enum_L99PM72_T2_HIGH_TIME_SELECTION_TYPE time);


enum_L99PM72_ERR_CODE_TYPE l99pm72_timer_t2_set_interval(enum_L99PM72_T2_PERIOD_TIME_TYPE time);



enum_L99PM72_ERR_CODE_TYPE l99pm72_watchdog_set_time(enum_L99PM72_WINDOW_WATCHDOG_TRIGGER_TIME_TYPE time);

enum_L99PM72_ERR_CODE_TYPE l99pm72_watchdog_disable(enum_L99PM72_WATCHDOG_STATE_TYPE mode);


enum_L99PM72_ERR_CODE_TYPE l99pm72_ctrl_wd_stdby_en( enum_L99PM72_V1_WATCHDOG_STANDBY_STATUS_TYPE en );


enum_L99PM72_ERR_CODE_TYPE l99pm72_ctrl_ouths_overcurrent_recovery( enum_L99PM72_OUTHS_OVERCURRENT_RECOVERY_STATUS_TYPE en );


enum_L99PM72_ERR_CODE_TYPE l99pm72_ctrl_lsd_undervoltage_ov_shutdown( enum_L99PM72_LOWSIDE_VOLTAGE_FAILURE_SHUTDOWN_TYPE en );

enum_L99PM72_ERR_CODE_TYPE l99pm72_ctrl_v1_reset_level( enum_L99PM72_RESET_VOLTAGE_LEVEL_TYPE level );



enum_L99PM72_ERR_CODE_TYPE l99pm72_ctrl_over_undervoltage_auto_recovery( enum_L99PM72_OVERVOLTAGE_STATUS_RECOVERY_TYPE en );




enum_L99PM72_ERR_CODE_TYPE l99pm72_lin_pullup_en( enum_L99PM72_LIN_PULLUP_TYPE en );

enum_L99PM72_ERR_CODE_TYPE l99pm72_lin_auto_disable( enum_L99PM72_LIN_TRANSMITTER_AUTO_OFF_STATUS_TYPE en );


enum_L99PM72_ERR_CODE_TYPE l99pm72_can_enable(enum_L99PM72_CAN_STATUS_TYPE en);

enum_L99PM72_ERR_CODE_TYPE l99pm72_can_loopback_enabled(enum_L99PM72_CAN_LOOPBACK_STATUS_TYPE en);


enum_L99PM72_ERR_CODE_TYPE l99pm72_can_receive_only_enabled(enum_L99PM72_CAN_LISTEN_ONLY_MODE_TYPE en);










enum_L99PM72_ERR_CODE_TYPE l99pm72_request_state_outputs(void);

enum_L99PM72_ERR_CODE_TYPE l99pm72_request_state_wakeup_and_bus(void);

enum_L99PM72_ERR_CODE_TYPE l99pm72_request_state_device(void);


enum_L99PM72_ERR_CODE_TYPE l99pm72_request_state_selective_wakeup(void);

enum_L99PM72_ERR_CODE_TYPE l99pm72_request_state_errorcnt_and_oscmon(void);








enum_L99PM72_ERR_CODE_TYPE l99pm72_clear_state_outputs(void);

enum_L99PM72_ERR_CODE_TYPE l99pm72_clear_state_wakeup_and_bus(void);

enum_L99PM72_ERR_CODE_TYPE l99pm72_clear_state_device(void);


enum_L99PM72_ERR_CODE_TYPE l99pm72_clear_state_selective_wakeup(void);

enum_L99PM72_ERR_CODE_TYPE l99pm72_clear_state_errorcnt_and_oscmon(void);



enum_L99PM72_ERR_CODE_TYPE l99pm72_clear_state_global(void);



#endif 


