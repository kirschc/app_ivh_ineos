###################################################################################################
# DEFINES
###################################################################################################
define PATH_TO_BIN/TEMP/NAME_OF_IMAGE_STEM
$(INT_CONF_PATH_TO_BIN)/$(INT_CONF_NAME_OF_IMAGE_STEM)_temp/$(INT_CONF_NAME_OF_IMAGE_STEM)
endef

###################################################################################################
# CONFIGURATIONS
###################################################################################################
# file name of final binary
INT_CONF_NAME_OF_IMAGE_STEM := bsw_$(SYSTEM_TARGET)_$(SYSTEM_ARCH)_cronus_flasher_only

INT_CONF_LINKER_FILE := $(INT_CONF_PATH_TO_LINK)/locate.ld

###################################################################################################
# BUILD RULES
###################################################################################################
# Applics Studio uses SET GREENHILLS_PATH=C:\ghs to set location for compiler
ifneq ($(INT_CONF_BUILD_BY_AS),APPLICS_STUDIO)
TOOLCHAIN_PATH = toolchain/ghs/
endif
CC              := $(TOOLCHAIN_PATH)cxrh850.exe
LD              := $(TOOLCHAIN_PATH)cxrh850.exe
BIN2HEX         := $(TOOLCHAIN_PATH)gsrec.exe
DEP_GCC         := gcc
IMAGE_FILE_TYPE := .s19

INT_CONF_PATH_TO_OBJ_TARGET := $(PATH_TO_OBJ/NAME_OF_IMAGE_STEM)
INT_CONF_NAME_OF_IMAGE      := $(INT_CONF_NAME_OF_IMAGE_STEM)$(IMAGE_FILE_TYPE)

###################################################################################################
# COMPILER AND LINKERFLAGS
###################################################################################################
CFLAGS_TARGET := -DCC_TARGET_APP
AFLAGS_TARGET := $(CFLAGS_TARGET)
LFLAGS_TARGET :=

###################################################################################################
# PATHES FOR OBJECTS AND INCLUDES
###################################################################################################
# application specific data
-include ../app_core/app.mk

# collector for general pathes
VPATH := $(INT_CONF_BSW_VPATH) \
         $(INT_CONF_APP_VPATH)

# collector for object files
INT_CONF_OBJFILES := $(INT_CONF_BSW_OBJFILES) \
                     $(INT_CONF_APP_OBJFILES)

# collector for include pathes
CFLAGS_INCLUDE_PATH := $(INT_CONF_BSW_INCLUDE_PATHES) \
                       $(INT_CONF_APP_INCLUDE_PATHES)

###################################################################################################
# TARGETS
###################################################################################################
ifneq ($(INT_CONF_BUILD_BY_AS),APPLICS_STUDIO)
$(PATH_TO_OBJ/NAME_OF_BSW_LIB).a : $(INT_CONF_OBJFILES)
	$(CC) -archive $(INT_CONF_BSW_OBJFILES) -o $@

$(PATH_TO_BIN/NAME_OF_IMAGE_STEM).elf : $(PATH_TO_OBJ/NAME_OF_BSW_LIB).a
else
$(PATH_TO_BIN/NAME_OF_IMAGE_STEM).elf : $(INT_CONF_OBJFILES)
endif
	$(LD) $(INT_CONF_LFLAGS) $(INT_CONF_LINKER_FILE) $(filter-out $(INT_CONF_BSW_OBJFILES), $(INT_CONF_OBJFILES)) $(PATH_TO_OBJ/NAME_OF_BSW_LIB).a -o $@

$(PATH_TO_BIN/NAME_OF_IMAGE_STEM).rec : $(PATH_TO_BIN/NAME_OF_IMAGE_STEM).elf
	$(BIN2HEX) -skip .BL_ACCESS_KEY -bytes 16 $< -o $@
	$(BIN2HEX) -skip .BL_ACCESS_KEY -hex386 $< -o $(PATH_TO_BIN/NAME_OF_IMAGE_STEM).hex

# Flash Image generation for the Bootloader 
$(PATH_TO_BIN/TEMP/NAME_OF_IMAGE_STEM).tmp1 : $(PATH_TO_BIN/NAME_OF_IMAGE_STEM).rec
	$(SREC_CAT) $< -crop 0x20000 0xffffff -o $@ # crop the app flash range

$(PATH_TO_BIN/TEMP/NAME_OF_IMAGE_STEM).tmp2 : $(PATH_TO_BIN/TEMP/NAME_OF_IMAGE_STEM).tmp1
	$(SREC_CAT) $< -fill 0x0 -over $< -within $< -range-pad 256 --address-length=4  -o $@ # fill and padding the firmware image

$(PATH_TO_BIN/TEMP/NAME_OF_IMAGE_STEM).tmp3 : $(PATH_TO_BIN/TEMP/NAME_OF_IMAGE_STEM).tmp2
	$(SREC_CAT) $< -crop 0x21000 0xffffff -o $@ # remove the flash header

$(PATH_TO_BIN/TEMP/NAME_OF_IMAGE_STEM).crc : $(PATH_TO_BIN/TEMP/NAME_OF_IMAGE_STEM).tmp3
	$(SREC_CAT) $< -crc32-l-e 0x0 -crop 0x0 0x4 -o $@ -Binary # calculate the application crc

$(PATH_TO_BIN/TEMP/NAME_OF_IMAGE_STEM).length : $(PATH_TO_BIN/TEMP/NAME_OF_IMAGE_STEM).tmp3
	$(SREC_CAT) $< -exclusive-length-l-e 0x0 -crop 0x0 0x4 -o $@ -Binary # calculate the application length

$(PATH_TO_BIN/TEMP/NAME_OF_IMAGE_STEM).tmp4 : $(PATH_TO_BIN/TEMP/NAME_OF_IMAGE_STEM).tmp2   \
                                              $(PATH_TO_BIN/TEMP/NAME_OF_IMAGE_STEM).crc    \
                                              $(PATH_TO_BIN/TEMP/NAME_OF_IMAGE_STEM).length
	$(SREC_CAT) $(PATH_TO_BIN/TEMP/NAME_OF_IMAGE_STEM).tmp2               \
	-exclude 0x2000c 0x20014 $(PATH_TO_BIN/TEMP/NAME_OF_IMAGE_STEM).crc   \
	-Binary -offset 0x2000c $(PATH_TO_BIN/TEMP/NAME_OF_IMAGE_STEM).length \
	-Binary -offset 0x20010 -o $@ # add the application crc and length to the flash header

$(PATH_TO_BIN/TEMP/NAME_OF_IMAGE_STEM).tmp5 : $(PATH_TO_BIN/TEMP/NAME_OF_IMAGE_STEM).tmp4
	$(SREC_CAT) $< -crop 0x20008 0x21000 -offset -0x20008 -o $@ -Binary # remove the application part and exclude the flash header key and crc part

$(PATH_TO_BIN/TEMP/NAME_OF_IMAGE_STEM).hcrc : $(PATH_TO_BIN/TEMP/NAME_OF_IMAGE_STEM).tmp5
	$(SREC_CAT) $< -Binary -offset 4 -crc32-l-e 0x0 -crop 0x0 0x4 -o $@ -Binary # calculate the flash header crc

$(PATH_TO_BIN/NAME_OF_IMAGE_STEM).s19: $(PATH_TO_BIN/TEMP/NAME_OF_IMAGE_STEM).tmp4 \
                                       $(PATH_TO_BIN/TEMP/NAME_OF_IMAGE_STEM).hcrc \
                                       | $(INT_CONF_PATH_TO_BIN)/$(INT_CONF_NAME_OF_IMAGE_STEM)_temp 
	$(SREC_CAT) $(PATH_TO_BIN/TEMP/NAME_OF_IMAGE_STEM).tmp4              \
	-exclude 0x20004 0x20008 $(PATH_TO_BIN/TEMP/NAME_OF_IMAGE_STEM).hcrc \
	-Binary -offset 0x20004 --address-length=4 -o $@ # add the flash header crc to the flash header and generate the final image
	srec_info $@