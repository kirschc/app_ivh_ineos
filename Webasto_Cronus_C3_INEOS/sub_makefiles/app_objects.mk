###################################################################################################
# LIST OF ALL USED OBJECTS
###################################################################################################
# following files are provided by base software but are not pre-compiled
# so they are not part of the base software library and must be rebuild
INT_CONF_APP_OBJFILES += $(INT_CONF_PATH_TO_OBJ_TARGET)/graph_code.o                         \
                         $(INT_CONF_PATH_TO_OBJ_TARGET)/graph_support.o                      \
                         $(INT_CONF_PATH_TO_OBJ_TARGET)/module_code.o                        \
                         $(INT_CONF_PATH_TO_OBJ_TARGET)/lin_db_tables.o                      \
                         $(INT_CONF_PATH_TO_OBJ_TARGET)/lin_diagnose.o                       \
                         $(INT_CONF_PATH_TO_OBJ_TARGET)/webasto_parameter.o                  \
                         $(INT_CONF_PATH_TO_OBJ_TARGET)/can_db_tables.o                      \
                         $(INT_CONF_PATH_TO_OBJ_TARGET)/can_db_tables_cfg.o                  \
                         $(INT_CONF_PATH_TO_OBJ_TARGET)/sfl_can_db_tables_data.o             \
                         $(INT_CONF_PATH_TO_OBJ_TARGET)/user_code.o                          \
                         $(INT_CONF_PATH_TO_OBJ_TARGET)/IUG_user_api_callback.o              \
                         $(INT_CONF_PATH_TO_OBJ_TARGET)/IUG_user_api_cmd_button.o            \
                         $(INT_CONF_PATH_TO_OBJ_TARGET)/IUG_user_api_user_device_user.o      \
                         $(INT_CONF_PATH_TO_OBJ_TARGET)/IUG_user_api_initial_operation_cfg.o \
                         $(INT_CONF_PATH_TO_OBJ_TARGET)/system_init.o                        \
                         $(INT_CONF_PATH_TO_OBJ_TARGET)/user_lin.o                           \
                         $(INT_CONF_PATH_TO_OBJ_TARGET)/heap_2.o                             \
                         $(INT_CONF_PATH_TO_OBJ_TARGET)/obd_diagnosis_cfg.o

# example how to add new object
# INT_CONF_APP_OBJFILES += $(INT_CONF_PATH_TO_OBJ_TARGET)/<filename>.o