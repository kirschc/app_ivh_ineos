###################################################################################################
# LIST OF ALL USED PATHES
###################################################################################################
INT_CONF_BSW_VPATH := src/app                                    			\
                      src/app/CAN_Webasto_bridge                 			\
                      src/app/Cronus_User                        			\
                      src/app/Cronus_User/bsw_cfg                			\
                      src/app/Cronus_User/demo_cmd_button        			\
                      src/app/Cronus_User/demo_ghi_htr           			\
                      src/app/Cronus_User/demo_htr               			\
                      src/app/Cronus_User/demo_user_device       			\
                      src/app/Cronus_User/demo_api_units_data_support		\
                      src/app/IUG_cfg                            			\
                      src/app/IUG_communication                  			\
                      src/app/IUG_communication_ghi              			\
                      src/app/IUG_communication_debug              			\
                      src/app/IUG_EOL                            			\
                      src/app/IUG_main                           			\
                      src/app/IUG_main/constant                  			\
                      src/app/IUG_main/diagnosis                 			\
                      src/app/IUG_main/error_handler             			\
                      src/app/IUG_main/error_handler/dtc_cfg     			\
                      src/app/IUG_main/initial_operation         			\
                      src/app/IUG_main/monitoring                			\
                      src/app/IUG_main/parameter_handling        			\
                      src/app/IUG_main/peripherals               			\
                      src/app/IUG_main/site                      			\
                      src/app/IUG_main/system                    			\
                      src/app/IUG_parameter                      			\
                      src/app/IUG_sandbox                        			\
                      src/app/IUG_unit                           			\
                      src/app/IUG_unit/bluetooth_ctl             			\
                      src/app/IUG_unit/cfg                       			\
                      src/app/IUG_unit/cmd_button                			\
                      src/app/IUG_unit/gen_control               			\
                      src/app/IUG_unit/gen_detection             			\
                      src/app/IUG_unit/gen_heater                			\
                      src/app/IUG_unit/iug                       			\
                      src/app/IUG_unit/iug_user_api              			\
                      src/app/IUG_unit/simulation                			\
                      src/app/IUG_unit/user_device               			\
                      src/app/IUG_unit/wbus                      			\
                      src/app/IUG_unit_ghi                       			\
                      src/app/IUG_unit_ghi/simulation            			\
                      src/app/IUG_user_api_deprecated            			\
                      src/app/IUG_user_api_header                			\
                      src/app/IUG_user_api_source                			\
                      src/app/IUG_utility                        			\
                      src/app/WBus_Driver                        			\
                      src/app/WBus_Driver/constant               			\
                      src/app/WBus_DriverCfg                    			\
                      src/app/WBus_SecurityAccess                			\
                      src/bspRH850/il                            			\
                      src/bspRH850/il/ecum                       			\
                      src/bspRH850/il/fee                        			\
                      src/bspRH850/il/memif                      			\
                      src/bspRH850/lld                           			\
                      src/bspRH850/lld/Adc                       			\
                      src/bspRH850/lld/can                       			\
                      src/bspRH850/lld/det                       			\
                      src/bspRH850/lld/device                    			\
                      src/bspRH850/lld/dio                       			\
                      src/bspRH850/lld/fdl                       			\
                      src/bspRH850/lld/fdl/lib                   			\
                      src/bspRH850/lld/fls                       			\
                      src/bspRH850/lld/lin_sci                   			\
                      src/bspRH850/lld/main                      			\
                      src/bspRH850/lld/mcu                       			\
                      src/bspRH850/lld/ostm                      			\
                      src/bspRH850/lld/port									\
                      src/bspRH850/lld/pwm                       			\
                      src/bspRH850/lld/reset                     			\
                      src/bspRH850/lld/rtca                      			\
                      src/bspRH850/lld/spi                       			\
                      src/bspRH850/lld/stbc                      			\
                      src/bspRH850/lld/timer                     			\
                      src/bspRH850/startup                       			\
                      src/cfl/bleBlStack                         			\
                      src/cfl/bleBlStack/FCL                     			\
                      src/cfl/bleBlStack/FCL/lib                 			\
                      src/cfl/bleBlStack/spi_pmic_driver         			\
                      src/cfl/linStack                           			\
                      src/config                                 			\
                      src/cppunitMinGw                           			\
                      src/dfl/bgm111                             			\
                      src/dfl/L99PM72                            			\
                      src/ds                                     			\
                      src/ds/can                                 			\
                      src/ds/dsl                                 			\
                      src/ds/gp                                  			\
                      src/ds/io                                  			\
                      src/ds/lin                                 			\
                      src/ds/parameter                          			\
                      src/ds/user                                			\
                      src/halImplLibc                            			\
                      src/halImplRH850                           			\
                      src/os/freeRTOS                            			\
                      src/os/freeRTOS/config                     			\
                      src/os/freeRTOS/RTOS                       			\
                      src/os/freeRTOS/RTOS/portable              			\
                      src/os/freeRTOS/RTOS/portable/GHS/RH850F1L 			\
                      src/sfl
 
###################################################################################################
# LIST OF PATHES USED FOR INCLUDES
###################################################################################################
INT_CONF_BSW_INCLUDE_PATH_APP := -I src/app                                         \
                                 -I src/app/CAN_Webasto_bridge                      \
                                 -I src/app/Cronus_User                             \
                                 -I src/app/Cronus_User/bsw_cfg                     \
                                 -I src/app/Cronus_User/demo_cmd_button             \
                                 -I src/app/Cronus_User/demo_ghi_htr                \
                                 -I src/app/Cronus_User/demo_htr                    \
                                 -I src/app/Cronus_User/demo_user_device            \
                                 -I src/app/Cronus_User/demo_api_units_data_support \
                                 -I src/app/IUG_cfg                                 \
                                 -I src/app/IUG_communication                       \
                                 -I src/app/IUG_communication_ghi                   \
                                 -I src/app/IUG_communication_debug                 \
                                 -I src/app/IUG_EOL                                 \
                                 -I src/app/IUG_main                                \
                                 -I src/app/IUG_main/constant                       \
                                 -I src/app/IUG_main/diagnosis                      \
                                 -I src/app/IUG_main/error_handler                  \
                                 -I src/app/IUG_main/error_handler/dtc_cfg          \
                                 -I src/app/IUG_main/initial_operation              \
                                 -I src/app/IUG_main/monitoring                     \
                                 -I src/app/IUG_main/parameter_handling             \
                                 -I src/app/IUG_main/peripherals                    \
                                 -I src/app/IUG_main/site                           \
                                 -I src/app/IUG_main/system                         \
                                 -I src/app/IUG_sandbox                             \
                                 -I src/app/IUG_unit                                \
                                 -I src/app/IUG_unit/bluetooth_ctl                  \
                                 -I src/app/IUG_unit/cfg                            \
                                 -I src/app/IUG_unit/cmd_button                     \
                                 -I src/app/IUG_unit/gen_control                    \
                                 -I src/app/IUG_unit/gen_detection                  \
                                 -I src/app/IUG_unit/gen_heater                     \
                                 -I src/app/IUG_unit/iug                            \
                                 -I src/app/IUG_unit/iug_user_api                   \
                                 -I src/app/IUG_unit/simulation                     \
                                 -I src/app/IUG_unit/user_device                    \
                                 -I src/app/IUG_unit/wbus                           \
                                 -I src/app/IUG_unit_ghi                            \
                                 -I src/app/IUG_unit_ghi/simulation                 \
                                 -I src/app/IUG_user_api_deprecated                 \
                                 -I src/app/IUG_user_api_header                     \
                                 -I src/app/IUG_user_api_source                     \
                                 -I src/app/IUG_utility                             \
                                 -I src/app/WBus_Driver                             \
                                 -I src/app/WBus_Driver/constant                    \
                                 -I src/app/WBus_DriverCfg                          \
                                 -I src/app/WBus_SecurityAccess                     \
                                 -I src/ds                                          \
                                 -I src/ds/can                                      \
                                 -I src/ds/dsl                                      \
                                 -I src/ds/gp                                       \
                                 -I src/ds/io                                       \
                                 -I src/ds/lin                                      \
                                 -I src/ds/parameter                                \
                                 -I src/ds/user

INT_CONF_BSW_INCLUDE_PATH_BSP := -I src/bspRH850/config      \
                                 -I src/bspRH850/il/ecum     \
                                 -I src/bspRH850/il/fee      \
                                 -I src/bspRH850/il/memif    \
                                 -I src/bspRH850/lld/Adc     \
                                 -I src/bspRH850/lld/can     \
                                 -I src/bspRH850/lld/det     \
                                 -I src/bspRH850/lld/device  \
                                 -I src/bspRH850/lld/dio     \
                                 -I src/bspRH850/lld/fdl     \
                                 -I src/bspRH850/lld/fdl/lib \
                                 -I src/bspRH850/lld/fls     \
                                 -I src/bspRH850/lld/lin_sci \
                                 -I src/bspRH850/lld/main    \
                                 -I src/bspRH850/lld/mcu     \
                                 -I src/bspRH850/lld/ostm    \
                                 -I src/bspRH850/lld/port    \
                                 -I src/bspRH850/lld/pwm     \
                                 -I src/bspRH850/lld/reset   \
                                 -I src/bspRH850/lld/rtca    \
                                 -I src/bspRH850/lld/spi     \
                                 -I src/bspRH850/lld/stbc    \
                                 -I src/bspRH850/lld/timer   \
                                 -I src/bspRH850/startup

INT_CONF_BSW_INCLUDE_PATH_OS := -I src/os                                     \
                                -I src/os/freeRTOS                            \
                                -I src/os/freeRTOS/config                     \
                                -I src/os/freeRTOS/RTOS                       \
                                -I src/os/freeRTOS/RTOS/include               \
                                -I src/os/freeRTOS/RTOS/portable              \
                                -I src/os/freeRTOS/RTOS/portable/GHS          \
                                -I src/os/freeRTOS/RTOS/portable/GHS/RH850F1L

INT_CONF_BSW_INCLUDE_PATH_CFL := -I src/cfl/bleBlStack                 \
                                 -I src/cfl/bleBlStack/FCL             \
                                 -I src/cfl/bleBlStack/FCL/lib         \
                                 -I src/cfl/bleBlStack/spi_pmic_driver \
                                 -I src/cfl/linStack

INT_CONF_BSW_INCLUDE_PATH_CONFIG := -I src/config

INT_CONF_BSW_INCLUDE_PATH_DFL := -I src/dfl/bgm111  \
                                 -I src/dfl/L99PM72

INT_CONF_BSW_INCLUDE_PATH_HAL := -I src/halDef       \
                                 -I src/halImplLibc  \
                                 -I src/halImplRH850

INT_CONF_BSW_INCLUDE_PATH_SFL := -I src/sfl

INT_CONF_BSW_INCLUDE_PATH_GHS_TOOLCHAIN := -I toolchain/ghs/ansi         \
                                           -I toolchain/ghs/include/v800 \
                                           -I toolchain/ghs/scxx

# collector for all base software objects
INT_CONF_BSW_INCLUDE_PATHES := $(INT_CONF_BSW_INCLUDE_PATH_APP)           \
                               $(INT_CONF_BSW_INCLUDE_PATH_BSP)           \
                               $(INT_CONF_BSW_INCLUDE_PATH_OS)            \
                               $(INT_CONF_BSW_INCLUDE_PATH_CFL)           \
                               $(INT_CONF_BSW_INCLUDE_PATH_CONFIG)        \
                               $(INT_CONF_BSW_INCLUDE_PATH_DFL)           \
                               $(INT_CONF_BSW_INCLUDE_PATH_HAL)           \
                               $(INT_CONF_BSW_INCLUDE_PATH_SFL)           \
                               $(INT_CONF_BSW_INCLUDE_PATH_GHS_TOOLCHAIN)